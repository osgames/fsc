/*******************************************************************************
*  MSG.H                                                                       *
*	- Management of messages                                                   *
*									                                           *
*  FREE SPACE COLONISATION                                                     *
*      (c)2002 - 2017 Paul Mueller <muellerp61@bluewin.ch>                     *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*******************************************************************************/

#ifndef _FSC_MSG_H_
#define _FSC_MSG_H_

/*******************************************************************************
* ENUMS                               								           *
*******************************************************************************/

typedef enum
{
    MSG_TYPE_STD   = 100,
    MSG_TYPE_LOG   = 200,
    MSG_TYPE_ERROR = 201
}
E_MSGTYPE;

/*******************************************************************************
* TYPEDEFS                               								       *
*******************************************************************************/

typedef struct
{
    int  type;         /* Type of message for user filter      */
    int  num;          /* Number event description for translation into    */
                       /* 'human readable form'  / reaction of AI          */
                       /* Sub-Code for message                 */
    char from_no;      /* Sender of message                    */
    char to_no;        /* Number of player which should handle that event  */
    char map_x, map_y; /* Where the event happens / Focus      */
    int  pos;
    int  star_no;      /* Which star */
    int  planet_no;
    int  colony_no;
    int  tech_no;
    int  unit_no;
    int  improve_no;
    int  args[4];      /* Arguments depending on comd/msg      */
    char other;        /* For diplomacy messages               */
    char ally;         /* For diplomacy messages               */
    char is_immediate;
}
MSG_INFO_T;

/*****************************************************************************
* CODE                                   								     *
*****************************************************************************/

void msg_init(void);
void msg_prepare(char from_no, char to_no, int type, MSG_INFO_T *pmsg);
void msg_send(MSG_INFO_T *pmsg, char immediate_no);
void msg_send_short(char from_no, char to_no, int type, int num);

int msg_get(char receiver_no, MSG_INFO_T *pmsg, char is_immediate, char remove_it);

void msg_log_open(void);
void msg_log_close(void);

#endif  /* _FSC_MSG_H_  */
