/*******************************************************************************
*  FSCTOOL.H                                                                   *
*	    - Declarations and functions for Free Space Colonization tools	       *
*                                                                              *
*  FREE SPACE COLONISATION                                                     *
*      (c)2002 - 2017 Paul Mueller <muellerp61@bluewin.ch>                     *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*******************************************************************************/

#ifndef _FSC_TOOL_H_
#define _FSC_TOOL_H_

/*******************************************************************************
* CODE  								                                       *
*******************************************************************************/

void fsctool_rand_set_seed(int num);
int  fsctool_rand_no(int num);
int  fsctool_rand(void);
int  fsctool_haschance_percent(int chance);
int  fsctool_roll_dice(char dice[4]);
char fsctool_rand_range(char range[2]);
void fsctool_mix_numbers(char *plist, int num_element);

/* ===== Tools for list management ===== */
int  fsctool_list_movecursor(int *plist, int *cursor, int *value, int dir);
int  fsctool_list_find_element(int *plist, int element_no);
int  fsctool_list_remove_element(int *plist, int element_no);

/* ========= Other tools ====== */
char *fsctool_str_fromlist(char *strlist, int strno);
void fsctool_expand_bits2numbers(char *pbits, int *pnumbers, int num_bits);
void fsctool_shrink_numbers2bits(int *pnumbers, char *pbits, int num_bits);

#endif /* _FSC_TOOL_H_ */
