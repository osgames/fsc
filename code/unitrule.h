/*******************************************************************************
*  UNITRULE.H                                                                  *
*	    - Data and functions for the units movement rules             	       *
*                                                                              *
*	FREE SPACE COLONISATION               	                                   *
*   Copyright (C) 2002  Paul Mueller <pmtech@swissonline.ch>                   *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*******************************************************************************/

#ifndef _FSC_UNITRULE_H_
#define _FSC_UNITRULE_H_

/*******************************************************************************
* CODE							                                               *
*******************************************************************************/

void unitruleSetBonuses(int unit_no, short int *bonuses);
int  unitruleGetPosOrder(char ability, int pos, char mapflags, char *orderlist);
char unitruleGetPlanetOrder(int unit_no, int planet_no);
int  unitruleShipAttack(int attacker, int defender);

#endif  /* _FSC_UNITRULE_H_ */
