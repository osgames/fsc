/*******************************************************************************
*  SDLGLCFG.C                                                                  *
*      - Read procedures for the configuration of SDLGL                        *
*                                                                              *
*  SDLGL-Library                                                               *
*      (c) 2005-2017 Paul Mueller <muellerp61@bluewin.ch>                      *
*                                                                              *
*  This program is free software; you can redistribute it and/or modify        *
*  it under the terms of the GNU General Public License as published by        *
*  the Free Software Foundation; either version 2 of the License, or           *
*  (at your option) any later version.                                         *
*                                                                              *
*  This program is distributed in the hope that it will be useful,             *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of              *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
*  GNU Library General Public License for more details.                        *
*                                                                              *
*  You should have received a copy of the GNU General Public License           *
*  along with this program; if not, write to the Free Software                 *
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  *
*******************************************************************************/

/*******************************************************************************
* INCLUDES                                 				                       *
*******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <ctype.h>      /* tolower() */


/* -- Own header -- */
#include "sdlglcfg.h"

/*******************************************************************************
* DEFINES                             				                           *
*******************************************************************************/

#define SDLGLCFG_LINELEN  256

#define SDLGLCFG_BLOCKSIGN_START 0
#define SDLGLCFG_BLOCKSIGN_END   1
#define SDLGLCFG_COMMENT_SIGN    2

/*******************************************************************************
* DATA                                				                           *
*******************************************************************************/

static FILE *pTextFile;
static char BlockSigns[4];

/*******************************************************************************
* CODE                                				                           *
*******************************************************************************/

/*
 * Name:
 *     sdlglcfg_strlwr
 * Description:
 *     Converts the given string to lower chars
 * Input:
 *      pstr *: Where to put the line, must be 254 or more chars long!
 * Output:
 *      Argument, all chars lowered
 */
static char *sdlglcfg_strlwr(char *psrc)
{
    char *p;

    p = psrc;

    while(*p)
    {
        *p = tolower(*p);
        p++;
    }

    return psrc;
}

/*
 * Name:
 *     sdlglcfg_get_valid_line
 * Description:
 *     Gets a line from the textfile. Comments ( ";" will be stripped ).
 *     The returned line holds at least one char.
 * Input:
 *      data *: Where to put the line, must be 254 or more chars long!
 * Output:
 *      Is a valid line, yes/no   0: No more line available
 */
static int sdlglcfg_get_valid_line(FILE *f, char *data, int bufsize)
{
    char *poutstr,
         *pcomment;


    do
    {
        poutstr = fgets(data, bufsize, f);

        if(poutstr == 0)
        {
            /* EOF reached or error */
            fclose(f);
            return 0;
        }

        /* Look for comments: */
        pcomment = strchr(poutstr,';');

        if(pcomment != 0)
        {
            *pcomment = 0;
        }
        else
        {
            /* Remove carriage-return */
            pcomment = strchr(poutstr,'\n');

            if(pcomment != 0)
            {
                *pcomment = 0;
            }
        }

        if (*poutstr == 0)
        {
            /* Empty string */
            poutstr = 0;
        }
        else
        {
            return 1;
        }
    }
    while(poutstr == 0);

    return 0;
}

/*
 * Name:
 *     sdlglcfg_strtoval
 * Description:
 *
 *     Gets a line from the textfile. Comments ( ";" will be stripped ).
 *     The returned line holds at least one char.
 * Input:
 *      pstring *: To change to pvalue
 *      valtype:  This kind of pvalue is in iven pstring
 *      pvalue *:  Where to return the pvalue
 *      vallen:   length, if pstring.
 */
static void sdlglcfg_strtoval(char *pstring, char valtype, void *pvalue, int vallen)
{
    int   ivalue;
    float fvalue;
    char *pstr;


    if(pstring[0] == 0)
    {
        return;
    }

    if(valtype == SDLGL_VAL_STRING || valtype == SDLGL_VAL_ONECHAR)
    {
        pstr = (char *)pvalue;

        /* Remove trailing spaces: 2010-01-02 <bitnapper> */
        while(*pstring == ' ')
        {
            pstring++;
        }

        if(valtype == SDLGL_VAL_STRING)
        {
            strncpy(pstr, pstring, vallen);
            pstr[vallen] = 0;
        }
        else
        {
            *pstr = *pstring;
        }
    }
    else
    {
        /* TODO: (2008-06-20) : Support an array of values,  if 'vallen' is given ==> do while(vallen > 0) */
        /* It's a pvalue */
        sscanf(pstring, "%d", &ivalue);

        switch(valtype)
        {
        case SDLGL_VAL_CHAR:
            *(char *)pvalue = (char)ivalue;
            break;

        case SDLGL_VAL_UCHAR:
            *(unsigned char *)pvalue = (unsigned char)ivalue;
            break;

        case SDLGL_VAL_SHORT:
            *(short int *)pvalue = (short int)ivalue;
            break;

        case SDLGL_VAL_USHORT:
            *(unsigned short int *)pvalue = (unsigned short int)ivalue;
            break;

        case SDLGL_VAL_INT:
            *(int *)pvalue = (int)ivalue;
            break;

        case SDLGL_VAL_UINT:
            *(unsigned int *)pvalue = (unsigned int)ivalue;
            break;

        case SDLGL_VAL_FLOAT:
            sscanf(pstring, "%f", &fvalue);
            *(float *)pvalue = fvalue;
            break;

        case SDLGL_VAL_BOOL:
            *(char *)pvalue = (pstring[0] == 'T') ? 0x01 : 0x00;
            break;
        }
    }
}

/*
 * Name:
 *     sdlglcfg_valtostr
 * Description:
 *     Converts given pvalue pointed with given type to a string
 * Input:
 *     pvalstr *: Where to return the string
 *     val_type:  Type of pvalue given in 'pvalue'
 *     pvalue *:  Pointer on pvalue of 'type'
 *     val_len:   Maximum length for strings
 * Output:
 *     Value as string
 */
static char *sdlglcfg_valtostr(char *pvalstr, char val_type, void *pvalue, int val_len)
{
    /* Init as empty string */
    pvalstr[0] = 0;

    if (pvalue)
    {
        switch(val_type)
        {
            case SDLGL_VAL_ONECHAR:
                val_len = 1;
            case SDLGL_VAL_NONE:
            case SDLGL_VAL_STRING:
                strncpy(pvalstr, (char *)pvalue, val_len);
                pvalstr[val_len] = 0;
                break;

            case SDLGL_VAL_CHAR:
                sprintf(pvalstr, "%d", (int)*(char *)pvalue);
                break;

            case SDLGL_VAL_SHORT:
                sprintf(pvalstr, "%d", (int)*(short int *)pvalue);
                break;

            case SDLGL_VAL_INT:
                sprintf(pvalstr, "%d", *(int *)pvalue);
                break;

            case SDLGL_VAL_UCHAR:
                sprintf(pvalstr, "%u", (int)*(unsigned char *)pvalue);
                break;

            case SDLGL_VAL_USHORT:
                sprintf(pvalstr, "%u", (int)*(unsigned short int *)pvalue);
                break;

            case SDLGL_VAL_UINT:
                sprintf(pvalstr, "%u", *(int *)pvalue);
                break;

            case SDLGL_VAL_FLOAT:
                sprintf(pvalstr, "%.5f", *(float *)pvalue);
                break;

            case SDLGL_VAL_BOOL:
                pvalstr[0] = *(char *)pvalue ? 'T' : 'F';
                pvalstr[1] = 0;
                break;

            default:
                /* Return empty string, if unknown */
                pvalstr[0] = 0;
                break;
        }

        /* Clamp, if needed */
        pvalstr[val_len] = 0;
    }

    return pvalstr;
}

/*
 * Name:
 *     sdlglcfg_get_namedvalue
 * Description:
 *     Gets a line from the textfile. Comments ( ";" will be stripped ).
 *     The returned line holds at least one char.
 * Input:
 *      pline *:    Line to get the value from
 *      pvallist *: Holding all possible values with names
 */
static void sdlglcfg_get_namedvalue(char *pline, SDLGLCFG_NAMEDVALUE_T *pvallist)
{
    char valstr[128];
    char namestr[128];
    char *pval;


    /* FIXME: Check for blocksign and return it, if needed */
    /* -------- */
    pval = strchr(pline, '=');

    if(pval)
    {
        /* Found an equal key_code -- Scan name later*/
        *pval = 0;
        pval++;

        sscanf(pline, "%s", namestr);
        sscanf(pval, "%s", valstr);

        if(*pval != 0)
        {
            sdlglcfg_strlwr(namestr); /* To lower for comparision */

            while(pvallist->type)
            {
                if (! strcmp(namestr, pvallist->pname))
                {
                    sdlglcfg_strtoval(valstr, pvallist->type, pvallist->pdata, pvallist->len);
                }

                pvallist++;
            }
        }
    }
}

/*
 * Name:
 *     sdlglcfg_block_findnext
 * Description:
 *     Gets the next line signed with '@'
 * Input:
 *      pname *: Where to return the name of the next block
 * Output:
 *     Found on yes/no
 */
static int sdlglcfg_block_findnext(char *pname)
{
    char line[SDLGLCFG_LINELEN];


    while(sdlglcfg_get_valid_line(pTextFile, line, SDLGLCFG_LINELEN - 2))
    {
        if(line[0] == '@')
        {
            strcpy(pname, &line[1]);
            return 1;
        }
    }

    /* Set to empty string */
    pname[0] = 0;

    return 0;
}

/*
 * Name:
 *     sdlglcfg_block_checkname
 * Description:
 *     Checks if given line holds a block start.
 * Input:
 *     pline *:      Pointer on line to check
 *     pblockname *: Name of block, if one is found
 *  Output:
 *     Pointer on name of block, if any is found
 */
static int sdlglcfg_block_checkname(char *pline, char *pblockname)
{
    if(pline[0] == '@')
    {
        sscanf(pline, "%s", pblockname);
        sdlglcfg_strlwr(pblockname);      /* To lower for comparision of names */

        return 1;
    }

    pblockname[0] = 0;

    return 0;
}

/*
 * Name:
 *     sdlglcfg_linetorecord
 * Description:
 *     Reads a 'record' from given 'line'. It's assumed that the values
 *     are delimited by a comma.
 * Input:
 *     line *:    Line to read values from
 *     prcf *:     Description of values on given line to read from
 *     fixedpos:  Values have a fixed position in line given in value-lenght
 */
static void sdlglcfg_linetorecord(char *line, SDLGLCFG_VALUE_T *prcf, int fixedpos)
{
    char *pbuf, *pbrk;


    /* Just in case it lacks... : Delimiter for last value in string */
    strcat(line, ",");
    pbuf = &line[0];

    /* Scan trough all descriptors */
    while(prcf->type > 0)
    {
        if(fixedpos)
        {
            /* Fixed column positions */
            sdlglcfg_strtoval(pbuf + prcf->pos, prcf->type, prcf->pdata, prcf->len);
        }
        else
        {
            /* data in 'buffer' is decomma-limited  */
            pbrk = strchr(pbuf, ',');

            if(pbrk)
            {
                *pbrk = 0;          /* End of string to scan value from */
                sdlglcfg_strtoval(pbuf, prcf->type, prcf->pdata, prcf->len);
                pbuf = &pbrk[1];    /* Next value                      */
            }
            else
            {
                /* No value anymore, bail out */
                break;
            }
        }

        prcf++;
    }
}


/* ========================================================================== */
/* ========================= PUBLIC FUNCTIONS =============================== */
/* ========================================================================== */

/*
 * Name:
 *     sdlglcfg_read_cfgfile
 * Description:
 *     Read a simple config file with named values
 * Input:
 *     pfilename *: Name of file to read in
 *     pvallist  *: List of named values
 */
void sdlglcfg_read_cfgfile(char *pfilename, SDLGLCFG_NAMEDVALUE_T *pvallist)
{
    FILE *pfile;
    char line[SDLGLCFG_LINELEN];


    pfile = fopen(pfilename, "rt");

    if(pfile)
    {
        while(sdlglcfg_get_valid_line(pfile, line, SDLGLCFG_LINELEN - 2))
        {
            sdlglcfg_get_namedvalue(line, pvallist);
        }
    }
}

/*
 * Name:
 *     sdlglcfg_read_data
 * Description:
 *     Reads data for all data given in array of 'precdef'
 * Input:
 *     pfilename *: Name of file to read data from
 *     precdef *:   Array of data definitions, to read from file
 */
int  sdlglcfg_read_data(char *pfilename, SDLGLCFG_RECORD_T *precdef)
{
    FILE *pfile;
    SDLGLCFG_RECORD_T *pdef;

    char line[SDLGLCFG_LINELEN];
    int num_data;
    char *pbase_rec, *pact_rec;
    int max_rec;


    pfile = fopen(pfilename, "rt");

    if(pfile)
    {
        num_data = 0;

        while(sdlglcfg_get_valid_line(pfile, line, SDLGLCFG_LINELEN - 2))
        {
            /* As long as there are valid lines... */
            if(line[0] == '@')
            {
                /* Check for EOF */
                if(line[1] == '@')
                {
                    fclose(pfile);

                    /* Return how much different data we have read */
                    return num_data;
                }

                /* Look up, if we have a definition for this name */
                sdlglcfg_strlwr(line);

                pdef = precdef;

                while(pdef->max_rec > 0)
                {
                    /* For comparision: All to lower case */
                    sdlglcfg_strlwr(pdef->data_name);

                    if(!strcmp(&line[1], pdef->data_name))
                    {
                        pbase_rec = (char *)pdef->precdata;
                        pact_rec  = pbase_rec + pdef->rec_size;
                        max_rec   = pdef->max_rec;

                        /* -- We found a definition to use */
                        num_data++;
                        break;
                    }

                    /* Try next definition */
                    pdef++;
                }
            }
            else
            {
                if(pdef->max_rec > 0)
                {
                    /* If it's a valid record definition */
                    if(max_rec > 0)
                    {
                        /* if not filled yet */
                        sdlglcfg_linetorecord(line, pdef->prcf, pdef->fixed_pos);

                        /* Copy data to actual record */
                        memcpy(pact_rec, pbase_rec, pdef->rec_size);

                        pact_rec += pdef->rec_size;

                        max_rec--;
                    }
                }
            }
        }
    }

    return 0;
}

/*
 * Name:
 *     sdlglcfg_file_open
 * Description:
 *     Opens given file as textfile for reading in line by line
 *     or to write at it line by line with write function
 * Input:
 *     filename *:  Name of file to read in
 *     blocksigns:  Start and (possible) end of 'blockname'
 *     write:       Open file for writing values back
 *     pnamenext *: Name of next block to read, if reading
 */
int sdlglcfg_file_open(char *pfilename, char blocksigns[4], int write, char *pnamenext)
{
    if(write)
    {
        pTextFile = fopen(pfilename, "wt");

        pnamenext[0] = 0;
    }
    else
    {
        pTextFile = fopen(pfilename, "rt");
    }

    if(pTextFile)
    {
        if(blocksigns[0])
        {
            /* Save this block-signs for later use with other functions */
            BlockSigns[0] = blocksigns[0];
            BlockSigns[1] = blocksigns[1];
            BlockSigns[2] = blocksigns[2];
            BlockSigns[3] = blocksigns[3];

            if(BlockSigns[1] == ' ')
            {
                BlockSigns[1] = 0;
            }

            if (! write)
            {
                return sdlglcfg_block_findnext(pnamenext);
            }
        }
        else
        {
            return 1;
        }
    }

    return 0;
}

/*
 * Name:
 *     sdlglcfg_file_close
 * Description:
 *     Closes actual text file, if open.
 * Input:
 *     None
 */
void sdlglcfg_file_close(void)
{
    fclose(pTextFile);
}

/*
int  sdlglcfg_write_data(char *pfilename, SDLGLCFG_RECORD_T *precdef);
*/

/*
 * Name:
 *     sdlglcfg_read_namedvalues
 * Description:
 *     Read values from new block, that has started
 * Input:
 *     pvallist *:  Named values to look for
 *     pnamenext *: Name of next block to read data from, if any
 * Output:
 *     Name of new block, if any
 */
int sdlglcfg_read_namedvalues(SDLGLCFG_NAMEDVALUE_T *pvallist, char *pnamenext)
{
    char line[SDLGLCFG_LINELEN];


    while(sdlglcfg_get_valid_line(pTextFile, line, SDLGLCFG_LINELEN -  2))
    {
        if(sdlglcfg_block_checkname(line, pnamenext))
        {
            /* Start of a new block */
            return 1;
        }
        else
        {
            sdlglcfg_get_namedvalue(line, pvallist);
            return 1;
        }

    }

    return 0;
}

/*
 * Name:
 *     sdlglcfg_read_records
 * Description:
 *
 *     Opens given file as textfile for reading in line by line
 * Input:
 *     pdef *:      Definition of a data block and its data fields
 *     fixedpos:    Values have a fixed position in line given in value-lenght
 *     pnamenext *: Name of next block to read
 * Output:
 *     New block to read yes/no
 */
int sdlglcfg_read_records(SDLGLCFG_RECORD_T *pdef, int fixedpos, char *pnamenext)
{
    char line[SDLGLCFG_LINELEN];
    char *pbaserec, *pactrec;
    int maxrec;


    pbaserec = (char *)pdef->precdata;
    pactrec  = pbaserec + pdef->rec_size;
    maxrec   = pdef->max_rec;

    while(sdlglcfg_get_valid_line(pTextFile, line, SDLGLCFG_LINELEN - 2))
    {
        if(sdlglcfg_block_checkname(line, pnamenext))
        {
            /* Start of a new block */
            return 1;
        }
        else
        {
            if(maxrec > 0)
            {
                /* if not filled yet */
                sdlglcfg_linetorecord(line, pdef->prcf, fixedpos);

                /* Copy data to actual rec */
                memcpy(pactrec, pbaserec, pdef->rec_size);

                pactrec += pdef->rec_size;

                maxrec--;
            }
        }
    }

    /* End of file */
    return 0;
}

/*
 * Name:
 *     sdlglcfg_read_strings
 * Description:
 *     Reads strings from a file, buffers are descripted in 'pinfo'
 *     The strings are copied into 'pbuffer' until there is no 'buf_size' left
 * Input:
 *     pfilename *: Name of file to read from 
 *     pinfo *:     Description of 
 *     fixedpos:    Values have a fixed position in line given in value-lenght
 *     pnamenext *: Name of next block to read
 * Output:
 *     
 */
int sdlglcfg_read_strings(char *pfilename, SDLGLCFG_STRINGS_T *pinfo, char *pbuffer, int buf_size)
{    
    FILE *pfile;
    SDLGLCFG_STRINGS_T *pdesc;

    char line[SDLGLCFG_LINELEN], *pstr;
    int num_data, str_len;


    pfile = fopen(pfilename, "rt");

    if(pfile)
    {
        num_data = 0;
        pbuffer[0] = 0;
        pbuffer[1] = 0;

        while(sdlglcfg_get_valid_line(pfile, line, SDLGLCFG_LINELEN - 2))
        {
            /* As long as there are valid lines... */
            if(line[0] == '@')
            {
                /* Check for EOF */
                if(line[1] == '@')
                {
                    fclose(pfile);

                    /* Return how much different data we have read */
                    return num_data;
                }

                /* Look up, if we have a definition for this name */
                /* Definition has always to be upper case        */  

                pdesc = pinfo;

                while(pdesc->which > 0)
                {
                    if(!strcmp(&line[1], pdesc->name))
                    {
                        /* Where to add the next string */
                        pdesc->plist = pbuffer;

                        /* -- We found a definition to use */
                        num_data++;
                        break;
                    }

                    /* Try next definition */
                    pdesc++;
                }
            }
            else
            {
                str_len = strlen(line);                

                if(buf_size > (str_len + 10))
                {
                    /* There's enough space left in the buffer */
                    strcpy(pbuffer, line);
                    /* Point behind the trailing zero */
                    pbuffer += str_len;
                    pbuffer++;
                }
            }
        }   
    } 
}

/* === Functions: Write data into a actual opened text file === */

/*
 * Name:
 *     sdlglcfg_write_line
 * Description:
 *     Writes given string into the actual opened file
 * Input:
 *     pline *: Pointer on an array of 'SDLGLCFG_FILE_T' descriptions
 */
void sdlglcfg_write_line(char *pline)
{
    fprintf(pTextFile, "%s\n", pline);
}

/*
 * Name:
 *     sdlglcfg_write_namedvalues
 * Description:
 *     Writes all names and its values from 'vallist' into actual opened text-file
 * Input:
 *     pvallist *: Pointer on a descritor of named values
 */
int sdlglcfg_write_namedvalues(SDLGLCFG_NAMEDVALUE_T *pvallist)
{
    char val_str[512];


    while(pvallist->type > 0)
    {
        sdlglcfg_valtostr(val_str, pvallist->type, pvallist->pdata, pvallist->len);

        fprintf(pTextFile, "%s = %s\n", pvallist->pname, val_str);

        pvallist++;
    }

    return 1;
}

/*
 * Name:
 *     sdlglcfg_write_records
 * Description:
 *     Writes all records and its values from 'lineinfo' into actual
 *     Writes a block name into the actual opened file
 *     Releases the buffer of given file
 * Input:
 *     precinfo *: Info about the records to write
 *     fixedpos:   Use 'len' as 'fixedpos' for writing of values
 */
int sdlglcfg_write_records(SDLGLCFG_RECORD_T *precinfo, int fixedpos)
{
    char val_str[SDLGLCFG_LINELEN];
    char line[SDLGLCFG_LINELEN];
    SDLGLCFG_VALUE_T *prcf;
    char *pbaserec, *pactrec;
    char *end;
    int fix_len, len;
    int maxrec;


    pbaserec = (char *)precinfo->precdata;
    pactrec  = pbaserec + precinfo->rec_size;
    maxrec   = precinfo->max_rec;

    while(pbaserec[0] > 0)
    {
        memcpy(pbaserec, pactrec, precinfo->rec_size);

        line[0] = 0;
        fix_len = 0;

        if(maxrec > 0)
        {
            /* if not filled yet */
            /* Scan trough all descriptors */
            prcf = precinfo->prcf;

            while(prcf->type > 0)
            {
                sdlglcfg_valtostr(val_str, prcf->type, prcf->pdata, prcf->len);

                if(fixedpos)
                {

                    /* Fixed column positions */
                    len = strlen(val_str);

                    strncpy(line + prcf->pos, val_str, len);

                    fix_len = prcf->pos + len;
                }
                else
                {
                    strcat(line, val_str);
                    strcat(line, ",");      /* data in 'buffer' is decomma-limited  */
                }

                prcf++;
            }
        }

        if(fixedpos)
        {
            line[fix_len] = 0;
        }
        else
        {
            end = strchr(line, 0);

            if(end > line && end[-1] == ',')
            {
                /* Remove trailing limiter */
                end[-1] = 0;
            }
        }

        /* Write it to file */
        fprintf(pTextFile, "%s\n", line);

        /* Get the next one */
        pactrec += precinfo->rec_size;

        maxrec--;
    }

    return 1;
}
