/* *****************************************************************************
*  GAME.C                                                                      *
*      - Definitions and data for the main game                                *
*                                                                              *
*  FREE SPACE COLONISATION                                                     *
*      (c)2002 - 2017 Paul Mueller <muellerp61@bluewin.ch>                     *
*                                                                              *
*  This program is free software; you can redistribute it and/or modify        *
*  it under the terms of the GNU General Public License as published by        *
*  the Free Software Foundation; either version 2 of the License, or           *
*  (at your option) any later version.                                         *
*                                                                              *
*  This program is distributed in the hope that it will be useful,             *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of              *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
*  GNU Library General Public License for more details.                        *
*                                                                              *
*  You should have received a copy of the GNU General Public License           *
*  along with this program; if not, write to the Free Software                 *
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  *
*******************************************************************************/

/* *****************************************************************************
* INCLUDES                                 				                       *
*******************************************************************************/

#include <stdio.h>          /* sprintf()                            */
#include <string.h>         /* strcpy()                             */


#include "fscshare.h"       /* Values used all over the game */
#include "fscmap.h"
#include "game2str.h"
#include "mapgen.h"         /* mapgen_new_map()              */
#include "msg.h"
#include "player.h"
#include "starsys.h"        /* starsys_init(), get list of  planets         */

/*
#include "colony.h"         / *  Settle Start-Colonies, Get the list of colonies    * /
#include "gamefile.h"       / * Load game                                    * /
#include "fscmap.h"         / * Set seen after 'settlement' start-position   * /

#include "unitinfo.h"
#include "unit.h"

#include "rules.h"
#include "fsctool.h"
*/

/* -- Own header -- */
#include "game.h"

/* *****************************************************************************
* DEFINES                               				                       *
*******************************************************************************/

#define GAME_MAIN_VERSION   0x04
#define GAME_SUB_VERSION    0x15
#define GAME_SVN_VERSION    218

#define GAME_BASE_YEAR      2100

#define GAME_MAX_PLAYER     8

/* *****************************************************************************
* TYPEDEFS                               				                       *
*******************************************************************************/

typedef struct
{
    /* ------------- Sign for saved game ------------------------------ */
    unsigned char mainversion;
    char subversion;                /* First subversion                 */
    int  svn_version;               /* Version in SVN-Tree              */
    int  base_year;
    char num_player;                /* Number of players in game        */
    /* ------------- Main info -------------------------- */
    char act_player;                /* Number of actual player          */
    char player_nos[10];            /* 0..7                             */
    char ai_control[10];            /* 0..7                             */
    /* ------------ General game data ------------ */
    int  turn_no;                   /* Months passed since game start   */
    GAME_SETTINGS_T settings;
}
GAME_T;

/* *****************************************************************************
* DATA                             				                               *
*******************************************************************************/

static GAME_T Base_Game =
{
    GAME_MAIN_VERSION, GAME_SUB_VERSION, GAME_SVN_VERSION,
    GAME_BASE_YEAR,             /* Year the game starts     */
    0,                          /* Number of actual player  */
    1,
    { 0, 1, 2, 3, 4, 5, 6, 7, 0 },
    { 1, 0, 0, 0, 0, 0, 0, 0, 0 },
    0
};

/* Information about actual game running */
static GAME_T Act_Game =
{
    GAME_MAIN_VERSION, GAME_SUB_VERSION, GAME_SVN_VERSION,
    GAME_BASE_YEAR     /* Year the game starts */
};

static GAME_PLAYINFO_T Act_PlayInfo[GAME_MAX_PLAYER + 2];

/*******************************************************************************
* CODE                               				                           *
*******************************************************************************/
#if 0
/*
 * Name:
 *     game_ibeginturn
 * Description:
 *     Does all work needed before a turn starts for a player
 * Input:
 *     player_no: For this nation
 *     turn_no:   Stats for this turn
 */
static void game_ibeginturn(char player_no)
{
}


/*
 * Name:
 *     game_iendturn
 * Description:
 *     Does all work needed after a turn is done for a player
 * Input:
 *     player_no: For this nation
 *     turn_no:   Stats for this turn
 */
static void game_iendturn(char player_no)
{

    int values[GVAL_MAX + 2];      /* For nation           */


    /* ----- Clear buffer for fill in of values ------- */
    memset(&values[0], 0, (GVAL_MAX + 2) * sizeof(int));

    /* 2) Update activities of units ------------------ */
    /* unitmoveEndTurn(player_no);                      */

    /* 3) Do update on all outposts ------------------- */
    /* outpostEndOfTurn(player_no, values);             */

    /* 4) Do update on all colonies ------------------- */
    /* colonyEndTurnUpdate(player_no, values);          */

    /* 5) Update all the player info (values from planets and outposts */
    /* nationEndTurnUpdate(player_no, values);          */

    /* 6) Update all players statistics --------------- */
    /* statsUpdate(gameGetTurn(), player_no, values);   */

    /* Restore moves for all active units    */
    /* unitRestoreMoves(player_no);          */
}

/*
 * Name:
 *     game_iendround
 * Description:
 *     Does all work after all players are done with their moves
 * Input:
 *     None
 */
static void game_iendround(void)
{
    /* Do terraform of planets          */
    /* Build outposts                   */
    /* Fill up the moves for all units  */
}
#endif
/*
 * Name:
 *     game_settle_startpos
 * Description:
 *     Using start positions, the start systems are settled by given players
 * Input:
 *      pinfo *: Pointer on map generation info, holding the start positions
 * Last change:
 *      2017-06-28 <bitnapper>
 */
static void game_settle_startpos(GAME_STARTINFO_T *pinfo)
{
    static char yield_weight[3] = { 3, 1, 1 };     /* Weight on food! */

    int player_no;
    int map_pos;
    /*
    int colony_no;
    char *pimprove_key;
    char *punit_key;
    */
    int  home_planet;
    char terraform;
    /* int  colony_no; */


    for(player_no = 1; player_no <= pinfo->num_player; player_no++)
    {
        map_pos = pinfo->start_pos_list[player_no];

        /* Get the home planet for this nation */
        home_planet = starsys_bestplanet(map_pos,
                                         'M',
                                         yield_weight,
                                         &terraform);

        player_init(player_no, map_pos, home_planet);

        if(home_planet > 0)
        {
            /* Initialize the player with needed info */
            player_init(player_no, map_pos, home_planet);
            #if 0
            /* --- Settle the home planet --- */
            colony_no = colony_settle_planet(home_planet,
                                             player_no,
                                             rules_start_population(pinfo->difficulty));

            /* ---------- Now add the basic buildings to colony ----------- */
            pimprove_key = rules_get_basic_buildings();

            while((*pimprove_key) > 0)
            {
                colony_build_improve(colony_no, pimprove_key);

                pimprove_key += 4;
            }

            /* Create basic units for given player_no */
            punit_key = rules_startunit_keys(pinfo->difficulty);

            while((*punit_key) > 0)
            {
                /* FIXME: Add only 'cargo_load' on special ships */
                colony_build_unit(colony_no, map_pos, punit_key);

                punit_key += 4;
            }
            #endif
        }
    }
}

/*
 * Name:
 *     game_new
 * Description:
 *     Does all initalizations for a new game
 * Input:
 *     pinfo *: Pointer on info for generation of game, NULL: Use default
 * Output:
 *     Success yes/no
 */
static int game_new(GAME_STARTINFO_T *pinfo)
{
    if(pinfo->num_player > 0)
    {
        /* Get basic values for the game */
        memcpy(&Act_Game, &Base_Game, sizeof(GAME_T));

        if(pinfo->num_player > GAME_MAX_PLAYER)
        {
            pinfo->num_player = GAME_MAX_PLAYER;
        }

        Act_Game.num_player = pinfo->num_player;

        /* Start with first player in list */
        Act_Game.act_player = 1;
        /* ------ Set turn counter to zero --------- */
        Act_Game.turn_no = 0;

        /* Generate a new map */
        mapgen_new_map(pinfo);
        /* ------- Put nations to its start systems and give them start info --- */
        /* Creates the units, too */
        game_settle_startpos(pinfo);

        return 1;
    }

    return 0;
}

/*
 * Name:
 *     game_choose_tile
 * Description:
 *     Translates a message if a tile is chosen
 * Input:
 *     vp_x, vp_y: Player has chosen tile at this position in viewport
 *     player_no: For this player
 * Output:
 *     File could be loaded true/false
 */
static void game_choose_tile(int vp_x, int vp_y, int player_no)
{
    /*
    FSCMAP_TILEINFO_T tile_info;
    int pos;


    pos = fscmap_pos_fromxy(pmsg->args[0], pmsg->args[0]);

    fscmap_get_tileinfo(pos, player_no, &tile_info);
    */
     /* ------ Actual unit has to be set to IDLE  */
    /*
        pmsg->type     = MSG_TYPE_UNIT;
        pmsg->unit_no  = UnitMngInfo.list[UnitMngInfo.cursor];
        pmsg->value1   = ORDER_NONE;
        pmsg->value2   = 0;
        / * If a unit is here, then choose this unit * /
        if(unitmoveChooseUnit(&UnitMngInfo, mappos, UNITMOVE_CHOOSE_POSITION)) {


            / * ------ Actual unit has to be set to IDLE  * /
            pmsg->type     = MSG_TYPE_UNIT;
            pmsg->unit_no  = UnitMngInfo.list[UnitMngInfo.cursor];
            pmsg->value1   = ORDER_NONE;
            pmsg->value2   = 0;
        }
        */
}

/*
 * Name:
 *     game_process_msg
 * Description:
 *     Processes all messages sent to the game
 * Input:
 *     game_type: Type of game to start
 *     pinfo *:   Pointer on info for generation of game
 * Output:
 *     File could be loaded true/false
 */
 /*
static void game_process_msg(MSG_INFO_T *pmsg)
{

}
*/

/*
 * Name:
 *     game_next_player
 * Description:
 *     Choose the next player for the turn
 * Input:
 *     game_type: Type of game to start
 *     pinfo *:   Pointer on info for generation of game
 * Output:
 *     File could be loaded true/false
 */
static void game_next_player(void)
{
    /*
    / * ----- Now set on 'no moves' because of cursor drawing --------- * /
    UnitMngInfo.moves_left = 0;
    UnitMngInfo.pos = 0;            / * Position not valid * /
    infobarHandleMsg(0, 0);         / * Clear possible message in buffer * /
    */
    Act_Game.act_player++;
}


/* ========================================================================== */
/* ========================= PUBLIC FUNCTIONS =============================== */
/* ========================================================================== */

/*
 * Name:
 *     game_start
 * Description:
 *     Starts a game, depending on arguments.
 * Input:
 *     game_no: Number of game to start
 *              -1:  It's a new game
 *              0:   It's a game in progress (auto-save)
 *              > 0: Number of game to load
 *     pinfo *: Pointer on Game-Startinfo, if new game
 * Output:
 *     File could be loaded true/false
 */
int game_start(int game_no, GAME_STARTINFO_T *pinfo)
{
    switch(game_no)
    {
        case GAME_NEWGAME:
            /* All is done, all data already generated */
            game_new(pinfo);
            return 1;

        case GAME_CURRENTGAME:
            /* All is done, all data already generated */
            game_new(pinfo);
            return 1;

        case GAME_SAVEDGAME:
            if(game_no >= 0)
            {
                /* Load this game */
                /* gamefile_load(pinfo->game_no); */
                return 1;
            }
            break;

    }

    return 0;
}

/* ========== Game basic player control functions ========= */

/*
 * Name:
 *     game_get_msg
 * Description:
 *     Returns the actual message for the type of player that is given
 * Input:
 *     pmsg *:   Where to fill in the message to process by the caller
 *     is_human: The caller is a human, yes / no
 * Output:
 *     There was a message t return yes/no
 */
int game_get_msg(MSG_INFO_T *pmsg, char is_human)
{
    GAME_T *pgame;
    int i;


    pgame = &Act_Game;

    for(i = 1; i < pgame->num_player; i++)
    {
        if(!pgame->ai_control[i])
        {
            if(is_human)
            {
                /* First try to return an immediate message */
                if(msg_get(pgame->player_nos[i], pmsg, 1, 1))
                {
                    /* Return the immediate message */
                    return 1;
                }
                else if(pgame->player_nos[i] == pgame->act_player)
                {
                    /* Return the next message from the message buffer */
                    msg_get(pgame->player_nos[i], pmsg, 0, 1);
                }
            }
        }
    }

    return 0;
}

/*
 * Name:
 *     game_get_result
 * Description:
 *     Gets a result from a command sent to the game
 * Input:
 *     is_human: Command is sent by a human
 *     cmd_no:   Numer of game command
 *     arg1, arg2: Additional arguments for 'cmd_no'
 */
int game_get_result(int player_no, MSG_INFO_T *pmsg, int is_human)
{

    return msg_get(player_no, pmsg, 1, 1);
}

/*
 * Name:
 *     game_send_input
 * Description:
 *     Send a command to the game for possible game actions.
 *     The comand is always for a human player.
 *     It's only executed if it's the humans player turn
 * Input:
 *     is_human:   Command is sent by a human
 *     cmd_no:     Number of game command
 *     sub_cmd_no: If part-command e.g direction for unit movement
 *     arg1, arg2: Additional arguments for 'cmd_no' e.g. x/y
 */
void game_send_input(int is_human, E_GAME_CMD cmd_no, int sub_cmd_no, int arg1, int arg2)
{
    FSCMAP_POSINFO_T pi;
    char ai_control;
    int pos_x, pos_y, map_pos;


    ai_control = Act_Game.ai_control[(int)Act_Game.act_player];

    if(is_human && !ai_control)
    {
        /* Only process if te actual player is a human (from interface) */
        switch(cmd_no)
        {
            case GCMD_ENDTURN:
                game_next_player();
                /*
                / * ----- Now set on 'no moves' because of cursor drawing --------- * /
                UnitMngInfo.moves_left = 0;
                UnitMngInfo.pos = 0;            / * Position not valid * /
                infobarHandleMsg(0, 0);         / * Clear possible message in buffer * /
                */
                break;

            case GCMD_VIEWPORT_MOVE:
                /* Move the viewport on map     */
                fscmap_viewport_move(sub_cmd_no);
                break;

            case GCMD_VIEWPORT_ZOOM:
                /* Zoom viewport in an out      */
                break;

            case GCMD_VIEWPORT_CENTER:
                /* Click on minmap (center) viewport    */
                if(sub_cmd_no == 0)
                {
                    /* Center to x/y */
                    fscmap_viewport_adjust(arg1, arg2, 1, 0);
                }
                else if(sub_cmd_no == 1)
                {
                    /* @TODO: 1: Center to active unit, if available */
                }
                else if(sub_cmd_no == 2)
                {
                    /* Center viewport to home planet: -2   */
                    map_pos = player_get_homepos(Act_Game.act_player);
                    printf("GCMD_VIEWPORT_CENTER : map_pos: %d\n", map_pos);
                    fscmap_xy_frompos(map_pos, &pi);
                    printf("GCMD_VIEWPORT_CENTER: map x/y: %d/%d\n\n", (int)pi.x, (int)pi.y);
                    fscmap_viewport_adjust(pi.x, pi.y, 1, 0);
                }
                break;

            case GCMD_VIEWPORT_CHOOSE:
                game_choose_tile(arg1, arg2, Act_Game.act_player);
                break;

            case GCMD_VIEWPORT_USE:
                /* game_use_tile(arg1, arg2, Act_Game.act_player); */
                /*  If unit is available
                    unitmove_goto();
                */
                break;

            case GCMD_UNIT_CHOOSE:
                /*
                unitmove_choose();
                */
                break;

            case GCMD_UNIT_MOVEDIR:
                break;

            case GCMD_UNIT_MOVEGOTO:
                break;

            case GCMD_UNIT_SKIP:
                break;

            case GCMD_UNIT_WAIT:
                break;

            case GCMD_UNIT_ORDER:
                break;

            case GCMD_CHOOSE_STAR:
                break;

            case GCMD_CHOOSE_PLANET:
                break;

            case GCMD_USE_PLANET:
                break;

            /* == Front end commands == */
            case GCMD_MENU:
                /* Front-End Command */
                break;

            case GCMD_ADVISOR:
                /* Front-End Command */
                break;

            case GCMD_MAP_CHOOSE:
                /*

                */
                break;

            case GCMD_NEWMAP:
            case GCMD_VIEW_MODE:
            case GCMD_CHOOSE_MSG:
                /* Front-End Command */
                break;

            /*
            case GCMD_UNIT_SKIP:
                unitmove_choose(&UnitMngInfo, 0, UNITMOVE_CHOOSE_NEXTSKIP);
                break;

            case GCMD_UNIT_WAIT:
                unitmove_choose(&UnitMngInfo, 0, UNITMOVE_CHOOSE_NEXT);
                break;
            */
        }
    }
}

/*
 * Name:
 *     game_main
 * Description:
 *     Calculates the consequences of all inputs, generate immediat messages
 *     if e.G. a Unit has to be move d on screen an so on.
 * Input:
 *     None
 */
void game_main(void)
{
    /* 1. Move next active unit by one move and send the message to all players
          which see Source- and Target-Position of moving unit: immediate */
}





/* ========== Act_Game additional functions =================== */

/*
 * Name:
 *     gameSettings
 * Description:
 *     Copies internal game data into given buffer or other way turn
 * Input:
 *     pgamedata *: Where to get/return the settings
 *     set:         if 'set', copy given data to game, otherwise copy game to buffer
 */
void gameSettings(GAME_SETTINGS_T *pgamedata, int set)
{

    if(set) {

        memcpy(pgamedata, &Act_Game.settings, sizeof(GAME_SETTINGS_T));

    }
    else {

        memcpy(&Act_Game.settings, pgamedata, sizeof(GAME_SETTINGS_T));

    }

}

/*
 * Name:
 *     game_fill_playinfo
 * Description:
 *     Returns the management data for the actual player.
 *
 * Input:
 *     player_no: Fill management- data for this player
 *     pinfo *:   Pointer on struct to fill in with data
 * Output:
 *     None
 */
void game_fill_playinfo(int player_no, GAME_PLAYINFO_T *pinfo)
{
    memset(pinfo, 0, sizeof(GAME_PLAYINFO_T));


    pinfo->player_no  = Act_Game.player_nos[player_no];
    pinfo->ai_control = Act_Game.ai_control[player_no];
    pinfo->home_pos   = player_get_homepos(player_no);


    /* @TODO: Create 'imp_avail' only once per round (GAME_PLAYINFO_T)  */
    /*
    pinfo->num_imp_avail = improve_get_availbits(pplayer->research.advancebits,
                                                 pinfo->imp_avail_bits);
    */
    /* List of available unit-types -- by tech (unittypeGetAvailable)  */
    /* @TODO: Create 'num_ut_avail' only once per round (GAME_PLAYINFO_T)  */
    /*
    pinfo->num_ut_avail  = unittype_get_avail(pplayer->research.advancebits,
                                              &pinfo->ut_avail[0]);

    pinfo->turn_no = Act_Game.turn_no;
    pinfo->year    = Act_Game.base_year + (Act_Game.turn_no / 48);
    pinfo->month   = (Act_Game.turn_no % 48) / 4;
    */
}

/*
 * Name:
 *     game_get_playinfo
 * Description:
 *     Returns the management data for the actual player. *
 * Input:
 *     pinfo *:   Pointer on struct to fill in with data
 * Output:
 *     None
 */
void game_get_playinfo(GAME_PLAYINFO_T *pinfo)
{
    /* @TODO: Fill it before returning it ? */
    memcpy(pinfo, &Act_PlayInfo[(int)Act_Game.act_player], sizeof(GAME_PLAYINFO_T));
}

/*
 * Name:
 *     game_get_unitinfo
 * Description:
 *     Returns the info about given unit *
 * Input:
 *     unit_no:     For unit with this number
 *     punitinfo *: Pointer on struct to fill in with data
 * Output:
 *     None
 */
void game_get_unitinfo(GAME_UNITINFO_T *punitinfo)
{
}

/* ========== Load and save data of game ========== */



/* ========== Other functions ========== */

/*
 * Name:
 *     game_get_version
 * Description:
 *     Generates a static string which holds the version number and the
 *     build date.
 * Input:
 *     None
 */
char *game_get_version(void)
{
    static char Version_Title[120];

    char subversion[12];


    if(Act_Game.subversion == 0)
    {
        /* Even sub-version */
        sprintf(subversion, "%d",((int)Act_Game.subversion));
    }
    else
    {
        sprintf(subversion, "%d-%0d", (int)Act_Game.subversion, Act_Game.svn_version);
    }

    sprintf(Version_Title, "V %d.%d.%s (Build: " __DATE__ ")",
            (int)(Act_Game.mainversion >> 4),
            (int)(Act_Game.mainversion & 0x0F),
            subversion);

    return Version_Title;
}

/*
 * Name:
 *    gameDebugInfo
 * Description:
 *     Generates a string holding general state data for game
 * Input:
 *      buffer *: To fill with debug info
 * Output:
 *      Pointer on given buffer.
 */
char *gameDebugInfo(char *buffer)
{

    /* Generate the debug string */
    sprintf(buffer, "Actual Player: %d\nNumber of Players: %d",
                    (int)Act_Game.act_player, (int)Act_Game.num_player);

    return buffer;
}

/*
 * Name:
 *     game_get_date
 * Description:
 *     Generates a string holding a data generated from number of turn
 * Input:
 *      pbuffer *: To fill with debug info
 * Output:
 *      Pointer on given buffer.
 */
void game_get_date(char *pbuffer)
{
    game2str_date(pbuffer, Act_Game.turn_no, Act_Game.base_year);
}

/*
 * Name:
 *     game_get_loadsave_info
 * Description:
 *     If 'save' is true then fill in struct 'info' with data needed for
 *     saving data given in 'data *'. 'numrec' must hold the maximum number
 *     of records that can be filled, 'recsize' the maximum size of record to
 *     be saved.
 * Input:
 *     recsize *: Where to fill in the size of the record
 *     numrec *:  Where to put the number of records to be saved
 *     save:      Return info for save / load given info into game
 * Output:
 *     Pointer on data to get data from / fill in with data
 */
int game_get_loadsave_info(FSCFILE_DATADESC_T *pdatadesc, char save)
{
    /* == Info about the Map-header == */
    pdatadesc->rec_info.data_name = FSCFILE_GAME;
    pdatadesc->rec_info.rec_size  = sizeof(GAME_T);
    pdatadesc->rec_info.num_rec   = (save) ? 1 : 1;
    pdatadesc->pdata = &Act_Game;

    return 1;
}
