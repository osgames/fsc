/*******************************************************************************
*  COLONY.C                                                                    *
*	   - Declarations and functions for handling game part of colonies         *
*                                                                              *
*   FREE SPACE COLONIZATION             								       *
*       Copyright (C) 2004 - 2017 Paul Mueller <pmtech@swissonline.ch>         *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*******************************************************************************/

/*******************************************************************************
* INCLUDES                                                                     *
*******************************************************************************/

#include <string.h>


#include "fscshare.h"
#include "fscmap.h"
#include "effect.h"
#include "game.h"
#include "improve.h"
#include "msg.h"
#include "player.h"
#include "starsys.h"
#include "unittype.h"


/* -- Own header -- */
#include "colony.h"

/*******************************************************************************
* DEFINES                                                                      *
*******************************************************************************/

#define COLONY_MAX            128
#define COLONY_IMPROVE_MAXAGE ((char)100)


/*******************************************************************************
* TYPEDEFS                                                                     *
*******************************************************************************/

typedef struct
{
    /* STRUCT freeze '2017-07-22' */
    char owner_no;      /* Owner of colony                      */
    int  planet_no;     /* Number of planet the colony is on    */
    /* --------- Basic values --------------------------------- */
    char pop_points;    /* Size of colony: Number of workers    */
                        /* Calculate population based on points */
                        /* Settle ships take at least one
                           population with them.
                           @TODO: Load more people (max. 3)
                           #RULES
                           */
    char pop_idle;      /* Number of Poulation which do not work    */
    char workers_yield; /* Maximum workers per yield                */
                        /* For any yield, all citizens can work     */
                        /* to the limit 'workersyield'              */
                        /* Allways: Planetsize: 2 => 4 .. 8 .. 12   */
    /* ---------- Workers per yield (points) ------------------------------ */
    char prod_use[GVAL_COLSHARE_MAX];   /* Workers set per production         */
    /* ----------- Bonuses in percent ------------------------------------- */
    char bonus_pts[GVAL_MAX];
    /* --------- Projects ------------------------------------------------- */
    COLONY_PROJ_T proj[COLONY_PROJ_MAX];
    /* Store: YIELD_FOOD: If > pop_points * 10 + (level of difficulty * 10) grow one point #RULES */
    short int prod_store[GVAL_PLANETYIELD_MAX];
    /* --------- Improvements built (List with bit for every improvement -- */
    unsigned char improve_bits[16];
    /* ---------- Additional state for terraforming and so on ------------- */
    char years_unhappy;    /* This will eventually keep track of how many years
                               a planet has been unhappy.  Depending on government
							   level, they will rebel and become independent
                               or switch to another player                  */
    int  age;               /* How many rounds does the colony exist?       */
    short int goal_no;      /* TODO: Colony works towards this GOAL_T         */
    char orig_owner_no;     /* Nationality of settlers for          */
                            /* conquered planets, if settled        */
                            /* Owner of planet                      */
    short int number;       /* Its number                           */
}
COLONY_T;

/*******************************************************************************
* ENUMS                                                                        *
*******************************************************************************/

/* #RULES#  -- Which resources are used in 'COLONY_T.prod_use' */
static int Resource_Kind[GVAL_COLSHARE_MAX] =
{
    /* Which resources are which kind ? */
    GVAL_FOOD, GVAL_RESOURCES,  GVAL_ECONOMY,
    /* GVAL_MILITARY, GVAL_SOCIAL, GVAL_STOCK    */
    GVAL_RESOURCES, GVAL_RESOURCES, GVAL_RESOURCES,
    /* GVAL_RESEARCH , GVAL_TAXES, GVAL_LUXURY   */
    GVAL_ECONOMY, GVAL_ECONOMY, GVAL_ECONOMY
};

/*******************************************************************************
* DATA                                                                         *
*******************************************************************************/

/* Colonies itself */
static COLONY_T Colonies[COLONY_MAX + 1];

/*******************************************************************************
* CODE                                                                         *
*******************************************************************************/

/*
 * Name:
 *     colony_get
 * Description:
 *     Returns the pointer on colony with given number, if number is valid
 * Input:
 *     None
 * Output:
 *     >0, if suceeded
 */
static COLONY_T *colony_get(int colony_no)
{
    if(colony_no > 0 && colony_no < COLONY_MAX)
    {
        return &Colonies[colony_no];
    }

    /* Return pointer on invalid colony  */
    Colonies[colony_no].owner_no = 0;

    return &Colonies[colony_no];
}

/*
 * Name:
 *     colony_get_new
 * Description:
 *     Returns the number of a buffer for a new colony.
 * Input:
 *     None
 * Output:
 *     >0, if suceeded
 */
static int colony_get_new(void)
{
    int i;


    for(i = 1; i < COLONY_MAX; i++)
    {
        if(Colonies[i].owner_no == 0)
        {
            return i;
        }
    }

    return 0;
}

/*
 * Name:
 *     colony_calc_prodtotal
 * Description:
 *     Calculates the production values based on 'pinfo->planet_yield'.
 *     'pinfo->planet_yield' MUST include all possible bonuses
 *     Does also an update on the values for 'turns left'
 * Input:
 *     pprod_use *: Calculate this usage of resources
 *     pproj *:     Pointe ron projects
 *     pinfo *:     Pointer on management info
 */
static void colony_calc_prodtotal(char *pprod_use, COLONY_PROJ_T *pproj, COLONY_INFO_T *pinfo)
{
    int i;


    for(i = 0; i < GVAL_COLSHARE_MAX; i++)
    {
        /* How many people work on this ? */
        pinfo->prod_total[i] = pprod_use[i];
        /* 'pinfo->planet_yield' MUST include all possible bonuses */
        pinfo->prod_val[i]   = pinfo->planet_yield[Resource_Kind[i]];
        /* Multiply with the planets yield */
        pinfo->prod_total[i] *= pinfo->prod_val[i];
    }

    /* Calculate the actual turns needed for the projects */
    pinfo->proj_turns_left[COLONY_PROJ_MILITARY] =
        unittype_build_time(pproj[COLONY_PROJ_MILITARY].which,
                            pproj[COLONY_PROJ_MILITARY].invested,
                            pinfo->prod_total[GVAL_MILITARY]);

    pinfo->proj_turns_left[COLONY_PROJ_SOCIAL] =
        improve_build_time(pproj[COLONY_PROJ_SOCIAL].which,
                           pproj[COLONY_PROJ_SOCIAL].invested,
                           pinfo->prod_total[GVAL_SOCIAL]);
}

/*
 * Name:
 *     colony_build_upgrade
 * Description:
 *     Builds upgradr with given number on given colony. Adds the effects
 *     for given upgrade to colony
 * Input:
 *     pcolony *:   Pointer on colony to build upgrades
 *     which:       Number of upgrade to build
 *     pkey_code *: If 'which' is not available
 */
static void colony_build_upgrade(COLONY_T *pcolony, int which, char key_code[4])
{
    PLANET_INFO_T planet_info;
    GAME_EFFECT_T ge;
    FSCMAP_POSINFO_T pi;
    int map_pos;





    if(improve_build(&pcolony->improve_bits[0], which, key_code, &ge) > 0)
    {
        map_pos = starsys_get_planetinfo(pcolony->planet_no, &planet_info);
        pi.owner = pcolony->owner_no;

        fscmap_xy_frompos(map_pos, &pi);

        /* If the improvement has effects... SHOULD! */
        effect_set(&ge, &pi, pcolony->number, pcolony->planet_no);
    }
}

/*
 * Name:
 *     colony_social_production
 * Description:
 *     Add social production to a colony
 * Input:
 *     pcolony *: Involving colony
 *     mgsinfo *: Pointer on message info to fill
 *     prod_pts:  This much points to add to production
 */
static int colony_social_production(COLONY_T *pcolony, MSG_INFO_T *msginfo, int prod_pts)
{
    COLONY_PROJ_T *proj;
    int turns_left;


    proj = &pcolony->proj[COLONY_PROJ_SOCIAL];

	/* Check if something is being built at all */
	if(proj->which > 0)
    {
        /* Add production */
        proj->invested += prod_pts;

	    turns_left = improve_build_time(proj->which, proj->invested, prod_pts);

        if(!turns_left)
        {
            /* Add building to the planets improvements list            */
            colony_build_upgrade(pcolony, proj->which, NULL);

        	/* inform player */
            msginfo->type       = MSG_TYPE_INFO;
            msginfo->num        = GMSG_IMPROVEBUILT;
            msginfo->improve_no = proj->which;

    	    msg_send(msginfo, 0);

    		/* Set prod points to zero */
            proj->invested = 0;
            /* @TODO:                                               */
            /* - Get next improvement to be built from governor for */
            /*   this colony. If governor is active at all          */
            proj->which    = 0;

            return 1;
        }
	}
    else
    {
        /* Move unused points to stock */
        pcolony->prod_store[GVAL_RESOURCES] += (short int)prod_pts;
    }

    return 0;
}


/*
 * Name:
 *     colony_military_production
 * Description:
 *     Add military production to a colony
 * Input:
 *     pcolony:   Involving colony
 *     msginfo *: Pointer on info about event
 *     prod_pts:  This much points to add to production
 */
static int colony_military_production(COLONY_T *pcolony, MSG_INFO_T *msginfo, int prod_pts)
{
    COLONY_PROJ_T *proj;
    int turns_left;
    /* int unit_no; */


	/* check if something being build */
    proj = &pcolony->proj[COLONY_PROJ_MILITARY];

	if(proj->which > 0)
	{
		/* add production to total */
        proj->invested += prod_pts;

		/* get unit cost value */
        turns_left = unittype_build_time(proj->which, proj->invested, 0);

		if(!turns_left)
		{
            /* Build the unit.  And put it to map at the    */
            /* position of the colony                       */
            /*
                FIXME: 'cargo_load' only needed for special kind of units:
                UNIT_ABILITY_CONSTRUCT: store[GVAL_RESOURCES];
                UNIT_ABILITY_COLONIZE: 'pop_points' OR 'pop_idle'
            */
#if 0
            unit_no = unit_create(proj->which,
                                  NULL,
                                  pcolony->owner_no,
                                  msginfo->pos,
                                  1);

            fcsmap_add_unit(msginfo->pos, unit_no);

			/* -------- Inform player that building is completed ------- */
            msginfo->num     = MSG_UNITBUILT;
            msginfo->unit_no = unit_no;
			msg_send(msginfo, 0);
#endif
			/* Set prod points to zero */
			proj->invested = 0;
		}
	}
    else
    {
        /* Move unused points to stock */
        pcolony->prod_store[GVAL_RESOURCES] += (short int)prod_pts;
    }

    return 0;
}

/* ========================================================================== */
/* ======================= PUBLIC FUNCTIONS ================================= */
/* ========================================================================== */

/*
 * Name:
 *     colony_init
 * Description:
 *     Initialises the buffer for the colonies.
 * Input:
 *     none
 */
void colony_init(void)
{
    memset(&Colonies[0], 0, COLONY_MAX * sizeof(COLONY_T));
}

/*
 * Name:
 *     colony_calc_yielduse
 * Description:
 *     Recalculates the values of the yield usage and the
 *     the number of turns used for production
 * Input:
 *     pinfo *:    Pointer on management info
 */
void colony_calc_yielduse(COLONY_INFO_T *pinfo)
{
    colony_calc_prodtotal(pinfo->prod_use, pinfo->proj, pinfo);
}

/*
 * Name:
 *     colony_get_info
 * Description:
 *     Fills info with data about the chosen colony.
 * Input:
 *     colony_no: Initialize this colony if > 0
 *                Otherwise do an update on given data, depending on 'which'
 *     pinfo *:   Fill this with data about given colony
 *     dir:       @TODO: if != 0: Look for next colony in this list-direction
 *     @TODO: Write changed values of old colony back to game
 */
void colony_get_info(int colony_no, COLONY_INFO_T *pinfo, int dir)
{
    PLAYER_RACE_T race;
    PLANET_INFO_T planet_info;
    COLONY_T *pcolony;
    int i;


    /* Fill in values for new colony */
    if(colony_no > 0 && colony_no < COLONY_MAX)
    {
        pcolony = &Colonies[colony_no];

        pinfo->colony_no  = colony_no;       /* Number of colony */
        pinfo->owner_no   = pcolony->owner_no;  /* Owner of colony  */
        pinfo->planet_no  = pcolony->planet_no;
        pinfo->pop_points = pcolony->pop_points;
        pinfo->pop_idle   = pcolony->pop_idle;
        pinfo->workers_yield = pcolony->workers_yield;

        /* And now get the projects */
        memcpy(pinfo->proj, pcolony->proj, COLONY_PROJ_MAX * sizeof(COLONY_PROJ_T));

        /* And now calculate all values */
        colony_calc_yielduse(pinfo);
        /* -- Now get the info about the planet -- */
        pinfo->map_pos = starsys_get_planetinfo(pcolony->planet_no, &planet_info);

        player_race(pcolony->owner_no, &race, 0);

        strcpy(pinfo->nation_name, race.name);
        strcpy(pinfo->planet_name, planet_info.name);
        strcpy(pinfo->planet_sign, planet_info.sign);

        for(i = 0; i < GVAL_PLANETYIELD_MAX; i++)
        {
            pinfo->planet_yield[i] = planet_info.yield[i];
        }
    }
}



/*
 * Name:
 *     colony_settle_planet
 * Description:
 *     Creates a new colony on given planet
 * Input:
 *     planet_no:  Number of colony the colony is on
 *     player_no:  Settles this planet
 *     pop_points: Number of pop_points which settles this colony
 * Output:
 *     > 0: Valid colony
 */
int colony_settle_planet(char player_no, int planet_no, char pop_points)
{
    static GAME_EFFECT_T effect[2] =
        {
            { GER_MAP, GVAL_SENSORS, 1 },
            { GER_MAP, GVAL_INFLUENCE, 101 }
        };

    FSCMAP_POSINFO_T pi;
    COLONY_T *pcolony;
    int  colony_no;
    int map_pos;

    colony_no = colony_get_new();

    if(colony_no > 0)
    {
        pcolony = &Colonies[colony_no];

        memset(pcolony, 0, sizeof(COLONY_T));

        /* -------- Set more basic parameters ------ */
        pcolony->number        = colony_no;
        pcolony->planet_no     = planet_no;
        pcolony->owner_no      = player_no;
        pcolony->orig_owner_no = player_no;
        pcolony->pop_points    = (pop_points < 1) ? 1 : pop_points;
        pcolony->pop_idle      = pcolony->pop_points;

        /* -------- Additional abilities -------- */
        pcolony->prod_use[GVAL_INFLUENCE] = 20;       /* Basic influence  */
        pcolony->prod_use[GVAL_MORALE]    = 50;       /* Basic morale     */
        pcolony->prod_use[GVAL_APPROVAL]  = 50;       /* Basic approval   */
        pcolony->prod_use[GVAL_NETINCOME] = 0;

        /* -------- Tell the planet, we're here and we're the owner ----- */

        /* @TODO: Replace any possible existing outpost stations with   */
        /*        improvements with same effects                        */
        map_pos = starsys_settle_planet(planet_no,
                                        player_no,
                                        STARSYS_SETTLETYPE_COLONY,
                                        colony_no,
                                        &pcolony->workers_yield);

        pi.owner = player_no;
        fscmap_xy_frompos(map_pos, &pi);

        /* We see the adjacent tiles */
        effect_set(&effect[0], &pi, colony_no, planet_no);

        /* --- Init the influence/ownership for a radius of one ---- */
        effect_set(&effect[1], &pi, colony_no, planet_no);
    }

    return colony_no;
}

/*
 * Name:
 *     colony_build_improve
 * Description:
 *     Builds improvement with given keycode in given colony. Adds the effect(s)
 *     of the improvement to colony
 * Input:
 *     colony_no:   Number of colony to build improvment
 *     key_code:   Code of improvement to build
 */
void colony_build_improve(int colony_no, char key_code[4])
{
    colony_build_upgrade(&Colonies[colony_no], 0, key_code);
}

/*
 * Name:
 *     colony_build_unit
 * Description:
 *     Builds unit with given number on given colony. Adds the effects
 *     for given improvement to colony
 * Input:
 *     colony_no:    Number of colony to build improvment
 *     map_pos:      At this position
 *     unit_ability: Code of improvement to build
 */
void colony_build_unit(int colony_no, int map_pos, int unit_ability)
{
    /* @TODO: Add proper code -- Has to be placed on map... */
    /*
    unit_no = unit_create(-1,
                          unit_ability,
                          &Colonies[colony_no].owner,
                          map_pos,
                          0);
    if(unit_no > 0)
    {
        fcsmap_add_unit(map_pos, unit_no);
    }
    */
}

/*
 * Name:
 *     colony_get_list
 * Description:
 *     Fills in given list with numbers of colonies for given owner
 * Input:
 *     nation_no: Get list for this owner
 *     plist *:   Fill in this list with colonies
 * Output:
 *     Number of elements in list
 */
int colony_get_list(char player_no, int *plist)
{
    int colony_no;
    int num_element;


    num_element = 0;
    colony_no   = 1;

    while(Colonies[colony_no].owner_no > 0)
    {
        if(Colonies[colony_no].owner_no == player_no)
        {
            /* Write number to list             */
            *plist = colony_no;

            /* Point on next element to fill    */
            plist++;
            num_element++;
        }

        colony_no++;
    }

    *plist = 0;     /* Sign end of list */

    return num_element;
}

/*
 * Name:
 *     colony_get_planetno
 * Description:
 *     Return the number of the planet the colony is on
 * Input:
 *     colony_no:  For this colony
 * Output:
 *      Number of planet
 */
int colony_get_planetno(int colony_no)
{
    return Colonies[colony_no].planet_no;
}

/* ============= Functions to change values =============== */

/*
 * Name:
 *     colony_add_bonus
 * Description:
 *     Add this production bonus to colony bonus
 * Input:
 *     colony_no:  For this colony
 *     bonus_type: GVAL_... This type of bonus
 *     ability:   This unit-ability / this improvements effect
 * Output:
 *     This ability / effect is held in given project type
 */
void colony_add_bonus(int colony_no, FSC_GVAL_TYPES val_type, char amount)
{
    Colonies[colony_no].bonus_pts[val_type] += amount;
}

/*
 * Name:
 *     colony_value
 * Description:
 *     Calculates the value of a colony for bribery attack or diplomacy.
 *     If 'count_capital', give it a special value.
 * Input:
 *     colony_no:     Evaluate value of this colony
 *     powner_no *:    Pointer where to return the owner of the colony
 *     count_capital: Make it 'very' expensive, if it's the owners capital
 * Output:
 *     Value of colony, in gold
 */
int colony_value(int colony_no, char *powner_no, int count_capital)
{
    PLANET_INFO_T planet_info;
    COLONY_T *pcolony;
    int yield[GVAL_MAX];
    int value, workers;


    value = 0;

    pcolony = colony_get(colony_no);

    starsys_get_planetinfo(pcolony->planet_no, &planet_info);

    workers = pcolony->pop_points;

    /* @TODO: Take callers weight into account */
    value += workers * 20;
    value += yield[GVAL_RESOURCES] * 8 / 10;
    value += yield[GVAL_ECONOMY] * 6 / 10;

    if(count_capital)
    {
        /* FIXME: Add a value of 100000 if it's the capital of the owner */
        value *= 2;
    }

    /* TODO: Add value of all buildings in this city.
              If count_capital ==> AI ==> Fuzzy value */
    /*
        city_built_iterate(pcity, pimprove) {
            want += impr_build_shield_cost(pimprove);
           if(improvement_obsolete(pplayer, pimprove)) {
                continue;
            }
            if(is_great_wonder(pimprove)) {
                want += impr_build_shield_cost(pimprove) * 2;
            } else if(is_small_wonder(pimprove)) {
                want += impr_build_shield_cost(pimprove);
            }
        } city_built_iterate_end;
        */

    *powner_no = pcolony->owner_no;

    return value;
}

/* ============= Other functions =========== */

/*
 * Name:
 *     colony_endturn
 * Description:
 *     Does an update on all planets. Loops through the whole list of
 *     planets, does an update on all values based on the games rules
 *     and informs the player and all other game parts which depends
 *     (e. g. the player about earning taxes and research points)
 *     about the consequences of the update.
 * Input:
 *     pstats *: Pointer on buffer to return the yield values for players for stats
 *
 * TODO: #RULES each improvement adds some of his bonuses every turn
 */
void colony_endturn(int *pstats)
{
    MSG_INFO_T msginfo;
    COLONY_INFO_T MngInfo;
    PLANET_INFO_T planet_info;
    COLONY_T *pcolony;
    /* int i; */


    /* Init Message-Info       */
    msg_prepare(0, 0, MSG_TYPE_GAME, &msginfo);


    /* == Loop trough all the colonies == */
    pcolony = &Colonies[1];

    while(pcolony->owner_no != 0)
    {
        /* Colonies with an owner -1 are deleted */
        if(pcolony->owner_no > 0)
        {
            /* The owner gets the message */
            msginfo.to_no     = pcolony->owner_no;
            msginfo.colony_no = pcolony->number;
            /* Save planet_no for log and information */
            msginfo.planet_no = pcolony->planet_no;

            /* @TODO: Use 'colony_get_mnginfo' */

            /* -- Get the info about the planet: yield -- */
            msginfo.pos = starsys_get_planetinfo(pcolony->planet_no, &planet_info);


            colony_calc_prodtotal(pcolony->prod_use, pcolony->proj, &MngInfo);

            /* Advance military production */
            colony_military_production(pcolony,
                                       &msginfo,
                                       MngInfo.prod_total[GVAL_MILITARY]);

            /* Advance social production */
            colony_social_production(pcolony,
                                     &msginfo,
                                     MngInfo.prod_total[GVAL_SOCIAL]);


            /* Return values for actual player */
#if 0
            for(i = 0; i < COLUSE_SHARE_MAX; i ++)
            {
                pstats[pcolony->owner_no][i] += mng.prodtotal[i];
            }

            /* @TODO: Grow the colony */

            /* Age of colony */
            pcolony->age++;

            /* ------ Additional statistic data ------ */
            pstats[pcolony->owner_no[GVAL_POPULATION] += mng.pcolony->pop_points;
            /* ------ TODO: Add influence for radius 1 ----------- */
            /* fscmap_set_influence(mng.pos,
                                  nation_no,
                                  1,
                                  mng.pcolony->pop_points / 100); */

            /* Stats: Number of colonies */
            pstats[pcolony->owner_no][GVAL_NUMCOLONY]++; */
#endif
        }

        /* Get next colony    */
        pcolony++;
    }
}


/* ========== Load and save data of colonies ========== */

/*
 * Name:
 *     colony_get_loadsave_info
 * Description:
 *     If 'save' is true then fill in struct 'info' with data needed for
 *     saving data given in 'data *'. 'numrec' must hold the maximum number
 *     of records that can be filled, 'recsize' the maximum size of record to
 *     be saved.
 * Input:
 *     pdatadesc *: Pointer on struct where to return the info about the data to laad/save
 *     save:      Return info for save / load given info into game
 * Output:
 *    Number of daa descriptions in 'pdatadesc'
 */
int colony_get_loadsave_info(FSCFILE_DATADESC_T *pdatadesc, char save)
{
    int num_rec;


    if(save)
    {
        num_rec = 1;

        while(Colonies[num_rec].owner_no != 0)
        {
            num_rec++;
        }

        num_rec--;
    }
    else
    {
        num_rec = COLONY_MAX;
    }

    /* == Info about the Map-header == */
    pdatadesc->rec_info.data_name = FSCFILE_COLONY;
    pdatadesc->rec_info.rec_size  = sizeof(COLONY_T);
    pdatadesc->rec_info.num_rec   = num_rec;
    pdatadesc->pdata = &Colonies[1];

    return 1;
}
