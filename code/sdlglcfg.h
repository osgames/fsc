/* *****************************************************************************
*  SDLGLCFG.H                                                                  *
*      - Read procedures for the configuration of SDLGL                        *
*                                                                              *
*  FREE SPACE COLONISATION                                                     *
*      Copyright (C) 2002-2017  Paul Müller <muellerp61@bluewin.ch>            *
*                                                                              *
*  This program is free software; you can redistribute it and/or modify        *
*  it under the terms of the GNU General Public License as published by        *
*  the Free Software Foundation; either version 2 of the License, or           *
*  (at your option) any later version.                                         *
*                                                                              *
*  This program is distributed in the hope that it will be useful,             *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of              *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
*  GNU Library General Public License for more details.                        *
*                                                                              *
*  You should have received a copy of the GNU General Public License           *
*  along with this program; if not, write to the Free Software                 *
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  *
*******************************************************************************/

#ifndef _SDLGL_CONFIG_H_
#define _SDLGL_CONFIG_H_

/* *****************************************************************************
* INCLUDES                                 				                       *
*******************************************************************************/

#include "../fscfe/sdlgldef.h"

/* *****************************************************************************
* TYPEDEFS                                 				                       *
*******************************************************************************/

typedef struct
{
    char type;     /* Type of value to read            */
    void *pdata;   /* Where to put the value           */
    char *pname;   /* name of value (case insensitive) */
    char len;      /* Len of data (strings and arrays) */
}
SDLGLCFG_NAMEDVALUE_T;

typedef struct
{
    char type;      /* Type of value to read                	*/
    void *pdata;    /* Where to put the data                	*/
    char len;       /* Len of data (strings and arrays)     	*/
    char pos;	    /* Of value in 'data-line' ignore delimiter	*/
}
SDLGLCFG_VALUE_T;

typedef struct
{
    int  max_rec;        /* Maximum of records                      */
    int  rec_size;       /* Size of record in buffer 'recdata'      */
    void *precdata;      /* Pointer on record data (any)            */
                         /* Points on record[0],                    */
                         /* holding data from SDLGLCFG_VALUE_T      */
    char data_name[16];  /* Name of data in file, except '@'        */
    SDLGLCFG_VALUE_T *prcf; /* Descriptor for values on single line */
    char fixed_pos;      /* Yes/no: Data not decomma limited        */
}
SDLGLCFG_RECORD_T;

typedef struct
{
    int which;          /* Which list of name to use, as number        */
    int min,            /* 1 or 0                                      */
        max;            /* Miniumum and maximum of string in list      */
    char name[16];      /* Name in Text-File (if loaded from such file */
    char *plist;        /* Pointer on list with strings [str]\0[str].. */
    int list_size;      /* For internal use                            */
}
SDLGLCFG_STRINGS_T;


/* *****************************************************************************
* CODE                                 				                           *
*******************************************************************************/

void sdlglcfg_read_cfgfile(char *pfilename, SDLGLCFG_NAMEDVALUE_T *pvallist);
int  sdlglcfg_read_data(char *pfilename, SDLGLCFG_RECORD_T *precdef);
int  sdlglcfg_file_open(char *pfilename, char blocksigns[4], int write, char *pnamenext);
void sdlglcfg_file_close(void);

/* int  sdlglcfg_write_data(char *pfilename, SDLGLCFG_RECORD_T *precdef); */
int sdlglcfg_read_namedvalues(SDLGLCFG_NAMEDVALUE_T *pvallist, char *pnamenext);
int sdlglcfg_read_records(SDLGLCFG_RECORD_T *precinfo, int fixedpos, char *pnamenext);
int sdlglcfg_read_strings(char *pfilename, SDLGLCFG_STRINGS_T *pinfo, char *pbuffer, int buf_size);

/* === Functions: Write data into a actual opened text file === */
void sdlglcfg_write_line(char *blockname);
int  sdlglcfg_write_namedvalues(SDLGLCFG_NAMEDVALUE_T *vallist);
int  sdlglcfg_write_records(SDLGLCFG_RECORD_T *precinfo, int fixedpos);

#endif  /* _SDLGL_CONFIG_H_ */
