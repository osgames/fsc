/*******************************************************************************
*  PLAYER.C                                                                    *
*	- Definitions of a player in the game game part                            *
*                                                                              *
*   FREE SPACE COLONISATION                                                    *
*       Copyright (C) 2002-2017  Paul Müller <muellerp61@bluewin.ch>           *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*******************************************************************************/

/*******************************************************************************
* INCLUDES                                                                     *
*******************************************************************************/

#include <string.h>         /* strcpy()                 */


#include "fscshare.h"
#include "fsctool.h"
#include "game.h"
#include "msg.h"
#include "rules.h"
#include "tech.h"


/* -- Own header --- */
#include "player.h"

/*******************************************************************************
* DEFINES                                                                      *
*******************************************************************************/

#define PLAYER_POLPARTY_MAX   10

/*******************************************************************************
* TYPEDEFS                                                                     *
*******************************************************************************/

typedef struct S_PLAYER
{
    char valid;         /* Is valid for lad/save operations */
    char alive;
    char ai_control;
    /* --------- Global bonuses in percent (maximum 250 % per bonus) -------- */
    PLAYER_RACE_T race;

    char government;    /* Id of government                                 */
    int  home_pos;      /* Used for start position, influence and so on     */
    short int home_star_no;
    short int home_planet_no;
    /* --------------- Save actual unit for every player ------------------ */
    int  act_unit;      /* The number of the actual chosen unit             */
    /* --------------- Central store for different basic yields ----------- */
    short int store[GVAL_PLANETYIELD_MAX];
    /* ------------ Additional data needed for gameplay ------------------- */
    char ranges[RULES_RANGE_MAX]; /* Range in tiles for the different ranges: */
                                  /* _SHORT, _MIDDLE, _LONG, _INFLUENCE       */
                                  /* _SENSORS ==> _VISION, _EXPLORE           */
    /* --------- Variable, based on improvements ---------------------------- */
    char bonus_pts[GVAL_MAX + 1];
    /* ----------- Technology ------------------------ */
    PLAYER_RESEARCH_T research;
    /* ---------- Senate (not supported yet) ------------------------------ */
    char pol_party;                          /* Basic political party        */
    char senate[PLAYER_POLPARTY_MAX + 1];   /* How many of each party are in senate? */
    char dipl[FSC_MAJORRACES_MAX + 1];      /* Known aliens: Have contact        */
    /* ===== */
    int net_income;
    /* ----------- Additional data needed for the AIs base planning -------- */
    int  prev_gold;
    int  maxbuycost;
    int  est_upkeep;        /* estimated upkeep of buildings in cities */
    int  handicap;		    /* sum of enum handicap_type */
    char skill_level;		/* 0-10 value for save/load/display        */
    char fuzzy;			    /* chance in 1000 to mis-decide            */
    int  corruption;
    int  expand;			/* percentage factor to value new colonies */
    int  science_cost;      /* Cost in bulbs to get new tech, relative
                               to non-AI players (100: Equal cost)     */
    /* ------------ Diplomacy info --------------------------------------- */
    char winning_strategy;
    int  timer; 		        /* pursue our goals with some stubbornness, in turns */
    char love_coeff;            /* Reduce love with this % each turn 	    */
    char love_incr;             /* Modify love with this fixed amount       */
    int  req_love_for[7];       /* ... this kind of 'FSCDIPL_PACT...'       */
    /* --------- Additional info ------------------------------------------ */
    int  ai_handicap;           /* TODO: Flags about Handicap for AI-Player */
    char mng_count;             /* Number of turns between check of colonies */
    char mng_diff;              /* Number of turns between management       */
    char unit_ability_want[15]; /* Want weight [0..100] for given ability   */
}
PLAYER_T;

/*******************************************************************************
* DATA                                                                         *
*******************************************************************************/

/*
@TODO: Add these to bonus if player-bonus-info is asked for
static char Player_BonusType[] =
{
    GVAL_ATTACK,
    GVAL_DEFENSE,
    GVAL_SPEED,
    GVAL_FOOD,
    GVAL_RESEARCH,
    GVAL_INFLUENCE,
    GVAL_ECONOMY,
    GVAL_DIPLOMACY,
    GVAL_SENSORS,
    GVAL_ESPIONAGE,
    0
};
*/

static PLAYER_T Players[FSC_MAJORRACES_MAX + 2] =
{
    /* 0: Invalid: Game-Manager, small players, pirates    */
    { 0, 1, 1 },
    { 1, 1, 0, { "Humans", "Wayward Horizon", 'M', 1,
            {
                1,  0,  0,  /* Weight food, unused, unused */
                3,  2,  0,  /* Share resources (Military, social, stock)    */
                2,  2,  0   /* Weights economy (research, taxes, luxury)    */
            }
         } },
    { 1, 1, 1, { "Metalines", "Zwakstroom", 'M', 1, {  1, 0, 0, 4, 2, 0, 1, 1, 0 } } },
    { 1, 1, 1, { "Silari",    "Mikael",     'M', 1, {  1, 0, 0, 4, 2, 0, 1, 2, 0 } } },
    { 1, 1, 1, { "Dorafians", "Ramesh",    'M', 1, {  2, 0, 0, 2, 1, 0, 2, 2, 0 } } },
    { 1, 1, 1, { "Errikangs", "Phoebus",    'M', 1, {  1, 0, 0, 2, 1, 0, 3, 2, 0 } } },
    { 1, 1, 1, { "Melonians", "Leo Albert", 'M', 1, {  3, 0, 0, 2, 1, 0, 3, 2, 0 } } },
    { 1, 1, 1, { "Tiburians", "Tamasz",     'M', 1, {  2, 0, 0, 1, 1, 0, 4, 1, 0 } } },
    { 1, 1, 1, { "Grobians",  "Penelope",   'M', 1, {  1, 0, 0, 4, 1, 0, 2, 1, 0 } } },
    { 0, 0, 0 }
};

/*******************************************************************************
* CODE                                                                         *
*******************************************************************************/

/*
 * Name:
 *     player_get
 * Description:
 *     Returns a pointer on player with given number. The pointer returned
 *     is always valid, but may point on player 0, which is invalid.
 * Input:
 "      player_no: Number of player to get pointer on for
 */
static PLAYER_T *player_get(int player_no)
{

    if(player_no < 1 && player_no > FSC_MAJORRACES_MAX)
    {
        /* Return pointer on invalid player */
        player_no = 0;
    }

    return &Players[player_no];
}

/*
 * Name:
 *     player_init_diplomacy
 * Description:
 *     Fills in all the values, an AI-Leader needds for its diplomacy decisions
 * Input:
 *     leaderno: Number of leaderno to get pointer on
 */
static void player_init_diplomacy(PLAYER_T *pplayer)
{
    pplayer->winning_strategy = 0;    /* WIN_OPEN; */
    pplayer->timer = 0;
    pplayer->love_coeff = 4;          /* 4%       */
    /*
    pplayer->love_incr = MAX_AI_LOVE * 3 / 100;
    pplayer->req_love_for[FSCDIPL_PACT_PEACE]    = MAX_AI_LOVE / 8;
    pplayer->req_love_for[FSCDIPL_PACT_ALLIANCE] = MAX_AI_LOVE / 4;
    */

    /* Turns between management of colonies */
    pplayer->mng_diff = 10;
}

        /* ===================== TECHNOLOGY FUNCTIONS ========== */

/*
 * Name:
 *     player_tech_researched
 * Description:
 *     Handles all the consequences as a player has researched a new tech
 * Input:
 *     player_no:  Number of player who has researched a new tech
 *     tech_found: Number of tech found
 */
static void player_tech_researched(int player_no, int tech_found)
{
    GAME_EFFECT_T ge;
    MSG_INFO_T msg;
    PLAYER_T *pplayer;


    pplayer = player_get(player_no);

    if(player_no > 0)
    {
        /* ------ Hail the Player that he found a new tech --------- */
        msg_prepare(0, player_no, MSG_TYPE_GAME, &msg);
        msg.num     = GRES_RESEARCHDONE;
        msg.tech_no = pplayer->research.actual;

        msg_send(&msg, 0);
    }

    tech_set_researched(pplayer->research.advancebits, tech_found, &ge);

    /* == @TODO: Add possible effect... */
}


/*
 * Name:
 *     player_count
 * Description:
 *     Returns the name of given player taken from leader.
 * Input:
 *     void
 * Output:
 *     Number of still active players
 */
static int player_count(void)
{
    int i;


    /* player 0 always exists   */
    i = 1;

    while(Players[i].valid)
    {
        i++;
    }

    return i;
}

/* ========================================================================== */
/* ========================= PUBLIC FUNCTIONS =============================== */
/* ========================================================================== */

        /* ===================== INIT FUNCTIONS ======================= */

/*
 * Name:
 *     player_init
 * Description:
 *     Inits the player with the given number.
 * Input:
 *     player_no:   Number of player to initialize
 *     map_pos:     The players 'home' position on map
 *     home_planet: The number of his home planet
 */
void player_init(int player_no, int map_pos, int home_planet)
{
    PLAYER_T *pplayer;
    int i;


    pplayer = player_get(player_no);

    /* -------- Initial values for leader -------------------------- */
    if(!pplayer->ai_control)
    {
        /* Equal values for 'human' players -- No special weight */
        for(i = GVAL_COLSHARE_MAX; i < GVAL_MAX; i++)
        {
            pplayer->race.base_weight[i] = 1;
            pplayer->race.act_weight[i]  = 1;
        }
    }

    /* Initalize diplomacy infos */
    player_init_diplomacy(pplayer);

    /* FIXME: Fill in better initialization code.       */
    pplayer->home_pos = map_pos;
    pplayer->home_star_no = home_planet;

    /* ---------- #RULES -------- */
    /* politicsGetRandomGov(POLITICS_GOV_BIG); */
    pplayer->government  = 1;
    /* Money the player owns  */
    pplayer->net_income = 50;
    /*
    rulesGetBaseRanges(&pplayer->ranges[0]);
    */

    /* TODO: Generate random player name, if not set yet */

    /* Found tech "nil", sets all techs which are for free  */
    player_tech_researched(player_no, 0);

    /* BASIC: Set default research for test purposes */
    #if 0
    tech_get_info(pplayer->research.advancebits, &tech_info);

    pplayer->research.actual   = tech_info.list_reachable[0];
    pplayer->research.invested = 0;
    #endif
}

/*
 * Name:
 *     player_remove
 * Description:
 *     Handles all the consequences as a player is removed from game
 * Input:
 *     player_no: Number of player to remove
 */
void player_remove(int player_no)
{
    Players[player_no].ai_control = -1;
    Players[player_no].alive      = 0;
    Players[player_no].valid      = 0;
}

/*
 * Name:
 *     player_race
 * Description:
 *     Get or set information about the race of this player
 * Input:
 *     player_no: Number of player to get/set the race for
 *     prace *:   Buffer on info to get/set
 *     set:       1: Set it, otherwise return it in this buffer
 */
void player_race(int player_no, PLAYER_RACE_T *prace, int set)
{
    if(set)
    {
        memcpy(&Players[player_no].race, prace, sizeof(PLAYER_RACE_T));
    }
    else
    {
        /* Get it */
        memcpy(prace, &Players[player_no].race, sizeof(PLAYER_RACE_T));
    }
}

/*
 * Name:
 *     player_get_researchinfo
 * Description:
 *     Get the information about this players research
 * Input:
 *     player_no:   Number of player to get/set the race for
 *     presearch *: Where to return the info
 */
void player_get_researchinfo(int player_no, PLAYER_RESEARCH_T *presearch)
{
    memcpy(presearch, &Players[player_no].research, sizeof(PLAYER_RESEARCH_T));
}

/*
 * Name:
 *     player_get_researchinfo
 * Description:
 *     Set the new tech to research
 * Input:
 *     player_no: Number of player to set research for
 *     tech_no:
 *     presearch *: Where to return the info
 */
void player_set_research(int player_no, int tech_no)
{
    Players[player_no].research.actual   = (short int)tech_no;
    Players[player_no].research.invested = 0;
}

/* ========= Functions to change values of player ======= */


/*
 * Name:
 *     player_add_bonus
 * Description:
 *     Adds the given bonus amount for given player
 * Input:
 *     player_no:  To set bonus for
 *     bonus_type: Which bonus to set
 *     amount:     Which amount to add
 */
void player_add_bonus(int player_no, int bonus_type, char amount)
{
    Players[player_no].bonus_pts[bonus_type] += amount;
}


/*
 * Name:
 *     player_set_value
 * Description:
 *     Sets the given value
 * Input:
 *     player_no:  To set value for
 *     value_no:   Which value to set
 *     amount:     Which amount to set
 */
void player_set_value(int player_no, int value_no, int amount)
{
    switch(value_no)
    {
        case 1:
            Players[player_no].home_pos = amount;
            break;

        case 2:
            Players[player_no].home_star_no = (short int)amount;
            break;

        case 3:
            Players[player_no].home_planet_no = (short int)amount;
            break;
    }
}

        /* ===================== INFORMATION FUNCTIONS ========== */


/*
 * Name:
 *     player_get_weights
 * Description:
 *     Returns a pointer on the actual weights for the different GVAL_...
 *     for this player
 * Input:
 *     player_no: Number of player to get name for
 * Output:
 *     Pointer on the actual weigths of the player for the different GVAL_...
 */
const char *player_get_weights(int player_no)
{
    return Players[player_no].race.act_weight;
}

/*
 * Name:
 *     player_get_ranges
 * Description:
 *     Return the numbers for the different ranges for  this player
 * Input:
 *     player_no: Number of player to get ranges for
 *     pranges *: Where to return the radius for the different ranges
 * Output:
 *     Pointer on the actual weigths of the player for the different GVAL_...
 */
void player_get_ranges(int player_no, char *pranges)
{
    int i;


    for(i = 0; i < RULES_RANGE_MAX; i++)
    {
        pranges[i] = Players[player_no].ranges[i];
    }
}

/*
 * Name:
 *     player_get_bonus
 * Description:
 *      Fills the given array with bonus values for this player
 * Input:
 *     player_no: Number of player to get values for
 *     Pointer on the actual weigths of the player for the different GVAL_...
 */
void player_get_bonus(int player_no, char *pbonus_pts)
{
    memcpy(pbonus_pts, Players[player_no].bonus_pts, GVAL_MAX * sizeof(char));
}


/*
 * Name:
 *     player_get_homepos
 * Description:
 *     Returns the position of the 'home-star'
 * Input:
 *     player_no: Number of player to get position
 */
int  player_get_homepos(int player_no)
{
    return Players[player_no].home_pos;
}

/*
 * Name:
 *     player_get_name
 * Description:
 *     Returns the name of given player taken from leader.
 * Input:
 *     player_no: Number of player to get name for
 */
char *player_get_name(int player_no)
{
    return Players[player_no].race.name;
}

/*
 * Name:
 *     playerGetFinancialInfo
 * Description:
 *     Returns the name of given player taken from leader.
 * Input:
 *     player_no:   Number of player to get info for
 *     income *:   Where to return the income data
 *     expenses *: Where to return the expenses data
 */
void playerGetFinancialInfo(char player_no, int *income, int *expenses)
{
    PLAYER_T *pplayer;


    pplayer = player_get(player_no);

    /* ------------ Income ------------ */
    income[PI_IT_TAXES]    = 0;
    income[PI_IT_TRADE]    = 0;
    income[PI_IT_TRIBUTE]  = 0;
    income[PI_IT_TREASURY] = pplayer->net_income;
    income[PI_IT_TOTAL]    = 0;

    /* ------------ Expenses ---------- */
    expenses[PI_EXP_MILITARY]    = 0;
    expenses[PI_EXP_SOCIAL]      = 1;
    expenses[PI_EXP_RESEARCH]    = 2;
    expenses[PI_EXP_MAINTENANCE] = 3;
    expenses[PI_EXP_LEASES]      = 4;
    expenses[PI_EXP_GIA]         = 5;
    expenses[PI_EXP_ESPIONAGE]   = 6;
    expenses[PI_EXP_DESTABILIZE] = 7;
    expenses[PI_EXP_TRIBUTEPAY]  = 8;
    expenses[PI_EXP_TOTAL]       = 9;

}

    /* =================== player FUNCTIONS =================== */

#if 0
/*
 * Name:
 *     playerUnitCreate
 * Description:
 *     Creates a unit of given 'unittype' for given player.
 * Input:
 *     unit_type:  Type of unit to create
 *     ability:    Create with this ability, if unittype id unknown
 *     player_no:  Number of player to create the unit for
 *     pos:        Where to spawn the unit
 *     cargo_load: Cargo to load on created unit
 * Output:
 *     Number of new unit created
 */
 ;
int playerUnitCreate(char unit_type, int ability, char player_no, int pos, char cargo_load)
{

    char avail_list[200];
    PLAYER_T *pplayer;
    int unit_id;


    pplayer = player_get(player_no);

    /* Get the unittype by ability, if needed */
    if(unit_type == 0)
    {
        unittypeGetAvailable(&pplayer->research.advancebits[0], avail_list);

        unit_type = unittypeBestRoleUnit(avail_list, ability);
    }

    /* Create the base */
    unit_id = unitCreate(unit_type, player_no, pos, cargo_load);

    if(unit_id > 0)
    {
        pplayer->act_unit = unit_id;

        /* Add the Players bonuses */
        unitruleSetBonuses(unit_id, pplayer->bonus_pts);
     }

     return unit_id;
}
#endif

/*
 * Name:
 *     playerEndTurnUpdate
 * Description:
 *     Does an update on all the stats for the player. Get's the values
 *     needed from 'values'.
 * Input:
 *     player_no: For this player
 *     pvalues *:  Values from all planets, in array indexed with 'GVAL_*'
 */
void playerEndTurnUpdate(int player_no, int *pvalues)
{

    PLAYER_T *pplayer;
    int i;


    pplayer = player_get(player_no);

    /* Update store values... */
    for(i = 0; i < GVAL_MAX; i++)
    {
        switch(i)
        {
            case GVAL_STOCK:
            case GVAL_TAXES:
            case GVAL_RESEARCH:
            case GVAL_NETINCOME:
                /* Cumulative */
                pplayer->store[i] += pvalues[i];
                break;

            default:
                /* New sum for display */
                pplayer->store[i] = pvalues[i];
                break;

        }

    }   /* Update store values */

    /* FIXME #RULES: if 'pplayer->store[GVAL_RESEARCH]' gets a certain
           value, the player gets a researchable tech for free   */
    pplayer->research.invested    += pplayer->store[GVAL_RESEARCH];
    pplayer->store[GVAL_RESEARCH] = 0;

    if(tech_get_turns(pplayer->research.actual, pplayer->research.invested, 0) == 0)
    {
        /* --- It's up to the player to react correctly on message --- */
        pplayer->research.actual   = 0;   /* No research at all  */
        pplayer->research.invested = 0;   /* Save bulbs ?        */

        player_tech_researched(player_no, pplayer->research.actual);
    }

    /* ----------------------- */
    /* Prevent from division by zero.
      FIXME:  There should be always be at least one colony.
      Otherwise the player is 'dead'
    */
    if(pvalues[GVAL_NUMCOLONY] > 0)
    {
        pplayer->store[GVAL_APPROVAL] /= pvalues[GVAL_NUMCOLONY];
    }

}

   /* ==================== Load and save data of Players ===== */

/*
 * Name:
 *     player_get_loadsave_info
 * Description:
 *     If 'save' is true then fill in struct 'info' with data needed for
 *     saving data given in 'data *'. 'numrec' must hold the maximum number
 *     of records that can be filled, 'recsize' the maximum size of record to
 *     be saved.
 * Input:
 *     pdatadesc *: Where to return the description of the data
 *     save:       Return info for save / load given info into game
 * Output:
 *     Number of data-descriptors returned
 */
int player_get_loadsave_info(FSCFILE_DATADESC_T *pdatadesc, char save)
{
    pdatadesc->rec_info.data_name = FSCFILE_PLAYER;
    pdatadesc->rec_info.rec_size  = sizeof(PLAYER_T);
    pdatadesc->pdata              = &Players[0];

    if(save)
    {
        /* First count Players */
        pdatadesc->rec_info.num_rec = player_count();
    }
    else
    {
        /* Tell loader how much space is available */
        pdatadesc->rec_info.num_rec = (FSC_MAJORRACES_MAX + 2);
    }

    return 1;
}


