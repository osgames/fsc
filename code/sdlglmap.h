/*******************************************************************************
*  SDLGLMAP_INFO.H                                                             *
*      - Functions for handling a sdlglmap (windowed view and so on)           *
*                                                                              *
*  SDLGL - SDLGLMAP_INFO                                                       *
*      (c)2002 - 2017 Paul Mueller <muellerp61@bluewin.ch>                     *
*                                                                              *
*  This program is free software; you can redistribute it and/or modify        *
*  it under the terms of the GNU General Public License as published by        *
*  the Free Software Foundation; either version 2 of the License, or           *
*  (at your option) any later version.                                         *
*                                                                              *
*  This program is distributed in the hope that it will be useful,             *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of              *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
*  GNU Library General Public License for more details.                        *
*                                                                              *
*  You should have received a copy of the GNU General Public License           *
*  along with this program; if not, write to the Free Software                 *
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  *
*******************************************************************************/

/* Last Change: 2008-05-10 / bitnapper */

#ifndef _SDLGL_MAP_H_
#define _SDLGL_MAP_H_

/*******************************************************************************
* DEFINES                                 				                       *
*******************************************************************************/

/* -------- Type of map grid ----------------- */
#define SDLGLMAP_TYPE_STD    0   /* Is a square map                      */
#define SDLGLMAP_TYPE_ISO    1   /* Return the position for an Iso-Map   */
#define SDLGLMAP_TYPE_ISO2   2

/* -------- Movement of window on map -------- */
#define SDLGLMAP_MOVEUP      0   /* Move the window on map up    */
#define SDLGLMAP_MOVERIGHT   1   /* Move the window on map right */
#define SDLGLMAP_MOVEDOWN    2   /* Move the window on map down  */
#define SDLGLMAP_MOVELEFT    3   /* Move the window on map left  */

/* ------- Movement directions --------------- */
#define SDLGLMAP_MOVEDIR_N   0
#define SDLGLMAP_MOVEDIR_NE  1
#define SDLGLMAP_MOVEDIR_E   2
#define SDLGLMAP_MOVEDIR_SE  3
#define SDLGLMAP_MOVEDIR_S   4
#define SDLGLMAP_MOVEDIR_SW  5
#define SDLGLMAP_MOVEDIR_W   6
#define SDLGLMAP_MOVEDIR_NW  7

/* -------- Zoom for map window -------------- */
#define SDLGLMAP_ZOOMIN      5
#define SDLGLMAP_ZOOMOUT     6

/* -------- Flags for map window ------------- */
#define SDLGLMAP_FHORIZWRAP  0x01        /* Wrap view window horizontal  */
#define SDLGLMAP_FVERTWRAP   0x02        /* Wrap view window vertical    */

/* -------- Additional defines --------------- */
#define SDLGLMAP_NUMADJACENT  (8 + 2)
#define SDLGLMAP_NUM_RADIUS   (176 + 4)

/*******************************************************************************
* TYPEDEFS                                 				                       *
*******************************************************************************/

typedef struct
{
    int x, y;
}
SDLGLMAP_XY;

typedef struct
{
    int x, y;   /* Top edge of viewport                    */
    int w, h;   /* Width and height                        */
    int min_w;  /* The minimum width of viewport in tiles  */
    int min_h;  /* The minimum height of viewport in tiles */
}
SDLGLMAP_VIEWPORT_T;

typedef struct
{
    int type;           /* Type of map SDLGLMAP_TYPE_*          */
    int flags;          /* SDLGLMAP_F*                          */

    /* ------------ Map window as in FSCMAP ------------------- */
    int size_w,
        size_h;         /* Size of map in tiles                 */
    int num_tiles;      /* (mapsize) Total size of map in tiles */
    /* -------- Visible window of map (players viewport) ------ */
    SDLGLMAP_VIEWPORT_T vp;
}
SDLGLMAP_INFO_T;

/*******************************************************************************
* CODE                                   				                       *
*******************************************************************************/

/* ======== Viewport functions ================ */
void sdlglmap_viewport_move(SDLGLMAP_INFO_T *pmap, int dir);
void sdlglmap_viewport_adjust(SDLGLMAP_INFO_T *pmap, int pos_x, int pos_y, char center, char in_vp);
void sdlglmap_viewport_zoom(SDLGLMAP_INFO_T *pmap, int zoom);

/* ======== Position functions ================ */
void sdlglmap_get_xy(SDLGLMAP_INFO_T *pmap, int map_pos, SDLGLMAP_XY *pos_xy, char in_vp);
void sdlglmap_get_diffxy(SDLGLMAP_INFO_T *pmap, int src_pos, int dst_pos, SDLGLMAP_XY *pos_xy);
int  sdlglmap_get_dest_pos(SDLGLMAP_INFO_T *pmap, int x, int y, int dir);
int  sdlglmap_get_pos(SDLGLMAP_INFO_T *pmap, int pos_x, int pos_y, char in_viewport);

/* ========== Additional functions ========== */
void sdlglmap_get_adjacent(SDLGLMAP_INFO_T *pmap, int x, int y, int adjacent[8]);
int *sdlglmap_get_radiuslist(SDLGLMAP_INFO_T *pmap, int x, int y, int radius, int *ppos_list);

void sdlglmap_clamp_area(SDLGLMAP_INFO_T *pmap, SDLGLMAP_VIEWPORT_T *parea);

/* ========= Functions for pathes ========== */
int  sdlglmap_get_dist(SDLGLMAP_INFO_T *pmap, int src_pos, int dest_pos);
char sdlglmap_get_heading(SDLGLMAP_INFO_T *pmap, int src_pos, int dest_pos);
char sdlglmap_get_long_heading(SDLGLMAP_INFO_T *pmap, int src_pos, int curr_pos, int dest_pos);

#endif  /* _SDLGL_MAP_H_ */
