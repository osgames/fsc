/*******************************************************************************
*  FSCDIPL.H                                                                   *
*	        - Games part of diplomacy	                               *
*                                                                              *
*   FREE SPACE COLONISATION             				       *				
*       Copyright (C) 2002-2008  Paul Mueller <pmtech@swissonline.ch>          *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*******************************************************************************/

#ifndef _FSC_FSCDIPL_H_
#define _FSC_FSCDIPL_H_

/*******************************************************************************
* DEFINES                                                                      *
*******************************************************************************/

#define FSCDIPL_MAX_LIST 100

/*******************************************************************************
* ENUMS                                                                        *
*******************************************************************************/

/* Diplomatic states (how one player views another).
 * (Some diplomatic states are "pacts" (mutual agreements), others aren't.)
 *
 * Adding to or reordering this array will break many things.
 */
enum {

    FSCDIPL_PACT_NOCONTACT = 0,
    FSCDIPL_PACT_ARMISTICE = 1,
    FSCDIPL_PACT_WAR = 2,
    FSCDIPL_PACT_CEASEFIRE = 3,
    FSCDIPL_PACT_PEACE = 4,
    FSCDIPL_PACT_ALLIANCE = 5,
    FSCDIPL_PACT_TEAM = 6,
    FSCDIPL_PACT_MAX = 8

} FSCDIPL_PACT_TYPE;

enum {

    DIPL_OK,
    DIPL_ERROR,
    DIPL_SENATE_BLOCKING,
    DIPL_ALLIANCE_PROBLEM

} FSCDIPL_REASON_TYPE;

enum {

   FSCDIPL_CLAUSE_NONE    = 0,
   FSCDIPL_CLAUSE_UNIT,
   FSCDIPL_CLAUSE_COLONY,
   FSCDIPL_CLAUSE_TECH,
   FSCDIPL_CLAUSE_PACT,		/* FSCDIPL_PACT_... */
   FSCDIPL_CLAUSE_GOLD,
   FSCDIPL_CLAUSE_MAP,
   FSCDIPL_CLAUSE_VISION,
   FSCDIPL_CLAUSE_EMBASSY

} FSCDIPL_TREATY_TYPE;

/*******************************************************************************
* TYPEDEFS                                                                     *
*******************************************************************************/

typedef struct {
	
    int  id;        /* Needed, if a clause has to be deleted            */
    int  type;		/* What we offer/want: FSCDIPL_CLAUSE_...           */
    char from;      /* Give this  (we: offer, he: we want               */
    char to;        /* To him                                           */
    int  number;	/* if unit/colony: Number, gold: value, pact: type  */
    int  ai_value;	/* Value for AI					                    */
    int  turns;     /* For gold offers / ceasefire                      */
	
} FSCDIPL_CLAUSE;	/* Offer for a treaty				                */

typedef struct {

    char who;
    int num_unit;
    int unit_list[FSCDIPL_MAX_LIST + 2];         /* List of units                   */
    int num_colony;
    int colony_list[FSCDIPL_MAX_LIST + 2];       /* List of colonies                */
    int num_tech;
    int tech_offerlist[FSCDIPL_MAX_LIST + 2];    /* List of techs 'who' can offer   */
    int num_planet;
    int planet_list[FSCDIPL_MAX_LIST + 2];       /* List of planets, colony/outpost */
    int num_pact;
    int pactlist[FSCDIPL_PACT_MAX + 2];         
   		
} FSCDIPL_INFO;	/* Info for diplomacy */

/*******************************************************************************
* CODE                                                                         *
*******************************************************************************/

void fscdiplInitMeeting(FSCDIPL_INFO *info, char me, char you);
void fscdiplAddClause(char from, char to, int clausetype, int number, int turns);
void fscdiplRemoveClause(int id);
void fscdiplAddTreaty(FSCDIPL_CLAUSE *pclause);
void fscdiplCancelTreaty(char me, char other, int type);
FSCDIPL_CLAUSE *fscdiplGetClauses(void);


#endif /* _FSC_FSCDIPL_H_ */
