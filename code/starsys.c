/* *****************************************************************************
*  STARSYS.C                                                                   *
*	- Definitions of a star system in the game, game info            	       *
*                                                                              *
*  FREE SPACE COLONISATION                                                     *
*      (c)2002 - 2017 Paul Mueller <muellerp61@bluewin.ch>                     *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*******************************************************************************/

/* *****************************************************************************
* INCLUDE                                                                      *
*******************************************************************************/

#include <stdio.h>      /* sprintf()    */
#include <string.h>     /* strcpy()     */
#include <ctype.h>      /* isdigit()    */


#include "fsctool.h"


/* --- Own header */
#include "starsys.h"

/* *****************************************************************************
* DEFINES                                                                      *
*******************************************************************************/

#define STAR_ID_INVALID   0

#define STARSYS_MAX_PLANET    1500
#define STARSYS_MAX_OUTPOST    300
#define STARSYS_MAX_PLANETSYS    5

/* *****************************************************************************
* TYPEDEFS                                                                     *
*******************************************************************************/

typedef struct
{
    char type;          /* Number of the stars type 1..6 --> pic	    */
                        /* > 0: Star is valid                           */
    /* ------------ Info about planetary system ----------------------- */
    char name[STAR_NAME_LEN + 1]; /* Name of star			            */
                        /* Used for the name of its planets, if needed  */
    char flags;         /* Short cut for display of special kind        */
                        /* STARSYS_F*                                   */
    /* ------------ Additinal info for display ------------------------ */
    char num_planet;    /* Number of planets of this star               */
    short int first_planet; /* Number of first planet in planet list   */
    short int map_pos;      /* Position of star in map                 */
    /* ----------------------- Info for game -------------------------- */
    char owner;         /* This is the owner of this planetary system   */
    short int  star_no; /* Number in array of stars                     */
}
STAR_T;

typedef struct
{
    char orig_class;        /* Char key_code 'planet class', as string  */
    char type_no;           /* Number of type_no for display           */
    short int orbits_star;  /* Is in orbit of this star             */
    char orbit_no;      /* Number of orbit around this star     */
    char name[PLANET_NAME_LEN + 1];
                        /* = "" Generated name based on orbit   */
                        /* != "" effective User-)name           */
    /* ---------- Basic info for planet ----------------------- */
    char owner;         /* This is the owner of this planet     */
    /* ---------- Basics -------------------------------------- */
    char yield_size;    /* 7 .. 24: Sets the display size       */
    char yield[GVAL_PLANETYIELD_MAX];
                        /* GVAL_PROD_FOOD, RESOURCES, ECONOMY   */
                        /* GVAL_FOOD = 0: Can't be terraformed  */
    char settle_class;  /* E.g. 'M', then it can be setteld     */
                        /* <> 'M': Can be terraformed           */
    /* ---------- Settlement ---------------------------------- */
    char settle_type;   /* Type of settlement:
                            STARSYS_SETTLETYPE_*                */
    short int settle_no;/* > 0: Number of settlement on planet  */
                        /* Ex: 'colony_no'                      */
    char terraform;     /* -1: Impossible to terraform          */
                        /* Otherwise progress of terraforming   */
                        /* Set to 0 as terraforming is done     */
    char special;       /* Special resources. Not used yet      */
}
PLANET_T;

typedef struct
{
    char proj_which;    /* Which thing are they building?           */
	int  proj_invested; /* How much do they have invested in it?    */
    char yield_kind;    /* Kind of yield this outpost generates     */
    char yield;         /* Yield value per round                    */
    char age;           /* Age of outpost                           */
                        /* if > 0: Project is completed and active  */
}
PLANET_OUTPOST_T;

/* Different types of planets */
typedef struct
{
    char key_code;              /* One char as key_code for this planet type        */
    char name[16];          /* Description of this planet type as string    */
    char yield_base[GVAL_PLANETYIELD_MAX];    /* GVAL_FOOD, _RESOURCES, _ECONOMY */
    char yield_rand[GVAL_PLANETYIELD_MAX];    /* GVAL_FOOD, _RESOURCES, _ECONOMY */
                            /* yield_base[0] = 0: Can't be settled          */
                            /* 7 .. 24: Sets the display size               */
                            /* 8..12: small, 13..20: medium, 21..24: large  */
    char type_no[2];           /* Base, number of icons above size             */
}
PLANET_TYPE_T;

/* *****************************************************************************
* DATA                                                                         *
*******************************************************************************/

    /* - Some Star system names - */
static int  NumStarNames = 460;
static char Star_Names[] =
{
    "\0Acamar\0Adelphous\0Alawanir\0Aldea\0Aldebaran\0Aldebaron\0"
    "Alfa Firstos\0Alpha Carinae\0Alpha Centauri\0Alpha Cygnus\0"
    "Alpha Trebolis\0Alpha Majori\0Alpha Moon\0Alpha\0Omicron\0Alpha Onias\0"
    "Alpha Proxima\0Alpha Qutor\0Alshain\0Altair\0Alsham\0Altaria\0"
    "Amargosa\0Andevian\0Angel One\0Angosia\0Antares\0Antica\0Antide\0"
    "Antos\0Antra\0Aolian\0Aquilae\0Archanis\0Arcea\0"
    "Archer\0Arcybite\0Ardana\0Argelius\0Argolis\0Argos\0Ariannus\0"
    "Arkaria\0Armus\0Arneb\0Arvada\0Atalia\0Atlec\0Aucdet\0"
    "Axanar\0Babel\0Bajor\0Balosnee\0Barcon\0Barzan\0Beltane\0"
    "Benecia\0Benev Selec\0Benzar\0Berengaria\0Bersallis\0Beta Agni\0"
    "Beta Aurigae\0Beta Geminorum\0Beta Lankal\0Beta Magellan\0"
    "Beta-Moon\0Beta Niobe\0Beta Portolan\0Beta Renna\0Beta Stromgren\0"
    "Beta Thoridar\0Beta Ulanis\0Beta Xaroni\0Betazed\0Beth Delta\0"
    "Bevora\0Bilana\0Bilaren\0Bolarus\0Boradis\0Boreal\0Boreth\0"
    "Borgolis\0Borka\0Bracas\0Brax\0Bre'el\0Brekka\0Brentalia\0"
    "Bringloid\0Browder\0Brucinis\0Bynaus\0Cetra Elevis\0Camus\0Canopus\0"
    "Camor\0Capella\0Cardassia\0Carema\0Carraya\0Catal\0Catulian\0Celtris\0"
    "Cerebus\0Cetus\0Ceti Alpha\0Chalna\0Chamra\0Chandra\0Cheron\0"
    "Cirrus\0Claurus\0Coltar\0Cor Caroli\0Coridan\0Corinth\0"
    "Cornelian\0Corvan\0Cryos\0Cygnet\0Cynia Minor\0Daled\0Danula\0"
    "Daken\0Daran\0Deinonychus\0Delb\0Delinia\0Delos\0Delta Daniris\0Delta Rana\0"
    "Delta Vega\0Deltived\0Deneb\0Deneva\0Denius\0Denkiri\0Denorius\0"
    "Dephi Ardu\0Deriben\0Dester\0Devidia\0Dimorus\0Donatu\0Doraf\0Durias\0"
    "Drema\0Drengin\0Dropek\0Dulisian\0Durenia\0Dytallix\0Eden\0Ekos\0El Adrel\0"
    "Elas\0Elba\0Emila\0Eminiar\0Endicor\0Ennan\0Epsilon Canaris\0"
    "Epsilon Hydra\0Elysus\0Epsilon Indi\0Epsilon Mynos\0Epsilon Silar\0"
    "Erabus\0Errikang\0Evadne\0Exalbia\0Exo\0Fabrina\0Fahleena\0"
    "Fendaus\0Figontyn\0Fogixsev\0Forlat\0Gagarin\0Galen\0Galor\0"
    "Galorndon Core\0Galvin\0Gamelan\0Gamma Vortis\0Gamma Septus\0"
    "Gamma Arigulon\0Gamma Erandi\0Gamma Eridon\0Gamma Hromi\0Gamma Hydra\0"
    "Gamma Duali\0Gamma Tauri\0Gamma Trianguli\0Garadius\0Garon\0Garth\0Garutha\0"
    "Gaspar\0Gemaris\0Genesis\0Gideon\0Gonal\0Gothos\0Granar\0Gravesworld\0"
    "Guernica\0Halee\0Hanoli\0Hansens Star\0Hamga\0"
    "Harrod\0Harrakis\0Haven\0Hayashi\0Hekara\0Hoel\0Holberg\0"
    "Hromi\0Hurada\0Hurkos\0Iconia\0Icor\0Idini\0Idran\0Ikalian\0"
    "Indri\0Ingraham B\0Sigma Iota\0Icanus\0Janus\0Jaros\0Jeraddo\0"
    "Jouret\0Kaelon\0Kalandan\0Kaldra\0Kataan\0Kavis Alpha\0Kea\0"
    "Kelrabi\0Kenda\0Khitomer\0Klaestron\0Klavdia\0Kora\0Korok\0Korris\0"
    "Kostolain\0Kraus\0Krios\0Kurl\0Lotretis\0Litresepti\0Lambdo Paz\0"
    "Landris\0Lantar\0Lapolis\0Largo\0Larsitus\0Lavinius\0Legara\0"
    "Lemma\0Ligon\0Ligos\0Lima Sierra\0Lonka\0Loren\0Lorenze\0"
    "Lunar\0Lyshan\0Lysia\0Mab-Bu\0Makus\0Malaya\0Malcor\0Malkus\0"
    "Malpa Lor\0Manark\0Manu\0Marcus\0Marejaretus\0Mariah\0"
    "Mariposa\0Marlonia\0Mauna\0Maxia Zeta\0Melina\0Melnos\0Melona\0"
    "Meltasioni\0Memory Alpha\0Mempa\0Merak\0Metaline\0Midos\0Milika\0"
    "Minara\0Minas\0Minos\0Minos Korva\0Mintaka\0Miramanee\0"
    "Minovair\0Miridian\0Mizar\0Moab\0Modean\0Mordan\0Morikin\0Morska\0"
    "Moselian\0Mudor\0Murasaki\0Murinat\0Mutara\0Myrmidon\0Nahmi\0"
    "Narenda\0Narva\0Nasreldine\0Nastor\0Nel Bato\0Nel\0Nelvana\0"
    "Nequencia\0Nervala\0Ngame\0Nimbus\0Norpin\0Ogus\0Ohniaka\0"
    "Omega\0Omega Sagitta\0Omicron Ceti\0Omicron\0Omicron Theta\0Ophicus\0"
    "Ordek\0Orelius\0Organia\0Ornara\0Otar\0Pa Lenor\0Pacifica\0Patroklos\0"
    "Paxan\0Pegos Minor\0"
    "Peliar Zel\0Pelleus\0Pelloris\0Pentarus\0Penthara\0Persephone\0Pheben\0"
    "Phelan\0Piers\0Plationius\0Pollux\0Poseidon\0Prakal\0Praxillus\0Pyris\0"
    "Qo'nos\0Quadra Sigma\0Qualor\0Quazulu\0Rachelis\0Rahm Izad\0Rakhar\0"
    "Ramatis\0Recek\0Regula\0Regulus\0Rekag Seronia\0Relva\0Remus\0"
    "Rigel\0Risa\0Rochani\0Rok\0Romulus\0Rousseau\0Ruah\0Rubicun\0"
    "Rura Penthe\0Rutia\0Sahndara\0Sander\0Sargon\0Sarona\0Sarpeidon\0"
    "Sarthong\0Scalos\0Scothis\0Selay\0Selcundi Drema\0Selebi\0"
    "Selonis\0Sentinel Minor\0Septimus Minor\0Setlik\0Shelia\0"
    "Shiralea\0Siblia\0Sigma Draconis\0Sigma Erandi\0Sigma\0Sirius\0Skowbo\0"
    "Solais\0Solari\0Solarion\0Stakoron\0Straleb\0Strnad\0Styris\0Sumiko\0"
    "Suvin\0T'lli Beta\0Tagra\0Tagus\0Takara\0Talos\0Tanagra\0Tantalus\0"
    "Taruga\0Tarchannen\0Tarella\0Tarod\0Tarsas\0Tarsus\0Tartaras\0"
    "Tau Alpha C\0Tau Ceti\0Tau Cygna\0Taurus Dinor\0Tavela Minor\0"
    "Teleris\0Tellun\0Telsia\0Terok Nor\0Tessen\0Tethys\0Thalos\0Thasus\0Thelka\0"
    "Theta Unobis\0Theta Cygni\0Thindor\0Tiburon\0Titus\0Tohvun\0Toria\0"
    "Torman\0Torona\0Tosk Parr\0Triadus\0Triona\0Triskelion\0Troyius\0Turcana\0"
    "Tycho\0Tyken\0Typhon\0Tyran\0Tyrellia\0Ultima Thule\0Vadris\0"
    "Vagra\0Valkyrie\0Valo\0Valt Minor\0Vandor\0Vaytan\0Vayanti\0Vega\0Velara\0"
    "Vendikar\0Ventax\0Verath\0Vilmor\0Volchok\0Voltarr\0Volterra\0Weber\0"
    "Westerfeld\0Xanthras\0Xasica\0Xendi Sabu\0Xerxes\0Yaris\0Yggdrasil\0Yor\0"
    "Zadar\0Zakdorn\0Zalkon\0Zayra\0Zed Lapis\0"
    "Zeon\0Zeta Alpha\0Zeta Gelis\0Zetar\0Zychtin\0\0"
};

static STAR_T   Stars[STARSYS_MAX_STAR + 3];
static PLANET_T Planets[STARSYS_MAX_PLANET + 2];
static PLANET_OUTPOST_T Outposts[STARSYS_MAX_OUTPOST + 2];


/* The different planet types. Basic for size 3 (yield per size: 1 / size) */
static PLANET_TYPE_T Planet_Types[] =
{
    { 0, "Invalid" },  /* 0 is always invalid if possible           */
    { 'Y',          /* One char as key_code for this planet type        */
      "Vulcanic",   /* Description of this planet type as string    */
                    /* E. g. 'Mercury' and 'Venus'                  */
      /* -- Yield bases and ranges: _FOOD, _RESOURCES, _ECONOMY --- */
      { 16, 8,  4 },
      {  3, 2, 14 },
      { 1 }         /* Single type_no (3 sizes) */
    },
    { 'M', "Earthlike",
      { 22, 18, 25 },
      {  5,  4,  4 },
      { 2 }        /* Single type_no (3 sizes) */
    },
    { 'G', "Desert", /* E. g. Mars  */
      { 12,  9,  8 },
      {  4, 16, 24 },
      { 3 }       /* Single type_no (3 sizes) */
    },
    { 'L', "Jungle",
      { 18, 16, 12 },
      {  4,  8, 22 },
      { 4 }        /* Single type_no (3 sizes) */
    },
    { 'P', "Polar",
      { 15, 18, 10 },
      {  3,  8, 15 },
      { 5 }        /* Single type_no (3 sizes) */
    },
    { 'J', "Sterile",
      { 11, 21,  9 },
      {  2,  5,  4 },
      { 6 }        /* Single type_no (3 sizes) */
    },
    { 'B', "Gas Giant",
      { 0, 12, 0 }, /* Terraforming is 'never' possible */
      { 0,  4, 0 },
      { 11, 6 }     /* type_no_count - 1: Range            */
    },
    { 0 },
    { 0 }
};

/* *****************************************************************************
* CODE                                                                         *
*******************************************************************************/

/*
 * Name:
 *     starsys_get_star
 * Description:
 *     Gets the pointer on a STAR_T. If the 'star_no' is given,
 *     the star with this number is returned, else the one at
 *     given 'pos'
 * Input:
 *     star_no: Number of STAR_T to get pointer for
 *     pos:
 * Output:
 *     Pointer on star. Not found: an 'empty' star
 */
static STAR_T *starsys_get_star(int star_no, int pos)
{
    if(star_no > 0 && star_no < STARSYS_MAX_STAR)
    {
        return &Stars[star_no];
    }
    else
    {
        /* Find the number of the star by position */
        star_no = 1;

        while(Stars[star_no].type > 0)
        {
            if(Stars[star_no].map_pos == pos)
            {
                return &Stars[star_no];
            }

            star_no++;
        }
    }

    return &Stars[0];
}

/*
 * Name:
 *     starsys_get_planet
 * Description:
 *     Returns a pointer on the planet with the given number
 *     If no planet is available, it returns a pointer on planet 0, which
 *     is invalid.
 * Input:
 *      planet_no: Number of planet to return pointer for
 */
static PLANET_T *starsys_get_planet(int planet_no)
{

    if((planet_no < 1) || (planet_no > STARSYS_MAX_PLANET))
    {
        /* Is invalid -- Catch possible errors */
        planet_no = 0;
        Planets[0].orig_class = 0;
    }

    return &Planets[planet_no];
}

/*
 * Name:
 *     starsys_get_randomname
 * Description:
 *     Returns a random name from internal list
 *     @TODO:
 *       - Read in Names from File
 *       - Preserve usage of already used name
 * Input:
 *     pdest *: Where to return the name
 */
static void starsys_get_randomname(char *pdest)
{
    char *pname;


    pname = fsctool_str_fromlist(&Star_Names[0], fsctool_rand_no(NumStarNames));
    strcpy(pdest, pname);
}

/*
 * Name:
 *     starsys_get_planettype
 * Description:
 *     Returns a pointer on PLANET_TYPE_T, based on planet type key_code
 * Input:
 *     find_class:  Return data for this key_code 0: return non-habitable-randomtype
 *     habit_class: Signature for class of a habitable planet
 */
static PLANET_TYPE_T *starsys_get_planettype(char find_class, char habit_class)
{
    PLANET_TYPE_T *ppt;
    int pt_list[100];
    int count, num;


    ppt = &Planet_Types[1];

    if(find_class > 0)
    {
        /* Find planet type of given class */
        while(ppt->key_code)
        {
            if(ppt->key_code == find_class)
            {
                return ppt;
            }
            ppt++;
        }
    }
    else
    {
        count = 0;
        num   = 1;

        while(Planet_Types[num].key_code)
        {
            if(Planet_Types[num].key_code != habit_class)
            {
                pt_list[count] = num;
                count++;
            }

            num++;
        }

        pt_list[count] = 0;

        return &Planet_Types[pt_list[fsctool_rand_no(count) - 1]];
    }

    /* Default */
    return &Planet_Types[1];
}

/*
 * Name:
 *     starsys_new_planets
 * Description:
 *     Returns a preinitialised buffer of PLANET_T's, if enough are left
 * Input:
 *     num_planet *: Number of planets to reserve in buffer.
 * Output:
 *     Number of first planet in PLANET_T-Vector
 */
static int starsys_new_planets(int *num_planet)
{
    int planet_no;
    int planets_left;


    for(planet_no = 1; planet_no < STARSYS_MAX_PLANET; planet_no++)
    {
        if(Planets[planet_no].orig_class == 0)
        {

            planets_left = STARSYS_MAX_PLANET - planet_no - 2;

            if(planets_left < (*num_planet))
            {
                (*num_planet) = planets_left;
            }

            /* We found an empty slots */
            memset(&Planets[planet_no], 0, (*num_planet) * sizeof(PLANET_T));

            return planet_no;
        }
    }

    return 0;
}


/*
 * Name:
 *     starsys_create_planetname
 * Description:
 *     Fills the given string buffer with the given name, added the pplanet
 *     number as roman number. Numbers supported are 1 .. 8
 * Input:
*     pdest *: Pointer on buffer to fill with planet name
 *    pname *: Name of star system (base for planet name)
 *    num:     Number of planets orbit (number of planet name)
 */
static void starsys_create_planetname(char *pdest, char *pname, int num)
{
    int diff;
    char romannum[12], *pnum;


    pnum = &romannum[0];

    if(num < 4)
    {
        while(num > 0)
        {
            *pnum = 'I';
            pnum++;
            num--;
        }
    }
    else if(num < 9)
    {
        diff = num - 5;
        while(diff < 0)
        {
            /* Leading numbers before "V"   */
            *pnum = 'I';
            pnum++;
            diff++;
        }

        *pnum = 'V';
        pnum++;

        while(diff > 0)
        {
            *pnum = 'I';
            pnum++;
            diff--;
        }
    }

    *pnum = 0;      /* Set end of string */

    /* FIXME: Don't allow buffer overflow in target */
    sprintf(pdest, "%s %s", pname, romannum);
}

/*
 * Name:
 *     starsys_create_planetdef
 * Description:
 *     Creates random PLANET_INITDEFS depending on 'habitable' and 'start_pos'
 * Input:
 *     ppid *:       Pointer on buffer to fill with data
 *     habit_class:  Generate at least one habitable planet (this class)
 *     is_start:     Is a start star system for a player, set at least one
 *                   planet of this settle class and don't do random adjustement
 */
static void starsys_create_planetdef(PLANET_INITDEF_T *ppid, char habit_class, int is_start)
{
    PLANET_TYPE_T *ppt;
    int orbit_no, habit_orbit;
    int num_planets, i;


    /* Number of planets */
    num_planets = fsctool_rand_no(STARSYS_MAX_PLANETSYS);

    /* Should at least one of the be planets habitable at all ? */
    if(habit_class != 0 || is_start)
    {
        /* Position of habitable planet */
        habit_orbit = fsctool_rand_no(num_planets);

        if(habit_orbit > 3)
        {
            habit_orbit = 3;
        }
    }
    else
    {
        habit_orbit = 0;
    }

    /* First find the habitable orbit */
    for(orbit_no = 1; orbit_no <= num_planets; orbit_no++)
    {
        if(orbit_no == habit_orbit)
        {
            /* Create habitable planet */
            ppt = starsys_get_planettype(habit_class, habit_class);

            if(is_start)
            {
                /* #RULES: Minimum start size */
                ppid->yield_size = 17;
            }
        }
        else
        {
            /* Create a random type, non habitable, planet */
            ppt = starsys_get_planettype(0, habit_class);
        }

        ppid->key_code = ppt->key_code;
        ppid->type_no  = ppt->type_no[0];

        /* Fill in basic yield */
        for(i = 0; i < 3; i ++)
        {
            ppid->yield[i] = ppt->yield_base[i];
        }

        if(!is_start)
        {
            /* 7 .. 17 */
            ppid->yield_size = 6 + fsctool_rand_no(18);

            /* Add some randomness to yield */
            for(i = 0; i < 3; i ++)
            {
                ppid->yield[i] += (char)(fsctool_rand_no(ppt->yield_rand[i]) - 1);
            }
        }

        /* ------- Next definition --- */
        ppid++;
    }

    /*End of list  */
    ppid->key_code = 0;
}

/*
 * Name:
 *     starsys_add_planets
 * Description:
 *     Create a system which is defined by given 'PLANET_INITDEF_T' and attach it to given star
 * Input:
 *     pstar *:    Pointer on star to attach the planets to
 *     pid *:      Pointer on PLANET_INITDEF_T
 *     start_size: > 0: Set this yield size fix for start
 * Last change:
 *    2017-07-15 <bitnapper>
 */
static void starsys_add_planets(STAR_T *pstar, PLANET_INITDEF_T *ppid, int start_size)
{
    PLANET_T *pplanet;
    PLANET_INITDEF_T *ppid_cnt;
    int planet_no;
    int orbit_no;
    int num_planet, i;


    /* Count the number of planets in pid */
    num_planet = 0;
    ppid_cnt = ppid;

    while(ppid_cnt->key_code > 0)
    {
        num_planet++;
        ppid_cnt++;
    }

    /* Array of Planets to fill -- Returns number of first planet in list */
    planet_no = starsys_new_planets(&num_planet);

    if(planet_no > 0)
    {
        /* Fill info about planets into star */
        pstar->first_planet = planet_no;          /* First planet in vector */
        pstar->num_planet   = (char)num_planet;   /* Save number of planets */

        pplanet = starsys_get_planet(planet_no);  /* Point on first planet  */

        for(orbit_no = 1; orbit_no <= num_planet; orbit_no++)
        {
            memset(pplanet, 0, sizeof(PLANET_T));

            if(ppid->key_code > 0)
            {
                pplanet->orig_class  = ppid->key_code;
                pplanet->type_no        = ppid->type_no;
                pplanet->orbits_star = pstar->star_no;
                pplanet->orbit_no    = (char)orbit_no;
                /* === Generate a name, if none is given === */
                if(ppid->name[0] == 0)
                {
                    /* Create a name using the stars name */
                    starsys_create_planetname(pplanet->name,
                                              pstar->name,
                                              orbit_no);
                }

                pplanet->owner = pstar->owner;

                if(!start_size)
                {
                    /* Create a random size 7..24 */
                    start_size = fsctool_rand_no(18) + 6;
                }

                pplanet->yield_size = start_size;

                /* Now set the yield, based on the planet type */
                for(i = 0; i < GVAL_PLANETYIELD_MAX; i++)
                {
                    pplanet->yield[i] = ppid->yield[i];
                }
                pplanet->settle_class = ppid->key_code;
                pplanet->terraform    = 0;

                /* Special case for gas giants */
                if(ppid->yield[0] == 0)
                {
                    /* Planet can't be terraformed */
                    pplanet->terraform = -1;
                }

                /* @TODO: Add randomness for 'special'. Create special resources */

                pplanet++;
                ppid++;
            }
        }
    }
}

/* ========================================================================== */
/* ======================== PUBLIC FUNCTIONS	============================= */
/* ========================================================================== */

/*
 * Name:
 *     starsys_init
 * Description:
 *     Clear the data for stars and planets
 * Input:
 *     None
 */
void starsys_init(void)
{
    memset(&Stars[0], 0, sizeof(STAR_T) * (STARSYS_MAX_STAR + 10));
    memset(&Planets[0], 0, sizeof(PLANET_T) * (STARSYS_MAX_PLANET));
}

/*
 * Name:
 *     starsys_create_one
 * Description:
 *     Creates a single star
 * Input:
 *     map_pos:      At this position on map
 *     habit_chance: Chance in percent to have at least one habitable planet
 *     type:         1: Random for Star number 0 (and Planets 0..5)
 *                   2: Random star for 'start_pos' (Allways yellow... :D)
 *                   3: Random star with planets
 *                   4: Random star without planets
 *     ppre_def *:   Not NULL, use this definitions to create the planets
 *                   First
 * Output:
 *    None
 */
int starsys_create_one(short int map_pos, int habit_chance, int type, PLANET_INITDEF_T *ppre_def)
{
    /* For random definitions */
    PLANET_INITDEF_T pdef[10];
    STAR_T *pstar;
    int star_no;
    char is_start, habit_class, start_size;


    /* === First create the star === */
    star_no = 1;

    /* Find the first free slot */
    for(star_no = 1; star_no < STARSYS_MAX_STAR; star_no++)
    {
        if(Stars[star_no].type == 0)
        {
            /* We found a free slot */
            break;
        }
    }

    if(star_no >= STARSYS_MAX_STAR)
    {
        /* Failed */
        return 0;
    }

    pstar = &Stars[star_no];


    /* Set its position and its owner, and itss own number for internal use */
    pstar->owner   = 0;
    pstar->map_pos = map_pos;
    pstar->star_no = star_no;

    if(type == 2)
    {
        /* Start positions have always a habitable planet */
        pstar->type  = STARSYS_YELLOWSTAR;
        pstar->flags = STARSYS_FHABITABLE;
        is_start     = 1;
        habit_class  = 'M';
        /* #RULES: This planet size for start systems */
        start_size   = 17;
    }
    else
    {
        /* Get a random star type for other star systems */
        pstar->type  = fsctool_rand_no(6);
        /* Random habitable */
        pstar->flags = fsctool_haschance_percent(habit_chance) ? STARSYS_FHABITABLE : 0;
        /* Get a random name */
        starsys_get_randomname(pstar->name);
        /* Settle class by chance */
        is_start     = 0;
        habit_class  = (pstar->flags & STARSYS_FHABITABLE) ? 'M' : 0;
        start_size   = 0;
    }

    /* === Second: Add planets, if needed === */
    if(type != 4)
    {
        /* Add planets */
        if(ppre_def)
        {
            /* Use planet definitons to create planets */
            /* Maybe there is info to set the star type, and to set the name */
            if(isdigit(ppre_def->key_code))
            {
                /* @TODO: Set predfined star type */
                /* Set predifined star name */
                strcpy(pstar->name, ppre_def->name);

                ppre_def++;
            }
        }
        else
        {
            starsys_create_planetdef(&pdef[0], habit_class, is_start);
        }

        starsys_add_planets(pstar, &pdef[0], start_size);
    }

    return star_no;
}

/* =========== STARS ========= */

/*
 * Name:
 *     starsys_get_starpos
 * Description:
 *     Returns the position of star with given number.
 *     Used for putting saved stars onto the map
 * Input:
 *     star_no: Get position for this star
 * Output:
 *     > 0: Position of star with given number
 */
int starsys_get_starpos(int star_no)
{
    if(Stars[star_no].type > 0)
    {
        return Stars[star_no].map_pos;
    }

    return 0;
}

/*
 * Name:
 *     starsys_get_starinfo
 * Description:
 *     Returns a list of the positions of all stars in map, as long as their
 *     number is less 'list_size'.
 *     The list is filtered based on 'which'.
 * Input:
 *     pos:         For this position
 *     star_type *: Return here the star type
 *     pstarname *: Return here the star name
 *     planets *:   List of planet numbers
 *     full_info:   Return info about planets and starname
 * Output:
 *    Owner of the star, if valid
 */

char starsys_get_starinfo(int pos, char *star_type, char *pstarname, int *planet, char full_info)
{
    STAR_T *pstar;
    int i;


    pstar = starsys_get_star(-1, pos);

    /* Return the color of the star */
    *star_type = pstar->type;
    /* Assume empty name */
    *pstarname = 0;

    /* Now write the name into the arguments, if asked for */
    if(full_info)
    {
        strcpy(pstarname, pstar->name);

        for(i = 0; i < pstar->num_planet; i++)
        {
            *planet = pstar->first_planet + i;

            planet++;
        }
    }

    /* Sign end of array */
    *planet = 0;

    return pstar->owner;
}

/*
 * Name:
 *     starsys_get_star_poslist
 * Description:
 *     Returns a list of the positions of all stars in map, as long as their
 *     number is less 'list_size'.
 *     The list is filtered based on 'which'.
 * Input:
 *     player_no:  For this playrt, if 'which' is set to _OWNED / _UNOWNED
 *     pos_list *: Where to return the positions
 *     list_size:  Size of given list
 *     which:      Which kind of list STAR_POSLIST_*
 * Output:
 *     Number of star-positions in list
 */
int starsys_get_star_poslist(char player_no, int *pos_list, int list_size, int which)
{
    int star_no;
    int count;
    int found_pos;


    star_no = 1;
    count   = 0;

    while(Stars[star_no].type > 0)
    {
        found_pos = -1;

        switch(which)
        {
            case STAR_POSLIST_ALL:
                found_pos = Stars[star_no].map_pos;
                break;

            case STAR_POSLIST_OWNED:
                if(Stars[star_no].owner == player_no)
                {
                    found_pos = Stars[star_no].map_pos;
                }
                break;

            case STAR_POSLIST_UNOWNED:
                if(Stars[star_no].owner != player_no)
                {
                    found_pos = Stars[star_no].map_pos;
                }
                break;
        }

        if(found_pos >= 0)
        {
            *pos_list = found_pos;
            pos_list++;
            count++;
            list_size--;

            if(list_size <= 0)
            {
                *pos_list = 0;

                return count;
            }
        }

        star_no++;
    }

    *pos_list = 0;

    return count;
}

/*
 * Name:
 *     starsys_get_name
 * Description:
 *     Get a name, based on which
 * Input:
 *     which:   0: Star, 1: Planet
 *     number:  Of star/planet
 *     pname *: Where to return the name
 */
void starsys_get_name(char which, int number, char *pname)
{
    if(which == 0)
    {
        strcpy(pname, Stars[number].name);
    }
    else if(which == 1)
    {
        strcpy(pname, Planets[number].name);
    }
    else
    {
        /* Return an empty string */
        *pname = 0;
    }
}

/* =========== PLANETS ========= */

/*
 * Name:
 *     starsys_get_planetinfo
 * Description:
 *     Returns all the info needed to check if a planet is usable for
 * Input:
 *     planet_no: Get info for this planet
 *     pinfo *:   Pointer on manage info to fill
 * Output:
 *     @TODO: Terraforming has so much procent left...
 */
int starsys_get_planetinfo(int planet_no, PLANET_INFO_T *pinfo)
{
    PLANET_T *pplanet;
    int i;


    pplanet = starsys_get_planet(planet_no);

    if(pplanet->orig_class != 0)
    {
        pinfo->owner_no     = pplanet->owner;
        pinfo->settle_class = pplanet->settle_class;

        /* -- It's a valid planet -- */
        /* If the caller wants the name */
        if(pplanet->name[0] == 0)
        {
            starsys_create_planetname(pplanet->name,
                                      Stars[pplanet->orbits_star].name,
                                      pplanet->orbit_no);
        }

        strcpy(pinfo->name, pplanet->name);

        pinfo->yield_size = pplanet->yield_size;

        for(i = 0; i < GVAL_PLANETYIELD_MAX; i++)
        {
            pinfo->yield[i] = pplanet->yield[i];
        }

        if(pplanet->terraform > 0)
        {
            pinfo->terraform_perc = pinfo->terraform_perc * pplanet->terraform / pplanet->yield_size;
        }

        pinfo->is_settled = pplanet->settle_type;

        return starsys_get_starpos(pplanet->orbits_star);
    }

    return 0;
}

/*
 * Name:
 *     starsys_set_planetupgrade
 * Description:
 *     Does an upgrade on the YIELD-values of given planet
 *     #RULES: Example: Soil enhancement 'GVAL_PLNTQUALITY'
 * Input:
 *     planet_no:   Number uf planet to make upgrade
 *     which:       Which yield to make upgrade to
 *     amount_pts: Upgrade in points
 */
void starsys_set_planetupgrade(int planet_no, int which, char amount_pts)
{
    PLANET_T *pplanet;


    if(which < GVAL_PLANETYIELD_MAX)
    {
        /* For safety */
        pplanet = starsys_get_planet(planet_no);

        /* Clamp the amount */
        amount_pts &= 0x7F;
        /* Add the amount */
        pplanet->yield[which] += amount_pts;
    }
}

    /* =========== Basic star handling functions ========= */

/*
 * Name:
 *     starsys_get_bestplanet
 * Description:
 *     Returns the number of the best planet in array from 'first_planet'
 *     counting 'num_planet'.
 * Input:
 *     map_pos:      At which position on map
 *     settle_class: This class can be settled
 *     weight:       Additional weight for the different yields
 *     terraform *:  For returning of points needed for terraforming, if any
 * Output:
 *     Number of planet.
 */
int starsys_bestplanet(int map_pos, char settle_class, char *pweight, char *terraform)
{
    STAR_T *pstar;
    PLANET_T *pplanet;
    char i, planet_no;
    int  best_ntf, best_tf;     /* Best of 'non_terraforming and of 'terraforming' */
    int  max_val, calc_val, j;
    char tf_cost;


    pstar = starsys_get_star(-1, map_pos);

    best_ntf = 0;
    best_tf  = 0;

    if(pstar->type > 0)
    {
        max_val  = 0;
        /* Cost for terraforming */
        tf_cost    = 70;
        *terraform = 80;

        for(i = 0; i < pstar->num_planet; i++)
        {
            planet_no = pstar->first_planet + i;
            pplanet   = starsys_get_planet(planet_no);

            calc_val = 0;

            for(j = 0; j < GVAL_PLANETYIELD_MAX; j++)
            {
                calc_val += (int)pplanet->yield_size * pplanet->yield[j] * pweight[j];
            }

             /* First: Look for planets type 'M' (no terraforming needed  */
            if(pplanet->settle_class == settle_class)
            {
                if(calc_val > max_val)
                {
                    best_ntf = planet_no;
                    max_val  = calc_val;
                }
            }
            else
            {
                /* Size of planet gives the time needed for terraforming */
                tf_cost = pplanet->yield_size;

                if(tf_cost < *terraform)
                {
                    best_tf = planet_no;
                    *terraform = (char)(tf_cost);
                }
            }
        }
    }

    if(best_ntf > 0)
    {
        return best_ntf;
    }

    /* Best of which can be terraformed */
    return best_tf;
}

    /* ============= Planet handling functions ============= */

/*
 * Name:
 *     starsys_get_planets_settled
 * Description:
 *     Fills in the list with number of given owners settled planets and
 *     fills in the number of the planet.
 * Input:
 *     owner_no: Get list for this owner. If -1, then get all
 *     plist *:  Pointer on list to fill with planet numbers
 * Output:
 *     Number of elements in list
 */
int starsys_get_planets_settled(char owner_no, int *plist)
{
    int num_element;
    int planet_no;


    num_element = 0;
    planet_no   = 1;

    while(Planets[planet_no].orig_class != 0)
    {
        if(Planets[planet_no].owner == owner_no || owner_no == -1)
        {
            if(Planets[planet_no].settle_no > 0)
            {
                *plist = planet_no;
                plist++;
                num_element++;
            }
        }

        planet_no++;
    }

    return num_element;
}

/*
 * Name:
 *     starsys_planet_doterraform
 * Description:
 *     Does terraforming on given planet, using given buildpoints
 * Input:
 *     planet_no:    Number of planet to do terraform on
 *     settle_class: Target class to terraform to
 *     build_points: Number of points to add to actual terraform-value
  * Output:
 *     Position of planet on map, if terraforming is completed
 */
int starsys_planet_doterraform(int planet_no, char settle_class, int build_points)
{
    PLANET_T *pplanet;


    pplanet = starsys_get_planet(planet_no);

    pplanet->terraform += (char)build_points;

    if(pplanet->terraform >= pplanet->yield_size)
    {
        pplanet->settle_class = settle_class;

        return starsys_get_star(pplanet->orbits_star, 0)->map_pos;
    }

    return 0;
}

/*
 * Name:
 *     starsys_settle_planet
 * Description:
 *     Adds a colony or a starbase on this planet
 * Input:
 *     planet_no:    Number of planet to do terraform on
 *     player_no:   Needed for adding OUTPOST_T
 *     settle_type: Target class to terraform to
 *     build_points: Number of points to add to actual terraform-value
  * Output:
 *     Position of planet on map
 */
int starsys_settle_planet(int planet_no, char player_no, char settle_type, int settle_no, char *workers_yield)
{
    PLANET_T *pplanet;


    pplanet = starsys_get_planet(planet_no);

    pplanet->settle_type = settle_type;
    pplanet->settle_no   = settle_no;
    pplanet->owner       = player_no;

    if(settle_type == STARSYS_SETTLETYPE_COLONY)
    {
        /* Return the number of workers for each yield */
        if(pplanet->yield_size > 19)
        {
            *workers_yield = 12;
        }
        else if(pplanet->yield_size > 10)
        {
            *workers_yield = 8;
        }
        else
        {
            *workers_yield = 4;
        }
    }
    else if(settle_type == STARSYS_SETTLETYPE_OUTPOST)
    {
        /* @TODO: IF 'settle_type' == STARSYS_SETTLETYPE_OUTPOST, add aone for this player */
        /* starsys_create_outpost(planet_no, player_no, settle_no / * Type of outpost * /) */
    }

    return starsys_get_star(pplanet->orbits_star, -1)->map_pos;
}

/*
 * Name:
 *     starsys_build_outpost
 * Description:
 *     Builds an outpost on given planet. If there's none started, create it for building.
 * Input:
 *     player_no:    Needed for adding OUTPOST_T
 *     planet_no:    Number of planet to do terraform on
 *     proj_which:   Which kind of outpost to build
 *     build_points: Number of points to add to actual terraform-value
  * Output:
 *     Build completed yes/no: < 0: Building failed
 */
int starsys_build_outpost(int player_no, int planet_no, char proj_which, int build_points)
{
    PLANET_T *pplanet;
    int i;


    pplanet = starsys_get_planet(planet_no);


    if(pplanet->owner > 0 && pplanet->owner != (char)player_no)
    {
        return 0;
    }

    if(pplanet->settle_type == STARSYS_SETTLETYPE_OUTPOST)
    {
        if(Outposts[pplanet->settle_no].proj_invested < 10)
        {
            Outposts[pplanet->settle_no].proj_invested += build_points;
        }

        if(Outposts[pplanet->settle_no].proj_invested >= 10)
        {
            /* Outpost is completed */
            return 1;
        }
    }
    else
    {
        for(i = 1; i < STARSYS_MAX_OUTPOST; i++)
        {
            if(Outposts[i].proj_which == 0)
            {
                /* -- We found an empty slot -- */
                Outposts[i].proj_which    = proj_which;
                Outposts[i].proj_invested = build_points;
                Outposts[i].yield_kind = 0;
                Outposts[i].yield = 0;
                Outposts[i].age = 0;


                pplanet->settle_type = STARSYS_SETTLETYPE_OUTPOST;
                pplanet->settle_no   = i;
                pplanet->owner       = player_no;

                return 1;
            }
        }
    }

    return 0;
}

/*
 * Name:
 *     starsys_explore
 * Description:
 *     Explores a STAR_T, if not already done yet:
 *     Generates the stars name and attaches random planets
 * Input:
 *     pos:     Position to explore
 *     star_no: Number of star
 */
void starsys_explore(int pos, int star_no)
{
    PLANET_INITDEF_T pdef[STARSYS_MAXPLANETSYST + 3];
    STAR_T *pstar;
    char habit_class;


    pstar = starsys_get_star(star_no, -1);

    if(pstar->type > 0)
    {
        if(pstar->num_planet == 0)
        {
            /* There's a STAR_T at this position and it has no planets    */
            habit_class = (pstar->flags & STARSYS_FHABITABLE) ? 'M' : 0;

            /* Generate the definitions for the planets                 */
            starsys_create_planetdef(&pdef[0], habit_class, 0);

            /* ------- And create the planets ------- */
            starsys_add_planets(pstar, &pdef[0], 0);
        }
    }
}

    /* ============== Load and save data for planets and stars============ */

/*
 * Name:
 *     starsys_get_loadsave_info
 * Description:
 *     If 'save' is true then fill in struct 'info' with data needed for
 *     saving data given in 'data *'. 'numrec' must hold the maximum number
 *     of records that can be filled, 'recsize' the maximum size of record to
 *     be saved.
 * Input:
 *     pdatadesc *: Pointer on struct where to return the info about the data to laad/save
 *     save:      Return info for save / load given info into game
 * Output:
 *    Number of daa descriptions in 'pdatadesc'
 */
int starsys_get_loadsave_info(FSCFILE_DATADESC_T *pdatadesc, char save)
{
    short int num_rec;



    /* == Info about the Stars == */
    pdatadesc->rec_info.data_name = FSCFILE_STAR;
    pdatadesc->rec_info.rec_size  = sizeof(STAR_T);
    if(save)
    {
        num_rec = 1;
        while(Stars[num_rec].type > 0)
        {
            num_rec++;
        }
        num_rec--;
    }
    else
    {
        num_rec = STARSYS_MAX_STAR;
    }
    pdatadesc->rec_info.num_rec = num_rec;

    /* == Info about the Planets == */
    pdatadesc->rec_info.data_name = FSCFILE_PLANET;
    pdatadesc->rec_info.rec_size  = sizeof(PLANET_T);
    if(save)
    {
        num_rec = 1;
        while(Planets[num_rec].orbits_star > 0)
        {
            num_rec++;
        }
        num_rec--;
    }
    else
    {
        num_rec = STARSYS_MAX_PLANET;
    }
    pdatadesc->rec_info.num_rec   = num_rec;
    pdatadesc->pdata = &Planets[1];
    pdatadesc++;


    /* == @TODO: Moons == */
    /*
    FSCFILE_MOON    = 12,
    */
    /* == Info about outposts == */
    pdatadesc->rec_info.data_name = FSCFILE_OUTPOST;
    pdatadesc->rec_info.rec_size  = sizeof(PLANET_OUTPOST_T);
    if(save)
    {
        num_rec = 1;
        while(Outposts[num_rec].proj_which > 0)
        {
            num_rec++;
        }
        num_rec--;
    }
    else
    {
        num_rec = STARSYS_MAX_OUTPOST;
    }
    pdatadesc->rec_info.num_rec   = num_rec;
    pdatadesc->pdata = &Outposts[1];
    pdatadesc++;

    return 3;
}
