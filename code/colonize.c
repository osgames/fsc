/*******************************************************************************
*  COLONIZE.C                                                                  *
*      - Functions and data for handling colonization 					       *
*                                                                              *
*  FREE SPACE COLONISATION                                                     *
*      (c)2004 Paul Mueller <pmtech@swissonline.ch>                            *
*                                                                              *
*  File by:                                                                    *
*      Brian Webb <bwebb@kasmir.org>                                           *
*                                                                              *
*  This program is free software; you can redistribute it and/or modify        *
*  it under the terms of the GNU General Public License as published by        *
*  the Free Software Foundation; either version 2 of the License, or           *
*  (at your option) any later version.                                         *
*                                                                              *
*  This program is distributed in the hope that it will be useful,             *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of              *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
*  GNU Library General Public License for more details.                        *
*                                                                              *
*  You should have received a copy of the GNU General Public License           *
*  along with this program; if not, write to the Free Software                 *
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  *
*******************************************************************************/

/*
  TODO

  add move to planet and colonize code to colonize_settle
  add move to planet and colonize code to colonizeOutpost
  finish colonizeMining, colonizeTerraform and archeology station stuff
 */

/*-----------------------------------------------------------*
 * INCLUDES
 *-----------------------------------------------------------*/

#include <stdio.h>

#include "unit.h"
#include "unittype.h"
#include "unitrule.h"
#include "unitinfo.h"       /* ORDER_*  */
#include "starsys.h"
#include "colony.h"


#include "colonize.h"       /* Own header */

/*******************************************************************************
* FORWARD DECLARATIONS					                                       *
*******************************************************************************/

static int colonize_settle(UNIT_T *ship, int targetno );

/*******************************************************************************
* CODE  								                                       *
*******************************************************************************/

/*
 * Name:
 *    colonize_settle
 * Description:
 *    Function that creates a colony on a planet
 * Input:
 *    ship:      Pointer to the unit doing the colonizing
 *    target_no: Number of target planet to settle
 * Output:
 *    result: integer containing result of order
 */
static int colonize_settle(UNIT_T *ship, int target_no)
{
    PLANET_INFO_T planet_info;


    starsys_get_planetinfo(target_no, &planet_info);

    if(planet_info.sign[0] != 'M')
	{
        return COLONIZE_ERROR_PLANET_TYPE;
    }

    if(ship->cargo_load <= 0)
	{
        return COLONIZE_ERROR_NO_POPULATION;
    }

    colony_settle_planet(target_no, ship->pi.owner, ship->cargo_load);
    /* TODO: Write log, if needed */

    return COLONIZE_SUCCESS;
}

/* ========================================================================== */
/* =========================== PUBLIC FUNCTION(S) =========================== */
/* ========================================================================== */

/*
 * Name:
 *    colonizeDoColonize
 * Description:
 *    The public colonization function
 * Input:
 *    unit_id:   The unit doing the colonization
 *    target_id: The planet being colonized
 * Output:
 *    result:    Wether or not the order was successful
 */
int colonizeDoColonize (int unit_id, int target_id)
{
    PLANET_INFO_T planet_info;
    int    result;
    UNIT_T *ship;
    UNITTYPE_T *put;


    ship   = unit_get(unit_id);
    starsys_get_planetinfo(target_id, &planet_info);


    put = unittype_get(ship->type, NULL);

    /* if the unit is not a colony unit */
    if(put->ability != UNIT_ABILITY_COLONIZE )
    {
        return COLONIZE_ERROR_BAD_UNIT_TYPE;
    }

    /* if the planet is already settled */
    if(planet_info.owner_no > 0)
    {
        return COLONIZE_ERROR_PLANET_USED;
    }

    /* FIXME: This should be a move to, not a failure, but
       in the interest of getting things working,
       this is a stop gap.
    */
    if(ship->order.target != target_id)
    {
        return COLONIZE_ERROR_NOT_IN_ORBIT;
    }
    else
    {
        /* we have a colonize order and a colony unit */
        result = colonize_settle(ship, target_id);
    }

    return result;
}

/*
 * Name:
 *    colonizeDoConstruct
 * Description:
 *    The public construction function
 * Input:
 *    unit_id:   The unit doing the colonization
 *    proj:      Number of project to build (Improvement->'PROJ_OUTPOST')
 *    target_id: The planet having something constructed on
 * Output:
 *    result:    Wether or not the order was successful
 */
int colonizeDoConstruct(int unit_id, char proj, int target_id)
{
    UNIT_T   *ship;


    ship  = unit_get(unit_id);

    /* if the unit is not a colony unit */
    if(ship->ability != UNIT_ABILITY_CONSTRUCT)
    {
        return COLONIZE_ERROR_BAD_UNIT_TYPE;
    }

    /* FIXME: This should be a move to, not a failure, but
       in the interest of getting things working,
       this is a stop gap.
    */
    if(ship->order.target != target_id)
    {
        return COLONIZE_ERROR_NOT_IN_ORBIT;
    }
    else
    {
        /* we have a colonize order and a colony unit */
        /* figure out what to do with that order */
        if(! starsys_build_outpost(ship->pi.owner, target_id, proj, 0))
        {
            /* Planet is already settled or creation of outpost failed */
            return COLONIZE_ERROR_PLANET_USED;
        }
    }

    return COLONIZE_SUCCESS;
}


