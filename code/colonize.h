/*******************************************************************************
*  COLONIZE.H                                                                  *
*      - Functions and data for handling colonization 					       *
*                                                                              *
*   FREE SPACE COLONISATION			                                           *
*      (c)2002 - 2017 Paul Mueller <muellerp61@bluewin.ch>                     *
*                                                                              *
*  File by:                                                                    *
*      Brian Webb <bwebb@kasmir.org>                                           *
*                                                                              *
*  This program is free software; you can redistribute it and/or modify        *
*  it under the terms of the GNU General Public License as published by        *
*  the Free Software Foundation; either version 2 of the License, or           *
*  (at your option) any later version.                                         *
*                                                                              *
*  This program is distributed in the hope that it will be useful,             *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of              *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
*  GNU Library General Public License for more details.                        *
*                                                                              *
*  You should have received a copy of the GNU General Public License           *
*  along with this program; if not, write to the Free Software                 *
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  *
*******************************************************************************/

#ifndef _FSC_COLONIZE_H
#define _FSC_COLONIZE_H

/*******************************************************************************
* INCLUDES                                 				                       *
*******************************************************************************/

#include "unitinfo.h"

/* -------- Defines -------- */

/* Return Values */

#define COLONIZE_ERROR_NONE           0x00
#define COLONIZE_ERROR_BAD_ORDER      0x01
#define COLONIZE_ERROR_BAD_UNIT_TYPE  0x02
#define COLONIZE_ERROR_NO_POPULATION  0x03
#define COLONIZE_ERROR_PLANET_USED    0x04
#define COLONIZE_ERROR_PLANET_TYPE    0x05
#define COLONIZE_ERROR_STATION_EXISTS 0x06
#define COLONIZE_ERROR_NOT_IN_ORBIT   0x07

#define COLONIZE_SUCCESS_ON_ROUTE     0xFE
#define COLONIZE_SUCCESS              0xFF

/* Colonize orders are all 0x1X */
#define COLONIZE_ORDER_FLAG           0x10

/* outpost type postions in improvement array */
#define COLONIZE_OTPST   5  /* "OuP" */
#define COLONIZE_ARCH    6  /* "ArS" */
#define COLONIZE_MINE    7  /* "MiS" */
#define COLONIZE_TERRA   8  /* "TeS" */

#define COLONIZE_SHPYD   2  /* "ShY" */
#define COLONIZE_RSRCH  27  /* "ReC" */
#define COLONIZE_MNFCT  10  /* "MaC" */

#define COLONIZE_OUTPOST    5 /* "OuP" */
#define COLONIZE_ARCHEOLOGY 6 /* "ArS" */
#define COLONIZE_MINING     7 /* "MiS" */
#define COLONIZE_TERRAFORM  8 /* "TeS" */

#define COLONIZE_SHIPYARD   2 /* "ShY" */
#define COLONIZE_RESEARCH  13 /* "ReL" */
#define COLONIZE_MANUFACT  10 /* "MaC" */

#define TEMP_UNSETTLED 0x00

/*******************************************************************************
* CODE                                   				                       *
*******************************************************************************/

int colonizeDoColonize (int unit_id, int target_id);
int colonizeDoConstruct (int unit_id, char proj, int target_id);

#endif
