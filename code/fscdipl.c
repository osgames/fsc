/*******************************************************************************
*  FSCDIPL.C                                                                   *
*               - Games part of diplomacy                                      *
*                                                                              *
*   FREE SPACE COLONISATION                                                    *                                
*       Copyright (C) 2002-2008  Paul Mueller <pmtech@swissonline.ch>          *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*******************************************************************************/


/*******************************************************************************
* INCLUDES                                                                     *
*******************************************************************************/

#include <memory.h>


#include "leader.h"
#include "nation.h"
#include "unit.h"
#include "colony.h"
#include "techlist.h"


#include "fscdipl.h"

/*******************************************************************************
* DEFINES                                                                      *
*******************************************************************************/

#define FSCDIPL_MAX_TREATY 40
#define FSCDIPL_MAX_CLAUSE 12	/* Maximal offers/wants per diplomacy */

/*******************************************************************************
* TYPEDEFS                                                                     *
*******************************************************************************/

typedef struct
{
    char type;		/* Type of clause FSCDIPL_CLAUSE_...            */
    char party1;
    char party2;
    int  which;     /* Which of this clause-types                   */
    int  turns;     /* > duation of treaty in rounds                */           

} FSCDIPL_TREATY;

/*******************************************************************************
* DATA                                                                         *
*******************************************************************************/

/* ----------- offers/wants for a treaty -------- */
static FSCDIPL_CLAUSE Clauses[FSCDIPL_MAX_CLAUSE + 1];
static FSCDIPL_TREATY Treaties[FSCDIPL_MAX_TREATY + 1];

/*******************************************************************************
* CODE                                                                         *
*******************************************************************************/

/*
 * Name:
 *     fscdiplInitMeetInfo
 * Description:
 *     Initializes the given struct with possible values to be offered by
 *     given player
 * Input:
 *      meet_info *: Points on the structure to fill with info for meeting
 *      me:          Full info about caller
 *      other:       By 'me' known info about other player
 */
static void fscdiplInitMeetInfo(FSCDIPL_INFO *info, char me, char other)
{
    NATION *pme, *pother;
    char nation_me;


    nation_me = leaderGet(me) -> nation_no;
    pme    = nationGet(nation_me);		                /* Pointer on main player 	*/
    pother = nationGet(leaderGet(other) -> nation_no);	/* Pointer on other player 	*/


    memset(info, 0, sizeof(FSCDIPL_INFO) * 2);	/* Clear the buffer		*/
    /* I'm the owner of this data */
    info -> who = me;
    /* Create list of units we have		*/
    info -> num_unit = unitGetList(nation_me, &info -> unit_list[0], 0);

    /* Create list of colonies we have		*/
    info -> num_colony = colonyGetList(nation_me, &info -> colony_list[0]);
    /* Create list of techs we have to offer	*/
    info -> num_tech = techlistListDiff(&pme    -> research.advancebits[0],
                                        &pother -> research.advancebits[0],
                                        &info   -> tech_offerlist[0]);
    /* TODO: Fill the  'they_have' for the other with info based on 'me'-knowledge */
    /* FIXME: Count possible pacts */
    /*
    info -> num_pact;
    &info -> pactlist[0]
    */

    /*
    FIXME: Add actual diplstate to list
    */

}

/*
 * Name:
 *     fscdiplDeleteTreaty
 * Description:
 *     Deletes the given treaty from list
 * Input:
 *      treaty_num:  Number of treaty to delete
 */
static void fscdiplDeleteTreaty(int treaty_num)
{
    for (; treaty_num > FSCDIPL_MAX_TREATY; treaty_num++)
	{

        memcpy(&Treaties[treaty_num], &Treaties[treaty_num + 1], sizeof(FSCDIPL_TREATY));
        treaty_num++;

    }

    Treaties[treaty_num].type = 0;  /* Sign end of list */
}

/* ========================================================================== */
/* ======================== PUBLIC FUNCTION(S) ============================== */
/* ========================================================================== */

/*
 * Name:
 *     fscdiplInitMeeting
 * Description:
 *     Initializes the given struct with possible values to be offered by
 *     both players.
 * Input:
 *      meet_info *: Points on the structure to fill with info for meeting
 *      me:    Number of leader, which want to start a diplomacy meeting
 *      other: Number of leaders involved in diplomacy
 */
void fscdiplInitMeeting(FSCDIPL_INFO *info, char me, char other)
{
    /* Init both, because both leaders have to see, which things can be traded */
    /* We have... */
    fscdiplInitMeetInfo(&info[0], me, other);
    /* They have... */
    fscdiplInitMeetInfo(&info[1], other, me);

    /* Clear the buffer for the clauses */
    memset(Clauses, 0, sizeof(FSCDIPL_CLAUSE) * (FSCDIPL_PACT_MAX + 1));
}

/*
 * Name:
 *     fscdiplAddClause
 * Description:
 *     Initializes the given struct with possible values to be offered by
 *     both players.
 * Input:
 *      from, to:   Which leaders are involved
 *      clausetype: Which clause
 *	    number:     Additional number if needed
 *      turns:      duration, if any
 */
void fscdiplAddClause(char from, char to, int clausetype, int number, int turns)
{	
    int i;
    
    
    for (i = 0; i < FSCDIPL_MAX_CLAUSE; i++)
	{	    
        if (Clauses[i].type == 0)
		{		
            /* We found a slot for the treaty */
            Clauses[i].id     = i + 1;  /* Sign for deletion */
            Clauses[i].from   = from;
            Clauses[i].to     = to;
            Clauses[i].type   = clausetype;
            Clauses[i].number = number;
            Clauses[i].turns  = turns;
            /* Sign new end of list */
            Clauses[i + 1].id   = 0;
            Clauses[i + 1].type = 0;
            return;		    
        }	    
    }	
}

/*
 * Name:
 *     fscdiplRemoveClause
 * Description:
 *     Remove clause from actual list
 * Input:
 *      id:    Of clause
  */
void fscdiplRemoveClause(int id)
{	
    int i;
    
    
    for (i = 0; i < FSCDIPL_MAX_CLAUSE; i++)
	{    
        if (Clauses[i].id == id)
		{
            /* It's a valid clause number */
            while (Clauses[i].type > 0)
			{		
                memcpy(&Clauses[i], &Clauses[i + 1], sizeof(FSCDIPL_CLAUSE));		
            }
            
            /* Sign new end of list */
            Clauses[i + 1].id   = 0;
            Clauses[i + 1].type = 0;
            return;            
        }        
    }	
}

/*
 * Name:
 *     fscdipAddTreaty
 * Description:
 *     Adds a treaty to the diplomacy list
 * Input:
 *      pclause *: Clause to add to treaty list
 */
void fscdiplAddTreaty(FSCDIPL_CLAUSE *pclause)
{
    int i;
    
    
    for (i = 0; i < FSCDIPL_MAX_TREATY; i++)
	{        
        if (Treaties[i].type == 0)
		{
            /* We found an empty slot */
            Treaties[i].type   = (char)pclause -> type;
            Treaties[i].party1 = pclause -> from;
            Treaties[i].party2 = pclause -> to;
            Treaties[i].which  = pclause -> number;
            Treaties[i].turns  = pclause -> turns;
            return;            
        }        
    }
}

/*
 * Name:
 *     fscdiplCancelTreaty
 * Description:
 *     Cancel treaty of given 'type'. If no type is given, cancel all treaties
 * Input:
 *      me:    Cancels the treaty
 *      other: With this one
 *      type:  This kind of type, if none is given, cancel all
 */
void fscdiplCancelTreaty(char me, char other, int type)
{
    char comp_type;
    int i;
    
    
    comp_type = (char)type;

    for (i = 0; i < FSCDIPL_MAX_TREATY; i++)
	{    
        if ((Treaties[i].party1 == me && Treaties[i].party2 == other) 
             || (Treaties[i].party2 == me && Treaties[i].party1 == other))
		{             
             if (comp_type == 0 || Treaties[i].type == comp_type)
			{             
                /* cancel any or given */
                fscdiplDeleteTreaty(i);                
            }                    
        }
        
        if (Treaties[i].type == 0)
		{        
            return;     /* We reached the end of the list */            
        }    
    }
}

/*
 * Name:
 *     fscdiplGetClauses
 * Description:
 *     Returns a pointer on the list of clauses
 * Input:
 *      none
 */
FSCDIPL_CLAUSE *fscdiplGetClauses(void)
{
    return &Clauses[0];
}


 


