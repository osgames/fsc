/*******************************************************************************
*  IMPROVE.H                                                                   *
*      - Handling of improvements (social projects) and it's lists             *
*                                                                              *
*   FREE SPACE COLONIZATION             								       *
*      (c)2002 - 2017 Paul Mueller <muellerp61@bluewin.ch>                     *
*                                                                              *
*  This program is free software; you can redistribute it and/or modify        *
*  it under the terms of the GNU General Public License as published by        *
*  the Free Software Foundation; either version 2 of the License, or           *
*  (at your option) any later version.                                         *
*                                                                              *
*  This program is distributed in the hope that it will be useful,             *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of              *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
*  GNU Library General Public License for more details.                        *
*                                                                              *
*  You should have received a copy of the GNU General Public License           *
*  along with this program; if not, write to the Free Software                 *
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  *
*******************************************************************************/

#ifndef _FSC_IMPROVE_H_
#define _FSC_IMPROVE_H_

/* Planet Improvements, including Wonders.  (Alternatively "Buildings".) */

/*******************************************************************************
* INCLUDES                                                                     *
*******************************************************************************/

#include "effect.h"             /* GAME_EFFECT_T_T          */
#include "sdlglcfg.h"  /* SDLGLCFG_RECORD_T        */

/*******************************************************************************
* DEFINES                                                                      *
*******************************************************************************/

#define IMPROVE_MAX_WONDER  15


/* ------- Types of improvements -------- */
#define PROJ_NONE     ((char) 0)
#define PROJ_SOCIAL   ((char) 1)
#define PROJ_MILITARY ((char) 2)
#define PROJ_CULTURE  ((char) 3)
#define PROJ_ACHIEVE  ((char) 4)
#define PROJ_SUPER    ((char) 5)
#define PROJ_WONDER   ((char) 6)
#define PROJ_OUTPOST  ((char) 7)    /* Outpost improvements         */
#define PROJ_EFFECT   ((char) 8)    /* Only add effect, no building */
#define PROJ_OTHER    ((char) 9)

/*******************************************************************************
* TYPEDEFS                                                                     *
*******************************************************************************/

typedef struct
{
    int city;           /* Id of city in which this wonder is built  */
                        /* 0 means: Destroyed                        */
    char id;            /* Id of this improve, 0 means not built yet */
                        /* resp. end of array                        */
    char owner;         /* Id of player which owns this wonder       */
                        /* Maybe color of player ?!                  */
}
IMPROVE_GLOBAL_T;        /* For improves with global (player) effects */
                         /* Wonders in this case                      */

typedef struct
{
    int  improve_no;
    char name[30];              /* Name of improvement  */
    int  cost;                  /* Cost of improvement  */
    char upkeep;                /* Cost for upkeep      */
    char proj_type;             /* Type of improvement  */
    GAME_EFFECT_T effect;       /* Effects of this improvement, vector 0-terminated       */
    int  turns;                 /* Turns needed to build it, taken 'invested' into account */
}
IMPROVE_INFO_T;

/*******************************************************************************
* CODE                                                                         *
*******************************************************************************/

void improve_init(void);
int  improve_get_info(int improve_no, IMPROVE_INFO_T *pinfo, int invested, int build_pts);
char *improve_get_name(int improve_no);
int  improve_build_time(int improve_no, int invested, int build_pts);

/*** =========== List functions =========== ***/
int  improve_get_by_type(char *projlist, char proj_type);
int  improve_get_avail(unsigned char *pimprovebits, char *ptech_keys);
void improve_bits_sub(unsigned char *pact_bits, unsigned char *sub_bits, unsigned char *result_bits);
int  improve_bitstonumbers(unsigned char *pimprovebits, char *plist);
const IMPROVE_GLOBAL_T *improve_get_wonder_list();

/*** =========== Managing functions =========== ***/
int  improve_build(unsigned char *pimprovebits, int improve_no, char key_code[4], GAME_EFFECT_T *pge);
int  improve_remove(unsigned char *pimprovebits, int improve_no, char key_code[4], GAME_EFFECT_T *pge);
void improve_replace(unsigned char *pil, char signold[4], char signew[4]);

/*** =========== Functions for reading in game data =========== ***/
int improve_get_datadesc(SDLGLCFG_RECORD_T *pdatadesc);

#endif  /* _FSC_IMPROVE_H_ */
