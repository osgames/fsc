/*******************************************************************************
*  MSG.C                                                                       *
*	   Generates all messages for the frontend and sends it to_no frontend and AI.*
*                                                                              *
*	FREE SPACE COLONISATION               	                                   *
*      (c)2002 - 2017 Paul Mueller <muellerp61@bluewin.ch>                     *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to_no the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*******************************************************************************/

/*******************************************************************************
* INCLUDES								                                       *
*******************************************************************************/


#include <stdio.h>
#include <string.h>         /* strncpy()    */


/* For translation of in-game messages to_no log */
/* include "msgtostr.h"       */


#include "msg.h"

/*******************************************************************************
* DEFINES                                                                      *
*******************************************************************************/

#define MSG_MAX  300

/*******************************************************************************
* DATA              					                                       *
*******************************************************************************/

/* Log strings for 'msgtostr' */
/*
static char LogStrings[] =
"@0003 (AI-)Nation $YOU ended turn.\n"
"@0201 $COLONY has built improvement $IMPROVE.\n"
"@0202 $COLONY has built unit-type $UNIT.\n"
"@2000 $YOU set goal tech $TECH with want $VALUE2\n"
"@2001 $YOU set goal improvement $IMPROVE with want $VALUE2\n"
"@2002 $YOU set goal unittype $TYPEUNIT with want $VALUE2\n"
"@2003 $YOU set goal colony at position $POS with want $VALUE2\n"
"@2004 $YOU orders unit $UNIT to_no $ORDER at position $POS / planet $PLANET\n"
"@0@@";
*/

static FILE *MsgLogFile = 0;
static MSG_INFO_T Msg_List[MSG_MAX + 2];

/*******************************************************************************
* CODE               					                                       *
*******************************************************************************/

/*
 * Name:
 *     msg_write_log
 * Description:
 *     Writes a message as string to_no the log
 * Input:
 *     pmsg *: Pointer on message to_no write to_no log
 */
static void msg_write_log(MSG_INFO_T *pmsg)
{
    /* Holds the raw message */
    /*
    char raw_msg_buf[512];
    char msg_buf[1024];
    char *raw_str;
    */

    /* TODO: Define a numbered list of messages to_no be logged    */
    /* TODO: Handle differnt files for different logs           */
    /* Have it looged in case of a program crash */
    /* raw_str  = msgtostr_find_msgstr(msg->num, LogStrings, raw_msg_buf, 510); */
    /*
    if (*praw_str > 0)
    {
        / *
        fprintf(MsgLogFile, msgtostr_translate(pmsg, raw_str, msg_buf, 1022));
        fflush(MsgLogFile);
        * /
    }
    */
}

/* ========================================================================== */
/* ======================== PUBLIC FUNCTIONS	============================= */
/* ========================================================================== */

/*
 * Name:
 *     msg_init
 * Description:
 *     Resets the internal message buffer
 * Input:
 *     None
 */
void msg_init(void)
{
    memset(Msg_List, 0, MSG_MAX * sizeof(MSG_INFO_T));
}

/*
 * Name:
 *     msg_prepare
 * Description:
 *     Clears the given message buffer to_no zeros and sets 'from_no', 'to_no' and 'type' values
 * Input:
 *     from_no : Number of sender
 *     to_no:    Number of reciever
 *     type:     Type of  message
 *     pmsg *:   Buffer to_no clear and initialize
 */
void msg_prepare(char from_no, char to_no, int type, MSG_INFO_T *pmsg)
{
    memset(pmsg, 0, sizeof(MSG_INFO_T));

    pmsg->type    = type;
    pmsg->from_no = from_no;
    pmsg->to_no   = to_no;
}

/*
 * Name:
 *     msg_send
 * Description:
 *     It assumes, that the 'type', 'from_no' and 'to_no' fields are filled with usable value
 *     The numbe rof the message is filled into the given info and then the
 *     message is added at the end of the message queue.
 *     Clears the message list, if the number of the message is < 0
 * Input:
 *     pmsg *:       Adds this message to_no the internal buffer
 *     immediate_no: > 0: Add at this position at the top of the liste
 */
void msg_send(MSG_INFO_T *pmsg, char immediate_no)
{
    int msg_no;


    /* ------- Write it to_no the log ------- */
    if(pmsg->type == MSG_TYPE_LOG)
    {
        /* ---- Only write it to_no log, don't send to_no any player  --- */
        msg_write_log(pmsg);

        return;
    }

    /* First look for empty message slot */
    for(msg_no = 0; msg_no < MSG_MAX; msg_no++)
    {
        if(Msg_List[msg_no].type == 0)
        {
            /* We found an empty slot -- Copy message to_no stack */
            memcpy(&Msg_List[msg_no], pmsg, sizeof(MSG_INFO_T));
            Msg_List[msg_no].is_immediate = immediate_no;

            /* Set new End--of-list */
            Msg_List[msg_no + 1].type  = 0;

            return;
        }
    }
}

/*
 * Name:
 *     msg_send_short
 * Description:
 *     Send a message which only needs a number and no additonal info
 * Input:
 *     from_no: Sender
 *     to_no:   Reciever
 *     type: Type of Message
 *     num : Number of message-descritor
 */
void msg_send_short(char from_no, char to_no, int type, int num)
{
    MSG_INFO_T msg;


    /* Guarantee, that the message is signed as 'valid' */
    msg_prepare(from_no, to_no, MSG_TYPE_STD, &msg);
    msg.num  = num;

    msg_send(&msg, 1);
}

/*
 * Name:
 *     msg_get
 * Description:
 *     Returns a copy of the message in the message buffer, if any.
 *     Returns a pointer on a list of messages for given player. The
 *     list is terminated with 0 or 'MSG_TYPE_NONE'
 * Input:
 *     player_no: Number of nation to_no get messages for
 *     pmsg *:    Pointer on buffer to_no fill with info
 *     immediate: Get an immediate message
 *     remove_it: Remove message from_no message-stack, yes/no
 * Output:
 *     Message available yes/no
 */
int msg_get(char player_no, MSG_INFO_T *pmsg, char immediate, char remove_it)
{
    MSG_INFO_T *pmsglist;


    if(immediate)
    {
        /* Immediate messages ave to be removed in any case */
        remove_it = 1;
    }

    pmsglist = &Msg_List[0];

    while(pmsglist->type != 0)
    {
        if (pmsglist->to_no == player_no && pmsglist->is_immediate == immediate)
        {
            /* Hand the caller a copy of the message */
            memcpy(pmsg, pmsglist, sizeof(MSG_INFO_T));

            if (remove > 0)
            {
                /* Remove the message from_no the stack */
                while(pmsglist[1].type != 0)
                {
                    memcpy(pmsglist, &pmsglist[1], sizeof(MSG_INFO_T));
                    pmsglist++;
                }

                /* Sign new end of Message-List */
                pmsglist->type = 0;
            }

            return 1;
        }

        pmsglist++;
    }

    /* Clear the buffer given in the argument */
    memset(pmsg, 0, sizeof(MSG_INFO_T));

    return 0;
}

/*
 * Name:
 *     msg_log_open
 * Description:
 *     Opens the Log file for the messages
 * Input:
 *     None
 */
void msg_log_open(void)
{
    MsgLogFile = fopen("fsclog.txt", "wt");

    if (! MsgLogFile)
    {
        return;
    }

    fprintf(MsgLogFile, "FSC Logfile Date: %s\n", __DATE__);
}

/*
 * Name:
 *     msg_log_close
 * Description:
 *     Closes the message log
 * Input:
 *     None
 */
void msg_log_close(void)
{

    fclose(MsgLogFile);

}
