/*******************************************************************************
*  GOAL.C                                                                      *
*	- Definition of goals. Tech for all players, others for AI                 *
*									                                           *
*   FREE SPACE COLONISATION			                                           *
*      (c)2002 - 2017 Paul Mueller <muellerp61@bluewin.ch>                     *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*******************************************************************************/

/*******************************************************************************
* INCLUDES								                                       *
*******************************************************************************/

#include <string.h>


#include "goal.h"

/*******************************************************************************
* DEFINES								                                       *
*******************************************************************************/

#define GOAL_MAX  300

/*******************************************************************************
* DATA  								                                       *
*******************************************************************************/

static GOAL_T Goal[GOAL_MAX + 1];

/*******************************************************************************
* CODE  								                                       *
*******************************************************************************/

/* ========================================================================== */
/* ======================== PUBLIC FUNCTIONS	============================= */
/* ========================================================================== */

/*
 * Name:
 *     goal_get
 * Description:
 *     Returns a pointer on goal with given number
 * Input:
 *     goal_no: Number of goal
 * Output:
 *     Pointer on goal
 */
GOAL_T *goal_get(int goal_no)
{
    if(goal_no > 0 && goal_no < GOAL_MAX)
    {
        return &Goal[goal_no];
    }

    /* Empty GOAL_T */
    return &Goal[0];
}

/*
 * Name:
 *     goal_get_list
 * Description:
 *     Fills list with numbers of goals for given owner
 *     Returns the number of goals in list. The list is 0-terminated.
 * Input:
 *     owner:  Owner to get the list of goals for
 *     list *: Pointer on list to fill with number of goals
 * Output:
 *     Number of goals in list
 */
int goal_get_list(char owner, int *list)
{
    int count, index;


    index = 1;
    count = 0;

    while(Goal[index].owner != 0)
    {
        if(Goal[index].owner == owner || owner == -1)
        {
            *list = index;
            list++;
            count++;
        }

        index++;
    }

    /* terminate it */
    *list = 0;

    return count;
}

/*
 * Name:
 *     goal_init
 * Description:
 *     Creates a new goal, if possible, else returns pointer on empty goal.
 * Input:
 *     owner:   For this owner
 *     who:     Who should work on this one
 *     pgoal *: Pointer on struct to initialize
 */
void goal_init(char owner, char who, GOAL_T *pgoal)
{
    memset(pgoal, 0, sizeof(GOAL_T));     /* Clear given buffer */

    pgoal -> owner = owner;
    pgoal -> who   = who;
}

/*
 * Name:
 *      goal_set
 * Description:
 *      Adds given goal to the goal list, if an empty slot is available.
 * Input:
 *     pgoal *: Pointer on goal to add to list
 * Output:
 *     Number of goal (for SELF-goals in AI)
 */
int goal_set(GOAL_T *pgoal)
{
    int i;


    for(i = 1; i < (GOAL_MAX - 2); i++)
    {
        if(Goal[i].owner <= 0)
        {
            /* We found an empty slot */
            memcpy(&Goal[i], pgoal, sizeof(GOAL_T));

            return i;
        }
    }

    return 0;
}

/*
 * Name:
 *      goal_delete
 * Description:
 *      Deletes given goal
 * Input:
 *     goal_no: Number of goal to delete
 */
void goal_delete(int goal_no)
{
    /* Remove the goal from the list of goals */
    memset(&Goal[goal_no], 0, sizeof(GOAL_T));

    /* ----- Sign it as unused ---- */
    Goal[goal_no].owner = -1;
}
