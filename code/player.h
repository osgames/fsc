/*******************************************************************************
*  PLAYER.H                                                                    *
*	- Definitions of a player in the game game part                            *
*                                                                              *
*   FREE SPACE COLONISATION                                                    *
*       Copyright (C) 2002-2017  Paul Müller <muellerp61@bluewin.ch>           *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*******************************************************************************/

#ifndef _FSC_PLAYER_H_
#define _FSC_PLAYER_H_

/*******************************************************************************
* INCLUDES                                                                     *
*******************************************************************************/

#include "fscshare.h"
#include "fscfile.h"

/*******************************************************************************
* TYPEDEFS                                                                     *
*******************************************************************************/

typedef enum
{
    PI_IT_TAXES    = 0, /* int taxes;           */
    PI_IT_TRADE    = 1, /* int trade;           */
    PI_IT_TRIBUTE  = 2, /* int tributeincome;   */
    PI_IT_TREASURY = 3, /* int treasury --> Money the player owns */
    PI_IT_TOTAL    = 4, /* int totalincome;     */
    PI_IT_MAX      = 5
}
PI_INCOME_TYPE;

typedef enum
{
    PI_EXP_MILITARY    = 0, /* int exp_military;         */
    PI_EXP_SOCIAL      = 1, /* int exp_social;           */
    PI_EXP_RESEARCH    = 2, /* int exp_research;         */
    PI_EXP_MAINTENANCE = 3, /* int exp_maintenance;      */
    PI_EXP_LEASES      = 4, /* int exp_leases;           */
    PI_EXP_GIA         = 5, /* int exp_GIA;              */
    PI_EXP_ESPIONAGE   = 6, /* int exp_espionage;        */
    PI_EXP_DESTABILIZE = 7, /* int exp_destabilization;  */
    PI_EXP_TRIBUTEPAY  = 8, /* int exp_tributepay;       */
    PI_EXP_TOTAL       = 9, /* int exp_total;            */
    PI_EXP_MAX         = 10
}
PI_EXPENSES_TYPE;

/*******************************************************************************
* TYPEDEFS								                                       *
*******************************************************************************/

typedef struct
{
    char name[FSC_NAMELEN_MAX + 1];        /* Name of this race               */
    char leader_name[FSC_NAMELEN_MAX + 1]; /* Name of its leader              */
    char planet_type;                      /* Preferred planet type           */
    char pol_party;                        /* Basic political party           */
    char base_weight[GVAL_MAX];            /* Weights prefferred for decision */
    char act_weight[GVAL_MAX];             /* Weights usef for decision       */
    /* --------- Fixed bonuses, set at game start --------------------------- */
    /* Which are the fix bonuses is mapped in internal list */
    char fix_bonus[10 + 1];
}
PLAYER_RACE_T;

typedef struct
{
    short int actual;               /* advance being researched in          */
    short int invested;             /* # bulbs already invested in research */
    unsigned char advancebits[20];  /* Bit set: Researched                  */
}
PLAYER_RESEARCH_T;


/*******************************************************************************
* CODE                                                                         *
*******************************************************************************/

/*** ===================== INIT FUNCTIONS ================= ***/
void player_init(int player_no, int map_pos, int home_planet);
void player_remove(int player_no);

/*** ====== MANAGEMENT Functions =============== ***/
void player_race(int player_no, PLAYER_RACE_T *prace, int set);
void player_get_researchinfo(int player_no, PLAYER_RESEARCH_T *presearch);
void player_set_research(int player_no, int tech_no);

/* ========= Functions to change values of player ======= */
void player_add_bonus(int player_no, int bonus_type, char amount);
void player_set_value(int player_no, int value_no, int amount);

/* ===================== INFORMATION FUNCTIONS ========== */
const char *player_get_weights(int player_no);
void player_get_ranges(int player_no, char *pranges);
void player_get_bonus(int player_no, char *pbonus_pts);
int  player_get_homepos(int player_no);

void playerGetFinancialInfo(char player_no, int *income, int *expenses);
int  player_get_value(char player_no, int which, int value);

/* =================== UNIT FUNCTIONS =================== */
int playerUnitCreate(char unit_type, int ability, char player_no, int pos, char cargo_load);

/* ==================== Additional functions ============ */
void playerEndTurnUpdate(int player_no, int *pvalues);
char *player_get_name(int player_no);

/* =================== Load and save data of player ===== */
int player_get_loadsave_info(FSCFILE_DATADESC_T *pdatadesc, char save);

#endif  /* _FSC_PLAYER_H_ */
