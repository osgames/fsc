 /*******************************************************************************
*  UNITMOVE.H                                                                  *
*	    - Declarations and functions for unit movement	                       *
*                                                                              *
*	FREE SPACE COLONISATION               	                                   *
*      (c)2002 - 2017 Paul Mueller <muellerp61@bluewin.ch>                     *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*******************************************************************************/

#ifndef _FSC_UNITMOVE_H_
#define _FSC_UNITMOVE_H_

/*******************************************************************************
* INCLUDES                                				                       *
*******************************************************************************/

#include "fscmap.h" /* FSCMAP_POSINFO_T */

/*******************************************************************************
* DEFINES                                				                       *
*******************************************************************************/

#define UNITMOVE_CHOOSE_NUMBER   1   /* Choose unit with this number */
#define UNITMOVE_CHOOSE_POSITION 2   /* Choose unit at this position */
#define UNITMOVE_CHOOSE_CURRENT  3   /* Choose unit at list-cursor   */
#define UNITMOVE_CHOOSE_NEXT     4   /* Choose next unit in list     */
#define UNITMOVE_CHOOSE_NEXTSKIP 5   /* Choose next unit in list
                                           and skip actual one          */

/*******************************************************************************
* TYPEDEFS                                				                       *
*******************************************************************************/

typedef struct
{
    int  unit_no;           /* Number of unit to move                       */
    FSCMAP_POSINFO_T pi;    /* Owner, map-position x/y and facing           */
                            /* Map x/y position of chosen unit, for drawing */
    char name[20 + 1];  /* Name of unit */
    int  hp[2];         /* UI_VALUE_ACT ..._FULL Unit_HPStr             */
    int  moves[2];      /* UI_VALUE_ACT ..._FULL                        */
    char type;          /* Type of this unit, for AI -- Unit_ClassName  */
                        /* Number of icon for display                   */
    int attack,
        defense;        /* Attack and defense values                    */
    int ability;        /* UNIT_AIROLE_* saves some func calls  */
                        /* Class of ship                        */
    int range_no;       /* To save some call while playing      */
                        /* The range of the ship: 0 / 1 / 2     */
                        /* RULES_RANGE_*                        */
    int order_no;       /* Actual order                         */
    int sensor_range;
    int cargo_type,
        cargo_load;     /* Number of settlers, production-points  */
    int  map_pos;
    /* Has to be filled, if planet is chosen with unit */
    int  planet_no;      /* Target for action, if a planet is chosen */
    char proj_list[10];
    /* The adjacent tiles for this unit */
    int numpos_reachable;
    int adjposlist[10];
}
UNIT_INFO_T;


/*******************************************************************************
* CODE      							                                       *
*******************************************************************************/

void unitmove_fill_manageinfo(char player_no, int unit_no, UNIT_INFO_T *pmnginfo);

char unitmove_get_planetactions(UNIT_INFO_T *punit_info);

void unitmoveCreate(int unit_no, int pos, int type_no);
int  unitmoveExecuteOrder(UNIT_INFO_T *punit_info);
int  unitmove_choose(UNIT_INFO_T *punit_info, int value, char which);
void unitmove_set_order(UNIT_INFO_T *punit_info, int order, int target, int build);
void unitmove_attack(int attacker_no, int defender_no);
void unitmoveEndTurn(char nation_no);


#endif  /* _FSC_UNITMOVE_H_ */
