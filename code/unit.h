/*******************************************************************************
* UNIT.H                                                                       *
*      - Declarations and functions for handling game part of units            *
*                                                                              *
*  FREE SPACE COLONISATION                                                     *
*      (c)2002 - 2017 Paul Mueller <muellerp61@bluewin.ch>                     *
*                                                                              *
*  This program is free software; you can redistribute it and/or modify        *
*  it under the terms of the GNU General Public License as published by        *
*  the Free Software Foundation; either version 2 of the License, or           *
*  (at your option) any later version.                                         *
*                                                                              *
*  This program is distributed in the hope that it will be useful,             *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of              *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
*  GNU Library General Public License for more details.                        *
*                                                                              *
*  You should have received a copy of the GNU General Public License           *
*  along with this program; if not, write to the Free Software                 *
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  *
*******************************************************************************/

#ifndef _FSC_UNIT_H_
#define _FSC_UNIT_H_

/*******************************************************************************
* INCLUDES                                                                     *
*******************************************************************************/

#include "fscfile.h"        /* Save data            */
#include "fscmap.h"         /* FSCMAP_POSINFO_T     */


#define UNIT_NAMELEN 16

/*******************************************************************************
* TYPEDEFS                                                                     *
*******************************************************************************/

typedef struct
{
    char what;          /* Order to execute                             */
    char target_type;   /* Describes kind of value held in 'target'     */
    short int target;   /* Number of planet the unit is working on      */
                        /* 'planet it's orbitting, if ORDER_ORBITTING   */
                        /* 'goto' position if ORDER_GOTO                */
    char build;         /* What to build, if build command              */
}
UNIT_ORDER_T;           /* Allows more orders. Order stack for AI   */

typedef struct
{
    short int next_id;  /* Link to next unit in single linked list */
    FSCMAP_POSINFO_T pi;
    char facing;
    char type;          /* Type of this unit, for AI            */
                        /* Number of icon for display           */
    char name[UNIT_NAMELEN + 1];
    /* ----------  Additional info about unit ----------------- */
    char hp[2];         /* UI_VALUE_ACT ..._FULL                */
    char moves[2];      /* UI_VALUE_ACT ..._FULL                */
                        /* (full moves of type, incl. bonus)    */
    char attack,
         defense;       /* Attack and defense values            */
    char sensor_range;  /* Sensorrange for this unit            */
    char ability;       /*                                      */
    UNIT_ORDER_T order; /* order.what --> order                 */
                        /* order.target > ORDER_GOTO: goto_dest */
    short int  slot_id; /* Id of cargohold data array, if any   */
                        /* If used for attachment of starbase   */
                        /* modules: Id of module list for this  */
                        /* starbase, other unit for charge      */
    char cargo_type;    /* UI_CARGO_...                         */
    char cargo_load;    /* How much cargo is loaded             */
    /* ------------ Additional data only used for game -------- */
    char race;          /* Race/Nationality of unit             */
                        /* Small races ?!                       */
    short int goto_start; /* Start position for fast GOTO-path   */
    short int goal_no;    /* > 0 if this unit has a GOAL_T         */
}
UNIT_T;

/*******************************************************************************
* CODE                                                                         *
*******************************************************************************/

void unit_init(void);
UNIT_T *unit_get(int unitno);        /* unitGet */
/* char *unitName(int unitno);  */
void unit_get_name(int unit_no, char *pdest);

int  unit_get_list(int player_no, int *plist, char withorder);

int  unit_count_at_pos(int pos, char owner, int militaryonly);
int  unitCountAtPosAirole(int pos, int airole);

/* Get info */
int unitTransportCapacity(UNIT_T *punit, int *captype);
int *unitPosTransportCapacity(int pos, char owner);

/* Create/delete and linked list functions */
void unit_list_insert(short int *pbase, short int unit_no); /* unitList... */
int  unit_list_find(short int *base, short int unitno);
int  unit_list_count(short int *base);
void unit_list_remove(short int *base, short int unitno);
void unit_delete(UNIT_T *punit);
int  unit_create(char unit_type, char key_code[4], char owner, char x, char y, char cargo_load);
/* int  unit_create_from_unit(int unit_no); */
int unit_morph(int unit_no);
int unit_find_next(char owner_no, int act_unit_no, char with_moves);
/* int unit_find_number(char owner_no, int unit_no); */

/* --------- Additional work for end of turn ----------- */
void unit_restore_moves(void);
void unitGetMilitaryMightSum(int *attackval, int *defenseval, int numvalues);
void unit_change_to_ability(UNIT_T *punit, int ability);

/* -------- Load and save data of units --------- */
int unit_get_loadsave_info(FSCFILE_DATADESC_T *pdatadesc, char save);

#endif /* _FSC_UNIT_H_ */
