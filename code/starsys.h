/*******************************************************************************
*  STARSYS.H                                                                   *
*	- Definitions of a star system in the game, game info            	       *
*                                                                              *
*   FREE SPACE COLONISATION                                                    *
*       Copyright (C) 2002 - 2017 Paul Mueller <muellerp61@bluewin.ch>         *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*******************************************************************************/

#ifndef _FSC_STARSYS_H_
#define _FSC_STARSYS_H_

/*******************************************************************************
* INCLUDES                                                                     *
*******************************************************************************/

#include "fscfile.h"
#include "fscshare.h"

/*******************************************************************************
* DEFINES                                                                     *
*******************************************************************************/

#define STAR_DATA_STARS   ((char)0x01)
#define STAR_DATA_PLANETS ((char)0x02)
#define STAR_DATA_NAMES   ((char)0x03)


#define STAR_POSLIST_ALL     1
#define STAR_POSLIST_OWNED   2
#define STAR_POSLIST_UNOWNED 3

#define STARSYS_MAX_STAR      450
#define STARSYS_MAXPLANETSYST   5   /* Maximum of planets per system    */
#define STAR_NAME_LEN          23   /* Maximum of chars for star name   */
#define PLANET_NAME_LEN        15   /* Maximum of chars for planet name */

/* -------- Star types -------- */
#define STARSYS_YELLOWSTAR    0x03     /* Number 3 is yellow star           */
#define STARSYS_MAX_STARTYPE   6       /* Maximum number of star types is 6 */

/* -------- Types of settlements for planets -------- */
#define STARSYS_SETTLETYPE_NONE    0x00
#define STARSYS_SETTLETYPE_COLONY  0x01
#define STARSYS_SETTLETYPE_OUTPOST 0x02

/*******************************************************************************
* TYPEDEFS                                                                     *
*******************************************************************************/

typedef struct
{
    /* ---------- Basic info for planet ----------------------- */
    char key_code;      /* 'orig_class' and 'settle_class' of planet */
                        /* if number, then this type of star,       */
                        /* and holds name of star                   */
    char name[PLANET_NAME_LEN + 1]; /* Name of the planet           */
    char yield_size;	/* Planets size	[7..24] (Number of yields)  */
                        /* Gas core _MUST_ be 0 for display         */
    char yield[GVAL_PLANETYIELD_MAX];
    char type_no;       /* Number of icon for this planet           */
}
PLANET_INITDEF_T;

typedef struct
{
    char owner_no;
    char settle_class;  /* 'orig_class' and 'settle_class' of planet */
                        /* if number, then this type of star,       */
                        /* and holds name of star                   */
    char name[PLANET_NAME_LEN + 1];  /* Name of the planet           */
    char sign[2];
    char yield_size;	/* Planets size	[7..24] (Number of yields)  */
                        /* Gas core _MUST_ be 0 for display         */
    char yield[GVAL_PLANETYIELD_MAX];
    int  terraform_perc;  /* In percent, if greater then 0 */
    char is_settled;    /* is aready settled */
}
PLANET_INFO_T;

/*******************************************************************************
* CODE                                                                         *
*******************************************************************************/

/* Initializes the starsys module: Clears all buffers for data  */
void starsys_init(void);
int  starsys_create_one(short int map_pos, int habit_chance, int type, PLANET_INITDEF_T *ppre_def);

/* =========== STARS ========= */
char starsys_get_starinfo(int pos, char *star_type, char *pstarname, int *planets, char full_info);
int  starsys_get_starpos(int star_no);
int  starsys_get_star_poslist(char player_no, int *pos_list, int list_size, int which);
void starsys_get_name(char which, int number, char *pname);

/* =========== PLANETS ========= */
int  starsys_get_planetinfo(int planet_no, PLANET_INFO_T *pinfo);
void starsys_set_planetupgrade(int planet_no, int which, char amount_pts);
int  starsys_bestplanet(int map_pos, char settle_class, char *pweight, char *terraform);

/* =========== Functions for settling planets ========= */
int starsys_get_bestplanet(int first_planet, char num_planet, char *yieldweight, char *terraform);
int starsys_planet_doterraform(int planet_no, char settle_class, int build_points);
int starsys_settle_planet(int planet_no, char player_no, char settle_type, int settle_no, char *workers_yield);
int starsys_build_outpost(int player_no, int planet_no, char proj_which, int build_points);

/* =========== Planets of star ========= */

int  starsys_get_planets_settled(char ownerno, int *planetnos);
void starsys_explore(int pos, int star_no);

/* ---------- For loading of data from file ----- */
int starsys_get_loadsave_info(FSCFILE_DATADESC_T *pdatadesc, char save);

#endif /* _FSC_STARSYS_H_ */
