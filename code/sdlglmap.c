/*******************************************************************************
*  SDLGLMAP.C                                                                  *
*      - Functions for handling a sdlglmap (windowed view and so on)           *
*                                                                              *
*  SDLGL - SDLGLMAP                                                            *
*      (c)2002 - 2017 Paul Mueller <muellerp61@bluewin.ch>                     *
*                                                                              *
*  This program is free software; you can redistribute it and/or modify        *
*  it under the terms of the GNU General Public License as published by        *
*  the Free Software Foundation; either version 2 of the License, or           *
*  (at your option) any later version.                                         *
*                                                                              *
*  This program is distributed in the hope that it will be useful,             *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of              *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
*  GNU Library General Public License for more details.                        *
*                                                                              *
*  You should have received a copy of the GNU General Public License           *
*  along with this program; if not, write to the Free Software                 *
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  *
*******************************************************************************/

/*******************************************************************************
* INCLUDES                                 				                     *
*******************************************************************************/

#include "sdlglmap.h"

/*******************************************************************************
* DEFINES                                  				                     *
*******************************************************************************/

#define SDLGLMAP_FNORTH  0x01
#define SDLGLMAP_FEAST   0x02
#define SDLGLMAP_FSOUTH  0x04
#define SDLGLMAP_FWEST   0x08

/*******************************************************************************
* DATA                                    				                       *
*******************************************************************************/

/* ------ Data for movement checking ------ */
SDLGLMAP_XY AdjacentXY[8] =
{
    {  0, -1 }, { +1, -1 }, { +1,  0 }, { +1, +1 },
    {  0, +1 }, { -1, +1 }, { -1,  0 }, { -1, -1 }
};

static SDLGLMAP_XY IsoDir[2][8] =
{
    {
        {  0, -1 }, { +1, -1 }, { +2, 0 }, { +1,  0 },
        {  0, +1 }, { -1,  0 }, { -2, 0 }, { -1, -1 }
    },
    {
        {  0, -1 }, { +1,  0 }, { +2, 0 }, { +1, +1 },
        {  0, +1 }, { -1, +1 }, { -2, 0 }, { -1,  0 }
    }
};

/* TODO: Add 'Iso2Dir'    */

static char Heading_List[] =
{
    0,  0,  2,  1,
    4, -1,  3, -1,
    6,  7, -1, -1,
    5, -1, -1, -1
};

static int RadiusCount[]       = { 0, 8, 20, 36, 60, 96, 128, 176, 0 };
static int Count_Per_Radius[]  = { 0, 8, 12, 16, 24, 36, 32 , 48, 0  };
static SDLGLMAP_XY Radius_XY[] =
{
    /* ----- Distance 1 ---- */
    { 0, -1 }, {  1, -1 }, {  1, 0 }, {  1,  1 },
    { 0,  1 }, { -1,  1 }, { -1, 0 }, { -1, -1 },
    /* ----- Distance 2 ---- */
    { -1, -2 }, {  0, -2 }, { +1, -2 }, { +2, -1 },
    { +2,  0 }, { +2, +1 }, { +1, +2 }, {  0, +2 },
    { -1, +2 }, { -2, +1 }, { -2,  0 }, { -2, -1 },
    /* ----- Distance 3 ---- */
    { -1, -3 }, {  0, -3 }, { +1, -3 }, { +2, -2 },
    { +3, -1 }, { +3,  0 }, { +3, +1 }, { +2, +2 },
    { +1, +3 }, {  0, +3 }, { -1, +3 }, { -2, +2 },
    { -3, +1 }, { -3,  0 }, { -3, -1 }, { -2, -2 },
    /* ----- Distance 4 ---- */
    { -1, -4 }, {  0, -4 }, { +1, -4 }, { +2, -3 },
    { +3, -3 }, { +3, -2 }, { +4, -1 }, { +4,  0 },
    { +4, +1 }, { +3, +2 }, { +3, +3 }, { +2, +3 },
    { +1, +4 }, {  0, +4 }, { -1, +4 }, { -2, +3 },
    { -3, +3 }, { -3, +2 }, { -4, +1 }, { -4,  0 },
    { -4, -1 }, { -3, -2 }, { -3, -3 }, { -2, -3 },
    /* ----- Distance 5 ---- */
    { -1, -5 }, {  0, -5 }, { +1, -5 }, { +2, -5 },
    { +2, -4 }, { +3, -4 }, { +4, -3 }, { +4, -2 },
    { +5, -2 }, { +5, -1 }, { +5,  0 }, { +5, +1 },
    { +5, +2 }, { +4, +2 }, { +4, +3 }, { +3, +4 },
    { +2, +4 }, { +2, +5 }, { +1, +5 }, {  0, +5 },
    { -1, +5 }, { -2, +5 }, { -2, +4 }, { -3, +4 },
    { -4, +3 }, { -4, +2 }, { -5, +2 }, { -5, +1 },
    { -5,  0 }, { -5, -1 }, { -5, -2 }, { -4, -2 },
    { -4, -3 }, { -3, -4 }, { -2, -4 }, { -2, -5 },
    /* ----- Distance 6 ---- */
    { -2, -6 }, { -1, -6 }, {  0, -6 }, { +1, -6 },
    { +2, -6 }, { +3, -5 }, { +4, -4 }, { +5, -3 },
    { +6, -2 }, { +6, -1 }, { +6,  0 }, { +6, +1 },
    { +6, +2 }, { +5, +3 }, { +4, +4 }, { +3, +5 },
    { +2, +6 }, { +1, +6 }, {  0, +6 }, { -1, +6 },
    { -2, +6 }, { -3, +5 }, { -4, +4 }, { -5, +3 },
    { -6, +2 }, { -6, +1 }, { -6,  0 }, { -6, -1 },
    { -6, -2 }, { -5, -3 }, { -4, -4 }, { -3, -5 },
    /* ----- Distance 7 ---- */
    { -2, -7 }, { -1, -7 }, {  0, -7 }, { +1, -7 },
    { +2, -7 }, { +3, -6 }, { +4, -6 }, { +4, -5 },
    { +5, -5 }, { +5, -4 }, { +6, -4 }, { +6, -3 },
    { +7, -2 }, { +7, -1 }, { +7,  0 }, { +7, +1 },
    { +7, +2 }, { +6, +3 }, { +6, +4 }, { +5, +4 },
    { +5, +5 }, { +4, +5 }, { +4, +6 }, { +3, +6 },
    { +2, +7 }, { +1, +7 }, {  0, +7 }, { -1, +7 },
    { -2, +7 }, { -3, +6 }, { -4, +6 }, { -4, +5 },
    { -5, +5 }, { -5, +4 }, { -6, +4 }, { -6, +3 },
    { -7, +2 }, { -7, +1 }, { -7,  0 }, { -7, -1 },
    { -7, -2 }, { -6, -3 }, { -6, -4 }, { -5, -4 },
    { -5, -5 }, { -4, -5 }, { -4, -6 }, { -3, -6 },
    { 0 }
};

/*******************************************************************************
* CODE                                     				                       *
*******************************************************************************/

/*
 * Name:
 *     sdlglmap_translate_toiso
 * Description:
 *     Translates the posxy to iso coordinates.
 * Input:
 *     pmap *:  For this map
 *     cx, cy:  Where he user has clicked
 *     w, h:    Size of window, the user clicked in
 *     posxy *: Where to return the position as XY.
 */
static void sdlglmap_translate_toiso(SDLGLMAP_INFO_T *pmap, int cx, int cy, int w, int h, SDLGLMAP_XY *posxy)
{
    int halfwidth, halfheight;
    int mapmodx, mapmody;       /* Position of click in actual tile square  */
    int tile_w, tile_h;


    tile_w = w / pmap->vp.w;
    tile_h = h / pmap->vp.h;
    mapmodx = cx % tile_w;
    mapmody = cy % tile_h;

    /* Position in rectangle which holds the diamond */
    halfwidth  = tile_w / 2;
    halfheight = tile_h / 2;

    /* Adjust relative tile x */
    posxy->x = (posxy->x * 2) + 1;

    /* Center of diamond to draw    */
    /* Translate to ISO-Input:      */
    /* Left top: x - 1              */
    /* Left bottom: x - 1 / y + 1   */
    /* Right top: x + 1             */
    /* Right bottom: x + 1 / y + 1  */

    /* Now adjust the center of the diamond shape, depending on subposition */
    if(mapmodx > halfwidth)
    {
        /* Checkvalue */
        mapmodx -= halfwidth;

        /* Right side of diamond shape */
        if(mapmody > halfheight)
        {
            mapmody -= halfheight;

            /* Right bottom piece of rectangle */
            mapmodx = halfheight - mapmodx;

            if((mapmody * 2) > mapmodx)
            {
                posxy->x++;
                posxy->y++;
            }
        }
        else
        {
            /* Right top piece in rectangle: Ok. */
            if((mapmody * 2) < mapmodx)
            {
                posxy->x++;
            }
        }
    }
    else
    {
        /* Left side of diamond shape */
        if(mapmody >  halfheight)
        {
            mapmody -= halfheight;

            /* Bottom piece */
            if((mapmody * 2) > mapmodx)
            {
                posxy->x--;
                posxy->y++;
            }
        }
        else
        {
            /* Top piece */
            mapmodx = halfwidth - mapmodx;

            if((mapmody * 2) < mapmodx)
            {
                posxy->x--;
            }
        }
    }
}

/*
 * Name:
 *     sdlglmap_viewport_center
 * Description:
 *     Centers the viewport window on map to given position.
 *     If "vp->cursor" < -1, then the cursor is invisible and nothing happens.
 * Input:
 *     pmap *:       For this map
 *     pos_x, pos_y: Position to center to
 *     vp *:         Pointer on viewport to handle
 */
static void sdlglmap_viewport_center(SDLGLMAP_INFO_T *pmap, int pos_x, int pos_y)
{
    int vp_max_x, vp_max_y;


    if(pos_x < 0 || pos_y < 0)
    {
        /* Ignore, for safety. */
        return;
    }

    pos_x -= (pmap->vp.w / 2);
    pos_y -= (pmap->vp.h / 2);

    /* Adjust to left top, if needed */
    pmap->vp.x = (pos_x < 0) ? 0 : pos_x;
    pmap->vp.y = (pos_y < 0) ? 0 : pos_y;

    /* Check overflow right/bottom */
    vp_max_x = pmap->size_w - pmap->vp.w;
    vp_max_y = pmap->size_h - pmap->vp.h;

    pmap->vp.x = (pmap->vp.x > vp_max_x) ? vp_max_x : pmap->vp.x;
    pmap->vp.y = (pmap->vp.y > vp_max_y) ? vp_max_y : pmap->vp.y;
}

/* ========================================================================== */
/* ============================= THE MAIN ROUTINE(S) ======================== */
/* ========================================================================== */

/*
 * Name:
 *     sdlglmap_viewport_move
 * Description:
 *     Moves the map window in given direction, if possible.
 *     0, 1, 2, 3: north/east/south/west
 * Input:
 *     pmap *: For this map
 *     dir:    Direction to move window to.
 */
void sdlglmap_viewport_move(SDLGLMAP_INFO_T *pmap, int dir)
{
    switch(dir)
    {
        case 0:
            /* Up */
            if(pmap->vp.y > 0)
            {

                pmap->vp.y--;
            }
            break;

        case 1:
            /* Right */
            if(pmap->vp.x < (pmap->size_w - pmap->vp.w))
            {

                pmap->vp.x++;
            }
            break;

        case 2:
            /* Down */
            if(pmap->vp.y < (pmap->size_h - pmap->vp.h))
            {
                pmap->vp.y++;
            }
            break;

        case 3:
            /* Left */
            if(pmap->vp.x > 0)
            {
                pmap->vp.x--;
            }
            break;
    }
}

/*
 * Name:
 *     sdlglmap_viewport_zoom
 * Description:
 *     Does a zoom on the map window, if possible.
 * Input:
 *     zoom: Value for kind of SDLGLMAP_ZOOM*
 */
void sdlglmap_viewport_zoom(SDLGLMAP_INFO_T *pmap, int zoom)
{
    int check_size;


    switch(zoom)
    {
        case SDLGLMAP_ZOOMIN:
            check_size = (pmap->vp.min_w * 2);

            if(pmap->vp.w >= check_size)
            {
                pmap->vp.w /= 2;
                pmap->vp.h /= 2;
            }
            break;

        case SDLGLMAP_ZOOMOUT:
            check_size = (pmap->vp.w * 2);

            if(check_size < pmap->size_w)
            {
                /* FIXME: Support display of whole map */
                pmap->vp.w *= 2;
                pmap->vp.h *= 2;
                /* FIXME: Adjust viewport to map */
            }
            break;

        default:
            /* Invalid command */
            break;
    }
}

/*
 * Name:
 *     sdlglmap_get_pos
 * Description:
 *     Returns a map position from XY
 * Input:
 *    pmap *:      For this map
 *    pos_x,
 *    pos_y:       Get map pos from these x/y corrdinates
 *    in_viewport: Position is relative to viewport, get absolute
 */
int sdlglmap_get_pos(SDLGLMAP_INFO_T *pmap, int pos_x, int pos_y, char in_viewport)
{
    if(in_viewport)
    {
        pos_x += pmap->vp.x;
        pos_y += pmap->vp.y;
    }

    return ((pos_y * pmap->size_w) + pos_x);
}

/*
 * Name:
 *     sdlglmap_get_xy
 * Description:
 *     Create XY-Coordinates from given pos in tile-array.
 *     If 'in_viewport', add the top edge of viewport
 * Input:
 *     pmap *:      For this map
 *     map_pos:     Position in tilelst to get XY-Position for
 *     posxy *:     Pointer on buffer where to return the xy-coordinates
 *     in_viewport: Position is relative to viewport, get absolute
 */
void sdlglmap_get_xy(SDLGLMAP_INFO_T *pmap, int map_pos, SDLGLMAP_XY *posxy, char in_viewport)
{
    posxy->x = map_pos % pmap->size_w;
    posxy->y = map_pos / pmap->size_w;

    if(in_viewport > 0)
    {
        /* ------- Adjust to 'base_pos' ---- */
        posxy->x += pmap->vp.x;
        posxy->y += pmap->vp.y;
    }
}

/*
 * Name:
 *     sdlglmap_get_diffxy
 * Description:
 *     Returns the difference between 'src_pos' and 'dst_pos' in tiles
 * Input:
 *     pmap *:   For this map
 *     src_pos,
 *     dst_pos:  Between this two positions
 *     posxy *:  Pointer on buffer where to return the xy-coordinates
 */
void sdlglmap_get_diffxy(SDLGLMAP_INFO_T *pmap, int src_pos, int dst_pos, SDLGLMAP_XY *pos_xy)
{
    int src_x, src_y, dst_x, dst_y;


    src_x = src_pos % pmap->size_w;
    src_y = src_pos / pmap->size_w;

    dst_x = dst_pos % pmap->size_w;
    dst_y = dst_pos / pmap->size_w;

    pos_xy->x = src_x - dst_x;
    pos_xy->y = src_y - dst_y;
}

/*
 * Name:
 *     sdlglmap_get_dist
 * Description:
 *     Returns the distance X/Y between 'src_pos' and 'dest_pos'
 * Input:
 *     pmap *:   For this map
 *     src_pos:
 *     dest_pos: To calculate the distance for
 * Output:
 *     Moves needed to get to 'dest_pos'
 */
int sdlglmap_get_dist(SDLGLMAP_INFO_T *pmap, int src_pos, int dest_pos)
{
    SDLGLMAP_XY src, dest, distxy;


    sdlglmap_get_xy(pmap, src_pos, &src, 0);
    sdlglmap_get_xy(pmap, dest_pos, &dest, 0);

    distxy.x = dest.x - src.x;
    distxy.y = dest.y - src.y;

    if(distxy.x < 0)
    {
        distxy.x = -distxy.x;
    }

    if(distxy.y < 0)
    {
        distxy.y = -distxy.y;
    }

    return ((distxy.x + distxy.y) - 1);
}

/* ========== Additional functions ========== */

/*
 * Name:
 *     sdlglmap_get_dest_pos
 * Description:
 *     Returns the destination as a position into the map array.
 *     If the position is invalid, -1 is returned
 * Input:
 *     pmap *: For this map
 *     x, y:   Actual position
 *     dir:    Direction of destination
 * Output:
 *     >=0: Position in Map-Array is valid
 */
int sdlglmap_get_dest_pos(SDLGLMAP_INFO_T *pmap, int x, int y, int dir)
{
    int idx;
    int checkw, checkh;
    int dest_pos;


    /* FIXME: Take the different types of maps into account */
    checkw = pmap->size_w;
    checkh = pmap->size_h;

    /* Src is changed to destination, if possible */
    switch(pmap->type)
    {
        case SDLGLMAP_TYPE_STD:
            /* Is a square map          */
            x += AdjacentXY[dir].x;
            y += AdjacentXY[dir].y;   /* Calculate new position   */
            break;

        case SDLGLMAP_TYPE_ISO:          /* Return the position for an Iso-Map   */
            idx = x & 1;
            x += IsoDir[idx][dir].x;
            y += IsoDir[idx][dir].y;
            checkw = pmap->size_w * 2;
            checkh = pmap->size_h * 2;
            break;

        case SDLGLMAP_TYPE_ISO2:
        default:
            /* Set to invalid position  */
            x = -1;
            y = -1;
            break;
    }

    /* Check the position for validity  */
    /* Assume destination invalid    */

    if((x >= 0) && (x < checkw) && (y >= 0) && (y < checkh))
    {
        dest_pos = (y * pmap->size_w) + x;
    }
    else
    {
        /* Otherwise the destination is invalid */
        dest_pos = -1;
    }

    /* Return the position in XY */
    return dest_pos;
}

/*
 * Name:
 *     sdlglmap_get_adjacent
 * Description:
 *     Fills the given list with the numbers of the adjacent positions.
 *     The list must have at least a size of SDLGLMAP_NUMADJACENT
 * Input:
 *     pmap *:   For this map
 *     x, y:     Position to get the adjacent positions for
 *     adjacent: Where to return
 */
void sdlglmap_get_adjacent(SDLGLMAP_INFO_T *pmap, int x, int y, int adjacent[8])
{
    int dir;


    for(dir = 0; dir < 8; dir++)
    {
        adjacent[dir] = sdlglmap_get_dest_pos(pmap, x, y, dir);
    }
}


/*
 * Name:
 *     sdlglmap_pos_get_radiuslist
 * Description:
 *     Returns a pointer on a list with positions (in circle distances)
 *     The end of list is signed with -1.
 * Input:
 *     pmap *:      Descritor of Map to use
 *     x, y:        Position to get list for
 *     radius:      Radius_XY to get list for
 *     ppos_list *: Where to return the positions found
 * Output:
 *     'ppos_list *'
 */
int *sdlglmap_get_radiuslist(SDLGLMAP_INFO_T *pmap, int x, int y, int radius, int *ppos_list)
{
    int count, *plist;
    SDLGLMAP_XY src_pos, dst_pos, *padjlist;


    if(radius > 6)
	{
        radius = 6;
    }

    src_pos.x = x;
    src_pos.y = y;

    padjlist = &Radius_XY[0];
    plist    = ppos_list;

    /* First position is position of caller -- Always valid */
    (*plist) = ((src_pos.y * pmap->size_w) + src_pos.x);
    plist++;

    /* Number of positions for given radius */
    count = RadiusCount[radius];

    while(count > 0)
	{
        dst_pos.x = src_pos.x + padjlist->x;
        dst_pos.y = src_pos.y + padjlist->y;

        /* Now check if position in map */
        if((dst_pos.x >= 0) && (dst_pos.x < pmap->size_w))
		{
            if((dst_pos.y >= 0) && (dst_pos.y < pmap->size_h))
			{
                /* Put only valid positions into the list */
                (*plist) = ((dst_pos.y * pmap->size_w) + dst_pos.x);

                plist++;
            }
        }

        padjlist++;
        count--;
    }

    *plist = -1;

    return ppos_list;
}

/*
 * Name:
 *     sdlglmap_viewport_adjust
 * Description:
 *     Adjusts the viewport so as that given position is always visible in viewport
 * Input:
 *     pmap *:   For this map
 *     pos_x, pos_y: Position to adjust to
 *     pos:        Position to adjust viewport to
 *     center:     If true, center to given position in any case
 *     is_viewpos: Given position is poition in viewport, otherwise a map position
 * Last change:
 *     2017-07-19 bitnapper
 */
void sdlglmap_viewport_adjust(SDLGLMAP_INFO_T *pmap, int pos_x, int pos_y, char center, char is_viewpos)
{
    int xsize, ysize;
    int xdiff, ydiff;


     if(is_viewpos)
     {
        if(pos_x < 2 || pos_x > (pmap->vp.w - 2) || pos_y < 2 || pos_y > (pmap->vp.y - 2))
        {
            center = 1;
        }

        pos_x += pmap->vp.x;
        pos_y += pmap->vp.y;
     }

    if(center)
    {
        sdlglmap_viewport_center(pmap, pos_x, pos_y);
    }
    else
    {
        xsize = pmap->vp.w - 2;   /* Number of tiles horizontally visible */
        ysize = pmap->vp.h - 2;   /* Number of tiles vertically visible   */
        xdiff = pos_x;
        ydiff = pos_y;

        /* Calculate, if we have to center: If position ist too near to the border of  the viewport */
        if((xdiff < 2) || (ydiff < 2) || (xdiff > xsize) || (ydiff > ysize))
        {
            /* Less the two tiles from border of viewport */
            center = 1;
        }
    }
}

/*
 * Name:
 *     sdlglmap_clamp_area
 * Description:
 *     Clamps the area given in 'parea' if it doesn't fit into given 'pmap'
 * Input:
 *     pmap *:    For this map
 *     src_pos:   Where we come from
 *     dest_pos:  Where we move to
 * Output:
 *     General direction leading to 'dest_pos' from 'src_pos'
 */
void sdlglmap_clamp_area(SDLGLMAP_INFO_T *pmap, SDLGLMAP_VIEWPORT_T *parea)
{
    int wadd;


    if(parea->x < 0)
    {
        /* Clamp left */
        parea->w += parea->x;
        parea->x = 0;
    }
    if(parea->y < 0)
    {
        /* Clamp top */
        parea->h += parea->y;
        parea->y = 0;
    }
    wadd = parea->x + parea->w;
    if(wadd > pmap->size_w)
    {
        /* Clamp width */
        parea->w -= (wadd - pmap->size_w);
    }
    wadd = parea->y + parea->h;
    if(parea->y + parea->h > pmap->size_h)
    {
        /* Clamp height */
        parea->h -= (wadd - pmap->size_h);
        parea->w += parea->x;
        parea->x = 0;
    }
}

/*
 * Name:
 *     sdlglmap_get_heading
 * Description:
 *     Calculates the heading from 'src_pos' to 'dest_pos'. Return the general
 *     heading.
 * Input:
 *     pmap *:    For this map
 *     src_pos:   Where we come from
 *     dest_pos:  Where we move to
 * Output:
 *     General direction leading to 'dest_pos' from 'src_pos'
 */
char sdlglmap_get_heading(SDLGLMAP_INFO_T *pmap, int src_pos, int dest_pos)
{
    char heading;
    int  diff;
    SDLGLMAP_XY src, dest;


    sdlglmap_get_xy(pmap, src_pos, &src, 0);
    sdlglmap_get_xy(pmap, dest_pos, &dest, 0);

    heading = 0;
    diff    = dest.y - src.y;

    if(diff < 0)
    {
        heading |= SDLGLMAP_FNORTH;
    }
    else if(diff > 0)
    {
        heading |= SDLGLMAP_FSOUTH;
    }

    diff = dest.x - src.x;

    if(diff < 0)
    {
        heading |= SDLGLMAP_FWEST;
    }
    else if(diff > 0)
    {
        heading |= SDLGLMAP_FEAST;
    }

    return Heading_List[(int)heading];
}

/*
 * Name:
 *     sdlglmap_get_long_heading
 * Description:
 *     Calculates the heading from 'src_pos' to 'dest_pos'. Return the general
 *     heading.
 *     FIXME: Takes distance into account for heading
 *            (using Bresenham's Algorithm)
 * Input:
 *     pmap *:    For this map
 *     src_pos:  Start pos of path (adjusted as
 *     currpos: Current position on path
 *     dest_pos: End position of path
 * Output:
 *     General direction leading to 'dest_pos' from 'src_pos'
 */
char sdlglmap_get_long_heading(SDLGLMAP_INFO_T *pmap, int src_pos, int currpos, int dest_pos)
{
    char heading;
    int  diffx, diffy;
    int  currdx, currdy;
    int  deltax, deltay, newdelta;
    int  partlen;
    SDLGLMAP_XY src, dest, curr;


    sdlglmap_get_xy(pmap, src_pos, &src, 0);    /* Start position X/Y           */
    sdlglmap_get_xy(pmap, dest_pos, &dest, 0);  /* Destination position         */
    sdlglmap_get_xy(pmap, currpos, &curr, 0);  /* Current position on XY-Axis  */

    heading   = 0;
    diffx     = dest.x - src.x;
    diffy     = dest.y - src.y;

    deltax = diffx;
    deltay = diffy;

    if(deltax < 0)
    {
        deltax = -deltax;
    }

    if(deltay < 0)
    {
        deltay = -deltay;
    }

    currdx = curr.x - src.x;
    if(currdx < 0)
    {
        currdx = -currdx;
    }

    currdy = curr.y - src.y;
    if(currdy < 0)
    {
        currdy = -currdy;
    }

    if(deltax > deltay)
    {
        /* Main direction is EAST/WEST  */
        if(diffx < 0)
        {
            heading |= SDLGLMAP_FWEST;
        }
        else if(diffx > 0)
        {
            heading |= SDLGLMAP_FEAST;
        }

        if(currdy < deltay)
        {
            /* Only if difference left, change direction */
            partlen  = deltax / (deltay + 1);
            /* ------ One time change of direction per partlen ---- */
            newdelta = currdx / partlen;
            if(newdelta > currdy)
            {
                /* Take one step sideward... */
                if(diffy < 0)
                {
                    heading |= SDLGLMAP_FNORTH;
                }
                else if(diffy > 0)
                {
                    heading |= SDLGLMAP_FSOUTH;
                }
            } /* if(newdelta > currdy)*/
        } /* if(currdy < deltay) */
    }
    else if(deltax < deltay)
    {
        /* main direction is NORTH / SOUTH */
        if(diffy < 0)
        {
            heading |= SDLGLMAP_FNORTH;
        }
        else if(diffy > 0)
        {
            heading |= SDLGLMAP_FSOUTH;
        }

        if(currdx < deltax)
        {

            /* Only if difference left, change direction */
            partlen  = deltay / (deltax + 1);

            /* ------ One time change of direction per partlen ---- */
            newdelta = currdy / partlen;
            if(newdelta > currdx)
            {
                if(diffx < 0)
                {
                    heading |= SDLGLMAP_FWEST;
                }
                else if(diffx > 0)
                {
                    heading |= SDLGLMAP_FEAST;
                }
            } /* if(newdelta > currdx) */
        } /* if(currdx < deltax)    */
    }
    else
    {
        /* 45 degrees */
        if(diffx < 0)
        {
            heading |= SDLGLMAP_FWEST;
        }
        else if(diffx > 0)
        {
            heading |= SDLGLMAP_FEAST;
        }

        if(diffy < 0)
        {
            heading |= SDLGLMAP_FNORTH;
        }
        else if(diffy > 0)
        {
            heading |= SDLGLMAP_FSOUTH;
        }
    }

    return Heading_List[(int)heading];
}
