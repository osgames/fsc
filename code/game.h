/*******************************************************************************
*  GAME.H                                                                      *
*      - Definitions and data for the main game                                *
*                                                                              *
*  FREE SPACE COLONISATION                                                     *
*      (c)2002 - 2017 Paul Mueller <muellerp61@bluewin.ch>                     *
*                                                                              *
*  This program is free software; you can redistribute it and/or modify        *
*  it under the terms of the GNU General Public License as published by        *
*  the Free Software Foundation; either version 2 of the License, or           *
*  (at your option) any later version.                                         *
*                                                                              *
*  This program is distributed in the hope that it will be useful,             *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of              *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
*  GNU Library General Public License for more details.                        *
*                                                                              *
*  You should have received a copy of the GNU General Public License           *
*  along with this program; if not, write to the Free Software                 *
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  *
*******************************************************************************/

#ifndef _FSC_GAME_H_
#define _FSC_GAME_H_

/*******************************************************************************
* INCLUDES                                                                     *
*******************************************************************************/

#include "msg.h"            /* struct 'MSG_INFO_T'  */
#include "fscfile.h"

/*******************************************************************************
* DEFINES                                				                       *
*******************************************************************************/

#define GAME_NEWGAME     -1
#define GAME_CURRENTGAME  0
#define GAME_SAVEDGAME    1

#define GAME_NAMELEN     32

#define GAME_MAX_STARTPOS (32 + 2)

#define GAME_MAX_ORDER 40
#define GAME_MAX_LIST  200        /* For unit and manage list */
#define GAME_MAX_COLONY 50

/*******************************************************************************
* ENUMS                               								           *
*******************************************************************************/

typedef enum
{
    /* TODO: Add 'negative' message types for special logs */
    MSG_TYPE_NONE   = 0,
    /* ------- Different types of messages for game ------- */
    MSG_TYPE_INFO   = 10,       /* What happened...         */
    MSG_TYPE_GAME   = 11,       /* Start / Load a game      */
    MSG_TYPE_UNIT   = 12,       /* Move a Unit              */
    MSG_TYPE_COLONY = 13,       /* For AI: Manage colony    */
    MSG_TYPE_DIPL   = 14        /* Diplomacy info           */
}
E_MSG_TYPE;

/* -- Commands to be sent to the game */
typedef enum
{
    /* == Less then zero: Commands for the frontend == */
    GCMD_NEWMAP     = -5,   /* GUI: Generate a new map       */
    GCMD_VIEW_MODE  = -4,   /* GUI: Switch between view/move mode (cursor)   */
    GCMD_CHOOSE_MSG = -3,   /* GUI: Click on message, jump to event position */
    GCMD_MENU       = -2,   /* GUI: Jump back to menu screen */
    GCMD_ADVISOR    = -1,   /* GUI: Open an advisor screen   */
    /* == Bigger then zero: Commands for the game engine ==  */
    GCMD_ENDTURN = 1,
    GCMD_VIEWPORT_MOVE,     /* Move the viewport on map     */
    GCMD_VIEWPORT_ZOOM,     /* Zoom viewport in an out      */
    GCMD_VIEWPORT_CENTER,   /* Click on minmap (center) viewport    */
    GCMD_VIEWPORT_CHOOSE,   /* Left click on map, choose something  */
    GCMD_VIEWPORT_USE,      /* Right click on map, use something    */
    GCMD_MAP_CHOOSE,
    GCMD_UNIT_CHOOSE,
    GCMD_UNIT_MOVEDIR,      /* Move in given direction              */
    GCMD_UNIT_MOVEGOTO,     /* Go to chosen field                   */
    GCMD_UNIT_SKIP,
    GCMD_UNIT_WAIT,
    GCMD_UNIT_ORDER,
    GCMD_CHOOSE_PLANET,
    GCMD_USE_PLANET,
    GCMD_CHOOSE_STAR
}
E_GAME_CMD;

/* -- Results to translate from the game -- */
typedef enum
{
    GRES_EXIT_SCREEN = -2,
    MSG_EXIT_PROGRAM = -1,
    GRES_NEW_GAME = 1,
    GRES_START_GAME  = 2,
    GRES_BEGIN_TURN,
    GRES_END_TURN,
    /* ---------- Unit-Messages --------- */
    GRES_UNIT_MOVED       = 101,
    MSG_UNIT_STUCKMAP     = 102,
    MSG_UNIT_STUCKRANGE   = 103,
    MSG_UNIT_NOMOVESLEFT  = 104,
    MSG_UNIT_WRONGOWNER   = 105,
    MSG_UNIT_SETFOCUS     = 106,
    GRES_UNIT_COLONIZED   = 107,
    MSG_UNIT_DONE         = 108,    /* If changed to_no outpost or built one */
    MSG_UNIT_OUTPOSTBUILT = 109,
    /* ---------- Colony-Messages ------- */
    GMSG_IMPROVEBUILT     = 201,
    GMSG_UNITBUILT        = 202,
    GMSG_NOGROW            = 204,
    GMSG_FAMINE            = 205,
    GMSG_POPULATION_GROWN  = 206,    /* A colonys population has grown */
    GRES_RESEARCHDONE      = 210,
    /* ------- Game-Messages ------------ */
    MSG_GAME_SAVEGAMELIST    = 501,
    MSG_GAME_NEWGAME         = 502,
    MSG_GAME_LOADGAME        = 503,
    MSG_GAME_LOADCURRENTGAME = 504,
    MSG_GAME_SAVEGAME        = 505,
    MSG_GAME_NEWMAP          = 506,
    MSG_GAME_ENDTURN         = 507,
    /* ------- Other messages ------------ */
    MSG_OTHER_ALIENSEEN    = 701,        /* There is an alien in vision range */
    MSG_PLANET_TERRAFORMED = 702,
    MSG_OUTPOST_BUILT      = 703,
    GMSG_RESEARCHDONE      = 704,
    MSG_RESEARCHDONEFIRST  = 705,
    /* ---- Diplomacy messages */
    MSG_DIPL_START        = 901,
    MSG_DIPL_NEGOTIATE    = 902,
    MSG_DIPL_DONE         = 903,
    /* --------- Log messages ------- */
    MSG_GOAL_TECH         = 2000,
    MSG_GOAL_IMPROVE      = 2001,
    MSG_GOAL_UNITTYPE     = 2002,
    MSG_GOAL_COLONY       = 2003,
    MSG_ORDER_UNIT        = 2004
}
E_GAME_RESULT;


/*******************************************************************************
* TYPEDEFS                                				                       *
*******************************************************************************/

typedef struct
{
    /* ------------ Data about the size of the galaxy -------------------- */
    int seed;           /* Random seed to use for generation of this map   */
                        /* if 0, a random seed is generated as the map is  */
                        /* created                                         */
    int secsize_w,
        secsize_h;      /* In sectors					                   */
    int tiles_sector;   /* For handling of sectors                         */
    int num_player;     /* Number of start positons                        */
    /* ------------ Data used while for map generation ------------------- */
    int habit_chance;   /* The chance in percent that a planetary system   */
                        /* holds a habitable planet at all                 */
    int difficulty;     /* For the game                                    */
    int star_density;   /* 0..2: scattered, standard, dense                */
    int viewport_w,
        viewport_h;     /* Size of actual viewport (minimum size)          */
    int start_pos_list[GAME_MAX_STARTPOS];

    /* Number of game to load, if any ------------------------------------- */
    int  game_no;
    int  map_w, map_h;  /* Size of generated map, for caller                */
    char test_mode;
}
GAME_STARTINFO_T;

typedef struct
{
    /* ------------- Additional data for saving (info) -------------------- */
    char game_name[20];
    /* All settings for this game */
    /* ------------ Game data settings ------------------------------------ */
    char difficulty;    /* How difficult it is for the human player, percent*/
    /* ------------ Gameplay options ------------- */
    char game_options;
    /* 0: auto_turn, 1: auto_build 2: auto_save 3: endturn_skipmovesleft
       4: galactic_news 5: watch_autopilotships 6: watch_alienships 7: show_shipdamage
    */
    /* ------------ Display options --------------- */
    char display_options;
    /* 0: show_shipbattles 1: explosions 2: draw_grid 3: build_starbaseprompt
       4: upgrade_starbaseprompt 5: show_ranges 6: show_owner
    */
    /* ------------ Sound options ----------------- */
    char sound_options;
    /* 0: sound 1: music 2: event_music 3: soundfx */
    char soundvolume;
    char musicvolume;
    char event_musivolume;
    char soundfxvolume;
}
GAME_SETTINGS_T;

typedef struct
{
    int  unit_pos;      /* Map position of chosen unit      */
    char name[16 + 1];  /* Name of unit */
    int  hp[2];         /* UI_VALUE_ACT ..._FULL Unit_HPStr             */
    int  moves[2];      /* UI_VALUE_ACT ..._FULL                        */
    char type;          /* Type of this unit, for AI -- Unit_ClassName  */
                        /* Number of icon for display                   */
    int attack,
        defense;        /* Attack and defense values                    */
    int ability;        /* UNIT_AIROLE_* saves some func calls  */
                        /* Class of ship                        */
    int range_no;       /* To save some call while playing      */
                        /* The range of the ship: 0 / 1 / 2     */
                        /* RULES_RANGE_*                        */
    int order;          /* Actual order                         */
    int cargo_type, cargo_load;
    int level;
    /* Has to be filled, if planet is chosen with unit */
    char special_orders[10];
    int  pos_x, pos_y;  /* Map x/y position of chosen unit       */
}
GAME_UNITINFO_T;

/* Game info for players */
typedef struct
{
    char ai_control;                    /* Is controlled by AI             */
    char player_no;                     /* Leader handles this nation      */
    /* -- Chosen things -- */
    int act_planet;                    /* Number of planet, if chosen     */
    int act_colony;
    int act_unit;
    int act_mappos;
    /* Info about actual chosen unit in 'list[] for display and management */
    /* ---------- Values of actual chosen unit itself ----------------- */
    GAME_UNITINFO_T act_unit_info;
    /* ------ Info about what can be built, based on actual research ----- */
    char imp_avail_bits[32];        /* All IMPROVE-ments available as bits */
    int  num_ut_avail;
    char ut_avail[64];              /* Unit-Types available for building   */
    /* ------- For management screens and display of planets ------------- */
    int home_pos;               /* Position of home planet                 */
    char dipl_you;              /* Other nation in diplomacy negotiation   */
    /* -- Other info for human player -- */
    int  sector_x, sector_y; /* Number of sector, in case it's not named   */
    char sectorname[32];     /* Name of actual chosen sector, if named     */
    int  money;              /* Money the player has                       */
    int  population;         /* The total poulation of all colonies        */
    /* === Other info === */
    int turn_no;                        /* Number of actual turn           */
    /* For AI: Counter for Unit management -- */
    int unit_manage_count;
}
GAME_PLAYINFO_T;


/* @TODO: Get these from GAME_MNG_T
    // Improvements as numbers
    int  num_imp_avail;
    char imp_avail[256];
    // Units available for building
    int  num_ut_avail;
    char ut_avail[128];
*/

/*******************************************************************************
* CODE                                   				                       *
*******************************************************************************/

int game_start(int game_no, GAME_STARTINFO_T *pinfo);

/* ========== Game basic player control functions ========= */

/* int  game_get_msg(MSG_INFO_T *pmsg, char is_human); */
void game_send_input(int is_human, E_GAME_CMD cmd_no, int sub_cmd_no, int arg1, int arg2);
void game_main(void);

/* ========== Game additional functions =================== */
void game_get_playinfo(GAME_PLAYINFO_T *pinfo);
void game_get_unitinfo(GAME_UNITINFO_T *punitinfo);
int  game_get_msg(MSG_INFO_T *pmsg, char is_human);
/* void gameSettings(GAME_SETTINGS_T *pgamedata, int set_it); */

void game_start_turn(GAME_PLAYINFO_T *pinfo);
void game_endturn(char player_no);
void game_nextturn(void);

/* ========== Other functions ========== */
void game_get_date(char *pbuffer);
char *game_get_version(void);
char *gameDebugInfo(char *buffer);

/* ============= Load and save data of game ============= */
int game_get_loadsave_info(FSCFILE_DATADESC_T *pdatadesc, char save);

#endif  /* _FSC_GAME_H_ */
