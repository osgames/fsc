/*******************************************************************************
*  RULES.C                                                                     *
*	    - Data and functions for the games rules                       	       *
*                                                                              *
*   FREE SPACE COLONISATION                                                    *
*      Copyright (C) 2002-2017  Paul Müller <muellerp61@bluewin.ch>            *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*******************************************************************************/

/*******************************************************************************
* INCLUDES								                                       *
*******************************************************************************/

#include <stdio.h>          /* sprintf()        */


#include "fscshare.h"       /* YIELD_*          */
#include "fsctool.h"
#include "unittype.h"       /* UNIT_ABILITY_*   */


#include "rules.h"

/*******************************************************************************
* TYPEDEFS								                                       *
*******************************************************************************/

typedef struct
{
    /* Struct for cosmic values */
    int  startmoney;        /* Money the player starts with                  */
    int  maxdebt;           /* Beyond this point, every building is  stopped */
    char baseranges[RULES_RANGE_MAX]; /* Base ranges for short, middle and long range */
                                      /* Taken from stars / starbases / outposts      */
                                      /* FIXME: Take from "Shipyard" instead of star  */
                                      /* And the ranges for influence and borders     */
    /* ----------- Basic buildings created on each planet at start of game   */
    /* Maximum of ten buildings                      */
    char basicbuild[11 * 4];
    char maxgrowth;
    char start_popbase;
    int  basic_units[6]; /* 0-terminated list of units with given         */
                            /* abilities to start with. Max. 5 units         */

    char startproject[2 * 4];/* Which projects to start with, military, social */
}
COSMIC_T;

/*******************************************************************************
* DATA   								                                       *
*******************************************************************************/

static COSMIC_T Cosmic =
{
    50,             /* Start money                                          */
    -20,            /* Maximum debt                                         */
    {
        2, 3, 4,    /* Base ranges for short, middle and long range units   */
        3           /* Basic ranges for 'influence'                         */
    },
    {
        /* Basic buildings for game start planet (colony)       */
        "GoB ShY DeR \0"
    },
    40,             /* Foodbox size per citizen point                       */
    17,             /* Base and random part size of planet for game start   */
                    /* Abilities for first units to start game with         */
    { UNIT_ABILITY_SCOUT, UNIT_ABILITY_COLONIZE, UNIT_ABILITY_CONSTRUCT },
    { "Con SoE" }   /* Military, Social                                     */
};

/*******************************************************************************
* CODE   								                                       *
*******************************************************************************/

/* ========================================================================== */
/* ========================== PUBLIC FUNCTIONS ============================== */
/* ========================================================================== */

/*
 * Name:
 *     rulesPopulationGrowth
 * Description:
 *     Grows the given population by the game basic value, taking the
 *     given food and morale into account.
 *     People shrinks if not fed enough.
 * Input:
 *     population *: Pointer on actual population to adjust
 *     foodearned:   In points,  Point needed per 100 Millon people
 *     growth:       In tenth of percent
 * Output:
 *     Number of population to add to actual population
 */
int rulesPopulationGrowth(int *population, int foodearned, char growth)
{
    int foodoverflow;
    int result;


    foodearned *= 100;                          /* For each people      */
    foodoverflow = foodearned - (*population);  /* Calc the difference  */

    if(*population < 100) {

        return 0;                               /* Don't shrink anymore */

    }

    if(foodoverflow < 0) {

        /* There is famine on this planet. Reduce population  */
        foodoverflow /= 3;          /* A third of lack of food  */
        result = foodoverflow;      /* Population shrinks       */

    }
    else if(foodoverflow > 0) {

        /* There is growth on this planet   */
        growth += (char)(foodoverflow / 100); /* +0.1 percent per overflow */
        result = (*population) * (int)growth;
        result /= 1000;                      /* Growth in tenth of percent */

    }
    else {

        return 0;                            /* No change        */

    }

    (*population) += result;
    /* Set ceiling to maximum of food */
    if((*population) > foodearned) {

        (*population) = foodearned;

    }

    return result;

}

/*
 * Name:
 *     rules_get_base_ranges
 * Description:
 *     Returns the basic ranges for a player
 * Input:
 *     pranges *:   Wherer to turn the ranges
 */
void rules_get_base_ranges(char *pranges)
{
    int i;


    for(i = 0; i < RULES_RANGE_MAX; i++)
    {
        pranges[i] = Cosmic.baseranges[i];
    }
}

/*
 * Name:
 *     rulesGetInfluence
 * Description:
 *     Returns the influence of a star system bases on the summed population
 *     of all planets.
 *     TODO: Take bonuses into account
 * Input:
 *     population: Population of planet
 * Output:
 *     Influence points this planet generates
 */
char rulesGetInfluence(int population)
{

    return (char)(population / 100);

}

/*
 * Name:
 *     rulesPlanetMorale
 * Description:
 *     Returns the morale for this planet, basing on the given arguments
 * Input:
 *     morale:        Actual morale
 *     moralebonus:   Bonus for morale in percent
 * Output:
 *     morale: Morale for this planet --> Approval rating ?!
 */
char rulesPlanetMorale(char morale, int moralebonus, int approval)
{

    int newmorale;


    newmorale = (morale * moralebonus / 100) + morale;
    approval -= 50;

    if(approval > 0) {

        newmorale += approval;

    }

    if(newmorale < 50) {

        newmorale = 50;            /* For test purposes */

    }
    else if(newmorale >= 100)
    {
        newmorale = 100;
    }

    return (char)newmorale;

}

/*
 * Name:
 *     rules_calc_damage
 * Description:
 *     Calcs the damage calculated by given 'attack' and 'defense'
 * Input:
 *     attack:  Attack value of attacker
 *     defense: Defense value of defender
 * Output:
 *     damage:
 */
char rules_calc_damage(char attack, char defense)
{
    int attval, defval, damage;

    /* FIXME: Create better calculation */
    attval = fsctool_rand_no(attack + 1) - 1;
    defval = fsctool_rand_no(defense + 1) - 1;
    damage = defval - attval;

    if(damage <= 0)
    {
        damage = 0;
    }

    return (char)(damage);
}

/*
 * Name:
 *     rulesCalcAttackChance
 * Description:
 *     Calcs the attack chance
 *     Calcs the damage calculated by given 'attack' and 'defense'
 * Input:
 *     attack:  Attack value of attacker
 *     defense: Defense value of defender
 * Output:
 *     Chance for attack to succeed. The bigger, the better
 */
int rulesCalcAttackChance(char attack, char defense)
{

    /* FIXME: Return a percent value */
    return (attack - defense);

}

/*
 * Name:
 *     rules_get_basic_buildings
 * Description:
 *     Returns a 0-terminated list of numbers of buildings to be built as the
 *     first planet is settled at start of a game
 * Input:
 *     None
 * Output:
 *     Pointer on a 0-terminated list of building signs with size 4.
 */
char *rules_get_basic_buildings(void)
{
    return &Cosmic.basicbuild[0];
}

/*
 * Name:
 *     rules_start_population
 * Description:
 *     Returns the start population for planet at game start
 * Input:
 *     None
 * Output:
 *     Start population as points
 */
int rules_start_population(char difficulty)
{
    return (Cosmic.start_popbase - difficulty);
}

/*
 * Name:
 *     rulesStartUnitAbilities
 * Description:
 *     Returns a 0-terminated list with abilities of units
 * Input:
 *     None
 */
int *rulesStartUnitAbilities(void)
{
    return &Cosmic.basic_units[0];
}

/*
 * Name:
 *     rules_get_planetvalue
 * Description:
 *     Returns the 'value' of a planet, using the values in 'yieldweight' as
 *     modifiers for the planets basic values
 * Input:
 *     maxworkers:     Maximum workers for this planet
 *     pyieldval *:    Pointer on yield values of planet
 *     pyieldweight *: Weight of each yield
 * Output:
 *     Weighted value
 */
int rules_get_planetvalue(char maxworkers, char *pyieldval, char *pyieldweight)
{
    int result;
    int i;


    /* Assume Gas core: Has always no value */
    result = 0;

    if(maxworkers > 0)
    {
        for (i = 0; i < GVAL_PLANETYIELD_MAX; i++)
        {
            result += (int)pyieldval[i] * (int)pyieldweight[i] * (int)maxworkers;
        }
    }

    return result;
}

/*
 * Name
 *     rules_get_startproject
 * Descriptions:
 *     Returns the start projects for planet settlement
 * Input:
 *     which: Which project to get (0: military)
 */
char *rules_get_startproject(int which)
{
    if(which < 2)
    {
        return &Cosmic.startproject[which * 4];
    }
    else
    {
        return "nil";
    }
}
