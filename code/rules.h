/*******************************************************************************
*  RULES.H                                                                     *
*	    - Data and functions for the games rules                       	       *
*                                                                              *
*	FREE SPACE COLONISATION               	                                   *
*   Copyright (C) 2002  Paul Mueller <pmtech@swissonline.ch>                   *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*******************************************************************************/

#ifndef _FSC_RULES_H_
#define _FSC_RULES_H_

/*******************************************************************************
* DEFINES 								                                       *
*******************************************************************************/

#define RULES_RANGE_SHORT       0       /* Range for short ranged ships */
#define RULES_RANGE_MIDDLE      1       /* Range for middle range ships */
#define RULES_RANGE_LONG        2       /* Range for long range ships   */
#define RULES_RANGE_INFLUENCE   3       /* Range for influence          */

#define RULES_RANGE_MAX         4       /* Ranges for all types         */
#define RULES_RANGE_MOVE        3       /* Number of movement ranges    */

/*******************************************************************************
* CODE  								                                       *
*******************************************************************************/

/* ------- Calculation of planetary values ----- */
int  rulesPopulationGrowth(int *population, int foodearned, char growth);
void rules_get_base_ranges(char *ranges);
char rulesGetInfluence(int population);
char rulesPlanetMorale(char morale, int moralebonus, int approval);
char rules_calc_damage(char attack, char defense);
int  rulesCalcAttackChance(char attack, char defense);
char *rules_get_basic_buildings(void);
int  rules_start_population(char difficulty);
int  *rulesStartUnitAbilities(void);
int  rules_get_planetvalue(char maxworkers, char *yieldval, char *yieldweight);
char *rules_get_startproject(int which);

#endif  /* _FSC_RULES_H_ */
