/*******************************************************************************
*  COLONY.H                                                                    *
*	   - Declarations and functions for handling game part of units            *
*                                                                              *
*   FREE SPACE COLONISATION			                                           *
*      (c)2002 - 2017 Paul Mueller <muellerp61@bluewin.ch>                     *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*******************************************************************************/

#ifndef _FSC_COLONY_H_
#define _FSC_COLONY_H_

/*******************************************************************************
* INCLUDES                                                                     *
*******************************************************************************/

#include "fscfile.h"
#include "fscshare.h"       /* GVAL_*      */

/*******************************************************************************
* DEFINES                                                                      *
*******************************************************************************/

/* ------ Project types ------- */
#define COLONY_PROJ_MILITARY 0
#define COLONY_PROJ_SOCIAL   1
#define COLONY_PROJ_MAX      2

#define COLONY_LIST_MAX 50

#define COLONY_DEFENSE       1


/* ------ Types of values to do an update on ---------- */
#define COLONY_FMNG_NONE   0x00
#define COLONY_FMNG_VALUES 0x01
#define COLONY_FMNG_PROD   0x02
#define COLONY_FMNG_ALL    0xFF

/*******************************************************************************
* TYPEDEFS                                                                     *
*******************************************************************************/

typedef struct
{
    char which;
    short int invested;
}
COLONY_PROJ_T;

typedef struct
{
    int colony_no;      /* Number of colony                     */
    char owner_no;      /* Owner of colony                      */
    int  planet_no;     /* Number of planet the colony is on    */
    /* --------- Basic values --------------------------------- */
    char pop_points;    /* Size of colony: Number of workers    */
                        /* Calculate population based on points */
                        /* Settle ships take at least one
                           population with them.
                           @TODO: Load more people (max. 3)
                           #RULES
                           */
    char pop_idle;      /* Number of Poulation which do not work    */
    char workers_yield; /* Maximum workers per yield                */
                        /* For any yield, all citizens can work     */
                        /* to the limit 'workersyield'              */
                        /* Allways: Planetsize: 2 => 4 .. 8 .. 12   */
    /* ---------- Workers per yield (points) ------------------------------ */
    char prod_use[GVAL_COLSHARE_MAX];   /* Workers set per production       */
    int  prod_val[GVAL_COLSHARE_MAX];   /* Value per production       */
    int  prod_total[GVAL_MAX];          /* 'prod_use' * 'prod_val'    */
    COLONY_PROJ_T proj[COLONY_PROJ_MAX];
    int proj_turns_left[COLONY_PROJ_MAX];
    char imp_avail[127];                /* What can be built ? */
    /* ------- Actual production points --------------------------- */
    int  map_pos;                   /* Position of colony on map    */
    /* Info for human player and ai */
    char nation_name[20];
    char planet_name[20];
    char planet_sign[2];
    int  planet_yield[3];           /* For changing it with upgrade */
    short int population;
    char morale;
    int expenses;
    int netincome;
    /* == Other info about the colony == */
    int  num_imp_built;          /* Number of improvements already built */
    char improve_built[127];     /* Numbers of the improvements already built */
    int  num_imp_avail;
    char improve_avail[127];     /* Number of improvements, that can be chosen */
}
COLONY_INFO_T;

/*******************************************************************************
* CODE                                                                         *
*******************************************************************************/

void colony_init(void);

/* ==== Get information ==== */
void colony_get_info(int colony_no, COLONY_INFO_T *pinfo, int dir);
int  colony_get_planetno(int colony_no);
int  colony_get_list(char player_no, int *plist);

/* ============= Handle colony =============== */
int  colony_settle_planet(char player_no, int planet_no, char pop_points);
void colony_build_unit(int colony_no, int map_pos, int unit_ability);
void colony_build_improve(int colony_no, char key_code[4]);
void colony_add_bonus(int colony_no, FSC_GVAL_TYPES val_type, char amount);
void colony_calc_yielduse(COLONY_INFO_T *pinfo);

/* void colonyArrangeProduction(int colony_no, char *yield_weight); */

/* ============= Help for AI ============================== */
int colonyProduction(int colony_no, int proj_type, char ability);

/* int colonyBonus(int colony_no, int which, int multiplier); */
int colony_value(int colony_no, char *owner, int count_capital);

/* ============= Other functions =========== */
void colony_endturn(int *pstats);

/* ============= Load and save data of colony ============= */
int colony_get_loadsave_info(FSCFILE_DATADESC_T *pdatadesc, char save);

#endif  /* _FSC_COLONY_H_ */
