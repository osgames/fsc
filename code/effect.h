/*******************************************************************************
*  EFFECT.H                                                                    *
*	        - Definitions of effects for the game                   	       *
*                                                                              *
*   FREE SPACE COLONISATION			                                           *
*      (c)2002 - 2017 Paul Mueller <muellerp61@bluewin.ch>                     *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*******************************************************************************/

#ifndef _FSC_EFFECT_H_
#define _FSC_EFFECT_H_

/*******************************************************************************
* INCLUDES                                                                     *
*******************************************************************************/

#include "fscmap.h"     /* FSCMAP_POSINFO_T */

/*******************************************************************************
* ENUMS                                                                        *
*******************************************************************************/

typedef enum
{
    GER_NONE      = 0,      /* No range, invalid            */
    GER_BUILDING  = 1,      /* Affects a building           */
    GER_COLONY    = 2,      /* Affects a colony             */
    GER_PLANET    = 3,      /* Affects a planet             */
    GER_PLAYER    = 4,      /* Affects the player           */
    GER_GALAXY    = 5,      /* Affects the galaxy           */
    GER_UNIT      = 6,      /* Affects ships                */
    GER_MAP       = 7,      /* Affects the map              */
    GER_OUTPOST   = 8,      /* Affects outpost on planet    */
    GER_AIINFO    = 9       /* Info for the AI for usage    */
}
E_GAME_EFFECT_RANGE;

/*******************************************************************************
* TYPEDEFS                                                                     *
*******************************************************************************/

typedef struct
{
    char range;
    char type;              /* GVAL_*                                  */
    char amount;            /* > 0 This is the amount of bonus in percent
                        	  -1: "info" holds the sign of the unit
                                  (outpost) to build
                            */
    char info[4];           /* Three chars + 0 as int     char info[4]; */
                            /* For signs: - for prerequisite buildings
                                          - for units (outposts) to build
                            */
}
GAME_EFFECT_T;

/*******************************************************************************
* CODE                                                                         *
*******************************************************************************/

void effect_set(GAME_EFFECT_T *pge, FSCMAP_POSINFO_T *ppi, int colony_no, int planet_no);
int  effect_calc_value(GAME_EFFECT_T *pge, char *pweights, int num_effect);

int  effect_get_by_keycode(GAME_EFFECT_T *plist, char key_code[4], GAME_EFFECT_T *pge, int max_effect);
int  effect_in_list(GAME_EFFECT_T *gpe, char key_code[4]);


#endif /* _FSC_EFFECT_H_ */
