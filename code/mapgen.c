/*******************************************************************************
*  MAPGEN.C                                                                    *
*      - Declarations and functions for generation of the map                  *
*                                                                              *
*   FREE SPACE COLONISATION			                                           *
*      (c)2002 - 2017 Paul Mueller <muellerp61@bluewin.ch>                     *
*                                                                              *
*  This program is free software; you can redistribute it and/or modify        *
*  it under the terms of the GNU General Public License as published by        *
*  the Free Software Foundation; either version 2 of the License, or           *
*  (at your option) any later version.                                         *
*                                                                              *
*  This program is distributed in the hope that it will be useful,             *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of              *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
*  GNU Library General Public License for more details.                        *
*                                                                              *
*  You should have received a copy of the GNU General Public License           *
*  along with this program; if not, write to the Free Software                 *
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  *
*******************************************************************************/

/*******************************************************************************
* INCLUDES								                                       *
*******************************************************************************/

#include <stdio.h>      /* sprintf()        */
#include <string.h>     /* strcpy()         */


/* ------ Own headers ---- */
#include "fscmap.h"
#include "fscshare.h"   /* General defines      */
#include "fsctool.h"    /* fsctool_rand_no()    */
#include "starsys.h"    /* starsys_create_one() */


/* -- Own header -- */
#include "mapgen.h"


/*******************************************************************************
* DATA								                                           *
*******************************************************************************/

#if 0
static PLANET_INITDEF_T Start_Systems[] =
{
   /* Define "Sol" System for first player */
   { '1', "Sol", STARSYS_YELLOWSTAR },
   { 'Y',               /* PLANET_TYPE_T as char        */
     "Mercury",         /* Name of planet 	            */
     7,                 /* Size, Gas core: Ignored	    */
     { 16, 8,  4 },
     3                  /* Number of the icon [1..17]   */
   },
   { 'Y', "Venus",    14, { 14, 8,   9 }, 8  },
   { 'M', "Earth",    17, { 22, 18, 25 }, 2  },
   { 'G', "Mars",     12, { 12,  9,  8 }, 7  },
   { 'B', "Jupiter",   0, {  0,  0,  0 }, 15 },
   { 0 }
};
#endif

static GAME_STARTINFO_T Default_Game =
{
    314528,     /* seed                                     */
    3, 3,       /* Width and height in sectors (of size 12) */
    12,         /* For handling of sectors                  */
    8,          /* Number of start positons (players)       */
    5,          /* The chance in 10 percent that a planetary system */
                /* holds a habitable planet at all                  */
    0,          /* the bigger, the harder [0 .. 6]                 */
    2,          /* 0..2: scattered, standard, dense                */
    16, 12      /* Size of actual viewport (minimum size)          */
};

/*******************************************************************************
* CODES								                                       *
*******************************************************************************/

/*
 * Name:
 *     mapgen_pos_exists
 * Description:
 *     Returns 1, if given position is found in given list
 * Input:
 *     plist *: Pointer on list
 *     pos:     Number of position
 *     num_el:  Number of elements in list
 */
static int mapgen_pos_exists(int *plist, int pos, int num_el)
{
    int i;


    for(i = 0; i < num_el; i++)
    {
        if (plist[i] == pos)
        {
            return 1;
        }
    }

    /* Position not found in list */
    return 0;
}

/*
 * Name:
 *     mapgen_poslist
 * Description:
 *     Creates random positions using the given map size values.
 *     The start positions are generated at the start of the list
 *     Because the stars must have at least one tile distance
 *     in between, the positions are generated with half the
 *     size of the map
 * Input:
 *     pgsi *:      Pointer on info to use for map generation
 *     num_star:    Number of stars/ppos_list to generate
 *     ppos_list *: Where to return the map positions
 * Output:
 *     Number of positions generated
 */
static int mapgen_poslist(GAME_STARTINFO_T *pgsi, int num_star, int *ppos_list)
{
    static char yield_weight[3] = { 3, 1, 1 };     /* Weight on food! */

    FSCMAP_POSINFO_T pi;
    int map_x, map_y;
    int part_w, part_h, sub_w, sub_h;
    int pos_count;
    int act_pos;
    int i;
    int size_w, size_h;
    int home_planet;


    size_w = pgsi->secsize_w * pgsi->tiles_sector;
    size_h = pgsi->secsize_h * pgsi->tiles_sector;

    /* Create a grid of start positions */;
    part_w = size_w / 4;
    part_h = size_h / 4;
    sub_w  = (size_h / 8) - 1;
    sub_h  = (size_h / 8) - 2;

    /* Number of positions in list */
    pi.owner  = 0;
    pi.range  = 0;
    pos_count = 0;

    for(i = 1; i <= pgsi->num_player; i++)
    {
        switch(i)
        {
            case 1:
                map_x = part_w;
                map_y = part_h;
                break;

            case 2:
                map_x = part_w * 3;
                map_y = part_h * 3;
                break;

            case 3:
                map_x = part_w * 3;
                map_y = part_h;
                break;

            case 4:
                map_x = part_w;
                map_y = part_h * 3;
                break;

            case 5:
                map_x = part_w * 2;
                map_y = part_h;
                break;

            case 6:
                map_x = part_w * 3;
                map_y = part_h * 2;
                break;

            case 7:
                map_x = part_w;
                map_y = part_h * 2;
                break;

            case 8:
                map_x = part_w * 2;
                map_y = part_h * 3;
                break;
        }

        /* First random position in grid */
        map_x += (fsctool_rand_no(part_w) - sub_w);
        map_y += (fsctool_rand_no(part_h) - sub_h);

        pi.x = (char)map_x;
        pi.y = (char)map_y;

        act_pos = fscmap_pos_fromxy(map_x, map_y, 0);

        ppos_list[i] = act_pos;

        /* @TODO: Generate Star with planets */
        fscmap_add_star(&pi, i);

        /* Set 'home' position */
        pgsi->start_pos_list[i] = act_pos;

        printf("map_pos[%d]: %d\n", i, act_pos);

        pos_count++;
    }


    for(i = 0; i < num_star; i++)
    {
        /* Other positions, generate positions in array */
        do
        {
            /* At minimum 1 tile away from map border */
            map_x = 1 + (fsctool_rand_no(size_w - 3));
            map_y = 1 + (fsctool_rand_no(size_h - 3));
            act_pos = fscmap_pos_fromxy(map_x, map_y, 0);
        }
        while(mapgen_pos_exists(ppos_list, act_pos, pos_count));

        ppos_list[pos_count] = act_pos;

        pi.x = (char)map_x;
        pi.y = (char)map_y;

        fscmap_add_star(&pi, pos_count);

        pos_count++;
    }

    return pos_count;
}


/* ========================================================================== */
/* ======================== PUBLIC FUNCTIONS	============================= */
/* ========================================================================== */

/*
 * Name:
 *      mapgen_new_map
 * Description:
 *      Generates a new game map. It is assumed that the Stars and
 *      planets are initialized to zero.
 *      If pgsi == NULL, then use default game for test purposes
 * Input:
 *     pgsi *: Pointer on 'info' which holds the info to use for map creation
 *
 */
void mapgen_new_map(GAME_STARTINFO_T *pgsi)
{
    int pos_list[STARSYS_MAX_STAR + 2];
    int i;
    int num_star;


    if(!pgsi)
    {
        /* Create a default game for test purposes */
        pgsi = &Default_Game;
    }

    /** ==== Initialize random generator ==== **/
    fsctool_rand_set_seed(pgsi->seed);

    /* ---------- Set basic data for filling ----------- */
    starsys_init();
    fscmap_create(pgsi);

    /* === Star-Density: 0: 3, 1: 5, 2: 7 per sector === */
    num_star = pgsi->secsize_w * pgsi->secsize_h * ((pgsi->star_density * 2) + 3);
    mapgen_poslist(pgsi, num_star, pos_list);

    /* Copy the number start positions into the 'MAPGEN'-Info */
    for(i = 0; i < pgsi->num_player; i++)
    {
        pgsi->start_pos_list[i] = pos_list[i];
    }
}

