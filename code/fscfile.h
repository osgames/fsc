/*******************************************************************************
*  FSCFILE.H                                                                   *
*      - Read and save games                                                   *
*                                                                              *
*   FREE SPACE COLONISATION                                                    *
*       Copyright (C) 2002-2017  Paul Müller <muellerp61@bluewin.ch>           *
*                                                                              *
*  This program is free software; you can redistribute it and/or modify        *
*  it under the terms of the GNU General Public License as published by        *
*  the Free Software Foundation; either version 2 of the License, or           *
*  (at your option) any later version.                                         *
*                                                                              *
*  This program is distributed in the hope that it will be useful,             *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of              *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
*  GNU Library General Public License for more details.                        *
*                                                                              *
*  You should have received a copy of the GNU General Public License           *
*  along with this program; if not, write to the Free Software                 *
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  *
*******************************************************************************/

#ifndef _FSC_FSCFILE_H_
#define _FSC_FSCFILE_H_

/*******************************************************************************
* ENUMS								                                       *
*******************************************************************************/

typedef enum
{
    FSCFILE_NONE    = 0,
    FSCFILE_GAME    = 1,
    FSCFILE_PLAYER  = 2,
    FSCFILE_STAR    = 10,
    FSCFILE_PLANET  = 11,
    FSCFILE_MOON    = 12,
    FSCFILE_OUTPOST = 13,
    FSCFILE_UNIT    = 20,
    FSCFILE_MAPHEAD   = 30,
    FSCFILE_MAPGLOBAL = 31,      /* Global info for all and 'player' Nr. 0   */
    FSCFILE_MAPPLAYER = 32,      /* Players knowledge of map */
    FSCFILE_COLONY    = 40,
    FSCFILE_DIPLINFO  = 50
}
E_FILE_DATANAME;


#define FSCFILE_MAXSAVE    10

/*******************************************************************************
* TYPEDEFS 								                                       *
*******************************************************************************/

/* A pointer on such a struct is to be filled by a procedure for info about */
/* the data to be loaded, saved                                             */
/* Returns a pointer on data buffer to save from/to load to                 */
/* char *[modulename]GetLoadSaveInfo(int *recsize, int *numrec, int save)   */

typedef struct S_DATAHEAD
{
    char data_name;
    short int num_rec;
    int rec_size;
}
FSCFILE_DATAHEAD_T;

typedef struct S_FILEDATA
{
    FSCFILE_DATAHEAD_T rec_info;
    void *pdata;
}
FSCFILE_DATADESC_T;

/*******************************************************************************
* CODE  								                                       *
*******************************************************************************/

void fscfile_set_dir(char *psave_dir, char *plang_dir);
int fscfile_load(int game_no);
int fscfile_save(int game_no);
int fscfile_get_namelist(char *psave_names, int buf_size, int name_len);

#endif  /* FSC_FSCFILE_H_ */
