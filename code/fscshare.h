/*******************************************************************************
*  FSCSHARE.H                                                                  *
*      - Generally shared game definitions and typedefs                        *
*                                                                              *
*  FREE SPACE COLONISATION                                                     *
*       Copyright (C) 2002 - 2017 Paul Mueller <muellerp61@bluewin.ch>         *
*                                                                              *
*  This program is free software; you can redistribute it and/or modify        *
*  it under the terms of the GNU General Public License as published by        *
*  the Free Software Foundation; either version 2 of the License, or           *
*  (at your option) any later version.                                         *
*                                                                              *
*  This program is distributed in the hope that it will be useful,             *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of              *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
*  GNU Library General Public License for more details.                        *
*                                                                              *
*  You should have received a copy of the GNU General Public License           *
*  along with this program; if not, write to the Free Software                 *
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  *
*******************************************************************************/

#ifndef _FSC_SHARE_H_
#define _FSC_SHARE_H_

/*******************************************************************************
* INCLUDES                                                                     *
*******************************************************************************/

#define STARSYS_FHABITABLE ((char)0x01)

#define FSC_NAMELEN_MAX     23       /* Excluding trailing 0                */

#define FSC_STARGRIDSIZE    4        /* Grid in sector for star placement   */

/* ------- Maximums for game data ----------- */
#define FSC_STARTYPE_MAX      6
#define FSC_FREESLOT_ID      -1

/* -------- General game maximums ------------ */
#define FSC_UNITTYPE_MAX     80
#define FSC_IMPROVE_MAX     127
#define FSC_EFFECT_MAX       50
#define FSC_MAJORRACES_MAX    8
#define FSC_LISTLEN_MAX     200

#define FSC_ISKEY(s1, s2) (((s1[0] - s2[0]) | (s1[1] - s2[1]) | (s1[2] - s2[2])) == 0)
#define FSC_SIGNCPY(s1, s2) { (s1[0] = s2[0]); (s1[1] = s2[1]); (s1[2] = s2[2]); }

/*******************************************************************************
* ENUMS                                                                        *
*******************************************************************************/

typedef enum
{
    /*** ====== Planet YIELD values + Bonuses ====== ***/
    GVAL_FOOD      = 0,       /* GVAL_POPGROWTH   = 24 ==> GVAL_FOOD = 0 */
    GVAL_RESOURCES = 1,
    GVAL_ECONOMY   = 2,       /* GVAL_TRADE    = 44 -- ==> GVAL_ECONOMY */
    GVAL_PLANETYIELD_MAX = 3,
    /*** ===== Colony: Use of yields and Weights for using YIELDS for AI ==== ***/
    /** Share of resources production **/
    GVAL_MILITARY  = 4,         /* Bonus: Faster building of ships          */
    GVAL_SOCIAL    = 5,         /* Bonus: Faster Building of improvements   */
    GVAL_STOCK     = 6,
    /** Share of economy production **/
    GVAL_RESEARCH  = 7,         /* Bonus: Better / faster research */
    GVAL_TAXES     = 8,         /* Bonus: More taxes               */
    GVAL_LUXURY    = 9,         /* Bonus: More luxury              */
    GVAL_COLSHARE_MAX = 10,
    /*** ===== Colony + Player: Bonus + fixed values ==== ***/
    GVAL_COLFLAGS  = 10,  /* Flags for special improvement effects     */
    GVAL_MORALE    = 11,  /* Morale: COLONY_T.prod_use[] == Peoples Morale */
                          /* Happyness of people                       */
    GVAL_INFLUENCE = 12,  /* FSCMAP_TILE_T.influence: Force of influence   */
    GVAL_REPAIR    = 13,  /* Bonus: UNIT_T.hp[UI_VALUE_ACT]:
                               Faster repair in space and at colony/outpost  */
    GVAL_SENSORS   = 14,  /* UNIT_T.sensor_range: Farther range        */
    GVAL_ATTACK    = 15,  /* Weight: How much attack do we want/have ? */
                          /* Bonus: UNIT_T.attack                      */
    GVAL_DEFENSE   = 16,  /* How much defens do we want/have ?         */
                          /* Bonus: UNIT_T.defense                     */
    GVAL_HITPOINTS = 17,  /* Bonus: UNIT_T.hp                          */
    GVAL_RANGE     = 18,  /* PLAYER_T.ranges                           */
    GVAL_SPEED     = 19,  /* Bonus: UNIT_T.moves                       */
    /** Special bonuses for later use **/
    GVAL_DIPLOMACY  = 20,
    GVAL_ESPIONAGE  = 21,
    GVAL_ASSIMILATE = 22, /* Assimilation of alien cultures           */
                          /* State of assimilation of people from     */
                          /* conquered planets (in percent)           */
                          /* > 0: Still to assimilate                 */
    GVAL_NAVIGATION = 23,
    GVAL_TERRAFORM  = 24, /* Bonus + AI-Weight                 */
    GVAL_OUTPOST    = 25, /* Bonus + AI-Weight                 */
    GVAL_NETINCOME  = 36,
    GVAL_APPROVAL   = 37,
    GVAL_NUMCOLONY  = 38,
    GVAL_EXPAND     = 39, /* AI-Weight                         */
    GVAL_MAX        = 40  /* Effects, too                             */
}
FSC_GVAL_TYPES;

#endif /* _FSC_SHARE_H_ */
