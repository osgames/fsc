/*******************************************************************************
 * UNITTYPE.H                                                                  *
 *      - Declarations and functions for handling of units (game part)         *
 *                                                                             *
*   FREE SPACE COLONISATION                                                    *
*       Copyright (C) 2002-2017  Paul Müller <muellerp61@bluewin.ch>           *
 *                                                                             *
 *  This program is free software; you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published by       *
 *  the Free Software Foundation; either version 2 of the License, or          *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  This program is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU Library General Public License for more details.                       *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with this program; if not, write to the Free Software                *
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
 ******************************************************************************/

#ifndef _FSC_UNITTYPE_H_
#define _FSC_UNITTYPE_H_

/*******************************************************************************
* INCLUDES                                                                     *
*******************************************************************************/

#include "effect.h"
#include "sdlglcfg.h"

/*******************************************************************************
* DEFINES                                                                      *
*******************************************************************************/

#define UNITTYPE_MAX         72
#define UNITTYPE_MAX_NAMELEN 16

/* -------- Threekinds of ranges ---------- */
#define UNITTYPE_RANGE_SHORT  0
#define UNITTYPE_RANGE_MIDDLE 1
#define UNITTYPE_RANGE_LONG   2

/*******************************************************************************
* TYPEDEFS                                                                     *
*******************************************************************************/

typedef enum
{
    UNIT_ABILITY_UNKNOWN      =  0x00,
    UNIT_ABILITY_MILITARY     =  0x01,
    UNIT_ABILITY_COLONIZE     =  0x02, /* Colonize planets --> SETTLE       */
                                    /* Can build an outpost,                */
                                    /* or a terraform station               */
    UNIT_ABILITY_TRANSPORT    =  0x03, /* Troop transport/transport         */
    UNIT_ABILITY_DIPLOMACY    =  0x04, /* Diplomacy                         */
    UNIT_ABILITY_TRADE        =  0x05, /* Trade                             */
    UNIT_ABILITY_SCOUT        =  0x06, /* Explore Superiority               */
    UNIT_ABILITY_CONSTRUCT    =  0x07, /* Build/extend starbase             */
                                       /* Can build an outpost,             */
                                       /* a archelogoical, a mining         */
                                       /* or a terraform station            */
                                       /* Can build a starbase              */
    /* -------- The following units are stationary units (moves = 0) ------ */
    UNIT_ABILITY_OUTPOST      = 0x09,
    UNIT_ABILITY_STARBASE     = 0x0A,
    UNIT_ABILITY_MAX          = ((char)20)
}
E_UNITTYPE_ABILITY;

typedef struct
{
    char key_code[4];   /* Sign for this unit for choosing by rules  */
    char name[UNITTYPE_MAX_NAMELEN + 1];
    char preq_code[4];  /* Sign of Prerequisite advance              */
    int  cost;          /* Cost * 100 for building this unit-type    */
    char maintenance;   /* Maintenance costs per turn                */
    char ability;       /*
                           AI role (Generally affects the way in
                           which computer players use the unit, but
                           roles >= 5 will actually affect abilities
                           of the unit)
                           UNIT_ABILITY_*
                        */
    char hit,           /* Hit points (damage which can be taken before elimination) */
         att,           /* Attack factor (chance to score hit attacking) */
         def;           /* Defense factor (chance to score hit defending) */

    char moves;         /* Movement rate (tiles per turn)                */
    char range;         /* Range from players planets/starbases/outpots  */
                        /* *_SHORT / *_MIDDLE / *_LONG                   */
    char sensor_range;  /* Range of sensors                              */

    GAME_EFFECT_T effect; /* Effect for AI-Planning and player info        */
}
UNITTYPE_T;

/*******************************************************************************
* CODE                                                                         *
*******************************************************************************/

UNITTYPE_T *unittype_get(int type_no, char key_code[4]);
int unittype_get_available(char *ptech_keycode, int *putypeavail);

const char *unittype_get_name(int type_no);
int unittype_get_ability(int type_no);
int unittype_best_roleunit(int *utlist, int role);

/* --------- Additional help functions for AI ----- */
int unittype_asses_by_cost(int *putypeavail, int max_cost, int *pcost, char att, char def);

/* --------- Info usable for every player --------- */
int unittype_build_time(int type_no, int invested, int build_pts);

int unittype_get_effect(int type_no, GAME_EFFECT_T *pge);

/* ------------ Functions for reading in config data ------ */
int unittype_get_datadesc(SDLGLCFG_RECORD_T *pdatainfo);

#endif  /* _FSC_UNITTYPE_H_ */
