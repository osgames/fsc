/* *****************************************************************************
 * UNITINFO.H -                                                                *
 *      Declarations and functions for frontend handling of units              *
 *                                                                             *
 *  FREE SPACE COLONISATION                                                    *
 *  Copyright (C) 2002  Paul Mueller / pmtech@swissonline.ch                   *
 *                                                                             *
 *  This program is free software; you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published by       *
 *  the Free Software Foundation; either version 2 of the License, or          *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  This program is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU Library General Public License for more details.                       *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with this program; if not, write to the Free Software                *
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
 ******************************************************************************/
#ifndef _FSC_UNITINFO_H_
#define _FSC_UNITINFO_H_

/* *****************************************************************************
* DEFINES                                                                      *
*******************************************************************************/

#define UNITINFO_MAX_NAMELEN  23

#define UI_STANDARD       0
#define UI_PLANETSUPPORT  1       /* For city investigation */
#define UI_PLANETPRESENT  2

/* -------- For cargo of transporters ------ */
#define UI_CARGO_NONE     0x01
#define UI_CARGO_PEOPLE   0x02
#define UI_CARGO_MONEY    0x03 /* From economy */
#define UI_CARGO_MATERIAL 0x04 /* From stock   */

/* ------ For values that may change ------ */
#define UI_VALUE_ACT      0
#define UI_VALUE_FULL     1

/* ----- Maximum for static lists ----- */
#define UNIT_MAX_GOTOPOS 12

/* ---------- Defines --> Activities are new ORDERS ------ */
#define ORDER_NONE          0x00

#define ORDER_SENTRY        0x01    /* Sentry mode: Orbit too   */
#define ORDER_GUARD         0x02    /* Guard mode               */
#define ORDER_AUTOATTACK    0x03
#define ORDER_AUTORETREAT   0x04
#define ORDER_PATROL        0x05    /* Patrol (multiple patrol points)  */
#define ORDER_BORDERPATROL  0x06    /* Patrols the borders              */
#define ORDER_GOTO          0x07    /* Autopilot                        */
#define ORDER_EXPLORE       0x08    /* Auto explore                     */
#define ORDER_BUILD         0x09    /* 'target' depends on 'order.build'*/
                                    /* 0: Build in space                */
                                    /* > 0: Build outpost on planet     */
#define ORDER_TERRAFORM     0x0A    /* Terraform 'target' (planet)      */
#define ORDER_COLONIZE      0x0B    /* Colonize 'target' (planet)       */
#define ORDER_ACTION_CANCEL 0x0C

/* --------- Actions to undertake for AI and display ---------------------- */
#define ORDER_ACTION_ATTACK       0x0D  /* Special for game loop and AI     */
#define ORDER_ACTION_DEFEND       0x0E
#define ORDER_ACTION_TRADEROUTE   0x0F  /* Traderoute between two planets   */


#define UNIT_TARGET_NONE      0x00  /* no idea, not needed e.g ORDER_EXPLORE  */
#define UNIT_TARGET_POS       0x01  /* (ORDER_GOTO)                           */
#define UNIT_TARGET_SPACE     0x02  /* (ORDER_BUILD) in Space                 */
#define UNIT_TARGET_PLANET    0x03  /* (ORDER_BUILD) Outpost auf Planet
                                       (ORDER_TERRAFORM)
                                       (ORDER_COLONIZE)                        */
#define UNIT_TARGET_HUNT      0x04  /* 'unit_no' is hunted by this one         */
#define UNIT_TARGET_GUARDU    0x05  /* 'unit_no' is guarded  by this one       */
#define UNIT_TARGET_GUARDP    0x06  /* 'planet_no' is guarded by this unit     */

/* ******************************************************************************
* ENUMS                                                                         *
********************************************************************************/

/* ******************************************************************************
* TYPEDEFS                                                                      *
********************************************************************************/

typedef struct
{
    int attackassist;       /* In points    */
    int starbasedefense;    /* In points    */
    int mining;             /* In percent   */
}
STARBASE_BONUS_T;

typedef struct
{
    int  id;             /* Number of activity */
    char description[UNITINFO_MAX_NAMELEN + 1];
    /* Maybe add here some more data about activities */
    char descchar;
}
UNIT_ACTIVITY_T;

#endif  /* _FSC_UNITINFO_H_ */
