/*******************************************************************************
*  TECH.C                                                                      *
*      - Handling civilization advances (technologies) and it's lists          *
*                                                                              *
*   FREE SPACE COLONISATION                                                    *
*       Copyright (C) 2002-2017  Paul Müller <muellerp61@bluewin.ch>           *
*                                                                              *
*  This program is free software; you can redistribute it and/or modify        *
*  it under the terms of the GNU General Public License as published by        *
*  the Free Software Foundation; either version 2 of the License, or           *
*  (at your option) any later version.                                         *
*                                                                              *
*  This program is distributed in the hope that it will be useful,             *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of              *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
*  GNU Library General Public License for more details.                        *
*                                                                              *
*  You should have received a copy of the GNU General Public License           *
*  along with this program; if not, write to the Free Software                 *
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  *
*******************************************************************************/

/*******************************************************************************
* INCLUDES                                                                     *
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#include "effect.h"


#include "tech.h"

/*******************************************************************************
* DEFINES                                                                      *
*******************************************************************************/

#define TECH_NAME_LEN    32
#define TECH_MAXPREQ      2

#define TECH_ISKEY(s1, s2) (((s1[0] - s2[0]) | (s1[1] - s2[1]) | (s1[2] - s2[2])) == 0)
#define TECH_SIGNCPY(s1,s2) { (s1[0] = s2[0]); (s1[1] = s2[1]); (s1[2] = s2[2]); }

/* ---------- The different technical categories -------- */
#define TECH_TECHCAT_MEDICAL        1
#define TECH_TECHCAT_INDUSTRIAL     2
#define TECH_TECHCAT_DEFENSE        3
#define TECH_TECHCAT_COMMUNICATION  4
#define TECH_TECHCAT_PROPULSION     5
#define TECH_TECHCAT_WEAPONS        6

/* -------- Techlist flags ------------ */
#define TECH_NONE         ((unsigned char)0x00)
#define TECH_UNKNOWN      ((unsigned char)0x00)
#define TECH_PREQ1        ((unsigned char)0x01)   /* Prerequisite one available    */
#define TECH_PREQ2        ((unsigned char)0x02)   /* Prerequisite two available    */
#define TECH_KNOWN        ((unsigned char)0x10)
#define TECH_GOAL         ((unsigned char)0x20)   /* On path to goal               */
#define TECH_SUBGOAL      ((unsigned char)0x40)   /* On path to subgoal            */
#define TECH_MARKED       ((unsigned char)0x80)   /* Already visited on search for */
                                                      /* a goal                        */

#define TECH_PREQMASK     ((unsigned char)(TECH_PREQ1 | TECH_PREQ2))
#define TECH_GOALMASK     ((unsigned char)(TECH_KNOWN | TECH_MARKED))

#define TECH_ISUNKNOWN(x)   (x == 0)
#define TECH_ISKNOWN(x)     ((x & TECH_KNOWN) == TECH_KNOWN)
#define TECH_ISREACHABLE(x) ((x & (TECH_PREQMASK | TECH_KNOWN)) == TECH_PREQMASK)
#define TECH_ISMARKED(x)    (x & TECH_MARKED)
#define TECH_ISGOALMARKED(x)(x & TECH_GOALMASK)

#define TECH_BITMASK(tech_no)  ((unsigned char)(0x01 << (char)(tech_no & 0x07)))
#define TECH_SETBIT (x, tech_no) ((x[tech_no >> 3] |= TECH_BITMASK(tech_no));

/*******************************************************************************
* TYPEDEFS                                                                     *
*******************************************************************************/

typedef struct
{
    char key_code[4];                 /* Brief 3 chars + zero: e.g. "AF1"   */
                                  /* Empty string signs end of array    */
    char name[TECH_NAME_LEN]; /* Name of advance                    */
    int  cost;                   /* Research cost in 'bulbs'         */

    /* Some additional data as help for the AI                          */
    char aivalue;                /* Basic value at which computer players
                                   view this advance (used in determining
                                   which discovery to pursue, which to
                                   acquire during exchanges, and value of
                                   gift during diplomacy).              */
    char modifier;               /* Modifier to value based on the "civilized"
                                   aspect of a leader's personality.  Positive
                                   values increase value for civilized leaders,
                                   decrease it for militaristic.  Negative
                                   values vice versa.                    */

    char preq[TECH_MAXPREQ][4]; /* Technologies required for thisone      */

    char period;
    char category;               /* Knowledge category                   */
    /* ------------ Info for current game ------------------------------ */
    char global;                 /* Number of empires which have reached */
                                 /* this advance.                        */

    GAME_EFFECT_T effect;       /* Effect of this technolgy, if any      */
}
TECH_T;

/*******************************************************************************
* DATA                                                                         *
*******************************************************************************/

static int Num_Technologies = 0;
static unsigned char Tech_Bits[16];     /* Technologies as Bits */

static TECH_T Technologies[TECH_MAX + 2] =
{
    { "nil", "Nothing" },
    { "AFl", "Cloaking Device",        5,   4,-2, { "Rad", "Too" }, 3, 4 },
    { "Alp", "On-board computer",      5,   5, 1, { "Mon", "Csc" }, 0, 3 },
    { "Ast", "Matter conversion",      5,   4, 1, { "Mys", "Mat" },  1, 3 },
    { "Ato", "Protomatter Absorbtion", 5,   4,-1, { "ToG", "Phy" }, 2, 3 },
    { "Aut", "Dreadnaught Design",     5,   6,-1, { "Cmb", "Stl" }, 3, 4 },
    { "Ban", "Interstellar Banking",   5,   4, 1, { "Tra", "Rep" }, 1, 1 },
    { "Bri", "Space Compression",      5,   4, 0, { "Iro", "Cst" }, 0, 4 },
    { "Bro", "Duranium",               5,   6, -1, { "Mon", "Csc" }, 0, 4 },
    { "Cer", "Holographics",           5,   5, 0, { "Mon", "Csc" }, 0, 2 },
    { "Che", "Phase Shifting",         5,   5,-1, { "Uni", "Med" }, 1, 3 },
    { "Chi", "Destroyer Design ",      5,   6,-2, { "Feu", "Hor" }, 1, 0 },
    { "CoL", "Fleet Directives",       5,   4, 1, { "Alp", "Csc" }, 0, 2 },
    { "Cmb", "Freighter Design",       5,   5,-1, { "Ref", "Exp" }, 2, 4 },
    { "Cmn", "Star Empire",            5,   5, 0, { "CoL", "Lit" }, 2, 2 },
    { "Cmp", "Positronics",            5,   4, 1, { "Min", "MP" },  3, 4 },
    { "Csc", "Space Flight",           5,   7,-1, { "nil", "nil" }, 2, 0 },
    { "Cst", "Tractor Beam",           5,   4, 0, { "Mas", "Cur" }, 0, 4 },
    { "Cor", "Interstellar Corporation", 5,   4, 0, { "RR", "Eco" }, 2, 1 },
    { "Cur", "Latinum",                  5,   4, 1, { "Bro", "Csc" }, 0, 1 },
    { "Dem", "Star Democracy",           5,   5, 1, { "no", "no" }, 2, 2 },
    { "Eco", "Interstellar Economics",   5,   4, 1, { "Uni", "Ban" }, 2, 1 },
    { "E1", "Virtual Particles",         5,   4, 0, { "Met", "Mag" }, 2, 4 },
    { "E2", "Subatomic Amplifier",       5,   4, 1, { "no", "no" }, 3, 4 },
    { "Eng", "Advanced Engineering",     5,   4, 0, { "Whe", "Cst" }, 0, 4 },
    { "Env", "Warp Speed Limit",         5,   3, 1, { "Rec", "SFl" }, 3, 2 },
    { "Esp", "Mind Control",             5,   2,-1, { "Gue", "nil" }, 3, 0 },
    { "Exp", "Genesis Project",          5,   5, 0, { "Gun", "Che" }, 2, 4 },
    { "Feu", "Crew Training",            5,   4,-1, { "War", "CoL" } },
    { "", "",         5,   3, -2, { "", "" } },
    { "", "",         5,   3, -2, { "", "" } },
    { "", "",         5,   3, -2, { "", "" } },
    { "", "",         5,   3, -2, { "", "" } },
    { "", "",         5,   3, -2, { "", "" } },
    { "", "",         5,   3, -2, { "", "" } },
    { "", "",         5,   3, -2, { "", "" } },
    { "", "",         5,   3, -2, { "", "" } },
    { "", "",         5,   3, -2, { "", "" } },
    { "", "",         5,   3, -2, { "", "" } },
    { "", "",         5,   3, -2, { "", "" } },
    { "", "",         5,   3, -2, { "", "" } },
    { "", "",         5,   3, -2, { "", "" } },
    #if 0
	Photon Absorption (Fli),             4,-1,  Cmb, ToG, 2, 4    ; Fli
	Collective (Fun),     3,-2,  MT,  Csc, 2, 2    ; Fun
	Protomatter Fusion (FP),       3, 0,  NP,  Sup, 3, 3    ; FP
	Genetic Modification (Gen),   3, 2,  Med, Cor, 3, 3    ; Gen
	Guerrilla Warfare,  4, 1,  Esp, Tac, 3, 0    ; Gue
	Defender Design (Gun),          8,-2,  Inv, Iro, 1, 0    ; Gun
	Warp Field (Hor),   7,0,  Csc, nil, 0, 0    ; Hor
	Industrial Replication (Ind),  6, 0,  no,  no, 2, 1    ; Ind
	Artificial Intelligence (Inv),          6, 0,  Eng, Lit, 1, 4    ; Inv
      	Duranium (Iro),       5,-1,  Bro, War, 0, 4    ; Iro
	Marquis Tactics (Lab),        4,-1,  MP,  Gue, 3, 2    ; Lab
	Tachyon Beam (Las),              4, 0,  NP,  MP,  3, 3    ; Las
	Fleet Manoeuvre (Ldr),         5,-1,  Chi, Gun, 1, 0    ; Ldr
	Vulcan Logic (Lit),           5, 2,  Wri, CoL, 0, 3    ; Lit
	Nanit Technology (Too),      4,-2,  Stl, Tac, 2, 4    ; Too
	Subspace Amplifier (Mag),          4,-1,  Phy, Iro, 1, 3    ; Mag
	Subspace (Map),         6,-1,  Alp, Csc, 0, 1    ; Map
	Force Field (Mas),            4, 1,  Mon, Csc, 0, 4    ; Mas
	Transporter (MP),    5, 0,  Aut, Cor, 3, 4    ; MP
	Distortion Field (Mat),        4,-1,  Alp, Mas, 0, 3    ; Mat
	Tricorder Tech (Med),           4, 0,  Phi, Tra, 1, 1    ; Med
	Tritanium Alloy(Met),         6,-2,  Gun, Uni, 1, 0    ; Met
	Absorption Field (Min),    4, 1,  Too, E1,  3, 4    ; Min
XXXXMobile Warfare,     8,-1,  no, no, 3, 0    ; Mob
	Stellar Government (Mon),           8, 0,  nil, nil, 0, 2    ; Mon
	Telepathy (MT),         5, 1,  Phi, PT,  1, 2    ; MT
	3D-Holographics (Mys),          4, 0,  Cer, Csc, 0, 2    ; Mys
	Subspace Guidance (Nav),         6,-1,  Sea, Ast, 1, 1    ; Nav
	Protomatter Fission (NF),    6,-2,  Ato, MP,  3, 3    ; NF
	Protomatter Power (NP),      3, 0,  NF,  E1,  3, 3    ; NP
	Federation Philosophy (Phi),         6, 1,  Mys, Lit, 1, 2    ; Phi
	Subspace Physics (Phy),            4,-1,  Nav, Lit, 1, 3    ; Phy
	Transparent Aluminium (Pla),           4, 1,  Ref, SFl, 3, 4    ; Pla
      Fer Civ,           4, 0,  no, no,  1, 4    ; Plu  (Cst & Pot)
	Visor (PT),         4, 0,  Cer, Hor, 0, 2    ; PT
	Automated Farms (Pot),            4, 1,  Mon, Csc, 0, 1    ; Pot
	Star Gate Tech (Rad),              5,-1,  Fli, E1,  3, 4    ; Rad
	Transwarp (RR),           6, 0,  SE, Bri,  2, 1    ; RR
	Waste Reprocessing (Rec),          2, 1,  MP, nil,  3, 2    ; Rec
	Dilithium Crystal (Ref),           4, 0,  Che, Cor, 2, 4    ; Ref
	Food Replication (Ref),      3, 1,  E1,  San, 3, 1    ; Rfg
	Federation (Rep),           5, 1,  CoL, Lit, 0, 2    ; Rep
       	Humanoid Robotics (Rob),           5,-2,  Cmp, nil, 3, 0    ; Rob
	Trilithium Crystal (Roc),           6,-2,  AFl, E1,  3, 0    ; Roc
	Biofiltration (San),         4, 2,  Med, Eng, 2, 1    ; San
	Subspace Communication (Sea),          4, 1,  Map, Pot, 0, 1    ; Sea
	Warp 10 (SFl),       4, 1,  Cmp, Roc, 3, 3    ; SFl
       Kli Civ (Sth),            3,-2,  no, no, 3, 0    ; Sth
	Time Travel (SE),       4,-1,  Phy, Inv, 2, 3    ; SE
	Starbase Design (Stl),              4,-1,  E1,  nil, 2, 4    ; Stl
	Ultraconductor (Sup),     4, 1,  Pla, Las, 3, 3    ; Sup
	Warship Design (Tac),            6,-1,  Csc, Ldr, 2, 0    ; Tac
	Bioimplants (The),           3, 2,  MT,  Feu, 1, 2    ; The
	Mirror Universe Theory (ToG),  4, 0,  Ast, Uni, 1, 3    ; ToG
	Interstellar Trade (Tra),              4, 2,  Cur, CoL, 0, 1    ; Tra
	University (Uni),         5, 1,  Mat, Phi, 1, 3    ; Uni
	Fleet Academy (War),       4,-1,  Mon, Csc, 0, 0    ; War
	Scout Design (Whe),              6,-1,  Hor, Csc, 0, 4    ; Whe
	Universal Translator (Wri),            4, 2,  Alp, Mon, 0, 3    ; Wri
	Future Technology,  1, 0,  FP,  Rec, 3, 3    ; ...
    #endif
    { "" },
    { "" }
};

/*******************************************************************************
* CODE                                                                         *
*******************************************************************************/

/*
 * Name:
 *     tech_can_research
 * Description:
 *     Checks if the key-codes for the pre-requisites of given tech are available
 * Input:
 *      ptech *:      Pointer on technology to check, if available
 *      pkey_codes *: Keycodes of the techs already researched
 * Output:
 *     Returns > 0, if the given tech can be researched
 */
static int tech_can_research(TECH_T *ptech, char *pkey_codes)
{
    char *pkey;
    int ok;


    /* First left, then right */
    ok = 0;
    pkey = pkey_codes;

    while(*pkey)
    {
        if(
           ((ptech->preq[0][0] - pkey[0]) |
            (ptech->preq[0][1] - pkey[1]) |
            (ptech->preq[0][2] - pkey[2])) == 0
        )
        {
            ok = 1;
            break;
        }

        pkey += 4;
    }

    if(ok)
    {
        pkey = pkey_codes;

        while(*pkey)
        {
            if(
                ((ptech->preq[1][0] - pkey[0]) |
                 (ptech->preq[1][1] - pkey[1]) |
                 (ptech->preq[1][2] - pkey[2])) == 0
            )
            {
                return 1;
            }

            pkey += 4;
        }
    }

    return 0;
}

/*
 * Name:
 *     tech_get_reachable
 * Description:
 *     Fills the 'pavail *' with the numbers ot the techs that are reachable
 *     for research in given 'punresearched *'.
 * Input:
 *     punresearched *: Number of techs not researched yet
 *     pkey_codes *:    Key codes of techs already researched
 *     pavail *:        Where to return the numbers of the techs that can be researched
 *
 *     presearched *: Pointer on list with number of techs already researched
 *     availlist *:   List to be filled with numbers of advances, that can be
 *                    researched. Zero terminated.
 * Output:
 *     Returns the number of advances in reach for research
 */
static int tech_get_reachable(int *punresearched, char *pkey_codes, int *pavail)
{
    int count;


    count = 0;

    while(*punresearched)
    {
        /* Check it the key-codes for the pre-requisites are available */
        if(tech_can_research(&Technologies[*punresearched], pkey_codes))
        {
            *pavail = *punresearched;
            pavail++;
            count++;
        }

        /* Next tech */
        punresearched++;
    }

    /* Sign end of array */
    *pavail = 0;

    return count;
}

/*
 * Name:
 *     tech_bitstonumbers
 * Description:
 *     Translates the bits in given array to numbers
 * Input:
 *     ptechbits *: Pointer on bitlist for technologies
 *     plist *:     Where to return the number of improvements already built
 *     num_tech:    Number of technologies available
 * Output:
 *     Pointer on effects, if any available.
 * Last change:
 *     2017-08-05 <bitnapper>
 */
static int tech_bitstonumbers(unsigned char *ptechbits, int *plist, int num_tech)
{
    int tech_no;
    int count;


    count = 0;

    for(tech_no = 1; tech_no <= num_tech; tech_no++)
    {
        if(ptechbits[tech_no >> 3] & TECH_BITMASK(tech_no))
        {
            /* Bit is set */
            *plist = tech_no;

            /* Advance list */
            plist++;
            count++;
        }
    }

    /* Sign End of array  */
    *plist = 0;

    return count;
}

/* ========================================================================== */
/* ========================= PUBLIC FUNCTIONS =============================== */
/* ========================================================================== */

/*
 * Name:
 *     tech_init
 * Description:
 *     Initalizes as internals for this module
 * Input:
 *      None
 */
void tech_init(void)
{
    TECH_T *ptech;


    memset(&Technologies[0], 0, sizeof(TECH_T));

    Technologies[0].key_code[0] = 'n';
    Technologies[0].key_code[1] = 'i';
    Technologies[0].key_code[2] = 'l';

    Num_Technologies = 0;

    ptech = &Technologies[1];

    while(ptech->key_code[0] != 0)
    {
        /* No one has reached this one yet */
        ptech->global = 0;

        ptech++;
        Num_Technologies++;
    }
}

/*** =========== Managing functions =========== ***/

/*
 * Name:
 *     tech_set_researched
 * Description:
 *     Sets the bit for the technology researched and returns its
 *     effects in 'pge *'
 * Input:
 *     ptechbits *: List with bits set for every tech researched
 *     tech_no:     Number of tech to add the bit for
 *     pge *:       Where to return the technoligys effect, if any
 * Output:
 *      Number of effects in 'pge *'
 */
int tech_set_researched(unsigned char *ptechbits, int tech_no, GAME_EFFECT_T *pge)
{
    /* Advance global counter */
    Technologies[tech_no].global++;

    /* Set bit in list */
    ptechbits[tech_no >> 3] |= TECH_BITMASK(tech_no);

    memcpy(pge, &Technologies[tech_no].effect, sizeof(GAME_EFFECT_T));

    return 1;
}

/*
 * Name:
 *     tech_get_info
 * Description:
 *     Returns the info for management of techs, based on 'ptechbits'
 * Input:
 *     ptechbits *: List with bits set for every tech researched
 *     pinfo *:      Where to return the management info
 * Last change:
 *      2017-08-05 <bitnapper>
 */
void tech_get_info(unsigned char *ptechbits, TECH_INFO_T *pinfo)
{
    unsigned char unresearched_bits[TECH_MAXBYTES];
    char key_codes[(TECH_MAX + 2) * 4];
    int avail[(TECH_MAX + 2)];
    int num_notresearched;
    int i;


    /* Clear the given buffer */
    memset(pinfo, 0, sizeof(TECH_INFO_T));

    /* Get the bits for the total techs available */
    Num_Technologies = 0;

    for(i = 1; i <= TECH_MAX; i ++)
    {
        if(Technologies[i].key_code[0] != 0)
        {
            Tech_Bits[i >> 3] |= TECH_BITMASK(i);
            Num_Technologies++;
        }
        else
        {
            break;
        }
    }


    for(i = 0; i < TECH_MAXBYTES; i++)
    {
        /* Set all tech-bits for difference */
        unresearched_bits[i] = Tech_Bits[i] & (~ptechbits[i]);
    }

    /* List of these, which are not researched yet */
    num_notresearched = tech_bitstonumbers(unresearched_bits, pinfo->list_unresearched, Num_Technologies);
    /* List of these, which are researched */
    pinfo->num_researched = tech_bitstonumbers(ptechbits, pinfo->list_researched, Num_Technologies);

    if(num_notresearched > 0)
    {
        /** Look up, which of these can be researched **/
        /* First: Get the key codes of the techs aleady researched */
        tech_get_keycodes(ptechbits, key_codes);

        pinfo->num_reachable = tech_get_reachable(pinfo->list_unresearched, key_codes, pinfo->list_reachable);
    }
}

/*
 * Name:
 *     tech_get_turns
 * Description:
 *     Returns how many turns it takes to research given tech
 * Input:
 *     tech_no:      For the technology with this number
 *     invested:     This much we have already invested
 *     research_pts: That much research points per turn available
 * Output:
 *     Number of turns needed to research this tech
 *     < 0: No Progress
 * Last change:
 *      2017-07-30 <bitnapper>
 */
int tech_get_turns(int tech_no, int invested, int research_pts)
{
    int cost, turns;


    cost = 100 * Technologies[tech_no].cost;

    if((invested + research_pts) >= cost)
    {
        return 0;
    }

    /* Otherwise, there are turns left todo */
    if(research_pts <= 0)
    {
        /* Research is stuck, no progress */
        return -1;
    }

    turns = cost / research_pts;

    /* Add possible partial turn needed */
    if((cost % research_pts) > 0)
    {
        turns++;
    }

    return turns;
}

/*
 * Name:
 *     tech_get_effects
 * Description:
 *     Returns the effects of tech with given number
 * Input:
 *     tech_no: For the technology with this number
 *     pge *:   Where to return the effects
 *     pcost *: Where to return the cost in bulbs, if needed
 * Output:
 *     Number of effects for this tech
 * Last change:
 *      2017-07-23 <bitnapper>
 */
int tech_get_effects(int tech_no, GAME_EFFECT_T *pge, int *pcost)
{
    if(pcost)
    {
        *pcost = 100 * Technologies[tech_no].cost;
    }

    memcpy(pge, &Technologies[tech_no].effect, sizeof(GAME_EFFECT_T));

    return 1;
}

/*
 * Name:
 *     tech_get_name
 * Description:
 *     Returns a pointer on a list of effects for given technology.
 *     Has effects, als long as Effect->range > 0.
 * Input:
 *     tech_no: Number of tech to get name for
 *     pname *: Where to rturn the name
 */
void tech_get_name(int tech_no, char *pname)
{
    if(tech_no > 0 && tech_no < TECH_MAX)
    {
        strcpy(Technologies[tech_no].name, pname);
    }

    *pname = "";
}



/*** =========== List functions =========== ***/

/*
 * Name:
 *     tech_get_keycodes
 * Description:
 *     Returns a pointer on a list of effects for given technology.
 *     Has effects, als long as Effect->range > 0.
 * Input:
 *     ptechbits *:  List with bits set for every tech to be returned as number
 *     pkey_codes *: Where to return the key_codes
 * Output:
 *     Number of key-codes found
 */
int tech_get_keycodes(unsigned char *ptechbits, char *pkey_codes)
{
    int tech_no;
    int count;
    int i;


    /* The first ke-code is alway "nil" */
    pkey_codes[0] = 'n';
    pkey_codes[1] = 'i';
    pkey_codes[2] = 'l';
    pkey_codes[3] = 0;

    pkey_codes += 4;

    count = 1;

    for(tech_no = 1; tech_no < TECH_MAX; tech_no++)
    {
        if(ptechbits[tech_no >> 3] & TECH_BITMASK(tech_no))
        {
            /* Bit is set */
            for(i = 0; i < 3; i++)
            {
                pkey_codes[i] = Technologies[i].key_code[i];
            }

            pkey_codes[4] = 0;

            /* Advance list */

            pkey_codes += 4;
            count++;
        }
    }

    /* Sign End of array  */
    pkey_codes[0] = 0;

    return count;
}


/*
 * Name:
 *     tech_get_datadesc
 * Description:
 *     Returns the description needed to fill in the data records from a
 *     configuration file line
 *     Initializes the buffer by zeroing out it at start
 * Input:
 *     pdatadesc *: Where to return the data-descriptor(s)
 * Output:
 *     Number of descriptors filled in
 */
int tech_get_datadesc(SDLGLCFG_RECORD_T *pdatadesc)
{
    static SDLGLCFG_VALUE_T ConfigValue[] =
    {
        { SDLGL_VAL_STRING, &Technologies[0].key_code[0], 3 },
        { SDLGL_VAL_STRING, &Technologies[0].name[0], TECH_NAME_LEN - 1 },
        { SDLGL_VAL_INT,    &Technologies[0].cost },
        { SDLGL_VAL_CHAR,   &Technologies[0].aivalue },
        { SDLGL_VAL_CHAR,   &Technologies[0].modifier },
        { SDLGL_VAL_STRING, &Technologies[0].preq[0][0], 3 },
        { SDLGL_VAL_STRING, &Technologies[0].preq[1][0], 3 },
        { SDLGL_VAL_CHAR,   &Technologies[0].category },
        { SDLGL_VAL_CHAR,   &Technologies[0].effect.range },
        { SDLGL_VAL_CHAR,   &Technologies[0].effect.type },
        { SDLGL_VAL_CHAR,   &Technologies[0].effect.amount },
        { SDLGL_VAL_STRING, &Technologies[0].effect.info[0], 3 },
        { 0 }
    };

    static SDLGLCFG_RECORD_T DataDesc =
    {
        TECH_MAX,           /* Maximum of records                   */
        sizeof(TECH_T),     /* Size of record in buffer 'recdata'   */
        &Technologies[0],   /* Pointer on record data (any)         */
        "TECH",             /* Name of data in file, except '@'     */
        &ConfigValue[0]     /* Descriptor for values on single line */
    };

    /* Initialize the data buffer */
    memset(&Technologies[0], 0, (TECH_MAX * 2) * sizeof(TECH_T));

    memcpy(pdatadesc, &DataDesc, sizeof(SDLGLCFG_RECORD_T));

    return 1;
}

/* TODO: Read effects for AI... if needed at all */
