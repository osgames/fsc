/*******************************************************************************
*  FSCMAP.H                                                                    *
*	- Declarations for a Free Space Colonization map		                   *
*                                                                              *
*   FREE SPACE COLONISATION			                                           *
*      (c)2002 - 2017 Paul Mueller <muellerp61@bluewin.ch>                     *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*******************************************************************************/

#ifndef _FSC_FSCMAP_H_
#define _FSC_FSCMAP_H_

/*******************************************************************************
* INCLUDES  							                                       *
*******************************************************************************/

#include "sdlglmap.h"

#include "fscfile.h"    /* Load/save data */
#include "game.h"

/*******************************************************************************
* DEFINES								                                       *
*******************************************************************************/

#define FSCMAP_INVALID_POS  -1  /* Index into tile list         */

#define FSCMAP_OFFMAP   -1      /* Position asked for is off map             */
#define FSCMAP_OFFRANGE -2      /* Position asked for is off given rangetype */
#define FSCMAP_HASSTAR  -3
#define FSCMAP_HASUNIT  -4

#define FSCMAP_OFFREACH(pos)    (pos < 0)   /* Id off reach for movement */



#define FSC_SHARE_VISION  0
#define FSC_SHARE_RANGE   1


#define FSCMAP_SINFO_INRANGE    0x01
#define FSCMAP_SINFO_OFFRANGE   0x02
#define FSCMAP_SINFO_UNEXPLORED 0x04
#define FSCMAP_SINFO_BOTHRANGE  (FSCMAP_SINFO_INRANGE | FSCMAP_SINFO_OFFRANGE)

/* === Flags for tile info, fro drawing and view === */
#define FSCMAP_FINFO_STAR      0x01    /* Return info about star on given tile  */
#define FSCMAP_FINFO_OWNER     0x02    /* Info about owner of tiles             */
#define FSCMAP_FINFO_RANGE     0x04    /* Info of the range of given player     */
#define FSCMAP_FINFO_INFLUENCE 0x08    /* Info about influence of given player  */
#define FSCMAP_FINFO_UNIT      0x10    /* Info about possible unit on this tile */
#define FSCMAP_FINFO_ALIEN     0x20    /* Something alien                       */
#define FSCMAP_FINFO_ALIENUNIT 0x30    /* Unit + alien                          */
#define FSCMAP_FINFO_OFFRANGE  0x40    /* Look for e.g star which is off range  */
#define FSCMAP_FINFO_CLEAR     0x80    /* Clear the info buffer in function     */

/*******************************************************************************
* TYPEDEFS								                                       *
*******************************************************************************/

typedef struct
{
    int sec_w, sec_h;   /* Extent of map in sectors */
    int map_w, map_h;   /* Extent of whole map      */
    int vp_x, vp_y;     /* Position of viewport     */
    int vp_w, vp_h;     /* Extent of viewport       */
}
FSCMAP_INFO_T;

/* -- Detailed Info about a tile -- */
typedef struct
{
    int  pos;                   /* Position on map of this info         */
    char owner;                 /* Owned by this player                 */
    unsigned char knowledge;    /* Flags of player map                  */
                                /* Cursor at this position, yes/no      */
    char star_type;             /* >= 0: Star present. < 0: No star     */
    char star_name[25];         /* Name of star, if enough knowledge    */
    int  star_planets[7];       /* Array of planet numbers, 0-terminated*/
    /* == Info about UNIT for display == */
    char unit_no;               /* > 0: Unit here at all: Icon          */
    char unit_type;             /* @TODO: Info about icon               */
    char unit_owner;
    char chosen;                /* 1: If chosen unit of actual player   */
    char influenced_by;
    char influence_proz;
    int  draw_x, draw_y;        /* Position for drawing, relative to area */
    int  map_dist;              /* For 'area-info'                      */
}
FSCMAP_TILEINFO_T;

typedef struct
{
    char owner;
    char value;                 /* E.g. Attack, defense, influence      */
}
FSCMAP_OWNERINFO_T;                /* Returns 'influence' etc. by owner    */

typedef struct
{
    char owner;                 /* To whom does ist belong ?                    */
    char x, y;                  /* Position on map                              */
    char range;                 /* Bits for range borders north/east/south/west */
                                /* Can move in this range                       */
}
FSCMAP_POSINFO_T;

/*******************************************************************************
* CODE									                                       *
*******************************************************************************/

/* ========== Building and initialisation of a map ========= */

int  fscmap_create(GAME_STARTINFO_T *pgsi);
void fscmap_populate(void);

/* ====== Things on map ======== */
int  fscmap_add_star(FSCMAP_POSINFO_T *ppi, int star_no);
int  fcsmap_unit_add(int map_pos, int unit_no);
void fscmap_unit_remove(int map_pos, int unit_no);

/* ------ Info about map -------- */
int  fscmap_pos_fromxy(int pos_x, int pos_y, char viewport);
void fscmap_xy_frompos(int map_pos, FSCMAP_POSINFO_T *ppi);

/* ================= Map manipulation ================= */
void fscmap_set_range(FSCMAP_POSINFO_T *ppi, int range_no, int radius);
void fscmap_set_vision(FSCMAP_POSINFO_T *ppi, int radius, char info);
void fscmap_set_influence(FSCMAP_POSINFO_T *ppi, int radius, char value);

/* -------- Info for movement ---------- */
int  fscmap_get_adjacent(FSCMAP_POSINFO_T *ppi, int *padjposlist, int range_no);
char fscmap_get_long_heading(int src_pos, int curr_pos, int dest_pos);
FSCMAP_TILEINFO_T *fscmap_get_nearest_tile(int map_pos, FSCMAP_TILEINFO_T *pti, int numtile);


/* ================= Management of viewport ================= */
void fscmap_viewport_move(int direction);
void fscmap_viewport_adjust(int pos_x, int pos_y, char center, char is_viewpos);

/* ================= Management of map tiles and info about it ================= */
void fscmap_get_info(FSCMAP_INFO_T *pinfo);
char fscmap_get_tileinfo(FSCMAP_POSINFO_T *ppi, FSCMAP_TILEINFO_T *pti, char fwhich);
int  fscmap_get_mapinfo(int player_no, FSCMAP_TILEINFO_T *pti, int num_ti, char fwhich, char get_vp);
int  fscmap_get_mapinfo_range(int player_no, FSCMAP_POSINFO_T *pti, int num_ti, int range_no, char get_vp);
int  fscmap_get_distinfo(FSCMAP_POSINFO_T *ppi, int dist, FSCMAP_TILEINFO_T *pti, int num_ti, char fwhich);
int  fscmap_get_rangeinfo(FSCMAP_POSINFO_T *ppi, int range_no, FSCMAP_TILEINFO_T *pti, int num_ti, char fwhich);

/* ---- Mixed tools: Info about  -- */
int fscmap_get_sectorinfo(int map_pos, char *pname, char *pnumber);

/* --------- Load/save info --------- */
int fscmap_get_loadsave_info(FSCFILE_DATADESC_T *pdatadesc, char save);

#endif	/* _FSC_FSCMAP_H_ */
