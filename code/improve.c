/* *****************************************************************************
*  IMPROVE.C                                                                   *
*      - Handling of improvements (social projects) and it's lists             *
*                                                                              *
*  FREE SPACE COLONISATION                                                     *
*      (c)2002 - 2017 Paul Mueller <muellerp61@bluewin.ch>                     *
*                                                                              *
*  This program is free software; you can redistribute it and/or modify        *
*  it under the terms of the GNU General Public License as published by        *
*  the Free Software Foundation; either version 2 of the License, or           *
*  (at your option) any later version.                                         *
*                                                                              *
*  This program is distributed in the hope that it will be useful,             *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of              *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
*  GNU Library General Public License for more details.                        *
*                                                                              *
*  You should have received a copy of the GNU General Public License           *
*  along with this program; if not, write to the Free Software                 *
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  *
*******************************************************************************/


/* *****************************************************************************
* INCLUDES                                                                     *
*******************************************************************************/

#include <string.h>


#include "fscshare.h"
#include "effect.h"


/* --- Own header --- */
#include "improve.h"

/* *****************************************************************************
* DEFINES                                                                      *
*******************************************************************************/

#define IMPROVE_NAME_LEN    32
#define IMPROVE_MAX_EFFECT   5
#define IMPROVE_MAX        120
#define IMPROVE_COST_MULT  100


#define IMPROVE_BITMASK(id)  ((unsigned char)(0x01 << (char)(id & 0x07)))

/* *****************************************************************************
* TYPEDEFS                                                                     *
*******************************************************************************/

/* Type of improvement. (Read from FSCRULES.TXT)        */
typedef struct
{
    char key_code[4];       /* Short sign of this improvement, sign[4]    */
    char name[IMPROVE_NAME_LEN];
    int  cost;
    char upkeep;
    char req[4];            /* Requires this tech-keycode to be built     */
                            /* 'nil\0' -> None required                   */
    char req_mod[4];        /* Module required for building this one      */
                            /* reqmod[4];                                 */
    char proj_type;         /* Project type: PROJ_*                       */
    /* Effect of improvement */
    GAME_EFFECT_T effect;
    char built_by;
    char obsolete;
}
IMPROVE_T;

/* *****************************************************************************
* DATA                                                                         *
*******************************************************************************/

static IMPROVE_T Improvements[IMPROVE_MAX + 1] =
{
    { "nil", "Nothing" },
    { "GoB", "Imperial Palace",    10, 0, "nil", "nil", 1, { GER_MAP, GVAL_INFLUENCE, 50 } },
    { "ShY", "Shipyard",            4, 0, "nil", "nil", 1, { GER_MAP, GVAL_RANGE, 0, "nil" } },
    { "DeR", "Delithium Refinery",  8, 0, "nil", "nil", 1, { 0 } },
    { "SeA", "Sensor Array",        4, 0, "nil", "nil", 1, { GER_MAP, GVAL_SENSORS, 3, "nil" } },
    { "EnN", "Entertainment Network", 3, 1, "nil", "nil", 1, { GER_COLONY, GVAL_MORALE, 10 } },
    { "SoE", "Soil Enhancement",      5, 0, "nil", "nil", 1, { GER_PLANET, GVAL_FOOD, 2 } },
    { "OuP", "Maintenance Station",   2, 0, "___", "___", 7, { GER_MAP, GVAL_RANGE, 0, "nil" } },
    { "ArF", "Archeological Facility", 2, 0, "___", "___", 7, { GER_PLAYER, GVAL_RESEARCH, 2 } },
    { "MiS", "Mining Station",         2, 0, "___", "___", 7, { GER_PLAYER, GVAL_RESOURCES, 3 } },
    { "TeS", "Terraform Station",      2, 0, "___", "___", 7, { GER_PLANET, GVAL_TERRAFORM, 3 } },
    { "SeS", "Sensor Station",         1, 0, "___", "___", 7, { GER_MAP, GVAL_SENSORS, 3 } },
    { "" }
};

#if 0
static IMPROVEGLOBAL_T Improve_Global[IMPROVE_MAX_WONDER + 2];
         /* contains info about global wonders  --> global_wonders */
         /* global_wonders[] may also be (0), or the number of a colony
	        which no longer exists, if the wonder has been destroyed    */
#endif

/* Count these for checking bit lists    */
static int Improve_Count;

/* *****************************************************************************
* CODE                                                                         *
*******************************************************************************/

/*
 * Name:
 *     improve_sign_in_list
 * Description:
 *     Initalizes the basics for the improvements
 * Input:
 *     cmp_sign:     This sign has to be found
 *     psign_list *: Pointer on ints, 0-terminated
 * Output:
 *     Found yes/no
 */
static int improve_sign_in_list(char cmp_sign[4], char *psign_list)
{
    /* First check for "nil" */
    if(FSC_ISKEY("nil", cmp_sign))
    {
        return 1;
    }

    while(*psign_list)
    {
        if(FSC_ISKEY(psign_list, cmp_sign))
        {
            return 1;
        }

        psign_list += 4;
    }

    return 0;
}




/*
 * Name:
 *     improve_get_number
 * Description:
 *     Returns the number of the improvement with given pkey_code.
 *     If not found, 0 is returned.
 * Input:
 *     key_code: Key-Code to get number for.
 *  Output:
 *     Number of improvement with given key-code. 0, if no found.
 */
static int improve_get_number(char key_code[4])
{
    int improve_no;


    improve_no  = 1;

    while(Improvements[improve_no].key_code[0] != 0)
    {
        if(FSC_ISKEY(Improvements[improve_no].key_code, key_code))
        {
            /* Return the number of the improvement */
            return improve_no;
        }

        improve_no++;
    }

    /* Not found */
    return 0;
}


/* ========================================================================== */
/* ========================= PUBLIC FUNCTIONS =============================== */
/* ========================================================================== */

/*
 * Name:
 *     improve_init
 * Description:
 *     Initalizes the basics for the improvements
 * Input:
 *     None
 */
void improve_init(void)
{
    IMPROVE_T *pit;


    Improve_Count = 0;

    pit = &Improvements[1];

    while(pit->name[0] != 0)
    {
        pit->built_by  = 0;    /* For wonders  */
        pit->obsolete = 0;     /* For wonders  */

        pit++;

        Improve_Count++;
    }
}

/*
 * Name:
 *     improve_get_info
 * Description:
 *     Fills in info for management for given improvement-number and
 *     'invested'
 * Input:
 *     improve_no: Getr management data for this improvement
 *     pinfo *:    Pointer on buffer to fill with info
 *     invested:   For turns needed to build this improvement
 *     build_pts:  Available to build this improvement
 * Output:
 *     Valid improvement yes/no
 * Last change:
 *     2017-07-30 <bitnapper>
 */
int improve_get_info(int improve_no, IMPROVE_INFO_T *pinfo, int invested, int build_pts)
{
    /* Clear the buffer */
    memset(pinfo, 0, sizeof(IMPROVE_INFO_T));

    if(improve_no > 0)
    {
        pinfo->improve_no = improve_no;
        /* Name of improvement */
        strcpy(pinfo->name, Improvements[improve_no].name);
        /* Cost of improvement */
        pinfo->cost      = IMPROVE_COST_MULT * Improvements[improve_no].cost;
        pinfo->upkeep    = Improvements[improve_no].upkeep;     /* Cost for upkeep     */
        pinfo->proj_type = Improvements[improve_no].proj_type;  /* Project-type        */
        /* --------- Data for decision and information ----------- */
        memcpy(&pinfo->effect, &Improvements[improve_no].effect, sizeof(GAME_EFFECT_T));

        pinfo->turns = improve_build_time(improve_no, invested, build_pts);

        return 1;
    }

    return 0;
}

/*
 * Name:
 *     improve_get_name
 * Description:
 *     Returns the name of given improvement
 * Input:
 *     improve_no: Get the name for this improvement
 * Output:
 *     Less then zero: Stuck, no points to build it
 * Last change:
 *     2017-08-05 <bitnapper>
 */

char *improve_get_name(int improve_no)
{
    if(improve_no > 0 && improve_no < IMPROVE_MAX)
    {
        return Improvements[improve_no].name;
    }

    return "";
}

/*
 * Name:
 *     improve_build_time
 * Description:
 *     Returns the number of turns needed to build given improvement
 * Input:
 *     improve_no: Get the info for this improvement
 *     invested:   We invested this much already
 *     buildpts:   Available points per turn
 * Output:
 *     Less then zero: Stuck, no points to build it
 * Last change:
 *     2017-07-31 <bitnapper>
 */
int improve_build_time(int improve_no, int invested, int build_pts)
{
    int turns;
    int cost_left;


    /* Huge number of turns, if no 'build_pts' */
    turns = -1;

    if(improve_no > 0)
    {
        /* #RULES  */
        cost_left = (IMPROVE_COST_MULT * Improvements[improve_no].cost) - invested;

        if(cost_left <= 0)
        {
            turns = 0;
        }
        else
        {
            if(build_pts  > 0)
            {
                turns = cost_left / build_pts;

                if((turns % build_pts) > 0)
                {
                    turns++;
                }
            }
        }
    }

    return turns;
}

/* =========== List functions =========== */

/*
 * Name:
 *     improve_get_by_type
 * Description:
 *     Returns a list of improvements that have this type
 *     In this case used for 'Outpost-Buildings'
 * Input:
 *     projlist *: Pointer on a list of projects, that can be built
 *     proj_type:  Type of improvement
  * Output:
 *     Number of elements in list
 * Last change:
 *     2017-06-08 <bitnapper>
 */
int improve_get_by_type(char *projlist, char proj_type)
{
    int num_element;
    int improve_no;


    /* Find all improvements which can be built based on the tech signs */
    num_element = 0;
    improve_no  = 1;

    while(Improvements[improve_no].key_code[0] > 0)
    {
        if(Improvements[improve_no].proj_type == proj_type)
        {
            /* Add the list to the possible projects */
            *projlist = (char)improve_no;
            projlist++;
            num_element++;
        }

        improve_no++;
    }

    /* Sign end of list */
    *projlist = 0;

    return num_element;
}

/*
 * Name:
 *     improve_get_avail
 * Description:
 *     Returns a list of improvements, that can be built based on
 *     given technology list
 * Input:
 *     pimprovebits *: A list of bits of improvments that can be built
 *     ptech_keys *:   A pointer on an array of tech signs as char[4]
 * Output:
 *     Number of elements in list
 * Last change:
 *     2017-07-22 <bitnapper>
 */
int improve_get_avail(unsigned char *pimprovebits, char *ptech_keys)
{
    int num_element;
    int improve_no;


    /* First clear the list of improvement bits */
    for(num_element = 0; num_element < 8; num_element++)
    {
        pimprovebits[num_element] = 0;
    }

    /* Find all improvements which can be built based on the tech signs */
    num_element = 0;
    improve_no  = 1;

    while(Improvements[improve_no].key_code[0] > 0)
    {
        if(improve_sign_in_list(Improvements[improve_no].req, ptech_keys))
        {
            /* Set bit in list */
            pimprovebits[improve_no >> 3] |= IMPROVE_BITMASK(improve_no);
            num_element++;
        }

        improve_no++;
    }

    return num_element;
}

/*
 * Name:
 *     improve_bits_sub
 * Description:
 *     Returns 'pact_bits' with 'sub_bits' removed in 'result_bits'
 * Input:
 *     improvebits *: Pointer on bitlist for improvements
 *     plist *:       Where to return the number of improvements already built
 * Output:
 *     Pointer on effects, if any available.
 * Last change:
 *     2017-07-22 <bitnapper>
 */
void improve_bits_sub(unsigned char *pact_bits, unsigned char *sub_bits, unsigned char *result_bits)
{
    int i;


    for(i = 0; i < 16; i++)
    {
        result_bits[i] = pact_bits[i] & (~sub_bits[i]);
    }
}

/*
 * Name:
 *     improve_bitstonumbers
 * Description:
 *     Translates the bits in given array to numbers
 * Input:
 *     pimprovebits *: Pointer on bitlist for improvements
 *     plist *:        Where to return the number-index of the bits in list
 * Output:
 *     Pointer on effects, if any available.
 * Last change:
 *     2017-08-06 <bitnapper>
 */
int improve_bitstonumbers(unsigned char *pimprovebits, char *plist)
{
    int improve_no;
    int count;


    count = 0;

    for(improve_no = 1; improve_no < IMPROVE_MAX; improve_no++)
    {
        if(pimprovebits[improve_no >> 3] & IMPROVE_BITMASK(improve_no))
        {
            /* Bit is set */
            *plist = (char)improve_no;

            /* Advance list */
            plist++;
            count++;
        }
    }

    /* Sign End of array  */
    *plist = 0;

    return count;
}

/* =========== Managing functions =========== */

/*
 * Name:
 *     improve_build
 * Description:
 *     Sets this improvement in given list. Returns the list of
 *     effects
 * Input:
 *     pimprovebits *: Pointer on bitlist for improvements
 *     improve_no:     Number of improvement, if available
 *     key_code *:     Sign, if id not available
 *     pge *:          Where to return the description of EFFECT_T the improvement has
 * Output:
 *     Was found and could be built, yes/no
 */
int improve_build(unsigned char *pimprovebits, int improve_no, char key_code[4], GAME_EFFECT_T *pge)
{

    if(improve_no == 0)
    {
        /* Take improve_no from 'sign' */
        improve_no = improve_get_number(key_code);
    }

    if(improve_no > 0)
    {
        /* Set bit in list, if needed be */
        if(Improvements[improve_no].proj_type != PROJ_EFFECT)
        {
            pimprovebits[improve_no >> 3] |= IMPROVE_BITMASK(improve_no);
        }

        memcpy(pge, &Improvements[improve_no].effect, sizeof(GAME_EFFECT_T));

        return 1;
    }

    memset(pge, 0, sizeof(GAME_EFFECT_T));

    return 0;
}

/*
 * Name:
 *     improve_remove
 * Description:
 *     Removes improvement from given list. Returns a pointer on the effects
 *     of given improvement.
 * Input:
 *     pimprovebits *: Pointer on bitlist for improvements
 *     improve_no:     Number of improvement, if available
 *     key_code *:     Key-Code, if number is not not available
 * Output:
 *     Number of effects in 'pge'
 */
int improve_remove(unsigned char *pimprovebits, int improve_no, char key_code[4], GAME_EFFECT_T *pge)
{
    if(improve_no == 0)
    {
        /* Take improve_no from 'sign' */
        improve_no = improve_get_number(key_code);
    }

    if(improve_no > 0)
    {
        pimprovebits[improve_no >> 3] &= (~IMPROVE_BITMASK(improve_no));

        memcpy(pge, &Improvements[improve_no].effect, sizeof(GAME_EFFECT_T));

        return 1;
    }

    memset(pge, 0, sizeof(GAME_EFFECT_T));

    return 0;
}

/*
 * Name:
 *     improve_replace
 * Description:
 *     Replaces given improvement 'signold' by given improvement 'signnew'.
 * Input:
 *      il *:    Bit-List where to replace the improvement
 *      signold: Sign of old improvement
 *      signnew: Sign of new improvement
 */
void improve_replace(unsigned char *pil, char signold[4], char signnew[4])
{
    char idold, idnew;

    idold = improve_get_number(signold);
    idnew = improve_get_number(signnew);

    /* Remove old building  */
    pil[idold >> 3] &= (unsigned char)(~IMPROVE_BITMASK(idold));
    /* Add new building     */
    pil[idnew >> 3] |= IMPROVE_BITMASK(idnew);
}


    /* ============= For loading of game data =========== */

/*
 * Name:
 *     improve_get_datadesc
 * Description:
 *     Returns the description needed to fill in the data records from a
 *     configuration file line
 *     Initializes the buffer by zeroing out it at start
 * Input:
 *     pdatadesc *: Where to return the data-descriptor(s)
 * Output:
 *     Number of descriptors filled in
 */
int improve_get_datadesc(SDLGLCFG_RECORD_T *pdatadesc)
{
    static SDLGLCFG_VALUE_T ConfigValue[] =
    {
        { SDLGL_VAL_STRING, &Improvements[0].key_code[0], 3 },
        { SDLGL_VAL_STRING, &Improvements[0].name[0], IMPROVE_NAME_LEN - 1 },
        { SDLGL_VAL_INT,    &Improvements[0].cost },
        { SDLGL_VAL_CHAR,   &Improvements[0].upkeep },
        { SDLGL_VAL_STRING, &Improvements[0].req[0], 3 },
        { SDLGL_VAL_STRING, &Improvements[0].req_mod[0], 3 },
        { SDLGL_VAL_CHAR,   &Improvements[0].proj_type },
        { SDLGL_VAL_CHAR,   &Improvements[0].effect.range },
        { SDLGL_VAL_CHAR,   &Improvements[0].effect.type },
        { SDLGL_VAL_CHAR,   &Improvements[0].effect.amount },
        { SDLGL_VAL_STRING, &Improvements[0].effect.info[0], 3 },
        { 0 }
    };

    static SDLGLCFG_RECORD_T DataDesc =
    {
        IMPROVE_MAX,            /* Maximum of records                   */
        sizeof(IMPROVE_T),      /* Size of record in buffer 'recdata'   */
        &Improvements[0],  /* Pointer on record data (any)         */
                                /* Points on record[0],                 */
                                /* holding data from SDLGLCFG_VALUE_T   */
        "IMPROVE",              /* Name of data in file, except '@'     */
        ConfigValue             /* Descriptor for values on single line */
    };

    /* Initialize the data buffer */
    memset(&Improvements[0], 0, (IMPROVE_MAX -1) * sizeof(IMPROVE_T));

    memcpy(pdatadesc, &DataDesc, sizeof(SDLGLCFG_RECORD_T));

    return 1;
}
