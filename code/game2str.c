/* *****************************************************************************
*  GAME2STR.C                                                                  *
*	    - Functions for usage of laungauge specific strings and messages       *
*                                                                              *
*   FREE SPACE COLONISATION                                                    *
*       Copyright (C) 2002 - 2017 Paul Mueller <muellerp61@bluewin.ch>         *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*******************************************************************************/

/* *****************************************************************************
* INCLUDES    							                                       *
*******************************************************************************/

#include <stdio.h>
#include <string.h>


#include "sdlglcfg.h"       /* Read data from text files */
#include "fsctool.h"

/* --- Own header --- */
#include "game2str.h"

/*******************************************************************************
* TYPEDEFS     							                                       *
*******************************************************************************/

/*******************************************************************************
* DATA     							                                           *
*******************************************************************************/

/* TODO: Handling of 'rulename.txt' which should overwrite the names of
   UNITTYPEs, TECHs and IMPROVEments after the rules are loaded */

static char Advisor_Names[] =
    "Domestic Advisor\0"
    "Trade Advisor\0"
    "Military Advisor\0"
    "Foreign Advisor\0"
    "Cultural Advisor\0"
    "Science Advisor\0"
    "Galaxy Wonders\0"
    "Histography\0"
    "Planet Management\0"
    "Demography\0"
    "Statistics\0"
    "\0"
    "\0";

static char Sector_Str[] = "Sector";
static char Date_Name[]  = "Date";
static char Month_Str[]  = "Jan\0Feb\0Mar\0Apr\0May\0Jun\0Jul\0Aug\0Sep\0Oct\0Nov\0Dec";

static char Game_StarDensity[] = "scattered\0standard\0dense\0\0";    /* 1..3 */
static char Game_Difficulty[] =
    "Ensign\0"
    "Lieutenant\0"
    "Lieutenant Commander\0"
    "Commander\0"
    "Captain\0"
    "Admiral\0\0\0";



static char UnitOrder_Names[] =
    "Idle\0"
    "Sentry\0"
    "Guard\0"
    "Auto-Attack\0"
    "Auto-Retreat\0"
    "Patrol\0"
    "Border-patrol\0"
    "Goto\0"
    "Explore\0"
    "Build\0"
    "Terraform\0"
    "Colonize\0"
    "Cancel\0"
    "Attack\0"
    "Defend\0"
    "Traderoute\0"
    "\0";

static char UnitAbility_Names[] =
    "Unknown\0"
    "Military\0"
    "Colonize\0"
    "Transport\0"
    "Diplomacy\0"
    "Trade\0"
    "Scout\0"
    "Construct\0"
    "Outpost\0"
    "Starbase\0"
"\0";

static char Range_Names[]  = "Short\0Middle\0Long\0";

static char Stat_Names[] =
    "Population\0"
    "Military\0"
    "Economy\0"
    "Research\0"
    "Influence\0"
    "Diplomacy\0"
    "Money\0"
    "\0";

/* Effect_Names == Resources Name, too */
static char Effect_Names[] =
    "Food\0"
    "Resources\0"
    "Economy\0"
    "Military\0"
    "Social\0"
    "Stock\0"
    "Research\0"
    "Taxes\0"
    "Luxury\0"
    "-\0"
    "-\0"
    "Morale\0"
    "Approval\0"
    "Net Income\0"
    "Expenses\0"
    "Assimilate\0"
    "-\0"
    "-\0"
    "-\0"
    "-\0"
    "-\0"
    "Weapons\0"
    "Defense\0"
    "Speed\0"
    "Pop. Growth\0"
    "Diplomacy\0"
    "Sensors\0"
    "Espionage\0"
    "Influence\0"
    "Navigation\0"
    "Hitpoints\0"
    "Repair\0"
    "Prestige\0"
    "Starships\0"
    "Soldiering\0"
    "-\0"
    "-\0"
    "Range\0"
    "Warfare\0"
    "Planet Quality\0"
    "-\0"
    "Propaganda\0"
    "Terraform\0"
    "Outpost\0"
    "Trade\0"
    "Population\0"
    "\0\0";


/* Names of effect ranges.
 * (These must correspond to enum effect_range_id in improvement.h.)
 */

static char EffectRangeNames[] =
    "None\0"
    "Building\0"
    "Colony\0"
    "Planet\0"
    "Player\0"
    "Galaxy\0"
    "Units\0"
    "Map\0"
    "Outpost\0"
    "\0\0";

static char UnitNames[] =
    /* Names for shuttles and other transport units */
    "\0"
    "Columbus\0Copernicus\0Cousteau\0"
    "Einstein\0El-Baz\0Fermi\0Feynman\0"
    "Galileo\0Goddard\0Hawking\0Justman\0"
    "Magellan\0Onizuka\0Pickover\0Pike\0"
    "Rees\0Sakharov\0Tichy\0Voltaire\0\0";


static char Ability_Names[] =
    "\0"                                    /* UNIT_ABILITY_UNKNOWN         */
    "\0"                                    /* UNIT_ABILITY_MILITARY        */
    "can terraform and colonize planets\0"  /* UNIT_ABILITY_COLONIZE        */
    "can transport troops and goods\0"      /* UNIT_ABILITY_TRANSPORT       */
    "can explore anomalies\0"               /* UNIT_ABILITY_INVESTIGATE     */
    "can do diplomacy on enemy territory\0" /* UNIT_ABILITY_DIPLOMACY       */
    "can create trade routes\0"             /* UNIT_ABILITY_TRADE           */
    "\0"                                    /* UNIT_ABILITY_SCOUT           */
    "can build and extend outposts\0"       /* UNIT_ABILITY_CONSTRUCT       */
    "can build an outpost\0"                /* UNIT_ABILITY_BUILDOUTPOST    */
    "is a starbase\0"                       /* UNIT_ABILITY_STARBASE        */
    "extends ships movement range\0"        /* UNIT_ABILTIY_OUTPOST         */
    "/0/0";


/* One big buffer for all messages */
/*
static char MsgStr[] =
"@0102 $UNIT is stucked on map"
"@0103 Unit $UNIT tries to move out of range."
"@0104 $UNIT has no moves left"
"@0105 Tried to move $UNIT of wrong ownership"
"@0201 $PLANET has built $IMPROVE. "
"@0202 $PLANET has built $UNIT."
"@0203 On planet $PLANET an $IMPROVE outpost-is built."
"@0204 Research for $TECH is completed. We need a new task."
"@0205 On planet $PLANET terraforming is completed."
"@0";
*/

static SDLGLCFG_STRINGS_T Name_List[] =
{
    {
        GAME2STR_LABEL_ADVISOR,     /* which        */
        1, 11,                      /* min, max     */
        "ADVISOR",                 /* For reading from file */
        Advisor_Names               /* Names itself */
    },
    { GAME2STR_LABEL_STATS, 0, 6, "STATS", Stat_Names },
    { GAME2STR_LABEL_UNITORDER, 0, 15, "UNITORDER", UnitOrder_Names },
    { GAME2STR_LABEL_EFFECT, 0, 44, "EFFECT", Effect_Names },
    { GAME2STR_LABEL_RANGE, 0, 2, "RANGE", Range_Names },
    { GAME2STR_LABEL_ABILITY, 0, 9, "UNITABILITY", UnitAbility_Names },
    { GAME2STR_LABEL_DIFFICULTY, 0, 5, "DIFFICULTY", Game_Difficulty },
    { GAME2STR_LABEL_STARDENSITY, 0, 2, "STARDENSITY", Game_StarDensity },
    { GAME2STR_LABEL_EFFECTRANGE, 0, 8, "EFFECTRANGE", EffectRangeNames },
    { GAME2STR_LABEL_UNITNAME, 0, 19, "UNITNAME", UnitNames },
    { GAME2STR_LABEL_ABILITY, 0, 19, "ABILITY", Ability_Names },
    /* Sign end of array */
    { 0 }
};

static SDLGLCFG_STRINGS_T Menu_Names[] =
{
    {
        1,
        0, 6,
        "MAIN",
        "\0Start a new game\0Load a saved game\0Load game in progress\0"
        "Set game options\0Exit to OS\0"
    },
    { 0 }
};


/* *****************************************************************************
* CODE       							                                       *
*******************************************************************************/

/* ========================================================================== */
/* =========================== PUBLIC FUNCTION(S) =========================== */
/* ========================================================================== */

/*
 * Name:
 *     game2str_label
 * Description:
 *     Return string with 'label_no' from buffer 'which'
 * Input:
 *     which:    Kind of string (source buffer)
 *     label_no: Number of label of given kind
 * Output:
 *     Pointer on string which is the name of the value
 */
char *game2str_label(E_GAME2STR_TYPE which, int label_no)
{
    SDLGLCFG_STRINGS_T *pgsl;


    if(label_no >= 0)
    {
        pgsl = &Name_List[0];

        while(pgsl->which > 0)
        {
            if(pgsl->which == which)
            {

                /* We found it */
                if(label_no >= pgsl->min && label_no <= pgsl->max)
                {
                    /* If the label-number starts with 1, decrement,
                       because the string list always starts at 0 */
                    if(pgsl->min > 0)
                    {
                        label_no -= pgsl->min;
                    }
                    /* The number of the label is valid */
                    return fsctool_str_fromlist(pgsl->plist, label_no);
                }
            }

            pgsl++;
        }
    }

    return "";
}

/*
 * Name:
 *     game2str_menuname
 * Description:
 *     Returns a pointer on the first string on the buffer with the
 *     concated strings of "menu" with given name
 * Input:
 *    pwhich *:
 * Output:
 *     Pointer on first string in buffer
 */
char *game2str_menuname(char *pwhich)
{
    SDLGLCFG_STRINGS_T *pgsl;


    if(pwhich)
    {
        pgsl = &Menu_Names[0];

        while(pgsl->which > 0)
        {
            if(pgsl->plist && !strcmp(pwhich, pgsl->name))
            {
                return &pgsl->plist[1];
            }

            pgsl++;
        }
    }

    /* In case of error, return an empty string */
    return NULL;
}

/*
 * Name:
 *     game2str_sector_name
 * Description:
 *     Returns the name of a sector
 * Input:
 *     pdest *: Where to return the complete string
 *     pname *: Name of the sector if named
 *     sec_x,
 *     sec_y:   Position of sector on map
 */
void game2str_sector_name(char *pdest, char *pname, int sec_x, int sec_y)
{
    char sec_name_ext[100];


    if(!pname)
    {
        /* Create name with number of sector */
        sprintf(sec_name_ext, "%d - %d", sec_x, sec_y);

        pname = sec_name_ext;
    }

    sprintf(pdest, "%s %s", Sector_Str, pname);
}

/*
 * Name:
 *     game2str_date
 * Description:
 *     Converts the number of turn to a date with year and month
 *     Base is the year 2100
 *     It is assumed 4 Turns per month (one every week)
 * Input:
 *     pdest *:   Where to return the result
 *     turn_no:   For this turn
 *     base_year: The year to use as base
 */
void game2str_date(char *pdest, int turn_no, int base_year)
{
    int year, month;
    char *pmonth_name;


    year  = 2100 + (turn_no / 48);
    month = (turn_no % 48) / 4;

    pmonth_name = fsctool_str_fromlist(Month_Str, month);

    sprintf(pdest, "%s: %s %d", Date_Name, pmonth_name, year);
}

/*
 * Name:
 *     game2str_unit_name -- // unitName
 * Description:
 *     Returns the name of the unit with the given id
 * Input:
 *     unit_no:
 */
char *game2str_unit_name(int unit_no)
{
     unit_no += (3 + unit_no);
     unit_no %= 19;

    return fsctool_str_fromlist(UnitNames, unit_no);
}
