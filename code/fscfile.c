/*******************************************************************************
*  FSCFILE.C                                                                   *
*      - Read and save games                                                   *
*                                                                              *
*  FREE SPACE COLONISATION                                                     *
*      (c)2002 - 2017 Paul Mueller <muellerp61@bluewin.ch>                     *
*                                                                              *
*  This program is free software; you can redistribute it and/or modify        *
*  it under the terms of the GNU General Public License as published by        *
*  the Free Software Foundation; either version 2 of the License, or           *
*  (at your option) any later version.                                         *
*                                                                              *
*  This program is distributed in the hope that it will be useful,             *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of              *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
*  GNU Library General Public License for more details.                        *
*                                                                              *
*  You should have received a copy of the GNU General Public License           *
*  along with this program; if not, write to the Free Software                 *
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  *
*******************************************************************************/

/*******************************************************************************
* INCLUDES								                                       *
*******************************************************************************/

#include <stdio.h>
#include <string.h>


#include "colony.h"
#include "fscmap.h"
#include "game.h"
#include "player.h"
#include "starsys.h"
#include "unit.h"


/* -- Own header -- */
#include "fscfile.h"

/*******************************************************************************
* DEFINES								                                       *
*******************************************************************************/

/*******************************************************************************
* DATA  								                                       *
*******************************************************************************/

static char File_Sign[]       = "FSCSAV";
/* Directory for language files. Configurable later */
static char Language_Dir[20] = "eng\\";
/* Directory for savegames. Configurable later */
static char SaveGame_Dir[32]  = "gamefile\\";
static char MonthStr[]       = "Jan\0Feb\0Mar\0Apr\0May\0Jun\0Jul\0Aug\0Sep\0Oct\0Nov\0Dec";

/*******************************************************************************
* CODE  								                                       *
*******************************************************************************/

/*
 * Name:
 *     gamefile_date_string
 * Description:
 *     Generates a string with month and year and prints it into the
 *     given pbuffer.
 * Input:
 *     year, month: To generate the date from
 *     pbuffer *:    Pointer on pbuffer to print in the
 */
static void gamefile_date_string(int year, int month, char *pbuffer)
{
    if(month > 0)
    {
        month--;
    }

    sprintf(pbuffer, "%s %d", &MonthStr[month - 1 * 4], year);
}

/*
 * Name:
 *     fscfile_create_name
 * Description:
 *     Creates a name for a 'savegame' file.
 * Input:
 *     save_no:   Number of file to save
 *     pbuffer *: Buffer where to put the filename
 * Output:
 *     Pointer on created filename.
 */
static char *fscfile_create_name(int save_no, char *pbuffer, char map_file)
{
    char *pext;

    if(save_no < 0)
    {
        sprintf(pbuffer, "%sauto.sav", SaveGame_Dir);
    }
    else
    {
        pext = (map_file > 0) ? "map" : "sav";

        sprintf(pbuffer, "%sfsc%02d.%s", SaveGame_Dir, save_no, pext);
    }

    return pbuffer;
}

/*
 * Name:
 *     fscfile_read_savename
 * Description:
 *     Reads only the header info of savegame with given number
 * Input:
 *     game_no:   Number of file to read
 *     pbuffer *: Return here the name of the game from the file
 * Output:
 *     File could be read yes/no
 */
static int fscfile_read_savename(int game_no, char *pbuffer)
{
    char databuffer[256];
    FILE *f;

    f = fopen(fscfile_create_name(game_no, databuffer, 0), "rb");

    if(! f)
    {
        return 0;
    }

    /* Read file-signature: FIXME: Validate it */
    fread(databuffer, 6, 1, f);
    /* Now read the game description itself from file */
    fread(pbuffer, 32, 1, f);
    /* Close the file */
    fclose(f);

    return 1;
}

/*
 * Name:
 *     fscfile_datahead_read
 * Description:
 *     Reads only the header info of savegame with given number
 * Input:
 *     game_no:   Number of file to read
 *     pbuffer *: Return here the generated name of the game
 * Output:
 *     File could be read yes/no
 */
static int fscfile_datahead_read(int game_no, char *pbuffer)
{
    FSCFILE_DATAHEAD_T blockhead;
    FSCFILE_DATADESC_T datainfo[3];
    char datebuffer[256];
    FILE *f;


    f = fopen(fscfile_create_name(game_no, pbuffer, 0), "rb");

    if(! f)
    {
        return 0;
    }

    /* Read file-signature: FIXME: Validate it */
    fread(datebuffer, 6, 1, f);

    while(fread(&blockhead, sizeof(FSCFILE_DATAHEAD_T), 1, f) > 0)
    {
        /* FIXME: Check result of read function */
        switch(blockhead.data_name)
        {
            case FSCFILE_GAME:
                /* Always one record... */
                game_get_loadsave_info(datainfo, 0);
                break;

            default:
                /* Skip unknown blocks */
                blockhead.data_name= 0;
                fseek(f, blockhead.rec_size * blockhead.num_rec, SEEK_CUR);
        }

        if(blockhead.data_name > 0)
        {
            /* Is a known block */
            /* FIXME: Check 'rec_size' in 'blockhead' against 'rec_size' in 'gfb' */
            if(! fread(datainfo[0].pdata, blockhead.rec_size, blockhead.num_rec, f))
            {
                /* Read failed */
                fclose(f);
                return 0;
            }

            if(blockhead.data_name == FSCFILE_GAME)
            {
                /* For generation of game-name */
                game_get_date(datebuffer);
                /* FIXME: Generate proper game name from difficulty,    */
                /*        player name, nationality and date             */
                sprintf(pbuffer, "%d) Humans: %s", game_no, datebuffer);
                fclose(f);
                return 1;
            }
        }
    }

    return 0;
}

/*
 * Name:
 *     fscfile_get_all_datainfo
 * Description:
 *     Loads game with given name. If 'gamename' == 0 then it's an autosave.
 *     FIXME: Add error checking, check version...
 * Input:
 *     pdatadesc *: Where to write the data-info to
 *     save:        Get info for saving yes/no
 * Output:
 *      Success yes / no.
 */
static int fscfile_get_all_datainfo(FSCFILE_DATADESC_T *pdatadesc, char save)
{
    int num_datainfo;


    /* FSCFILE_GAME */
    num_datainfo = game_get_loadsave_info(pdatadesc, save);
    pdatadesc += num_datainfo;
    /* FSCFILE_PLAYER */
    num_datainfo = player_get_loadsave_info(pdatadesc, save);
    pdatadesc += num_datainfo;
    /*
    FSCFILE_STAR
    FSCFILE_PLANET
    FSCFILE_MOON
    FSCFILE_OUTPOST
    */
    num_datainfo = starsys_get_loadsave_info(pdatadesc, save);
    pdatadesc += num_datainfo;
    /* FSCFILE_UNIT */
    num_datainfo = unit_get_loadsave_info(pdatadesc, save);
    pdatadesc += num_datainfo;
    /*
    FSCFILE_MAPHEAD   = 8,
    FSCFILE_MAPGLOBAL = 8,      / * Global info for all and 'player' Nr. 0   * /
    FSCFILE_MAPPLAYER = 9,      / * Players knowledge of map * /
    */
    num_datainfo = fscmap_get_loadsave_info(pdatadesc, save);
    pdatadesc += num_datainfo;
    /* FSCFILE_COLONY */
    num_datainfo = colony_get_loadsave_info(pdatadesc, save);
    pdatadesc += num_datainfo;
    /* FSCFILE_DIPLINFO */
    /*
    num_datainfo = fscdipl_get_loadsave_info(pdatainfo, save);
    pdatainfo += num_datainfo;
    */

    return num_datainfo;
}

/* ========================================================================== */
/* ======================== PUBLIC FUNCTIONS	============================= */
/* ========================================================================== */

/*
 * Name:
 *     fscfile_set_dir
 * Description:
 *     Sets the directories for savegame and language
 * Input:
 *     psave_dir *: Where to look for the savegames
 *     plang_dir *: Where to loof for the text files for user defined language
 * Output:
 *      Success yes / no.
 */
void fscfile_set_dir(char *psave_dir, char *plang_dir)
{
    if(psave_dir)
    {
        /* @TODO: Attach backslash, if needed... */
        strncpy(SaveGame_Dir, psave_dir, 30);
        SaveGame_Dir[30] = 0;
    }

    if(plang_dir)
    {
        /* @TODO: Attach backslash, if needed... */
        strncpy(Language_Dir, plang_dir, 18);
        SaveGame_Dir[18] = 0;
    }
}

/*
 * Name:
 *     fscfile_load
 * Description:
 *     Loads game with given name. If 'gamename' == 0 then it's an autosave.
 *     FIXME: Add error checking, check version...
 * Input:
 *     game_no: Number of game to load
 * Output:
 *      Success yes / no.
 */
int fscfile_load(int game_no)
{
    char namebuffer[512];
    FSCFILE_DATAHEAD_T blockhead;
    FSCFILE_DATADESC_T datainfo[50], *pdatainfo;
    FILE *f;
    int rec_diff;    /* Difference of size of saved record an space for record */
    int count_diff;
    int rec_size;


    f = fopen(fscfile_create_name(game_no, namebuffer, 0), "rb");

    if(! f)
    {
        return 0;
    }

    /* Now  get all the data information from the different modules */
    fscfile_get_all_datainfo(datainfo, 0);

    /* Read file-signature: FIXME: Validate it */
    fread(namebuffer, 6, 1, f);

    /* FIXME: Check version */
    while(fread(&blockhead, sizeof(FSCFILE_DATAHEAD_T), 1, f) > 0)
    {
        /* FIXME: Check result of read function */
        pdatainfo = datainfo;

        while(pdatainfo->rec_info.data_name > 0)
        {
            /* Look up if we know this txpe of data */
            if(blockhead.data_name == pdatainfo->rec_info.data_name)
            {
                /* We fount the description -- buffer is cleared as 'datainfo' was fetched */
                break;
            }

            pdatainfo++;
        }

        if(pdatainfo->rec_info.data_name > 0)
        {
            /*
                Read record by record if
                'datainfo.recsize' != 'blockhead.recsize'
                and check for maximum number of records...
            */
            rec_diff   = blockhead.rec_size - pdatainfo->rec_info.rec_size;
            count_diff = blockhead.num_rec  - pdatainfo->rec_info.num_rec;

            if(count_diff > 0)
            {
                /* Too much data in file */
                blockhead.num_rec = pdatainfo->rec_info.num_rec;
            }

            if(rec_diff == 0)
            {
                if(! fread(pdatainfo->pdata, blockhead.rec_size, blockhead.num_rec, f))
                {
                    /* Read failed */
                    fclose(f);
                    return 0;
                }

                if(count_diff > 0)
                {
                    /* Skip the overflow */
                    fseek(f, blockhead.rec_size * count_diff, SEEK_CUR);
                }
            }
            else
            {
                /* Read record by record, if size doesn't fit... */
                rec_size = blockhead.rec_size;

                if(rec_diff > 0)
                {
                    /* Bigger in file then in module */
                    rec_size = pdatainfo->rec_info.rec_size;
                }

                while(blockhead.num_rec > 0)
                {
                    if(! fread(pdatainfo->pdata, rec_size, 1, f))
                    {
                        /* Read failed */
                        fclose(f);
                        return 0;

                    }

                    /* Read as much as possible... */
                    if(rec_diff > 0)
                    {
                        /* Size in file is bigger then given by module */
                        fseek(f, rec_diff, SEEK_CUR);
                    }

                    pdatainfo->pdata += pdatainfo->rec_info.rec_size;

                    blockhead.num_rec--;
                }
                /* while(blockhead.num_rec > 0) */

                if(count_diff > 0)
                {
                    fseek(f, blockhead.rec_size * count_diff, SEEK_CUR);
                }
            }
        }
        /* if(pdatainfo->rec_info.data_name > 0) */
    }

    /* Close input file */
    fclose(f);

    return 1;
}

/*
 * Name:
 *     fscfile_save
 * Description:
 *     Saves game with given number. If 'game_no' == 0 then it's an autosave.
 *     FIXME: Add error checking
 * Input:
 *     game_no:   Number of game to save
 *     game_name: Name of the game to save (for later use)
 * Output:
 *     Success yes / no.
 */
int fscfile_save(int game_no)
{
    FSCFILE_DATADESC_T datainfo[50], *pdatainfo;
    FILE *f;
    char filename[256];


    f = fopen(fscfile_create_name(game_no, filename, 0), "wb");

    if(! f)
    {
        return 0;
    }

    /* Now  get all the data information from the different modules */
    fscfile_get_all_datainfo(datainfo, 1);

    /* Write file-signature */
    fwrite(File_Sign, 6, 1, f);

    /* Now write the different blocks */
    pdatainfo = datainfo;

    while(pdatainfo->rec_info.data_name > 0)
    {
        /* Write header */
        fwrite(&pdatainfo->rec_info, sizeof(FSCFILE_DATAHEAD_T), 1, f);
        /* Write data itself */
        fwrite(pdatainfo->pdata, pdatainfo->rec_info.rec_size, pdatainfo->rec_info.num_rec, f);

        pdatainfo++;
    }

    fclose(f);
    return 1;
}

/*
 * Name:
 *     fscfile_get_namelist
 * Description:
 *     Returns a list of pointers on names of savegames. Starting with
 *     number 1. Beause numebr 0 is autosave.
 * Input:
 *     psave_names: Pointer on a pbuffer where to put the savegame names
 *     buf_size:    Size of buffer 'psave_names' for game names
 *     name_len:    Maximum lenght of game name
 * Output:
 *     Number of save game files available (FSCFILE_MAXSAVE)
 */
int fscfile_get_namelist(char *psave_names, int buf_size, int name_len)
{
    int game_no;
    char *pact_name;


    pact_name = psave_names;
    /* For checking of buffer available */
    buf_size  -= 32;

    for(game_no = 1; game_no <= FSCFILE_MAXSAVE; game_no++)
    {
        if(! fscfile_read_savename(game_no, pact_name))
        {
            /* Number of games */
            return (game_no - 1);
        }

        pact_name += 32;

        /* Check for pbuffer left */
        if((pact_name - psave_names) < name_len)
        {
            /* Number of games */
            return (game_no - 1);
        }
    }

    return FSCFILE_MAXSAVE;
}

