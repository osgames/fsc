/*******************************************************************************
*  EFFECT.C                                                                    *
*	        - Definitions of effects for the game                   	       *
*                                                                              *
*   FREE SPACE COLONISATION			                                           *
*      (c)2002 - 2017 Paul Mueller <muellerp61@bluewin.ch>                     *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*******************************************************************************/


/*******************************************************************************
* INCLUDES                                                                     *
*******************************************************************************/

#include <string.h>


#include "fscmap.h"     /* fscmapSetReachable()     */
#include "fscshare.h"   /* FSC_SIGNCMP              */
#include "colony.h"
#include "player.h"
#include "rules.h"      /* RULES_RANGE_MOVE         */
#include "starsys.h"


#include "effect.h"

/*******************************************************************************
* DEFINES                                                                      *
*******************************************************************************/

/*******************************************************************************
* DATA                                                                         *
*******************************************************************************/


/*******************************************************************************
* CODE                                                                         *
*******************************************************************************/

/*
 * Name:
 *     effect_set_map
 * Description:
 *     Sets the given effect for the map 'GER_MAP'
 * Input:
 *     pge *: Points on effects to apply
 *     ppi *: Pointer on position and owner info
 */
static void effect_set_map(GAME_EFFECT_T *pge, FSCMAP_POSINFO_T *ppi)
{
    char range_radius[RULES_RANGE_MAX];
    int  range_no;


    switch(pge->type)
    {
        case GVAL_RANGE:
            /* Info about ranges of this player */
            player_get_ranges(ppi->owner, range_radius);

            for(range_no = 0; range_no < RULES_RANGE_MOVE; range_no++)
            {
                fscmap_set_range(ppi, range_no, range_radius[range_no]);
            }
            break;

        case GVAL_SENSORS:
            fscmap_set_vision(ppi, pge->amount, 0);
            break;

        case GVAL_INFLUENCE:
            player_get_ranges(ppi->owner, range_radius);
            fscmap_set_influence(ppi,
                                 range_radius[RULES_RANGE_INFLUENCE],
                                 (char)pge->amount);
            break;
    }
}

/* ========================================================================== */
/* ======================== PUBLIC FUNCTION(S) ============================== */
/* ========================================================================== */

/*
 * Name:
 *     effect_set
 * Description:
 *     Adds an effect to the players game, based on it's type
 * Input:
 *     pge *: Points on effects to apply
 *     ppi *: Pointer on info about position and owner
 *     colony_no: Affects this colony
 *     planet_no: Affects this planet
 */
void effect_set(GAME_EFFECT_T *pge, FSCMAP_POSINFO_T *ppi, int colony_no, int planet_no)
{
    if(pge->amount > 0)
    {
        switch(pge->range)
        {
            case GER_COLONY:
                /* Affects a colony */
                colony_add_bonus(colony_no, pge->type, pge->amount);
                break;

            case GER_PLANET:
                /* Affects a planet */
                starsys_set_planetupgrade(planet_no, pge->type, pge->amount);
                break;

            case GER_PLAYER:
                /* Affects the player */
                player_add_bonus(ppi->owner, pge->type, pge->amount);
                break;

            case GER_GALAXY:
                /* @TODO: Affects the galaxy -- special YIELDS_         */
                /* fscmap_add_effect(); ?? */
                /* e.g: Adversaries can't wage war against this player ? */
                break;

            case GER_UNIT:
                /* Affects ships -- Effect value in points (to remove, if needed) */
                break;

            case GER_MAP:
                /* Affects the map */
                effect_set_map(pge, ppi);
                break;

            case GER_OUTPOST:
                break;

            case GER_AIINFO:
                break;
        }
    }
}

/*
 * Name:
 *     effect_calc_value
 * Description:
 *     Returns the cumulated value of given 'GAME_EFFECT_T'-s, based on
 *     given weights.
 * Input:
 *     pge *:      Pointer on effects to calculate
 *     weights *:  Pointer on array with weigths for the different YIELDS
 *     num_effect: Number of effects in 'pge'
 */
int effect_calc_value(GAME_EFFECT_T *pge, char *pweights, int num_effect)
{
    int i, value, weight;


    value = 0;

    for(i = 0; i < num_effect; i++)
    {
        weight = (int)(pweights[(int)pge->type]);
        value += (int)pge[i].amount * (int)pge[i].range * weight;
    }

    return value;
}
