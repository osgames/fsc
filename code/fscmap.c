/*******************************************************************************
*  FSCMAP_T.C                                                                  *
*	    - Declarations and functions for a Free Space Colonization map	       *
*                                                                              *
*	FREE SPACE COLONISATION               	                                   *
*      (c)2002 - 2017 Paul Mueller <muellerp61@bluewin.ch>                     *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*******************************************************************************/

/* *****************************************************************************
* INCLUDES								                                       *
*******************************************************************************/

#include <stdio.h>      /* sprintf() */
#include <string.h>     /* memset() */


#include "fscshare.h"   /* FSC_MAJORRACES_MAX   */
#include "fsctool.h"    /* fsctool_rand_no()    */


#include "player.h"
#include "rules.h"
#include "starsys.h"
#include "unit.h"       /* Handling of units on map */


#include "fscmap.h"

/* *****************************************************************************
* DEFINES 								                                       *
*******************************************************************************/

/* -------- Map flags ----- */
#define FSCMAP_MAXXSIZE    (9 * 12)
#define FSCMAP_MAXYSIZE    (9 * 12)
#define FSCMAP_TILE_MAX    ((FSCMAP_MAXXSIZE * FSCMAP_MAXYSIZE) + 2)
#define FSCMAP_SECTOR_MAX  128
#define FSCMAP_PLAYERMAP_SIZE ((FSC_MAJORRACES_MAX + 1) * (FSCMAP_TILE_MAX + 2))
#define FSCMAP_PLAYERMAP_BASE(player_no) ((Fsc_Map.map.num_tiles * (int)player_no))

/* -------- Direction flags ----- */
#define FSC_MAP_FNORTH  0x01
#define FSC_MAP_FEAST   0x02
#define FSC_MAP_FSOUTH  0x04
#define FSC_MAP_FWEST   0x08

/* ------ Flags for knowledge and ranges ----------------------------------- */
#define FSCMAP_FRANGE_0            0x01  /* Movement Range 0 possible        */
#define FSCMAP_FRANGE_1            0x02  /* Movement Range 1 possible        */
#define FSCMAP_FRANGE_2            0x04  /* Movement Range 2 possible        */
#define FSCMAP_FSTAR_PRESENT       0x08  /* There's a star on this tile,     */
                                         /* Maybe known                      */
#define FSCMAP_FUNIT_PRESENT       0x10  /* There's a unit on this tile      */
#define FSCMAP_FTILE_SEEN          0x20  /* We see this tile by unit         */
#define FSCMAP_FTILE_EXPLORED      0x40  /* We explored this tile (planets)  */
#define FSCMAP_FTILE_VISIBLE       0x80  /* Vision by settled planets        */
#define FSCMAP_FTILE_VISION        (FSCMAP_FTILE_SEEN | FSCMAP_FTILE_VISIBLE)
#define FSCMAP_FPLANET_PRESENT     (FSCMAP_FSTAR_PRESENT | FSCMAP_FTILE_EXPLORED)

/* *****************************************************************************
* TYPEDEFS 								                                       *
*******************************************************************************/

typedef struct
{
    SDLGLMAP_INFO_T map;
    char sector_w,
         sector_h;          /* Number of sectors                */
    char tiles_sector;      /* Tiles per sector                 */
    char num_player;        /* Number of players on this map    */
}
FSCMAP_T;



/* === The general game map, which is known by the game and is saved === */
typedef struct
{
    char flags;         /* FSCMAP_FSTAR_PRESENT / FSCMAP_UNIT_PRESENT   */
    char influencedby;  /* Influenced by this player (number)           */
    char influence;     /* Influence counter > 100: (101) Territory     */
    char owner;         /* Owned by this player (number)                */
                        /* TODO: #RULES#: Use value of military might, too */
}
FSCMAP_TILE_T;

typedef struct
{
    char owner;         /* Color of owner of this sector 1..6           */
                        /* If there's an owner, the sector has a name   */
                        /* like: blablah - Sector (%d-%d)               */
    char name[FSC_NAMELEN_MAX + 1];
    char discovered;    /* by which player, for display of name         */
}
FSCMAP_SECTOR_T;

/* Global map, generated as a game is started, only needed while game runs */
typedef struct
{
    int  unit_no;       /* Number of first unit on tile, if any         */
    int  star_no;       /* Number of star on this tile                  */
    char fow[8];        /* Fog of war: Counter for each BIG player      */
    char star_type;     /* For tests and fast response: Color of star   */
    char owner;         /* For tests and fast response: Owner of tile   */
}
FSCMAP_GAMETILE_T;

/* *****************************************************************************
* DATA    								                                       *
*******************************************************************************/
                                /* 8, 12, 16, 24, 36, 32 , 48 */
static int Count_Per_Radius[] = { 0, 8, 12, 16, 24, 36, 32 , 48, 0  };

/* === The general data which desribes the game map, standard data for test purposes ==== */
static FSCMAP_T Fsc_Map =
{
    {
        /* Type of map SDLGLMAP_TYPE_*          */
        0, 0, 36, 36, 1296,
        /* -------- Visible window of map (players viewport) ------ */
        { 0, 0, 16, 12, 16, 12 }
    },
    3, 3,   /* Number of sectors: w/h */
    12,     /* Tiles per sector                 */
    8       /* Number of players on this map    */
};


/* ========= Tile info ============ */
static unsigned char Range_Flags[6] =
{
    FSCMAP_FRANGE_0, FSCMAP_FRANGE_1, FSCMAP_FRANGE_2
};

static FSCMAP_SECTOR_T   Sectors[FSCMAP_SECTOR_MAX];
static FSCMAP_TILE_T     Tiles[FSCMAP_TILE_MAX];
static FSCMAP_GAMETILE_T Global_Map[FSCMAP_TILE_MAX];
/* -- The map bits for the different players (visibility and other knowledge) */
static unsigned char Player_Map[FSCMAP_PLAYERMAP_SIZE];

/* ******************************************************************************
* CODE  								                                        *
********************************************************************************/

/*
 * Name:
 *     fscmap_calc_influence
 * Description:
 *     Calculates the influence for tile at given position
 * Input:
 *      player_no: Influence for this player
 *      pos:       At this position
 *      value:     Influence value to use
 * Output:
 *      > 0: Ownership taken by caller
 */
static int fscmap_calc_influence(char player_no, int pos, char value)
{
    FSCMAP_TILE_T *ptile;
    int newinfluence;


    ptile = &Tiles[pos];

    if(ptile->owner > 0)
	{
		/* Tile is already owned    */
        if(ptile->owner == player_no)
		{
			/* Owned by caller          */
            /* Step up with single points -- better (longer) ownership */
            if(ptile->influence < 120)
			{
                ptile->influence++;
            }
        }
        else
		{
			/* Reduce the actual owners influence */
            if(ptile->influence > 101)
			{
                /* Strong ownership reduces slow */
                ptile->influence -= (char)(value / 10);
            }
            else
			{
                /* Ownership is weakened */
                ptile->influence -= (char)value;
            }

            if(ptile->influence < 70)
			{
                /* Looses ownership, keeps influence */
                ptile->owner = 0;
            }
        }
    }
    else
	{
		/* Not owned yet, calc influence */
        if(ptile->influencedby > 0)
		{
			/* Is influenced at all */
            newinfluence = ptile->influence;

            if(ptile->influencedby == player_no)
			{
                /* Accumulate influence */
                newinfluence += value;
            }
            else
			{
                newinfluence -= value;
                if(newinfluence < 0)
				{
					/* Change influence to caller */
                    ptile->influencedby = player_no;
                    newinfluence = -newinfluence;
                }
            }
        }
        else
		{
			/* First time influence */
            ptile->influencedby = player_no;
            newinfluence = value;
        }

        if(newinfluence >= 100)
		{
			/* Ownership falls to the caller */
            ptile->owner = player_no;
            newinfluence = 101;
        }

        ptile->influence = (char)newinfluence;
    }   /* (ptile->owner > 0) */

    return (ptile->owner == player_no);
}

/*
 * Name:
 *     fscmap_set_pos_flags
 * Description:
 *     Sets the knowledge flags for this tile.
 * Input:
 *      pos:       At this position
 *      player_no: For this player
 *      flags:     Flag-Bits to set
 */
static void fscmap_set_pos_flags(int pos, char player_no, unsigned char flags)
{
    int i;
    unsigned char *pplayermap;


    /* Given position is seen in any case */
    pplayermap = &Player_Map[pos];

    /* ------- Set the tile info for the flags ------------ */
    *pplayermap     |= flags;  /* -- Global player is omniscent */
    Tiles[pos].flags |= flags;  /* Do update of global tile      */

    /* All players which see this tile or share the view get the knowledge */
    /* But only, if it's a unit or a star                                  */
    if(flags & (FSCMAP_FUNIT_PRESENT | FSCMAP_FSTAR_PRESENT))
	{
        for(i = 1; i <= Fsc_Map.num_player; i++)
		{
            /* Next player */
            pplayermap += Fsc_Map.map.num_tiles;

            /* Propagate the presence of a star to any player */
            if(flags & FSCMAP_FSTAR_PRESENT)
			{
                *pplayermap |= FSCMAP_FSTAR_PRESENT;
            }

            /* Propagate the knowledge of a unit to any player which sees it */
            if((flags & FSCMAP_FUNIT_PRESENT) && (*pplayermap & FSCMAP_FTILE_SEEN))
			{
                *pplayermap |= FSCMAP_FUNIT_PRESENT;
            }
        }
    }

    /* ---------- This player gets the knowledge for this position --- */
    Player_Map[FSCMAP_PLAYERMAP_BASE(player_no) + pos] |= flags;
}

#if 0
/*
 * Name:
 *     fscmap_pos_remove_vision
 * Description:
 *     Removes the players vision from this tile.
 * Input:
 *      pos:          At this position
 *      player_no:    For this player
 *      pplayermap *: Pointer on map to use
 */
static void fscmap_pos_remove_vision(int pos, int player_no, unsigned char *pplayermap)
{
    Global_Map[pos].fow[player_no]--;

    if(Global_Map[pos].fow[player_no] <= 0)
	{
        Global_Map[pos].fow[player_no] = 0;
        pplayermap[pos] &= (unsigned char)(~FSCMAP_FTILE_SEEN);
    }
}


/*
 * Name:
 *     fscmap_remove_vision
 * Description:
 *     Remove this knowledge from this tile. Does an update to the knowledge
 *     of the players who share what the player sees ore see this tile.
 * Input:
 *      pos:       At this position
 *      player_no: For this player
 *      radius:    For this radius: 0: Given position
 */
static void fscmap_remove_vision(int pos, char player_no, char radius)
{
    int pos_list[180];
    int num_pos, i;
    int *prange_list;     /* Pointer of all positions in range */
    unsigned char *pplayermap;


    pplayermap = &Player_Map[FSCMAP_PLAYERMAP_BASE(player_no)];

    prange_list = fscmap_pos_get_radiuslist(pos, radius, pos_list);

    fscmap_pos_remove_vision(pos, player_no, pplayermap);

    while((*prange_list) >= 0)
	{
        fscmap_pos_remove_vision(*prange_list, player_no, pplayermap);

        prange_list++;
    }
}
#endif

#if 0
/*
 * Name:
 *     fscmap_unfog_area
 * Description:
 *     Removes a the fog of war for given area. Sets 'knownledge'.
 * Input:
 *     player_no:    For this player
 *     pos:          Position to unfog area for
 *     radius:       Radius_XY to unfog
 *     pplayermap *: Map to work with
 */
static void fscmap_unfog_area(int player_no, int pos, char radius, unsigned char *pplayermap)
{
    int pos_list[180];
    int num_pos, i;
    int *prangelist;     /* Pointer of all positions in range */


    /* Given position is seen in any case */
    pplayermap[pos] |= (unsigned char)(FSCMAP_FTILE_SEEN | Tiles[pos].flags);
    Global_Map[pos].fow[player_no]++;        /* Increment vision counter */

    if(radius > 0)
	{
        prangelist = fscmap_pos_get_radiuslist(pos, radius, pos_list);

        while((*prangelist) >= 0)
		{
            /* --- Set seen for 'passive' vision and get what comes 'in sight' --- */
            pplayermap[*prangelist] |= (unsigned char)(FSCMAP_FTILE_SEEN | Tiles[*prangelist].flags);
             /* Increment vision counter */
            Global_Map[*prangelist].fow[player_no]++;

            prangelist++;
        }
    }
}
#endif

/*
 * Name:
 *     fscmap_get_adjacent
 * Description:
 *     Generates a list of tile positions adjacent to the given
 *     position. Invalid positions are returned as special position
 *     numbers (FSCMAP_T_OFF*)
 *     -------
 *     |7|0|1|
 *     |-+-+-|
 *     |6| |2|
 *     |-+-+-|
 *     |5|4|3|
 *     -------
 *
 * Input:
 *     ppi *:         Position descriptor
 *     map_pos:       Map position to get the adjacent list for
 *     padjposlist *: Where to return the list of positions
 *     range_no:      Number of range to check for movement
 * Output:
 *     Number of tiles, that are reachable from this position
 */
int fscmap_get_adjacent(FSCMAP_POSINFO_T *ppi, int *padjposlist, int range_no)
{
    int num_reachable, dir, check_pos;
    unsigned char range_mask;       /* Used to check if given tile can be reached */
    unsigned char *pplayermap;      /* Pointer on map holding the range flags     */


    sdlglmap_get_adjacent(&Fsc_Map.map, ppi->x, ppi->y, padjposlist);

    pplayermap = &Player_Map[FSCMAP_PLAYERMAP_BASE(ppi->owner)];
    range_mask = Range_Flags[range_no];

    /* -------- Now check the range -------- */
    num_reachable = 0;

    for(dir = 0; dir < 8; dir++)
	{
        check_pos = padjposlist[dir];

        if(check_pos < 0)
		{
			/* Position asked for is off map */
            padjposlist[dir] = FSCMAP_OFFMAP;
        }
        else
		{
            /* Position in map */
            if(pplayermap[check_pos] & range_mask)
			{
                /* Is in range */
                num_reachable++;
            }
            else
			{
                /* Position asked for is off range */
                padjposlist[dir] = FSCMAP_OFFRANGE;
            }
        }
    }

    return num_reachable;
}




#if 0
/*
 * Name:
 *     fscmap_range_info
 * Description:
 *     Returns 1, if given position is at the border for given movement
 *     range, for given mapbits.
 *     Sets a bit for each side of square at border:
 *      0x01: North
 *      0x02: East
 *      0x04: South
 *      0x08: West
 * Input:
 *     pos:       Position to check near radius for
 *     player_no: To check for movement
 *     range_no:  Range to check
 * Output:
 *     border_flags:  0: No border at all
 *     Is at border, yes/no (At least one border flag set)
 */
static char fscmap_range_info(int pos, char player_no, int range_no)
{
    int poslist[10];
    int num_reachable, i;
    char border_flags;
    char range_mask;
    unsigned char *pplayermap;  /* Pointer on map holding the range flags     */


    border_flags = 0;
    range_mask   = Range_Flags[range_no];
    pplayermap   = &Player_Map[FSCMAP_PLAYERMAP_BASE(player_no)];

    if(pplayermap[pos] & range_mask)
	{
        /* Only border info, if center tile in range */
        num_reachable = fscmap_get_adjacent(pos, &poslist[0], player_no, range_no);

        if(num_reachable < 8)
		{
			/* Minimum one tile is off range - -Fill in the flags */
			for(i = 0, range_mask = 0x01; i < 8; i+= 2, range_mask <<= 0x01)
			{
                if(poslist[i] < 0)
                {
                    border_flags |= range_mask;
                }
			}
        }
    }

    return border_flags;
}
#endif

/*
 * Name:
 *     fscmap_get_playermap
 * Description:
 *     Get the knowledge flags for this player.
 * Input:
 *      map_pos:
 *      player_no:
 * Output:
 *      Pointer on Knowledge-flag
 */
static unsigned char *fscmap_get_playermap(int map_pos, int player_no)
{
    return &Player_Map[FSCMAP_PLAYERMAP_BASE(player_no) + map_pos];
}

/*
 * Name:
 *     fscmap_get_playermap_xy
 * Description:
 *     Get the knowledge flags for this player.
 * Input:
 *      map_pos:
 *      player_no:
 * Output:
 *      Pointer on Knowledge-flag
 */
static unsigned char *fscmap_get_playermap_xy(int x, int y, int player_no)
{
    return &Player_Map[FSCMAP_PLAYERMAP_BASE(player_no) + (Fsc_Map.map.size_w * y) + x];
}

/*
 * Name:
 *     fscmap_get_areainfo
 * Description:
 *     Returns the tile-info for the whole map or the viewport, depending on flag
 * Input:
 *     player_no: For this player
 *     parea *:   Pointer on area to get information for
 *     pti *:     Where to return the info
 *     num_ti:    Number of elements in 'pti'
 *     fwhich:
 */
static int fscmap_get_areainfo(int player_no, SDLGLMAP_VIEWPORT_T *parea, FSCMAP_TILEINFO_T *pti, int num_ti, char fwhich)
{
    FSCMAP_POSINFO_T ppi;
    int x, y;
    int cnt;


    /* -- Clear the destination buffer -- */
    memset(pti, 0, sizeof(FSCMAP_TILEINFO_T) * num_ti);

    cnt = 0;
    num_ti--;
    ppi.owner = (char)player_no;
    ppi.y     = (char)parea->y;

    for(y = 0; y < parea->h; y++, ppi.y++)
    {
        ppi.x = (char)parea->x;

        for(x = 0; x < parea->w; x++, ppi.x++)
        {
            if(num_ti > 0)
            {
                if(fscmap_get_tileinfo(&ppi, pti, fwhich))
                {
                    /* This tile has information -- add it drawing position */
                    pti->draw_x = x;
                    pti->draw_y = y;

                    pti++;
                    num_ti--;
                    cnt++;
                }
            }
            else
            {
                /* No more buffer for tile info */
                break;
            }
        }
    }

    /* Sign end of array */
    pti->pos = -1;

    /* Number of tileinfos we've got */
    return cnt;
}


/* ========================================================================== */
/* ======================== PUBLIC FUNCTIONS	============================= */
/* ========================================================================== */

/*
 * Name:
 *     fscmap_create
 * Description:
 *     Creates a map using given data. Prepares internal 'Global Map'
 * Input:
 *     secsize_w,
 *     secsize_h:    Size in tiles
 *     tiles_sector: Tiles per sector
 *     num_player:   Number of players on this map
 *     map_size *:   Size of map in tiles
 */
int fscmap_create(GAME_STARTINFO_T *pgsi)
{

    /* Create a map using the given map data */
    memset(&Fsc_Map, 0, sizeof(FSCMAP_T));

    Fsc_Map.map.size_w = (int)(pgsi->secsize_w * pgsi->tiles_sector);
    Fsc_Map.map.size_h = (int)(pgsi->secsize_h * pgsi->tiles_sector);

    /* -------- Set the basic data -------- */
    Fsc_Map.sector_w     = pgsi->secsize_w;
    Fsc_Map.sector_h     = pgsi->secsize_h;
    Fsc_Map.tiles_sector = pgsi->tiles_sector;
    Fsc_Map.num_player   = pgsi->num_player;
    Fsc_Map.map.vp.w     = pgsi->viewport_w;
    Fsc_Map.map.vp.h     = pgsi->viewport_h;
    Fsc_Map.map.vp.min_w = pgsi->viewport_w;
    Fsc_Map.map.vp.min_h = pgsi->viewport_h;


    /* Clear knowledge and Range */
    memset(Player_Map, 0, FSCMAP_PLAYERMAP_SIZE);

    /* ==== Init the global map needed while game is running ==== */
    memset(Global_Map, 0, (FSCMAP_TILE_MAX * sizeof(FSCMAP_GAMETILE_T)));
    /* = And now the tiles of the global map = */
    memset(Tiles, 0, (FSCMAP_TILE_MAX * sizeof(FSCMAP_TILE_T)));

    pgsi->map_w = Fsc_Map.map.size_w;
    pgsi->map_h = Fsc_Map.map.size_h;

    return 1;
}

/*
 * Name:
 *     fscmap_add_star
 * Description:
 *     Adds a star to the game map.
 *     Returns an error, if there's already a star at this position.
 *     ToDo: Minimum star distance 1 Tile ?
 * Input:
 *     ppi *:   Position of star to add star to
 *     star_no: Number of star to add
 * Output:
 *     Star could be placed on map
 */
int fscmap_add_star(FSCMAP_POSINFO_T *ppi, int star_no)
{
    int map_pos;


    map_pos = (Fsc_Map.map.size_w * ppi->y) + ppi->x;

    if((Tiles[map_pos].flags & FSCMAP_FSTAR_PRESENT) || Global_Map[map_pos].star_no > 0)
    {
        /* Already a star at this position */
        return 0;
    }
    else
    {
        Global_Map[map_pos].star_no   = star_no;
        Global_Map[map_pos].star_type = fsctool_rand_no(6);

        Tiles[map_pos].flags |= FSCMAP_FSTAR_PRESENT;
    }

    /* Return an error, if there is already a star on this tile */
    return 1;
}


/*
 * Name:
 *     fscmap_unit_add
 * Description:
 *     Moves the unit to given tile and calculates all effects
 *     that it may have. (Map known / map seen and so on).
 *     It's assumed, that no enemy unit is at given tile - so a
 *     possible fight has to be resolved before calling this function!
 * Input:
 *     dest_pos: Position where to move the unit: -1: Self
 *     unit_no : Unit to move to given tile
 */
void fscmap_unit_add(int dest_pos, int unit_no)
{
#if 0
    UNIT_T *punit;
    unsigned char *pplayermap;


    punit = unit_get(unit_no);


    /* Set new position of unit */
    if(dest_pos < 0)
    {
        /* -- Use self */
        dest_pos = punit->pos;
    }
    else
    {
        punit->pos = dest_pos;
    }

    /* Put unit to map */
    unit_list_insert(&Global_Map[dest_pos].unit_no, unit_no);

    pplayermap = &Player_Map[FSCMAP_PLAYERMAP_BASE(punit->owner)];

    /* --------- Tell global map, that a unit has moved is here ------- */
    Tiles[dest_pos].flags |= FSCMAP_FUNIT_PRESENT;

    /* --------- Explore given position ------ */
    pplayermap[dest_pos] |= (FSCMAP_FTILE_EXPLORED | FSCMAP_FUNIT_PRESENT);
    pplayermap[dest_pos] |= FSCMAP_FUNIT_PRESENT;

    if(pplayermap[dest_pos] & FSCMAP_FSTAR_PRESENT)
	{
        /* Explore this star -- Generate the planets, if not avail yet */
        starsys_explore(dest_pos, Global_Map[dest_pos].star_no);
    }

    fscmap_unfog_area(punit->owner, dest_pos, punit->sensor_range, pplayermap);
#endif
}


/*
 * Name:
 *     fscmap_populate
 * Description:
 *     Must be called _after_ map is created.
 *     Puts stars and units to internal 'Global_Map'
 * Input:
 *     None
 */

void fscmap_populate(void)
{
    int list[2000], *plist;
    int pos, i;
    unsigned char *pplayermap;


    /* Puts all stars to map */
    if(starsys_get_star_poslist(0, list, 1998, STAR_POSLIST_ALL))
	{
        /* If there is a star at all */
        plist = list;

        pos = starsys_get_starpos(*plist);

        if(pos > 0)
        {
            /* Position of a valid star */
            Tiles[pos].flags |= FSCMAP_FSTAR_PRESENT;
            Global_Map[pos].star_no = *plist;

            /* And is also known by any player in game        */
            /* Propagate the presence of a star to any player */
            pplayermap = &Player_Map[pos];

            for(i = 0; i <= Fsc_Map.num_player; i++)
            {
                *pplayermap |= FSCMAP_FSTAR_PRESENT;

                /* Next players map */
                pplayermap += Fsc_Map.map.num_tiles;
            }

        }

        /* Next number of star */
        plist++;
    }
#if 0
    /* Puts all units to map */
    if(unit_get_list(-1, list, -1))
    {
        plist = list;

        /* If there are units at all */
        while(*plist)
        {
             fscmap_unit_to_pos(*plist, -1);

             plist++;
        }
    }
#endif
}



/*
 * Name:
 *     fscmap_unit_remove
 * Description:
 *     Removes the unit from given tile and calculates all effects
 *     that it may have. (Map known / map seen and so on)
 * Input:
 *     map_pos: From this position < 0: Use position of unit itself
 *     unit_no: Number of unit
 */
void fscmap_unit_remove(int map_pos, int unit_no)
{

    if(map_pos < 0)
    {
        /* Use Position of unit */

    }
#if 0
    UNIT_T *punit;
    unsigned char *pplayermap;
    unsigned char clearmask;
    int i;


    punit = unit_get(unit_no);

    fscmap_remove_vision(&punit->pi, punit->sensor_range);

    unit_list_remove(&Global_Map[punit->pos].unit_no, unit_no);

    /* Remove unit-avail-flag if there's no more unit at given position */
    if(Global_Map[punit->pos].unit_no <= 0)
	{
        pplayermap = &Player_Map[punit->pos];
        clearmask  = (unsigned char)(~FSCMAP_FUNIT_PRESENT);

        pplayermap[punit->pos] &= clearmask;
        Tiles[punit->pos].flags &= clearmask;

        /* Remove flag at all players, which can see this tile */
        for(i = 1; i <= Fsc_Map.num_player; i++)
		{
        	pplayermap += Fsc_Map.map.num_tiles;
        	if(*pplayermap & FSCMAP_FTILE_VISION)
			{
                *pplayermap &= (unsigned char)(~FSCMAP_FUNIT_PRESENT);
            }
        }
    }
#endif
}

    /* ========= Range lists ========= */

/*
 * Name:
 *     fscmap_get_long_heading
 * Description:
 *     Returns the info of the nearest tile in list
 * Input:
 *     map_pos:  Map position to get the nearest position for
 *     pti *:    Pointer on list of tiles to check
 *     num_tile: Number of tiles in 'pti'
 * Output:
 *     New heading for a moving unit
 */
char fscmap_get_long_heading(int src_pos, int curr_pos, int dest_pos)
{
    return sdlglmap_get_long_heading(&Fsc_Map.map, src_pos, curr_pos, dest_pos);
}

    /* ================ Units on map =================================== */

/*
 * Name:
 *     fscmap_unit_at
 * Description:
 *     Returns the first unit at given postion, if any
 * Input:
 *     pos: To get first unit from
 */
int fscmap_unit_at(int pos)
{
    return Global_Map[pos].unit_no;
}

#if 0
/*
 * Name:
 *     fscmapAlienUnitInRange
 * Description:
 *     Returns a value > 0, nearest alien unit in given range
 *     TODO:
 * Input:
 *     player_no: For this player
 *     map_pos:   From where to calc the range
 *     radius:    Radius_XY to check
 */
int fscmapAlienUnitInRange(int player_no, int map_pos, char radius)
{
    int pos_list[180];
    int num_pos, i;
    UNIT_T *punit_seen;
    unsigned char *pmap;
    int *prangelist;         /* Pointer of all positions in range */


    prangelist = fscmap_pos_get_radiuslist(map_pos, radius, pos_list);
    pmap       = fscmap_get_playermap(0, player_no);

    while(*prangelist >= 0)
	{
        if(pmap[*prangelist] & FSCMAP_FUNIT_PRESENT)
		{
            punit_seen = unit_get(fscmap_unit_at(*prangelist));

            if(punit_seen->owner != player_no)
			{
                return *prangelist;
            }
        }

        prangelist++;
    }

    return 0;
}
#endif

/* ========================= Info about map ==================== */

/*
 * Name:
 *     fscmap_pos_fromxy
 * Description:
 *     Returns a map position calculated from given xy
 *     If 'viewport' is set, then take viewport into account
 * Input:
 *    pos_x, pos_y: XY-Position to translate
 *    viewport:     Take viewport into account yes/no
 */
int fscmap_pos_fromxy(int pos_x, int pos_y, char viewport)
{
    if(viewport)
    {
        /* Add left top of viewport */
        pos_x += Fsc_Map.map.vp.x;
        pos_y += Fsc_Map.map.vp.y;
    }
    return ((pos_y * Fsc_Map.map.size_w) + pos_x);
}

/*
 * Name:
 *     fscmap_xy_frompos
 * Description:
 *     Create XY-Coordinates from given pos in tile-array
 * Input:
 *     map_pos: Position in tilelst to get XY-Position for
 *     ppi *:   Where to return the position
 */
void fscmap_xy_frompos(int map_pos, FSCMAP_POSINFO_T *ppi)
{
    ppi->x = (char)(map_pos % Fsc_Map.map.size_w);
    ppi->y = (char)(map_pos / Fsc_Map.map.size_w);
}


    /* ================= Map manipulation ============== */

/*
 * Name:
 *     fscmap_set_range
 * Description:
 *     Sets the tiles on 'reachable' for given player.
 *     The callers position is included
 * Input:
 *     ppi *:    Position to set reachability for
 *     range_no: For this range (For Map-Infoflags)
 *     radius:   For this radius
 */
void fscmap_set_range(FSCMAP_POSINFO_T *ppi, int range_no, int radius)
{
    int pos_list[180], *ppos_list;
    unsigned char range_mask;
    unsigned char *pplayermap;   /* Pointer on map holding the info flags   */


    pplayermap = &Player_Map[FSCMAP_PLAYERMAP_BASE(ppi->owner)];
    ppos_list  = sdlglmap_get_radiuslist(&Fsc_Map.map, ppi->x, ppi->y, radius, pos_list);
    range_mask = Range_Flags[range_no];


    while((*ppos_list) >= 0)
    {
        pplayermap[*ppos_list] |= range_mask;
        ppos_list++;
    }
}

/*
 * Name:
 *     fscmap_set_flags
 * Description:
 *     Sets the knowledge flags for all tiles in range
 *     of the players who are in 'sharemask'.
 * Input:
 *     ppi *:  Position to set flags for
 *     radius: For this radius: 0: Given position
 *     flags:  Flag-Bits to set
 */
void fscmap_set_flags(FSCMAP_POSINFO_T *ppi, char radius, unsigned char flags)
{
    int pos_list[180];
    int *prangelist;     /* Pointer of all positions in range */
    int map_pos;


    map_pos = fscmap_pos_fromxy(ppi->x, ppi->y, 0);
    fscmap_set_pos_flags(ppi->owner, map_pos, flags);

    if(radius > 0)
	{
        prangelist = sdlglmap_get_radiuslist(&Fsc_Map.map, ppi->x, ppi->y, radius, pos_list);

        while((*prangelist) >= 0)
		{
            fscmap_set_pos_flags(ppi->owner, *prangelist, flags);

            prangelist++;
        }
    }
}

/*
 * Name:
 *     fscmap_set_vision
 * Description:
 *     Sets positions in given radius from given position to state 'visible'.
 * Input:
 *     ppi *:  Position to set flags for
 *     radius: For this radius: 0: Given position
 *     info:   Set additional flags
 */
void fscmap_set_vision(FSCMAP_POSINFO_T *ppi, int radius, char info)
{
    char flags;


    flags = FSCMAP_FTILE_SEEN | FSCMAP_FTILE_VISIBLE;

    if(info)
	{
        fscmap_set_flags(ppi, radius, FSCMAP_FTILE_EXPLORED);
    }

    fscmap_set_flags(ppi, radius, flags);
}

/*
 * Name:
 *     fscmap_set_influence
 * Description:
 *     Sets the owner at given position
 * Input:
 *     ppi *:  Position to set flags for
 *     radius:    Set influence for this radius
 *     value:     Value of influence
 */
void fscmap_set_influence(FSCMAP_POSINFO_T *ppi, int radius, char value)
{
    int pos_list[180], *ppos_list;
    int rcount, i, map_pos;


    /* First position is center with maximum value */
    if(value >= 100)
    {
        Global_Map[map_pos].owner = ppi->owner;
    }
    else
    {
        map_pos = fscmap_pos_fromxy(ppi->x, ppi->y, 0);

        fscmap_calc_influence(ppi->owner, map_pos, value);
    }

    if(radius > 0)
	{
        ppos_list = sdlglmap_get_radiuslist(&Fsc_Map.map, ppi->x, ppi->y, radius, pos_list);

        for(rcount = 1; rcount <= radius; rcount++)
		{
            /* Falloff: Half the influence of previous radius */
            value /= 2;

            if(value <= 0)
			{
                return;     /* No influence to calc anymore */
            }

            for(i = 0; i < Count_Per_Radius[rcount]; i++)
			{
                if(*ppos_list < 0)
				{
                    /* End of list */
                    return;
                }

                fscmap_calc_influence(ppi->owner, *ppos_list, value);

                ppos_list++;
            }
        }
    }
}

/* ============= Movement of visible map window (ViewPort) ============ */

/*
 * Name:
 *     fscmap_viewport_move
 * Description:
 *     Moves the map window in given direction, if possible.
 *     0..3: North/east/south/west
 * Input:
 *      direction: Direction to move window to.
 */
void fscmap_viewport_move(int direction)
{
    sdlglmap_viewport_move(&Fsc_Map.map, direction);
}

/*
 * Name:
 *     fscmap_viewport_adjust
 * Description:
 *     Adjusts the viewport to given position. Centers it to position if
 *     needed of forced.
 * Input:
 *     pos_x, pos_y: Position to adjust to
 *     pos:          Position to adjust viewport to
 *     center:       If true, center to given position in any case
 *     is_viewpos:   Given position is position in viewport, otherwise a map position
 *
 */
void fscmap_viewport_adjust(int pos_x, int pos_y, char center, char is_viewpos)
{
    sdlglmap_viewport_adjust(&Fsc_Map.map, pos_x, pos_y, center, is_viewpos);
}

/*
 * Name:
 *     fscmap_get_info
 * Description:
 *     Returns the info about he map size and its size in sectors
 * Input:
 *     pinfo *:
 *      Returns Map-Info: width, height in tiles
 *                        width, height in sectors
 *
 */
void fscmap_get_info(FSCMAP_INFO_T *pinfo)
{
    /* - -Retrurn the info about the different extents */
    pinfo->map_w = Fsc_Map.map.size_w;
    pinfo->map_h = Fsc_Map.map.size_h;
    pinfo->sec_w = Fsc_Map.sector_w;
    pinfo->sec_h = Fsc_Map.sector_h;
    pinfo->vp_x  = Fsc_Map.map.vp.x;
    pinfo->vp_y  = Fsc_Map.map.vp.y;
    pinfo->vp_w  = Fsc_Map.map.vp.w;
    pinfo->vp_h  = Fsc_Map.map.vp.h;
}

/*
 * Name:
 *     fscmap_get_tileinfo
 * Description:
 *     Returns the info about stars and units on a single tile
 * Input:
 *     ppi *:  Pointer on position info
 *     pti *:  Where to return the info
 *     fwhich: Flags: Which info to return (This about stars is always returned!
 * Output:
 *     knowledge, if any
 */
char fscmap_get_tileinfo(FSCMAP_POSINFO_T *ppi, FSCMAP_TILEINFO_T *pti, char fwhich)
{
    UNIT_T *punit;
    int unit_no, map_pos;
    char star_owner;
    char info;


    /* Clear Buffer, if needed */
    if(fwhich & FSCMAP_FINFO_CLEAR)
    {
        memset(pti, 0, sizeof(FSCMAP_TILEINFO_T));
    }

    map_pos = fscmap_pos_fromxy(ppi->x, ppi->y, 0);

    /* === Player 0 knows every star === */
    if(ppi->owner == 0)
    {
        if(Global_Map[map_pos].star_no > 0)
        {
            pti->pos       = map_pos;
            pti->star_type = Global_Map[map_pos].star_type;
            pti->owner     = Global_Map[map_pos].owner;
            return 1;
        }

        return 0;
    }

    /* == General information... === */
    pti->pos          = map_pos;
    pti->star_name[0] = 0;      /* Set a valid pointer in any case  */
    pti->star_type    = -1;     /* Assume: No star at this position */


    pti->knowledge = *fscmap_get_playermap(map_pos, ppi->owner);
    /* Anybody knows the stars. 2017-07-18 <bitnapper> */
    pti->knowledge |= (Tiles[map_pos].flags & FSCMAP_FSTAR_PRESENT);

    /* ------------------ */
    if(pti->knowledge)
	{
        /* If the player has any knowledge at all about this tile... */
        if(fwhich & FSCMAP_FINFO_STAR)
        {
            if(pti->knowledge & (FSCMAP_FTILE_EXPLORED | FSCMAP_FTILE_VISION))
            {
                /* The player knows the owner of the star */
                /* @TODO: Only show ownership, if player has contact with owner */
                info = (pti->knowledge & FSCMAP_FTILE_EXPLORED);

                star_owner = starsys_get_starinfo(map_pos,
                                                  &pti->star_type,
                                                  pti->star_name,
                                                  pti->star_planets,
                                                  info);

                if(star_owner == ppi->owner)
                {
                    /*
                        #RULES: Special case: Not enough influence for tile ownership,
                               but claimed ownership by given player (colony)
                    */
                    pti->owner = star_owner;
                }
            }
        }
        if(fwhich & FSCMAP_FINFO_OWNER)
        {
            if(Tiles[map_pos].owner > 0)
            {
                if(pti->knowledge & (FSCMAP_FTILE_EXPLORED | FSCMAP_FTILE_VISION))
                {
                    pti->owner = Tiles[map_pos].owner;
                }
            }
        }
        if(fwhich & FSCMAP_FINFO_INFLUENCE)
        {
            if(pti->knowledge & (FSCMAP_FTILE_EXPLORED | FSCMAP_FTILE_VISION))
            {
                /* @todo: Only show influence, if other player is known */
                pti->owner = Tiles[map_pos].owner;

                pti->influenced_by  = Tiles[map_pos].influencedby;
                pti->influence_proz = Tiles[map_pos].influence;
                return 1;
            }
        }
        if(fwhich & FSCMAP_FINFO_UNIT)
        {
            if(pti->knowledge & FSCMAP_FUNIT_PRESENT)
            {
                /* Number of first unit, if any   */
                unit_no = Global_Map[map_pos].unit_no;

                if(unit_no > 0)
                {
                    pti->unit_no = unit_no;

                    punit = unit_get(unit_no);
                    /* Fill in info about first unit... */
                    pti->unit_type  = punit->type;
                    pti->unit_owner = punit->pi.owner;
                }
            }
        }
    }

    return pti->knowledge;
}

/*
 * Name:
 *     fscmap_get_mapinfo
 * Description:
 *     Returns the tile-info for the whole map or the viewport, depending on flag
 * Input:
 *     player_no: For this player
 *     pti *:     Where to return the info
 *     int num_ti: Number of elements in 'pti'
 *     fwhich:     Flags:
 *     get_vp:     Get it for vieport yes/no
 */
int fscmap_get_mapinfo(int player_no, FSCMAP_TILEINFO_T *pti, int num_ti, char fwhich, char get_vp)
{
    SDLGLMAP_VIEWPORT_T area;


    if(get_vp)
    {
        /* Get the information about the viewport */
        memcpy(&area, &Fsc_Map.map.vp, sizeof(SDLGLMAP_VIEWPORT_T));
    }
    else
    {
        area.x = 0;
        area.y = 0;
        area.w = Fsc_Map.map.size_w;
        area.h = Fsc_Map.map.size_h;
    }

    return fscmap_get_areainfo(player_no, &area, pti, num_ti, fwhich);
}

/*
 * Name:
 *     fscmap_get_mapinfo
 * Description:
 *     Returns the tile-info for the whole map or the viewport, depending on flag
 * Input:
 *     player_no: For this player
 *     pti *:     Where to return the info
 *     int num_ti: Number of elements in 'pti'
 *     range_no:   For this range-no (0..2)
 *     get_vp:     Get it for vieport yes/no
 */
int fscmap_get_mapinfo_range(int player_no, FSCMAP_POSINFO_T *pti, int num_ti, int range_no, char get_vp)
{
    SDLGLMAP_VIEWPORT_T area;
    FSCMAP_POSINFO_T pi;
    int adjacent_pos[8];
    int x, y;
    int cnt, i;
    char set_flag;


    if(get_vp)
    {
        /* Get the information about the viewport */
        memcpy(&area, &Fsc_Map.map.vp, sizeof(SDLGLMAP_VIEWPORT_T));
        area.x = Fsc_Map.map.vp.x;
        area.y = Fsc_Map.map.vp.y;
        area.w = Fsc_Map.map.vp.w;
        area.h = Fsc_Map.map.vp.h;
    }
    else
    {
        area.x = 0;
        area.y = 0;
        area.w = Fsc_Map.map.size_w;
        area.h = Fsc_Map.map.size_h;
    }



    /* -- Clear the destination buffer -- */
    memset(pti, 0, sizeof(FSCMAP_POSINFO_T) * num_ti);

    cnt = 0;
    num_ti--;
    pi.y = (char)area.y;

    for(y = 0; y < area.h; y++, pi.y++)
    {
        pi.x = (char)area.x;

        for(x = 0; x < area.w; x++, pi.x++)
        {
            if(num_ti > 0)
            {
                if(fscmap_get_adjacent(&pi, adjacent_pos, range_no))
                {
                    /* If any position can be reached at all */
                    for(i = 0, set_flag = 0x01; i < 8;  i += 2, set_flag <<= 1)
                    {
                        if(adjacent_pos[i] >= 0)
                        {
                            pti->range |= set_flag;
                        }
                    }

                    if(pti->range)
                    {
                        /* This tile has information -- add its drawing position */
                        pti->owner = 1;
                        pti->x = (char)x;
                        pti->y = (char)y;

                        pti++;
                        num_ti--;
                        cnt++;
                    }
                }
            }
            else
            {
                /* No more buffer for tile info */
                break;
            }
        }
    }

    /* Sign end of array */
    pti->owner = 0;

    /* Number of tileinfos we've got */
    return cnt;
}

/*
 * Name:
 *     fscmap_get_distinfo
 * Description:
 *     Get information about te square from given position with 'dist'
 *     on both sides: e.g: dist=1 is a square of 3 by 3 tiles
 * Input:
 *     player_no: For this player
 *     map_pos:   Around this map position
 *     dist:      Square distance
 *     pti *:     To fill with information
 *     num_ti:    Number of elements in 'pti'
 *     fwhich:    Which information to return
 * Output:
 *     Number of positions in list
 */
int fscmap_get_distinfo(FSCMAP_POSINFO_T *ppi, int dist, FSCMAP_TILEINFO_T *pti, int num_ti, char fwhich)
{
    SDLGLMAP_VIEWPORT_T area;

    /* Area is always a square */
    area.x = (int)ppi->x - dist;
    area.y = (int)ppi->y - dist;
    area.w = (dist * 2) + 1;
    area.h = area.w;

    sdlglmap_clamp_area(&Fsc_Map.map, &area);

    return fscmap_get_areainfo(ppi->owner, &area, pti, num_ti, fwhich);
}

/*
 * Name:
 *     fscmap_get_rangeinfo
 * Description:
 *     Get information about te square from given position with 'dist'
 *     on both sides: e.g: dist=1 is a square of 3 by 3 tiles
 * Input:
 *     ppi *:     Pointer on position-descriptor to get information for
 *     range_no:  For this range
 *     pti *:     To fill with information
 *     num_ti:    Number of elements in 'pti'
 *     fwhich:    Which info to look for
 * Output:
 *     Number of tile information in list
 */
int fscmap_get_rangeinfo(FSCMAP_POSINFO_T *ppi, int range_no, FSCMAP_TILEINFO_T *pti, int num_ti, char fwhich)
{
    FSCMAP_POSINFO_T pi;
    int pos_list[180], *ppos_list;
    char ranges[4];
    int radius;
    int num_info, i;


    player_get_ranges(ppi->owner, ranges);

    ppos_list = sdlglmap_get_radiuslist(&Fsc_Map.map, ppi->x, ppi->y, radius, pos_list);
    num_info = 0;
    pi.owner = ppi->owner;

    while(*ppos_list >= 0)
    {
        pi.x = (*ppos_list) % Fsc_Map.map.size_w;
        pi.y = (*ppos_list) / Fsc_Map.map.size_w;;

        if(fscmap_get_tileinfo(&pi, pti, fwhich))
        {
            pti++;
            num_ti--;
            num_info++;

            if(num_ti <= 0)
            {
                return num_info;
            }
        }

        ppos_list++;
    }

    return num_info;
}

/*
 * Name:
 *     fscmap_get_sectorinfo
 * Description:
 *     Returns the name of a sector, if available
 * Input:
 *     map_pos:   For this map position
 *     pname *:   Return the name, if any
 *     pnumber *: In the format [X]-[Y]
 * Output:
 *     Sector has a name yes/no
 */
int fscmap_get_sectorinfo(int map_pos, char *pname, char *pnumber)
{
    FSCMAP_POSINFO_T pi;
    int sector_no, sec_x, sec_y;


    /* Calculate the number of the sector.Numbers start by 1 */
    fscmap_xy_frompos(map_pos, &pi);

    sec_x = (pi.x / Fsc_Map.tiles_sector);
    sec_y = (pi.y / Fsc_Map.tiles_sector);

    /* Get the name of the sector */
    sector_no = ((sec_y * Fsc_Map.sector_w) + sec_x);

    sprintf(pnumber, "%d-%d", sec_x + 1, sec_y + 1);

    if(Sectors[sector_no].name[0] != 0)
	{
        strcpy(pname, Sectors[sector_no].name);

        return 1;
    }

    return 0;
}

/* ============= Procedures for load/save data ============ */

/*
 * Name:
 *     fscmapGetLoadSaveInfo
 * Description:
 *     If 'save' is true then fill in struct 'info' with data needed for
 *     saving data given in 'data *'. 'numrec' must hold the maximum number
 *     of records that can be filled, 'recsize' the maximum size of record to
 *     be saved.
 * Input:
 *     pdatadesc *: Pointer on struct where to return the info about the data to laad/save
 *     save:      Return info for save / load given info into game
 * Output:
 *    Number of daa descriptions in 'pdatadesc'
 */
int fscmap_get_loadsave_info(FSCFILE_DATADESC_T *pdatadesc, char save)
{
    short int num_rec;


    /* == Info about the Map-header == */
    pdatadesc->rec_info.data_name = FSCFILE_MAPHEAD;
    pdatadesc->rec_info.rec_size  = sizeof(FSCMAP_T);
    pdatadesc->rec_info.num_rec   = 1;
    pdatadesc->pdata = &Fsc_Map;

    /* == Global Tile-Info == */
    pdatadesc++;
    num_rec = (save) ? Fsc_Map.map.num_tiles : FSCMAP_TILE_MAX;

    pdatadesc->rec_info.data_name = FSCFILE_MAPGLOBAL;
    pdatadesc->rec_info.rec_size  = ((int)num_rec * sizeof(FSCMAP_TILE_T));
    pdatadesc->rec_info.num_rec   = 1;
    pdatadesc->pdata = &Tiles[0];

    /* == Tile info per player: Players knowledge of map == */
    pdatadesc++;
    pdatadesc->rec_info.data_name = FSCFILE_MAPPLAYER;
    pdatadesc->rec_info.rec_size  = (int)Fsc_Map.map.num_tiles * sizeof(char);
    pdatadesc->rec_info.num_rec   = Fsc_Map.num_player;
    pdatadesc->pdata = &Player_Map[Fsc_Map.map.num_tiles];

    /* == Number of data descriptors == */
    return 3;
}


