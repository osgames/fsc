/*******************************************************************************
*  GAME2STR.H                                                                  *
*	- Translates in-game values to human readable strings            	       *
*                                                                              *
*   FREE SPACE COLONISATION                                                    *
*       Copyright (C) 2002 - 2017 Paul Mueller <muellerp61@bluewin.ch>         *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*******************************************************************************/

#ifndef _FSC_GAME2STR_H_
#define _FSC_GAME2STR_H_

/*******************************************************************************
* ENUMS                                                                        *
*******************************************************************************/

typedef enum
{
    GAME2STR_LABEL_NONE      = 0,
    GAME2STR_LABEL_ADVISOR   = 1,
    GAME2STR_LABEL_STATS     = 2,
    GAME2STR_LABEL_UNITORDER = 3,
    GAME2STR_LABEL_EFFECT    = 4,
    GAME2STR_LABEL_RANGE     = 5,
    GAME2STR_LABEL_ABILITY   = 6,
    GAME2STR_LABEL_DIFFICULTY = 7,      /* For game setup */
    GAME2STR_LABEL_STARDENSITY = 8,      /* For game setup */
    GAME2STR_LABEL_EFFECTRANGE = 9,
    GAME2STR_LABEL_UNITNAME     = 10,
    GAME2STR_LABEL_UNITTYPENAME = 11,
    GAME2STR_LABEL_IMPROVENAME  = 12
}
E_GAME2STR_TYPE;

/* *****************************************************************************
* DEFINES      							                                       *
*******************************************************************************/

#define GAME2STR_SPEC_DATE          0
#define GAME2STR_SPEC_POPULATION    1

/* *****************************************************************************
* CODE       							                                       *
*******************************************************************************/

char *game2str_label(E_GAME2STR_TYPE which, int label_no);
char *game2str_menuname(char *pwhich);
void game2str_sector_name(char *pdest, char *pname, int sec_x, int sec_y);
void game2str_date(char *pdest, int turn_no, int base_year);
char *game2str_unit_name(int unit_no);

#endif  /* _FSC_GAME2STR_H_ */
