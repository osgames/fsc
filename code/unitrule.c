/*******************************************************************************
*  UNITRULE.C                                                                  *
*	    - Data and functions for the units movement rules             	       *
*                                                                              *
*	FREE SPACE COLONISATION               	                                   *
*   Copyright (C) 2002  Paul Mueller <pmtech@swissonline.ch>                   *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*******************************************************************************/

/*******************************************************************************
* INCLUDES								                                       *
*******************************************************************************/

#include <memory.h>
#include <string.h> 


#include "code/fsctool.h"
#include "code/nation.h"
#include "code/rules.h"
#include "code/star.h"
#include "code/unit.h"
#include "code/unitinfo.h"       /* ORDER_*   */
#include "code/unittype.h"


#include "code/unitrule.h"

/*******************************************************************************
* DEFINES								                                       *
*******************************************************************************/

#define UNITRULE_STARBASE_POINTS 6      /* How many movement points */
                                        /* Needed to build starbase */
                                        /* Three rounds for standard constructor */

/*******************************************************************************
* DATA  								                                       *
*******************************************************************************/

/* --------- Different actions as strings ------- */
static char BaseOrders[] = {   /* Commands possible for every unit */

    ORDER_SENTRY, ORDER_PATROL,
    ORDER_BORDERPATROL, ORDER_GOTO, ORDER_EXPLORE, 0

};

static char MilitaryOrders[] = {    /* Orders only possible for military units */

    ORDER_AUTOATTACK, ORDER_AUTORETREAT, 0

};

/* ------ FIXME: Change this to new usage ---- */
/* ------- This order --------- */
static char BuildSpaceOrders[]   = { ORDER_BUILD, 0 };
static char ColonizeOrders[]     = { ORDER_COLONIZE, ORDER_TERRAFORM, 0 };

/*******************************************************************************
* CODE  								                                       *
*******************************************************************************/


/*
 * Name:
 *     unitruleMilitaryOrders
 * Description:
 *     Returns a list of possible actions for given unit at given position
 * Input:
 *     pos:         Position to check for possibel actions
 *     orderlist *: Pointer on list where to return the possible actions
 */
static void unitruleMilitaryOrders(int pos, char knowledge, char *orderlist)
{

    strcat(orderlist, MilitaryOrders);
    
}

/*
 * Name:
 *     unitruleColonizeActions
 * Description:
 *     Returns a list of possible actions for given unit at given position
 * Input:
 *     pos:         Position to check for possibel actions
 *     orderlist *: Pointer on list where to return the possible actions
 */
static void unitruleColonizeActions(int pos, char *orderlist)
{

    /* FIXME: Add proper codes          */
    /* Can build an outpost, an archeological or terraform station  */
    /* if there's an unsettled star system                          */
    /* A constructor can build more of them, depending on unittype  */
    /* 'punit -> numbuild'                                          */
    if (starGet(0, pos) > 0) {

        /* FIXME: Check, if this planetary system is owned by us and so on  */
        /* FIXME: Adjust actions depending on chosen 'target'               */
        /* Colonize planets --> SETTLE      */
        /* Can terraform planets            */

        /* FIXME: Only offer terraforming, if possible */
        strcat(orderlist, ColonizeOrders);

    }

}

/*
 * Name:
 *     unitruleGetConstructActions
 * Description:
 *     Returns a list of possible actions for given unit at given position
 * Input:
 *     pos:         Position to check for possibel actions
 *     orderlist *: Pointer on list where to return the possible actions
 */
static void unitruleGetConstructActions(int pos, char *orderlist)
{

    if (! starGet(0, pos)) {

        /* TODO: Get numbers of unittypes the constructor can   */
        /*        be morphed to ?                               */
        strcat(orderlist, BuildSpaceOrders);

    }
    
}

/*
 * Name:
 *     unitruleGetDefense
 * Description:
 *     Adds up bonus's for ships that are in orbit.
 * Input:
 *     ship: Number of ship to get defense for
 */
static int unitruleGetDefense(int ship)
{

    UNIT *pship;
    int Defense;
    int subtotal;


    pship    = unitGet(ship);
    Defense  = pship -> defense;

    if (pship -> order.target > 0) {

    	Defense = Defense * 3 / 2;      /* * 1.5 */

        /* FIXME: Add defense support again ASAP */
        /* subtotal = starGetPlanet(pship -> destination) -> bonus[YIELD_DEFENSE]; */
        subtotal = 0;

    	subtotal += 100;
    	Defense  = (Defense * subtotal) / 100;

    }

    return Defense;

}

/* ========================================================================== */
/* =========================== PUBLIC FUNCTION(S) =========================== */
/* ========================================================================== */

/*
 * Name:
 *     unitruleSetBonuses
 * Description:
 *     Adds the given bonuses to the unit with given id.
 * Input:
 *     unit_no:     Number of unit to add the bonuses for
 *     bonuses *:   Pointer on bonus list
 *     attackbonus: Attack bonus in percent
 *     defbonus:    Defense bonus in percent
 *     speedbonus:  Speed bonus in percent
 */
void unitruleSetBonuses(int unitid, short int *bonuses)
{

    UNIT *punit;
    UNITTYPE *ut;
    int hitpoints, attack, defense, speed;
    int hitbonus, attackbonus, defbonus, speedbonus;


    punit = unitGet(unitid);
    ut = unittypeGet(punit -> type);
    
    /* ++++++++ Test code ++++++++ */
    if (ut -> ability == UNIT_ABILITY_TRANSPORT) {

        /* FIXME: Make only a difference between military and civil units   */
        /*        Load units aboard with slots                              */
        /* FIXME: Add slots to unit                                         */
        punit -> cargo_type = UI_CARGO_PEOPLE;
        punit -> cargo_load = (char)100;

    }

    hitbonus    = bonuses[YIELD_HITPOINTS] * punit -> moves[UI_VALUE_FULL] / 100;
    speedbonus  = bonuses[YIELD_SPEED]     * punit -> moves[UI_VALUE_FULL] / 100;
    attackbonus = bonuses[YIELD_ATTACK]    * punit -> attack / 100;
    defbonus    = bonuses[YIELD_DEFENSE]   * punit -> defense / 100;

    hitpoints = (int)punit -> hp[UI_VALUE_FULL] + hitbonus;
    speed     = (int)punit -> moves[UI_VALUE_FULL] + speedbonus;
    attack    = (int)punit -> attack  + attackbonus;
    defense   = (int)punit -> defense + defbonus;

    punit -> hp[UI_VALUE_FULL]    = (char)hitpoints;
    punit -> hp[UI_VALUE_ACT]     = (char)hitpoints;
    punit -> moves[UI_VALUE_FULL] = (char)speed;
    punit -> moves[UI_VALUE_ACT]  = (char)speed;
    punit -> attack               = (char)attack;
    punit -> defense              = (char)defense;

}

/*
 * Name:
 *     unitruleGetPosOrder
 * Description:
 *     Returns a list of possible orders given unit ability for info in
 *     'mapflags'
 * Input:
 *     ability:     Number of unit to get action-list for
 *     pos:         Position to check for possibel actions 
 *     knowledge:   Knowledge of caller about this position   
 *     orderlist *: Pointer on list where to return the possible actions
 *                  if needed. Can be 0.
 * Output:
 *     Number of actions possible
 */
int unitruleGetPosOrder(char ability, int pos, char knowledge, char *orderlist)
{

    int numorder;

   
    *orderlist = 0;                     /* Assume empty list            */
    strcpy(orderlist, BaseOrders);      /* Is possible for every unit */

    switch(ability) {

        case UNIT_ABILITY_MILITARY:
            /* TODO: Can attack if there's an enemy unit */
            unitruleMilitaryOrders(pos, knowledge, orderlist);
            break;

        case UNIT_ABILITY_TRANSPORT: /* Troop transport/transport       */
            /* FIXME: Can move there if there's no enemy unit           */
            *orderlist = 0;
            break;

        case UNIT_ABILITY_DIPLOMACY: /* Diplomacy                           */
            /* FIXME: Can do diplomacy actions if an alien star is at 'pos' */
            break;

        case UNIT_ABILITY_TRADE:    /* Trade                                */
            /* FIXME: Can do trade actions if an alien/own star is at 'pos' */
            /*        Works automatic.                                      */
            /*        Handles like GOTO                                     */
            *orderlist = 0;
            break;

        case UNIT_ABILITY_SCOUT:    /* Explore Superiority              */
            /* Base orders */
            break;

        case UNIT_ABILITY_CONSTRUCT: /* Build/extend starbase            */
            unitruleGetConstructActions(pos, orderlist);
            break;

        default:;

    }

    strcat(orderlist, BaseOrders);
    numorder = strlen(orderlist);
    

    return numorder;       /* Number of orders available */

}

/*
 * Name:
 *     unitruleGetPlanetOrder
 * Description:
 *     Returns a list of possible orders for given unit with given planet
 * Input:
 *     unit_no:     Number of unit to get action-list for
 *     planet_no:   Planet to check for possible orders
 * Output:
 *     Number of order, if any
 */
char unitruleGetPlanetOrder(int unit_no, int planet_no)
{

    UNIT *punit;
    STAR *pstar;
    PLANET *pplanet;


    punit = unitGet(unit_no);
    
    /* There are only actions possible, if the unit has moves left at all   */
    if (punit -> moves[UI_VALUE_ACT] <= 0) {

        return 0;

    }

    pplanet = starGetPlanet(planet_no);
    
    /* The unit must be at the same position as the planet */
    if (punit -> pos != starGet(pplanet -> orbits_star, 0) -> pos) {
    
        return 0;
        
    }

    if (pplanet -> settleno > 0) {

        if (pplanet -> settletype == STARINFO_SETTLETYPE_COLONY) {
        
            return 0;   /* Already settled */
            
        }

    }

    switch(punit -> ability) {
    
        case UNIT_ABILITY_COLONIZE:
            if (pplanet -> terraform[1] > 0) {
            
                return ORDER_TERRAFORM;
                
            }
            /* TODO: Take over possible outpost on planet. Take other players into account */
            return ORDER_COLONIZE;

        case UNIT_ABILITY_CONSTRUCT:
            /* TODO: Add possibility to take over other nations outposts */
            if (pplanet -> terraform[1] > 0) {
            
                return ORDER_BUILD;
                
            }
            break;
            
    }

    return 0; 

}

/*
 * Name:
 *     unitruleShipAttack
 * Description:
 *     Receives the attacking ship and the defending ship.
 * Input:
 *     attacker:   The ship index of the attacking ship
 *     defender:   The ship index of the defending ship
 * Output:
 *     Returns the ship that is destroyed. 
 */
int unitruleShipAttack(int attacker, int defender)
{

    UNIT *pattacker, *pdefender;
    int Complete;
    int Attack;
    int Defense;
    int FirstRound;
    int Defender;
    int Strength_Defender;
    int Strength_Attacker;
    int DefenderExp;
    int AttackerExp;
    int NewExp;


    Complete = 0;
    FirstRound = 0;


    /* Strenght is how many hitpoints each ship has. */
    pattacker = unitGet(attacker);
    pdefender = unitGet(defender);

    Strength_Defender = pdefender -> hp[UI_VALUE_ACT];
    Strength_Attacker = pattacker -> hp[UI_VALUE_ACT];

    DefenderExp = pdefender -> hp[UI_VALUE_ACT];
    AttackerExp = pattacker -> hp[UI_VALUE_ACT];

    /* Defense Value                                */
    /* The defender gets a bonus if it is in orbit  */
    if (pdefender -> order.target > 0) {

	    Defender = unitruleGetDefense(defender)+3;

    }
    else {

        Defender = pdefender -> defense;

    }

    /* If the attacker has no weapons, it's doomed. */
    if (pattacker -> attack == 0) {

        /* May just forbid attacking ? */
        return attacker;

    }

    /* If the defender has no weapons and is in orbit it is doomed.             */
    /* This keeps people from parking colony ships and transports as defenders. */
    if ((pdefender -> attack == 0) && (pdefender -> order.target > 0)) {

        return defender;

    }

    /* Begin the battle */
    while(! Complete) {

        /* Attack Value */
	    /* Attack=rand()%((Galaxy->StarShip[attacker].att)+1); */
        Attack = fsctoolRand(pattacker -> attack);

    	/* Defense Value.  First few rounds the attacker has the advantage. */
    	if (FirstRound < 2) {

		    /* Defense = rand()%(Defender+1); */
            Defense = fsctoolRand(Defender);

        }
    	else {

		    /* Defense=rand()%(Galaxy->StarShip[defender].att+1); */
            Defense = fsctoolRand(pdefender -> attack);

        }

        /* FIXME: Add level strength to attack ?! */

	    /* Whoever loses the round takes damage. */
    	if (Attack >= Defense) {

		    Strength_Defender -= ((Attack - Defense)+1);

        }
    	else {

		    Strength_Attacker-=((Defense-Attack)+1);

        }

    	/* If someone has no strength then they have been destroyed. */
    	if (Strength_Attacker <= 0) {

            pdefender -> strength = (char)Strength_Defender;
            /* Add experience... */

            /* pdefender -> defense++;  */    /* Add defense ?! */ 
            NewExp = AttackerExp + pdefender -> experience;

            if (NewExp >= 100) {

                pdefender -> experience = (char)(NewExp - 100);
                pdefender -> level++;

            }
            else {

                pdefender -> experience += (char)AttackerExp;

            }

            return attacker;

	    }
	    else if(Strength_Defender<=0) {

            pattacker -> strength = (char)Strength_Attacker;
            /* Add experience... */

            /* pdefender -> attack++;  */    /* Add attack ?! */
            NewExp = DefenderExp + pattacker -> experience;
            if (NewExp >= 100) {

                pattacker -> experience = (char)(NewExp - 100);
                pattacker -> level++;

            }
            else {

                pattacker -> experience += (char)DefenderExp;

            }

		    return defender;

	    }

    } /* while (! Complete) */

    return 0;

}


