/*******************************************************************************
*  GOAL_T.H                                                                      *
*	- Definition of goals. Tech for all players, others for AI                 *
*     Includes some helper functions for AI                                    *
*									                                           *
*   FREE SPACE COLONISATION                                                    *
*   Copyright (C) 2002  Paul Mueller <pmtech@swissonline.ch>                   *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*******************************************************************************/

#ifndef _FSC_GOAL_H_
#define _FSC_GOAL_H_

/*******************************************************************************
* DEFINES     							                                       *
*******************************************************************************/

#define GOAL_DELETED ((char)-1)

#define GOAL_WHO_LEADER  1
#define GOAL_WHO_COLONY  2
#define GOAL_WHO_UNIT    3


#define GOAL_TYPE_NONE    ((char)0)
#define GOAL_TYPE_UNIT    ((char)1)
#define GOAL_TYPE_TECH    ((char)2)
#define GOAL_TYPE_COLONY  ((char)3)
#define GOAL_TYPE_OUTPOST ((char)4)
#define GOAL_TYPE_IMPROVE ((char)5)     /* Which improvement */
#define GOAL_TYPE_ATTACK  ((char)6)
#define GOAL_TYPE_DEFENSE ((char)7)

/*******************************************************************************
* TYPEDEFS     							                                       *
*******************************************************************************/

typedef struct
{

    char owner;     /* Goal belongs to this player. -1: Deleted slot    */
    char who;       /* Who should work toward this goal ?               */
    char type;      /* GOAL_TYPE_*   0: Empty slot -1: Deleted slot     */
                    /*
                        GOAL_TYPE_UNIT:    This unit is the goal
                        GOAL_TYPE_TECH: :  This tech is the goal
                        GOAL_TYPE_COLONY:  Build colony a given position
                        GOAL_TYPE_OUTPOST: Build outpost here,
                                           which kind of outpost
                        GOAL_TYPE_IMPROVE: Which improvement
                        GOAL_TYPE_ATTACK:  That much attack points
                        GOAL_TYPE_DEFENSE: That much defense at this position
                    */
    int  value;     /* Dependig on goal type:
                        GOAL_TYPE_UNIT:    Number of unittype planned to build
                        GOAL_TYPE_TECH:    Number of tech planned to research
                        GOAL_TYPE_COLONY:  None: Build a position 'pos':
                        GOAL_TYPE_OUTPOST: Type of outpost to build
                        GOAL_TYPE_IMPROVE: Which improvement to build
                        GOAL_TYPE_ATTACK:  Number of attack points
                        GOAL_TYPE_DEFENSE: Number of defense points
                    */
    int  want;      /* Want value of nation for this goal   */
    int  pos;       /* At this position on map              */
    int  assigned_to;/* This colony / unit is working toward this    */
    int  preq_goal; /* >0: Depends on this goal. Used by AI */

}
GOAL_T;

/*******************************************************************************
* CODE      							                                       *
*******************************************************************************/

GOAL_T *goal_get(int goal_no);
int  goal_get_list(char owner, int *list);

void goal_init(char owner, char who, GOAL_T *pgoal);
int  goal_set(GOAL_T *pgoal);
void goal_delete(int goal_no);

#endif  /* _FSC_GOAL_H_ */

