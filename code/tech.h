/*******************************************************************************
*  TECH.H                                                                      *
*      - Handling civilization advances (technologies) and it's lists          *
*                                                                              *
*   FREE SPACE COLONISATION                                                    *
*       Copyright (C) 2002-2017  Paul Müller <muellerp61@bluewin.ch>           *
*                                                                              *
*  This program is free software; you can redistribute it and/or modify        *
*  it under the terms of the GNU General Public License as published by        *
*  the Free Software Foundation; either version 2 of the License, or           *
*  (at your option) any later version.                                         *
*                                                                              *
*  This program is distributed in the hope that it will be useful,             *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of              *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
*  GNU Library General Public License for more details.                        *
*                                                                              *
*  You should have received a copy of the GNU General Public License           *
*  along with this program; if not, write to the Free Software                 *
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  *
*******************************************************************************/

#ifndef _FSC_TECH_H_
#define _FSC_TECH_H_

/*******************************************************************************
* INCLUDES                                                                     *
*******************************************************************************/

#include "effect.h"             /* struct GAME_EFFECT_T    */
#include "sdlglcfg.h"           /* SDLGLCFG_RECORD_T       */

/*******************************************************************************
* DEFINES                                                                      *
*******************************************************************************/

#define TECH_MAXBYTES  16
#define TECH_MAX      120

/*******************************************************************************
* TYPEDEFS 								                                       *
*******************************************************************************/

typedef struct
{
    int  num_researched;
    int  list_researched[TECH_MAX + 2];
    int  num_reachable;
    int  list_reachable[TECH_MAX + 2];
    /* For AI-GOALS */
    int  list_unresearched[TECH_MAX + 2];
}
TECH_INFO_T;

/*******************************************************************************
* CODE                                                                         *
*******************************************************************************/

/*** =========== Basic functions ============= ***/
void tech_init(void);

/*** =========== Managing functions =========== ***/
int  tech_set_researched(unsigned char *ptechbits, int tech_no, GAME_EFFECT_T *pge);
void tech_get_info(unsigned char *ptechbits, TECH_INFO_T *pinfo);
int  tech_get_turns(int tech_no, int invested, int research_pts);
int  tech_get_effects(int tech_no, GAME_EFFECT_T *pge, int *pcost);
void tech_get_name(int tech_no, char *pname);

/*** =========== List functions =========== ***/
int  tech_get_keycodes(unsigned char *ptechbits, char *pkey_codes);


/* ------------ Functions for reading in config data ------ */
int tech_get_datadesc(SDLGLCFG_RECORD_T *pdatadesc);

#endif  /* _FSC_TECH_H_ */
