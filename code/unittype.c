/*******************************************************************************
*  UNITTYPE.C                                                                  *
*      - Data for and handling of unittypes                                    *
*                                                                              *
*   FREE SPACE COLONISATION                                                    *
*       Copyright (C) 2002-2017  Paul Müller <muellerp61@bluewin.ch>           *
*                                                                              *
*  This program is free software; you can redistribute it and/or modify        *
*  it under the terms of the GNU General Public License as published by        *
*  the Free Software Foundation; either version 2 of the License, or           *
*  (at your option) any later version.                                         *
*                                                                              *
*  This program is distributed in the hope that it will be useful,             *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of              *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
*  GNU Library General Public License for more details.                        *
*                                                                              *
*  You should have received a copy of the GNU General Public License           *
*  along with this program; if not, write to the Free Software                 *
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  *
*******************************************************************************/

/*******************************************************************************
* INCLUDES								                                       *
*******************************************************************************/

#include <stdio.h>      /* sprintf      */
#include <string.h>     /* memmove      */

/*#include "../fscfe/sdlgldef.h" */

#include "effect.h"
#include "fscshare.h"
#include "fsctool.h"
#include "sdlglcfg.h"


#include "unittype.h"

/*******************************************************************************
* DEFINES  								                                       *
*******************************************************************************/

#define UNITTYPE_BITMASK(id)  ((unsigned char)(0x01 << (char)(id & 0x07)))

#define UNITTYPE_ISKEY(k1, k2) (((k1[0] - k2[0]) | (k1[1] - k2[1]) | (k1[2] - k2[2])) == 0)


/*******************************************************************************
* DATA     								                                       *
*******************************************************************************/

/* starbase is built by constructor */
/* Read from "fscrules.txt" */
static UNITTYPE_T Unit_Types[UNITTYPE_MAX + 2] =
{
    { "CoS", "Colony Ship", "nil",  5, 0, 2,  10, 0, 1,  2, 1, 2,  { 5, GVAL_EXPAND, 5 } },
    { "Sco", "Scout",       "nil",  2, 0, 6,   1, 0, 1,  3, 2, 3,  { 5, GVAL_SENSORS, 5 } },
    { "Con", "Constructor", "nil",  8, 0, 7,  13, 0, 1,  2, 1, 3,  { 5, GVAL_RANGE, 5 } },
    { "" }
};

/* Read from "fscrules.txt" */
static GAME_EFFECT_T UnitTypeEffects[UNITTYPE_MAX + 2];   /* Read from "fscrules.txt" */

/*******************************************************************************
* CODE    								                                       *
*******************************************************************************/

/* ========================================================================== */
/* ========================= PUBLIC FUNCTIONS =============================== */
/* ========================================================================== */


/*
 * Name:
 *     unittype_get
 * Description:
 *
 * Input:
 *     type_no:  Number of type to return
 *     key_code: Return for this key_Code, use if type_no is 0
 */
UNITTYPE_T *unittype_get(int type_no, char key_code[4])
{
    int ut_num;


    if(!type_no)
    {
        /* Find the type by key_code */
        ut_num = 1;

        while(Unit_Types[ut_num].key_code[0] > 0)
        {
            if(UNITTYPE_ISKEY(Unit_Types[ut_num].key_code, key_code))
            {
                type_no = ut_num;
                break;
            }

            ut_num++;
        }
    }

    return &Unit_Types[type_no];
}

/*
 * Name:
 *     unittype_get_available
 * Description:
 *     Fills in given list with number of units which are available
 *     based on 'ptech_keycode *'
 * Input:
 *     ptech_keycode *: Pointer on list of signs of technology (e.g. "nil")
 *     putypeavail *:   Where to return the list of unittypes that are available
 * Output:
 *     Number of elements in list
 * Last change:
 *     2017-07-20 <bitnapper>
 */
int unittype_get_available(char *ptech_keycode, int *putypeavail)
{
    int num_element;
    int ut_num;


    num_element = 0;

    /* Run trough all keycodes of researched techs */
    while(ptech_keycode[0] != 0)
    {
        ut_num = 1;

        while(Unit_Types[ut_num].key_code[0] > 0)
        {
            if(UNITTYPE_ISKEY(ptech_keycode, Unit_Types[ut_num].key_code))
            {
                *putypeavail = ut_num;

                putypeavail++;
                num_element++;
            }

            ut_num++;
        }

        ptech_keycode += 4;
    }

    /* Sign end of list */
    *putypeavail = 0;

    return num_element;
}

/*
 * Name:
 *     unittypeGetName
 * Description:
 *     Returns the name of a unit type
 * Input:
 *      type_no: Type of unit to get name for
 */
const char *unittype_get_name(int type_no)
{
    if(type_no > 0)
    {
        return (Unit_Types[type_no].name);
    }

    return "";
}

/*
 * Name:
 *     unittype_get_ability
 * Description:
 *     Returns the additional info for a unit tpye, if any available
 * Input:
 *      type_no: Unit type to get the value for
 * Output:
 *      > 0: Unit has this ability
 */
int unittype_get_ability(int type_no)
{
    if(type_no > 0)
    {
        return (Unit_Types[type_no].ability);
    }

    return 0;
}

/*
 * Name:
 *     unittype_best_roleunit
 * Description:
 *     Return "best" unit this city can build, with given role.
 *     Returns 0 if none match.
 * Input:
 *     putypeavail *: Pointer on list of possible unit types
 *     role:          Role to look for
 */
int unittype_best_roleunit(int *putypeavail, int role)
{
    int best;


    best = 0;

    if(putypeavail)
    {
        /* Gets the best role unit */
        /* The units are orderer in the list from worst to best */
        while((*putypeavail) > 0)
        {

            if(Unit_Types[*putypeavail].ability == role)
            {
                best = *putypeavail;
            }

            putypeavail++;
        }
    }
    else
    {
        /* Get first role unit */
        best = 1;

        while(Unit_Types[best].name[0] != 0)
        {
            if(Unit_Types[best].ability == role)
            {
                return best;
            }

            best++;
        }
    }

    return best;
}



/*
 * Name:
 *     unittype_asses_by_cost
 * Description:
 *     Returns best value given by 'flags' where buildcost <= maxcost.
 *     Returns the effective build cost in '*cost'
 *     If cheapest buildcost > max_cost, the cheapest unittype is
 *     returned.
 * Input:
 *     putypeavail *: Pointer on list of possible unit types
 *     max_cost:      Maximum cost
 *     cost *:        Cost for unit
 *     att:           Weight value for attack, if > 0
 *     def:           Weight value for defense
 */
int unittype_asses_by_cost(int *putypeavail, int max_cost, int *pcost, char att, char def)
{
    UNITTYPE_T *put;
    int cheapest_cost;
    int best_value;
    int value;
    int cheapest_unit, best_unit;


    cheapest_cost = 100000;
    best_value    = 0;
    cheapest_unit = 0;
    best_unit     = 0;

    while(*putypeavail > 0)
    {
        put = &Unit_Types[*putypeavail];

        if(put->cost < cheapest_cost)
        {
             cheapest_cost = put->cost;
             cheapest_unit = *putypeavail;
        }

        if(put->cost <= max_cost)
        {
            value = 0;

            /* Weight the valuation */
            value += (int)put->att * att;
            value += (int)put->def * att;

            if(value > best_value)
            {
                best_value = value;
                best_unit  = *putypeavail;
            }

            if(put->cost == max_cost)
            {
                break;
            }
        }

        putypeavail++;
    }

    if(cheapest_cost > max_cost)
    {
        best_unit = cheapest_unit;
    }

    *pcost = Unit_Types[best_unit].cost;

    return best_unit;
}

/*
 * Name:
 *     unittype_build_time
 * Description:
 *     Returns the turns needed to build this unit, given the
 *     points already 'invested' and the 'buildpts' available
 *     if 'buildpoints <= 0' then 9999 rounds are returned as result
 * Input:
 *     type_no:   Which type to build
 *     invested:  Points already invested
 *     build_pts: Buildpoints available for build this unit
 * Output:
 *     Number of turns left for building this type, -1: Stalled (0 buildpoints)
 */
int unittype_build_time(int type_no, int invested, int build_pts)
{
    int cost_left;
    int turns;


    if(build_pts <= 0 || type_no <= 0)
    {
        /* Has stalled or nothing chosen to build */
        return -1;
    }

    if(type_no > 0)
    {
        cost_left = Unit_Types[type_no].cost - invested;

        if(cost_left <= 0)
        {
            turns = 0;
        }
        else
        {
            if(build_pts  > 0)
            {
                turns = cost_left / build_pts;

                if((turns % build_pts) > 0)
                {
                    turns++;
                }
            }
        }
    }

    return turns;
}

/*
 * Name:
 *     unittype_get_effect
 * Description:
 *     Returns the effect for given unit-type. Pointer is alvais valid.
 *     But if range = 0 the there's no effect to set.
 * Input:
 *     type_no: Of unit to get effect for
 *     pge *:   Where to return the effect
 * Output:
 *     Number of effects in 'pge'
 */
int unittype_get_effect(int type_no, GAME_EFFECT_T *pge)
{
    memcpy(pge, &Unit_Types[type_no].effect, sizeof(GAME_EFFECT_T));

    return 1;
}

/*
 * Name:
 *     unittype_get_datadesc
 * Description:
 *     Returns the description needes to fill in the data records from a
 *     configuration file line
 *     Initializes the buffer by zeroing out it at start
 * Input:
 *     pdatainfo *: Where to return the descriptor(s) for the data
 * Output:
 *     Pointer on 'SDLGLCFG_RECORD_T'
 */
int unittype_get_datadesc(SDLGLCFG_RECORD_T *pdatainfo)
{
    static SDLGLCFG_VALUE_T RecValue[] =
    {
        { SDLGL_VAL_STRING, &Unit_Types[0].key_code[0], 3 },
        { SDLGL_VAL_STRING, &Unit_Types[0].name[0], UNITTYPE_MAX_NAMELEN - 1 },
        { SDLGL_VAL_STRING, &Unit_Types[0].preq_code[0], 3 },
        { SDLGL_VAL_INT,    &Unit_Types[0].cost },
        { SDLGL_VAL_CHAR,   &Unit_Types[0].maintenance },
        { SDLGL_VAL_CHAR,   &Unit_Types[0].ability },
        { SDLGL_VAL_CHAR,   &Unit_Types[0].hit },
        { SDLGL_VAL_CHAR,   &Unit_Types[0].att },
        { SDLGL_VAL_CHAR,   &Unit_Types[0].def },
        { SDLGL_VAL_CHAR,   &Unit_Types[0].moves },
        { SDLGL_VAL_CHAR,   &Unit_Types[0].range },
        { SDLGL_VAL_CHAR,   &Unit_Types[0].sensor_range },
        { SDLGL_VAL_CHAR,   &Unit_Types[0].effect.range },
        { SDLGL_VAL_INT,    &Unit_Types[0].effect.type },
        { SDLGL_VAL_CHAR,   &Unit_Types[0].effect.amount },
        { SDLGL_VAL_STRING, &Unit_Types[0].effect.info[0], 3 },
        { 0 }
    };

    static SDLGLCFG_RECORD_T DataInfo =
    {
        UNITTYPE_MAX,       /* Maximum of records                   */
        sizeof(UNITTYPE_T), /* Size of record in buffer             */
        &Unit_Types[0],      /* Pointer on record data (any)         */
        "UNITTYPE",
        &RecValue[0]        /* Descriptor vor values on single line */
    };

     /* Initialize the data buffer */
    memset(&Unit_Types[0], 0, (UNITTYPE_MAX + 1) * sizeof(UNITTYPE_T));

    memcpy(pdatainfo, &DataInfo, sizeof(SDLGLCFG_RECORD_T));

    return 1;
}
