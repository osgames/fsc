/*******************************************************************************
*  MSGTOSTR.C                                                                  *
*	   - Translating message structs to human readable strings                 *
*                                                                              *
*   FREE SPACE COLONISATION                                                    *
*       Copyright (C) 2002-2017  Paul Müller <muellerp61@bluewin.ch>           *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*******************************************************************************/

/*******************************************************************************
* INCLUDES                                                                     *
*******************************************************************************/

#include <stdio.h>              /* sprintf()        */
#include <string.h>


#include "colony.h"
#include "fscmap.h"         /* fscmapGetSectorName() */
#include "improve.h"        /* improveName()         */
#include "player.h"         /* nationGetName()       */
#include "starsys.h"        /* starGetName(), starGetPlanetName()    */
#include "tech.h"           /* tech_get_name()       */
#include "unit.h"           /* unitGetName()         */
#include "unittype.h"       /* unittypeGetName()     */


/* -- Own header -- */
#include "msgtostr.h"

/* *****************************************************************************
* DATA                                                                         *
*******************************************************************************/

/* Order strings for loggging of orders */
static char *UnitOrder_List[] =
{
    "Idle",
    "Sentry",
    "Guard",
    "Auto-Attack",
    "Auto-Retreat",
    "Patrol",
    "Border-patrol",
    "Goto",
    "Explore",
    "Build",
    "Terraform",
    "Colonize",
    "Cancel",
    "Attack",
    "Defend",
    "Traderoute",
    ""
};

/* *****************************************************************************
* CODE                                                                         *
*******************************************************************************/

/*
 * Name:
 *     msgtostr_order_name
 * Description:
 *     Returns the name of the order, if the number of the order is valid
 * Input:
 *     order_no: Return string for this order
 *     pdest *:  Where to return the order name
 */
static void msgtostr_order_name(int order_no, char *pdest)
{
    *pdest = 0;

    if(order_no >= 0 && order_no < 16)
    {
         strcpy(pdest, UnitOrder_List[order_no]);
         return;
    }
}

/*
 * Name:
 *     msgtostr_check_sign
 * Description:
 *     Compares the given sign with given name. If it's the same, then
 *     return pointer on value behind the name to check.
 * Input:
 *     psign *:    Sign for check
 *     pcompare *: Compare with this one
 *     len *:      If > 0, then the sign is found
 * Output:
 *     len: Lenghth of psign
 */
static int msgtostr_check_sign(char *psign, char *pcompare, int *len)
{
    int length;


    length = strlen(psign);

    if(! strncmp(psign, pcompare, length))
    {
        *len = length;      /* It's this one        */
        return length;      /* return size of sign  */
    }

    *len = 0;

    return 0;
}

/*
 * Name:
 *     msgtostr_get_name
 * Description:
 *     Get the name, depending on 'psign'. 'psign' has to be ended by a ' '.
 *     Looks up if the sign is available. If so, the replacement string
 *     is attached to 'buffer'.
 * Input:
 *     psign *:  Pointer on message description to generate string for
 *     buffer *: Where to attach he name, if available.
 *     pinfo *:   Pointer on values for the different strings
 * Output:
 *     Pointer on end of sign that was found.
 */
static char *msgtostr_get_name(char *psign, char *buffer, MSG_INFO_T *pinfo)
{
    PLAYER_RACE_T race;
    char sec_name[50];
    char sec_pos[50];
    int pos_x, pos_y;
    int  length;
     /* Pointer on string that replaces the sign */
    char replacestr[300];


    /* -- We have to know which of the arguments has the correct number */
    if(msgtostr_check_sign("STAR", psign, &length))
    {
        starsys_get_name(0, pinfo->star_no, replacestr);
    }
    else if(msgtostr_check_sign("PLANET", psign, &length))
    {
        starsys_get_name(1, pinfo->planet_no, replacestr);
    }
    else if(msgtostr_check_sign("COLONY", psign, &length))
    {
        starsys_get_name(1, colony_get_planetno(pinfo->colony_no), replacestr);
    }
    else if(msgtostr_check_sign("TECH", psign, &length))
    {
        tech_get_name(pinfo->tech_no, replacestr);
    }
    else if(msgtostr_check_sign("UNIT", psign, &length))
    {
        unit_get_name(pinfo->unit_no, replacestr);
    }
    else if(msgtostr_check_sign("IMPROVE", psign, &length))
    {
        strcpy(improve_get_name(pinfo->improve_no), replacestr);

    }
    else if(msgtostr_check_sign("SECTOR", psign, &length))
    {
        if(fscmap_get_sectorinfo(pinfo->pos, sec_name, sec_pos))
        {
            sprintf(replacestr, "%s (%s)", sec_name, sec_pos);
        }
        else
        {
            sprintf(replacestr, "%s", sec_pos);
        }
    }
    else if(msgtostr_check_sign("VALUE1", psign, &length))
    {
        sprintf(replacestr, "%d", pinfo->args[0]);
    }
    else if(msgtostr_check_sign("VALUE2", psign, &length))
    {
        sprintf(replacestr, "%d", pinfo->args[1]);
    }
    else if(msgtostr_check_sign("VALUE3", psign, &length))
    {
        sprintf(replacestr, "%d", pinfo->args[2]);
    }
    else if(msgtostr_check_sign("ME", psign, &length))
    {
        player_race(pinfo->from_no, &race, 0);
        strcpy(replacestr, race.name);
    }
    else if(msgtostr_check_sign("YOU", psign, &length))
    {
        player_race(pinfo->to_no, &race, 0);
        strcpy(replacestr, race.name);
    }
    else if(msgtostr_check_sign("ALLY", psign, &length))
    {
        player_race(pinfo->ally, &race, 0);
        strcpy(replacestr, race.name);
    }
    else if(msgtostr_check_sign("OTHER", psign, &length))
    {
        player_race(pinfo->other, &race, 0);
        strcpy(replacestr, race.name);
    }
    else if(msgtostr_check_sign("TYPEUNIT", psign, &length))
    {
        strcpy(replacestr, unittype_get_name(pinfo->args[0]));
    }
    else if(msgtostr_check_sign("ORDER", psign, &length))
    {
        msgtostr_order_name(pinfo->args[0], replacestr);
    }
    else if(msgtostr_check_sign("POS", psign, &length))
    {
        sprintf(replacestr, "%d-%d", (int)pinfo->map_x, (int)pinfo->map_y);
    }
    else
    {
        length = 0;
    }

    /* TODO: Add function for adding $SECTORPOS */
    if(length > 0)
    {
        /* We have a string to replace the given sign name */
        psign += length;
        strcat(buffer, replacestr);
    }

    return psign;
}

/* ========================================================================== */
/* =========================== PUBLIC FUNCTION(S) =========================== */
/* ========================================================================== */

/*
 * Name:
 *     msgtostr_find_msgstr
 * Description:
 *     Looks for a message with given numner in given buffer.
 *     Returns the string in given 'msgbuf'
 * Input:
 *     msg_no:    Number of message to find
 *     str_src *: Points on buffer where to look for given string
 *     str_buf *: Buffer to return the string
 *     buf_size:  Size of buffer
 * Output:
 *     Pointer on given buffer
 */
char *msgtostr_find_msgstr(int msg_no, char *str_src, char *str_buf, int buf_size)
{

    char *pmsg;
    char *pend;
    int find_message_no, str_len;


    pmsg = str_src;
    find_message_no = 0;

    pmsg = strchr(pmsg, '@');

    while(pmsg)
    {
        pmsg++;     /* Point on number to scan */

        sscanf(pmsg, "%d", &find_message_no);

        if(find_message_no > 0 && find_message_no ==  msg_no) {

            /* We found a message with given number */
            pmsg += 4;

            /* TODO: Remove leading spaces */
            pend = strchr(pmsg, '@');   /*Signs end of buffer */

            if(! pend) {

                pend = pmsg;

            }

            str_len = pend - pmsg;

            if(str_len >= buf_size) {

                str_len = buf_size - 1;

            }

            /* Hand over the string to the caller */
            memcpy(str_buf, pmsg, str_len);
            str_buf[str_len] = 0;

            return str_buf;

        }

        pmsg = strchr(pmsg, '@');

    }

    return "";
}

/*
 * Name:
 *     msgtostr_translate
 * Description:
 *     Generates a message string from given MSG_INFO-Field.
 *     Replacement signs in given 'raw_str' are replaced by the names given
 *     by values held in given 'MSG_INFO' struct
 * Input:
 *     pinfo *:    Pointer on message description to get name numbers from
 *     praw_str *: Pointer on raw string holding the variable names $[NAME]
 *     pbuffer *:  Where to return the generated message
 *     buf_size:   Size of buffer, where to return the generated message
 * Output:
 *     Pointer on message generated (buffer *)
 */
char *msgtostr_translate(MSG_INFO_T *pinfo, char *praw_str, char *pbuffer, int buf_size)
{
    char msg_buf[1024];         /* Should be enough for completed message   */
    char *pmsg;
    char *pchar;


    pmsg       = praw_str;       /* Point on start of raw string             */
    msg_buf[0] = 0;

    if(pmsg)
    {
        /* 2) Copy message with replacement of variables */
        do
        {
            /* 3) Replace '$'-Variables by names from game */
            pchar = strchr(praw_str, '$');

            if(pchar)
            {
                /* Sign end of string */
                *pchar = 0;
            }

            /* Copy from here to '$'-sign or end    */
            strcat(msg_buf, pmsg);

            if(pchar)
            {
                /* Put back the variable sign in 'source' */
                *pchar ='$';
                /* Point on variable                      */
                pmsg = &pchar[1];
                pmsg = msgtostr_get_name(pmsg, msg_buf, pinfo);
            }
        }
        while(pchar);
    }

    strncpy(pbuffer, msg_buf, buf_size - 1);

    pbuffer[buf_size] = 0;

    /* Empty string if message not found */
    return pbuffer;
}

