Change Log V 0.4.2

2010-01-14 <bitnapper>
- Added code for Game-Setup Screen (non-functional at the moment, just for display)
- Removed Flags definition from header of FSCMAP. Made 'SetFlags' functions local

2010-01-15 <bitnapper>
- Main-Screen returns to Menu-Screen, if 'Menu' is chosen.
- Made new module 'GAMEMENU' as replacement 'GAMECFG'
- New Menu point in Main-Menu for Game-Settings
- Adjustements in DRAWTOOL
- Additional drawing code for 'GAMEMENU'
- Made game setup screen functional

2010-01-16 <bitnapper>
- Adjusted Auto-Explore code for units
- Extended TILEINFO with 'range'-info for units.
- Fixed a bug in Movement code for Explorers
- Further tests of AI
- Added basic logging of messages
- Removed unneeded Header-File TECHINFO
- Some code cleanup in message logging
- Minor changes in code ('hangs' after about 6 rounds ?)
- Adjusted code for unit movement
- Changed number of players to 7 for test map configuration
- Test of visibility of alien units

2010-01-17 <bitnapper> ( V 0.4.1.4)
- Update in vision code for map
- Fixed bug in building improvement lists for colony management
- Improved code for generation of colony management values
- Added special function for execution of auto movements for AI
- Fixed a bug in 'Build'-Order given by keyboard
- Fixed some values in FSCRULES-File

2010-01-19 <bitnapper>
- FSCCMD: Update of unit movement code while not end of turn
- Adjusted display of Info-Boxes for units and game state

2010-01-20 <bitnapper>
- AI-Leader sets stars as goal
- Fixed bug in calculation of upgrade of 'Planet-Quality'

2010-01-21 <bitnapper>
- Small code cleanups. Reduction of used functions

2010-01-22 <bitnapper>
- Added display info about actual units in screen for military advisor
- Added some documentation text

2010-01-24 <bitnapper>
- Adjustment in code for GOAL-generation of AI-Leader
- Small adjustment of AI-Colony-Management

2010-01-26 <bitnapper>
- Work towards code for AI settling planets and building outposts
- Update of basic AI-Unit-Management-Code
- Added Module for AI-Tech-Management
- Bugfix in code for finding unit with order
- Additional code for execution of unit movement for AI-Part

2010-01-28 <bitnapper>
- Outpost are now set to ORDER_SENTRY
- New function 'starGetPosList()' for usage with AI-Planning
- 'fscmapStarList()' filter-arguments are reduced and code is updated
- Adjustement of default orders

2010-01-29 <bitnapper>
- Adjustment of code for AI: Colony management
- Added 'MSG_TYPE_COLONY'
- Added 'msgSendDelayed()' and 'msgEndOfTurn()'
- Added module files for 'Constructors'
- Adjusted display code for main screen
- Re-coded 'infobarUnitOrderText()' for extended order-desription
 

