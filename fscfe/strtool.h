/* *****************************************************************************
*  STRTOOL.H                                                                   *
*      - Some tools for generating strings used by the frontend                *
*                                                                              *
*  FREE SPACE COLONISATION                                                     *
*      (c)2002-2010 Paul Mueller <pmtech@swissonline.ch>                       *
*                                                                              *
*  This program is free software; you can redistribute it and/or modify        *
*  it under the terms of the GNU General Public License as published by        *
*  the Free Software Foundation; either version 2 of the License, or           *
*  (at your option) any later version.                                         *
*                                                                              *
*  This program is distributed in the hope that it will be useful,             *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of              *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
*  GNU Library General Public License for more details.                        *
*                                                                              *
*  You should have received a copy of the GNU General Public License           *
*  along with this program; if not, write to the Free Software                 *
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  *
*******************************************************************************/

#ifndef _FSC_STRTOOL_H_
#define _FSC_STRTOOL_H_

/* *****************************************************************************
* DEFINES                                                                      *
*******************************************************************************/

#define STRTOOL_STATS       1
#define STRTOOL_UNITORDER   2
#define STRTOOL_EFFECT      3
#define STRTOOL_RANGE       4
#define STRTOOL_ABILITY     5

/* *****************************************************************************
* CODE                                                                         *
*******************************************************************************/

void strtoolDateString(int year, int month, char *buffer);
void strtoolPopulationValue(int population, char *value);
char *strtoolGetString(int which, int number);

#endif  /* _FSC_STRTOOL_H_ */
