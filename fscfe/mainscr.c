/*******************************************************************************
*   MAINSCR.C                                                                  *
*	    - Displays and handles all commmands for the main screen               *
*                                                                              *
*	FREE SPACE COLONISATION               	                                   *
*      (c)2002 - 2017 Paul Mueller <muellerp61@bluewin.ch>                     *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*******************************************************************************/

/*******************************************************************************
* INCLUDES								                                  *
*******************************************************************************/

#include <string.h>         /* strcpy()     */


#include "sdlgl.h"


#include "../code/game.h"
#include "../code/unitinfo.h"
#include "../code/msg.h"


#include "advisors.h"
#include "drawtool.h"
#include "maindraw.h"


/* --- Own header -- */
#include "mainscr.h"

/*******************************************************************************
* DEFINES							                                       *
*******************************************************************************/

#define MAINCSCR_MAX_FIELDS 256

/* ----- Number of special Input-Fields --------- */
#define MAINSCR_CMD_INPUTFLD_MAP           0
#define MAINSCR_CMD_INPUTFLD_MINIMAPZOOM   3
#define MAINSCR_CMD_INPUTFLD_STARSYSTEM    14

/*******************************************************************************
* ENUMS     							                                       *
*******************************************************************************/

/* --------- MAINSCR_CMD_BASE ------------ */

typedef enum
{
    MSD_MAP        = 1,
    MSD_MENUBUTTON = 2,
    MSD_MINIMAP    = 3,
    MSD_MESSAGE    = 4
}
MAINSCR_DRAW;

/*******************************************************************************
* DATA  								                                       *
*******************************************************************************/

/* -- For test purposes a default game -- */
static GAME_STARTINFO_T Default_Game =
{
    314528,     /* seed                                     */
    3, 3,       /* Width and height in sectors (of size 12) */
    12,         /* For handling of sectors                  */
    8,          /* Number of start positons (players)       */
    5,          /* The chance in 10 percent that a planetary system */
                /* holds a habitable planet at all                  */
    0,          /* the bigger, the harder [0 .. 6]                 */
    2,          /* 0..2: scattered, standard, dense                */
    16, 12      /* Size of actual viewport (minimum size)          */
};

static GAME_PLAYINFO_T GamePlayInfo;


/* =============== The INPUT-Rectangles =========== */
static SDLGL_INPUT_T MainScr_InputArea[MAINCSCR_MAX_FIELDS] =
{
    /* #define SDLGL_TYPE_MAP         0x0D     Map square (sub-rectangles)      */
    { SDLGL_TYPE_MAP, {   0,  0, 1024, 768 }, GCMD_VIEWPORT_CHOOSE, 0, NULL, 0, 32, 16 },
    /* Set the actual map-size at the start of the game */
    { SDLGL_TYPE_MAP, { 776, 26,  241, 241 }, GCMD_VIEWPORT_CENTER, 0, NULL, 0, 36, 36 },
    /* --------- Six buttons for choosing of different info screens --------- */
    /* Planet list     SDLK_F1 */
    { SDLGL_TYPE_BUTTON, { 776, 270, 32, 32 }, GCMD_ADVISOR, ADVISOR_DOMESTIC },
    /* Trade route list  SDLK_F2 */
    { SDLGL_TYPE_BUTTON, { 986, 270, 32, 32 }, GCMD_ADVISOR, ADVISOR_TRADE },
    /* Unit list SDLK_F3 */
    { SDLGL_TYPE_BUTTON, { 818, 270, 32, 32 }, GCMD_ADVISOR, ADVISOR_MILITARY },
    /* Diplomacy screen SDLK_F4 */
    { SDLGL_TYPE_BUTTON, { 860, 270, 32, 32 }, GCMD_ADVISOR, ADVISOR_FOREIGN },
    /* Influence/Culture  SDLK_F5 */
    { SDLGL_TYPE_BUTTON, { 902, 270, 32, 32 }, GCMD_ADVISOR, ADVISOR_CULTURE },
    /* Tech list  SDLK_F6  */
    { SDLGL_TYPE_BUTTON, { 944, 270, 32, 32 }, GCMD_ADVISOR, ADVISOR_TECH },
    /* ---------------------------------------------------------------------- */
    /* GCMD_VIEWPORT_CENTER: -1: Center to active unit, if available -- */
    { SDLGL_TYPE_BUTTON,   { 776,  2, 79, 18 },  GCMD_VIEWPORT_CENTER, 1, "Find"  },
    { SDLGL_TYPE_BUTTON,   { 857,  2, 79, 18 },  GCMD_MENU, 0, "Menu"  },
    { SDLGL_TYPE_BUTTON,   { 938,  2, 79, 18 },  GCMD_ENDTURN, 0, "Turn"  },
    { SDLGL_TYPE_TEXTAREA, { 100, 22, 600, 16 }, GCMD_CHOOSE_MSG  },
    { 0 }

};

    /* -------- Different input keys to set ------- */
static SDLGL_INPUTKEY_T MainScr_CmdKeys[] =
{
    /* ------- The map cursor (FSCCMD_MOVEUNIT)------ */
    { { SDLK_KP1 }, GCMD_UNIT_MOVEDIR, 5 },
    { { SDLK_KP2 }, GCMD_UNIT_MOVEDIR, 4 },
    { { SDLK_KP3 }, GCMD_UNIT_MOVEDIR, 3 },
    { { SDLK_KP4 }, GCMD_UNIT_MOVEDIR, 6 },
    { { SDLK_KP6 }, GCMD_UNIT_MOVEDIR, 2 },
    { { SDLK_KP7 }, GCMD_UNIT_MOVEDIR, 7 },
    { { SDLK_KP8 }, GCMD_UNIT_MOVEDIR, 0 },
    { { SDLK_KP9 }, GCMD_UNIT_MOVEDIR, 1 },
    /* ----- Move the map itself (FSCCMD_MOVEMAP) ---------- */
    { { SDLK_UP  },   GCMD_VIEWPORT_MOVE, 0 },
    { { SDLK_RIGHT }, GCMD_VIEWPORT_MOVE, 1 },
    { { SDLK_DOWN  }, GCMD_VIEWPORT_MOVE, 2 },
    { { SDLK_LEFT  }, GCMD_VIEWPORT_MOVE, 3 },
    /* ------- Short cuts ----- */
    /* 'F'ind: GCMD_VIEWPORT_CENTER: 1: Center to active unit, if available -- */
    { { SDLK_f }, GCMD_VIEWPORT_CENTER, 1 },
    { { SDLK_SPACE }, GCMD_UNIT_SKIP },
    { { SDLK_w },     GCMD_UNIT_WAIT },
    { { SDLK_b },     GCMD_UNIT_ORDER, ORDER_BUILD },
    { { SDLK_t },     GCMD_UNIT_ORDER, ORDER_TERRAFORM },
    { { SDLK_x },     GCMD_UNIT_ORDER, ORDER_EXPLORE },
    { { SDLK_g },     GCMD_UNIT_ORDER, ORDER_GUARD },
    { { SDLK_LCTRL, SDLK_n }, GCMD_NEWMAP },
    { { SDLK_RETURN }, GCMD_ENDTURN },
    /* 'H'ome: Center viewport to home planet: 2 */
    { { SDLK_h },      GCMD_VIEWPORT_CENTER, 2 },
    { { SDLK_ESCAPE }, GCMD_MENU },
    /* ------------- Choose the different management screens -------- */
    { { SDLK_F1  }, GCMD_ADVISOR, ADVISOR_DOMESTIC },
    { { SDLK_F2  }, GCMD_ADVISOR, ADVISOR_TRADE },
    { { SDLK_F3  }, GCMD_ADVISOR, ADVISOR_MILITARY },
    { { SDLK_F4  }, GCMD_ADVISOR, ADVISOR_FOREIGN },
    { { SDLK_F5  }, GCMD_ADVISOR, ADVISOR_CULTURE },
    { { SDLK_F6  }, GCMD_ADVISOR, ADVISOR_TECH },
    { { SDLK_F7  }, GCMD_ADVISOR, ADVISOR_WONDERLIST },
    { { SDLK_F8  }, GCMD_ADVISOR, ADVISOR_HISTOGRAPHY },
    { { SDLK_F9  }, GCMD_ADVISOR, ADVISOR_PLANETMANAGE },
    { { SDLK_F10 }, GCMD_ADVISOR, ADVISOR_DEMOGRAPHY },
    { { SDLK_F11 }, GCMD_ADVISOR, ADVISOR_STATS },
    { { 0 } }
};

static SDLGL_LABEL_T MainScr_Label[] =
{
    { SDLGL_VAL_STRING, 776,  2, "Find", 0, 0, 'L'  },
    { SDLGL_VAL_STRING, 857,  2, "Menu" },
    { SDLGL_VAL_STRING, 938,  2, "Turn" },
    { 0 }
};


/*******************************************************************************
* FORWARD DECLARATIONS        		                                           *
*******************************************************************************/

static int  mainscr_process_input(SDLGL_EVENT_T *pevent);

/*******************************************************************************
* CODE  								                                       *
*******************************************************************************/

/*
 * Name:
 *     mainscr_ownerdraw_func
 * Description:
 *     Sets the map screen data, depending on view mode (with / without
 *     planets screen.
 * Input:
 *     pfield *: Pointer on field description
 *     prect *:  Pointer on rectangle to draw
 *     el_no:    Number of element, if it's a slider box
 */
static void mainscr_ownerdraw_func(SDLGL_INPUT_T *pfield, SDLGL_RECT_T *prect, int el_no)
{
   switch(pfield->sdlgl_type)
   {
        case SDLGL_TYPE_MAP:
            if(pfield->code == GCMD_VIEWPORT_CHOOSE)
            {
                maindraw_main(&pfield->rect, 1);
            }
            else if(pfield->code == GCMD_VIEWPORT_CENTER)
            {
                /* @TODO: Get 'player_no' from 'GamePlayInfo' */
                maindraw_minimap(&pfield->rect, 1);
            }
            break;
    }
}

#if 0
/*
 * Name:
 *     mainscrDrawMenu
 * Description:
 *     Draws the menus that may be on top of screen
 * Input:
 *     fields *: Pointer on fields
 */
static void mainscrDrawMenu(SDLGL_INPUT_T *fields)
{
    SDLGL_RECT_T bkrect;


    while(fields->sdlgl_type != 0) {

        /* Find the popup menu */
        if(fields->sdlgl_type == INFOBAR_ORDERLIST)
        {

            /* We found the popup, now draw it. The backround _MUST_ be before menu strings */
            /* Draw the background of menu */
            bkrect.x = fields->rect.x - 3;
            bkrect.y = fields->rect.y - 3;
            bkrect.w = fields->rect.w + 6;
            bkrect.h = fields->rect.h + 6;
            drawtoolRectangle(&bkrect, DRAWTOOL_FILLHI, 90);
            fields++;

            fscfont_print_inputtext(fields, INFOBAR_ORDERLIST, "\xFF\xFF\xFF", "\xFF\x00\x00");
            return;
        }

        fields++;
    }
}
#endif



#if 0
/*
 * Name:
 *     mainscr_click_minimap
 * Description:
 *     Centers to minimap
 * Input:
 *     event *:
 */
static void mainscr_click_minimap(SDLGL_EVENT_T *pevent)
{
    FSCMAP_TILEINFO_T tile_info;
    int map_pos;


    fscmap_viewport_adjust(pevent->map_x, pevent->map_y, 1, 0);

    map_pos = fscmap_pos_fromxy(pevent->map_x, pevent->map_y);

    /* Return the info about given tile */
    fscmap_get_tileinfo(map_pos, GamePlayInfo.player_no, &tile_info);

    /* On basis of the tile-info, display stars and planets */
    infobarTileInfo(map_pos, GamePlayInfo.player_no, GamePlayInfo.chosen_planetno);
}



/*
 * Name:
 *     mainscr_choose_planet
 * Description:
 *     Chooses planet and adds commands accordingly
 * Input:
 *     event *: Pointer on event to translate
 */
static void mainscr_choose_planet(SDLGL_EVENT_T *pevent)
{
    /* Choose planet        */
    GamePlayInfo.chosen_planetno = pevent->sub_code;

    if(pevent->sdlcode == SDLGL_KEY_MOULEFT)
    {
        infobarSetPlanetInfo(pevent->sub_code);
    }
    else if(pevent->sdlcode == SDLGL_KEY_MOURIGHT)
    {
        /* Opens the popup-menu for possible actions on chosen planet
           for "Constructor" and "Colony Ship" */
        infobarSetPlanetMenu(pevent->mou.x + (pevent->mou.w / 3),
                             pevent->mou.y + (pevent->mou.h / 3),
                             UnitMngInfo.list[UnitMngInfo.cursor],
                             event->sub_code,
                             GamePlayInfo.player_no);
    }
}

#endif

#if 0
/*
 * Name:
 *     mainscr_click_msg
 * Description:
 *     Removes a message, if needed or
 *     TODO: Centers on Positon, where the message happened or opens
 *           the management screen accordingly.
 * Input:
 *     event *: Pointer on event to translate
 */
void mainscr_click_msg(SDLGL_EVENT_T *pevent)
{
    MSG_INFO_T pmsg;


    if(pevent->sdlcode == SDLGL_KEY_MOULEFT)
    {
        /* Delete the message without reaction */
        infobarHandleMsg(0, NULL);
    }
    else if(event->sdlcode == SDLGL_KEY_MOURIGHT)
    {
        /* TODO: Center on position of message or open the
           management screen, if needed */
        infobarHandleMsg(-1, &pmsg);
    }

}
#endif

#if 0
/*
 * Name:
 *     mainscrUnitMoved
 * Description:
 *     Handles all actions needed after a unit has moved
 * Input:
 *     pmsg *: Sent by game about movement of unit
 */
static void mainscrUnitMoved(MSG_INFO_T *pmsg)
{
    if(pmsg->unit_no > 0)
    {
        mapdrawStartAnimation(&MainScr_InputArea[0].rect,
                              pmsg->unit_no,
                              pmsg->pos,
                              pmsg->arg[0]);

        /* Now, it this is our chosen unit */
        if(pmsg->unit_no == UnitMngInfo.list[UnitMngInfo.cursor])
        {
            /* Choose next unit, if this one has no moves left */
            if(pmsg->arg[1] <= 0)
            {
                /* Choose next unit. Same as 'Skip */
                unitmoveChooseUnit(&UnitMngInfo, 0, UNITMOVE_CHOOSE_NEXTSKIP);

            }
            else {

                unitmoveChooseUnit(&UnitMngInfo, 0, UNITMOVE_CHOOSE_CURRENT);


            }

            fscmap_adjust_viewport(UnitMngInfo.pos, 0);
            /* Display info about star system, if needed */
            infobarTileInfo(UnitMngInfo.pos, GamePlayInfo.player_no, GamePlayInfo.chosen_planetno);

        }   /* it it's our unit */

    }

}
#endif

/*
 * Name:
 *     mainscr_process_gui_input
 * Description:
 *     Translates the keybard and mouse inputs of the human (local, non-AI)
 *     player to game commands.
 * Input:
 *     code,
 *     sub_code: Main- and Sub-Code to translate
 *     pevent *: Pointer on pevent generated by SDLGL-Library
 *     pmsg *:   To fill in with command data translated from pevent, if any
 * Output:
 *      game-command yes/no
 */
static int mainscr_process_gui_input(int code, int sub_code)
{
    switch(code)
    {
        case  GCMD_NEWMAP:
            /* GUI: Generate a new map       */
            break;

        case GCMD_VIEW_MODE:
            /* GUI: Switch between view/move mode (cursor)   */
            break;

        case GCMD_CHOOSE_MSG:
            /* GUI: Click on message, jump to event position */
            break;

        case GCMD_MENU:
            /* GUI: Jump back to menu screen */
            return SDLGL_INPUT_REMOVE;

        case GCMD_ADVISOR:
            /* GUI: Open an advisor screen   */
            advisor_main(sub_code);
            break;
    }
    return SDLGL_INPUT_OK;
}

/*
 * Name:
 *     mainscr_process_input
 * Description:
 *     Translates the keyboard and mouse inputs of the human (local, non-AI)
 *     player to game commands.
 * Input:
 *     pevent *: Pointer on pevent generated by SDLGL-Library
 * Output:
 *     Possible result of command to return to SDLGLs main loop
 */
static int mainscr_process_input(SDLGL_EVENT_T *pevent)
{
    int arg1, arg2;


    arg1 = 0;
    arg2 = 0;

    /* @TODO: Block this input, if it's another players turn */
    /* Command table as replacement for "switch"-Command */
    if(pevent->code < 0)
    {
        /* It's a command only for the GUI */
        return mainscr_process_gui_input(pevent->code, pevent->sub_code);
    }
    else if(pevent->code > 0)
    {
        /* Pre-Process mouse input */
        switch(pevent->code)
        {
            case GCMD_CHOOSE_PLANET:
                if(pevent->sdlcode == SDLGL_KEY_MOURIGHT)
                {
                    pevent->code = GCMD_USE_PLANET;
                    /* Sub-Code: Number of planet chosen */
                }
                break;

            case GCMD_VIEWPORT_CHOOSE:
                if(pevent->sdlcode == SDLGL_KEY_MOURIGHT)
                {
                    pevent->code = GCMD_UNIT_MOVEGOTO;
                    /* Choose in viewport */
                    pevent->sub_code = 2;
                    arg1 = pevent->map_x;
                    arg2 = pevent->map_y;
                }
                else
                {
                    /* Choose in viewport */
                    pevent->sub_code = 2;
                    arg1 = pevent->map_x;
                    arg2 = pevent->map_y;
                }
                break;

            case GCMD_VIEWPORT_CENTER:
                arg1 = pevent->map_x;
                arg2 = pevent->map_y;
                break;
        }

        /* Send a command to the game */
        game_send_input(1, pevent->code, pevent->sub_code, arg1, arg2);
    }

    return 0;
}

/*
 * Name:
 *     mainscr_draw_func
 * Description:
 *     Display function for the screens set by the "mainscr" functions.
 * Input:
 *      pfields *: Pointer on array of fields to draw
 *      pevent *:  Event holding different info used for drawing
 */
static void mainscr_draw_func(SDLGL_INPUT_T *pfields, SDLGL_EVENT_T *pevent)
{
    maindraw_timer_func(pevent->secondspassed);

    drawtool_drawfields(pfields, mainscr_ownerdraw_func);


    /* Fill debug info an hand it over to 'infobar' */
    /*
    infobarDebugInfo(gameDebugInfo(buffer));

    if(pfields)
    {
        / * 1) Draw owner drawn fields * /
        mainscrOwnDrawFunction(fields);

        / * 2) Draw the info about the unit, if any  ------ * /
        infobarDrawUnitInfo(3, 680, UnitMngInfo.list[UnitMngInfo.cursor], 1);

        / * 4) Draw textures  ------  * /
        / * sdlgltexDisplayIconList(fields, 0); * /
    }
    */

    /*
    if(pevent->tickspassed > 0)
    {
        mapdrawTimeFunc(pevent->tickspassed);
    }
    */
    /*
    / * Print the labels for the buttons * /
    fscfont_print_labels(0, 0, MainScreenLabels);

    infobarDrawLabels();        / * Labels of a possible star/planet info * /
    infobarDrawGlobalInfo(3, 3);

    / * Now for the 'popup' -- Draw possible menu strings for unit commands * /
    mainscrDrawMenu(fields);
    */
}

/*
 * Name:
 *     mainscr_process_game_msg
 * Description:
 *     Fetches the result of 'unitmoveGetResult()' and handles it as needed
 *     TODO: Fill in the message into the players message buffer, if needed.
 *     TODO: Create the message using 'msgtostr...()' --> support of languages
 * Input:
 *     pmsg *:
 * Last change:
 *     2010-01-21 / bitnapper
 */
static void mainscr_process_game_msg(MSG_INFO_T *pmsg)
{
    switch(pmsg->num)
    {
        /* ------ Game/Turn starts ------- */
        case GRES_START_GAME:
            /* To start position (Star)*/
            /* fscmap_adjust_viewport(pmsg->args[0], pmsg->args[1], 1, 0); */
            /* TODO: Ask for first tech to research -- */
        case GRES_BEGIN_TURN:
            /* Get my data for management while this turn is running */
            game_get_playinfo(&GamePlayInfo);
            /*
            unitmoveGetManageInfo(GamePlayInfo.player_no, &UnitMngInfo);
            unitmoveChooseUnit(&UnitMngInfo, 0, UNITMOVE_CHOOSE_CURRENT);
            */
            /* -------- Set some display values ------ */
            /*
            infobarUpdateGlobalInfo(UnitMngInfo.pos, GamePlayInfo.player_no);
            infobarTileInfo(UnitMngInfo.pos, GamePlayInfo.player_no, GamePlayInfo.chosen_planetno);
            */
            break;

        case GRES_UNIT_COLONIZED:
            /* mainscrUnitColonized(pmsg); */
            break;

        /* ------ Actions to undertake --- */
        case GRES_UNIT_MOVED:
            /* mainscrUnitMoved(pmsg); */
            break;

        case MSG_DIPL_START:
            /* TODO: Open diplomacy negotiation screen with caller as other player */
            break;


        case MSG_UNIT_DONE:
            /*
            unitmoveChooseUnit(&UnitMngInfo, 0, UNITMOVE_CHOOSE_NEXT);
            */
            break;

        default:
            /*
            infobarHandleMsg(pmsg->num, pmsg);
            */
            break;
    }
}

/*
 * Name:
 *     mainscr_input_func
 * Description:
 *     Input translation function for menu
 * Input:
 *      pevent *:
 */
static int mainscr_input_func(SDLGL_EVENT_T *pevent)
{
    MSG_INFO_T msg;


    /* FIRST: Check for emergency exit */
    if(pevent->sdlcode == SDLK_ESCAPE)
    {
        return SDLGL_INPUT_EXIT;
    }

    /* SECOND: Don't allow any action, if an animation is running */
    #if 0
    if(mapdraw_anim_on())
    {
        return SDLGL_INPUT_OK;
    }
    #endif

    /* THIRD: Don't allow input, if a message is handled */
    /* Display something, diplomatic contact with another player */
    if(game_get_msg(&msg, 0))
    {
        /* FIXME: Log this message using 'pmsg'-module */
        mainscr_process_game_msg(&msg);

        return SDLGL_INPUT_OK;
    }

    /* FOURTH: Translate GUI-Input */
    if(mainscr_process_input(pevent) == GRES_EXIT_SCREEN)
    {
        /* Return to calling 'Main-Menu' screen */
        return SDLGL_INPUT_REMOVE;
    }

    /* FIFTH: Let the game do the AI / translate my input, lets game processs ther results of the actions */
    game_main();

    /* SIXTH: Get play info, may have changed in the meantime */
    game_get_playinfo(&GamePlayInfo);

    return SDLGL_INPUT_OK;
}

/* ========================================================================== */
/* ======================== PUBLIC FUNCTIONS	============================= */
/* ========================================================================== */

/*
 * Name:
 *     mainscr_main
 * Description:
 *     Starts a game depending on given 'which'.
 * Input:
 *     which:  Menu chosen
 */
int mainscr_main(int which)
{
    game_start(which, &Default_Game);

    /* ------- Now get my management data: Nation-number -------- */
    game_get_playinfo(&GamePlayInfo);

    /* --------- Now set up the display ------------------ */
    sdlgl_input_new(mainscr_draw_func,
                    mainscr_input_func,
                    MainScr_InputArea,
                    MainScr_CmdKeys,
                    MAINCSCR_MAX_FIELDS - 1,
                    0);

    return 1;
}

