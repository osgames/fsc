/*******************************************************************************
*   GAMEMENU.C                                                                 *
*	    - Screens for setting up of a new game and game settings               *
*                                                                              *
*	FREE SPACE COLONISATION               	                                   *
*      Copyright (C) 2002-2017  Paul Müller <muellerp61@bluewin.ch>            *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*******************************************************************************/

/*******************************************************************************
* INCLUDES								                                       *
*******************************************************************************/

#include <string.h>


#include "sdlgl.h"

/* -- Includes frontend -- */
#include "drawtool.h"
#include "_testscr.h"           /* Screen for test purposes    */

/* -- Includes from game -- */
#include "../code/fscfile.h"
#include "../code/fscshare.h" /* Shared definitons                    */
#include "../code/fsctool.h"
#include "../code/game.h"           /* Header for game data to configure */
#include "../code/mapgen.h"
#include "../code/player.h"         /* Race-Data, for configuration      */
#include "../code/game2str.h"       /* Values as strings                 */

/* ------ Own header -------- */
#include "gamemenu.h"

/*******************************************************************************
* DEFINES								                                       *
*******************************************************************************/

#define GAMEMENU_BASE_WSIZE      3 /* Start with 3 sectors (instead of 5)       */
#define GAMEMENU_BASE_HSIZE      3
#define GAMEMENU_BASE_TILESPERSECTOR 12
#define GAMEMENU_BASE_HABITABLE  5
#define GAMEMENU_BASE_DIFFICULTY 0

/* ------ Input-Fields -------- */
#define GAMEMENU_FILE_SAVE      ((char)1)
#define GAMEMENU_FILE_LOAD      ((char)2)
#define GAMEMENU_ABILITYBONUS   ((char)8)
#define GAMEMENU_BACK           ((char)9)
#define GAMEMENU_CONFIRM        ((char)10)

#define GAMEMENU_DIFFICULTY  ((char)20)
#define GAMEMENU_STARDENSITY ((char)21)

/*******************************************************************************
* DATA  								                                       *
*******************************************************************************/

/* Copy of player struct, for editing */
static int AbilityShare = 10;



static PLAYER_RACE_T Race;
static GAME_STARTINFO_T EditGame;     /* Copy to work with                    */

/* ------ Default-Info for the test game -------------- */
static GAME_STARTINFO_T DefaultGame =
{
    /* Preset info for test games... */
    /* ------------ Data about the size of the galaxy ----------------- */
    0,              /* Random seed to use for generation of this map    */
                    /* if 0, a random seed is generated as the map is   */
                    /* created                                          */
    /* ------------ Data about the size of the galaxy ----------------- */
    GAMEMENU_BASE_WSIZE,
    GAMEMENU_BASE_HSIZE,            /* In sectors                           */
    GAMEMENU_BASE_TILESPERSECTOR,   /* For handling of sectors              */
    /* ------------ Data used while for map generation ---------------- */
	4,                          /* Number of players            		    */
    5,                          /* The chance in pointd from 'habitable'    */
    0,                          /* How difficult it is for the human leader */
    1,                          /* Star density                             */
    16, 12                      /* Viewport-Size in tiles                   */
};


static SDLGL_LABEL_T Setup_Labels[] =
{
    /* All the strings to draw */
    { SDLGL_VAL_STRING,  80,  70, "Empire Name:", 0, 12 },
    { SDLGL_VAL_STRING,  80, 100, "Leader Name:" },
    /* ------ Political parties ----- */
    { SDLGL_VAL_STRING, 196, 156, "Political parties" },
    { SDLGL_VAL_STRING, 115, 191, "Federalists" },
    { SDLGL_VAL_STRING, 115, 235, "Populists" },
    { SDLGL_VAL_STRING, 115, 278, "War Party" },
    { SDLGL_VAL_STRING, 300, 191, "Pacifists" },
    { SDLGL_VAL_STRING, 300, 235, "Technologists" },
    { SDLGL_VAL_STRING, 300, 278, "Mercantilists" },
    /* --------------------- */
    { SDLGL_VAL_STRING, 162, 335, "Difficulty Level" },
    /* --------------------- */
    { SDLGL_VAL_STRING, 172, 412, "Ability Bonuses" },
    { SDLGL_VAL_STRING, 161, 469, "Weapons" },        /* Left 5 Values to change */
    { SDLGL_VAL_STRING, 161, 514, "Defense" },
    { SDLGL_VAL_STRING, 161, 559, "Speed" },
    { SDLGL_VAL_STRING, 161, 604, "Population" },
    { SDLGL_VAL_STRING, 161, 649, "Research" },
        /* --- */
    { SDLGL_VAL_STRING, 362, 469, "Influence" },       /* Right 5 Values to change */
    { SDLGL_VAL_STRING, 362, 514, "Trade" },
    { SDLGL_VAL_STRING, 362, 559, "Diplomacy" },
    { SDLGL_VAL_STRING, 362, 604, "Sensors" },
    { SDLGL_VAL_STRING, 362, 649, "Espionage" },
    /* --------------------- */
    { SDLGL_VAL_STRING, 705, 334, "Galaxy Size" },
    { SDLGL_VAL_STRING, 705, 429, "Star Density" },
    { SDLGL_VAL_STRING, 705, 524, "Habitable Planets" },
    { SDLGL_VAL_STRING, 705, 619, "Number of Players" },
    { 0 }
};

static SDLGL_INPUT_T Game_SetupFields[40] =
{
    /* Empire Name: from PLAYER_T.race */
    { SDLGL_TYPE_TEXT, { 250,  67, 248, 20 }, 1, 0, &Race.name, SDLGL_VAL_STRING, 0, FSC_NAMELEN_MAX },
    /* Leader Name: from PLAYER_T.race */
    { SDLGL_TYPE_TEXT, { 250,  97, 248, 20 }, 1, 0, &Race.leader_name, SDLGL_VAL_STRING, 0, FSC_NAMELEN_MAX },
    /* ------ Political parties: Choose radio -------- */
    { SDLGL_TYPE_RADIO, {  84, 184, 25, 25 }, 1, 0, &Race.pol_party, SDLGL_VAL_CHAR, 0, 5 },
    { SDLGL_TYPE_RADIO, {  84, 226, 25, 25 }, 1, 1, &Race.pol_party, SDLGL_VAL_CHAR, 0, 5 },
    { SDLGL_TYPE_RADIO, {  84, 270, 25, 25 }, 1, 2, &Race.pol_party, SDLGL_VAL_CHAR, 0, 5 },
    { SDLGL_TYPE_RADIO, { 267, 184, 25, 25 }, 1, 3, &Race.pol_party, SDLGL_VAL_CHAR, 0, 5 },
    { SDLGL_TYPE_RADIO, { 267, 226, 25, 25 }, 1, 4, &Race.pol_party, SDLGL_VAL_CHAR, 0, 5 },
    { SDLGL_TYPE_RADIO, { 267, 270, 25, 25 }, 1, 5, &Race.pol_party, SDLGL_VAL_CHAR, 0, 5 },
    /* ---------- Advantage factor ------ */
    { SDLGL_TYPE_CHOOSENUM, { 132, 358, 208, 20 }, 1, GAMEMENU_DIFFICULTY, &EditGame.difficulty, SDLGL_VAL_INT, 0, 5, 1 },
    /* --- Ability bonuses --- */
    { SDLGL_TYPE_NUMBER, { 220, 430, 30, 20 }, 1, 0, &AbilityShare, SDLGL_VAL_INT, 0, 10, 1  },
    /* -----> Horchoose: left 15; middle 20; right 15;  ------ */
    { SDLGL_TYPE_CHOOSENUM, {  92, 467, 60, 20 }, GAMEMENU_ABILITYBONUS, 0, &Race.fix_bonus[0], SDLGL_VAL_CHAR, 0, 3 },
    { SDLGL_TYPE_CHOOSENUM, {  92, 512, 60, 20 }, GAMEMENU_ABILITYBONUS, 1, &Race.fix_bonus[1], SDLGL_VAL_CHAR, 0, 3 },
    { SDLGL_TYPE_CHOOSENUM, {  92, 557, 60, 20 }, GAMEMENU_ABILITYBONUS, 2, &Race.fix_bonus[2], SDLGL_VAL_CHAR, 0, 3 },
    { SDLGL_TYPE_CHOOSENUM, {  92, 602, 60, 20 }, GAMEMENU_ABILITYBONUS, 3, &Race.fix_bonus[3], SDLGL_VAL_CHAR, 0, 3 },
    { SDLGL_TYPE_CHOOSENUM, {  92, 647, 60, 20 }, GAMEMENU_ABILITYBONUS, 4, &Race.fix_bonus[4], SDLGL_VAL_CHAR, 0, 3 },
            /* ----- */
    { SDLGL_TYPE_CHOOSENUM, { 293, 467, 60, 20 }, GAMEMENU_ABILITYBONUS, 5, &Race.fix_bonus[5], SDLGL_VAL_CHAR, 0, 3 },
    { SDLGL_TYPE_CHOOSENUM, { 293, 512, 60, 20 }, GAMEMENU_ABILITYBONUS, 6, &Race.fix_bonus[6], SDLGL_VAL_CHAR, 0, 3 },
    { SDLGL_TYPE_CHOOSENUM, { 293, 557, 60, 20 }, GAMEMENU_ABILITYBONUS, 7, &Race.fix_bonus[7], SDLGL_VAL_CHAR, 0, 3 },
    { SDLGL_TYPE_CHOOSENUM, { 293, 602, 60, 20 }, GAMEMENU_ABILITYBONUS, 8, &Race.fix_bonus[8], SDLGL_VAL_CHAR, 0, 3 },
    { SDLGL_TYPE_CHOOSENUM, { 293, 647, 60, 20 }, GAMEMENU_ABILITYBONUS, 9, &Race.fix_bonus[9], SDLGL_VAL_CHAR, 0, 3 },
    /* ------ Galaxy size and habitable planets --------- */
    { SDLGL_TYPE_CHOOSENUM, { 705, 358, 135, 20 }, 1, 0, &EditGame.secsize_w, SDLGL_VAL_INT, 3, 9 },
    { SDLGL_TYPE_CHOOSENUM, { 705, 453, 135, 20 }, 1, GAMEMENU_STARDENSITY, &EditGame.star_density, SDLGL_VAL_INT, 0, 2, 1 },
    { SDLGL_TYPE_CHOOSEH,   { 705, 548, 135, 20 }, 1, 0, &EditGame.habit_chance, SDLGL_VAL_INT, 0, 10, 0, 10 },
    { SDLGL_TYPE_CHOOSENUM, { 705, 643, 135, 20 }, 1, 0, &EditGame.num_player, SDLGL_VAL_INT, 2, 8 },
    /* ---------- Buttons -------- */
    { SDLGL_TYPE_BUTTON,    {  45, 696,  95, 21 }, GAMEMENU_BACK, 0, "Back" },
    { SDLGL_TYPE_BUTTON,    { 877, 696,  95, 21 }, GAMEMENU_CONFIRM, 0, "Confirm" },
    { 0 }

};

static SDLGL_INPUTKEY_T Game_SetupCmd[] =
{
    { { SDLK_ESCAPE }, GAMEMENU_BACK },
    { { SDLK_RETURN }, GAMEMENU_CONFIRM },
    { { 0 } }
};

/* ================= Savegame-Menu ================== */

static char Game_FileNames[32 * 12];

static SDLGL_INPUT_T Game_SaveNames[] =
{
    { SDLGL_TYPE_MENU, { 250, 160, 248, 16 }, GAMEMENU_FILE_SAVE,  1, &Game_FileNames[0],   SDLGL_VAL_STRING, 0, 31 },
    { SDLGL_TYPE_MENU, { 250, 182, 248, 16 }, GAMEMENU_FILE_SAVE,  2, &Game_FileNames[32],  SDLGL_VAL_STRING, 0, 31 },
    { SDLGL_TYPE_MENU, { 250, 204, 248, 16 }, GAMEMENU_FILE_SAVE,  3, &Game_FileNames[64],  SDLGL_VAL_STRING, 0, 31 },
    { SDLGL_TYPE_MENU, { 250, 226, 248, 16 }, GAMEMENU_FILE_SAVE,  4, &Game_FileNames[96],  SDLGL_VAL_STRING, 0, 31 },
    { SDLGL_TYPE_MENU, { 250, 248, 248, 16 }, GAMEMENU_FILE_SAVE,  5, &Game_FileNames[128], SDLGL_VAL_STRING, 0, 31 },
    { SDLGL_TYPE_MENU, { 250, 270, 248, 16 }, GAMEMENU_FILE_SAVE,  6, &Game_FileNames[160], SDLGL_VAL_STRING, 0, 31 },
    { SDLGL_TYPE_MENU, { 250, 292, 248, 16 }, GAMEMENU_FILE_SAVE,  7, &Game_FileNames[192], SDLGL_VAL_STRING, 0, 31 },
    { SDLGL_TYPE_MENU, { 250, 314, 248, 16 }, GAMEMENU_FILE_SAVE,  8, &Game_FileNames[224], SDLGL_VAL_STRING, 0, 31 },
    { SDLGL_TYPE_MENU, { 250, 336, 248, 16 }, GAMEMENU_FILE_SAVE,  9, &Game_FileNames[256], SDLGL_VAL_STRING, 0, 31 },
    { SDLGL_TYPE_MENU, { 250, 358, 248, 16 }, GAMEMENU_FILE_SAVE, 10, &Game_FileNames[288], SDLGL_VAL_STRING, 0, 31 },
    { SDLGL_TYPE_MENU, { 250, 380, 248, 16 }, GAMEMENU_BACK, 0, "Cancel", SDLGL_VAL_STRING },
    { 0 }
};

/*******************************************************************************
* CODE  								                                       *
*******************************************************************************/

/*
 * Name:
 *     gamemenu_set_valuestrings
 * Description:
 *     Does all the drawing for this screen.
 * Input:
 *     None
 */
static void gamemenu_set_valuestrings(void)
{
    SDLGL_INPUT_T *pinput;


    pinput = &Game_SetupFields[0];

    while(pinput->sdlgl_type > 0)
    {
        if(pinput->sub_code == GAMEMENU_DIFFICULTY)
        {
            /* Draw the string for the difficulty */
            /*  */
            pinput->pnames = game2str_label(GAME2STR_LABEL_DIFFICULTY, 0);
        }
        else if(pinput->sub_code == GAMEMENU_STARDENSITY)
        {
            /* Draw the string for the difficulty */
            /* pinput->pnames */
            pinput->pnames = game2str_label(GAME2STR_LABEL_STARDENSITY, 0);
        }

        pinput++;
    }
}

/*
 * Name:
 *     gamemenu_change_ability
 * Description:
 *     Adjusts the abilty bonus
 * Input:
 *     pevent *: Pointer on pevent to translate
 */
static void gamemenu_change_ability(SDLGL_EVENT_T *pevent)
{
    int i, use_diff, total_use;


    for(i = 0, total_use = 0; i < 10; i++)
    {
        total_use += Race.fix_bonus[i];
    }

    use_diff = AbilityShare - total_use;

    if(use_diff >= 0)
    {
        /* Push it back into buffer for AbilityShare */
        AbilityShare = use_diff;
    }
    else if(use_diff < 0)
    {
        AbilityShare = 0;

        /* Remove what is chosen too much */
        Race.fix_bonus[(int)pevent->sub_code] += use_diff;
    }
}

/*
 * Name:
 *     gamemenu_setup_display_func
 * Description:
 *     Does all the drawing for this screen.
 * Input:
 *     pfields *: Pointer on pfields to draw
 *     pevent *:  Holds mouse position
 */
static void gamemenu_setup_display_func(SDLGL_INPUT_T *pfields, SDLGL_EVENT_T *pevent)
{
    /* @TODO: Draw a better background */
    drawtool_draw_starbk(120);

    /* 1) Draw the standard fields and buttons */
    drawtool_drawfields(pfields, NULL);

    /* 2) draw all the labels */
    drawtool_labels(Setup_Labels);
}

/*
 * Name:
 *     gamemenu_setup_process_input
 * Description:
 *     Includes nation and leader setup in one screen
 * Input:
 *     pevent *:  Event to translate
 */
static int gamemenu_setup_process_input(SDLGL_EVENT_T *pevent)
{
    if(pevent->code > 0)
    {
        switch(pevent->code)
        {
            case GAMEMENU_FILE_LOAD:
                /* fscfile_load(pevent->sub_code); */
                break;

            case GAMEMENU_FILE_SAVE:
                /* fscfile_save(pevent->sub_code); */
                break;

            case GAMEMENU_BACK:
                return SDLGL_INPUT_REMOVE;

            case GAMEMENU_CONFIRM:
                /* Load/save the game chosen */
                /* game_start(GAME_NEWGAME, &EditGame); */
                return SDLGL_INPUT_REMOVE;
        }
    }

    return SDLGL_INPUT_OK;
}


/*
 * Name:
 *     gamemenu_file_display_func
 * Description:
 *     Does all the drawing for this screen.
 * Input:
 *     pfields *: Pointer on pfields to draw
 *     pevent *:  Holds mouse position
 */
static void gamemenu_file_display_func(SDLGL_INPUT_T *pfields, SDLGL_EVENT_T *pevent)
{
    /* 1) Draw the standard fields and buttons */
    drawtool_drawfields(pfields, NULL);
}

/*
 * Name:
 *     gamemenu_file_process_input
 * Description:
 *     Processes the input for loading/saving games
 * Input:
 *     pevent *:  Event to translate
 */
static int gamemenu_file_process_input(SDLGL_EVENT_T *pevent)
{
    if(pevent->code > 0)
    {
        switch(pevent->code)
        {
            case GAMEMENU_ABILITYBONUS:
                gamemenu_change_ability(pevent);
                break;

            case GAMEMENU_BACK:
                return SDLGL_INPUT_REMOVE;

            case GAMEMENU_CONFIRM:
                /* The galaxy is square in sectors */
                EditGame.secsize_h = EditGame.secsize_w;

                /* Generate the map */
                mapgen_new_map(&EditGame);
                /* And now display the map */

                /* game_start(GAME_NEWGAME, &EditGame); */
                return SDLGL_INPUT_REMOVE;
        }
    }

    return SDLGL_INPUT_OK;
}



/* ========================================================================== */
/* ======================== PUBLIC FUNCTIONS	============================= */
/* ========================================================================== */

/*
 * Name:
 *     gamemenu_new_game
 * Description:
 *     Sets up the input screen for a new game and returns the chosen settings
 *     in given structure.
 * Input:
 *     None
 */
void gamemenu_new_game(void)
{
    int i;

    /* Init default values:     */
    memcpy(&EditGame, &DefaultGame, sizeof(GAME_STARTINFO_T));

    AbilityShare = 10;

    player_race(1, &Race, 0);

    /* Set the names of the named values */
    gamemenu_set_valuestrings();

    sdlgl_input_new(gamemenu_setup_display_func,
                    gamemenu_setup_process_input,
                    Game_SetupFields,
                    Game_SetupCmd,
                    40 - 1,
                    0);
}

/*
 * Name:
 *     gamemenu_choose_game
 * Description:
 *     Sets up the input screen for saving a game
 * Input:
 *     load: Otherwise save
 */
void gamemenu_choose_game(int load)
{
    int i;
    char code;


    code = load ? GAMEMENU_FILE_LOAD : GAMEMENU_FILE_SAVE;

    for(i = 0; i < FSCFILE_MAXSAVE; i++)
    {
        Game_SaveNames[i].code = code;
        /* -- For test purposes, create the names -- */
        sprintf(Game_SaveNames[i].pdata, "Savegame No. %d", i + 1);
    }

    sdlgl_input_new(gamemenu_file_display_func,
                    gamemenu_file_process_input,
                    Game_SaveNames,
                    Game_SetupCmd,
                    40 - 1,
                    0);
}
