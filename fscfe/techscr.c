/*******************************************************************************
*  TECHSCR.C                                                                   *
*      - The input screen for technology management and it's subscreens        *
*                                                                              *
*  FREE SPACE COLONISATION                                                     *
*      (c)2002 Paul Mueller <pmtech@swissonline.ch>                            *
*                                                                              *
*  This program is free software; you can redistribute it and/or modify        *
*  it under the terms of the GNU General Public License as published by        *
*  the Free Software Foundation; either version 2 of the License, or           *
*  (at your option) any later version.                                         *
*                                                                              *
*  This program is distributed in the hope that it will be useful,             *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of              *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
*  GNU Library General Public License for more details.                        *
*                                                                              *
*  You should have received a copy of the GNU General Public License           *
*  along with this program; if not, write to the Free Software                 *
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  *
*******************************************************************************/

/*******************************************************************************
* INCLUDES                                                                     *
*******************************************************************************/

#include <string.h>         /* memset() */


#include "sdlgl.h"
#include "sdlglfld.h"
#include "sdlgltex.h"
#include "drawtool.h"
#include "infobar.h"
#include "fscfont.h"
#include "nation.h"
#include "techlist.h"


#include "fscfe/techscr.h"

/*******************************************************************************
* ENUMS             					                                       *
*******************************************************************************/

#define TC_NONE     0x00
#define TC_CHOOSE   0x01
#define TC_DONE     0x02
#define TC_ARCHIVES 0x03
#define TC_MARK     0x04
#define TC_TECHLIST 0x05

#define TSD_ADVANCE ((char)SDLGL_TYPE_MENU + 10)

/*******************************************************************************
* DATA                                                                         *
*******************************************************************************/

/* ------ In case the nation chooses to change the research ------- */
static int changed_from;  /* if the nation changed techs, which one changed   */
static int before_change; /* if the nation changed techs, how many            */
			              /* from bulbs he had before the change              */
static GAME_MNG *TechScrInfo;
static TECH_MNG TechMngInfo;

static SDLGL_FIELD TechscrManageFields[] = {

    /* ------ Techs scroll list(FSCCMD_CHOOSETECH) ----------------------- */
    { SDLGLFLD_SLI_BOX, {  46, 116, 160, 400 }, TC_TECHLIST, SDLGLFLD_SLI_BOX }, /* Box with elements    */
    { SDLGLFLD_SLI_BOX, { 226, 128,  12, 376 }, TC_TECHLIST, SDLGLFLD_SLI_SLIBK }, /* Background of slider */
    { SDLGLFLD_SLI_BOX, { 226, 128,  12,  47 }, TC_TECHLIST, SDLGLFLD_SLI_SLIBUTTON }, /* Slider button        */
    { SDLGLFLD_SLI_BOX, { 226, 504,  12,  12 }, TC_TECHLIST, SDLGLFLD_SLI_ARROWDOWN },
    { SDLGLFLD_SLI_BOX, { 226, 116,  12,  12 }, TC_TECHLIST, SDLGLFLD_SLI_ARROWUP },
    { SDLGLFLD_SLI_BOX, {  46, 116, 160,  50 }, TC_TECHLIST, SDLGLFLD_SLI_ELEMENT }, /* Actual element       */
    /* -------- Advance in technology points --- */
    { TSD_ADVANCE, { 30, 60, 600, 16 } },
    /* -------- The different buttons ------------ */
    { SDLGL_TYPE_BUTTON, {  86, 530, 94, 25 }, TC_CHOOSE },             /* Choose it    */
    /* ------ Basic tech screen buttons --------------- */
    { SDLGL_TYPE_BUTTON, {  82, 643, 94, 25 }, TC_ARCHIVES },
    { SDLGL_TYPE_BUTTON, { 614, 643, 94, 25 }, TC_DONE }, /* Close screen */
    { 0 }

};

static SDLGL_CMDKEY TechScrCmdKey[] =
{
    { { SDLK_RETURN }, TC_DONE },
    { { 0 } }
};

/* ------- The different labels to draw --------- */
static FSCFONT_LABEL TechscrLabels[] = {

    { FSCFONT_VALSTR, 394, 10, "Technology advisor", FSCFONT_TEXT_HCENTER, { 85, 123, 142 }, 20 },
    /* -------- The button labels -------- */
    { FSCFONT_VALSTR,  86 + 47, 530 + 12, "Choose", FSCFONT_TEXT_CENTER, { 255, 255, 255 } , 12 } ,
    { FSCFONT_VALSTR,  82 + 47, 643 + 12, "Archives", FSCFONT_TEXT_CENTER },
    { FSCFONT_VALSTR, 614 + 47, 643 + 12, "Done", FSCFONT_TEXT_CENTER },
    { FSCFONT_VALSTR,  30, 40, "Researching:" },
    { FSCFONT_VALSTR, 180, 40 },                    /* Name of technology actual researched */
    { 0 }

};


static SDLGLFLD_DATASLIDERBOX TechListBox = {

    TC_TECHLIST,        /* Can be used by user to find box in a list         */
    0,                  /* Number of top element                             */
    8,                  /* Number of elements visible in slider box          */
    0,                  /* Number of elements in slider box                  */
    0,                  /* Number of horizonal elements for h. slider boxes  */
                        /* if > 0, it's a horizontal sliderbox               */
    0,                  /* For highlight info: From top of box               */
    0,                  /* Number of actual element (effective)              */
    &TechscrManageFields[0]  /* Pointer on first field of box                */

};

/*******************************************************************************
* CODE                                                                         *
*******************************************************************************/

/*
 * Name:
 *     techscrDrawSliderBoxElements
 * Description:
 *     Draws the elements of the slider box. Field has to point on the box
 * Input:
 *     field *: Pointer on box of slider box
 */
static void techscrDrawSliderBoxElements(SDLGL_FIELD *field)
{

    SDLGL_RECT drawrect;
    int i, numdrawel;


    drawrect.x = field -> rect.x + 3;
    drawrect.y = field -> rect.y + 3;
    drawrect.w = field -> rect.w - 6;
    drawrect.h = field -> rect.h / TechListBox.elvisi;  /* Size of element */


    fscfontSetSize(10);
    fscfontSetColorNo(SDLGL_COL_WHITE);

    numdrawel = TechListBox.numelement;
    if (numdrawel > TechListBox.elvisi) {

        numdrawel = TechListBox.elvisi;

    }

    for (i = 0; i < numdrawel; i++) {

        fscfontStringToRect(&drawrect,
                            techlistName(TechMngInfo.reachable_list[TechListBox.eltop + i]),
                            FSCFONT_TEXT_TORECT );

        drawrect.y += drawrect.h;

    }

}

/*
 * Name:
 *     techscrChooseTech
 * Description:
 *     Choose a tech, depending on the mouse position
 * Input:                                                   7
 *     None
 */
static void techscrChooseTech(void)
{

    NATION *pnation;


    pnation  = nationGet(TechScrInfo -> nation_no);

    if (pnation -> research.actual > 0) {

        /* Don't allow change of tech if one is chosen  */
        /* return; */
        /* Allow changig of tech for test purposes          */
        /* TODO: Add penalty, if chosen research is changed */

    }

    pnation -> research.actual = TechMngInfo.reachable_list[TechListBox.actel];
     /* --------- Calculate cost and invested ------- */
    pnation -> research.cost   = techlistBulbsRequired(&TechMngInfo, pnation -> research.actual);

    /* Now set the field for the technology chosen */
    TechscrLabels[5].plabel = techlistName(pnation -> research.actual);
    
   

}

/*
 * Name:
 *     techscrTranslateInput
 * Description:
 *     Translates the input for the star system screen
 * Input:
 *     event *: Struct holding the input event info
 */
static int techscrTranslateInput(SDLGL_EVENT *event)
{

    if (event -> code > 0) {

        switch(event -> code) {

            case TC_TECHLIST:
                /* Possible update needed */
                sdlglfldSliderBox(event, &TechListBox);
                /* TODO: Display costs and advantages of tech chosen in techlist */
                /* TechMngInfo.reachable_list[TechListBox.actel] */
                break;

            case TC_CHOOSE:
                techscrChooseTech();
                break;

            case TC_DONE:
                return SDLGL_INPUT_REMOVE;

            case TC_ARCHIVES:
                break;

        } /* switch(event -> code) */

    }

    return SDLGL_INPUT_OK;

}

/*
 * Name:
 *     techscrDrawFields
 * Description:
 *     Returns a pointer on the input fields for this screen
 * Input:
 *      fields *: Pointer on array of fields to draw
 *      event *:  Event holding different info used for drawing
 */
static void techscrDrawFields(SDLGL_FIELD *fields)
{

    NATION *pnation;


    if (fields) {

        while(fields -> sdlgl_type != 0) {

            switch (fields -> sdlgl_type) {

                case SDLGLFLD_SLI_BOX:
                    if (fields -> sub_code == SDLGLFLD_SLI_BOX) {

                        drawtoolSliderBox(fields, fields -> code); /* The whole Box */
                        techscrDrawSliderBoxElements(fields);

                    }
                    break;

                case SDLGL_TYPE_BUTTON:
                    drawtoolButton(&fields -> rect, DRAWTOOL_BUTTON7, fields -> fstate);
                    break;

                case TSD_ADVANCE:
                    pnation = nationGet(TechScrInfo -> nation_no);
                    if (pnation -> research.cost > 0) {

                        drawtoolHorizValueBar(&fields -> rect, DRAWTOOL_BAR2,
                                              pnation -> research.invested,
                                              pnation -> research.cost);

                    }
                    break;

            }

            fields++;

        }

    }

}

/*
 * Name:
 *     techscrDisplayFunction
 * Description:
 *     Returns a pointer on the input fields for this screen
 * Input:
 *      fields *: Pointer on array of fields to draw
 *      event *:  Event holding different info used for drawing
 */
static void techscrDisplayFunction(SDLGL_FIELD *fields, SDLGL_EVENT *event)
{

    glClear(GL_COLOR_BUFFER_BIT);           /* Simply clear the screen  */

    if (fields) {

        /* 1) Draw the standard input fields...                 */
        /* drawfldMain(fields); */

        /* 2) Draw the part using the own functions for fields  */
        techscrDrawFields(fields);

        /* 3) Draw textures     */
        sdlgltexDisplayIconList(fields, 0);


    }

    /* Print all labels */
    fscfontPrintLabels(0, 0, TechscrLabels);
    infobarDrawGlobalInfo(3, 5);

}

/* ========================================================================== */
/* =========================== PUBLIC FUNCTIONS ============================= */
/* ========================================================================== */

/*
 * Name:
 *     techscrManage
 * Description:
 *     Opens the technology management screen and gathers its input
 *     until nation presses the button 'Done'
 * Input:
 *      info *: Pointer on info about actual leader
 */
void techscrManage(GAME_MNG *mng)
{

    NATION *pnation;
        
    
    TechScrInfo = mng;

    pnation  = nationGet(mng -> nation_no);
    
    /* Add preparation function and then call subscreen */
    techlistGetMngInfo(&TechMngInfo, &pnation -> research.advancebits[0]);
    TechListBox.numelement = TechMngInfo.reachable_num;

    sdlglInputNew(techscrDisplayFunction,
                  techscrTranslateInput,
                  TechscrManageFields,
                  TechScrCmdKey,
                  0);

    /* Add the fields from the display of the techs available in the */
    /* Scroll-Box                                                   */
    TechListBox.eltop = 0;      /* OVerwrite possible saved position from last call */
    changed_from  = pnation -> research.actual;
    before_change = pnation -> research.invested;

    /* Now set the field for the technology actually chosen */
    /* Now set the field for the technology chosen */
    TechscrLabels[5].plabel = techlistName(pnation -> research.actual);

}
