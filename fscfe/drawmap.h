/*******************************************************************************
*  DRAWMAP.H                                                                   *
*	    - All map drawing functions                                   	       *
*                                                                              *
*   FREE SPACE COLONISATION                                                    *
*      Copyright (C) 2002-2017  Paul Müller <muellerp61@bluewin.ch>            *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*******************************************************************************/

#ifndef _FSC_DRAWMAP_H_
#define _FSC_DRAWMAP_H_

/*******************************************************************************
* INCLUDES								                                       *
*******************************************************************************/

#include "sdlgldef.h"

/*******************************************************************************
* DEFINES								                                       *
*******************************************************************************/

/* ------- The different cursorstates --------- */
#define DRAWMAP_CURSOR_NONE       ((char)0)   /* No cursor visible at all */
#define DRAWMAP_CURSOR_UNITFOCUS  ((char)1)   /* Cursor has focus on unit */
#define DRAWMAP_CURSOR_VIEWMODE   ((char)2)   /* Cursor for view mode     */

/*******************************************************************************
* CODE								                                           *
*******************************************************************************/

void drawmap_main(SDLGL_RECT_T *prect, int grid_on);
void drawmap_minimap(SDLGL_RECT_T *prect, char player_no);

int  drawmap_animation_on(void);
void drawmap_start_animation(SDLGL_RECT_T *prect, int unitno, int srcpos, int destpos);
void drawmap_timer_func(float seconds_passed);

#endif  /* _FSC_DRAWMAP_H_ */
