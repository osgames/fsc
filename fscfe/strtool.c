/*******************************************************************************
*  STRTOOL.C                                                                   *
*      - Some tools for generating strings used by the frontend                *
*                                                                              *
*  FREE SPACE COLONISATION                                                     *
*      (c)2002 Paul Mueller <pmtech@swissonline.ch>                            *
*                                                                              *
*  This program is free software; you can redistribute it and/or modify        *
*  it under the terms of the GNU General Public License as published by        *
*  the Free Software Foundation; either version 2 of the License, or           *
*  (at your option) any later version.                                         *
*                                                                              *
*  This program is distributed in the hope that it will be useful,             *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of              *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
*  GNU Library General Public License for more details.                        *
*                                                                              *
*  You should have received a copy of the GNU General Public License           *
*  along with this program; if not, write to the Free Software                 *
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  *
*******************************************************************************/

/*******************************************************************************
* INCLUDES                                                                     *
*******************************************************************************/

#include <stdio.h>          /* sprintf()        */
#include <memory.h>         /* memcpy()         */
#include <string.h>         /* strcpy()         */


#include "fscshare.h"
#include "starinfo.h"       /* PLANET_STATE_*   */
#include "unit.h"           /* unitCreate()     */
#include "unittype.h"       /* unittypeGet()    */
#include "unitrule.h"


#include "fscfe/strtool.h"

/*******************************************************************************
* DATA                                                                         *
*******************************************************************************/

static char *UnitOrderList[] = {

    "Idle",
    "Sentry",
    "Guard",
    "Auto-Attack",
    "Auto-Retreat",
    "Patrol",
    "Border-patrol",
    "Goto",
    "Explore",
    "Build",
    "Terraform",
    "Colonize",
    "Cancel",
    "Attack",
    "Defend",
    "Traderoute",
    ""
};

static char *StrToolUnitAbilityName[] = {

    "Unknown",          /* UNIT_ABILITY_UNKNOWN         */
    "Military",         /* UNIT_ABILITY_MILITARY        */
    "Colonize",         /* UNIT_ABILITY_COLONIZE        */
    "Transport",        /* UNIT_ABILITY_TRANSPORT       */
    "Diplomacy",        /* UNIT_ABILITY_DIPLOMACY       */
    "Trade",            /* UNIT_ABILITY_TRADE           */
    "Scout",            /* UNIT_ABILITY_SCOUT           */
    "Construct",        /* UNIT_ABILITY_CONSTRUCT       */
    "Outpost",          /* UNIT_ABILITY_OUTPOST         */
    "Starbase",         /* UNIT_ABILITY_STARBASE        */
    ""

};

static char *RangeName[] = {

    "Short",
    "Middle",
    "Long"

};

static char MonthStr[] = "Jan\0Feb\0Mar\0Apr\0May\0Jun\0Jul\0Aug\0Sep\0Oct\0Nov\0Dec";
static char *StatNames[] = {

    "Population",
    "Military",
    "Economy",
    "Research",
    "Influence",
    "Diplomacy",
    "Money"

};

static char *EffectNames[] = {

    "Food",
    "Resources",
    "Economy",
    "Military",
    "Social",
    "Stock",
    "Research",
    "Taxes",
    "Luxury",
    "-",
    "-",
    "Morale",
    "Approval",
    "Net Income",
    "Expenses",
    "Assimilate",
    "-",
    "-",
    "-",
    "-",
    "-",
    "Weapons",
    "Defense",
    "Speed",
    "Pop. Growth",
    "Diplomacy",
    "Sensors",
    "Espionage",
    "Influence",
    "Navigation",
    "Hitpoints",
    "Repair",
    "Prestige",
    "Starships",
    "Soldiering",
    "-",
    "-",
    "Range",
    "Warfare",
    "Planet Quality",
    "-",
    "Propaganda",
    "Terraform",
    "Outpost",
    "Trade",
    "Population"
    ""
};

/*******************************************************************************
* CODE                                                                         *
*******************************************************************************/

/* ========================================================================== */
/* =========================== PUBLIC FUNCTION(S) =========================== */
/* ========================================================================== */

/*
 * Name:
 *     strtoolDateString
 * Description:
 *     Generates a string with month and year and prints it into the
 *     given buffer. 
 * Input:
 *     year, month: To generate the date from
 *                  Month number has to be given from 0 .. 11
 *     buffer *:    Pointer on buffer to print in the
 */
void strtoolDateString(int year, int month, char *buffer)
{

    sprintf(buffer, "%s %d", &MonthStr[month * 4], year);

}

/*
 * Name:
 *     strtoolPopulationValue
 * Description:
 *     Generates a string for a planets population using the given number.
 *     Returns a pointer on a static string holding this string
 * Input:
 *     population: In millions
 *     value *:    Where to return the string
 */
void strtoolPopulationValue(int population, char *value)
{

    int pop, poppart;
    char poppartstr[10];


    if (population < 1000) {

        sprintf(value, "%d M", population); /* In Millions */

    }
    else {

        pop     = population / 1000;    /* Full billions    */
        poppart = population % 1000;    /* Part billions    */

        sprintf(poppartstr, "%03d", poppart);
        poppartstr[2] = 0;                      /* Maximum two digits */
        sprintf(value, "%d.%s B", pop, poppartstr);

    }

}

/*
 * Name:
 *     strtoolGetString
 * Description:
 *     Returns a pointer on the string asked for.
 *     If 'buffer' is given, the string is copied into this buffer, too
 * Input:
 *     which:    Which kind of string
 *     number:   Number of string in this list
 */
char *strtoolGetString(int which, int number)
{

    char *presult;


    presult = "";       /* Return valid result in any case */

    if (number >= 0) {

        switch(which) {

            case STRTOOL_STATS:
                if (number > 0) {

                    presult = StatNames[number - 1];

                }
                break;

            case STRTOOL_UNITORDER:
                if (number < 16) {

                    presult = UnitOrderList[number];

                }
                break;

            case STRTOOL_EFFECT:    /* Of improvements */
                if (number < 45) {

                    presult = EffectNames[number];

                }
                break;

            case STRTOOL_RANGE:
                if (number < 3) {

                    presult = RangeName[number];

                }
                break;

            case STRTOOL_ABILITY:   /* Of units */
                if (number < 10) {

                    presult = StrToolUnitAbilityName[number];

                }
                break;

        }

    } /* if (number >= 0) */
    
    return presult;

}
