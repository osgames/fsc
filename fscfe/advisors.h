/*******************************************************************************
*   ADVISORS.H                                                                 *
*	    - Command functions, setting and drawing input for advisor screens     *
*                                                                              *
*  FREE SPACE COLONISATION               	                                   *
*      Copyright (C) 2002-2017  Paul Müller <muellerp61@bluewin.ch>            *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*******************************************************************************/

#ifndef _FSC_ADVISORS_H_
#define _FSC_ADVISORS_H_

/*******************************************************************************
* DEFINES 								                                       *
*******************************************************************************/

/* =========== The different advisors =========== */
typedef enum
{
    ADVISOR_NONE         = 0,
    ADVISOR_DOMESTIC     = 1,   /* Planet list: Colonies and outposts   */
    ADVISOR_TRADE        = 2,   /* Active trade routes,trad income
                                      and special resources        */
    ADVISOR_MILITARY     = 3,   /* Unit-List                            */
    ADVISOR_FOREIGN      = 4,   /* Diplomacy-Screen                     */
    ADVISOR_CULTURE      = 5,   /* Influence                            */
    ADVISOR_TECH         = 6,   /* Choose technology and tech goals     */
    ADVISOR_WONDERLIST   = 7,   /* List of Wonders built                */
    ADVISOR_HISTOGRAPHY  = 8,   /*                                      */
    ADVISOR_PLANETMANAGE = 9,   /*                                      */
    ADVISOR_DEMOGRAPHY   = 10,  /*                                      */
    ADVISOR_STATS        = 11   /*                                      */
}
E_ADVISOR_TYPE;

/*******************************************************************************
* CODE  								                                       *
*******************************************************************************/

void advisor_main(int which);

#endif  /* _FSC_ADVISORS_H_ */
