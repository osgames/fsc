/*******************************************************************************
*   FOREISCR.H                                                                 *
*	   - Drawing and generation of input fields for the                        *
*        'Foreign policy Screen'                                               *
*                                                                              *
*   FREE SPACE COLONISATION             								       *
*       Copyright (C) 2002  Paul Mueller <pmtech@swissonline.ch>               *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*******************************************************************************/

#ifndef _FSC_FOREIGNSCR_H_
#define _FSC_FOREIGNSCR_H_

/*******************************************************************************
* INCLUDES                                                                     *
*******************************************************************************/

#include "fscfe/sdlgl.h"

/*******************************************************************************
* DEFINES                                                                      *
*******************************************************************************/

#define FOREISCR_RELATIONS      1
#define FOREISCR_STATS          2
#define FOREISCR_REPORT         3
#define FOREISCR_TREATIES       4
#define FOREISCR_UNITEDPLANETS  5
#define FOREISCR_MINORRACES     6

/*******************************************************************************
* CODE                                                                         *
*******************************************************************************/

SDLGL_FIELD *foreiscrSetDisplay(int subscr);
void foreiscrDraw(int subscr);

#endif  /* _FSC_FOREIGNSCR_H_ */
