/*******************************************************************************
*  INFOBAR.C                                                                   *
*      - Functions and data for handling of infobar menu				       *
*                                                                              *
*  FREE SPACE COLONISATION                                                     *
*      (c)2004-2010 Paul Mueller <pmtech@swissonline.ch>                       *
*                                                                              *
*  This program is free software; you can redistribute it and/or modify        *
*  it under the terms of the GNU General Public License as published by        *
*  the Free Software Foundation; either version 2 of the License, or           *
*  (at your option) any later version.                                         *
*                                                                              *
*  This program is distributed in the hope that it will be useful,             *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of              *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
*  GNU Library General Public License for more details.                        *
*                                                                              *
*  You should have received a copy of the GNU General Public License           *
*  along with this program; if not, write to the Free Software                 *
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  *
*******************************************************************************/

/*******************************************************************************
* INCLUDES                              				                       *
*******************************************************************************/

#include <memory.h>             /* memcpy(), memset()   */
#include <string.h>             /* strncpy()            */


#include "sdlgl.h"
#include "sdlgltex.h"
#include "fscfont.h"
#include "fscinit.h"
#include "drawtool.h"


#include "fsccmd.h"
#include "fscmap.h"
#include "improve.h"
#include "msg.h"
#include "language.h"
#include "nation.h"
#include "outpost.h"
#include "starinfo.h"
#include "star.h"
#include "unit.h"
#include "unitinfo.h"           /* Holding comands to set           */
#include "unitrule.h"           /* Get possible actions for unit    */


#include "infobar.h"

/*******************************************************************************
* DEFINES                                  				                       *
*******************************************************************************/

/* ----- Different cursors ------ */
#define INFOBAR_CURSOR_FIX       0
#define INFOBAR_CURSOR_GREEN     1
#define INFOBAR_CURSOR_RED       2
#define INFOBAR_CURSOR_BLUE      3
#define INFOBAR_CURSOR_OWNERRECT 4
#define INFOBAR_DRAW_NOTHING     10000

#define INFOBAR_MSGLINELEN    119
#define INFOBAR_DEBUGINFOSIZE 256

/* --------- Position of unit orders menu -------- */

#define INFOBAR_UNITORDER_W 244
#define INFOBAR_UNITORDER_H 160

#define INFOBAR_UNITINFO_Y  344
#define INFOBAR_UNITINFO_H   80     /* Including command */

#define INFOBAR_MAX_MSG 100

/*******************************************************************************
* TYPEDEFS                                                                     *
*******************************************************************************/

typedef struct {

    /* ------ Strings always displayed on main screen --------------------- */
    char sectorname[32];    /* Actual chosen sector as text                 */
    char datestr[20];       /* Actual game date as string                   */
    char moneystr[20];      /* Actual amount of money as string             */
    char popvalstr[20];     /* Population as string                         */
    char statisticname[20]; /* Title of statistic screen                    */

} INPUT_STATE;

/*******************************************************************************
* DATA                                    				                       *
*******************************************************************************/

static MSG_INFO PlanetMenuMsg;
static char DebugInfo[INFOBAR_DEBUGINFOSIZE + 2];
static char MsgLine[INFOBAR_MSGLINELEN + 2]; /* Message line for actual message */
/* Buffer for 'End-of-Turn' messages */
static int NumMsg;
static MSG_INFO Messages[INFOBAR_MAX_MSG + 2];

static INPUT_STATE InputState;
static FSCFONT_LABEL PlanetLabels[10];
static FSCFONT_LABEL StarInfoLabels[20];

static char TerraformText[] = "Terraform: ";
static char ColonyText[] = "Colony";

static FSCFONT_LABEL GlobalInfoLabels[6] = {
    { FSCFONT_VALSTR,   5,  3, InputState.sectorname, 0, { 205, 154,  89 }, 12 },
    { FSCFONT_VALSTR, 150,  3, InputState.datestr, 0 },
    { FSCFONT_VALSTR, 270,  3, InputState.moneystr, 0, { 1, 224, 0 } },
    { FSCFONT_VALSTR, 420,  3, InputState.popvalstr, 0, {255, 255, 255 } },
    { 0 }
};

static FSCFONT_LABEL MsgLineLabel[] = {
    { FSCFONT_VALSTR,  100,  24, MsgLine, 0, {255, 255, 255 }, 12 },
    { 0 }
};

static FSCFONT_LABEL_VALUE PlanetInfoLabels[7] = {
    /* 0: Name of planet */
    { 0,               {  5,  24, 90, 12 }, "", 0, 0, { 224, 224, 224  }, 12 },
    { FSCFONT_VALCHAR, {  5,  38, 90, 12 }, "Size: ", 0, FSCFONT_TEXT_LEFTADJUST },
    { FSCFONT_VALCHAR, {  5,  52, 90, 12 }, "Growth: ", 0, FSCFONT_TEXT_LEFTADJUST },
    { FSCFONT_VALSTR,  {  5,  66, 90, 12 }, "Yields: ", 0, FSCFONT_TEXT_LEFTADJUST },
    { FSCFONT_VALSTR,  {  5,  80, 90, 12 }, 0, 0, FSCFONT_TEXT_LEFTADJUST },
    { FSCFONT_VALSTR,  {  5,  94, 90, 12 }, 0, 0, FSCFONT_TEXT_LEFTADJUST },
    { 0 }
};

static FSCFONT_LABEL_VALUE UnitInfoLabels[] = {

    /* 0: Name of unit */
    { FSCFONT_VALSTR,  {  67,  2, 0, 0 }, 0, 0, 0, { 170, 170, 0  }, 10 },
    /* 1: Moves       */
    { FSCFONT_VALCHAR, {  30, 55, 0, 0 }, 0 ,  0, FSCFONT_TEXT_TWOVALUES },
    /* 2: troops / level */
    { FSCFONT_VALCHAR, { 155, 55, 0, 0 }, 0, 0, FSCFONT_TEXT_LEFTADJUST },
    /* 3: Unit Ability name */
    { FSCFONT_VALSTR,  {  69, 17, 60, 0 }, "Class:", 0, FSCFONT_TEXT_LEFTADJUST, { 85,  85,  255 } },
    /* 4: Hitpoints         */
    { FSCFONT_VALCHAR, {  69, 30, 35, 0 }, "HP:", 0, FSCFONT_TEXT_TWOVALUES | FSCFONT_TEXT_LEFTADJUST },
    /* 5: Attack value      */
    { FSCFONT_VALCHAR, {  69, 43, 35, 0 }, "AT:", 0, FSCFONT_TEXT_LEFTADJUST },
    /* 6: Defense          */
    { FSCFONT_VALCHAR, {  69, 55, 35, 0 }, "DE:", 0, FSCFONT_TEXT_LEFTADJUST },
    /* 7: Unit order name  */
    { FSCFONT_VALSTR,  {  30, 68, 0,  0 }, 0, 0, 0, { 255, 85,  85  } },
    { FSCFONT_VALSTR,  {  30, 84, 0,  0 }, 0  },
    { { 0 } }

};

static SDLGL_CMDKEY PlanetCmdKeys[] = {

    /* ------- The map cursor (FSCCMD_MOVEUNIT)------ */
    { { SDLK_RETURN }, ORDER_ACTION_CANCEL },
    { { 0 } }

};

/*******************************************************************************
* CODE                                    				                       *
*******************************************************************************/

/*
 * Name:
 *     infobarMsgAdd
 * Description:
 *     Adds given message to the local 'Message' buffer.
 *     If it's the first message, it's translated to a human readable string
 * Input:
 *     msg *: Pointer on message to add to buffer
 */
static void infobarMsgAdd(MSG_INFO *msg)
{

    if (NumMsg < INFOBAR_MAX_MSG) {

        memcpy(&Messages[NumMsg], msg, sizeof(INFOBAR_MAX_MSG));
        NumMsg++;

    }

    if (NumMsg == 0) {

        languageMsgToString(msg, MsgLine, INFOBAR_MSGLINELEN);

    }

}

/*
 * Name:
 *     infobarMsgRemove
 * Description:
 *     Returns the actual message in given buffer and removes it from the
 *     internal buffer
 * Input:
 *     msg *:
 */
static void infobarMsgRemove(MSG_INFO *msg)
{

    MSG_INFO *msglist;


    msglist = &Messages[0];

    /* 1: Return the message in given buffer (may be an empty message) */
    memcpy(msg, msglist, sizeof(MSG_INFO));

    /* 2: Remove the message from the local list */
    while (msglist[1].type != 0) {

        memcpy(msglist, &msglist[1], sizeof(MSG_INFO));
        msglist++;

    }

    /* Sign new end of Message-List */
    msglist -> type = 0;

    /* 3: Display next message in 'human readable' form, if available */
    if (Messages[0].type > 0) {

        /* Display new message */
        languageMsgToString(msg, MsgLine, INFOBAR_MSGLINELEN);

    }
    else {

        MsgLine[0] = 0;     /* No message to display */

    }

}

/*
 * Name:
 *     infobarPlanetInput
 * Description:
 *     Adds the input fields for the given star own rectangle, having
 *     'actplanet' chosen.
 * Input:
 *     rect *:        Draw it into given rectangle
 *     ti *:          Pointer on tile info
 *     chosen_planet: Number of planet which is chosen, if any
 */
static void infobarPlanetInput(SDLGL_RECT *rect, FSCMAP_TILEINFO *ti, int chosen_planet)
{

    SDLGL_FIELD *target, TargetBuffer[100];
    FSCFONT_LABEL *plb;
    int topx, midy;         /* For system textures: star and planets    */
    int planetsize;         /* Size of planets texture rectangle        */
    int num_planet;
    int planet_no;
    PLANET *pplanet;        /* Pointer on planet        */


    /* First clear the buffer target buffer */
    target = &TargetBuffer[0];
    memset(target, 0, sizeof(SDLGL_FIELD) * 100);

    /* Reset labels for planets */
    plb = &PlanetLabels[0];
    plb -> posx = 0;

    topx = rect -> x + rect -> w - 124;             /* Changing top X            */
    midy = rect -> y + ((rect -> h - 94) / 2) + 47; /* Middle axis for planets   */

    /* Create the planet display in given rectangle */

    /* 1) Get name of planetary system --------------------- */
    if (ti -> starname[0] > 0) {

        plb -> valtype  = FSCFONT_VALSTR;
        plb -> posx     = rect -> x + rect -> w - 10;
        plb -> posy     = rect -> y + rect -> h - 20;
        plb -> plabel   = ti -> starname;   /* Name of planetary system  */;
        drawtoolGetColor(DRAWTOOL_COLOR_RACE, ti -> owner, plb -> color);
        plb -> format   = FSCFONT_TEXT_RIGHTADJUST;
        plb -> fontsize = 16;
        plb++;

    }

    /* 2) Set texture for star ------------------------------ */
    target -> sdlgl_type  = SDLGL_TYPE_TEXTURE;
    target -> code        = INFOBAR_CHOOSESTAR; /* Command to return    */
        /* ----- Texture itself -------- */
    target -> rect.x      = topx;
    target -> rect.y      = midy - 47;
    target -> rect.w      = 94;
    target -> rect.h      = 94;
    target -> tex         = FSCINIT_STARICONMAP;
    target -> subtex      = ti -> startype;      /* Number of a star texture */
    target++;

    /* 3) If planetary system is not known, don't display planets ---- */
    if (! ti -> first_planet) {

        target -> sdlgl_type = 0;     /* Set end of buffer */
        target -> tex        = 0;     /* Set end of buffer */
        sdlglInputAdd(INFOBAR_PLANETSYS, &TargetBuffer[0], 0, 0);

        plb -> valtype = 0;     /* End of label buffer */
        return;

    }

    /* ----------- Set the given fields into input fields ------- */
    topx -= 9;                     /* Right side of first planet */

    /* ---------- Set size of font ans set color white -------- */
    drawtoolGetColor(DRAWTOOL_COLOR_RACE, 0, &plb -> color[0]);
    plb -> fontsize  = 12;

    /* Create the planet input, if planets are known */
    planet_no  = ti -> first_planet;
    num_planet = ti -> num_planet;

    while (num_planet > 0) {

        pplanet = starGetPlanet(planet_no);

        planetsize = 64;        /* Assume: Gas core */
        if (pplanet -> size > 0) {

            planetsize = pplanet -> size * 3;

        }

        topx -= (planetsize + 8);   /* Icon-size + a small space in pixels */

        target -> sdlgl_type  = SDLGL_TYPE_TEXTURE;   /* Draw a planet texture */
        target -> code        = INFOBAR_CHOOSEPLANET; /* Command               */
        target -> sub_code    = planet_no;            /* Sub-Command           */
            /* ----- Texture itself -------- */
        target -> rect.x      = topx;
        target -> rect.y      = midy - (planetsize / 2);
        target -> rect.w      = planetsize;
        target -> rect.h      = planetsize;
        target -> tex         = FSCINIT_PLANETS;
        target -> subtex      = (char)(pplanet -> icon - 1);    /* Number of texture */
        target++;

        /* -- Add the char for the planets type --- */
        plb -> valtype = FSCFONT_VALSTR;
        plb -> posx    = topx + (planetsize / 2);
        plb -> posy    = midy + (planetsize / 2) + 6;
        plb -> plabel  = pplanet -> sign;
        plb -> valtype = 0;
        plb -> format  = FSCFONT_TEXT_HCENTER;
        /* Set color for string, based of planets terraform state   */
        if (pplanet -> size == 0) {     /* Special case 'Gas core'  */

            drawtoolGetColor(DRAWTOOL_COLOR_RACE, 1, plb -> color);

        }
        else {

            drawtoolGetStateColor(pplanet ->  terraform[0],
                                  pplanet ->  terraform[1],
                                  plb -> color);

        }
        plb++;

        /* ------ Set cursor, if actual planet is chosen ----- */
        if (planet_no == chosen_planet) {

            memcpy(target, &target[-1], sizeof(SDLGL_FIELD));   /* Get rectangle and type */
            target -> sdlgl_type = SDLGL_TYPE_TEXTURE;
            target -> tex        = FSCINIT_CURSOR;
            target -> subtex     = INFOBAR_CURSOR_GREEN;
            target++;

        }

        /* --- Display, if there iss a colony or a outpost on this planet --- */
        if (pplanet -> settleno > 0) {

            /* Details are given in display of planets info text */
            memcpy(target, &target[-1], sizeof(SDLGL_FIELD));   /* Get rectangle and type */

            target -> sdlgl_type  = SDLGL_TYPE_TEXTURE;
            target -> tex         = FSCINIT_CURSOR;
            target -> subtex      = 4;                      /* Owner-rectangle */
            drawtoolGetColor(DRAWTOOL_COLOR_RACE, pplanet -> owner, &target -> color[0]);
            target++;

        }

        planet_no++;         /* Next planet */
        num_planet--;

    }

    if (ti -> owner > 0) {

        target -> sdlgl_type  = SDLGL_TYPE_TEXTURE;
        target -> rect.x      = rect -> x + rect -> w - 60;
        target -> rect.y      = rect -> y + rect -> h - 60;
        target -> rect.w      = 50;
        target -> rect.h      = 50;
        target -> tex         = FSCINIT_IMPERIUMSIGN;
        target -> subtex      = (char)(ti -> owner - 1);    /* Number of texture */
        target++;

    }

    target -> sdlgl_type = 0;     /* Set end of buffer        */
    target -> tex        = 0;     /* Set end of buffer        */
    plb -> valtype       = 0;     /* Set end of label buffer  */

    /* ----------- Set the given fields into input fields --------- */
    /* TODO: Set position of display of planet-sys over function 'sdlglInputAdd' */
    sdlglInputAdd(INFOBAR_PLANETSYS, &TargetBuffer[0], 0, 0);

}

/*
 * Name:
 *     infobarSetUnitLabelValues
 * Description:
 *     Binds the values of given unit to the 'unit-info' labels.
 *     If unit is 0, then the labels are set for no display.
 * Input:
 *     unitno: Number of unit to set the labesl for
 */
static void infobarSetUnitLabelValues(int unitno)
{

    static char order_name[128];

    UNIT *punit;


    if (unitno > 0) {

        /* Get the unit info to bind to values */
        punit = unitGet(unitno);

        /* FIXME: Bind all values of  the actual info to the labels */
        UnitInfoLabels[0].valtype = FSCFONT_VALSTR;
        UnitInfoLabels[0].plabel  = punit -> name;
        UnitInfoLabels[1].pvalue  = &punit -> moves[0];
        /* Additional info */
        if (punit -> cargo_type > 0) {   /* There's cargo aboard */

            UnitInfoLabels[2].plabel  = "Troops:"; /* FIXME: Set different cargos */
            UnitInfoLabels[2].rect.w = 80;
            UnitInfoLabels[2].pvalue = &punit -> cargo_load;

        }
        else {

            UnitInfoLabels[2].plabel  = "Level";
            UnitInfoLabels[2].rect.w = 60;
            UnitInfoLabels[2].pvalue = &punit -> level;

        }

        UnitInfoLabels[3].pvalue = languageLabel(LANGUAGE_LABEL_ABILITY, punit -> ability);
        UnitInfoLabels[4].pvalue = &punit -> hp[0];
        UnitInfoLabels[5].pvalue = &punit -> attack;
        UnitInfoLabels[6].pvalue = &punit -> defense;
        UnitInfoLabels[7].plabel = infobarUnitOrderText(punit -> order.what,
                                                        punit -> order.target,
                                                        punit -> order.build,
                                                        order_name);

    }
    else {

        UnitInfoLabels[0].valtype = 0;          /* No labels at all */

    }

}

/*
 * Name:
 *     infobarHandleUnitCmd
 * Description:
 *     Translates the input for the star system screen
 * Input:
 *     event *: Struct holding the input event info
 */
static int infobarHandleUnitCmd(SDLGL_EVENT *event)
{

    if (event -> code > 0) {

        switch(event -> code) {
        
            case ORDER_ACTION_CANCEL:
                event -> code = 0;                /* Clear event code   */
                msgPrepare(0, 0, &PlanetMenuMsg); /* Clear message      */
                return SDLGL_INPUT_REMOVE;
            
            default:
                PlanetMenuMsg.value1 = event -> code;       /* Order    */
                PlanetMenuMsg.value3 = event -> sub_code;   /* Build    */
                fsccmdMsgSend(&PlanetMenuMsg);
                return SDLGL_INPUT_REMOVE;  /* We want to close the popup menu */
            

        } /* switch(event -> code) */

    }

    return SDLGL_INPUT_OK;
    
}

/*
 * Name:
 *     infobarDrawDebugInfo
 * Description:
 *     Draws the 'Message/Debug-Info'
 * Input:
 *     None
 */
static void infobarDrawDebugInfo(void)
{

    static SDLGL_RECT msg_rect = { 776, 729, 241, 32 };
    static FSCFONT_LABEL debug_label[2] = {
        { FSCFONT_VALSTR, 781, 732, DebugInfo, 0, { 255, 255, 255 }, 10 },
        { 0 }
    };

    if (DebugInfo[0] != 0) {

        drawtoolSimpleButton(&msg_rect, 0, 2);
        fscfontPrintLabels(0, 0, debug_label);

    }

}

/* ========================================================================== */
/* =========================== PUBLIC FUNCTION(S) =========================== */
/* ========================================================================== */

/*
 * Name:
 *     infobarTileInfo
 * Description:
 *     Set input code about a tile at given position, if any is visible
 *     for the caller.
 *     - Star
 *     - Planets
 * Input:
 *     pos:              At this position on map
 *     nation_no:        Draw for this nation
 *     chosen_planet_no: Planet is chosen, if any
 */
void infobarTileInfo(int pos, char nation_no, int chosen_planet_no)
{

    static SDLGL_RECT  rect = { 244, 640, 532, 128 };
    static FSCMAP_TILEINFO ti;


    languageSectorName(pos, InputState.sectorname);    /* Set sector name for this position */
    
    /* First look up, if there is any info to display */
    fscmapGetTileInfo(pos, nation_no, &ti);

    if (ti.startype > 0) {

        /* We know the type of the star, display it */
        if (! ti.first_planet) {

            chosen_planet_no = -1;

        }

        infobarPlanetInput(&rect, &ti, chosen_planet_no);

    }
    else {

        /* Remove the display for the star system, if set */
        sdlglInputRemove(INFOBAR_PLANETSYS);
        PlanetLabels[0].valtype     = 0;    /* Remove its labels            */
        PlanetInfoLabels[0].valtype = 0;    /* Remove possible info-labels  */
        return;

    }

}

/*
 * Name:
 *     infobarUnitOrderText
 * Description:
 *     Sets the order text for given unit.
 * Input:
 *     order_no:  This order
 *     target_no: This target
 *     build_no:  This is to build, if 'build'-command
 *     buffer *:  Where to print the result
 * Output:
 *     Pointer on generated text
 */
char *infobarUnitOrderText(char order_no, int target_no, char build_no, char *buffer)
{

    char *porder;


    porder = languageLabel(LANGUAGE_LABEL_UNITORDER, order_no);

    if (order_no == ORDER_BUILD) {

        if (build_no > 0) {
            /* Target is planet */
            sprintf(buffer, "%s %s: %s", porder, improveName(build_no),
                    starGetPlanetName(target_no));

        }
        else {

            sprintf(buffer, "%s %s", porder, "Outpost");

        }

    }
    else if (order_no == ORDER_TERRAFORM) {

        sprintf(buffer, "%s %s", porder, starGetPlanetName(target_no));

    }
    else {

        sprintf(buffer, "%s", porder);

    }

    return buffer;
    
}

/*
 * Name:
 *     infobarDebugInfo
 * Description:
 *     Copies given string into 'DebugInfo'. If string is NULL, then the
 *     'DebugInfo' is cleared.
 * Input:
 *     info *: Pointer on string to fill into 'DebugInfo'.
 */
void infobarDebugInfo(char *info)
{

    if (info) {

        strncpy(DebugInfo, info, INFOBAR_DEBUGINFOSIZE);
        DebugInfo[INFOBAR_DEBUGINFOSIZE] = 0;

    }
    else {

        DebugInfo[0] = 0;       /* Clear the string */

    }

}

/*
 * Name:
 *     infobarHandleMsg
 * Description:
 *     Translates given message into human readable form.
 *     If 'msg_num' = 0, the message buffer and 'MsgLine' are cleared   
 *     TODO: If 'msg_num' < 0, then clear the message buffer and load
 *     the next message into given buffer
 * Input:
 *     msg_num: Number of message to translate into buffer
 *     msg *: Pointer on message
 */
void infobarHandleMsg(int msg_num, MSG_INFO *msg)
{

    char buffer[1024];


    if (msg_num > 0) {

        /* TODO: Use language function to generate language based strings from message */
        switch(msg_num) {

            /* ------- Messages ------ */
            case MSG_UNIT_NOMOVESLEFT:
                sprintf(buffer, "Unit has no moves left: %d.", msg -> to);
                break;

            case MSG_UNIT_STUCKRANGE:
                sprintf(buffer, "You tried to move off range.");
                break;

            case MSG_UNIT_STUCKMAP:
                sprintf(buffer, "You tried to move off map.");
                break;

            case MSG_UNIT_WRONGOWNER:
                sprintf(buffer, "Unit has the wrong owner: %d.", msg -> to);
                break;

            case MSG_RESEARCHDONE:
                if (msg -> tech_no > 0) {
                    /* Only show if valid tech */
                    sprintf(buffer, "Actual research %d is completed.", msg -> tech_no);

                }
                else {
                    sprintf(buffer, "We dont know what we should research yet.");
                }
                break;
                
            case MSG_RESEARCHDONEFIRST:
                sprintf(buffer, "We dont know what we should research yet.");
                break;

            default:
                infobarMsgAdd(msg);
                buffer[0] = 0;
                break;

        }

        if (buffer[0] > 0) {

            strncpy(MsgLine, buffer, INFOBAR_MSGLINELEN);
            MsgLine[INFOBAR_MSGLINELEN] = 0;

        }

    }
    else {

        if (msg_num == 0) {

            MsgLine[0] = 0;             /* Clear message-line   */
            NumMsg = 0;
            Messages[0].type = 0;       /* Clear all messages   */

        }
        else if (msg_num == -1) {

            /* Removes the message from message buffer */
            infobarMsgRemove(msg);
    
        }

    }

}

/*
 * Name:
 *     infobarSetPlanetInfo
 * Description:
 *     Fills in the the info-strings about the given planet.
 * Input:
 *     planet_no: Fill it in for this planet
 *
 */
void infobarSetPlanetInfo(int planet_no)
{

    static char yield_str[20];
    PLANET *pplanet;
    OUTPOST *poutpost;
    int label_no;


    if (planet_no > 0) {

        pplanet = starGetPlanet(planet_no);

        PlanetInfoLabels[0].valtype = FSCFONT_VALSTR;       /* Activate it */
        PlanetInfoLabels[0].plabel  = starGetPlanetName(planet_no);
        PlanetInfoLabels[1].pvalue  = &pplanet -> size;
        PlanetInfoLabels[2].pvalue  = &pplanet -> growth;
        PlanetInfoLabels[3].pvalue  = yield_str;
        PlanetInfoLabels[4].valtype = 0;                    /* No additional info */

        sprintf(yield_str, "%d/%d/%d",
                (int)pplanet -> yield[YIELD_FOOD],
                (int)pplanet -> yield[YIELD_RESOURCES],
                (int)pplanet -> yield[YIELD_ECONOMY]);

        /* Add labels for terraform and outpsot / colony, as appropriate */
        label_no = 4;
        if (pplanet -> terraform[1] > 0) {

            PlanetInfoLabels[label_no].valtype  = FSCFONT_VALCHAR;
            PlanetInfoLabels[label_no].plabel   = TerraformText;
            PlanetInfoLabels[label_no].pvalue   = &pplanet -> terraform[0];
            PlanetInfoLabels[label_no].format   = FSCFONT_TEXT_TWOVALUES | FSCFONT_TEXT_LEFTADJUST;
            PlanetInfoLabels[label_no].color[0] = 0;    /* No color change */
            PlanetInfoLabels[label_no].rect.w   = 130;
            label_no++;

        }

        /* Now add info about outpost / colony */
        if (pplanet -> settleno > 0) {

            if (pplanet -> settletype == STARINFO_SETTLETYPE_COLONY) {

                PlanetInfoLabels[label_no].valtype = FSCFONT_VALSTR;
                PlanetInfoLabels[label_no].plabel  = ColonyText;
                PlanetInfoLabels[label_no].pvalue  = nationGetName(pplanet -> owner);
                drawtoolGetColor(DRAWTOOL_COLOR_RACE,
                                 pplanet -> owner,
                                 PlanetInfoLabels[label_no].color);
                PlanetInfoLabels[label_no].rect.w   = 90;

            }
            else if (pplanet -> settletype == STARINFO_SETTLETYPE_OUTPOST) {

                poutpost = outpostGet(pplanet -> settleno);

                PlanetInfoLabels[label_no].valtype = FSCFONT_VALSTR;
                PlanetInfoLabels[label_no].plabel  = improveName(poutpost -> building);
                drawtoolGetColor(DRAWTOOL_COLOR_RACE,
                                 pplanet -> owner,
                                 PlanetInfoLabels[label_no].color);

            }

        }

        PlanetInfoLabels[label_no + 1].valtype = 0;

    }

}

/*
 * Name:
 *     infobarSetPlanetMenu
 * Description:
 *     Adds a pop-up-menu with possible commands for given unit
 *     on given planet, depending on the units type.
 * Input:
 *     x, y:      Where to draw on screen
 *     planet_no: Fill it in for this planet
 *     unit_no:   Commands for this unit
 *     planet_no: This planet
 *     nation_no: This nation sends the message
 */
void infobarSetPlanetMenu(int x, int y, int unit_no, int planet_no, char nation_no)
{

    SDLGL_FIELD fields[22], *pfields;
    char projlist[22], *plist;
    char order;
    int numproj;


    /* Get the possible commands */
    order = unitruleGetPlanetOrder(unit_no, planet_no);

    if (order > 0) {

        /* Prepare massage for possbile send command in menu */
        msgPrepare(nation_no, 0, &PlanetMenuMsg);

        PlanetMenuMsg.type    = MSG_TYPE_UNIT;      /* Correct message type */
        PlanetMenuMsg.unit_no = unit_no;            /* this unit is working */
        PlanetMenuMsg.value2  = planet_no;          /* Target is known      */

        /* ------ Create input menu ------------- */
        pfields = &fields[0];
        memset(pfields, 0, 20 * sizeof(SDLGL_FIELD));   /* Clear buffer */

        /* -------- Fill in the base data ------------ */
        pfields -> sdlgl_type   = INFOBAR_ORDERLIST;
        pfields -> code         = 0;
        pfields -> rect.x       = x - INFOBAR_UNITORDER_W;
        pfields -> rect.y       = y;
        pfields -> rect.w       = INFOBAR_UNITORDER_W;
        pfields -> rect.h       = 16;
        pfields++;

        /* Copy to first menu point */
        memcpy(pfields, &pfields[-1], sizeof(SDLGL_FIELD));

        if (order == ORDER_BUILD) {

            /* Add the number of outposts, that can be built and name it */
            numproj = improveGetListByType(projlist, PROJ_OUTPOST);

            fields[0].rect.h += (numproj * 16);
            fields[0].rect.y -= fields[0].rect.h;

            pfields -> rect.y  = fields[0].rect.y;      /* Top-start            */
            pfields -> rect.h  = 12;                    /* Height of rectangle  */

            plist = projlist;
            while (*plist > 0) {

                pfields -> code     = ORDER_BUILD;
                pfields -> sub_code = *plist;
                pfields -> pdata    = improveName(*plist);

                plist++;
                pfields++;

                memcpy(pfields, &pfields[-1], sizeof(SDLGL_FIELD));
                pfields -> rect.y += 16;

            }

        }
        else {

            fields[0].rect.h += 16;
            fields[0].rect.y -= fields[0].rect.h;
            pfields -> rect.y  = fields[0].rect.y;      /* Top start            */
            pfields -> rect.h  = 12;                    /* Height of rectangle  */
            /* ------- Field with single command ------------- */
            pfields -> code    = order;
            pfields -> pdata   = languageLabel(LANGUAGE_LABEL_UNITORDER, order);
            pfields++;

            memcpy(pfields, &pfields[-1], sizeof(SDLGL_FIELD));
            pfields -> rect.y += 16;

        }

        pfields -> code  = ORDER_ACTION_CANCEL;
        pfields -> pdata = languageLabel(LANGUAGE_LABEL_UNITORDER, ORDER_ACTION_CANCEL);
        pfields++;

        pfields -> sdlgl_type = 0;

        /* ----- Now add menu as popup ------- */
        sdlglInputPopup(infobarHandleUnitCmd, &fields[0], PlanetCmdKeys, 0, 0);

    }

}

/*
 * Name:
 *     infobarDrawUnitInfo
 * Description:
 *     Draws:
 *          - Unit info background
 *          - Unit icon (TO DO?)
 *          - Unit info labels
 *     At given position for given unit   
 * Input:
 *     x,
 *     y:         Add this value to the basis
 *     unit_no:   Draw values for this unit
 *     draw_rect: Draw the background rectangle yes/no
 */
void infobarDrawUnitInfo(int x, int y, int unit_no, int draw_rect)
{

    static SDLGL_RECT UnitInfoRect = { 0, 0, INFOBAR_UNITORDER_W, INFOBAR_UNITINFO_H };


    if (unit_no > 0) {     /* Only if there's info to print */

        
        if (draw_rect > 0) {
        
            /* ------- Draw the background for the info --------- */
            UnitInfoRect.x = x;
            UnitInfoRect.y = y;
            drawtoolRectangle(&UnitInfoRect, DRAWTOOL_FILLHI, 64);
            
        }

        /* FIXME: Draw unit icon ------------------ */
        /* -------- Set the labels ---------------- */
        infobarSetUnitLabelValues(unit_no);
        /* -------- Draw the labels --------------- */
        fscfontPrintLabelValues(x, y, UnitInfoLabels);

    } /* if (unitno > 0) */

}

/*
 * Name:
 *     infobarDrawGlobalInfo
 * Description:
 *     Draws the 'Message/Debug-Info'
 * Input:
 *     x, y: Draw at this position
 */
void infobarDrawGlobalInfo(int x, int y)
{

    fscfontPrintLabels(x, y, GlobalInfoLabels);

}

/*
 * Name:
 *     infobarDrawLabels
 * Description:
 *     Draws dynamic Labels depending on
 *     Draws the 'Message/Debug-Info'
 * Input:
 *     None
 */
void infobarDrawLabels(void)
{

    infobarDrawDebugInfo();

    if (PlanetLabels[0].valtype > 0) {

         fscfontPrintLabels(0, 0, PlanetLabels);

    }

    if (StarInfoLabels[0].valtype > 0) {

         fscfontPrintLabels(0, 0, StarInfoLabels);

    }
    
    if (PlanetInfoLabels[0].valtype > 0) {

         fscfontPrintLabelValues(0, 0, PlanetInfoLabels);

    }
    
    if (MsgLine[0] > 0) {

        fscfontPrintLabels(0, 0, MsgLineLabel);
        
    }

}

/*
 * Name:
 *     infobarUpdateGlobalInfo
 * Description:
 *     Does an update on the
 * Input:
 *     mappos:    Map-Position to generate sector-name for
 *     nation_no: > 0: For this nation (money and
 */
void infobarUpdateGlobalInfo(int mappos, char nation_no)
{

    NATION *pnation;


    /* Only do an update sector-name in any case */
    languageSectorName(mappos, InputState.sectorname);

    if (nation_no > 0) {

        memset(&InputState, 0, sizeof(INPUT_STATE));    /* Clear the buffer */

        pnation = nationGet(nation_no);

        /* Generate the string for the game date from turn */
        languageSpecialString(LANGUAGE_SPEC_DATE, 0, InputState.datestr);
        /* Get the string for the money */
        sprintf(InputState.moneystr, "$ %d", pnation -> store[YIELD_NETINCOME]);

        languageSpecialString(LANGUAGE_SPEC_POPULATION,
                              pnation -> store[YIELD_POPULATION],
                              InputState.popvalstr);

    }

}
