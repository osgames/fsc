/*******************************************************************************
*   ADVISORS.C                                                                 *
*	    - Command functions, setting and drawing input for advisor screens     *
*                                                                              *
*	FREE SPACE COLONISATION               	                                   *
*       Copyright (C) 2003  Paul Mueller <pmtech@swissonline.ch>               *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*******************************************************************************/

/*******************************************************************************
* INCLUDES								                                       *
*******************************************************************************/

#include <string.h>     /* strcpy() */


#include "../code/game.h"
#include "../code/game2str.h"   /* Descriptions for labels */

#include "sdlgl.h"
#include "sdlgldef.h"
#include "drawmap.h"
#include "drawmap.h"
#include "drawtool.h"           /* Tools for drawing the different screens */

/*
#include "fscfe/advmilit.h"
#include "fscfe/colscr.h"
#include "fscfe/domescr.h"
#include "fscfe/fscfont.h"
#include "fscfe/infobar.h"
#include "fscfe/mapdraw.h"
#include "fscfe/drawtool.h"
#include "fscfe/techscr.h"
#include "fscfe/language.h"
*/


/* -- Own header -- */
#include "advisors.h"

/*******************************************************************************
* DEFINES								                                       *
*******************************************************************************/

#define ADVISOR_MAX_INPUTFIELD   255

/* ------- Drawing fields ---------- */
#define ADVISOR_SIDEBAR     ((char)SDLGL_TYPE_MAX + 10)
#define ADVISOR_MINIMAP     ((char)SDLGL_TYPE_MAX + 11)
#define ADVISOR_BUTTON3     ((char)SDLGL_TYPE_MAX + 12)
#define ADVISOR_BASEBLOCK   ((char)100)

/*******************************************************************************
* ENUMS 								                                       *
*******************************************************************************/

typedef enum
{
    ADVCMD_NONE  = 0,
    ADVCMD_DONE  = 1,
    ADVCMD_CLOSE = 2
}
ADVISOR_CMD;

/*******************************************************************************
* DATA  								                                       *
*******************************************************************************/

/* Sidebar holds all base info about the game */
static SDLGL_INPUT_T AdvisorFld_Template[] =
{
    { ADVISOR_MINIMAP, { 768 + 8, 0 + 45, 241, 241 } },
    /* ------ Button command and text drawn on it ------- */
    { ADVISOR_BUTTON3, { 664, 734, 94, 25 } },
    { 0 }
};

static SDLGL_INPUTKEY_T Advisor_CmdKey[] =
{
    { { SDLK_ESCAPE }, ADVCMD_CLOSE, ADVCMD_CLOSE },
    { { SDLK_RETURN }, ADVCMD_CLOSE, ADVCMD_DONE },
    { { 0 } }
};

/* Use Labels for drawing of screen */
static char Screen_Title[40];

static SDLGL_LABEL_T AdvisorLabel_Template[] =
{
    { SDLGL_VAL_STRING, 364, 20,  Screen_Title, 0, 20, 'L' },
    { SDLGL_VAL_STRING, 664 + 37, 734 + 12, "Done", 0, 12, 'C' },
    { 0 }
};

static SDLGL_INPUT_T Advisor_Fields[ADVISOR_MAX_INPUTFIELD + 1] =
{
    { 0 }
};

/*******************************************************************************
* CODE  								                                       *
*******************************************************************************/

/*
 * Name:
 *     advisor_ownerdraw_func
 * Description:
 *     Draws all the fields, which are not drawn by the available standard
 *     texture and font functions
 * Input:
 *      pfields *: Pointer on array of fields to draw
 */
static void advisor_ownerdraw_func(SDLGL_INPUT_T *pfields)
{
     while(pfields->sdlgl_type != 0)
     {
         switch(pfields->sdlgl_type)
         {
            case ADVISOR_SIDEBAR:
                /* infobarDrawGlobalInfo(fields->rect.x, fields->rect.y); */
                break;

            case ADVISOR_MINIMAP:
                /* drawmap_minimap(); */
                break;

            case ADVISOR_BUTTON3:
                /* drawtoolButton(&fields->rect, DRAWTOOL_BUTTON7, fields->fstate); */
                break;
        }

        pfields++;
    }
}

/*
 * Name:
 *     advisor_display_function
 * Description:
 *     Display function for the screens set by the "advisor" functions.
 * Input:
 *      pfields *: Pointer on array of fields to draw
 *      pevent *:  Event holding different info used for drawing
 */
static void advisor_display_function(SDLGL_INPUT_T *pfields, SDLGL_EVENT_T *pevent)
{
    /* Simply clear the screen  */
    /* glClear(GL_COLOR_BUFFER_BIT);            */

    if(pfields)
    {
        drawtool_drawfields(pfields, advisor_ownerdraw_func);

        /* FIXME: 1)    Draw textures */
        /* sdlgltexDisplayIconList(SDLGL_INPUT_T *fields, int stoptype); */

        /* 2) Draw owner drawn fields */
        /* advisor_ownerdraw_func(pfields); */

    }

    /* 3) Draw strings */
    drawtool_labels(AdvisorLabel_Template);
}

/*
 * Name:
 *     advisor_input_function
 * Description:
 *     Input translation function for menu
 * Input:
 *      event *:
 */
static int advisor_input_function(SDLGL_EVENT_T *pevent)
{
    if(pevent->code > 0)
    {
        switch(pevent->code)
        {
            case ADVCMD_CLOSE:
                /* To be fixed because this is the emergency exit ? 11-21-2004 / pam */
                pevent->code     = 0;
                pevent->sub_code = 0;
                return SDLGL_INPUT_REMOVE;
        }
    }

    return SDLGL_INPUT_OK;
}

/* ========================================================================== */
/* ========================== PUBLIC FUNCTIONS ============================== */
/* ========================================================================== */

/*
 * Name:
 *     advisor_main
 * Description:
 *     Opens the advisor screen asked for
 * Input:
 *      which:  Which advisor screen to open
 */
void advisor_main(int which)
{
    /* --------- Reset fille target buffer ------ */
    Advisor_Fields[0].sdlgl_type = 0;

    /* --------- Fill in the template for the advisor -------- */
    strcpy(Screen_Title, game2str_label(GAME2STR_LABEL_ADVISOR, which));

    switch(which)
    {
        case 0:
            /* Invalid advisor */
            return;
        case ADVISOR_DOMESTIC:  /* Planet list                  */
            /* domescrManage(mng); */
            return;

        case ADVISOR_TRADE:     /* Trade for each planet ?!     */
            break;

        case ADVISOR_MILITARY:  /*  Unit-List + Minimap         */
            /* advmilitManage(mng); */
            return;

        case ADVISOR_FOREIGN:   /* Diplomacy screen             */
        case ADVISOR_CULTURE:   /* Cultural influence screen    */
            break;

        case ADVISOR_TECH:
            /* @TODO: Display list of technologies and what is actal researched */
            return;

        case ADVISOR_WONDERLIST:
        case ADVISOR_HISTOGRAPHY:
            break;

        case ADVISOR_PLANETMANAGE:
            /* @TODO: Display a list op planets with colonies and outposts */
            /* colscrManage(mng); */
            return;

        case ADVISOR_DEMOGRAPHY:
        case ADVISOR_STATS:
        default:
            break;

    }

    /* Second set the the fields, depending on advisor screen   */
    /* FIXME:                                                   */
    /* Get the input fields and the command keys from advisors  */
    sdlgl_input_new(advisor_display_function,
                    advisor_input_function,
                    Advisor_Fields,
                    Advisor_CmdKey,
                    ADVISOR_MAX_INPUTFIELD,
                    0);
}
