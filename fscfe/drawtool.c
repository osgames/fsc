/*******************************************************************************
*  DRAWTOOL.C                                                                  *
*      - Output of text in an OpenGL-Window.                                   *
*                                                                              *
*   FREE SPACE COLONISATION                                                    *
*      Copyright (C) 2002-2017  Paul M�ller <muellerp61@bluewin.ch>            *
*                                                                              *
*  This program is free software; you can redistribute it and/or modify        *
*  it under the terms of the GNU General Public License as published by        *
*  the Free Software Foundation; either version 2 of the License, or           *
*  (at your option) any later version.                                         *
*                                                                              *
*  This program is distributed in the hope that it will be useful,             *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of              *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
*  GNU Library General Public License for more details.                        *
*                                                                              *
*  You should have received a copy of the GNU General Public License           *
*  along with this program; if not, write to the Free Software                 *
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  *
*******************************************************************************/

/*******************************************************************************
* INCLUDES							                                           *
*******************************************************************************/

/* The bitmaps are integrated. Makes it easier to print out some text   */
/*  without thinking of it to have loaded some stuff. E. g. for debug   */

#include <stdio.h>      /* sprintf */
#include <stdarg.h>
#include <string.h>
#include <math.h>       /* sin, cos */


#include "sdlgl.h"
#include "../code/fsctool.h"


/* ---- Own headers --- */
#include "drawtool.h"

/*******************************************************************************
* DEFINES								                                       *
*******************************************************************************/

#define DRAWTOOL_MAXFONT 5
#define SDLGDRW_TABSIZE 8

#define SDLGDRW_MAXTEXCHAR 512	/* Four fonts with 127 chars */

/* Internal constants */
#define SDLGDRW_FIELDBORDER  4	/* Width of border in pixels 		        */
#define SDLGDRW_SPECIALDIST 12	/* Distance between start of special field  */
            				/* and its text 			                */

/*******************************************************************************
* TYPEDEFS								                                       *
*******************************************************************************/

typedef struct
{
    GLubyte *font;
    int	 fontw,		/* fontb */
    	 fonth;		/* Width, heigth of font */
    int  cwidth;	/* Width of Char-Bitmap */
    char *xadd;     /* List for xadd for every char */

    /* Extensions */
    GLuint textureID;
}
FONT_T;


/******************************************************************************
* DATA  		      						      *
******************************************************************************/

static char Button_Chars[] =
{
    15, 0, 14, 0, 13, 0,        /* Push button  */
    12, 0, 11, 0, 10, 0,	    /* Radio button */
     9, 0,  8, 0,  7, 0, 6, 0   /* Arrows	   */
};

static GLubyte Draw_Colors[][4] =
{
    {   0,   0,   0,   0 },  /* 0. BLACK */
    {   0,   0, 170,   0 },  /* 1. BLUE */
    {   0, 170,   0,   0 },  /* 2. GREEN */
    {   0, 170, 170,   0 },  /* 3. CYAN */
    { 170,   0,   0,   0 },  /* 4. RED */
    { 170,   0, 170,   0 },  /* 5. MAGENTA */
    { 170, 170,   0,   0 },  /* 6. BROWN */
    { 170, 170, 170,   0 },  /* 7. LIGHTGREY */
    {  85,  85,  85,   0 },  /* 8. DARKGRAY */
    {  85,  85, 255,   0 },  /* 9. LIGHTBLUE */
    {  85, 255,  85,   0 },  /* 10. LIGHTGREEN */
    {  85, 255, 255,   0 },  /* 11. LIGHTCYAN */
    { 255,  85,  85,   0 },  /* 12. LIGHTRED */
    { 255,  85, 255,   0 },  /* 13. LIGHTMAGENTA */
    { 255, 255,  85,   0 },  /* 14. YELLOW */
    { 255, 255, 255,   0 },  /* 15. WHITE */
    /* The colors for the different players DRAWTOOL_COLOR_PLAYER */
    { 255, 255, 255,   0 },  /* 16: Default white */
    {   1,   0, 255,   0 },  /* Player1  */
    { 224,  56,  40,   0 },  /* Player2  */
    {  80, 192,  96,   0 },  /* Player3  */
    { 112, 216, 248,   0 },  /* Player4  */
    { 114, 112, 176,   0 },  /* Player5  */
    { 240, 240,  56,   0 },  /* Player6  */
    {  85, 255, 255,   0 },  /* Player7  */
    { 170, 170, 170,   0 },  /* Player8  */
    /* --------- Colors for small civilizations -----------------   */
    { 127, 127, 127,   0 },  /* 25: Dummy    */
    /* ---- Colors for range borders: DRAWTOOL_COLOR_RANGE ---- */
    {   1, 170,   0 },  /* 26: DRAWTOOL_GREEN       */
    { 255, 255,  85 },  /*     DRAWTOOL_YELLOW      */
    { 255,  0,    9 },  /*     DRAWTOOL_RED         */
    /* ------- More colors ------------------------ */
    {  30,  60,  97 },  /*29: DRAWTOOL_BORDERCOL    */
    {  16,  48,  73 },  /* DRAWTOOL_SLICOLOR        */
    {   9,  32,  50 },  /* DRAWTOOL_RADIOCOLOR1     */
    {  16,  48,  73 },   /* DRAWTOOL_RADIOCOLOR2    */
     /* ---------- Additional drawing colors: 25 ----------------- */
    { 170, 170, 0  ,   0 },  /* GAME_YELLOW  */
    {  85, 123, 142,   0 },  /* FSCFONT_TITLE_COLOR  */
    /* \40\x00\xFF */
    { 205, 154,  89,   0 },  /* FSCFONT_GAME_ORANGE  */
    /* colscr = 240, 120, 32 */
    /* colscr = 175, 224, 112 */
    { 175, 224, 112,   0 },  /* FSCFONT_GAME_YELLOW2  */
    {  64,   0, 255,   0 },  /* FSCFONT_GAME_BLUE     */
    {   0,  36,  72,   0 },  /* FSCFONT_GAME_DARKBLUE */
    {  53,  74,  77,   0 },  /* 39: MAPGRID-COLOR     */
    {   0,  84,   0,   0 },  /* 40: SECBORDER-COLOR   */
    /* Colors for Stars 41 .. 47 (41: Invalid) */
    { 255, 255, 255,  0 },  /* 41: Default white */
    { 175,  00,  00,  0 },  /* RED         */
    {  84, 211,  84,  0 },  /* Blue         */
    { 255, 255,  85,  0 },  /* 14. YELLOW   */
    { 255, 191,   0,  0 },  /* Orange       */
    { 234, 234, 234,  0 },  /* White        */
    {  46, 254,  46,  0 },  /* Green        */
    { 0 }
};

static unsigned char Poly_Colors[][4] =
{
    { 107, 149, 165 },
    {  14,  39,  44 },
    {  16,  48,  73 },
    { 255, 255, 255 },
    {  80,  48,  16 },  /* DRAWTOOL_BUTTONGOLDLO    */
    { 255, 192,  15 },  /* DRAWTOOL_BUTTONGOLDLOHI  */
    {   0,   0,   0 },  /* DRAWTOOL_BUTTONBLACK     */
    { 159,  14,   6 },  /* DRAWTOOL_FILLHILITE      */
    {   1,   0, 170 },  /* DRAWTOOL_DEEPBLUE        */
    /* ------- Replaces 'drawcolor' --------------- */
    { 195, 196, 188 },  /* DRAWTOOL_COLOR1          */
    {   6,  34,  58 },  /* DRAWTOOL_COLOR2          */
    {  22,  40,  54 },  /* DRAWTOOL_COLOR3          */

};

static GLubyte _Font6[] =
{
    /*  C = 0 chars 0..7, holding �, �, � */
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x88, 0xF8, 0x88, 0x70, 0x50,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x88, 0xF8, 0x88, 0x70, 0x50,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,

    /* 8..15 -------------------	*/
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,

    /* 16..23 -------------------	*/
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x70, 0x88, 0x88, 0x70, 0x50,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,

    /* 24..31 -------------------	*/
    /* Arrows		*/
    0x00, 0x20, 0x20, 0xF8, 0x70, 0x20,
    0x00, 0x20, 0x70, 0xF8, 0x20, 0x20,
    0x00, 0x20, 0x30, 0xF8, 0x30, 0x20,
    0x00, 0x20, 0x60, 0xF8, 0x60, 0x20,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,

    /* 32..39 -------------------	*/
    /* Start with SPACE at 32		*/
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x20, 0x00, 0x20, 0x20, 0x20,
    0x00, 0x00, 0x00, 0x00, 0x90, 0x90,
    0x00, 0x50, 0xF8, 0x50, 0xF8, 0x50,
    0x00, 0x60, 0x30, 0x70, 0x60, 0x30,
    0x00, 0x88, 0x40, 0x20, 0x10, 0x88,
    0x00, 0x74, 0xD8, 0x60, 0x50, 0x70,
    0x18, 0x20, 0x00, 0x00, 0x00, 0x00,

    /* 40..47 -------------------   */
    0x00, 0x20, 0x40, 0x40, 0x40, 0x20,
    0x00, 0x40, 0x20, 0x20, 0x20, 0x40,
    0x00, 0xA8, 0x70, 0xF8, 0x70, 0xA8,
    0x00, 0x20, 0x20, 0xF8, 0x20, 0x20,
    0x00, 0x20, 0x10, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0xF8, 0x00, 0x00,
    0x00, 0x20, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x80, 0x40, 0x20, 0x10, 0x08,

    /* 48..55 -------------------   */
    0x00, 0x70, 0xC8, 0xA8, 0x98, 0x70,
    0x00, 0x70, 0x20, 0x20, 0x60, 0x20,
    0x00, 0xF8, 0x80, 0x70, 0x08, 0xF0,
    0x00, 0xF0, 0x08, 0x78, 0x08, 0xF0,
    0x00, 0x10, 0xF8, 0x90, 0x50, 0x30,
    0x00, 0xF0, 0x08, 0xF0, 0x80, 0xF0,
    0x00, 0x70, 0x88, 0xF0, 0x80, 0x70,
    0x00, 0x20, 0x20, 0x10, 0x08, 0xF8,

    /* 56..63 -------------------   */
    0x00, 0x70, 0x88, 0x70, 0x88, 0x70,
    0x00, 0x70, 0x08, 0x78, 0x88, 0x70,
    0x00, 0x00, 0x10, 0x00, 0x10, 0x00,
    0x00, 0x20, 0x10, 0x00, 0x10, 0x00,
    0x00, 0x20, 0x40, 0x80, 0x40, 0x20,
    0x00, 0x00, 0xF8, 0x00, 0xF8, 0x00,
    0x00, 0x40, 0x20, 0x10, 0x20, 0x40,
    0x00, 0x20, 0x00, 0x30, 0x08, 0x70,

    /* 64..71 -------------------   */
    0x00, 0xF8, 0x80, 0xB8, 0xA8, 0x78,
    0x00, 0x88, 0x88, 0xF8, 0x88, 0x70,
    0x00, 0xF0, 0x88, 0xF0, 0x88, 0xF0,
    0x00, 0x70, 0x88, 0x80, 0x88, 0x70,
    0x00, 0xF0, 0x88, 0x88, 0x88, 0xF0,
    0x00, 0xF8, 0x80, 0xF0, 0x80, 0xF8,
    0x00, 0x80, 0x80, 0xF0, 0x80, 0xF8,
    0x00, 0x70, 0x88, 0x98, 0x80, 0x70,

    /* 72..79 -------------------   */
    0x00, 0x88, 0x88, 0xF8, 0x88, 0x88,
    0x00, 0x70, 0x20, 0x20, 0x20, 0x70,
    0x00, 0x70, 0x88, 0x08, 0x08, 0x08,
    0x00, 0x88, 0x90, 0xE0, 0xA0, 0x90,
    0x00, 0xF8, 0x80, 0x80, 0x80, 0x80,
    0x00, 0x88, 0x88, 0xA8, 0xD8, 0x88,
    0x00, 0x88, 0x98, 0xA8, 0xC8, 0x88,
    0x00, 0x70, 0x88, 0x88, 0x88, 0x70,

    /* 80..87 -------------------   */
    0x00, 0x80, 0x80, 0xF0, 0x88, 0xF0,
    0x00, 0x68, 0x90, 0x88, 0x88, 0x70,
    0x00, 0x88, 0x88, 0xF0, 0x88, 0xF0,
    0x00, 0xF0, 0x08, 0x70, 0x80, 0x78,
    0x00, 0x20, 0x20, 0x20, 0x20, 0xF8,
    0x00, 0x70, 0x88, 0x88, 0x88, 0x88,
    0x00, 0x20, 0x50, 0x88, 0x88, 0x88,
    0x00, 0x50, 0xA8, 0xA8, 0x88, 0x88,

    /* 88..95 -------------------   */
    0x00, 0x88, 0x50, 0x20, 0x50, 0x88,
    0x00, 0x20, 0x20, 0x70, 0x88, 0x88,
    0x00, 0xF8, 0x40, 0x20, 0x10, 0xF8,
    0x00, 0xE0, 0x80, 0x80, 0x80, 0xE0,
    0x00, 0x08, 0x10, 0x20, 0x40, 0x80,
    0x00, 0xE0, 0x20, 0x20, 0x20, 0xE0,
    0x1C, 0x14, 0x1C, 0xE0, 0xA0, 0xE0,
    0xFC, 0x00, 0x00, 0x00, 0x00, 0x00,

    /* 96..103 -------------------  */
    0x00, 0x00, 0x00, 0x00, 0x10, 0x20,
    0x00, 0x88, 0x88, 0xF8, 0x88, 0x70,
    0x00, 0xF0, 0x88, 0xF0, 0x88, 0xF0,
    0x00, 0x70, 0x88, 0x80, 0x88, 0x70,
    0x00, 0xF0, 0x88, 0x88, 0x88, 0xF0,
    0x00, 0xF8, 0x80, 0xF0, 0x80, 0xF8,
    0x00, 0x80, 0x80, 0xF0, 0x80, 0xF8,
    0x00, 0x70, 0x88, 0x98, 0x80, 0x70,

    /* 104..111 ------------------- */
    0x00, 0x88, 0x88, 0xF8, 0x88, 0x88,
    0x00, 0x70, 0x20, 0x20, 0x20, 0x70,
    0x00, 0x70, 0x88, 0x08, 0x08, 0x08,
    0x00, 0x88, 0x90, 0xE0, 0xA0, 0x90,
    0x00, 0xF8, 0x80, 0x80, 0x80, 0x80,
    0x00, 0x88, 0x88, 0xA8, 0xD8, 0x88,
    0x00, 0x88, 0x98, 0xA8, 0xC8, 0x88,
    0x00, 0x70, 0x88, 0x88, 0x88, 0x70,

    /* 112..119 ------------------- */
    0x00, 0x80, 0x80, 0xF0, 0x88, 0xF0,
    0x00, 0x68, 0x90, 0x88, 0x88, 0x70,
    0x00, 0x88, 0x88, 0xF0, 0x88, 0xF0,
    0x00, 0xF0, 0x08, 0x70, 0x80, 0x78,
    0x00, 0x20, 0x20, 0x20, 0x20, 0xF8,
    0x00, 0x70, 0x88, 0x88, 0x88, 0x88,
    0x00, 0x20, 0x50, 0x88, 0x88, 0x88,
    0x00, 0x50, 0xA8, 0xA8, 0x88, 0x88,

    /* 120..127 ------------------- */
    0x00, 0x88, 0x50, 0x20, 0x50, 0x88,
    0x00, 0x20, 0x20, 0x70, 0x88, 0x88,
    0x00, 0xF8, 0x40, 0x20, 0x10, 0xF8,
    0x00, 0x30, 0x40, 0x80, 0x40, 0x30,
    0x00, 0x20, 0x20, 0x20, 0x20, 0x20,
    0x00, 0xC0, 0x20, 0x30, 0x20, 0xC0,
    0x00, 0x00, 0x10, 0xA8, 0x40, 0x00,
    0xFC, 0xFC, 0xFC, 0xFC, 0xFC, 0xFC,

};

static GLubyte _Font8[] =
{
    /* db 16 dup (0) */
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x3C, 0x66, 0x66, 0x66, 0x66, 0x00, 0x6C,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x3C, 0x66, 0x3E, 0x06, 0x3C, 0x00, 0x34,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x20, 0x30, 0x38, 0x3C, 0x38, 0x30, 0x20, /* Arrow right	    */
    0x00, 0x04, 0x0C, 0x1C, 0x3C, 0x1C, 0x0C, 0x04, /* Arrow left	    */

    /* 8..15 ------------------- */
    0x00, 0x00, 0x10, 0x38, 0x7C, 0xFE, 0x00, 0x00, /* Arrow down	    */
    0x00, 0x00, 0xFE, 0x7C, 0x38, 0x10, 0x00, 0x00, /* Arrow up		    */
    0x3C, 0x42, 0x99, 0xBD, 0xBD, 0x99, 0x42, 0x3C, /* Radio button set	    */
    0x3C, 0x42, 0x81, 0x81, 0x81, 0x81, 0x42, 0x3C, /* Radio button empty   */
    0x3C, 0x7E, 0xFF, 0xFF, 0xFF, 0xFF, 0x7E, 0x3C, /* Backgd. Radio button */
    0xFF, 0xC3, 0xA5, 0x99, 0x99, 0xA5, 0xC3, 0xFF, /* Pushbutton activated */
    0xFF, 0x81, 0x81, 0x81, 0x81, 0x81, 0x81, 0xFF, /* Pushbutton empty     */
    0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, /* Filled - Background  */

    /* 16..23 ------------------- */
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x3C, 0x66, 0x66, 0x66, 0x3C, 0x00, 0x6C,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,

    /* 24..31 -------------------       */
    /*  Arrow Up / ArrowDown / ArrowLeft / ArrowRight        */
    0x00, 0x10, 0x10, 0x10, 0x92, 0x7C, 0x38, 0x10,
    0x00, 0x10, 0x38, 0x7C, 0x92, 0x10, 0x10, 0x10,
    0x00, 0x10, 0x20, 0x60, 0xFE, 0x60, 0x20, 0x10,
    0x00, 0x10, 0x08, 0x0C, 0xFE, 0x0C, 0x08, 0x10,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,

    /* 32..39 ---------------- Start with Blank */
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x18, 0x00, 0x18, 0x18, 0x18, 0x18, 0x18,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x66, 0x66, 0x66,
    0x00, 0x08, 0xFE, 0x08, 0x24, 0xFE, 0x24, 0x00,
    0x00, 0x18, 0x3C, 0x4C, 0x18, 0x32, 0x3C, 0x18,
    0x00, 0x46, 0x66, 0x30, 0x18, 0x6C, 0x64, 0x00,
    0x00, 0x76, 0xEC, 0xDC, 0x72, 0x38, 0x6C, 0x38,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x18, 0x18,

    /* 40..47 -------------------                */
    0x00, 0x0E, 0x18, 0x30, 0x30, 0x30, 0x18, 0x0E,
    0x00, 0x70, 0x18, 0x0C, 0x0C, 0x0C, 0x18, 0x70,
    0x00, 0x10, 0x54, 0x08, 0xC6, 0x08, 0x54, 0x10,
    0x00, 0x18, 0x18, 0x7E, 0x7E, 0x18, 0x18, 0x00,
    0x00, 0x60, 0x30, 0x30, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x7E, 0x00, 0x00, 0x00,
    0x00, 0x18, 0x18, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x40, 0x60, 0x30, 0x18, 0x0C, 0x06, 0x02,

    /* 48..55 -------------------               */
    0x00, 0x3C, 0x66, 0x76, 0x6E, 0x66, 0x66, 0x3C,
    0x00, 0x38, 0x18, 0x18, 0x18, 0x18, 0x38, 0x18,
    0x00, 0x7E, 0x66, 0x30, 0x0C, 0x46, 0x66, 0x3C,
    0x00, 0x3C, 0x46, 0x06, 0x0C, 0x18, 0x4C, 0x3E,
    0x00, 0x1C, 0x0C, 0x7E, 0x64, 0x34, 0x1C, 0x3C,
    0x00, 0x3C, 0x66, 0x46, 0x0E, 0x7C, 0x60, 0x7E,
    0x00, 0x3C, 0x66, 0x66, 0x7C, 0x60, 0x62, 0x3C,
    0x00, 0x30, 0x30, 0x30, 0x18, 0x0C, 0x66, 0x7E,

    /* 56..63 -------------------               */
    0x00, 0x3C, 0x66, 0x6E, 0x3C, 0x76, 0x66, 0x3C,
    0x00, 0x3C, 0x46, 0x06, 0x3E, 0x66, 0x66, 0x3C,
    0x00, 0x00, 0x18, 0x18, 0x00, 0x18, 0x18, 0x00,
    0x00, 0x30, 0x18, 0x18, 0x00, 0x18, 0x18, 0x00,
    0x00, 0x00, 0x12, 0x24, 0x6C, 0x24, 0x12, 0x00,
    0x00, 0x00, 0x7E, 0x00, 0x00, 0x7E, 0x00, 0x00,
    0x00, 0x00, 0x08, 0x24, 0x36, 0x24, 0x08, 0x00,
    0x00, 0x18, 0x00, 0x18, 0x0C, 0x06, 0x66, 0x3C,

    /* 64..71 -------------------               */
    0x00, 0x3E, 0x60, 0x66, 0x6A, 0x6E, 0x66, 0x3C,
    0x00, 0xCC, 0x46, 0x7E, 0x26, 0x36, 0x1E, 0x0E,
    0x00, 0xF8, 0x66, 0x66, 0x7C, 0x66, 0x66, 0xFC,
    0x00, 0x3C, 0x66, 0xC0, 0xC0, 0xC0, 0x66, 0x3C,
    0x00, 0xF0, 0x6C, 0x66, 0x66, 0x66, 0x6C, 0xF8,
    0x00, 0xF8, 0x66, 0x60, 0x78, 0x60, 0x66, 0xFC,
    0x00, 0x40, 0x60, 0x60, 0x78, 0x60, 0x66, 0x7C,
    0x00, 0x38, 0x66, 0xC6, 0xCE, 0xC0, 0x66, 0x3C,

    /* 72..79 -------------------               */
    0x00, 0xE4, 0x66, 0x66, 0x7E, 0x66, 0x66, 0xE6,
    0x00, 0x30, 0x18, 0x18, 0x18, 0x18, 0x18, 0x38,
    0x00, 0x38, 0x6C, 0x0C, 0x0C, 0x0C, 0x0C, 0x1C,
    0x00, 0xE4, 0x6E, 0x78, 0x70, 0x78, 0x6C, 0xE6,
    0x00, 0xFC, 0x66, 0x60, 0x60, 0x60, 0x60, 0xE0,
    0x00, 0xE4, 0x62, 0x62, 0x6A, 0x7E, 0x76, 0xE2,
    0x00, 0xE4, 0x66, 0x6E, 0x7A, 0x72, 0x72, 0xE2,
    0x00, 0x38, 0x6C, 0xC6, 0xC6, 0xC6, 0x6C, 0x38,

    /* 80..87 -------------------       */
    0x00, 0xC0, 0x60, 0x60, 0x7C, 0x66, 0x66, 0xFC,
    0x00, 0x0C, 0x68, 0xD4, 0xC6, 0xC6, 0x6C, 0x38,
    0x00, 0xE4, 0x66, 0x6C, 0x7C, 0x66, 0x66, 0xFC,
    0x00, 0x78, 0x66, 0x06, 0x1C, 0x30, 0x66, 0x3C,
    0x00, 0x60, 0x30, 0x30, 0x30, 0x30, 0xB2, 0xFE,
    0x00, 0x38, 0x74, 0x62, 0x62, 0x62, 0x62, 0xE6,
    0x00, 0x10, 0x18, 0x34, 0x34, 0x62, 0x62, 0xC6,
    0x00, 0xC4, 0xEE, 0xFA, 0xD2, 0xC2, 0xC2, 0xC6,

    /* 88..95 Last is underscore       */
    0x00, 0xC4, 0x66, 0x3C, 0x18, 0x3C, 0x66, 0xC6,
    0x00, 0x30, 0x18, 0x18, 0x18, 0x34, 0x62, 0xC6,
    0x00, 0x7C, 0x66, 0x30, 0x18, 0x0C, 0x66, 0x3E,
    0x00, 0x1E, 0x18, 0x10, 0x10, 0x10, 0x18, 0x1E,
    0x00, 0x02, 0x06, 0x0C, 0x18, 0x30, 0x60, 0x40,
    0x00, 0x78, 0x18, 0x08, 0x08, 0x08, 0x18, 0x78,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0xFE, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,

    /* 96..103 -------------------      */
    0x00, 0x00, 0x00, 0x00, 0x08, 0x18, 0x18, 0x00,
    0x00, 0x3C, 0x66, 0x3E, 0x06, 0x3C, 0x00, 0x00,
    0x00, 0xFC, 0x66, 0x66, 0x66, 0x7C, 0x60, 0xE0,
    0x00, 0x38, 0x64, 0x60, 0x64, 0x38, 0x00, 0x00,
    0x00, 0x3C, 0x66, 0x66, 0x66, 0x3E, 0x06, 0x0E,
    0x00, 0x3C, 0x60, 0x7E, 0x66, 0x3C, 0x00, 0x00,
    0x00, 0x60, 0x30, 0x30, 0x30, 0x7C, 0x30, 0x1C,
    0x00, 0x38, 0x04, 0x3E, 0x66, 0x3E, 0x00, 0x00,

    /* 104..111  -------------------    */
    0x00, 0xE4, 0x66, 0x66, 0x66, 0x7C, 0x60, 0xE0,
    0x00, 0x30, 0x18, 0x18, 0x18, 0x38, 0x00, 0x18,
    0x00, 0x60, 0x90, 0x18, 0x18, 0x38, 0x00, 0x18,
    0x00, 0xE6, 0x6C, 0x78, 0x6C, 0x66, 0x60, 0xE0,
    0x00, 0x30, 0x18, 0x18, 0x18, 0x18, 0x18, 0x38,
    0x00, 0xC4, 0xC6, 0xD6, 0xFE, 0xEC, 0x00, 0x00,
    0x00, 0x64, 0x66, 0x66, 0x66, 0x7C, 0x00, 0x00,
    0x00, 0x3C, 0x66, 0x66, 0x66, 0x3C, 0x00, 0x00,

    /* 112..119  -------------------            */
    0x00, 0x60, 0x78, 0x66, 0x66, 0xFC, 0x00, 0x00,
    0x00, 0x06, 0x3E, 0x66, 0x66, 0x3E, 0x00, 0x00,
    0x00, 0x40, 0x60, 0x60, 0x6C, 0x78, 0x00, 0x00,
    0x00, 0x7C, 0xC6, 0x3C, 0x60, 0x3E, 0x00, 0x00,
    0x00, 0x30, 0x18, 0x18, 0x18, 0x7E, 0x18, 0x00,
    0x00, 0x3C, 0x66, 0x66, 0x66, 0x66, 0x00, 0x00,
    0x00, 0x18, 0x3C, 0x66, 0x66, 0x66, 0x00, 0x00,
    0x00, 0x68, 0x7C, 0xD6, 0xC6, 0xC6, 0x00, 0x00,

    /*   120..127  -------------------  */
    0x00, 0x66, 0x3C, 0x18, 0x3C, 0x66, 0x00, 0x00,
    0x00, 0x78, 0x84, 0x3E, 0x66, 0x66, 0x00, 0x00,
    0x00, 0x7C, 0x32, 0x18, 0x8C, 0x7E, 0x00, 0x00,
    0x00, 0x0E, 0x18, 0x18, 0x30, 0x18, 0x18, 0x0E,
    0x00, 0x18, 0x18, 0x18, 0x00, 0x18, 0x18, 0x18,
    0x00, 0x70, 0x18, 0x18, 0x0C, 0x18, 0x18, 0x70,
    0x00, 0x00, 0x00, 0x0C, 0x9E, 0xF2, 0x60, 0x00,
    0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,

    /* 128 ----------                   */
    0x00, 0x00, 0x00, 0x00, 0xC6, 0x6C, 0x38, 0x10
};


/* --------------- */
static FONT_T _wFonts[DRAWTOOL_MAXFONT] =
{
    /* _Font6	*/
    { _Font6, 6, 6, 8 },
    /* --------------- */
    /* _Font8	*/
    { _Font8, 8, 8, 8 },
    /* ---------------	*/
    /* Installed Font(s)	*/
    { _Font8, 8, 8, 8 }
};

static DRAWTOOL_STYLE_T BasicStyle =
{
    SDLGDRW_FONT8,
    SDLGDRW_FONT8,
    /* 'text_col' For the different button colors  */
    {
        { 255, 255, 255, 100, 100, 100 },	/* For non highlighted text and hotkey  */
        { 255,  85,  85, 170,  20,  20 },	/* Color of highlighted text and hotkey      */
        { 255, 255, 255,   0,   0,   0 },	/* Color for labels in map	                 */
        {   0,   0,   0,   0,   0,   0 },   /* Color for Arrows on buttons, labels and edit */
    },
    { 170, 0,   0, 255, 255, 85, 0, 170, 0 }, /* Colors for HP-Bar                */
    {  40,  32, 172 },	/* Color for scrollbox background  */
    {   0,   0, 255 },  /* Color of progress bar           */
    /* 'button_col */
    {
        {  64, 157, 201,  64, 109, 201,  45,  48,  17 }, /* BUTTONMID, BUTTONTOP, BUTTONBOTTOM */
        {  14,  39,  44,  6,  48,  73,    6,  48,  73 }, /* Inverted                           */
        { 255, 255, 255,  45,  48,  17,  64, 109, 201 }, /* While background: Edit, inverted   */
        { 159,  80, 240, 208, 176, 255,  64,  24, 111 }, /* DRAWTOOL_BUTTONVIOLET     */
        {  80,  48,  16, 143,  80,  31,  16,   8,   0 }, /* DRAWTOOL_BUTTONGOLDLO     */
        { 255, 192,  15, 255, 248,  31, 144, 104,   0 }, /* DRAWTOOL_BUTTONGOLDLOHI   */
        {   0,   0,   0, 192, 192, 192,  63,  63,  63 }, /* DRAWTOOL_BUTTONBLACK      */
        { 159,  14,   6, 63,   63,  63, 192, 192, 192 }, /* DRAWTOOL_FILLHILITE       */
        {   1,   0,   0,   0,   0,   0,   0,   0,   0 }, /* DRAWTOOL_FILLBLACK        */
        {   0,   0, 170,   0,   0, 170,   0,   0, 170 }  /* DRAWTOOL_DEEPBLUE         */
    }
};

static DRAWTOOL_STYLE_T SmallStyle =
{
    SDLGDRW_FONT6,
    SDLGDRW_FONT6,
    /* 'text_col' For the different button colors  */
    {
        { 255, 255, 255, 100, 100, 100 },	/* For non highlighted text and hotkey  */
        { 170,   0,   0, 170,   0, 100 },	/* Color of highlighted text and hotkey      */
        { 255, 255, 255,   0,   0,   0 },	/* Color for labels in map	                 */
        {   0,   0,   0,   0,   0,   0 },   /* Color for Arrows on buttons, labels and edit */
    },
    { 170, 0,   0, 255, 255, 85, 0, 170, 0 }, /* Colors for HP-Bar                */
    {  40,  32, 172 },	/* Color for scrollbox background  */
    {   0,   0, 255 },  /* Color of progress bar           */
    /* 'button_col */
    {
        {  14,  39,  44,  16,  48,  73,  16,  48,  73 }, /* BUTTONMID, BUTTONTOP, BUTTONBOTTOM */
        {  64, 157, 201,  45,  48,  17,  64, 109, 201 }, /* Inverted                           */
        { 255, 255, 255,  45,  48,  17,  64, 109, 201 }, /* While background: Edit, inverted   */
        { 159,  80, 240, 208, 176, 255,  64,  24, 111 }, /* DRAWTOOL_BUTTONVIOLET     */
        {  80,  48,  16, 143,  80,  31,  16,   8,   0 }, /* DRAWTOOL_BUTTONGOLDLO     */
        { 255, 192,  15, 255, 248,  31, 144, 104,   0 }, /* DRAWTOOL_BUTTONGOLDLOHI   */
        {   0,   0,   0, 192, 192, 192,  63,  63,  63 }, /* DRAWTOOL_BUTTONBLACK      */
        { 159,  14,   6, 63,   63,  63, 192, 192, 192 }, /* DRAWTOOL_FILLHILITE       */
        {   1,   0,   0,   0,   0,   0,   0,   0,   0 }, /* DRAWTOOL_FILLBLACK        */
        {   0,   0, 170,   0,   0, 170,   0,   0, 170 }  /* DRAWTOOL_DEEPBLUE         */
    }
};

static DRAWTOOL_STYLE_T ActStyle =
{
    SDLGDRW_FONT8,
    SDLGDRW_FONT8,
    /* 'text_col' For the different button colors  */
    {
        { 255, 255, 255, 100, 100, 100 },	/* For non highlighted text and hotkey  */
        { 255,  85,  85, 170,  20,  20 },	/* Color of highlighted text and hotkey      */
        { 255, 255, 255,   0,   0,   0 },	/* Color for labels in map	                 */
        {   0,   0,   0,   0,   0,   0 },   /* Color for Arrows on buttons, labels and edit */
    },
    { 170, 0,   0, 255, 255, 85, 0, 170, 0 }, /* Colors for HP-Bar                */
    {  40,  32, 172 },	/* Color for scrollbox background  */
    {   0,   0, 255 },  /* Color of progress bar           */
    /* 'button_col */
    {
        {  64, 157, 201,  64, 109, 201,  45,  48,  17 }, /* BUTTONMID, BUTTONTOP, BUTTONBOTTOM */
        {  14,  39,  44, 107, 149, 165, 107, 149, 165 }, /* Inverted                           */
        { 255, 255, 255, 107, 149, 165, 107, 149, 165 }, /* While background: Edit, inverted   */
        { 159,  80, 240, 208, 176, 255,  64,  24, 111 }, /* DRAWTOOL_BUTTONVIOLET     */
        {  80,  48,  16, 143,  80,  31,  16,   8,   0 }, /* DRAWTOOL_BUTTONGOLDLO     */
        { 255, 192,  15, 255, 248,  31, 144, 104,   0 }, /* DRAWTOOL_BUTTONGOLDLOHI   */
        {   0,   0,   0, 192, 192, 192,  63,  63,  63 }, /* DRAWTOOL_BUTTONBLACK      */
        { 159,  14,   6, 63,   63,  63, 192, 192, 192 }, /* DRAWTOOL_FILLHILITE       */
        {   1,   0,   0,   0,   0,   0,   0,   0,   0 }, /* DRAWTOOL_FILLBLACK        */
        {   0,   0, 170,   0,   0, 170,   0,   0, 170 }  /* DRAWTOOL_DEEPBLUE         */
    }
};

/* -- Star field descriptor: [0]: star_x, [1]: star_y, [2]: radius, [3]: Color */
static short int StarField_Desc[150 * 4];

/*******************************************************************************
* CODE								                                           *
*******************************************************************************/

/*
 * Name:
 *     drawtool_iget_textsize
 * Description:
 *     Fills the given rectangle with the measure the text needs in pixels.
 *     Uses font actually set.
 * Input:
 *     pstring *: String to return the rectangle for
 *     pcell *:   Pointer on the rectangle the string has to be drawn into
 *     pformat *: Holds the size of the rectangle needed to draw the string
 */
static void drawtool_iget_textsize(char *pstring, SDLGL_RECT_T *pcell, SDLGL_RECT_T *pformat)
{
    int len, lines;


    len   = 0;
    lines = 1;		/* Minimum one line */

    while(*pstring != 0)
    {
        if((*pstring >= ' ') && !strchr("~{}\n", *pstring))
        {
            len++;
        }
        else if(*pstring == '\n')
        {
            lines++;
        }

        pstring++;
    }

    pformat->x = pcell->x;
    pformat->y = pcell->y;
    pformat->w = (len * _wFonts[ActStyle.drawfont_no].fontw);
    pformat->h = (lines * _wFonts[ActStyle.drawfont_no].fonth);
}

/*
 * Name:
 *     drawtool_iget_textalignrect
 * Description:
 *     Fills the given rectangle with the measure the text needs in pixels.
 *     Uses font actually set.
 * Input:
 *     pstring *: String to return the rectangle for
 *     pcell *:   Pointer on the rectangle the string has to be drawn into
 *     pformat *: Holds the size of the rectangle needed, adjusted
 *     align:     'pformat' is aligned as given
 */
static void drawtool_iget_textalignrect(char *pstring, SDLGL_RECT_T *pcell, SDLGL_RECT_T *pformat, char align)
{
    drawtool_iget_textsize(pstring, pcell, pformat);

    /* Is always vertical aligned! */
    pformat->y += (pcell->h - pformat->h) / 2;


    /* Now adjust 'pformat' */
    if(align == 'R')
    {
        pformat->x = (pcell->x + pcell->w - 3) - pformat->w;
    }
    else if(align == 'C')
    {
        pformat->x = pcell->x + (pcell->w / 2) - (pformat->w / 2);
    }
    else
    {
        /* Fixed left position from border, if a 'button' */
        pformat->x = pcell->x + 3;
    }
}



/*
 * Name:
 *     drawtool_iprint_char
 * Description:
 *     Output of one single char.
 * Input:
 *     ppos *:    Position where to print
 *     c:         The char to put out
 *     col_style: Style to use for printing text
 */
static void drawtool_iprint_char(SDLGL_RECT_T *ppos, char c, int col_style)
{
    FONT_T *pfont;


    pfont = &_wFonts[ActStyle.drawfont_no];

    /* Because OpenGL draws the bitmaps from bottom to top, we have to  */
    /* add the heigth of the font to the position			*/
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glColor3ubv(ActStyle.text_col[col_style]);

    glRasterPos2i(ppos->x, ppos->y + pfont->fonth);

    glBitmap(pfont->cwidth, pfont->fonth,
             0, 0,
             pfont->fontw, 0,
             &pfont->font[c * pfont->fonth]);
}

/*
 * Name:
 *     drawtool_istring
 * Description:
 *     Prints a string, using actual font set
 * Input:
 *     prect *:    Pointer on SDLGL_RECT_T to draw text into
 *     ptext *:    Text to print.
 *     col_style:  0: Standard, 1: highlighted, 2: edit_text
 */
static void drawtool_istring(SDLGL_RECT_T *ppos, char *ptext, int col_style)
{
    static int secondtime = 0;

    unsigned char *ptextcolor;
    FONT_T *fptr;
    GLubyte *ptext_cur;	/* Pointer on text to display 	*/
    int  cx,
    	 cy;		    /* Actual char position 	*/
    GLubyte c;
    int sx, sy;


    /* first test for NULL-Pointer */
    if(ptext == NULL)
    {
        return;
    }

    if(! secondtime)
    {
        /* Set pixelstore only once */
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        secondtime++;
    }

    ptextcolor = ActStyle.text_col[col_style];

    glColor3ubv(ptextcolor);

    fptr = &_wFonts[ActStyle.drawfont_no];

    /* Because OpenGL draws the bitmaps from bottom to top, we have to  */
    /* add the heigth of the font to the position			*/
    sx = ppos->x;
    sy = ppos->y + fptr->fonth;

    cx = sx;
    cy = sy;				    /* Save for Scrolling 		*/

    glRasterPos2i(cx, cy);		/* Set Start-Position for draw	*/


    /****** Put Text itself ***********************************/
    ptext_cur = (GLubyte *)ptext;

    while(*ptext_cur != 0)
    {
        /* Get char */
        c = (char)(*ptext_cur & 0x7F);  /* Max. 127 */

        switch (c)
        {
            case '\n':
            case '\r':		        /* Make CR */
                cy += fptr->fonth;	/* Next line downwards */
                cx = sx;		    /* Start of next line  */
                glRasterPos2i(cx, cy);
                break;

            case 6:
            	/* Set Color */
                ptext_cur++;
				glColor3ubv(Draw_Colors[*ptext_cur]);
                glRasterPos2i(cx, cy);
                break;

            case '{':
                /* Set Color */
				glColor3ubv(&ptextcolor[3]);
                glRasterPos2i(cx, cy);
                break;

            case '~':
                ptext_cur++;				    /* Don't draw 	    */
                glColor3ubv(&ptextcolor[3]);    /* Force set color  */
                glRasterPos2i(cx, cy);

                /* Draw the followin char in highlighted color 	*/
                c = (char)(*ptext_cur & 0x7F); 		/* Max. 127 	*/

                glBitmap(fptr->cwidth, fptr->fonth,
	                     0, 0,
                         fptr->fontw, 0,
                         &fptr->font[c * fptr->fonth]);

                /* Set color back to normal */
                glColor3ubv(ptextcolor);
                glRasterPos2i(cx + fptr->fontw, cy); /* Force set color  */
                break;

            case '}':
                ptext_cur++;			    /* Don't draw 	     */
                glColor3ubv(ptextcolor);	/* Switch back color */
                glRasterPos2i(cx, cy);
                break;

            case '\t':

            	/* Ignore tab */
                cx += fptr->fontw;
                break;

            default:
                glBitmap(fptr->cwidth, fptr->fonth,
                	     0, 0,
                	     fptr->fontw, 0,
                	     &fptr->font[c * fptr->fonth]);
        }

        /* next char */
        ptext_cur++;

        cx += fptr->fontw;
    }
}

/*
 * Name:
 *     drawtool_ibordered_poly
 * Description:
 *     Draws a bordered polygon, using the given points.Points 'must' be
 *     given counter-clockwise!
 * Input:
 *     ppoints *: Pointer on numpoints (x,y) vertices
 *     numpoints: Number of points to draw
 *     fillcolor: 0: Don't fill
 *                > 0: Number of fill color to use
 *     trans:     0: None, > 0: Transparency
 */
static void drawtool_ibordered_poly(int *ppoints, int numpoints, int fillcolor, unsigned char trans)
{
    int count;
    int *plist;
    unsigned char *color;


    /* First draw filled part, if any */
    if (fillcolor > 0)
    {
        color = Poly_Colors[fillcolor];

        if(trans > 0)
        {
            /* -- Invoke rendering transparency -- */
            color[3] = trans;
            glColor4ubv(color);
            glEnable(GL_BLEND);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        }
        else
        {
            glColor3ubv(color);
        }

        count = numpoints;
        plist = ppoints;

        glBegin(GL_TRIANGLE_FAN);
            while(count > 0)
            {
                glVertex2iv(plist);
                plist += 2;
                count--;
            }
        glEnd();

        if(trans > 0)
        {
            /* -- Stop rendering transparency -- */
            glDisable(GL_BLEND);
        }
    }

    glColor3ubv(Poly_Colors[0]);

    glBegin(GL_LINE_LOOP);
        while(numpoints > 0)
        {
            glVertex2iv(ppoints);
            ppoints += 2;
            numpoints--;
        }
    glEnd();
}

/*
 * Name:
 *     drawtool_irectangle
 * Description:
 *     Draws a bordered polygon, using the given points.Points 'must' be
 *     given counter-clockwise!
 * Input:
 *     rect:       Draw this rectangle
 *     fill_color: 0: Don't fill
 *     trans:      Transparency, if any
 */
static void drawtool_irectangle(SDLGL_RECT_T *prect, int fill_color, unsigned char trans)
{
    int points[16];
    int i;


    /* Create the rectangle edge points */
    for(i = 0; i < 8; i += 2)
    {
        points[i]     = prect->x;  /* Set the base */
        points[i + 1] = prect->y;  /* Set the base */
    }

    points[4] += prect->w;
    points[6] += prect->w;
    points[3] += prect->h;
    points[5] += prect->h;

    drawtool_ibordered_poly(points, 4, fill_color, trans);
}

/*
 * Name:
 *     drawtool_idraw_buttonbk
 * Description:
 *     Draws a simple filled 2D-Rectangle, with a "Shadow"
 * Input:
 *     prect *:  Pointer on a text rectangle
 *     col_type: < 0: HP-Bar (3-Color:  Else Type of button (Color-Style))
 *     bar_val:  < 0: Don't draw, else size of a bar in percent of internal size of button
 */
static void drawtool_idraw_buttonbk(SDLGL_RECT_T *prect, int col_type, int bar_val)
{
    DRAWTOOL_STYLE_T *pstyle;
    int x, y, x2, y2;
    int bar_size;
    unsigned char *pbutton_col, bar_col_no;


    pstyle = &ActStyle;

    x = prect->x;
    y = prect->y;
    x2 = x + prect->w - 1;
    y2 = y + prect->h - 1;


    /* Get pointer on colors for button */
    if(bar_val >= 0)
    {
        bar_size = prect->w - 3;

        if(bar_val < 100)
        {
            bar_size = bar_size * bar_val / 100;
        }

        bar_size = (bar_size <= 0) ? 2 : bar_size;

        /* Standard-Button inverted */
        pbutton_col = pstyle->button_col[1];
        bar_col_no  = SDLGL_COL_BLUE;

        if(col_type < 0)
        {
            /* Set Color for HP-Bar */
            if(bar_val < 10)
            {
                bar_col_no = SDLGL_COL_RED;
            }
            else if(bar_val < 30)
            {
                bar_col_no = SDLGL_COL_YELLOW;
            }
            else
            {
                bar_col_no = SDLGL_COL_GREEN;
            }
        }
    }
    else
    {
        /* Standard-Button inverted */
        pbutton_col = pstyle->button_col[col_type];
    }

    /* Set the color for the background */
    glColor3ubv(&pbutton_col[0]);

    glBegin(GL_QUADS);
        glVertex2i(x, y2);
        glVertex2i(x2, y2);
        glVertex2i(x2, y);
        glVertex2i(x, y);
    glEnd();

    /* -- Special: Progress / HP-Bar */
    if(bar_val >= 0)
    {
        /* Draw the bar */
        sdlgl_set_color(bar_col_no);

        glBegin(GL_QUADS);
            glVertex2i(x + 1, y2);
            glVertex2i(x + bar_size, y2);
            glVertex2i(x + bar_size, y);
            glVertex2i(x + 1, y);
        glEnd();
    }

    glBegin(GL_LINES);
        /* Top Edge Line */
        glColor3ubv(&pbutton_col[3]);
        glVertex2i(x, y);
        glVertex2i(x2, y);
        glVertex2i(x2, y);
        glVertex2i(x2, y2);

    	/* Bottom Edge Line */
    	glColor3ubv(&pbutton_col[6]);
        glVertex2i(x2, y2);
        glVertex2i(x, y2);
        glVertex2i(x, y2);
        glVertex2i(x, y);

        if(prect->w > 14 || prect->h)
        {
            /* Double-Edge-Line only if minimum size */
            x  += 1;
            y  += 1;
            x2 -= 1;
            y2 -= 1;

            /* Top Edge Line */
            glColor3ubv(&pbutton_col[3]);
            glVertex2i(x, y);
            glVertex2i(x2, y);
            glVertex2i(x2, y);
            glVertex2i(x2, y2);

            /* Bottom Edge Line */
            glColor3ubv(&pbutton_col[6]);
            glVertex2i(x2, y2);
            glVertex2i(x, y2);
            glVertex2i(x, y2);
            glVertex2i(x, y);
        }
    glEnd();
}



/*
 * Name:
 *     drawtool_dlgbutton1
 * Description:
 *     Draws button with cutted edges at top
 * Input:
 *     rect:      Draw into this rectangle
 *     filled:    0: Don't fill
 *                1: Fill with 'lo' color
 *                2: Fill with 'hi' color
 */
static void drawtool_dlgbutton1(SDLGL_RECT_T *prect, int filled)
{
    int points[16];
    int i;


    /* Create the rectangle edge points */
    for(i = 0; i < 12; i += 2)
    {
        points[i]     = prect->x;
        points[i + 1] = prect->y;  /* Set the base */
    }

    points[1]  += prect->h;
    points[2]  += prect->w;
    points[3]  += prect->h;
    points[4]  += prect->w;
    points[5]  += 7;
    points[6]  += (prect->w - 7);
    points[8]  += 7;
    points[11] += 7;

    drawtool_ibordered_poly(points, 6, filled, 0);
}

/*
 * Name:
 *     drawtool_ibutton_labelled
 * Description:
 *     Draws a "button" with the given text. The text starts half font
 *     size form left and half font size from top from he given rectangle.
 *     Draws a standard background.
 * Input:
 *     prect *:   Rectangle holding the size of the button
 *     ptext *:   Text to print into the button. If NULL, no text is drawn
 *     col_style: Number of color for drawing text
 *     but_style: Style of button to draw
 */
static void drawtool_ibutton_labelled(SDLGL_RECT_T *prect, char *ptext, int col_style, int but_style)
{
    SDLGL_RECT_T sizerect;


    /* First draw the background */
    if(but_style == 0)
    {
        drawtool_idraw_buttonbk(prect, 0, -1);
    }
    else if(but_style == 1)
    {
        drawtool_dlgbutton1(prect, 1);
    }
    else if(but_style == 2)
    {
        drawtool_irectangle(prect, 1, 0);
    }

    if(ptext)
    {
        drawtool_iget_textalignrect(ptext, prect, &sizerect, 'C');
        drawtool_istring(&sizerect, ptext, col_style);
    }
}

/*
 * Name:
 *     drawtool_iprint_value
 * Description:
 *     Prints a value given in 'value *' in format 'which'
 * Input:
 *     pinput *: Print value for  this field
 *     prect *:  Into this rectangle
 *     with_bk:  Print the value with a backgound yes/no
 *     align:    Align text (default: 'L'eft)
 *     @TODO: col_style: Set number of text style for drawing
 */
static void drawtool_iprint_value(SDLGL_INPUT_T *pinput, SDLGL_RECT_T *prect, char with_bk, char align)
{
    SDLGL_RECT_T text_rect;
    char val_str[100];
    int value;
    char *pdata;


    if(pinput)
    {
        pdata = (char *)pinput->pdata;

        if(pdata)
        {
            /* play it save */
            val_str[0] = 0;
            memcpy(&text_rect, prect, sizeof(SDLGL_RECT_T));

            if(with_bk)
            {
                /* First draw the background */
                drawtool_idraw_buttonbk(&text_rect, 0, 0);

                text_rect.x += 3;
                text_rect.y += 3;
            }

            switch(pinput->val_type)
            {
                case SDLGL_VAL_NONE:
                case SDLGL_VAL_STRING:
                    drawtool_istring(&text_rect, pdata, 2);
                    return;

                case SDLGL_VAL_CHAR:
                case SDLGL_VAL_UCHAR:
                    value  = *(char *)pdata;
                    sprintf(val_str, "%d", value);
                    break;

                case SDLGL_VAL_SHORT:
                    value = *(short int *)pdata;
                    sprintf(val_str, "%d", value);
                    break;

                case SDLGL_VAL_USHORT:
                    value = *(unsigned short int *)pdata;
                    sprintf(val_str, "%d", value);
                    break;

                case SDLGL_VAL_INT:
                    value = *(int *)pdata;
                    sprintf(val_str, "%d", value);
                    break;

                case SDLGL_VAL_UINT:
                    value = *(int *)pdata;
                    sprintf(val_str, "%u", value);
                    break;

                case SDLGL_VAL_FLOAT:
                    sprintf(val_str, "%f", *(float *)pdata);
                    break;

                case SDLGL_VAL_ONECHAR:
                    val_str[0] = *(char *)pdata;
                    val_str[1] = 0;
                    break;

                default:
                    break;
            }

            /* Draw values right adjusted, if no other format is given */
            align = (align == 0) ? 'R' : align;

            /* Now draw the value as name, if there any value names are given */
            if(pinput->pnames)
            {
                pdata = pinput->pnames;

                while(value > 0)
                {
                    pdata = strchr(pdata, 0);
                    value--;

                    /* Skip leading break-char of string */
                    pdata++;

                    if(*pdata == 0)
                    {
                        break;
                    }
                }
            }
            else
            {
                pdata = val_str;
            }

            drawtool_iget_textalignrect(pdata, prect, &text_rect, align);

            /* Draw the edited value -- Sub-Code: Kind of edited value in 'pdata' */
            drawtool_istring(&text_rect, pdata, 0);
        }
    }
}

/*
 * Name:
 *     drawtool_iinput_text
 * Description:
 *     Draws an "EditField "button" with the given text. The text starts
 *     half font size form left and half font size from top from he given
 *     rectangle.
 *     Draws a "shadowed" background with black chars on white background
 *     in it. If "curpos" is >= 0, the an underscore is drawn at the position
 *     of the cursor "curpos" chars from left.
 *     Always FONT8 is used.
 *     curpos:  Where to draw the cursor (char position !)
 * Input:
 *     pinput *: Description of edit-pinput to print
 */
static void drawtool_iinput_text(SDLGL_INPUT_T *pinput)
{
    SDLGL_RECT_T sizerect;
    char bk_style, text_style;


    if(pinput->code > 0)
    {
        bk_style = 2;
        text_style = 3;
    }
    else
    {
        bk_style = 1;
        text_style = 2;
    }
    /* First draw the background -- Active -- Inactive */
    drawtool_idraw_buttonbk(&pinput->rect, bk_style, -1);

    drawtool_iget_textalignrect(pinput->pdata, &pinput->rect, &sizerect, 0);

    /* Draw the edited value -- Sub-Code: Kind of edited value in 'pdata' */
    drawtool_istring(&sizerect, pinput->pdata, text_style);

    /* Now add the cursor, if needed */
    if (pinput->el_top >= 0 && (pinput->fstate & SDLGL_FSTATE_HASFOCUS))
    {
        sizerect.x += (_wFonts[ActStyle.drawfont_no].fontw * pinput->el_top);
        sizerect.y += 1;
        drawtool_istring(&sizerect, "_", 3);
    }
}



/*
 * Name:
 *     drawtool_iinput_textarea
 * Description:
 *     Prints the string into the given rectangle. In case the string
 *     doesn't fit into the width of the rectangle, it's wrapped onto the
 *     next line(s).
 *     Words are assumed to end with one of the the following characters:
 *		space   (' ')
 *	    tab     ('\t')
 *		CR      ('\r')
 *		newline ('\n')
 *	 	and	    ('-')
 *     The standard white color is used.
 *     If the 'pinput' has no 'code' set (readonly), then the background is not drawn
 * Input:
 *     pinput *: Pointer on input field to draw
 */
static void drawtool_iinput_textarea(SDLGL_INPUT_T *pinput)
{
    SDLGL_RECT_T sizerect, text_rect;
    char *pnextbreak, *pprevbreak;
    char prevsavechar, nextsavechar = 0;
    char *ptext;


    if(pinput->pdata)
    {
        ptext = (char *)pinput->pdata;

        /* -- REctangle, if without button-background -- */
        text_rect.x = pinput->rect.x;
        text_rect.y = pinput->rect.y;
        text_rect.w = pinput->rect.w;
        text_rect.h = pinput->rect.h;

        /* First draw the background, if needed */
        if(pinput->code > 0)
        {
            drawtool_idraw_buttonbk(&text_rect, 1, -1);

            text_rect.x += 3;
            text_rect.y += 3;
            text_rect.w -= 6;
            text_rect.h -= 6;
        }

        /* First try if the whole string fits into the rectangles width */
        drawtool_iget_textsize(ptext, &text_rect, &sizerect);

        if(sizerect.w <= text_rect.w && sizerect.h <= text_rect.h)
        {
            drawtool_istring(&text_rect, ptext, 0);
        }
        else
        {
            /* Run pointer into the string 		    */
            pprevbreak = ptext;

            /* We have to scan word by word */
            do
            {
                pnextbreak = strpbrk(pprevbreak, " \t\r\n-");

                if(pnextbreak)
                {
                    /* The end of string is not reached yet */
                    pnextbreak++;  	      	    /* Include the breaking character */
                    nextsavechar = *pnextbreak; /* Save the break char.	          */
                    *pnextbreak  = 0;	        /* Create a string out of it.     */
                }

                /* Now get the size, of the string */
                drawtool_iget_textsize(ptext, &text_rect, &sizerect);

                if(sizerect.w > text_rect.w)
                {
                    /* One word to much, use previous break. */
                    prevsavechar = *pprevbreak;
                    *pprevbreak  = 0;	        /* Create the string */

                    /* Print the string... 	 		 */
                    drawtool_istring(&text_rect, ptext, 0);

                    /* Move string to new start */
                    ptext  = pprevbreak;
                    *ptext = prevsavechar;   	    /* Get back the prev char */

                    /* Move to next line... */
                    text_rect.y += sizerect.h;
                }
                else
                {
                	/* Check for end of string */
                    if(! pnextbreak)
                    {
                        drawtool_istring(&text_rect, ptext, 0);
                        return;
                    }
                    /* Move one word further.   */
                    pprevbreak = pnextbreak;
                }

                if(pnextbreak)
                {
                    /* bring the next char back */
                	*pnextbreak = nextsavechar;
                }
        	}
            while(*ptext != 0);
        }
    }
}

/*
 * Name:
 *     drawtool_ifield_special
 * Description:
 *     Draws standard dialog fields in font 8, using special bitmaps from
 *     font. The text is always rectangle.
 *     Settings as font and color  are taken from 'ActStyle'
 * Input:
 *     pinput *: Pointer onf field to draw
 */
static void drawtool_ifield_special(SDLGL_INPUT_T *pinput)
{
    SDLGL_RECT_T draw_rect[8];
    char val_str[30];
    char *pbc;
    int value, i, d_add;


    pbc = Button_Chars;

    switch(pinput->sdlgl_type)
    {
        case SDLGL_TYPE_RADIO:
            sdlgl_input_get_value(pinput, &value);

            drawtool_irectangle(&pinput->rect, 1, 0);

            if((char)value == pinput->sub_code)
            {
                /* Mark it as set */
                draw_rect[0].x = pinput->rect.x + 6;
                draw_rect[0].y = pinput->rect.y + 6;
                draw_rect[0].w = pinput->rect.w - 12;
                draw_rect[0].h = pinput->rect.h - 12;
            }
            drawtool_drawrect_colno(&draw_rect[0], SDLGL_COL_LIGHTGREY, 1);
            /* drawtool_irectangle(&draw_rect[0], 1, 0); */
            break;

        case SDLGL_TYPE_CHECKBOX:
            /* 1. Fill the background */
            drawtool_iprint_char(&pinput->rect, *pbc, 0);

            /* 2: Draw the state -- Point on state bitmaps	*/
            pbc +=2;

            if(pinput->sdlgl_type == SDLGL_TYPE_CHECKBOX && pinput->fstate & SDLGL_FSTATE_CHECKED)
            {
                /* Point on number of bitmap for state set */
                pbc += 2;
            }
            else
            {
                sdlgl_input_get_value(pinput, &value);

                if((char)value == pinput->sub_code)
                {
                    /* Point on number of bitmap for state set */
                    pbc += 2;
                }
            }

            drawtool_iprint_char(&pinput->rect, *pbc, 3);
            break;

        case SDLGL_TYPE_CHOOSENUM:
            /* Get value as string for printing */
            sdlgl_get_drawinfo(pinput, draw_rect);
            /* First draw the button left */
            drawtool_ibutton_labelled(&draw_rect[0], "-", 2, 2);
            /* Second print the button with the value */
            draw_rect[1].x += 2;
            draw_rect[1].w -= 4;
            drawtool_iprint_value(pinput, &draw_rect[1], 1, 'C');
            /* Third draw the button right */
            drawtool_ibutton_labelled(&draw_rect[2], "+", 2, 2);
            break;

        case SDLGL_TYPE_POINTBAR:
            if(pinput->val_max > 0)
            {
                sdlgl_input_get_value(pinput, &value);

                memcpy(&draw_rect[0], &pinput->rect, sizeof(SDLGL_RECT_T));

                draw_rect[0].w /= pinput->val_max;
                d_add = draw_rect[0].w;
                draw_rect[0].w--;

                /* First draw the background */
                for(i = 0; i < pinput->val_max; i++)
                {
                    drawtool_idraw_buttonbk(&draw_rect[0], 4, -1);
                    draw_rect[0].x += d_add;
                }

                /* Second draw chosen points draw the background */
                draw_rect[0].x = pinput->rect.x;
                for(i = 0; i < value; i++)
                {
                    drawtool_idraw_buttonbk(&draw_rect[0], 5, -1);
                    draw_rect[0].x += d_add;
                }
            }
            break;

        case SDLGL_TYPE_PROGRESSBAR:
            sdlgl_input_get_value(pinput, &value);

            sprintf(val_str, "%d%%", value);
            /* 1: Draw the background*/
            drawtool_idraw_buttonbk(&pinput->rect, 1, value);

            /* 3: Draw the progress in percent as text, "info" holds the number */
            drawtool_iget_textalignrect(val_str, &pinput->rect, &draw_rect[0], 'C');
            drawtool_istring(&draw_rect[0], val_str, 0);
            break;
    }
}

/*
 * Name:
 *     drawtool_islider
 * Description:
 *     Draws a slider using the info given int array pinfo[]
 * Input:
 *      prect *: Pointer on rectangles with size of complete slider
 *      is_hor:  Is horizontal yes/no
 */
static void drawtool_islider(SDLGL_RECT_T *pinfo, int is_hor)
{
    int chr_idx;


    /* 1. Draw the slider background */
    drawtool_drawrect(pinfo, ActStyle.scrollbk, 1);

    chr_idx = is_hor ? 16 : 12;
    /* 2. Draw the button with arrow up/left */
    drawtool_ibutton_labelled(&pinfo[1], &Button_Chars[chr_idx], 3, 0);
     /* 3. Draw the button with arrow down/right */
    drawtool_ibutton_labelled(&pinfo[2], &Button_Chars[chr_idx + 2], 3, 0);
    /* And now the slider button itself */
    drawtool_ibutton_labelled(&pinfo[3], NULL, 0, 0);
}

/*
 * Name:
 *     drawtool_isliderboxv
 * Description:
 *     Draws a slider box with a vertical slider.
 * Input:
 *     pinput *: Pointer on field with info about sliderbox
 *     drawfunc: Not NULL: Defined by user for drawing its elements
 */
static void drawtool_isliderboxv(SDLGL_INPUT_T *pinput, DRAWTOOL_USERFUNC_T drawfunc)
{
    SDLGL_RECT_T info[7];
    int i;


    sdlgl_get_drawinfo(pinput, info);

    if(drawfunc && pinput->el_visi > 0)
    {
        /* Caller to draw the elements in slider-box */
        /* Call for every element in list */
        for(i = 0; i < pinput->el_visi; i++)
        {
            drawfunc(pinput, &info[0], pinput->el_top + i);

            info[0].y += info[0].h;
        }
    }

    /* Now draw the slider and its buttons  */
    /* 1: Draw the box */
    drawtool_idraw_buttonbk(&pinput->rect, 0, -1);
    /* 2. Draw the slider */
    drawtool_islider(&info[2], 0);
}


/*
 * Name:
 *     drawtool_isliderboxh
 * Description:
 *     Draws a horizonta� slider with bottions on each side.
 *     Slider is 10 pixels width
 * Input:
 *     pinput *: Pointer on field with info about sliderbox
 *     drawfunc: Not NULL: Defined by user for drawing its elements
 */
static void drawtool_isliderboxh(SDLGL_INPUT_T *pinput, DRAWTOOL_USERFUNC_T drawfunc)
{
    SDLGL_RECT_T info[7];
    int i;


    sdlgl_get_drawinfo(pinput, info);

    if(drawfunc && pinput->el_visi > 0)
    {
        /* Caller to draw the elements in slider-box */
        /* Call for every element in list */
        for(i = 0; i < pinput->el_visi; i++)
        {
            drawfunc(pinput, &info[0], pinput->el_top + i);

            info[0].x += info[0].w;
        }
    }

    /* Now draw the slider and its buttons  */
    /* 1: Draw the box */
    drawtool_idraw_buttonbk(&pinput->rect, 0, -1);
    /* 2. Draw the slider */
    drawtool_islider(&info[2], 1);
}

/*
 * Name:
 *     drawtool_idraw_choose
 * Description:
 *     Draws a kind of slider, but with a rectangle for every point
 *     that can be chosen
 * Input:
 *     pinput *: Pointer on field with info about sliderbox
 */
static void drawtool_idraw_choose(SDLGL_INPUT_T *pinput)
{
    SDLGL_RECT_T info[7];
    int i, d_add, d_start, value;


    sdlgl_get_drawinfo(pinput, info);
    /* Get the value from field */
    sdlgl_input_get_value(pinput, &value);

    /* Draw the base: The buttons (violet) */
    drawtool_idraw_buttonbk(&info[0], 3, -1);
    drawtool_idraw_buttonbk(&info[2], 3, -1);

    if(pinput->el_visi > 0)
    {
        if(pinput->sdlgl_type == SDLGL_TYPE_CHOOSEH)
        {
            info[1].w = ((info[1].w - 1) / pinput->el_visi) - 1;
            info[1].x++;
            d_start = info[1].x;
            d_add   = (info[1].w + 1);

            /* Draw background points, lo */
            for(i = 1; i <= pinput->el_visi; i++)
            {
                drawtool_idraw_buttonbk(&info[1], 4, -1);
                info[1].x += d_add;
            }

            /* Draw highlighted points */
            info[1].x = d_start;
            for(i = 1; i <= value; i++)
            {
                drawtool_idraw_buttonbk(&info[1], 5, -1);
                info[1].x += d_add;
            }
        }
        else if(pinput->sdlgl_type == SDLGL_TYPE_CHOOSEV)
        {
            info[1].h = ((info[1].h - 1) / pinput->el_visi) - 1;
            info[1].y++;
            d_start = info[1].y;
            d_add = (info[1].h + 1);

            /* Draw background points, lo */
            for(i = 1; i <= pinput->el_visi; i++)
            {
                drawtool_idraw_buttonbk(&info[1], 4, -1);
                info[1].y += d_add;
            }

            /* Draw highlighted points */
            info[1].y = d_start;
            for(i = 1; i <= value; i++)
            {
                drawtool_idraw_buttonbk(&info[1], 5, -1);
                info[1].y += d_add;
            }
        }
    }
}

/* ========================================================================= */
/* ============================= THE PUBLIC ROUTINES ======================= */
/* ========================================================================= */

/*
 * Name:
 *     drawtool_set_colorno
 * Description:
 *     Sets a color from the special 'Color_Set'
 * Input:
 *     pal_no:   From which color range ?
 *     color_no: Number of color to set
 */
void drawtool_set_colorno(int pal_no, int color_no)
{
    switch(pal_no)
    {
        case 0:
            /* Don't changes base */
            break;

        case 1:
            /* -- Color of player -- */
            pal_no = 16;
            break;

        case 2:
            /* -- Color for range borders -- */
            pal_no = 26;
            break;

        case 3:
            /* 0: Map-Grid-Color        */
            /* 1: Sector-Border Color   */
            pal_no = 39;
            break;

        case 4:
            /* 0: Invalid color   */
            /* 1..6: Star Colors  */
            pal_no = 41;
            break;
    }

    glColor4ubv(Draw_Colors[pal_no + color_no]);
}

/*
 * Name:
 *     drawtool_set_color
 * Description:
 *     Sets a color from the special 'Color_Set'
 * Input:
 *     pal_no:   From which color range ?
 *     color_no: Number of color to set
 */
char drawtool_get_colorno(int pal_no, int color_no)
{
    /* @TODO: Add list of color numbers from SDLGL-Standard-Palette */
    switch(pal_no)
    {
        case 0:
            return color_no;

        case 1:
            /* -- Color of player -- */
            return (16 + color_no);

        case 2:
            /* -- Color for range borders -- */
            return (26 + color_no);

        case 3:
            /* 0: Map-Grid-Color        */
            /* 1: Sector-Border Color   */
            return (39 + color_no);

        case 4:
            /* 0: Map-Grid-Color        */
            /* 1: Sector-Border Color   */
            return (41 + color_no);
    }

    return 0;
}

/*
 * Name:
 *     drawtool_circle
 * Description:
 *     Draws a circle at the center of given rectangle.
 *     The number of segments is evaluated based on the radius of the circle
 * Input:
 *     prect *: Pointer on a text rectangle.
 *     radius:  With this radius
 *     col_no:  Number of color to use for drawing of filled part
 *     flags:  Draw it filled 0: No 1: filled 2: Filled with border
 */
void drawtool_circle(SDLGL_RECT_T *prect, int radius, char col_no, char flags)
{
    int i;
    float num_segments;
    float theta, angle, x, y, cx, cy;


    /* -- Calculate the number of segments -- */
    num_segments = (float)radius * 6;
    angle        = 2.0f * 3.1415926f / num_segments;
    /* --- Get the center --- */
    cx = (float)(prect->x + (prect->w / 2));
    cy = (float)(prect->y + (prect->h / 2));

    /* First draw the filled part, if set  */
    if(flags & 0x01)
	{
        glShadeModel(GL_SMOOTH);
        theta = 0.0f;

        /* -- Draw it filled -- */
        /* glBegin(GL_LINE_LOOP); */
        glBegin(GL_TRIANGLE_FAN);
            /* -- First Vertex in the center of the circle */
            if(flags & 0x02)
            {
                /* White center */
                drawtool_set_colorno(0, SDLGL_COL_WHITE);
            }

            glVertex2f(cx, cy);

            x = (float)radius * cos(0.0);
            y = (float)radius * sin(0.0);

            drawtool_set_colorno(0, col_no);
            glVertex2f(cx + x, cy + y);

            for(i = 0; i < num_segments; i++)
            {
                theta -= angle;

                x = (float)radius * cos(theta);
                y = (float)radius * sin(theta);

                glVertex2f(cx + x, cy + y);
            }
        glEnd();
    }

    if(flags & 0x04 || !flags)
    {
        drawtool_set_colorno(0, col_no);
        /* -- And now draw the border, use white, if border for filled */
        if(flags & 0x04)
        {
            /* its the border of a filled circle */
            drawtool_set_colorno(0, SDLGL_COL_WHITE);
        }

        theta = 0.0f;

        glBegin(GL_LINE_LOOP);
            /* glVertex2d(radius * cos(0.0) , radius * sin(0.0)); */
            for(i = 0; i < num_segments; i++)
            {
                /* get the current angle */
                x = (float)radius * cos(theta);
                y = (float)radius * sin(theta);

                glVertex2f(cx + x, cy + y);

                theta += angle;
            }
        glEnd();
    }
}

/*
 * Name:
 *     drawtool_drawrect
 * Description:
 *     Draws a simple filled/non filles 2D-Rectangle with the given color
 *     number
 * Input:
 *     prect:   Pointer on a text rectangle.
 *     color:  Pointer on a vector with RGB-color
 *     filled: Draw it filled 1 / 0
 */
void drawtool_drawrect(SDLGL_RECT_T *prect, unsigned char *color, int filled)
{
    int x, y, x2, y2;

    x  = prect->x;
    y  = prect->y;
    x2 = x + prect->w;    /* Remove subtraction of 1: 27.06.2008 / PAM */
    y2 = y + prect->h;


    glColor3ubv(color);
    glBegin(filled ? GL_QUADS : GL_LINE_LOOP);
        glVertex2i(x, y2);
        glVertex2i(x2, y2);
        glVertex2i(x2, y);
    	glVertex2i(x, y);
    glEnd();
}

/*
 * Name:
 *     drawtool_drawrect_colno
 * Description:
 *     Draws a rectangle with the given font color number
 * Input:
 *     prect:  Pointer on a text rectangle.
 *     color:  Pointer on a vector with RGB-color
 *     filled: Draw it filled 1 / 0
 *     border: > 0: Draw a border with this color
 */
void drawtool_drawrect_colno(SDLGL_RECT_T *prect, int color_no, int filled)
{
    drawtool_drawrect(prect, Draw_Colors[color_no], filled);
}

/*
 * Name:
 *     drawtool_drawrect_sides
 * Description:
 *     Draws a rectangle using tne range color numbers in 'range_col_no'
 *     If color = 0, no line is drawn
 * Input:
 *     prect *:
 *     range_no:
 *     side_bits:
 */
void drawtool_drawrect_sides(SDLGL_RECT_T *prect, int range_no, char side_bits)
{
    SDLGL_RECT_T drect;
    GLubyte *pcolor;


    /* Range-Colors - 1 */
    pcolor = &Draw_Colors[26 + range_no][0];

    drect.x = prect->x;
    drect.y = prect->y;
    drect.w = prect->w;
    drect.h = prect->h;

    glBegin(GL_LINES);
        if(side_bits & 0x01)
        {
            glColor3ubv(pcolor);

            glVertex2i(drect.x, drect.y);
            glVertex2i(drect.x + drect.w, drect.y);

        }
        if(side_bits & 0x02)
        {
            glColor3ubv(pcolor);

            glVertex2i(drect.x + drect.w, drect.y);
            glVertex2i(drect.x + drect.w, drect.y + drect.h);

        }
        if(side_bits & 0x04)
        {
            glColor3ubv(pcolor);

            glVertex2i(drect.x, drect.y + drect.h);
            glVertex2i(drect.x + drect.w, drect.y + drect.h);
        }
        if(side_bits & 0x08)
        {
            glColor3ubv(pcolor);

            glVertex2i(drect.x, drect.y);
            glVertex2i(drect.x, drect.y + drect.h);
        }
    glEnd();
}

/*
 * Name:
 *     drawtool_generate_starbk
 * Description:
 *     Generates random star descriptions for drawing a starfield
 * Input:
 *     ppos *:  Pointer on SDLGL_RECT_T, holding drawing position
 *     ptext *: Text to print.
 */
void drawtool_generate_starbk(short int width, short int height)
{
    short int *pstar_desc;
    int i;
    int base_color_no;

    /* -- Star field descriptor: [0]: star_x, [1]: star_y, [2]: radius, [3]: Color */
    pstar_desc = &StarField_Desc[0];

    base_color_no = drawtool_get_colorno(4, 0);

    for(i = 0; i < 148; i++)
    {
        /* Generate the star descriptions */
        pstar_desc[0] = (short int)fsctool_rand_no(width);
        pstar_desc[1] = (short int)fsctool_rand_no(height);
        pstar_desc[2] = (short int)fsctool_rand_no(2);
        pstar_desc[3] = (short int)(base_color_no + (fsctool_rand_no(26) % 20));

        pstar_desc += 4;
    }
}

/*
 * Name:
 *     drawtool_draw_starbk
 * Description:
 *     Generates random star descriptions for drawing a starfield
 * Input:
 *     ppos *:  Pointer on SDLGL_RECT_T, holding drawing position
 *     ptext *: Text to print.
 */
void drawtool_draw_starbk(int num_star)
{
    SDLGL_RECT_T rect;
    short int *pstar_desc;
    int i;


    rect.w = 6;
    rect.h = 6;


    num_star = (num_star > 148) ? 148 : num_star;
    pstar_desc = &StarField_Desc[0];

    for(i = 0; i < num_star; i++)
    {
        rect.x = pstar_desc[0];
        rect.y = pstar_desc[1];

        drawtool_circle(&rect, pstar_desc[2], pstar_desc[3], 0x03);

        pstar_desc += 4;
    }
}

/*
 * Name:
 *     drawtool_print_string
 * Description:
 *     Prints a string at given position, using actually set font and color.
 * Input:
 *     ppos *:  Pointer on SDLGL_RECT_T, holding drawing position
 *     ptext *: Text to print.
 */
void drawtool_print_string(SDLGL_RECT_T *ppos, char *ptext)
{
     drawtool_istring(ppos, ptext, 0);
}

/*
 * Name:
 *     drawtool_drawfields
 * Description:
 *     Draw all standard fields for SDLGL. Uses the colors defined in 'DRAWTOOL_STYLE_T'
 *     Draws all kind of fields defined with 'SDLGL_TYPE_...'
 *     Calls 'drawfunc' if type is not known oder additional drawing is needed from caller
 * Input:
 *     pinput * : Pointer on array of fields to print
 *     drawfunc:  Is called for owner draw functions or draw-hel (e. g. Box-Elements)
 * Output:
 *     None
 */
void drawtool_drawfields(SDLGL_INPUT_T *pinput, DRAWTOOL_USERFUNC_T drawfunc)
{
    SDLGL_RECT_T info[7];
    SDLGL_RECT_T sizerect;
    int text_style;
    char align;


    while(pinput->sdlgl_type > 0)
    {
        switch(pinput->sdlgl_type)
        {
            /* -------- SDLGL-Types for input handling and drawing --------- */
            /*
            #define SDLGL_TYPE_MAP         0x0D
            #define SDLGL_TYPE_MENUBAR
            */
            case SDLGL_TYPE_STD:
                drawtool_ibutton_labelled(&pinput->rect, pinput->pdata, 0, 0);
                break;

            case SDLGL_TYPE_BUTTON:
                drawtool_ibutton_labelled(&pinput->rect, pinput->pdata, 0, 1);
                break;

            case SDLGL_TYPE_MENUBK:
                drawtool_irectangle(&pinput->rect, 1, 0);
                break;

            case SDLGL_TYPE_LABEL:
                drawtool_istring(&pinput->rect, pinput->pdata, 0);
                break;

            case SDLGL_TYPE_MENU:

                sizerect.x = pinput->rect.x;
                sizerect.y = pinput->rect.y;

                text_style = (pinput->fstate & SDLGL_FSTATE_MOUSEOVER) ? 1 : 0;

                if(pinput->fstate & SDLGL_FSTATE_CHECKED)
                {
                    /* Flag, if this is a checked menu point */
                    if(pinput->el_top & pinput->sub_code)
                    {
                        /* Field is checked */
                        drawtool_iprint_char(&sizerect, Button_Chars[4], SDLGL_COL_BLACK);
                        sizerect.x += SDLGDRW_SPECIALDIST;
                    }
                }
                else
                {

                }
                drawtool_ibutton_labelled(&pinput->rect, pinput->pdata, text_style, 2);
                /* drawtool_istring(&sizerect, pinput->pdata, text_style); */
                break;

            case SDLGL_TYPE_TEXT:
                drawtool_iinput_text(pinput);
                break;

            case SDLGL_TYPE_TEXTAREA:
                drawtool_iinput_textarea(pinput);
                break;

            case SDLGL_TYPE_NUMBER:
                align = (pinput->el_top > 0) ? 'C' : 'R';
                drawtool_iprint_value(pinput, &pinput->rect, 1, align);
                break;

            case SDLGL_TYPE_VALUE:
                align = (pinput->el_top > 0) ? 'C' : 'R';
                drawtool_iprint_value(pinput, &pinput->rect, 0, align);
                break;

            case SDLGL_TYPE_SLIBOXV:
                drawtool_isliderboxv(pinput, drawfunc);
                break;

            case SDLGL_TYPE_SLIBOXH:
                drawtool_isliderboxh(pinput, drawfunc);
                break;

            case SDLGL_TYPE_SLIDERV:
                sdlgl_get_drawinfo(pinput, info);
                drawtool_islider(info, 0);
                break;

            case SDLGL_TYPE_SLIDERH:
                sdlgl_get_drawinfo(pinput, info);
                drawtool_islider(info, 1);
                break;

            case SDLGL_TYPE_CHECKBOX:
            case SDLGL_TYPE_RADIO:
                drawtool_ifield_special(pinput);
                break;

            case SDLGL_TYPE_CHOOSEH:
            case SDLGL_TYPE_CHOOSEV:
                /* @TODO: Fixed number of Elements */
                drawtool_idraw_choose(pinput);
                break;

            case SDLGL_TYPE_POINTBAR:
            case SDLGL_TYPE_CHOOSENUM:
            case SDLGL_TYPE_PROGRESSBAR:
                drawtool_ifield_special(pinput);
                break;

            case SDLGL_TYPE_MAP:
                sizerect.x = pinput->el_top;
                sizerect.y = pinput->el_visi;
                sizerect.w = 5;
                sizerect.h = 5;

                if(pinput->val_min > 0 && pinput->val_max > 0)
                {
                    sizerect.w = pinput->rect.w / pinput->val_min;
                    sizerect.h = pinput->rect.h / pinput->val_max;
                }
                sizerect.x *= sizerect.w;
                sizerect.y *= sizerect.h;
            default:
                /* Unknown type, to be drawn by caller  */
                if(drawfunc)
                {
                    drawfunc(pinput, &sizerect, 0);
                }
        }

        pinput++;
    }
}

/*
 * Name:
 *     drawtool_labels_pos
 * Description:
 *     Draw simple labels on the screen, the given X and Y values are added to the
 *     position given in the label-descriptor.
 *     If 'draw_bk' is given, then a background is drawn, using 'draw_w' and 'draw_h'
 * Input:
 *     draw_x, int draw_y: To add to position of label
 *     draw_w, int draw_h: For background rectangle
 *     plabel *:    Pointer on labels to be drawn
 *     draw_bk:     Draw the background yes/no (Idea: Give style for background)
 */
void drawtool_labels_pos(int draw_x, int draw_y, int draw_w, int draw_h, SDLGL_LABEL_T *plabel, char draw_bk)
{
    SDLGL_RECT_T rect;
    char val_buf[101], *pstr;


    rect.w = draw_w;
    rect.h = draw_h;

    if(draw_bk)
    {
        rect.x = draw_x;
        rect.y = draw_y;

        /* Draw a background rectangle */
        drawtool_drawrect_colno(&rect, SDLGL_COL_LIGHTBLUE, 1);
    }

    while(plabel->val_type > 0)
    {
        /*
            @TODO:
            Set font size:  plabel->font_size
            Set draw color: plabel->color_no --> style_no
        */
        /* -- Convert value to string, if needed -- */
        if(plabel->val_type != SDLGL_VAL_STRING)
        {
            sdlgl_valtostr(plabel->val_type, plabel->pdata, val_buf, 100);
            pstr = val_buf;
        }
        else
        {
            pstr = (char *)plabel->pdata;
        }

        /* Now print it at given position */
        rect.x = plabel->pos_x + draw_x;
        rect.y = plabel->pos_y + draw_y;

        drawtool_istring(&rect, pstr, 0);

        /* Next label to print */
        plabel++;
    }
}

/*
 * Name:
 *     drawtool_labels
 * Description:
 *     Draw simple labels on the screen.
 * Input:
 *     plabel *:    Pointer on labels to be drawn
 */
void drawtool_labels(SDLGL_LABEL_T *plabel)
{
    SDLGL_RECT_T rect;
    char val_buf[101], *pstr;


    while(plabel->val_type > 0)
    {
        /*
            @TODO:
            Set font size:  plabel->font_size
            Set draw color: plabel->color_no --> style_no
        */
        /* -- Convert value to string, if needed -- */
        if(plabel->val_type != SDLGL_VAL_STRING)
        {
            sdlgl_valtostr(plabel->val_type, plabel->pdata, val_buf, 100);
            pstr = val_buf;
        }
        else
        {
            pstr = (char *)plabel->pdata;
        }

        /* Now print it at given position */
        rect.x = plabel->pos_x;
        rect.y = plabel->pos_y;

        drawtool_istring(&rect, pstr, 0);

        /* Next label to print */
        plabel++;
    }
}

/****************************** SETFONT ************************	*/

/*
 * Name:
 *     drawtool_set_styleno
 * Function:
 *     Sets the style used for drawing
 * Input:
 *     style_no: Number of style to set
 * Output:
 *     Number of previously used style
 */
void drawtool_set_styleno(int style_no)
{
    if(style_no == 0)
    {
        memcpy(&ActStyle, &BasicStyle, sizeof(DRAWTOOL_STYLE_T));
    }
    else if(style_no == 1)
    {
        memcpy(&ActStyle, &SmallStyle, sizeof(DRAWTOOL_STYLE_T));
    }
}

/*
 * Name:
 *     drawtool_font_add
 * Function:
 *     Sets a font for "TextOut"
 * Input:
 *     font:   Font-Bitmap(s)
 *     fontw:  Width of font
 *     fonth:  Height of font
 *     font_no: Number to use for font
 */
void drawtool_font_add(unsigned char * font, int fontw, int fonth, int font_no)
{
    FONT_T *fptr;


    if(font_no < 0 || font_no > DRAWTOOL_MAXFONT)
    {
    	font_no = 2;
    }

    fptr = &_wFonts[font_no];

    fptr->font  = font;
    fptr->fontw = fontw;
    fptr->fonth = fonth;
}


/*
 * Name:
 *     drawtool_set_drawstyle
 * Function:
 *     Overwrites the 'ActStyle' with the pstyle goven in argument
 * Input:
 *     pstyle *:   Pointer on a struct with pstyle data.
 */
void drawtool_set_drawstyle(DRAWTOOL_STYLE_T *pstyle)
{
    if(pstyle)
    {
        memcpy(&ActStyle, pstyle, sizeof(DRAWTOOL_STYLE_T));
    }
    else
    {
        /* Reset it to default */
        memcpy(&ActStyle, &BasicStyle, sizeof(DRAWTOOL_STYLE_T));
    }
}

