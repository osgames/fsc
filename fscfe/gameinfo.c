/*******************************************************************************
*  GAMEINFO.C                                                                  *
*      - Functions and data for handling of infobar menu				       *
*                                                                              *
*  FREE SPACE COLONISATION                                                     *
*      (c)2002 - 2017 Paul Mueller <muellerp61@bluewin.ch>                     *
*                                                                              *
*  This program is free software; you can redistribute it and/or modify        *
*  it under the terms of the GNU General Public License as published by        *
*  the Free Software Foundation; either version 2 of the License, or           *
*  (at your option) any later version.                                         *
*                                                                              *
*  This program is distributed in the hope that it will be useful,             *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of              *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
*  GNU Library General Public License for more details.                        *
*                                                                              *
*  You should have received a copy of the GNU General Public License           *
*  along with this program; if not, write to the Free Software                 *
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  *
*******************************************************************************/

/*******************************************************************************
* INCLUDES                              				                       *
*******************************************************************************/

#include <string.h>             /* strncpy()            */


#include "sdlgldef.h"
#include "sdlgl.h"


#include "drawtool.h"           /* SDLGL_LABEL_T */
#include "inputdef.h"


#include "../code/game.h"
#include "../code/msg.h"        /*  */
#include "../code/starsys.h"
#include "../code/unitinfo.h"

/*
#include "sdlgltex.h"
#include "fscinit.h"
#include "drawtool.h"

#include "fscmap.h"
#include "improve.h"

#include "nation.h"
#include "outpost.h"

#include "unit.h"

#include "unitrule.h"
*/


/* --- Own header --- */
#include "gameinfo.h"

/*******************************************************************************
* DEFINES                                  				                       *
*******************************************************************************/

/* ----- Different cursors ------ */
#define GAMEINFO_CURSOR_FIX       0
#define GAMEINFO_CURSOR_GREEN     1
#define GAMEINFO_CURSOR_RED       2
#define GAMEINFO_CURSOR_BLUE      3
#define GAMEINFO_CURSOR_OWNERRECT 4
#define GAMEINFO_DRAW_NOTHING     10000

#define GAMEINFO_MSGLINELEN    119
#define GAMEINFO_DEBUGINFOSIZE 256

/* --------- Position of unit orders menu -------- */

#define GAMEINFO_UNITORDER_W 244
#define GAMEINFO_UNITORDER_H 160

#define GAMEINFO_UNITINFO_Y  344
#define GAMEINFO_UNITINFO_H   80     /* Including command */

#define GAMEINFO_MAX_MSG 100



/*******************************************************************************
* TYPEDEFS                                                                     *
*******************************************************************************/

typedef struct
{
    /* ------ Strings always displayed on main screen --------------------- */
    char sectorname[32];    /* Actual chosen sector as text                 */
    char datestr[20];       /* Actual game date as string                   */
    char moneystr[20];      /* Actual amount of money as string             */
    char popvalstr[20];     /* Population as string                         */
    char statisticname[20]; /* Title of statistic screen                    */
}
INPUT_STATE_T;



/*******************************************************************************
* DATA                                    				                       *
*******************************************************************************/

/* === Global info === */
static INPUT_STATE_T InputState;

static SDLGL_LABEL_T GlobalInfo_Labels[] =
{
    { SDLGL_VAL_STRING,   5,  3, InputState.sectorname, 0, 12 },
    { SDLGL_VAL_STRING, 150,  3, InputState.datestr },
    { SDLGL_VAL_STRING, 270,  3, InputState.moneystr },
    { SDLGL_VAL_STRING, 420,  3, InputState.popvalstr },
    { 0 }
};

/* === Info about unit === */
static GAME_UNITINFO_T Unit_Info;

static char Unit_MovesStr[10] = "1/3";
static char Unit_HPStr[10]    = "1/3";
static char Unit_TroopsLevel[30];
static char Unit_ClassName[40];
static char Unit_OrderStr[100];

static SDLGL_LABEL_T Unit_Desc[] =
{
    /* 0: Name of unit */
    { SDLGL_VAL_STRING, 67,  2, Unit_Info.name },
     /* 1: Moves       */
    { SDLGL_VAL_STRING, 30, 55, Unit_MovesStr },
     /* 2: troops / level */
    { SDLGL_VAL_STRING, 155, 55, Unit_TroopsLevel },
    /* 3: Unit Ability name */
    { SDLGL_VAL_STRING, 69, 17, "Class:" },
    { SDLGL_VAL_STRING, 124, 17, Unit_ClassName },
    /* 4: Hitpoints         */
    { SDLGL_VAL_STRING, 69, 30, "HP:" },
    { SDLGL_VAL_STRING, 94, 30, Unit_HPStr },
    /* 5: Attack value      */
    { SDLGL_VAL_STRING, 69, 43, "AT:" },
    { SDLGL_VAL_INT , 94, 43, &Unit_Info.attack },
    /* 6: Defense          */
    { SDLGL_VAL_STRING, 69, 55, "DE:" },
    { SDLGL_VAL_INT, 94, 55, &Unit_Info.defense },
    /* 7: Unit order name  */
    { SDLGL_VAL_STRING, 30, 68, Unit_OrderStr },
    { 0 }
};

/* ==== Input descriptors to fill for display a star system with star and its planets === */
static PLANET_INFO_T Planet_Info;

static SDLGL_INPUT_T StarSys_Input[] =
{
    /* 0: Star at the rightmost position, draw its texture */
    { SDLGL_TYPE_TEXTURE, { 330, 0, 64, 64 }, GUICMD_CHOOSESTAR, 0, NULL, 0, FSC_TEX_STAR, 0, 1 },
    /* 1..5: Planets in Star-System: First a little bit farther from star as other planets */
    { SDLGL_TYPE_TEXTURE, { 250,  0, 64, 64 }, GUICMD_CHOOSEPLANET, 0, NULL, 0, FSC_TEX_PLANET, 0, 0 },
    { SDLGL_TYPE_TEXTURE, { 200,  0, 64, 64 }, GUICMD_CHOOSEPLANET, 0, NULL, 0, FSC_TEX_PLANET, 0, 1 },
    { SDLGL_TYPE_TEXTURE, { 150,  0, 64, 64 }, GUICMD_CHOOSEPLANET, 0, NULL, 0, FSC_TEX_PLANET, 0, 1 },
    { SDLGL_TYPE_TEXTURE, { 100,  0, 64, 64 }, GUICMD_CHOOSEPLANET, 0, NULL, 0, FSC_TEX_PLANET, 0, 1 },
    { SDLGL_TYPE_TEXTURE, {  50,  0, 64, 64 }, GUICMD_CHOOSEPLANET, 0, NULL, 0, FSC_TEX_PLANET, 0, -1 },
    /* #6: And now the description ot hte actual chosen planet / star: Background */
    { SDLGL_TYPE_LABEL, {   0,  0, 60, 64 }, 0, 0, Planet_Info.name  },
    { SDLGL_TYPE_LABEL, {   0, 14, 60, 64 }, 0, 0, "YIELDS" },
    { SDLGL_TYPE_LABEL, {   0, 28, 60, 64 }, 0, 0, "Food:" },
    { SDLGL_TYPE_LABEL, {   0, 28, 60, 64 }, 0, 0, &Planet_Info.yield[0], SDLGL_VAL_INT },
    { SDLGL_TYPE_LABEL, {   0, 42, 60, 64 }, 0, 0, "Resources:" },
    { SDLGL_TYPE_LABEL, {   0, 42, 60, 64 }, 0, 0, &Planet_Info.yield[1], SDLGL_VAL_INT },
    { SDLGL_TYPE_LABEL, {   0, 56, 60, 64 }, 0, 0, "Economy:" },
    { SDLGL_TYPE_LABEL, {   0, 56, 60, 64 }, 0, 0, &Planet_Info.yield[2], SDLGL_VAL_INT },
    { SDLGL_TYPE_LABEL, {   0, 72, 60, 64 }, 0, 0, "Terraform:" },
    { SDLGL_TYPE_LABEL, {   0, 72, 60, 64 }, 0, 0, &Planet_Info.terraform_perc, SDLGL_VAL_INT },
    { 0 }
};

/* === Info about planet === */

/*
static SDLGL_INPUTKEY_T PlanetCmdKeys[] =
{
    { { SDLK_RETURN }, ORDER_ACTION_CANCEL },
    { { 0 } }
};
*/


/* === Other info === */
static char UnitOrder_Names[10 * 32];
static SDLGL_INPUT_T UnitOrder_Input[] =
{
    { SDLGL_TYPE_MENU, { 0, 0, 64, 12 }, GUICMD_UNIT_CANCEL },
    { SDLGL_TYPE_MENU, { 0, 0, 64, 12 }, GUICMD_UNIT_BUILD },
    { SDLGL_TYPE_MENU, { 0, 0, 64, 12 }, GUICMD_UNIT_BUILD },
    { SDLGL_TYPE_MENU, { 0, 0, 64, 12 }, GUICMD_UNIT_BUILD },
    { SDLGL_TYPE_MENU, { 0, 0, 64, 12 }, GUICMD_UNIT_BUILD },
    { SDLGL_TYPE_MENU, { 0, 0, 64, 12 }, GUICMD_UNIT_BUILD },
    { SDLGL_TYPE_MENU, { 0, 0, 64, 12 }, GUICMD_UNIT_BUILD },
    { SDLGL_TYPE_MENU, { 0, 0, 64, 12 }, GUICMD_UNIT_BUILD },
    { SDLGL_TYPE_MENU, { 0, 0, 64, 12 }, GUICMD_UNIT_BUILD },
    { 0 }
};

/*
static MSG_INFO_T PlanetMenuMsg;
static char DebugInfo[GAMEINFO_DEBUGINFOSIZE + 2];
*/
/* Message line for actual message */
/*
static char MsgLine[GAMEINFO_MSGLINELEN + 2];
*/
/* Buffer for 'End-of-Turn' messages */
/*
static int NumMsg;
static MSG_INFO_T Messages[GAMEINFO_MAX_MSG + 2];

static SDLGL_LABEL_T PlanetLabels[10];
static SDLGL_LABEL_T StarInfoLabels[20];

static char TerraformText[] = "Terraform: ";
static char ColonyText[] = "Colony";

static SDLGL_LABEL_T MsgLineLabel[] =
{
    { SDLGL_VAL_STRING,  100,  24, MsgLine, 0, 12 },
    { 0 }
};
*/

/*******************************************************************************
* CODE                                    				                       *
*******************************************************************************/

/* ========================================================================== */
/* =========================== PUBLIC FUNCTION(S) =========================== */
/* ========================================================================== */

/*
 * Name:
 *     gameinfo_add_starsysinfo
 * Description:
 *
 * Input:
 *     player_no: For this player
 *     planet_no: Which planet is chosen ?
 */
void gameinfo_add_starsysinfo(char player_no, int planet_no)
{
    int i;

    /* Wrap color of star around */
    StarSys_Input[0].el_top++;

    if(StarSys_Input[0].el_top > 6) StarSys_Input[0].el_top = 1;

    /* Now set the number of planets and their size */


}

/*
 * Name:
 *     gameinfo_draw_starsys
 * Description:
 *     Draw the information about a star system for test purposes
 * Input:
 *     x, y: At this position on screen
 */
void gameinfo_draw_starsys(int x, int y)
{
    SDLGL_INPUT_T *pinput;
    SDLGL_RECT_T draw_rect;
    int radius;
    int col_no;


    pinput = StarSys_Input;

    while(pinput->sdlgl_type > 0)
    {
        draw_rect.x = pinput->rect.x + x;
        draw_rect.y = pinput->rect.y + y;
        draw_rect.w = pinput->rect.w;
        draw_rect.h = pinput->rect.h;

        /* drawtool_drawrect_colno(&draw_rect, SDLGL_COL_WHITE, 0); */

        /* Now draw the circles */
        if(pinput->val_min == FSC_TEX_STAR)
        {
            col_no = drawtool_get_colorno(4, pinput->el_top);
            radius = (draw_rect.w / 2) - 2;

            drawtool_circle(&draw_rect, radius, col_no, 0x03);
        }
        else if(pinput->val_min == FSC_TEX_PLANET)
        {
            radius = (pinput->el_top < 0) ? 22 : (8 + (4 * pinput->el_top));

            drawtool_circle(&draw_rect, radius, SDLGL_COL_LIGHTGREY, 0);
        }

        pinput++;
    }

    /* drawtool_drawfields(SDLGL_INPUT_T *pinput, NULL); */
}

/*
 * Name:
 *     gameinfo_create_ordermenu
 * Description:
 *     Creates the needed menupoints for given orders in 'porder_list'
 *     Adds a pop-up-menu with possible commands for given unit
 *     on given planet, depending on the units type.
 * Input:
 *     bott_x,
 *     bott_y: Bottom right edge of menu
 *     porder_list *: Pointer on list with orders, may be empty
 */
SDLGL_INPUT_T *gameinfo_create_ordermenu(int bott_x, int bott_y, char *porder_list)
{
    SDLGL_INPUT_T *pinput;
    char *porder_name;


    /* Point on first menu point */
    if(*porder_list)
    {
        porder_name = UnitOrder_Names;
        pinput = UnitOrder_Input;

        /* -- There is at least on e possible order in the list -- */
        /* X-Position is to the left -- First input is 'Cancel' */
        bott_x -= pinput->rect.w;
        bott_y -= pinput->rect.h;

        /* -- Add the 'cancel' command                          -- */
        pinput->sdlgl_type = SDLGL_TYPE_MENU;
        pinput->rect.x = bott_x;
        pinput->rect.y = bott_y;
        pinput->pdata  = "Cancel";      /* @TODO: Take from 'game2str' */
        pinput++;

        while(*porder_list)
        {
            /* @TODO: Take from 'game2str' */
            sprintf(porder_name, "Unit-Order %d", (int)*porder_list);
            /* Position of next menu point */
            bott_y -= pinput->rect.h;

            pinput->sdlgl_type = SDLGL_TYPE_MENU;
            pinput->rect.x     = bott_x;
            pinput->rect.y     = bott_y;
            pinput->pdata      = porder_name;
            pinput->sub_code   = *porder_list;

            pinput++;
            porder_list++;

            /* Next buffer for ored name */
            porder_name += 32;
        }
    }

    /* Sign end of array */
    pinput->sdlgl_type = 0;

    /* ----- Now add menu as popup ------- */
    /* sdlgl_input_popup(infobarHandleUnitCmd, &fields[0], PlanetCmdKeys, 0, 0); */

    /* Return the buffer of the input */
    return UnitOrder_Input;
}

/*
 * Name:
 *     gameinfo_unit
 * Description:
 *     Draws:
 *          - Unit info background
 *          - Unit icon (TO DO?)
 *          - Unit info labels
 *     At given position for actual chosen unit, if unit_no is 0
 * Input:
 *     x,
 *     y:       Add this value to the basis
 *     unit_no: Draw values for this unit
 *     draw_bk: Draw the background rectangle yes/no
 */
void gameinfo_unit(int x, int y, int unit_no, char draw_rect)
{
    if(unit_no != 0)
    {
        /* FIXME: Get Unit info from game, if unit number is given
                Otherwise get info about actual chosen unit        */
        /* Only if there's info to print */
        /* Convert values to strings, where needed */
        sprintf(Unit_MovesStr, "%d/%d", Unit_Info.moves[0], Unit_Info.moves[1]);
        sprintf(Unit_HPStr, "%d/%d", Unit_Info.hp[0], Unit_Info.hp[1]);

        if(Unit_Info.cargo_type > 0)
        {
            /* FIXME: Set different cargos */
            sprintf(Unit_TroopsLevel, "Troops: %d", Unit_Info.cargo_load);
        }
        else
        {
            sprintf(Unit_TroopsLevel, "Level: %d", Unit_Info.level);
        }

        /* === @TODO: Get Strings for order and Class === */
        sprintf(Unit_OrderStr, "Order: %d", Unit_Info.order);
        sprintf(Unit_ClassName, "Ability: %d", Unit_Info.ability);

        drawtool_labels_pos(x, y, GAMEINFO_UNITORDER_W, GAMEINFO_UNITINFO_H, Unit_Desc, draw_rect);
    }
}

/*
 * Name:
 *     gameinfo_draw_globallabels
 * Description:
 *     Draws the labels for general info always needed
 * Input:
 *     None
 */
void gameinfo_draw_globallabels(void)
{
    drawtool_labels(GlobalInfo_Labels);
}
