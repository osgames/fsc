/*******************************************************************************
*   SDLGL.C                      	                                           *
*       A simple Main-Function to separate calls to SDL from the rest of       *
*	the code. Initializes and maintains an OpenGL window                       *
*   Merges inputs for mouse and keyboard                                       *
*                                                                              *
*   Initial code: January 2001, Paul Mueller                                   *
*									                                           *
*   Copyright (C) 2001-2010  Paul M�ller <pmtech@swissonline.ch>               *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*                                                                              *
*                                                                              *
* Last change: 2010-08-01                                                      *
*******************************************************************************/


/******************************************************************************
* INCLUDES                                                                    *
******************************************************************************/

/* Make sure that _MACOS is defined in MetroWerks compilers */

#if defined( __MWERKS__ ) && defined( macintosh )
	#define	_MACOS
#endif


#include <SDL.h>
#include <stdlib.h>     /* malloc() */
#include <memory.h>
#include <ctype.h>      /* isdigit()    */
#include <string.h>     /* strchr()     */


/* --- Include own header -------- */
#include "sdlgl.h"


/******************************************************************************
* DEFINES                                                                     *
******************************************************************************/

#define SDLGL_MAXINPUTSTACK 32   	/* Maximum depth of stack 	        */

#define SDLGL_DBLCLICKTIME  56      /* Ticks                            */
#define SDLGL_TOOLTIPTIME   500     /* Ticks until tooltip-flag is set  */
                                    /* Translation                      */

#define SDLGL_MAXINPVEC     512     /* Maximum input-Vectors            */

/******************************************************************************
* TYPEDEFS                                                                    *
******************************************************************************/

typedef struct
{
    SDLGL_INPUT_T    *pDrawAreas;     /* Returned to user for drawing     */
    SDLGL_INPUT_T    *pInputAreas;    /* Areas checked for input          */
    SDLGL_INPUTKEY_T *pCmdKeys;	      /* Shortcut keys	                  */
    SDLGL_DRAWFUNC   DrawFunc;        /* Function to use for drawing      */
    SDLGL_INPUTFUNC  InputFunc;       /* Function for input translation   */
    SDLGL_INPUT_T    *pInputFocus;    /* This Area has the focus          */
    int num_total;                    /* Number of fields in 'pDrawAreas' */
    int num_remaining;                /* Remaining fields for var. Areas  */
    int name;                         /* 'Name' on stack for later use    */
                                      /* Remove back to this 'name'       */
}
SDLGL_INPUTSTACK_T;

/******************************************************************************
* FORWARD DECLARATIONS                                                        *
******************************************************************************/

static void sdlgl_idefault_drawfunc(SDLGL_INPUT_T *fields, SDLGL_EVENT_T *pevent);
static int  sdlgl_idefault_inputfunc(SDLGL_EVENT_T *pevent);

/******************************************************************************
* DATA                                                                        *
******************************************************************************/

static GLubyte stdcolors[256 * 4] =
{
    0x00, 0x00, 0x00, 0x00,  0x00, 0x00, 0xAA, 0xFF,  0x00, 0xAA, 0x00, 0xFF, /* 0..2 */
    0x00, 0xAA, 0xAA, 0xFF,  0xAA, 0x00, 0x00, 0xFF,  0xAA, 0x00, 0xAA, 0xFF,
    0xAA, 0xAA, 0x00, 0xFF,  0xAA, 0xAA, 0xAA, 0xFF,  0x55, 0x55, 0x55, 0xFF,
    0x55, 0x55, 0xFF, 0xFF,  0x55, 0xFF, 0x55, 0xFF,  0x55, 0xFF, 0xFF, 0xFF,
    0xFF, 0x55, 0x55, 0xFF,  0xFF, 0x55, 0xFF, 0xFF,  0xFF, 0xFF, 0x55, 0xFF,
    0xFF, 0xFF, 0xFF, 0xFF,  0xFC, 0xC8, 0x64, 0xFF,  0xF0, 0x48, 0x44, 0xFF, /* 15..17 */
    0xDC, 0x30, 0x2C, 0xFF,  0xCC, 0x1C, 0x18, 0xFF,  0xB8, 0x08, 0x08, 0xFF,
    0xFC, 0x64, 0x64, 0xFF,  0x8C, 0x04, 0x14, 0xFF,  0x70, 0x08, 0x20, 0xFF,
    0x54, 0x08, 0x24, 0xFF,  0x38, 0x08, 0x08, 0xFF,  0x1C, 0x04, 0x04, 0xFF,
    0xFC, 0xE4, 0xA0, 0xFF,  0xF4, 0xCC, 0x78, 0xFF,  0xE4, 0xBC, 0x68, 0xFF,
    0xD0, 0xA8, 0x54, 0xFF,  0xC0, 0x94, 0x48, 0xFF,  0xE4, 0x90, 0x74, 0xFF,
    0xD0, 0x78, 0x64, 0xFF,  0xBC, 0x64, 0x54, 0xFF,  0xA0, 0x4C, 0x44, 0xFF,
    0x88, 0x38, 0x38, 0xFF,  0x78, 0x34, 0x34, 0xFF,  0x64, 0x28, 0x2C, 0xFF,
    0x50, 0x20, 0x24, 0xFF,  0x40, 0x18, 0x1C, 0xFF,  0x00, 0x18, 0x0C, 0xFF, /* 39..41 */
    0xFC, 0xE8, 0xC0, 0xFF,  0xB4, 0x88, 0x3C, 0xFF,  0xF0, 0xEC, 0x44, 0xFF,
    0xE0, 0xD8, 0x30, 0xFF,  0xD4, 0xC4, 0x20, 0xFF,  0xC8, 0xB4, 0x10, 0xFF,
    0xC4, 0xE0, 0x8C, 0xFF,  0xA8, 0xCC, 0x78, 0xFF,  0x90, 0xBC, 0x68, 0xFF,
    0x78, 0xA8, 0x58, 0xFF,  0x60, 0x98, 0x4C, 0xFF,  0x4C, 0x84, 0x3C, 0xFF,
    0x38, 0x74, 0x34, 0xFF,  0x28, 0x64, 0x28, 0xFF,  0xD8, 0xD8, 0xFC, 0xFF,
    0xCC, 0xCC, 0xFC, 0xFF,  0xBC, 0xBC, 0xF4, 0xFF,  0xB8, 0x94, 0x0C, 0xFF, /* 57..59 */
    0xA4, 0x84, 0x0C, 0xFF,  0x90, 0x74, 0x08, 0xFF,  0x7C, 0x64, 0x08, 0xFF,
    0x68, 0x50, 0x04, 0xFF,  0xD0, 0xFC, 0xD0, 0xFF,  0x4C, 0xE8, 0x40, 0xFF,
    0x44, 0xD4, 0x34, 0xFF,  0x40, 0xC0, 0x28, 0xFF,  0x3C, 0xB0, 0x1C, 0xFF,
    0xA0, 0xFC, 0xA0, 0xFF,  0x00, 0x8C, 0x10, 0xFF,  0x04, 0x74, 0x18, 0xFF,
    0x04, 0x5C, 0x20, 0xFF,  0x04, 0x44, 0x1C, 0xFF,  0x04, 0x2C, 0x18, 0xFF,
    0xF8, 0xCC, 0xCC, 0xFF,  0xF4, 0xAC, 0xAC, 0xFF,  0xF0, 0x90, 0x90, 0xFF,
    0xFC, 0x74, 0x74, 0xFF,  0xCC, 0xEC, 0x90, 0xFF,  0xFC, 0xAC, 0x68, 0xFF,
    0xF0, 0xA0, 0x58, 0xFF,  0xD8, 0x90, 0x50, 0xFF,  0xC4, 0x84, 0x48, 0xFF,
    0xB0, 0x78, 0x40, 0xFF,  0x9C, 0x68, 0x34, 0xFF,  0x8C, 0x58, 0x30, 0xFF,  /* 84..86 */
    0x7C, 0x48, 0x2C, 0xFF,  0x68, 0x38, 0x2C, 0xFF,  0x54, 0x30, 0x24, 0xFF,  /* 87..89 */
    0x40, 0x20, 0x20, 0xFF,  0xFC, 0xA0, 0xCC, 0xFF,  0xE8, 0x74, 0xB8, 0xFF,  /* 90..92 */
    0xD8, 0x4C, 0xAC, 0xFF,  0xC4, 0x2C, 0xA8, 0xFF,  0xB4, 0x10, 0xA8, 0xFF,  /* 93..95 */
    0xD8, 0xB0, 0x8C, 0xFF,  0xC8, 0xA0, 0x80, 0xFF,  0xB8, 0x94, 0x78, 0xFF,  /* 96..98 */
    0xA8, 0x88, 0x6C, 0xFF,  0x9C, 0x7C, 0x64, 0xFF,  0x8C, 0x70, 0x5C, 0xFF,  /* 99..102 */
    0x7C, 0x64, 0x50, 0xFF,  0x6C, 0x54, 0x44, 0xFF,  0x5C, 0x44, 0x38, 0xFF,  /* 102..104*/
    0x4C, 0x38, 0x2C, 0xFF,  0xDC, 0x58, 0x1C, 0xFF,  0xF8, 0xAC, 0xD0, 0xFF,  /* 105..107 */
    0x84, 0x04, 0x94, 0xFF,  0x68, 0x08, 0x80, 0xFF,  0x50, 0x08, 0x6C, 0xFF,
    0x3C, 0x0C, 0x58, 0xFF,  0xFC, 0xB4, 0x2C, 0xFF,  0xEC, 0xA4, 0x24, 0xFF,
    0xDC, 0x94, 0x1C, 0xFF,  0xCC, 0x88, 0x18, 0xFF,  0xBC, 0x7C, 0x14, 0xFF,
    0xA4, 0x6C, 0x1C, 0xFF,  0x94, 0x5C, 0x0C, 0xFF,  0x7C, 0x50, 0x18, 0xFF,
    0x64, 0x40, 0x18, 0xFF,  0x50, 0x34, 0x18, 0xFF,  0x38, 0x24, 0x14, 0xFF,
    0x24, 0x18, 0x0C, 0xFF,  0x08, 0x04, 0x00, 0xFF,  0xF4, 0xC0, 0xA0, 0xFF,
    0xE0, 0xA0, 0x84, 0xFF,  0xCC, 0x84, 0x6C, 0xFF,  0xAC, 0xB4, 0xE0, 0xFF,
    0x9C, 0xA0, 0xD4, 0xFF,  0x88, 0x90, 0xCC, 0xFF,  0x7C, 0x80, 0xC4, 0xFF,
    0x64, 0x68, 0xB4, 0xFF,  0x50, 0x54, 0xA4, 0xFF,  0x3C, 0x40, 0x94, 0xFF,
    0x2C, 0x30, 0x88, 0xFF,  0xE4, 0xC8, 0x38, 0xFF,  0xC8, 0xA0, 0x34, 0xFF,  /* 142..144 */
    0xAC, 0x7C, 0x30, 0xFF,  0x90, 0x60, 0x2C, 0xFF,  0x78, 0x48, 0x28, 0xFF,  /* 145..147 */
    0xC8, 0x8C, 0x68, 0xFF,  0xA8, 0x78, 0x54, 0xFF,  0x8C, 0x60, 0x44, 0xFF,
    0x50, 0xC4, 0xFC, 0xFF,  0x50, 0xA4, 0xFC, 0xFF,  0x50, 0x88, 0xFC, 0xFF,
    0x50, 0x6C, 0xFC, 0xFF,  0xA4, 0xFC, 0xFC, 0xFF,  0x38, 0x3C, 0xDC, 0xFF,
    0x20, 0x28, 0xC8, 0xFF,  0x50, 0xE0, 0xFC, 0xFF,  0x00, 0x04, 0x80, 0xFF,
    0x00, 0x04, 0x58, 0xFF,  0x00, 0x04, 0x30, 0xFF,  0x28, 0x10, 0x10, 0xFF,
    0x10, 0x04, 0x04, 0xFF,  0x98, 0x68, 0x4C, 0xFF,  0x7C, 0x50, 0x3C, 0xFF,
    0x60, 0x3C, 0x2C, 0xFF,  0xF0, 0xE8, 0xE8, 0xFF,  0xE0, 0xD8, 0xD0, 0xFF,
    0xD0, 0xC4, 0xBC, 0xFF,  0xBC, 0xAC, 0xA4, 0xFF,  0xA4, 0x98, 0x90, 0xFF,
    0x8C, 0x80, 0x80, 0xFF,  0x74, 0x70, 0x70, 0xFF,  0x5C, 0x5C, 0x58, 0xFF,
    0x44, 0x44, 0x44, 0xFF,  0x2C, 0x2C, 0x2C, 0xFF,  0x18, 0x18, 0x18, 0xFF,
    0x0C, 0x0C, 0x0C, 0xFF,  0x00, 0x00, 0x1C, 0xFF,  0xF4, 0xA4, 0x74, 0xFF,
    0xDC, 0x94, 0x5C, 0xFF,  0xC8, 0x84, 0x48, 0xFF,  0xE0, 0xE0, 0xE0, 0xFF,
    0xD0, 0xD0, 0xD8, 0xFF,  0xC0, 0xC4, 0xD0, 0xFF,  0xB4, 0xB8, 0xC8, 0xFF,
    0xA4, 0xA4, 0xB8, 0xFF,  0x94, 0x94, 0xAC, 0xFF,  0x7C, 0x7C, 0x98, 0xFF,
    0x6C, 0x6C, 0x88, 0xFF,  0x58, 0x58, 0x74, 0xFF,  0x44, 0x44, 0x60, 0xFF,
    0x34, 0x34, 0x50, 0xFF,  0x24, 0x24, 0x3C, 0xFF,  0x18, 0x18, 0x2C, 0xFF,
    0xB0, 0x74, 0x40, 0xFF,  0x94, 0x6C, 0x40, 0xFF,  0x7C, 0x60, 0x3C, 0xFF,
    0xB8, 0x98, 0x98, 0xFF,  0xAC, 0x88, 0x84, 0xFF,  0x98, 0x74, 0x6C, 0xFF,
    0x88, 0x60, 0x58, 0xFF,  0x74, 0x4C, 0x44, 0xFF,  0x64, 0x3C, 0x34, 0xFF,
    0x54, 0x30, 0x28, 0xFF,  0x3C, 0x20, 0x18, 0xFF,  0x28, 0x14, 0x0C, 0xFF,
    0x14, 0x08, 0x04, 0xFF,  0x08, 0x04, 0x00, 0xFF,  0x00, 0x00, 0x0C, 0xFF,
    0xFC, 0xFC, 0xFC, 0xFF,  0x70, 0x58, 0x38, 0xFF,  0x60, 0x48, 0x28, 0xFF,
    0x50, 0x3C, 0x1C, 0xFF,  0xD4, 0x84, 0x60, 0xFF,  0xC0, 0x78, 0x58, 0xFF,
    0xB8, 0x6C, 0x4C, 0xFF,  0xAC, 0x64, 0x44, 0xFF,  0x9C, 0x5C, 0x40, 0xFF,
    0x8C, 0x54, 0x3C, 0xFF,  0x80, 0x50, 0x38, 0xFF,  0x78, 0x48, 0x34, 0xFF,
    0x6C, 0x40, 0x2C, 0xFF,  0x60, 0x38, 0x24, 0xFF,  0x54, 0x2C, 0x1C, 0xFF,
    0x4C, 0x28, 0x18, 0xFF,  0xFC, 0xFC, 0xFC, 0xFF,  0xFC, 0xFC, 0xFC, 0xFF,
    0xFC, 0xFC, 0xFC, 0xFF,  0xFC, 0xFC, 0xFC, 0xFF,
    /* ---------- */
    0xCC, 0xF0, 0xD0, 0xFF,  0xBC, 0xDC, 0xBC, 0xFF,  0xB0, 0xC8, 0xAC, 0xFF,
    0xA0, 0xB4, 0x98, 0xFF,  0x88, 0xA0, 0x84, 0xFF,  0x78, 0x8C, 0x74, 0xFF,
    0x64, 0x78, 0x60, 0xFF,  0x54, 0x64, 0x54, 0xFF,  0x40, 0x50, 0x40, 0xFF,
    0x2C, 0x38, 0x2C, 0xFF,  0x14, 0x18, 0x14, 0xFF,  0x08, 0x0C, 0x04, 0xFF,
    0xFC, 0xFC, 0xFC, 0xFF,  0xFC, 0xFC, 0xFC, 0xFF,  0xFC, 0xFC, 0xFC, 0xFF,
    0xFC, 0xFC, 0xFC, 0xFF,  0x84, 0x68, 0x58, 0xFF,  0x7C, 0x44, 0x24, 0xFF,
    0x70, 0x58, 0x48, 0xFF,  0x68, 0x34, 0x14, 0xFF,  0x5C, 0x48, 0x38, 0xFF,
    0x54, 0x2C, 0x0C, 0xFF,  0x48, 0x38, 0x2C, 0xFF,  0x40, 0x20, 0x04, 0xFF,
    0x34, 0x28, 0x20, 0xFF,  0x2C, 0x14, 0x00, 0xFF,  0x20, 0x18, 0x14, 0xFF,
    0x1C, 0x0C, 0x00, 0xFF,  0x10, 0x0C, 0x08, 0xFF,  0x0C, 0x04, 0x00, 0xFF,
    0x00, 0x00, 0x00, 0xFF,  0xA8, 0xA8, 0xA8, 0xFF
};

static SDLGL_CONFIG_T mainConfig =
{
    /* If no user input is given, use standard values */
    "sdlgl-library V 0.96 - Build(" __DATE__  ")", /* Caption for window, if any */
    640, 480,			/* Size of screen	                 */
    32,					/* Colordepth of screen	             */
    0,					/* Draw mode: wireframe or filled    */
    0, 					/* Hide mouse, if asked for	         */
    0,					/* Windowed                          */
    1,					/* Use standard color settings       */
    					/* If debugmode yes/no	      	     */
    0,                  /* No Z-Buffer                       */
    SDLGL_DBLCLICKTIME, /* Time for sensing of double clicks */
    640, 480
};

/* -------- Data for inputstack ------------- */
static SDLGL_INPUTKEY_T Default_Cmds[2];
static SDLGL_INPUT_T DefaultArea[2];
static SDLGL_INPUT_T InputMou_Buffer[SDLGL_MAXINPVEC + 2];

static SDLGL_INPUT_T *pFocusArea = DefaultArea; /* Never NULL   */
static SDLGL_EVENT_T ActualEvent;


static SDLGL_INPUTSTACK_T Input_Stack[SDLGL_MAXINPUTSTACK + 2] =
{
    {
        InputMou_Buffer,		   /* No draw-areas defined    */
        InputMou_Buffer,		   /* No inputdesc defined		*/
        Default_Cmds,              /* No shortcut keys defined	*/
        sdlgl_idefault_drawfunc,
        sdlgl_idefault_inputfunc,
        &DefaultArea[0],
        SDLGL_MAXINPVEC,
        SDLGL_MAXINPVEC,
        0
    },
    {
        InputMou_Buffer,		        /* No draw-vectors defined      */
        InputMou_Buffer,		        /* No inputdesc defined		    */
        Default_Cmds,              /* No shortcut keys defined		*/
        sdlgl_idefault_drawfunc,
        sdlgl_idefault_inputfunc,
        &DefaultArea[0],
        SDLGL_MAXINPVEC,
        SDLGL_MAXINPVEC,
        0
    }
};

static int InputStackIdx = 0;  /* StackIndex 0 is the default handler */
static char Edit_Buffer[30];
static char *pEditBuf;


/*******************************************************************************
*  CODE									                                       *
*******************************************************************************/

/*
 * Name:
 *     sdlgl_istrtoval
 * Description:
 *     Converts the given string to a pvalue and returns it in argument 'pvalue'
 * Input:
 *     type:       Type of pvalue given in 'pvalue'
 *     pval_str *: Pointer on pvalue in string format
 *     pvalue *:   Pointer on pvalue to return the converted pvalue in
 *     val_len:    length, if string
 */
static void sdlgl_istrtoval(char type, char *pval_str, void *pvalue, int val_len)
{
    int   ivalue;
    float fvalue;
    char *pstr;


    if(pval_str[0] != 0)
    {
        if(type == SDLGL_VAL_STRING || type == SDLGL_VAL_ONECHAR)
        {
            pstr = (char *)pvalue;

            while(*pstr == ' ')
            {
                pstr++;
            }

            if(type == SDLGL_VAL_STRING)
            {
                strncpy(pstr, pval_str, val_len);
                pstr[val_len] = 0;
            }
            else
            {
                *pstr = *pval_str;
            }
        }
        else
        {
            /* It's a pvalue */
            sscanf(pval_str, "%d", &ivalue);

            switch(type)
            {
                case SDLGL_VAL_CHAR:
                    *(char *)pvalue = (char)ivalue;
                    break;

                case SDLGL_VAL_UCHAR:
                    *(unsigned char *)pvalue = (unsigned char)ivalue;
                    break;

                case SDLGL_VAL_SHORT:
                    *(short int *)pvalue = (short int)ivalue;
                    break;

                case SDLGL_VAL_USHORT:
                    *(unsigned short int *)pvalue = (unsigned short int)ivalue;
                    break;

                case SDLGL_VAL_INT:
                    *(int *)pvalue = (int)ivalue;
                    break;

                case SDLGL_VAL_UINT:
                    *(unsigned int *)pvalue = (unsigned int)ivalue;
                    break;

                case SDLGL_VAL_FLOAT:
                    sscanf(pval_str, "%f", &fvalue);
                    *(float *)pvalue = fvalue;
                    break;
            }
        }
    }
}

/*
 * Name:
 *     sdlgl_imouse_inrect
 * Description:
 *     Checks if the given x,y position is in the given mouvec-rect
 * Input:
 *     pinput * : Pointer on pinput to be checked
 *     inpvec: Pointer on inputvec which holds the rectangle to check
 * Output:
 *     1: if Position in rect
 *     0: if not
 *     pevent: the xrel + yrel are set to the relative position in the
 *		 checked rectangle.
 */
static int sdlgl_imouse_inrect(SDLGL_INPUT_T *pinput, SDLGL_EVENT_T *pevent)
{
    int relx,
        rely;		/* Relative position */


    /* First check horizontally */
    relx = pevent->mou.x - pinput->rect.x;

    if(relx >= 0)
    {
        /* Is right of X 		*/
        if(relx > pinput->rect.w)
        {
            /* Right side out of Rectangle */
            return 0;
        }

        /* Now check vertically */
        rely = pevent->mou.y - pinput->rect.y;

    	if(rely >= 0)
    	{
            /* Is lower of Y-Pos */
            if(rely <= pinput->rect.h)
            {
                 /* Is in Rectangle   */
                pevent->mou.x = relx;
                pevent->mou.y = rely;

            	return 1;
            }
    	}
    }

    return 0;
}

/*
 * Name:
 *     sdlgl_iinput_find_type
 * Description:
 *     Finds first occurence of pinput with given 'typeno' in given 'fields'.
 *     If 'fields' == 0 then use 'display' - fields.
 *     If not found, a pointer on a pinput with the type == 0 is returned
 *     (poniter on end of array).
 * Input:
 *     sdlgl_type: Number of sdlgl_type to find: 0: Don't check
 *     pfields *:   Where to start searching
 */
SDLGL_INPUT_T *sdlgl_iinput_find_type(char sdlgl_type, SDLGL_INPUT_T *pfields)
{
    if(pfields)
    {
        while(pfields->sdlgl_type != 0)
        {
            if(pfields->sdlgl_type == sdlgl_type)
            {
                return pfields;
            }

            pfields++;
        }
    }
    /* Point on end of array, if not found */
    return pfields;
}

/*
 * Name:
 *     sdlgl_iedit_stdkey
 * Description:
 *     Processes the key input for an edit pinput, depending on its type
 * Input:
 *     pinput *: Pointer on field to handle
 *     keycode:  To handle
 *     modflags;
 * Output:
 *     > 0: Valid char, changed to upper, if needed
 */
static char sdlgl_iedit_stdkey(SDLGL_INPUT_T *pinput, int keycode, int modflags)
{
    char edit_char;


    if(isprint(keycode))
    {
        edit_char = (char)keycode;

        switch(pinput->val_type)
        {
            case SDLGL_VAL_CHAR:
            case SDLGL_VAL_UCHAR:
            case SDLGL_VAL_SHORT:
            case SDLGL_VAL_USHORT:
            case SDLGL_VAL_INT:
            case SDLGL_VAL_UINT:
                if(isdigit(edit_char))
                {
                    return edit_char;
                }
                break;

            case SDLGL_VAL_FLOAT:
                if(isdigit(edit_char) || edit_char == '.')
                {
                    return edit_char;
                }
                break;

            case SDLGL_VAL_ONECHAR:
            case SDLGL_VAL_STRING:
                if((keycode >= SDLK_a) && (keycode <= SDLK_z) && (modflags & KMOD_SHIFT))
                {
                    /* Change to capitals */
                    edit_char -= (char)32;
                }

                if(pinput->sub_code == SDLGL_VAL_ONECHAR)
                {
                    if(isalpha(keycode))
                    {
                        return edit_char;
                    }

                    return 0;
                }
                return edit_char;
        }
    }

    return 0;
}

/*
 * Name:
 *     sdlgl_iedit_process_key
 * Description:
 *     Processes the key input for an edit pinput.
 * Input:
 *     pinput *: Pointer on pinput to handle
 *     ptext *:  To edit
 *     keycode:  To process
 *     modflags: To check  ==> pevent->modflags
 */
static int sdlgl_iedit_process_key(SDLGL_INPUT_T *pinput, char *ptext, int keycode, int modflags)
{
    char *lp;
    char edit_char;
    int  size;


    switch(keycode)
    {
    	case SDLK_INSERT:
            /* editfld->editmode ^= 1; */	/* Switch the insert mode flag */
            pinput->fstate ^= SDLGL_FSTATE_EDITINS;
            return 0;

        case SDLK_LEFT:
            if(pinput->el_top > 0)
            {
            	pinput->el_top--;	/* Move the cursor left	       */
            }
            return 0;

        case SDLK_RIGHT:
            if(pinput->el_top < (pinput->val_max - 1))
            {
            	/* Don't move behind the end of string */
                if(ptext[pinput->el_top] != 0)
                {
                    pinput->el_top++;
                }
            }
            return 0;

        case SDLK_HOME:
            if(modflags)
            {
            	/* Delete the input-field */
                ptext[0] = 0;
            }

            /* Move the cursor home */
            pinput->el_top = 0;
            return 1;

        case SDLK_END:
            pinput->el_top = (short int)strlen(ptext);

            if(pinput->el_top > (pinput->val_max - 1))
            {
               pinput->el_top = (short int)(pinput->val_max - 1);
            }
            return 0;

        case SDLK_BACKSPACE:
            /* Remove the char if at end of string */
            if(pinput->el_top > 0)
            {
                if(ptext[pinput->el_top] == 0 || ptext[pinput->el_top + 1] == 0)
                {
                    /* Delete this char */
                    ptext[pinput->el_top] = 0;
                }

                if(pinput->el_top < (char)(pinput->code - 1))
                {
                    pinput->el_top--;
                }
            }

        case SDLK_DELETE:
            lp   = ptext + pinput->el_top;
            /* Including '\0' at end of string */
            size = strlen(lp + 1) + 1;

            memmove(lp, lp + 1, size);
            return 1;

        default:
            edit_char = sdlgl_iedit_stdkey(pinput, keycode, modflags);

            if(edit_char > 0)
            {
                ptext[pinput->el_top] = edit_char;
                sdlgl_iedit_process_key(pinput, ptext, SDLK_RIGHT, 0);

                return 1;
            }
            break;
    }

    return 0;
}

/*
 * Name:
 *     sdlgl_ikeyboard_translate_input
 * Name:
 *     Checks the key on validity, including check for modifier keys.
 *     If the key in SDLGL_EVENT_T is the one looked for, the SDLGL_EVENT_T
 *     struct is modified to type "SDLGL_INPUT_COMMAND".
 * Input:
 *     pevent:     Pointer on SDLGL_INPUT_T which holds the actual key and the
 *  	          key modifiers.
 *     pressed:   yes/no
 * Output:
 *     > 0 if a valid command was generated
 */
static int sdlgl_ikeyboard_translate_input(SDLGL_EVENT_T *pevent, int pressed)
{
    SDLGL_INPUTKEY_T *psk, *psk_list;
    int i;
    unsigned char mask;


    /* 1) Check if there are command keys to translate */
    psk_list = Input_Stack[InputStackIdx].pCmdKeys;

    if(! psk_list)
    {
        return 0;
    }

    /* 2) Check if there's a code to translate */
    if(! pevent->sdlcode)
    {
        return 0;
    }

    /* 3) Do manage 'pressed'  */
    pevent->pressed = (char)pressed;

    if(pevent->pressed)
    {
        /* 3a) Fill in the keymask everywhere it is used    */
        /* Check from 'right' to 'left' for combination     */
        for(i = 1, mask = 0x02; i >= 0; i--, mask >>= 1)
        {
            psk = psk_list;

            while(psk->keys[0] > 0)
            {
                if(psk->keys[i] == pevent->sdlcode)
                {
                    psk->pressed |= mask;

                    /* Found a candidate, check combined keys */
                    if(psk->pressed == psk->keymask)
                    {
                        /* Set the command */
                        pevent->code       = psk->code;
                        pevent->sub_code   = psk->sub_code;

                        /* ---- Throw away other candidates --- */
                        psk--;

                        while(psk >= psk_list)
                        {
                            if(psk->keys[i] == pevent->sdlcode)
                            {
                                psk->pressed &= (char)~mask;
                            }

                            psk--;
                        }

                        return 1;
                    }
                }

                psk++;
            } /* while(psk->keys[i] > 0) */

        }

    }
    else
    {
        /* Find 'best' key released (most flags)    */
        psk = psk_list;

        while(psk->keys[0] > 0)
        {
            if(psk->release_code)
            {
                for(i = 0; i < 2; i++)
                {
                    if(psk->keys[i] > 0)
                    {
                        if(psk->keys[i] == pevent->sdlcode)
                        {
                            if(psk->pressed == psk->keymask)
                            {
                                /* Send code for released key */
                                pevent->code       = psk->code;
                                pevent->sub_code   = psk->release_code;
                                break;
                            }
                        }
                    }
                }
            }

            psk++;
        }

        /* Remove the key everywhere in array */
        psk = psk_list;

        while(psk->keys[0] > 0)
        {
            for(i = 0, mask = 0x01; i < 2; i++, mask <<= 1)
            {
                if(psk->keys[i] == pevent->sdlcode)
                {
                    psk->pressed &= (char)~mask;
                }
            }

            psk++;
        }
    }

    if(pevent->code == 0)
    {
        return 0;
    }

    return 1;
}

/* Name:
 *     sdlgl_iinput_move_focus
 * Description:
 *     Changes the focus to new given Focus-Pointer, if not null
 *     If there is no input base, its set to default values
 * Input:
 *     pinput *:  Pointer on new Input-Area to set focus to
 *     focus_add: For changing focus bei Keyboard-Input
 */
static void sdlgl_iinput_move_focus(SDLGL_INPUT_T *pinput, int focus_add)
{
    SDLGL_INPUT_T *pbase;


    /* 1) Get list base  of fields */
    pbase = Input_Stack[InputStackIdx].pInputAreas;

    if(! pbase)
    {
        /* No fields available at all */
        Input_Stack[InputStackIdx].pInputFocus = InputMou_Buffer;
        pFocusArea = InputMou_Buffer;
        return;
    }

    /* 2: Remove focs from actual field */
    pFocusArea->fstate &= (unsigned char)(~SDLGL_FSTATE_HASFOCUS);

    if(pFocusArea->sdlgl_type == SDLGL_TYPE_NUMBER)
    {
        /* Return the internal edited value string as a value */
        sdlgl_istrtoval(pFocusArea->val_type, Edit_Buffer, pFocusArea->pdata, 29);
    }

    if(pinput)
    {
        Input_Stack[InputStackIdx].pInputFocus = pinput;
        pFocusArea = pinput;
    }
    else if(focus_add != 0)
    {
        if(focus_add > 0)
        {
            pFocusArea++;

            if(pFocusArea->sdlgl_type == 0)
            {
                /* Wrap around */
                pFocusArea = pbase;
            }
        }
        else if(focus_add < 0)
        {

            if(pFocusArea == pbase)
            {
                /* Wrap around */
                pFocusArea = sdlgl_iinput_find_type(0, pbase);
            }
            else
            {
                pFocusArea--;
            }
        }
    }

    /* -- Set the focus -- */
    pFocusArea->fstate |= SDLGL_FSTATE_HASFOCUS;

    /* -- Now do additional work for handling input */
    switch(pFocusArea->sdlgl_type)
    {
        case SDLGL_TYPE_TEXT:
            /* Set the pointer on the edit-Buffer */
            pEditBuf = (char *)pFocusArea->pdata;
            sdlgl_iedit_process_key(pFocusArea, pEditBuf, SDLK_END, 0);
            break;

        case SDLGL_TYPE_NUMBER:
            /* -- Use internal bufferfor editiing values */
            pEditBuf = Edit_Buffer;
            sdlgl_valtostr(pFocusArea->val_type, pFocusArea->pdata, Edit_Buffer, 29);
            sdlgl_iedit_process_key(pFocusArea, pEditBuf, SDLK_END, 0);
            break;
    }
}


/* Name:
 *     sdlgl_iinput_switch_bit
 * Description:
 *     Switches the flag in value pointed on by 'pinput->pdata'
 *     'pinput->val_max': Holds the maximum number of bits in array
 *     'pinput->subcode' holds the number of the flag to be switched
 * Input:
 *     pinput *: Pointer on field to handle
 */
static void sdlgl_iinput_switch_bit(SDLGL_INPUT_T *pinput)
{
    char sub_bit;
    int  len, i, bit_index;
    char *pvalue;


    pvalue    = (char *)pinput->pdata + (pinput->sub_code / 8);
    len       = (pinput->val_max / 8) + ((pinput->val_max % 8) > 0) ? 1 : 0;
    bit_index = pinput->sub_code / 8;
    sub_bit   = ((unsigned char)(0x01 << (char)(pinput->sub_code & 0x07)));

    if(pinput->sdlgl_type == SDLGL_TYPE_CHECKBOX)
    {
        /* Switch bit in array, onlysingle one possible */
        pvalue[bit_index] ^= sub_bit;
        pinput->fstate &= ~SDLGL_FSTATE_CHECKED;
        pinput->fstate |= (pvalue[bit_index] & sub_bit) ? SDLGL_FSTATE_CHECKED : 0x00;
    }
}

/* Name:
 *     sdlgl_iinput_set_value
 * Description:
 *     Writes the given value to 'pinput->pdata' if value
 *     is between 'pinput->val_min' and 'pinput->val_max'
 *     Returns the value in the format given by 'pinput->val_type'
 *     SUPPORTS JUST SIGEND VALUES!
 * Input:
 *     pinput *: Pointer on field to handle
 *     value:    Value to write into 'pinput->pdata'
 */
static void sdlgl_iinput_set_value(SDLGL_INPUT_T *pinput, int value)
{
    char *pvalue;


    if(pinput)
    {
        if(pinput->pdata)
        {
            if(value >= pinput->val_min && value <= pinput->val_max)
            {
                pvalue = (char *)pinput->pdata;

                switch(pinput->val_type)
                {
                    case SDLGL_VAL_CHAR:
                        *(char *)pvalue = (char)value;
                        break;

                    case SDLGL_VAL_SHORT:
                        *(short int *)pvalue = (short int)value;
                        break;

                    case SDLGL_VAL_INT:
                        *(int *)pvalue = (int)value;
                        break;
                }
            }
        }
    }
}

/* Name:
 *     sdlgl_iinput_slider_calc
 * Description:
 *     Changes the value of 'pinput->el_top', if its between 'pinput->val_min/val_max'
 * Input:
 *     pinput *:    Change value for this field
 *     sli_len:     Total Length of slider to check
 *     sli_pos:     pevent->mou.x
 *     button_size: Size of button at End of sli...__CHOOSE... Only elements from [1 .. max_value]
 */
static void sdlgl_iinput_slider_calc(SDLGL_INPUT_T *pinput, int sli_len, int sli_pos, int button_size)
{
    int value, val_max;


    /* Get the value */
    if(sdlgl_input_get_value(pinput, &value))
    {
        /* If its a valid value */
        val_max = pinput->val_max;

        if(pinput->sdlgl_type == SDLGL_TYPE_CHOOSEH || pinput->sdlgl_type == SDLGL_TYPE_CHOOSEV)
        {
            val_max++;
        }

        if(sli_pos < button_size)
        {
            /* Decrement the current element */
            if(value > pinput->val_min)
            {
                value--;
            }
        }
        else if(sli_pos > (sli_len - button_size))
        {
            /* Increment the current element */
            if(value < val_max)
            {
               value++;
            }
        }
        else
        {
            /* Now calculate number of the chosen element */
            sli_pos -= button_size;
            sli_len -= (2 * button_size);

            value = pinput->val_max * sli_pos / sli_len;

            if(pinput->sdlgl_type == SDLGL_TYPE_CHOOSEH || pinput->sdlgl_type == SDLGL_TYPE_CHOOSEV)
            {
                /* The value that can be chosen starts with 1 */
                value++;
            }
        }

        sdlgl_iinput_set_value(pinput, value);
    }
}

/* Name:
 *     sdlgl_iinput_slider
 * Description:
 *     Switches the flag in value pointed on by 'pinput->pdata'
 *     'pinput->val_max': Holds the maximum number of bits in array
 *     'pinput->subcode' holds the number of the flag to be switched

 * Input:
 *     pinput *: Pointer on field to handle
 *     pevent *: Holding needed info
 *     is_hor:   Is horizontal slider
 */
static void sdlgl_iinput_slider(SDLGL_INPUT_T *pinput, SDLGL_EVENT_T *pevent, char is_hor)
{
    int button_size, sli_len, sli_pos;


    if(pinput)
    {
        if(is_hor)
        {
            button_size = (pinput->rect.h < 16) ? pinput->rect.h : 16;
            sli_len     = pinput->rect.w;
            sli_pos     = pevent->mou.x;
        }
        else
        {
            button_size = (pinput->rect.w < 16) ? pinput->rect.w : 16;
            sli_len     = pinput->rect.h;
            sli_pos     = pevent->mou.y;
        }

        sdlgl_iinput_slider_calc(pinput, sli_len, sli_pos, button_size);
    }
}


/* Name:
 *     sdlgl_iinput_sliderbox
 * Description:
 *     Switches the flag in value pointed on by 'pinput->pdata'
 *     'pinput->val_max': Holds the maximum number of bits in array
 *     'pinput->subcode' holds the number of the flag to be switched
 *     The button size for sensing the mouse is always 18
 * Input:
 *     pinput *: Pointer on field to handle
 *     pevent *: Holding needed info
 *     is_hor:   IS horizontal slider
 */
static void sdlgl_iinput_sliderbox(SDLGL_INPUT_T *pinput, SDLGL_EVENT_T *pevent, char is_hor)
{
    int on_slider;
    int sli_len, sli_pos, button_size;
    int el_chosen;


    if(pinput)
    {
        if(is_hor)
        {
            on_slider   = (pevent->mou.y > (pinput->rect.h - 16)) ? 1 : 0;
            button_size = (pinput->rect.h < 16) ? pinput->rect.h : 16;
            sli_len     = pinput->rect.w;
            sli_pos     = pevent->mou.x;
        }
        else
        {
            on_slider = (pevent->mou.x > (pinput->rect.w - 16)) ? 1 : 0;
            sli_len   = pinput->rect.h;
            sli_pos   = pevent->mou.y;
        }

        if(on_slider)
        {
            /* Is on slider */
            sdlgl_iinput_slider_calc(pinput, sli_len, sli_pos, button_size);
        }
        else
        {
            /* Calculate the chosen element */
            el_chosen = pinput->el_top + (pinput->el_visi * sli_pos / sli_len);
            sdlgl_iinput_set_value(pinput, el_chosen);
        }
    }
}

/* Name:
 *     sdlgl_iinput_mouse_focus
 * Description:
 *     Handles the keyboard input in detail for special kind of fields
 * Input: *
 *     pinput *: Pointer on new input field
 *     key_code:
 */
static void sdlgl_iinput_change_focus(SDLGL_INPUT_T *pinput)
{
    pFocusArea->fstate &= (unsigned char)(~SDLGL_FSTATE_HASFOCUS);
    /* Do all the work, if input looses focus */

    /* -- Now set the new focus -- */
    pFocusArea     = pinput;
    pinput->fstate |= SDLGL_FSTATE_HASFOCUS;

    if(pinput)
    {
        /* If a field is chosen */
        switch(pinput->sdlgl_type)
        {
            case SDLGL_TYPE_TEXT:
                /* Set the cursor at the mouse position to End of String */
                sdlgl_iedit_process_key(pinput, (char *)pinput->pdata, SDLK_END, 0);
                break;
        }
    }
}



/* Name:
 *     sdlgl_iinput_mouse_focus
 * Description:
 *     Handles the keyboard input in detail for special kind of fields
 * Input:
 *     pevent *: To translate
 *     pinput *: Pointer on field to handle
 *     key_code:
 */
static void sdlgl_iinput_mouse_focus(SDLGL_INPUT_T *pinput, SDLGL_EVENT_T *pevent)
{
    if(pinput)
    {
        /* If a field is chosen */
        switch(pinput->sdlgl_type)
        {
            case SDLGL_TYPE_TEXT:
                /* Set the cursor at the mouse position to End of String */
                sdlgl_iedit_process_key(pinput, (char *)pinput->pdata, SDLK_END, 0);
            case SDLGL_TYPE_TEXTAREA:
            case SDLGL_TYPE_NUMBER:
                break;

            case SDLGL_TYPE_RADIO:
                sdlgl_iinput_set_value(pinput, pinput->sub_code);
                break;

            case SDLGL_TYPE_CHECKBOX:
                sdlgl_iinput_switch_bit(pinput);
                break;

            case SDLGL_TYPE_SLIBOXV:
                sdlgl_iinput_sliderbox(pinput, pevent, 0);
                break;

            case SDLGL_TYPE_SLIBOXH:
                sdlgl_iinput_sliderbox(pinput, pevent, 1);
                break;

            case SDLGL_TYPE_SLIDERV:
                sdlgl_iinput_slider(pinput, pevent, 0);
                break;

            case SDLGL_TYPE_SLIDERH:
                sdlgl_iinput_slider(pinput, pevent, 1);
                break;

            case SDLGL_TYPE_CHOOSEV:
                sdlgl_iinput_slider(pinput, pevent, 0);
                break;


            case SDLGL_TYPE_CHOOSEH:
                sdlgl_iinput_slider(pinput, pevent, 1);
                break;

            case SDLGL_TYPE_CHOOSENUM:
                sdlgl_iinput_slider(pinput, pevent, 1);
                break;

            case SDLGL_TYPE_MAP:
                /* val_min: X-Elements, val_max: Y-Elements  */
                /* el_top:  Chosen X-Element, el_visi: Chosen Y-Elements  */
                if(pinput->val_min > 0 && pinput->val_max > 0)
                {
                    pevent->map_x = pinput->val_min * pevent->mou.x / pinput->rect.w;
                    pevent->map_y = pinput->val_max * pevent->mou.y / pinput->rect.h;
                    /* el_top and el_visi hold the x,y-Position of chosen tile for drawing */
                    pinput->el_top  = pevent->map_x;
                    pinput->el_visi = pevent->map_y;
                }
                break;

            /*
            SDLGL_TYPE_BUTTON
            SDLGL_TYPE_VALUE       0x0E
            SDLGL_TYPE_PROGRESSBAR 0x10
            SDLGL_TYPE_MENUBAR     0x1F
            SDLGL_TYPE_MENU        0x20
            */
            default:

                break;
        }
    }
}


 /* Name:
 *     sdlgl_iinput_focus_keyboard
 * Description:
 *     Handles the keyboard input in detail for special kind of fields
 * Input:
 *     pinput *: Pointer on field to handle
 *     pevent *: To translate
 */
static void sdlgl_iinput_focus_keyboard(SDLGL_INPUT_T *pinput, SDLGL_EVENT_T *pevent)
{
    if(pinput)
    {
        /* If a field is chosen */
        switch(pinput->sdlgl_type)
        {
            case SDLGL_TYPE_TEXT:
                sdlgl_iedit_process_key(pinput, pinput->pdata, pevent->sdlcode, pevent->modflags);
                break;
            case SDLGL_TYPE_TEXTAREA:
                /* pedit_text = (char *)pinput->pdata; */
                break;

            case SDLGL_TYPE_NUMBER:
                break;

            case SDLGL_TYPE_RADIO:
                break;

            case SDLGL_TYPE_CHECKBOX:
                sdlgl_iinput_switch_bit(pinput);
                break;

            case SDLGL_TYPE_BUTTON:
                break;

            case SDLGL_TYPE_SLIBOXV:
            case SDLGL_TYPE_SLIDERV:
                break;

            case SDLGL_TYPE_SLIBOXH:
            case SDLGL_TYPE_SLIDERH:
                break;
            /*
            SDLGL_TYPE_MAP
            SDLGL_TYPE_VALUE       0x0E
            SDLGL_TYPE_HORCHOOSE   0x0F
            SDLGL_TYPE_PROGBAR     0x10
            SDLGL_TYPE_MENUBAR     0x1F
            SDLGL_TYPE_MENU        0x20
            */
        }
    }
}

/*
 * Name:
 *     sdlgl_imouse_input_process
 * Description:
 *     Translates the mouse input, using the 'pfields *', if any are
 *     available. Otherwise the "pevent" is left unchanged. If an overlapped
 *     rectangle is hit by the mouse, the first one which has a value in
 *     the 'code' pinput is  the one which generates the command.
 * Input:
 *     pevent *:  Pointer on an inputevent, holding the mouse variables.
 *     pfields *: Pointer onSDLGL_INPUT's to check for mouse input.
 */
static void sdlgl_imouse_input_process(SDLGL_EVENT_T *pevent, SDLGL_INPUT_T *pfields)
{
    static SDLGL_INPUT_T *MouseArea = InputMou_Buffer; /* Never NULL   */
    static int tooltipticks = 0;

    SDLGL_INPUT_T *pbase;              /* Save pbase    */


    /* Adjust the mouse position to the graphics size of screen */
    pevent->mou.x = pevent->mou.x * mainConfig.displaywidth / mainConfig.scrwidth;
    pevent->mou.y = pevent->mou.y * mainConfig.displayheight / mainConfig.scrheight;
    /* Adjust the movement value, too */
    pevent->mou.w = pevent->mou.w * mainConfig.displaywidth / mainConfig.scrwidth;
    pevent->mou.h = pevent->mou.h * mainConfig.displayheight / mainConfig.scrheight;

    switch(pevent->sdlcode)
    {
        case SDL_BUTTON_LEFT:
            pevent->sdlcode = SDLGL_KEY_MOULEFT;
            break;

        case SDL_BUTTON_MIDDLE:
            pevent->sdlcode = SDLGL_KEY_MOUMIDDLE;
            break;

        case SDL_BUTTON_RIGHT:
            pevent->sdlcode = SDLGL_KEY_MOURIGHT;
            break;

        default:
            /* Is a mouse drag pevent */
            break;
    }

    pbase = pfields;

    if(pfields)
    {
        /* Search from top to bottom */
        while(pfields->sdlgl_type != 0)
        {
            /* Points on trailing type 0 */
            pfields++;
        }

        while(pfields > pbase)
        {
            pfields--;

            /* @TODO: Add NEW handling of keyboard based on pfields->sdlgl_type
               E.g. SDLGL_TYPE_EDIT + SDLGL_TYPE_DRAG */

            /* if the mouse has to be checked */
            if(pfields->code && sdlgl_imouse_inrect(pfields, pevent))
            {
                /* Support-flag for tooltips */
                if(MouseArea != pfields)
                {
                    /* Change active mouse-over-area */
                    MouseArea->fstate &= (unsigned char)(~SDLGL_FSTATE_CLEAR);
                    MouseArea         = pfields;
                    MouseArea->fstate |= SDLGL_FSTATE_MOUSEOVER;
                    tooltipticks      = 0;
                }
                else
                {
                    /* Same area as last call: Dragged in this Input-Rectangle... */
                    tooltipticks += pevent->tickspassed;

                    if(tooltipticks >= mainConfig.tooltiptime)
                    {
                        MouseArea->fstate |= SDLGL_FSTATE_TOOLTIP;
                    }
                }

                if(pevent->pressed && pevent->sdlcode)
                {
                    /* it's an active pinput */
                    if(pFocusArea != pfields)
                    {
                        /* Change focus */
                        sdlgl_iinput_change_focus(pfields);
                    }

                    /* Generate an Event: code and sub_code */
                    pevent->code     = pfields->code;
                    pevent->sub_code = pfields->sub_code;

                    /* Handle the 'click' event, depending on field type */
                    sdlgl_iinput_mouse_focus(pfields, pevent);
                }

                return; /* Event generated */

            } /* if in rectangle */

        } /* while(pfields > pbase) */

        /* Mouse off all pfields -- Reset 'mouse-over' */
        MouseArea->fstate &= (unsigned char)(~SDLGL_FSTATE_CLEAR);
        MouseArea         = DefaultArea;
    }  /* if(pfields) */

    /* Translate it as key if no pevent-code is generated. For combined input */
    sdlgl_ikeyboard_translate_input(pevent, pevent->pressed);
}

/*
 * Name:
 *     sdlgl_idefault_drawfunc
 * Description:
 *     Simply clears the screen
 * Input:
 *     fields: Pointer on fields to draw
 *     fps:    Actual number of fps
 */
#ifdef __BORLANDC__
        #pragma argsused
#endif
static void sdlgl_idefault_drawfunc(SDLGL_INPUT_T *fields, SDLGL_EVENT_T *pevent)
{
    /* Simply clear the screen */
    glClear(GL_COLOR_BUFFER_BIT);
}

/*
 * Name:
 *     sdlgl_icheck_input
 * Description:
 *     Polls all input devices and stores the input in SDLGL_EVENT_T.
 *     Merges the different types of input into this one single type.
 *     Translates the first input-psdl_event returned from SDL. All the others
 *     are ignored.
 * Input:
 *     pevent *:  Pointer on struct to fill with command info
 * Output:
 *      Result for reaction
 */
static int sdlgl_icheck_input(SDLGL_EVENT_T *pevent)
{
    SDL_Event psdl_event;


    memset(pevent, 0, sizeof(SDLGL_EVENT_T));

    while(SDL_PollEvent(&psdl_event))
    {
        if(! pevent->code)
        {
            switch(psdl_event.type)
            {
                case SDL_QUIT:
                	return SDLGL_INPUT_EXIT;

                case SDL_MOUSEMOTION:
                    pevent->mou.x = psdl_event.motion.x;
                    pevent->mou.y = psdl_event.motion.y;
                    /* TODO: Add x/y-rel to drag rectangle */
                    pevent->mou.w = psdl_event.motion.xrel;
                    pevent->mou.h = psdl_event.motion.yrel;
                    /* TODO: Set 'pevent->modflags = ' */
                    /* Now translate the input */
                    if(psdl_event.motion.state & SDL_BUTTON_LMASK)
                    {
                        pevent->pressed  = 1;
                        pevent->sdlcode  = SDLGL_KEY_MOULDRAG;
                    }
                    else if(psdl_event.motion.state & SDL_BUTTON_RMASK)
                    {
                        pevent->pressed  = 1;
                        pevent->sdlcode  = SDLGL_KEY_MOURDRAG;
                    }
                    else
                    {
                        /* Button don't care, no command, but mark 'mouse over' */
                        pevent->pressed  = 0;
                        pevent->sdlcode  = SDLGL_KEY_MOUMOVE;
                    }
                    sdlgl_imouse_input_process(pevent, Input_Stack[InputStackIdx].pInputAreas);
                    break;

                case SDL_MOUSEBUTTONDOWN:
                    /* Click only on mouse release ==> Button up */
                    /* May be start of drag action               */
                    break;

                case SDL_MOUSEBUTTONUP:
                    pevent->mou.x   = psdl_event.button.x;
                    pevent->mou.y   = psdl_event.button.y;
                    pevent->mou.w   = 0;
                    pevent->mou.h   = 0;
                    pevent->sdlcode = psdl_event.button.button;
                    pevent->pressed = 1;

                    sdlgl_imouse_input_process(pevent, Input_Stack[InputStackIdx].pInputAreas);
                    break;

                case SDL_KEYDOWN:
                    /* Check debug-state */
                    if(mainConfig.debugmode > 0 && psdl_event.key.keysym.sym == SDLK_ESCAPE)
                    {
                        return SDLGL_INPUT_EXIT;
                    }
                case SDL_KEYUP:
                    /* Translate Keyboard-Input */
                    pevent->mou.x    = 0;
                    pevent->mou.y    = 0;
                    pevent->sdlcode  = psdl_event.key.keysym.sym;
                    pevent->modflags = psdl_event.key.keysym.mod;

                    if(! sdlgl_ikeyboard_translate_input(pevent, psdl_event.type == SDL_KEYDOWN ? 1 : 0))
                    {
                        /* No special command, handle it as simple key by caller */
                        if(psdl_event.type == SDL_KEYUP)
                        {
                            /* Translate to input into focus area, if possible */
                            sdlgl_iinput_focus_keyboard(pFocusArea, pevent);
                        }
                    }
    	            break;

                default:
                    pevent->code       = 0;   /* Ignore it */

            }   /* switch(psdl_event.type) */
        }

        /* Do nothing at all, if already a code has been generated */
        /* Just remove it from buffer  */
    }  	/* while(SDL_PollEvent(&psdl_event)) */

    return SDLGL_INPUT_OK;
}

/*
 * Name:
 *     sdlgl_iinput_reset
 * Description:
 *     Resets the inputstack to the base and removes all possible
 *     inputdescs.
 * Input:
 *     None
 */
static void sdlgl_iinput_reset(void)
{
    SDL_Event pevent;


    /* Input stack */
    InputStackIdx = 0;
    memcpy(&Input_Stack[1], &Input_Stack[0], sizeof(Input_Stack[0]));
    sdlgl_iinput_move_focus(0, 0);

    /* Clear any input anyway */
    memset(&ActualEvent, 0, sizeof(SDLGL_EVENT_T));
    while( SDL_PollEvent(&pevent) );                /* Remove all input */
}

/*
 * Name:
 *     sdlgl_idefault_inputfunc
 * Description:
 *     Makes a minimum translation of user input.
 * Input:
 *     pevent:     Pointer on pretranslated input.
 *
 */
static int sdlgl_idefault_inputfunc(SDLGL_EVENT_T *pevent)
{
    if(pevent->sdlcode == SDLK_ESCAPE)
    {
        return SDLGL_INPUT_EXIT;
    }

    return SDLGL_INPUT_OK;
}

/*
 * Name:
 *     sdlgl_ifields_count
 * Description:
 *     Returns the number of fields with types != 0 in given fields.
 * Input:
 *     pfields *: Buffer to count the valid fields in
 */
static int sdlgl_ifields_count(SDLGL_INPUT_T *pfields)
{
    int count;


    count = 0;

    while(pfields->sdlgl_type != 0)
    {
        pfields++;
        count++;
    }

    return count;
}

/*
 * Name:
 *     sdlgl_ipop_input
 * Description:
 *     Removes a handler and its inputdescs from the internal buffer.
 *     Includes possible
 * Input:
 *     None
 */
static void sdlgl_ipop_input(void)
{
    SDLGL_INPUTKEY_T *pcmdkeys;
    SDLGL_EVENT_T sdlglevent;
    SDL_Event pevent;


    if(InputStackIdx > 1)
    {
        /* Call for possible release of memory */
        memset(&sdlglevent, 0, sizeof(SDLGL_EVENT_T));
        sdlglevent.code  = SDLGL_INPUT_CLEANUP;

        Input_Stack[InputStackIdx].InputFunc(&sdlglevent);

        /* Remove possible popup fields */
        Input_Stack[InputStackIdx].pInputAreas->sdlgl_type = 0;

        /* Decrement stack pointer */
        InputStackIdx--;
    }

    /* Clear any input anyway */
    memset(&ActualEvent, 0, sizeof(SDLGL_EVENT_T));

    /* Remove all input */
    while(SDL_PollEvent(&pevent));

    /* Clear possible set command keys */
    pcmdkeys = Input_Stack[InputStackIdx].pCmdKeys;

    if(pcmdkeys)
    {
        while(pcmdkeys->code > 0)
        {
            pcmdkeys->pressed = 0;
            pcmdkeys++;
        }
    }
}


/*
 * Name:
 *     sdlgl_icreate_input
 * Description:
 *     Creates a new desription on stack, holding the default values
 *     Adds default function, if needed.
 *     If the stack is full, the actual data is used.
 *     Adds a the Draw Function, the Inputhandler and a possible array of
 *     SDLGL_INPUT_T's and SDLGL_INPUTKEY_T's on the inputstack. For every argument
 *     set to NULL the internal default is used.
 *     If the stack is full, the topmost data will be overwritten.
 *     The INPUT_DESC's set for drawing are the same as these given for input.
 * Input:
 *     name: Name of input, for later use 'Back to this input with this name'
 *  Output:
 *     New input definition could be created, yes/no
 */
static int sdlgl_icreate_input(int name)
{
    SDL_Event pevent;
    int retval;


    retval = 0;

    if((InputStackIdx + 2) < SDLGL_MAXINPUTSTACK)
    {
    	/* Increment stackpointer only if possible */
        ++InputStackIdx;
        /* Initialize the new display with default functions and values */
        Input_Stack[InputStackIdx].pCmdKeys  = Default_Cmds;
        Input_Stack[InputStackIdx].DrawFunc  = sdlgl_idefault_drawfunc;
        Input_Stack[InputStackIdx].InputFunc = sdlgl_idefault_inputfunc;
        Input_Stack[InputStackIdx].name      = name;

        retval = 1;
    }

    /* Clear any input anyway */
    memset(&ActualEvent, 0, sizeof(SDLGL_EVENT_T));
    /* Remove all input */
    while(SDL_PollEvent(&pevent));

    return retval;
}

/*
 * Name:
 *     sdlgl_iadd_vectors
 * Description:
 *     Adds the given vectors to the Input-Buffer of the actual stack
 *     Creates a new desription on stack, holding the default values

 * Input:
 *     src *:      Vectors to copy
 *     dest *:     Where to copy them
 *     block_sign: To add, for dynamic input vectors
 *     x, y:       Add this value to base position, if needed
 * Output:
 *     Number of vectors copied
 */
static int sdlgl_iadd_vectors(SDLGL_INPUT_T *psrc, char block_sign, int x, int y)
{
    int num_src, num_dest;
    SDLGL_INPUT_T *pdest;


    num_src = 0;
    pdest   = Input_Stack[InputStackIdx].pInputAreas;

    if(psrc && pdest)
    {
        /* Count the fields, that should be copied   */
        num_src  = sdlgl_ifields_count(psrc);
        num_dest = Input_Stack[InputStackIdx].num_remaining;

        if(num_dest > num_src)
        {
            Input_Stack[InputStackIdx].num_remaining -= num_src;

            pdest = sdlgl_iinput_find_type(0, Input_Stack[InputStackIdx].pInputAreas);

            while(psrc->sdlgl_type != 0)
            {
                memcpy(pdest, psrc,  sizeof(SDLGL_INPUT_T));
                /* Set block_sign' for handling 'remove' */
                pdest->block_sign = block_sign;
                pdest->rect.x += x;
                pdest->rect.y += y;
                pdest++;
                psrc++;

            }
            /* Sign end of buffer */
            pdest->sdlgl_type = 0;
        }
    }

    return num_src;
}

/*
 * Name:
 *     sdlgl_iset_input
 * Description:
 *     Every non-NULL argument replaces the actual inputs in the actual display
 *     Drawing handler is taken from actual handler.
 * Input:
 *     inputfunc: A SDLGL_INPUTFUNC function pointer (may be NULL)
 *     fields:    A pointer on an array of SDLGL_INPUT_T's (may be NULL)
 *     pcmdkeys:   A pointer on an array of SDLGL_INPUTKEY_T's (may be NULL)
 *     x, y:      Position to add to input-vectors, if needed
 */
static void sdlgl_iset_input(SDLGL_INPUTFUNC inputfunc,
                             SDLGL_INPUT_T *fields,
                             SDLGL_INPUTKEY_T *pcmdkeys,
                             int x,
                             int y)
{
    SDLGL_INPUTKEY_T *pck;
    SDLGL_INPUT_T *pdest;
    int i, num_total;
    unsigned char mask;


    if(! pcmdkeys)
    {
        pcmdkeys = Default_Cmds;
    }
    else
    {
        /* Prepare the comparision mask for combined keys */
        pck = pcmdkeys;

        while(pck->keys[0] > 0)
        {
            pck->keymask = 0;

            for(i = 0, mask = 0x01; i < 2; i++, mask <<= 1)
            {
                if(pck->keys[i] > 0)
                {
                    pck->keymask |= mask;
                }
            }

            pck++;
        }
    }

    Input_Stack[InputStackIdx].pCmdKeys = pcmdkeys;

    if(inputfunc)
    {
        Input_Stack[InputStackIdx].InputFunc = inputfunc;
    }

    /* Get the values form previous buffer */
    num_total = Input_Stack[InputStackIdx - 1].num_remaining;

    pdest = sdlgl_iinput_find_type(0, Input_Stack[InputStackIdx - 1].pInputAreas);

    Input_Stack[InputStackIdx].pDrawAreas    = pdest;
    Input_Stack[InputStackIdx].pInputAreas   = pdest;
    Input_Stack[InputStackIdx].num_total     = num_total;
    Input_Stack[InputStackIdx].num_remaining = num_total;

    /* Copy fields to buffer */
    sdlgl_iadd_vectors(fields, 0, x, y);
}

/* ========================================================================== */
/* ========================= PUBLIC FUNCTIONS =============================== */
/* ========================================================================== */

/*
 * Name:
 *     sdlgl_input_new
 * Description:
 *     Adds a the Draw Function, the Inputhandler and a possible array of
 *     SDLGL_INPUT_T's and SDLGL_INPUTKEY_T's on the inputstack. For every argument
 *     set to NULL the internal default is used.
 *     If the stack is full, the topmost data will be overwritten.
 *     The INPUT_DESC's set for drawing are the same as these given for input.
 * Input:
 *     drawfunc:    SDLGL_DRAWFUNC function pointer
 *     inputfunc:   SDLGL_INPUTFUNC function pointer (may be NULL)
 *     pfields *:   Pointer on an array of SDLGL_INPUT_T's (may be NULL)
 *     pcmdkeys *:  Pointer on an array of SDLGL_INPUTKEY_T's (may be NULL)
 *     max_el:      > 0: Maximum of elements in 'pfields*'
 *     name:        'Name' of Input. To go back by name in future
 */
void sdlgl_input_new(SDLGL_DRAWFUNC  drawfunc,
                     SDLGL_INPUTFUNC inputfunc,
                     SDLGL_INPUT_T    *pfields,
                     SDLGL_INPUTKEY_T *pcmdkeys,
                     int max_el,
                     int name)
{
    if(sdlgl_icreate_input(name))
    {
        if(drawfunc)
        {
            Input_Stack[InputStackIdx].DrawFunc = drawfunc;
        }

        /* Now replace these functions / arrays, which are given */
        sdlgl_iset_input(inputfunc, pfields, pcmdkeys, 0, 0);
    }

    /* ------ Let OpenGL clear the display -------- */
    glClear(GL_COLOR_BUFFER_BIT);
}

/*
 * Name:
 *     sdlgl_input_popup
 * Description:
 *     Saves the previous state and uses the new handler and the new data for
 *     translation of input.
 *     Only the new given fields and commands are checked for input.
 *     But all fields since last 'InputNew' are handed over to caller for drawing
 * Input:
 *     inputfunc: Function to call for this input
 *     fields:    A pointer on an array of SDLGL_INPUT_T's (may be NULL)
 *     cmdkeys:   A pointer on an array of SDLGL_INPUTKEY_T's (may be NULL)
 *     x, y:      Where to place the fields on screen (add this)
 * Output:
 *     None
 */
void sdlgl_input_popup(SDLGL_INPUTFUNC inputfunc,
                       SDLGL_INPUT_T *fields,
                       SDLGL_INPUTKEY_T *cmdkeys,
                       int x,
                       int y)
{

    if(sdlgl_icreate_input(0))
    {
        sdlgl_iset_input(inputfunc, fields, cmdkeys, x, y);

        /* Take Draw-Function and Draw-Areas from previous display */
        Input_Stack[InputStackIdx].pDrawAreas = Input_Stack[InputStackIdx - 1].pDrawAreas;
        Input_Stack[InputStackIdx].DrawFunc  = Input_Stack[InputStackIdx - 1].DrawFunc;
    }
}


/*
 * Name:
 *     sdlgl_init
 * Description:
 *     - sets up an 2D-OpenGL screen with the size and colordepth
 *	 given in the config struct.
 * Input:
 *     configdata:
 *     	   The graphics configuration.
 * Output:
 *	errorcode as defined in mainloop.h
 */
int sdlgl_init(SDLGL_CONFIG_T *configdata)
{
    char  errmsg[256];
    int   eachcolordepth;
    int   scrflags;


    /* Initialize SDL for video output */
    if( SDL_Init(SDL_INIT_VIDEO) < 0 )
    {
        sprintf(errmsg, "Unable to initialize SDL: %s\n", SDL_GetError());
        puts(errmsg);
        return INIT_ERROR_SDL_INIT;	/* Return value for users "main()" function */
    }

    /* Now copy the users config data to internal structure, if any available */
    /* Otherwise use the standard values 				      */
    if(configdata)
    {
    	memcpy(&mainConfig, configdata, sizeof (SDLGL_CONFIG_T));

        if(mainConfig.dblclicktime == 0)
        {
            mainConfig.dblclicktime = SDLGL_DBLCLICKTIME;       /* Set default */
        }

        if(mainConfig.tooltiptime == 0)
        {
            mainConfig.tooltiptime = SDLGL_TOOLTIPTIME;
        }

    }

    if(mainConfig.debugmode)
    {
        mainConfig.colordepth = 0;		/* Use standard setings of screen     */
        SDL_WM_GrabInput(SDL_GRAB_ON);  /* Grab all input if in windowed mode */
    }
    else
    {
        /* Set Colordepth */
        switch(mainConfig.colordepth)
        {
            case 16: eachcolordepth = 5;
        	    break;

            case 24:
            case 32: eachcolordepth = 8;
        	    break;

            default:
            	eachcolordepth = mainConfig.colordepth / 3;
    	} /* switch */


        SDL_GL_SetAttribute( SDL_GL_RED_SIZE,   eachcolordepth );
        SDL_GL_SetAttribute( SDL_GL_GREEN_SIZE, eachcolordepth  );
        SDL_GL_SetAttribute( SDL_GL_BLUE_SIZE,  eachcolordepth );

        if(mainConfig.zbuffer > 0)
        {
            SDL_GL_SetAttribute( SDL_GL_DEPTH_SIZE, mainConfig.zbuffer );
        }

        SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );
    }

    scrflags = SDL_DOUBLEBUF | SDL_OPENGL;

    if(mainConfig.screenmode == SDLGL_SCRMODE_FULLSCREEN)
    {
    	scrflags |= SDL_FULLSCREEN;
    }

    /* Set the video mode with the data from configOpenGL screen */
    if( SDL_SetVideoMode(mainConfig.scrwidth, mainConfig.scrheight,
    			          mainConfig.colordepth, scrflags ) == NULL )
    {
        sprintf(errmsg, "Unable to create OpenGL screen: %s\n", SDL_GetError());
    	puts(errmsg);
    	SDL_Quit();

        return INIT_ERROR_SDL_SCREEN;
    }

    /* Set Clear-Color */
    glClearColor(0.0, 0.0, 0.0, 0.0);
    /* Set the Viewport to whole Screen */
    glViewport(0, 0, mainConfig.scrwidth, mainConfig.scrheight);

    /* Set Up An Ortho Screen */
    if(mainConfig.displaywidth == 0)
    {
        mainConfig.displaywidth  = mainConfig.scrwidth;
        mainConfig.displayheight = mainConfig.scrheight;
    }

    sdlglSetViewSize(mainConfig.displaywidth, mainConfig.displayheight);

    glEnable(GL_CULL_FACE);             /* Set backface-culling 		           */
    glPolygonMode(GL_FRONT, GL_FILL);	/* Draw only the front-polygons, fill-mode */
    glClear(GL_COLOR_BUFFER_BIT);	    /* Clear the screen first time for simple  */
    					                /* usages.				                   */

    /* Init the input stuff */
    sdlgl_iinput_reset();		/* Reset the internal handler stack to basic values     */

    /* Set the title bar in environments that support it */
    SDL_WM_SetCaption(mainConfig.wincaption, NULL);

    if(mainConfig.hidemouse)
    {
        /* Hide the mouse cursor */
        SDL_ShowCursor(0);
    }

    /* Move the mouse to the middle of the screen */
    SDL_WarpMouse((unsigned short)(mainConfig.scrwidth / 2),
                  (unsigned short)(mainConfig.scrheight / 2));

    return INIT_ERROR_NONE;
}


/*
 * Name:
 *     sdlgl_execute
 * Description:
 *     - sets up an 2D-OpenGL screen with the size and colordepth
 *	 given in the config struct.
 *     - Enters a draw / get-input loop until the used function
 *	 (the installed inputhandler returns "SDLGL_INPUTEXIT") asks
 * 	 for the exit of the loop.
 * Input:
 *	config:       The graphics configuration.
 * Output:
 *	exitcode for users "main()" function
 */
int sdlgl_execute(void)
{
    static int fpscount = 0;	/* FPS-Counter 						*/
    static int fpsclock = 0;	/* Clock for FPS-Counter			*/
    static int fps = 0;			/* Holds FPS until next FPS known	*/

    int   done;
    unsigned int mainclock,
	             newmainclock;

    int    tickspassed; /* To hand to the Main-Function. Helps the */
                        /* main function to hold animations and so */
                        /* on frame independent			  */


    /* Set the flag to not done */
    done = 0;

    /* Now init the clock */
    mainclock = SDL_GetTicks();


    /* Loop, drawing and checking events */
    while(! done)
    {
    	newmainclock = SDL_GetTicks();

        /* Update the clock  */
        tickspassed = newmainclock - mainclock;
        /* ------------ Count the frames per second ----------- */
        fpscount++;
        fpsclock += tickspassed;

        if(fpsclock >= SDLGL_TICKS_PER_SECOND)
        {
            fps      = fpscount;
            fpscount = 0;
            fpsclock -= SDLGL_TICKS_PER_SECOND;
        }

        /* ----------------- Draw the screen ------------------ */
        memset(&ActualEvent, 0, sizeof(SDLGL_EVENT_T));                   /* Clear it        */
        ActualEvent.tickspassed   = tickspassed;
        ActualEvent.secondspassed = (float)tickspassed / (float)SDLGL_TICKS_PER_SECOND;
        ActualEvent.fps           = fps;

        /* Now get the actual position of the mouse. Always return it */
        SDL_GetMouseState(&ActualEvent.mou.x, &ActualEvent.mou.y);
        ActualEvent.mou.x = ActualEvent.mou.x * mainConfig.displaywidth / mainConfig.scrwidth;
        ActualEvent.mou.y = ActualEvent.mou.y * mainConfig.displayheight / mainConfig.scrheight;

        /* Clear the screen */
        glClear(GL_COLOR_BUFFER_BIT);
        /* Now call the drawing function */
        Input_Stack[InputStackIdx].DrawFunc(Input_Stack[InputStackIdx].pDrawAreas, &ActualEvent);

        /* ------ Gather and translate user input -------- */
        if(sdlgl_icheck_input(&ActualEvent) == SDLGL_INPUT_EXIT)
        {
            done = 1;       /* Exit loop  */
        }
        else
        {
            /* Set values for timed input ... */
            ActualEvent.tickspassed = tickspassed;
            ActualEvent.secondspassed = (float)tickspassed / (float)SDLGL_TICKS_PER_SECOND;
            ActualEvent.fps         = fps;

            switch(Input_Stack[InputStackIdx].InputFunc(&ActualEvent))
            {
                case SDLGL_INPUT_EXIT:
                    done = 1;
                    break;

                case SDLGL_INPUT_REMOVE:
                	/* Remove an inputhandler and it's fields from inputstack */
                    sdlgl_ipop_input();
                    break;

                case SDLGL_INPUT_RESET:
                	sdlgl_iinput_reset();
                    break;
            }
        }

        glFlush();

        /* swap buffers to display, since we're double buffered.	*/
        SDL_GL_SwapBuffers();

        mainclock = newmainclock;
    }

    return 1;
}

/*
 * Name:
 *     sdlgl_shutdown
 * Description:
 *     Makes the cleanup work for the mainloop and closes the OpenGL
 *     screen.
 *     Any users cleanup work for the graphics has to be done before this
 *     procedure is called.
 * Output:
 *      Exitcode for users "main()" function
 */
void sdlgl_shutdown(void)
{
    SDL_WM_GrabInput(SDL_GRAB_OFF);
    SDL_Quit();
}

/*
 * Name:
 *     sdlgl_set_color
 * Description:
 *     Set the standard color with the given number.
 * Last change:
 *      2008-06-21 / bitnapper ( Standard-Palette with 256 colors)
 */
void sdlgl_set_color(int col_no)
{
    col_no &= 0xFF;	/* Prevent wrong numbers */

    glColor3ubv(&stdcolors[col_no * 4]);
}

/*
 * Name:
 *     sdlgl_get_color
 * Description:
 *     Gets the color with given number from the palette (including alpha channel)
 * Input:
 *     color:      Number of color to fetch
 *     col_buf *: Where to return the color
 */
void sdlgl_get_color(int colno, unsigned char *col_buf)
{
    colno &= 0xFF;	/* Prevent wrong numbers */
    colno *= 4;     /* Get index into array  */

    col_buf[0] = stdcolors[colno + 0];
    col_buf[1] = stdcolors[colno + 1];
    col_buf[2] = stdcolors[colno + 2];
    col_buf[3] = stdcolors[colno + 3];
}

/*
 * Name:
 *     sdlglSetPalette
 * Description:
 *     Sets the part of the palette with given color info
 * Input:
 *     from: Start in palette
 *     to:   Last color to set
 *
 */
void sdlglSetPalette(int from, int to, unsigned char *colno)
{
    int index;


    /* Prevent wrong numbers */
    from &= 0xFF;
    to   &= 0xFF;
    from *= 4;
    to   *= 4;

    for(index = from; index <= to; index++)
    {
        stdcolors[index] = *colno;
        colno++;
    }
}

/*
 * Name:
 *     sdlglAdjustRectToScreen
 * Description:
 *     Adjusts the given rectangle to screen, depending on given flags
 * Input:
 *     src:   Pointer on rectangle to adjust
 *     dest:  Where to put the adjusted values. If NULL, change source.
 *     flags: What to adjust
 */
void sdlglAdjustRectToScreen(SDLGL_RECT_T *src, SDLGL_RECT_T *dest, int flags)
{
    int i;
    int testflag;
    int diffx, diffy;   /* Difference between rectangele an screen size */

    if(dest == NULL)
    {
        dest = src;
    }
    else
    {
        dest->x = src->x;
        dest->y = src->y;
        dest->w = src->w;
        dest->h = src->h;
    }

    i        = 10;          /* Maximum number of flags */
    testflag = 1;
    diffx    = mainConfig.scrwidth  - dest->w;
    diffy    = mainConfig.scrheight - dest->h;

    while(i > 0)
    {
        switch(flags & testflag)
        {
            case SDLGL_FRECT_HCENTER:
                dest->x = (diffx / 2);
                break;

            case SDLGL_FRECT_VCENTER:
                dest->y = (diffy / 2);
                break;

            case SDLGL_FRECT_SCRLEFT:           /* On left side of screen     */
                dest->x = 0;
                break;

            case SDLGL_FRECT_SCRRIGHT:          /* On right side of screen    */
                dest->x = (diffx - 1);
                break;

            case SDLGL_FRECT_SCRTOP:            /* At top of screen           */
                dest->y = 0;
                break;

            case SDLGL_FRECT_SCRBOTTOM:         /* At bottom of screen        */
                dest->y = (diffy - 1);
                break;

            case SDLGL_FRECT_SCRWIDTH:          /* Set to screen width        */
                dest->x = 0;
                dest->w = mainConfig.scrwidth;
                break;

            case SDLGL_FRECT_SCRHEIGHT:         /* Set to screen height       */
                dest->y = 0;
                dest->h = mainConfig.scrheight;
                break;
        }  /* switch(flags & testflag) */

        i--;
        testflag <<= 1;
    } /* while(i > 0) */
}

/*
 * Name:
 *     sdlglScreenShot
 * Description:
 *     dumps the current screen (GL context) to a new bitmap file
 *     right now it dumps it to whatever the current directory is
 *     returns TRUE if successful, FALSE otherwise
 * Input:
 *     filename *:   name of file to save the screenshot to
 * Output:
 *     Suceeded Yes/no
 */
int sdlglScreenShot(const char *filename)
{
    SDL_Surface *screen, *temp;
    unsigned char *pixels;
    int i;


    screen = SDL_GetVideoSurface();

    temp = SDL_CreateRGBSurface(SDL_SWSURFACE, screen->w, screen->h, 24,
       #if SDL_BYTEORDER == SDL_LIL_ENDIAN
           0x000000FF, 0x0000FF00, 0x00FF0000, 0
       #else
           0x00FF0000, 0x0000FF00, 0x000000FF, 0
       #endif
           );

    if(temp == NULL)
    {
        return 0;
    }

    pixels = malloc(3*screen->w * screen->h);

    if(pixels == NULL)
    {
        SDL_FreeSurface(temp);
        return 0;
    }

    glReadPixels(0, 0, screen->w, screen->h, GL_RGB, GL_UNSIGNED_BYTE, pixels);

    for(i=0; i < screen->h; i++)
    {
        memcpy(((char *) temp->pixels) + temp->pitch * i, pixels + 3 * screen->w * (screen->h-i-1), screen->w * 3);
    }
    free(pixels);

    SDL_SaveBMP(temp, filename);
    SDL_FreeSurface(temp);

    return 1;
}

/*
 * Name:
 *     sdlglSetViewSize
 * Description:
 *     Sets the size for the view to the given value. The mouse position
 *     value will be adjusted to this one appropriatly.
 * Input:
 *      width, height:  Size to set the view to
 */
void sdlglSetViewSize(int width, int height)
{
    glLoadIdentity();
    glOrtho(0.0,  width, height, 0.0, -100.0, 100.0 );

    /* --- Save it for adjustement of mouse position ----- */
    mainConfig.displaywidth  = width;
    mainConfig.displayheight = height;
}

/* Name:
 *     sdlgl_input_get_value
 * Description:
 *     Gets the given value from 'pinput->pdata' and returns it
 *     as an integer
 *     SUPPORTS JUST SIGEND VALUES!
 * Input:
 *     pinput *:  Pointer on field to handle
 *     pretval *: Where to return the integer value
 * Output:
 *     Returned a valid value yes/no
 */
int sdlgl_input_get_value(SDLGL_INPUT_T *pinput, int *pretval)
{
    char *pvalue;


    if(pinput->pdata)
    {
        /* only if a valid pointer */
        pvalue = (char *)pinput->pdata;

        switch(pinput->val_type)
        {
            case SDLGL_VAL_CHAR:
                *pretval = (int)*(char *)pvalue;
                return 1;

            case SDLGL_VAL_SHORT:
                *pretval = (int)*(short int *)pvalue;
                return 1;

            case SDLGL_VAL_INT:
                *pretval = *(int *)pvalue;
                return 1;
        }
    }

    /* Return a valid value */
    *pretval = 0;

    return 0;
}

/*
 * Name:
 *     sdlgl_valtostr
 * Description:
 *     Converts given pvalue pointed with given type to a string
 * Input:
 *     type:       Type of pvalue given in 'pvalue'
 *     pvalue *:   Pointer on pvalue of 'type'
 *     pval_str *: Where to return the edit-pvalue as string
 *     max_len:    Maximum length for 'pval_str'
 */
void sdlgl_valtostr(char type, void *pvalue, char *pval_str, int max_len)
{
    /* Init as empty string */
    pval_str[0] = 0;

    if(pvalue)
    {
        switch(type)
        {
            case SDLGL_VAL_NONE:
            case SDLGL_VAL_STRING:
                strncpy(pval_str, pvalue, max_len);
                pval_str[max_len] = 0;               /* Clamp, if needed */
                break;

            case SDLGL_VAL_CHAR:
            case SDLGL_VAL_UCHAR:
                sprintf(pval_str, "%d", *(int *)pvalue);
                break;

            case SDLGL_VAL_SHORT:
            case SDLGL_VAL_USHORT:
                sprintf(pval_str, "%d", *(short int *)pvalue);
                break;

            case SDLGL_VAL_INT:
                sprintf(pval_str, "%d", *(int *)pvalue);
                break;

            case SDLGL_VAL_UINT:
                sprintf(pval_str, "%u", *(unsigned int *)pvalue);
                break;

            case SDLGL_VAL_FLOAT:
                sprintf(pval_str, "%f", *(float *)pvalue);
                break;

            case SDLGL_VAL_ONECHAR:
                pval_str[0] = *(char *)pvalue;
                pval_str[1] = 0;
                break;
        }
    }
}

/*
 * Name:
 *     sdlgl_input_sliderinfo
 * Description:
 *     Sets the input focus for the keyboard to given input pinput.
 *     If given input-pinput is 0, then the focus is set to the internal
 *     dummy input pinput.
 *     If 'pinput' is NULL, then the focus is moved into given direction 'dir'.
 * Input:
 *     pinput *:       To generate the info for drawing for
 *     pdraw_info *: Where to return all thes rectangles
 * Last change:
 *     2017-06-30: bitnapper
 */
void sdlgl_get_drawinfo(SDLGL_INPUT_T *pinput, SDLGL_RECT_T *pdraw_info)
{
    int i, button_size;

    /* For test purposes */
    pinput->sli_pos = 12;

    /* Make fife copies for result */
    for(i = 0; i < 6; i++)
    {
        /* [Box-Element, Box-Background], Slider-Background, Sub-Button, Add-Button, Slider-Button */
        memcpy(&pdraw_info[i], &pinput->rect, sizeof(SDLGL_RECT_T));
    }


    switch(pinput->sdlgl_type)
    {
        case SDLGL_TYPE_SLIBOXV:
            /* Box-Element */
            pdraw_info[0].h /= (pinput->el_visi > 0) ? pinput->el_visi : 1;
            /* Box-Background -- Full rectangle */
            /* Slider-Background + Buttons*/
            for(i = 2; i < 6; i++)
            {
                pdraw_info[i].x += pdraw_info[i].w - 16;
                pdraw_info[i].w = 16;
            }
            /* Slider Box width  */
            /* Top button */
            pdraw_info[3].h = 16;
            /* Botton button */
            pdraw_info[4].y += pdraw_info[4].h - 16;
            pdraw_info[4].h = 16;
            /* Slider-Button -- Act-Element ? */
            pdraw_info[5].y += pinput->sli_pos + 16;
            pdraw_info[5].h = 16;
            break;

        case SDLGL_TYPE_SLIBOXH:
            /* Box-Element */
            pdraw_info[0].w /= (pinput->el_visi > 0) ? pinput->el_visi : 1;
            /* Box-Background -- Full rectangle */
            /* Slider-Background + Buttons*/
            for(i = 2; i < 6; i++)
            {
                pdraw_info[i].y += pdraw_info[i].h - 16;
                pdraw_info[i].h = 16;
            }
            /* Left button */
            pdraw_info[3].w = 16;
            /* Right button */
            pdraw_info[4].x += pdraw_info[4].w - 16;
            pdraw_info[4].w = 16;
            /* Slider-Button -- Act-Element ? */
            pdraw_info[5].x += pinput->sli_pos + 16;
            pdraw_info[5].w = 16;
            break;

        case SDLGL_TYPE_SLIDERV:
            /* Slider-Background -- Full background */
            /* Top button */
            pdraw_info[1].h = 16;
            /* Botton button */
            pdraw_info[2].y += pdraw_info[2].h - 16;
            pdraw_info[2].h = 16;
            /* Slider-Button -- Act-Element ? */
            pdraw_info[3].y += pinput->sli_pos + 16;
            pdraw_info[3].h = 16;
            break;

        case SDLGL_TYPE_SLIDERH:
            /* Slider-Background -- Full background */
            /* Left button */
            pdraw_info[1].w = 16;
            /* Right button */
            pdraw_info[2].x += pdraw_info[2].w - 16;
            pdraw_info[2].w = 16;
            /* Slider-Button -- Act-Element ? */
            pdraw_info[3].x += pinput->sli_pos + 16;
            pdraw_info[3].w = 16;
            break;

        case SDLGL_TYPE_CHOOSEV:
            button_size = (pdraw_info[0].w < 16) ? pdraw_info[0].w : 16;
            /* Top button   */
            pdraw_info[0].h = button_size;
            /* Space for 'pinput->el_visi' (Buttons) */
            pdraw_info[1].y += button_size;
            pdraw_info[1].h -= (button_size * 2);
            /* Botton button     */
            pdraw_info[2].y += pdraw_info[2].h - button_size;
            pdraw_info[2].h = button_size;
            break;

        case SDLGL_TYPE_CHOOSEH:
            button_size = (pdraw_info[0].h < 16) ? pdraw_info[0].h : 16;
            /* Left button   */
            pdraw_info[0].w = button_size;
            /* Space for 'pinput->el_visi' (Buttons) */
            pdraw_info[1].x += button_size;
            pdraw_info[1].w -= (button_size * 2);
            /* Right button     */
            pdraw_info[2].x += pdraw_info[2].w - button_size;
            pdraw_info[2].w = button_size;
            break;

        case SDLGL_TYPE_CHOOSENUM:
            /* Left button */
            pdraw_info[0].w = 16;
            /* Field displaying value */
            pdraw_info[1].x += 16;
            pdraw_info[1].w -= 32;
            /* Right button */
            pdraw_info[2].x += pdraw_info[2].w - 16;
            pdraw_info[2].w = 16;
            break;
    }
}



/*
 * Name:
 *     sdlgl_input_remove
 * Description:
 *     Removes input with given
 *     Removes the block of fields in actual Input-Fields, signed by
 *     'block_sign', if available at all.
 * Input:
 *     block_sign: Value which signs the Input-Fields to remove
 */
void sdlgl_input_remove(char block_sign)
{

    SDLGL_INPUT_T *pstart, *pend, *pfields;


    /* 1) Check for valid 'block_sign'          */
    if(! block_sign)
    {
        return;
    }

    /* 2) Get fields pointer                    */
    pfields = Input_Stack[InputStackIdx].pInputAreas;

    if(pfields)
    {
        /* 1) Find block start, if it exists at all */
        while(pfields->sdlgl_type != 0)
        {
            if(pfields->block_sign == block_sign)
            {
                pstart = pfields;

                while(pfields->block_sign == block_sign)
                {
                    /* Find end of block */
                    pfields++;
                }

                pend = pfields;
                /* Remove the buffer, copy at minimum the end key_code   */
                do
                {
                    memcpy(pstart, pend, sizeof(SDLGL_INPUT_T));

                    pstart++;
                    pend++;

                }
                while(pend->sdlgl_type != 0);

                return;
            }

            pfields++;
        }
    }
}

/*
 * Name:
 *     sdlgl_input_add
 * Description:
 *     Adds the given fields to actual Input-Fields.
 *     If a pinput block signed with 'block_sign' already exists, it is replaced
 *     by the given pinput block.
 *     If the block doesn't fit into destination, the block isn't copied.
 * Input:
 *     block_sign: To set, replaces possible existing block
 *     psrc *:     Fields to add to Input-Fields.
 *     x, y:       Start at this position on screen (add to given pos)
 *                 (< 0: center it)
 */
void sdlgl_input_add(char block_sign, SDLGL_INPUT_T *psrc, int x, int y)
{
    SDLGL_RECT_T center_pos;


    /* 1) Check for valid 'block_sign'          */
    if(! block_sign)
    {
        return;
    }

    /* Remove possible existing input fields of same type */
    sdlgl_input_remove(block_sign);

    sdlglAdjustRectToScreen(&psrc->rect, &center_pos, SDLGL_FRECT_SCRCENTER);

    if(x < 0)
    {
        x = center_pos.x;
    }
    if(y < 0)
    {
        y = center_pos.y;
    }

    /* Prevent from going off screen  */
    if(x + psrc->rect.w > mainConfig.displaywidth)
    {
        x -= psrc->rect.w;
    }

    if(y + psrc->rect.h >  mainConfig.displayheight)
    {
        y -= psrc->rect.h;
    }

    sdlgl_iadd_vectors(psrc, block_sign, x, y);
}

