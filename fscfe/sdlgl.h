/*******************************************************************************
*  SDLGL.H                                                                     *
*	- Entrypoint for the library, starts the menu-loop.		                   *
*									                                           *
*   Copyright (C) 2001  Paul Mueller <pmtech@swissonline.ch>                   *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*                                                                              *
*                                                                              *
* Last change: 2008-06-21                                                      *
*******************************************************************************/

#ifndef _SDLGL_MAIN_H_
#define _SDLGL_MAIN_H_

/*******************************************************************************
* INCLUDES								                                       *
*******************************************************************************/

#include <SDL.h>

/* -- Global definitions -- */
#include "sdlgldef.h"

/*******************************************************************************
* DEFINES AND INCLUDES FOR OpenGL                                              *
*******************************************************************************/

/* XXX This is from Win32's <windef.h> */

#  ifndef APIENTRY
#   if (_MSC_VER >= 800) || defined(_STDCALL_SUPPORTED) || defined(__BORLANDC__) /* Borland is needed */
#    define APIENTRY    __stdcall
#   else
#    define APIENTRY
#   endif
#  endif

   /* XXX This is from Win32's <winnt.h> */

#  ifndef CALLBACK
#   if (defined(_M_MRX000) || defined(_M_IX86) || defined(_M_ALPHA) || defined(_M_PPC)) && !defined(MIDL_PASS)
#    define CALLBACK __stdcall
#   else
#    define CALLBACK
#   endif
#  endif

   /* XXX This is from Win32's <wingdi.h> and <winnt.h> */

#  ifndef WINGDIAPI
#   define WINGDIAPI __declspec(dllimport)
#  endif
   /* XXX This is from Win32's <ctype.h> */
#  ifndef _WCHAR_T_DEFINED
typedef unsigned short wchar_t;
#   define _WCHAR_T_DEFINED
#  endif

#include <GL/gl.h> 		/* Header File For The OpenGL32 Library	*/
#include <GL/glu.h>     /* Header File For The GLu32 Library    */
/*******************************************************************************
* DEFINES								                                       *
*******************************************************************************/

/* ------ Different screen modes ------ */
#define SDLGL_SCRMODE_WINDOWED   0
#define SDLGL_SCRMODE_FULLSCREEN 1
#define SDLGL_SCRMODE_AUTO       2              /* Not supported yet */

/* -- Flags for rectangle functions, the flags are tested from low to high -- */
#define SDLGL_FRECT_HCENTER     0x0001          /* Center the rectangle hor.  */
#define SDLGL_FRECT_VCENTER     0x0002          /* Center vertically          */
#define SDLGL_FRECT_SCRLEFT     0x0004          /* On left side of screen     */
#define SDLGL_FRECT_SCRRIGHT    0x0008          /* On right side scr          */
#define SDLGL_FRECT_SCRTOP      0x0010          /* At top of screen           */
#define SDLGL_FRECT_SCRBOTTOM   0x0020          /* At bottom of screen        */
#define SDLGL_FRECT_SCRWIDTH    0x0040          /* Set to screen width        */
#define SDLGL_FRECT_SCRHEIGHT   0x0080          /* Set to screen height       */
#define SDLGL_FRECT_SCRCENTER   (SDLGL_FRECT_HCENTER | SDLGL_FRECT_VCENTER)
#define SDLGL_FRECT_SCRSIZE     (SDLGL_FRECT_SCRWIDTH | SDLGL_FRECT_SCRHEIGHT)

/* Clock */
#define SDLGL_TICKS_PER_SECOND 1000

/* Input results to be returned by the SDLGL_INPUTFUNC */
#define SDLGL_INPUT_OK       0  /* Anything else >= 0                       */
#define SDLGL_INPUT_EXIT    -1  /* Exits the library and ends process       */
#define SDLGL_INPUT_REMOVE  -2	/* Removes the actual display and its       */
                                /* input descriptors and handler            */
#define SDLGL_INPUT_RESET   -3	/* Reset to default input handler		    */

/* Error codes for sdlgl_init. */
#define INIT_ERROR_NONE       0
#define INIT_ERROR_SDL_INIT   1
#define INIT_ERROR_SDL_SCREEN 2

/* ------- Special key definitions ------- */
#define SDLGL_KEY_MOULEFT       2000
#define SDLGL_KEY_MOUMIDDLE     2001
#define SDLGL_KEY_MOURIGHT      2002
#define SDLGL_KEY_MOUMOVE       2004
#define SDLGL_KEY_MOULDRAG      2005
#define SDLGL_KEY_MOUMDRAG      2006
#define SDLGL_KEY_MOURDRAG      2007
#define SDLGL_KEY_ANY           2030    /* Hand any key to given function */

/* ----- Focusstate for focus field -------- */
#define SDLGL_FSTATE_HASFOCUS  0x01
#define SDLGL_FSTATE_MOUSEOVER 0x02
#define SDLGL_FSTATE_MOUPRESS  0x04     /* Input 'pressed' on given field   */
#define SDLGL_FSTATE_MOUDRAG   0x08     /* Mouse dragged on given field     */
#define SDLGL_FSTATE_TOOLTIP   0x10     /* Mouse over since tooltip-time    */
#define SDLGL_FSTATE_CHECKED   0x20     /* For Radio-Button and Checkbox    */
#define SDLGL_FSTATE_EDITINS   0x40     /* For edit-fields                  */
#define SDLGL_FSTATE_CURSORON  0x80     /* For edit-fields: Blinking cursor */
#define SDLGL_FSTATE_CLEAR     0xFE     /* Clear all, except focusflag      */

/* -------- SDLGL-Types for input handling and drawing --------- */
#define SDLGL_TYPE_NONE        0x00     /* End of vector                    */
#define SDLGL_TYPE_STD         0x01     /* No special key handling          */
#define SDLGL_TYPE_LABEL       0x02     /* No special key handling          */
#define SDLGL_TYPE_TEXT        0x03     /* Input of single line of text     */
#define SDLGL_TYPE_TEXTAREA    0x04     /* Input of multipe lines of text   */
#define SDLGL_TYPE_NUMBER      0x05     /* Edit value of given type         */
#define SDLGL_TYPE_RADIO       0x06
#define SDLGL_TYPE_CHECKBOX    0x07
#define SDLGL_TYPE_BUTTON      0x08     /* Simple Button                    */
#define SDLGL_TYPE_SLIBOXV     0x09     /* Box with vertical slider         */
#define SDLGL_TYPE_SLIBOXH     0x0A     /* Box with horizontal slider       */
#define SDLGL_TYPE_SLIDERV     0x0B     /* Vertical slider                  */
#define SDLGL_TYPE_SLIDERH     0x0C     /* Horizontal slider                */
#define SDLGL_TYPE_MAP         0x0D     /* Map square (sub-rectangles)      */
#define SDLGL_TYPE_VALUE       0x0E     /* Print a value given in field     */
#define SDLGL_TYPE_CHOOSEH     0x0F
#define SDLGL_TYPE_CHOOSEV     0x10
#define SDLGL_TYPE_CHOOSENUM   0x11     /* Choose a number with buttons     */
#define SDLGL_TYPE_PROGRESSBAR 0x12
#define SDLGL_TYPE_POINTBAR    0x13     /* A bar of points (draw only)      */
#define SDLGL_TYPE_MENUBAR     0x1E     /* At top of screen                 */
#define SDLGL_TYPE_MENUBK      0x1F     /* Dropdown- or Local-Menus         */
#define SDLGL_TYPE_MENU        0x20     /* Drawing / manage menu strings    */
#define SDLGL_TYPE_TEXTURE     0x2F     /* Draw a texture val_min, val_max  */
#define SDLGL_TYPE_MAX         0x30     /* Maximum internal types           */

/* --- Special 'codes' for special movements if input is cleared --- */
#define SDLGL_INPUT_LOSEOVER     0x70   /* Field lost mouse-over            */
#define SDLGL_INPUT_GETOVER      0x71   /* Field got mouse-over             */
#define SDLGL_INPUT_MOUSEMOVE    0x72   /* Mouse moved over input-field     */
#define SDLGL_INPUT_MOUSEDRAG    0x73   /* Mouse dragged over input-field   */
#define SDLGL_INPUT_LOSEFOCUS    0x74   /* For Edit-Fields                  */
#define SDLGL_INPUT_GETFOCUS     0x75
#define SDLGL_INPUT_ISCHAR       0x76
#define SDLGL_INPUT_CLEANUP      0x7E

/*******************************************************************************
* GLOBAL TYPEDEFS							                                   *
*******************************************************************************/

typedef struct
{
    char *wincaption;		/* Caption for window, if any	    */
    int scrwidth, scrheight;/* Size of screen to set	        */
    int colordepth;		    /* Colordepth of screen		        */
    int wireframe;		    /* Draw mode: wireframe or filled   */
    int hidemouse;		    /* Hide mouse, if asked for	        */
    int screenmode;		    /* SDLGL_SCRMODE*       	        */
    int debugmode;		    /* Debugmode: For use by caller     */
    int zbuffer;            /* > 0: Set the zbuffer             */
    int dblclicktime;       /* Time for sensing doubleclicks    */
    int displaywidth,
        displayheight;      /* Width and height of display to set */
    int tooltiptime;        /* Time until tooltip flag is set     */
}
SDLGL_CONFIG_T;

typedef struct
{
    int  keys[2];	/* Code for keys SDLK_...                         */
                    /* 'keys[0]' == 0 signs end of array              */
    char code;		/* Is returned if the given key(s) is/are pressed */
    char sub_code;  /* Is returned if the given key(s) is/are pressed */
    char release_code;      /* Send this code also on 'release'       */
    char pressed;   /* Flags if indexed key is pressed                */
    char keymask;   /* Keys needed for checking combined keys         */
}
SDLGL_INPUTKEY_T;

typedef struct
{
    char code;	    /* Code from INPUTVEC, if in a rect	        */
    			    /* 0, if no code generated by input         */
                    /* translation				                */
    char sub_code;  /* Additional code for input, size for EDIT */
    int  sdlcode;	/* SDLK_... code			                */
    int  modflags;  /* Flags of mod keys			            */
    			    /* SDL_BUTTON_... if SDLGL_INP_MOUSE	    */
    /* ------------ Mouse related stuff ----------------------- */
    SDLGL_RECT_T mou; /* Relative position ( from upper     	*/
                    /* left ) into the SDLGL_INPUT_T-rectangle 	*/
                    /* if on such a rectagle.		            */
                    /* Else position from left top of           */
                    /* screen for DRAW-Event                    */
                    /* w / h : Distance the mouse has moved     */
                    /* since last event. For drag events.       */
                    /* If map: Map-Rectangle clicked on         */
    int map_x,
        map_y;      /* Of map element, (from 'field->el_visi')  */
	/* --------- Additional info ------------------------------ */
    int   tickspassed;	  /* Ticks passed since last call of input    */
    float secondspassed;  /* Seconds passed since last call           */
    int  fps;
    char pressed;         /* Event was genererated on press/release */
}
SDLGL_EVENT_T;

/* ------ Drawing and input functions -------- */
typedef void (* SDLGL_DRAWFUNC)(SDLGL_INPUT_T *pfields, SDLGL_EVENT_T *pevent);
    /* Type definition of inputhandler: Is called for every input generated
       by a definition of a SDLGL_INPUT_T or SDLGL_INPUTKEY_T */
typedef int  (* SDLGL_INPUTFUNC)(SDLGL_EVENT_T *pevent);

/*******************************************************************************
* CODE 								                                           *
*******************************************************************************/

/* Set up for C function definitions, even when using C++ */
#ifdef __cplusplus
extern "C" {
#endif

/* -------- Creating Input-Screens and setting inputvectors ---------- */
void sdlgl_input_new(SDLGL_DRAWFUNC   drawfunc,
                     SDLGL_INPUTFUNC  inputfunc,
                     SDLGL_INPUT_T    *pfields,
                     SDLGL_INPUTKEY_T *pcmdkeys,
                     int max_el,
                     int name);

void sdlgl_input_popup(SDLGL_INPUTFUNC  inputfunc,
                       SDLGL_INPUT_T    *pfields,
                       SDLGL_INPUTKEY_T *pck,
                       int x,
                       int y);

/* To be called first. After this one is called, the graphics screen is set */
/* up and all OpenGL functions are available 				                */
int  sdlgl_init(SDLGL_CONFIG_T *configdata);

/* Main loop itself. Never returns, until the users (or the standard)   */
/* inputhandler function returns "SDLGL_INPUT_EXIT"		                */
int  sdlgl_execute(void);

/* Procedure to be called after the user made all cleanup work for his  */
/* calls of the OpenGL graphics interface				*/
void sdlgl_shutdown(void);

/* ----------- Some little help functions -----------  */
int  sdlgl_input_get_value(SDLGL_INPUT_T *pinput, int *pretval);
void sdlgl_valtostr(char type, void *pvalue, char *pval_str, int max_len);
void sdlgl_get_drawinfo(SDLGL_INPUT_T *pinput, SDLGL_RECT_T * pdraw_info);

/* Some standard colors */
void sdlgl_set_color(int colno);
void sdlglGetColor(int colno, unsigned char *pcol_buf);
void sdlglSetPalette(int from, int to, unsigned char *pcolno);
void sdlglAdjustRectToScreen(SDLGL_RECT_T *psrc, SDLGL_RECT_T *dst, int flags);
int  sdlglScreenShot(const char *filename);
void sdlglSetViewSize(int width, int height);

/* ----------- Functions for dynamic input fields -------- */
void sdlgl_input_remove(char block_sign);
void sdlgl_input_add(char block_sign, SDLGL_INPUT_T *psrc, int x, int y);

/* Set up for C function definitions, even when using C++ */
#ifdef __cplusplus
}
#endif

#endif /* _SDLGL_MAIN_H_	*/

