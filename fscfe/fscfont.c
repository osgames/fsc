/*******************************************************************************
*  FSCFONT.H                                                                   *
*      - Textured fonts for FSC                      					       *
*                                                                              *
*	FREE SPACE COLONISATION               	                                   *
*      (c)2002 - 2017 Paul Mueller <muellerp61@bluewin.ch>                     *
*                                                                              *
*  This program is free software; you can redistribute it and/or modify        *
*  it under the terms of the GNU General Public License as published by        *
*  the Free Software Foundation; either version 2 of the License, or           *
*  (at your option) any later version.                                         *
*                                                                              *
*  This program is distributed in the hope that it will be useful,             *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of              *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
*  GNU Library General Public License for more details.                        *
*                                                                              *
*  You should have received a copy of the GNU General Public License           *
*  along with this program; if not, write to the Free Software                 *
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  *
*******************************************************************************/

/*******************************************************************************
* INCLUDES                                                                     *
*******************************************************************************/

#include <stdarg.h>         /* va_                          */
#include <string.h>         /* strpbrk()                    */


#include "sdlgl.h"          /* Header for Open GL functions */
#include "sdlgltex.h"       /* Texture loading              */


/* -- Own header -- */
#include "fscfont.h"

/*******************************************************************************
* DEFINES                                                                      *
*******************************************************************************/

#define FSCFONT_TABSIZE    32           /* Tab size in pixels   */
#define FSCFONT_MAX         4
#define FSCFONT_HEIGHT     30
#define FSCFONT_ADD         4           /* Vertical Gap between letters     */
#define FSCFONT_NUMCHAR   128           /* Number of chars in bitmap        */


/*******************************************************************************
* TYPEDEFS                                                                     *
*******************************************************************************/

typedef struct
{
    GLfloat measure[8];	/* Four pairs of edge values 		    */
    GLfloat uv[8]; 	    /* Four pairs of texture coordinates 	*/
    int     xspacing;	/* The horizontal distance		        */

}
FONTCHAR_T;

typedef struct
{
    int width,
        heigth;         /* Widht/height of a char cell in texture   */
    int base;           /* Base where it starts                     */
    /* ------------- Original size -------------------------------- */
    int height;         /* Basic height                             */
    int yspacing;       /* Distance between lines                   */
    GLuint  texID;      /* ID of texture for this font              */
    int numchar;        /* Number of chars in font (for resize)     */
    /* ------------ Actual size ----------------------------------- */
    float factor;
    FONTCHAR_T *fc;     /* Pointer on font chars for this texture   */
}
FSC_FONT_T;

/*******************************************************************************
* DATA                                                                         *
*******************************************************************************/

static unsigned char FscFont_ActColor[] = { 0xFF, 0xFF, 0xFF, 0xFF };
static unsigned char Label_Color[] =
{
    0xFF, 0xFF, 0xFF,               /* White                    */
    0xB2, 0x99, 0x00,               /* Lo-Menu                  */
    0xE5, 0x19, 0x00,               /* Hi-Menu                  */
    0xFF, 0xFF, 0xFF,               /* White                    */
    255,  85, 85                    /* Red for menu highlight   */
};

/* The fscfont rectangles with texture coordinates and spacing measure*/
static FONTCHAR_T FscFont_Chars[FSCFONT_NUMCHAR * FSCFONT_MAX];

static FSC_FONT_T Fonts[FSCFONT_MAX] =
{
    {
        16, 20,   /* Widht/height of a char cell in texture   */
        /* --------------- */
        32,       /* Base where it starts                     */
        16,       /* Basic height of char                     */
        20,       /* Distance between lines                   */
        0,       /* Texture ID                               */
        96,       /* Number of chars in texture               */
        1.0,      /* Factor to multiply char disatances with  */
        &FscFont_Chars[0]
    }
};

static FSC_FONT_T *ActFont = &Fonts[0];

/*******************************************************************************
* CODE                                                                         *
*******************************************************************************/

/*
 * Name:
 *     fscfont_begin_text
 * Description:
 *     Sets all the OpenGL-States needed for drawin text
 * Replaces:
 *     BeginText
 */
static void fscfont_begin_text(void)
{
    glPushAttrib(GL_ENABLE_BIT | GL_COLOR_BUFFER_BIT);

    glEnable( GL_TEXTURE_2D );	    /* Enable texture mapping */
    glBindTexture(GL_TEXTURE_2D, ActFont->texID);
    glAlphaFunc(GL_GREATER,0);
    glEnable(GL_ALPHA_TEST);
    /* glDisable(GL_DEPTH_TEST); */
    glDisable(GL_CULL_FACE);
    /* Set the color for the string */
    glColor3ubv(FscFont_ActColor);
}


/*
 * Name:
 *     fscfont_end_text
 * Replaces:
 *     EndText
 */
static void fscfont_end_text(void)
{
    /* Back to white */
    FscFont_ActColor[0] = 0xFF;
    FscFont_ActColor[1] = 0xFF;
    FscFont_ActColor[2] = 0xFF;
    glDisable(GL_ALPHA_TEST);
    glPopAttrib();
}

/*
 * Name:
 *     fscfont_draw_one_char
 * Description:
 *     This function draws a letter or number. It is assumed that the screen
 *     is set up in a way that x/y 0,0 is at the left top of the screen
 * Input:
 *     letter: Which char to draw
 *     x, y:   Where to draw the char.
 * Output:
 *     xspacing for this char
 */
static int fscfont_draw_one_char(int letter, int x, int y)
{
    int i;
    FONTCHAR_T drawchar;


    /* make a copy */
    memcpy(&drawchar, &ActFont->fc[letter - ActFont->base], sizeof(FONTCHAR_T));

    /* move the charpos */
    for(i = 0; i < 8; i += 2) {

        drawchar.measure[i]     += x;
        drawchar.measure[i + 1] += y;

    }

    glBegin(GL_QUADS);
    	glTexCoord2fv(&drawchar.uv[0]);
        glVertex2fv(&drawchar.measure[0]);

    	glTexCoord2fv(&drawchar.uv[2]);
        glVertex2fv(&drawchar.measure[2]);

    	glTexCoord2fv(&drawchar.uv[4]);
        glVertex2fv(&drawchar.measure[4]);

    	glTexCoord2fv(&drawchar.uv[6]);
        glVertex2fv(&drawchar.measure[6]);
    glEnd();

    return (int)((ActFont->factor) * drawchar.xspacing);
}


/*
 * Name:
 *     fscfont_draw_string
 * Description:
 *     Draws a string without calling "BeginText" and "EndText"
 * Input:
 *     ppos *:
 *     ptext *:
 * Output:
 *      new y-pos
 */
static int fscfont_draw_string(SDLGL_RECT_T *ppos, char *ptext)
{
    unsigned char cTmp;
    int start_x, tab;		/* Save for carriage newline */
    int act_x, act_y;
    int cnt = 0;


    /* Just for the case */
    if(! ptext)
    {
        /* Save from drawing empty strings      */
        return ppos->y;
    }

    cTmp    = ptext[0];
    start_x = ppos->x;                 /* Aligned where we started */
    act_x   = ppos->x;
    act_y   = ppos->y;

    while(cTmp != 0)
    {
        /* Convert ASCII to our own little fscfont */
        switch (cTmp)
        {
            case '\t':
            case '~':
                {
                    /* Use squiggle for tab */
            	    tab = act_x % FSCFONT_TABSIZE;
                    if(tab == 0)
                        tab = FSCFONT_TABSIZE;
            	    act_x += tab;
        	    }
            	break;

            case '\n':
                /* Newline */
                act_y += (int)((ActFont->factor) * ActFont->yspacing);      /* Down a line 		    */
                act_x = start_x;		              /* back to start of line  */
             	break;

            default:
            {
                /* Normal letter */
                act_x += fscfont_draw_one_char(cTmp, act_x, act_y);
            }
        }

        cnt++;
        cTmp = ptext[cnt];
    }

    return act_y;
}

/*
 * Name:
 *     fscfont_string_to_msgrect
 * Description:
 *     Prints the string into the given rectangle. In case the string
 *     doesn't fit into the width of the rectangle, it's wrapped onto the
 *     next line(s).
 *     Words are assumed to end with one of the the following characters:
 *		space   (' ')
 *	    tab     ('\t')
 *		CR      ('\r')
 *		newline ('\n')
 *	 	and	    ('-')
 *     The actually set color is used.
 * Input:
 *     rect *:   Pointer on rect to draw the string within
 *     string:   String to print
 */
static void fscfont_string_to_msgrect(SDLGL_RECT_T *rect, char *string)
{
    SDLGL_RECT_T sizerect;
    SDLGL_RECT_T argrect;
    char *pnextbreak, *pprevbreak;
    char prevsavechar, nextsavechar = 0;


    if(! string)
    {
        /* Just for the case ... */
        return;
    }


    /* First try if the whole string fits into the rectangles width */
    fscfont_string_size(string, &sizerect);

    if(sizerect.w <= rect->w)
    {
        /* Print the string */
        fscfont_draw_string(rect, string);
    }
    else
    {
        /* Drawing position if more then one line, a bit smaller then the given rectangle */
        argrect.x = rect->x + 3;
        argrect.y = rect->y + 3;
        argrect.w = rect->w - 6;
        argrect.h = rect->h - 6;

        /* Run pointer into the string 		    */
        pprevbreak = string;

        /* We have to scan word by word */

        do {

            pnextbreak = strpbrk(pprevbreak, " \t\r\n-");

            if(pnextbreak) {

                /* The end of string is not reached yet */
                pnextbreak++;  	      	    /* Include the breaking character */
                nextsavechar = *pnextbreak; /* Save the break char.	       */
                *pnextbreak   = 0;	    /* Create a string out of it.     */

            }

            /* Now get the size, of the string */
            fscfont_string_size(string, &sizerect);

            if(sizerect.w > rect->w)
            {
                /* One word to much, use previous break. */
                prevsavechar = *pprevbreak;
                *pprevbreak  = 0;	/* Create the string */

                /* Print the string... */
                fscfont_draw_string(&argrect, string);

                /* Move string to new start */
                string  = pprevbreak;
                *string = prevsavechar;   	/* Get back the prev char */

                /* Move to next line... */
                argrect.y += sizerect.h;

            }
            else
            {
            	/* Check for end of string */
                if(! pnextbreak)
                {
                    fscfont_draw_string(&argrect, string);
                    return;
                }
                /* Move one word further.   */
                pprevbreak  = pnextbreak;
            }


            if(pnextbreak)
            {
                /* bring the next char back */
            	*pnextbreak = nextsavechar;
            }
    	}
        while(*string != 0);
    }
}

/*
 * Name:
 *     fscfont_draw_string_adjusted
 * Description:
 *     Draws the given text into the given rectangle, adjusting it
 *     to the rectangle depending on the given flags.
 * Input:
 *     prect *: Rect to draw the text into
 *     ptext *: Text to draw
 *     align:   How to adjust relative to the given rectangle
 */
static void fscfont_draw_string_adjusted(SDLGL_RECT_T *prect, char *ptext, char align)
{
    SDLGL_RECT_T text_size;


    if(! ptext)
    {
        return;
    }

    text_size.x = prect->x;
    text_size.y = prect->y;

    if(align)
    {
        /* If special position at all: Write text with wrap into rectangle */
        if(align == 'W')
        {
            /* Fit the text into given rectangle */
            fscfont_string_to_msgrect(prect, ptext);
            return;
        }

        fscfont_string_size(ptext, &text_size);

        if(align == 'C')
        {
            text_size.x += (prect->w - text_size.w) / 2;
        }
        else if(align == 'R')
        {
            text_size.x += (prect->w - text_size.w);
        }

        if(align != 'T')
        {
            /* If not hold it at top... */
            text_size.y += (prect->h - text_size.h) / 2;
        }
    }

    fscfont_draw_string(&text_size, ptext);
}

/*
 * Name:
 *     fscfont_print_single_value
 * Description:
 *     Prints a single value
 * Input:
 *     prect *:  Where to print
 *     val_type: Type of data
 *     pdata *:  Pointer on data to print
 *     align:    How to align the string to rectangle
 */
static void fscfont_print_single_value(SDLGL_RECT_T *prect, char val_type, void *pdata, char align)
{
    /* Buffer for completed string, if value  */
    char val_str[82];


    sdlgl_valtostr(val_type, pdata, val_str, 81);

    fscfont_draw_string_adjusted(prect, val_str, align);
}

/* ========================================================================== */
/* ========================= PUBLIC FUNCTIONS =============================== */
/* ========================================================================== */

/*
 * Name:
 *     fscfont_draw_char
 * Description:
 *     This function draws a letter or number.
 * Input:
 *     letter: Which letter to draw
 *     x, y:   Where to draw the char.
 * Replaces:
 *     draw_one_fscfont
 */
void fscfont_draw_char(int letter, int x, int y)
{
    fscfont_begin_text();
    fscfont_draw_one_char(letter, x, y);
    fscfont_end_text();
}

/*
 * Name:
 *     fscfont_string
 * Description:
 *     Prints out a string using the actual font and actual color
 * Input:
 *      szText: Text to print out.
 *      pos *:	Where to print out the text
 * Output:
 *	    New y-position. Changed value form input if one or more newline
 *	    characters are included into the string.
 */
int fscfont_string(SDLGL_RECT_T *pos, char *szText)
{
    int result;


    fscfont_begin_text();
    result = fscfont_draw_string(pos, szText);
    fscfont_end_text();

    return result;
}

/*
 * Name:
 *     fscfont_stringf
 * Description:
 *     Prints out a string using the actual font and actual color
 * Input:
 *      szText: Text to print out.
 *      pos *:	Where to print out the text
 * Output:
 *	    New y-position. Changed value form input if one or more newline
 *	    characters are included into the string.
 */
int fscfont_stringf(SDLGL_RECT_T *pos, char *text, ...)
{
    int newy;
    char buffer[512];
    va_list ap;


    va_start(ap, text);

    vsprintf(buffer, text, ap);

    newy = fscfont_string(pos, buffer);
    va_end(ap);

    return newy;
}

/*
 * Name:
 *     fscfont_text_to_rect
 * Description:
 *     Draws the given text into the given rectangle, adjusting it
 *     to the rectangle depending on the given flags.
 * Input:
 *     prect *: Rect to draw the text into
 *     ptext *: Text to draw
 *     align:   How to adjust relative to the given rectangle
 */
void fscfont_text_to_rect(SDLGL_RECT_T *prect, char *ptext, char align)
{
    fscfont_begin_text();
    fscfont_draw_string_adjusted(prect, ptext, align);
    fscfont_end_text();
}

/*
 * Name:
 *     fscfont_set_color_no
 * Description:
 *     Sets given color-number as font color.
 * Input:
 *     color_no: Number of color to set -- SDLGL_COL_
 */
void fscfont_set_color_no(int color_no)
{
    unsigned char col_buf[4];


    sdlgl_get_color(color_no, col_buf);
    fscfont_set_colorv(col_buf);
}

/*
 * Name:
 *     fscfont_set_colorv
 * Description:
 *     Sets given color as font color
 * Input:
 *     color *: Pointer on three chars defining the color
 */
void fscfont_set_colorv(unsigned char *color)
{
     FscFont_ActColor[0] = color[0];
     FscFont_ActColor[1] = color[1];
     FscFont_ActColor[2] = color[2];
}

/*
 * Name:
 *     fscfont_load
 * Description:
 *     This function loads the fscfont bitmap and sets up the coordinates
 *     of each fscfont on that bitmap...
 * Input:
 *      None
 */
void fscfont_load(void)
{
    int cnt, x;
    int line;
    float runu, runv;
    FONTCHAR_T *fc;


    ActFont->texID = sdlgltexLoadSingleA("data/FontOCRA.bmp", 0);

    cnt = 0;
    runu = 0.0;
    runv = 0.0;
    fc = ActFont->fc;

    /* Now generate the texture offsets for all chars */
    for(line = 0; line < 6; line++)
    {
        for(x = 0; x < (256/16); x++)
        {
             /* counterclockwise */
             /* first measure */
             FscFont_Chars[cnt].measure[0] = 0;
             FscFont_Chars[cnt].measure[1] = 20;
             FscFont_Chars[cnt].measure[2] = 0; /* size in pixels */
             FscFont_Chars[cnt].measure[3] = 0;
             FscFont_Chars[cnt].measure[4] = 16;
             FscFont_Chars[cnt].measure[5] = 0;
    	     FscFont_Chars[cnt].measure[6] = 16;
             FscFont_Chars[cnt].measure[7] = 20;

             FscFont_Chars[cnt].uv[2] = runu + 0.002;
             FscFont_Chars[cnt].uv[3] = runv + 0.002;
    	     FscFont_Chars[cnt].uv[0] = runu + 0.002;
             FscFont_Chars[cnt].uv[1] = runv + 0.15625 - 0.004;
             FscFont_Chars[cnt].uv[6] = runu + 0.0625  - 0.004;
             FscFont_Chars[cnt].uv[7] = runv + 0.15625 - 0.004;
             FscFont_Chars[cnt].uv[4] = runu + 0.0625  - 0.004;  /* 0.001 */
             FscFont_Chars[cnt].uv[5] = runv + 0.002;

             runu += 0.0625;
             fc[cnt].xspacing = 16;
             cnt++;
        }

        runu = 0.0;		        /* U Back to begin of line */
        runv += 0.15625;        /* V on next line 	       */
    }
}

/*
 * Name:
 *     fscfont_release
 * Description:
 *     Releases the texture of the fscfont loaded by "fscfont_load"
 * Input:
 *     None
 */
void fscfont_release(void)
{
    glDeleteTextures(1, &ActFont->texID);
}

/*
 * Name:
 *     fscfontSet
 * Description:
 *     Sets the font to use for text output. (Actually unused).
 * Input:
 *     fontno:
 */
void fscfontSet(int fontno)
{
    if(fontno < 0 || fontno >= FSCFONT_MAX)
    {
        fontno = 0;
    }
    else
    {
        /* Always 0 at the moment */
        fontno = 0;
    }

    ActFont = &Fonts[fontno];
}

/*
 * Name:
 *     fscfont_set_size
 * Description:
 *     Sets the height to use for text output. A height of zero resets
 *     the original font size
 * Input:
 *     height: If height is 0, the basic
 */
void fscfont_set_size(int height)
{
    int i;
    float newwidth, newheight, factor;
    FONTCHAR_T *fc;


    if(height <= 0)
    {
        factor = 1.0;
        newheight = ActFont->height;
        newwidth  = ActFont->width;
    }
    else
    {
        /* Otherwise adjust all font rectangles */
        factor = (float)height / (float)ActFont->height;

        newheight = (float)ActFont->height * factor;
        newwidth  = (float)ActFont->width  * factor;
    }

    fc = &ActFont->fc[0];
    ActFont->factor = factor;

    for(i = 0; i < ActFont->numchar; i++)
    {
        fc[i].measure[1] = newheight;
        fc[i].measure[7] = newheight;
        fc[i].measure[4] = newwidth;
        fc[i].measure[6] = newwidth;
    }
}

/*
 * Name:
 *     fscfont_string_size
 * Description:
 *     Returns the size of the given string in pixels. The left upper edge
 *     is always at (0, 0)
 * Input:
 *     text: Text to get the size from
 *     rect: Rect struct for return of text size
 */
void fscfont_string_size(char *text, SDLGL_RECT_T *rect)
{
    char  letter;
    int   tab;
    int   width;
    FONTCHAR_T *fc;


    if(! text)
    {
        return;
    }

    width = 0;
    rect->w = 0;
    rect->h = ActFont->yspacing;
    fc = ActFont->fc;

    while(*text != 0)
    {
        letter = *text;

        switch(letter)
        {
            case '\t':
            case  '~':
                {
                    /* Use squiggle for tab */
                    tab = (width % FSCFONT_TABSIZE);
                    if(tab == 0)
                    {
                        tab = FSCFONT_TABSIZE;
                    }
                    width += tab;
                    break;
                }
            /* Newline */
            case '\n':
                /* Down a line           */
                rect->h += ActFont->yspacing;
                if(width > rect->w)
                {
                    /* New horizonal size 	 */
                    rect->w = width;
                }
                /* Back to start of line */
                width = 0;
             	break;

            default: {
                /* Normal letter    */
                width += fc[letter - ActFont->base].xspacing;
            }
        }

        text++;
    }

    /* now adjust to actual chosen font size */
    rect->w = (int)((ActFont->factor) * width);	        /* New horizontal size 	 */
    rect->h = (int)((ActFont->factor) * rect->h);
}

/*
 * Name:
 *     fscfont_print_labels
 * Description:
 *     Prints a list of labels at given positions on screen, using the actual
 *     color and the actual set font size.
 * Input:
 *      x, y:   Is added to the basic value
 *      list *: Pointer on a list to with info how to print the values
 */
void fscfont_print_labels(int x, int y, SDLGL_LABEL_T *plabel)
{
    SDLGL_RECT_T rect;


    fscfont_begin_text();

    /* ------- Rectangle is alway of size 'zero' ------ */
    rect.w = 0;
    rect.h = 0;

    while(plabel->val_type > 0)
    {
        /* -----  Set size of font, if needed -- */
        if(plabel->font_size > 0)
        {
            fscfont_set_size(plabel->font_size);
        }

        /* ------ Set color, if needed -------- */
        if(plabel->color_no > 0)
        {
            /* Color without transparency */
            glColor3ubv(&Label_Color[plabel->color_no * 3]);
        }

        rect.x = plabel->pos_x + x;
        rect.y = plabel->pos_y + y;

        fscfont_print_single_value(&rect,
                                   plabel->val_type,
                                   plabel->pdata,
                                   plabel->align);

        /* ------ Print next value ------- */
        plabel++;
    }

    fscfont_end_text();
}

/*
 * Name:
 *     fscfont_print_inputtext
 * Description:
 *     Prints a list of strings at given position on screen. Sets color
 *     and font size, if needed.
 * Input:
 *     pfields *: Pointer on a list of fields to draw a part as menu strings
 *     inp_type:  This type of fields is to print as menu strings
 */
void fscfont_print_inputtext(SDLGL_INPUT_T *pfields, char inp_type)
{
    /* Changing font size, based on rectangle size */
    static int font_size = 0;


    fscfont_begin_text();

    while(pfields->sdlgl_type != 0)
    {
        if(pfields->sdlgl_type == inp_type)
        {
            if(pfields->rect.h > 0 && pfields->rect.h != font_size)
            {
                /* Set font size to rectangle height, if height changed */
                font_size = pfields->rect.h;
                fscfont_set_size(font_size);
            }

            if(pfields->fstate & SDLGL_FSTATE_MOUSEOVER)
            {
                glColor3ubv(&Label_Color[6]);
            }
            else
            {
                glColor3ubv(&Label_Color[3]);
            }
        }

        fscfont_draw_string(&pfields->rect, pfields->pdata);

        pfields++;
    }

    /* Reset static variable */
    font_size = 0;

    fscfont_end_text();
}
