/*******************************************************************************
*   FOREISCR.C                                                                 *
*	   - Drawing and generation of input fields for the                        *
*        'Foreign policy Screen'                                               *
*                                                                              *
*   FREE SPACE COLONISATION             								       *
*       Copyright (C) 2002  Paul Mueller <pmtech@swissonline.ch>               *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*******************************************************************************/

/*******************************************************************************
* INCLUDES                                                                     *
*******************************************************************************/

#include "fscfe/fscfont.h"
#include "fscfe/drawtool.h"


#include "fscfe/foreiscr.h"

/*******************************************************************************
* DEFINES                                                                      *
*******************************************************************************/

#define FOREIGN_BASE 1

/*******************************************************************************
* ENUMS                                                                        *
*******************************************************************************/

#define FC_NONE       0x00
#define FC_CHOOSERACE 0x01
#define  FC_SPEAKTO   0x02

#define FCD_CHOOSESPEAKTO   ((char)SDLGL_TYPE_MAX + 10)

/*******************************************************************************
* DATA                                                                         *
*******************************************************************************/

static int chosen_race = 0;

/* Names for minor races */
static char MinorRaceNames[][24 + 1] = {

    "",
    "MinorRace1",
    "MinorRace2",
    "MinorRace3",
    "MinorRace4",
    "MinorRace5",
    "MinorRace6",
    "MinorRace7",
    "MinorRace8",
    "MinorRace9",
    "MinorRace10",
    "MinorRace11",
    "MinorRace12",
    "MinorRace13",
    "MinorRace14",
    "MinorRace15",
    "MinorRace16",
    "MinorRace17",
    "MinorRace18",
    "MinorRace19",
    "MinorRace20",
    "MinorRace21",
    "MinorRace22",
    "MinorRace23",
    "MinorRace24",
    "MinorRace25",
    "MinorRace26",
    "MinorRace27",
    "MinorRace28",
    "MinorRace29",
    "MinorRace30",
    "MinorRace31",
    "MinorRace32",
    "MinorRace33",
    "MinorRace34",
    "MinorRace35",
    "MinorRace36",
    ""

};


static char *DiplInfoStr[] = {

    "Military:",
    "Economy:",
    "Relations:"

};

static char *StrengthStr[] = {      /* For military and economy */

    "Weakest",
    "Weak",
    "Average",
    "Strong",
    "Strongest"

};

static char *RelationsStr[] = {

    "At War",
    "Hostile",
    "Cool",
    "Normal"
    "Warm",
    "Tight",
    "Allied"

};

static SDLGL_FIELD RelationFields[] = { /* Inputfields for relations screen */

    { 0 }

};

static SDLGL_FIELD StatsFields[] = {    /* Inputfields for stats screen     */

    { 0 }

};

static SDLGL_FIELD ReportFields[] = {    /* Inputfields for stats screen     */

    { 0 }

};

static SDLGL_FIELD TreatiesFields[] = {    /* Inputfields for stats screen     */

    { 0 }

};

static SDLGL_FIELD UPFields[] = {    /* Inputfields for stats screen     */

    { 0 }

};

static SDLGL_FIELD MinorRacesFields[] = {    /* Inputfields for stats screen     */

    { FCD_CHOOSESPEAKTO, {  74, 363, 61, 45 }, FOREIGN_BASE, FC_CHOOSERACE }, /* Choose speak to */
    { SDLGL_TYPE_BUTTON,       {  55, 436, 94, 25 },  FOREIGN_BASE, FC_SPEAKTO, "Speak To" },
    { SDLGL_TYPE_RB,     { 676, 569, 16, 16 }, FOREIGN_BASE, FC_NONE },
    { 0 }

};


/*******************************************************************************
* CODE                                                                         *
*******************************************************************************/

/*
 * Name:
 *     foreiscrOwnDrawFunction
 * Description:
 *     Displays the screen
 * Input:
 *      fields *: Pointer on fields to draw
 *      event *:  Event holding mouse position
 */
static void foreiscrOwnDrawFunction(SDLGL_FIELD *fields)
{

    if (fields) {

        while(fields -> sdlgl_type != 0) {

            switch (fields -> sdlgl_type) {

                case FCD_CHOOSESPEAKTO:
                    break;

                case SDLGL_TYPE_RB:
                    drawtoolRadioButton(fields, chosen_race);
                    break;

                case SDLGL_TYPE_BUTTON:
                    drawtoolButton(&fields -> rect, DRAWTOOL_BUTTON7, fields -> fstate);
                    break;


            }

            fields++;

        }

    }

}

/*
 * Name:
 *     foreiscrDrawRelations
 * Description:
 *
 * Input:
 *     None
 */
static void foreiscrDrawRelations(void)
{
}

/*
 * Name:
 *     foreiscrDrawStats
 * Description:
 *
 * Input:
 *     None
 */
static void foreiscrDrawStats(void)
{
}

/*
 * Name:
 *     foreiscrDrawReport
 * Description:
 *
 * Input:
 *     None
 */
static void foreiscrDrawReport(void)
{
}

/*
 * Name:
 *     foreiscrDrawTreaties
 * Description:
 *
 * Input:
 *     None
 */
static void foreiscrDrawTreaties(void)
{
}

/*
 * Name:
 *     foreiscrDrawUnitedPlanets
 * Description:
 *
 * Input:
 *     None
 */
static void foreiscrDrawUnitedPlanets(void)
{
}

/*
 * Name:
 *     foreiscrDrawMinorRaces
 * Description:
 *
 * Input:
 *     None
 */
static void foreiscrDrawMinorRaces(void)
{
}

/* ========================================================================== */
/* ======================= PUBLIC FUNCTIONS ================================= */
/* ========================================================================== */

/*
 * Name:
 *     foreiscrSetDisplay
 * Description:
 *     Returns the input fields needed for the given subscreen in the foreign
 *     relations screen.
 * Input:
 *     subscr: Number of subscreen to get the inputfields for
 */
SDLGL_FIELD *foreiscrSetDisplay(int subscr)
{

    switch(subscr) {

        case FOREISCR_RELATIONS:
            return &RelationFields[0];

        case FOREISCR_STATS:
            return &StatsFields[0];

        case FOREISCR_REPORT:
            return &ReportFields[0];

        case FOREISCR_TREATIES:
            return &TreatiesFields[0];

        case FOREISCR_UNITEDPLANETS:
            return &UPFields[0];

        case FOREISCR_MINORRACES:
            return &MinorRacesFields[0];

    }

    return &RelationFields[0];

}

/*
 * Name:
 *     foreiscrDraw
 * Description:
 *     Draws the given subscreen (static part of display
 * Input:
 *     subscr: Number of subscreen to get the inputfields for
 */
void foreiscrDraw(int subscr)
{

    switch(subscr) {

        case FOREISCR_RELATIONS:
            foreiscrDrawRelations();
            break;

        case FOREISCR_STATS:
            foreiscrDrawStats();
            break;

        case FOREISCR_REPORT:
            foreiscrDrawReport();
            break;

        case FOREISCR_TREATIES:
            foreiscrDrawTreaties();
            break;

        case FOREISCR_UNITEDPLANETS:
            foreiscrDrawUnitedPlanets();
            break;

        case FOREISCR_MINORRACES:
            foreiscrDrawMinorRaces();
            break;

    }

}



