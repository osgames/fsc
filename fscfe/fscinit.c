/*******************************************************************************
*  FSCINIT.C                                                                   *
*	    - Does all initialization work, load/delete graphics an rules	       *
*                                                                              *
*	FREE SPACE COLONISATION               	                                   *
*   Copyright (C) 2002  Paul Mueller <pmtech@swissonline.ch>                   *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*******************************************************************************/

/*******************************************************************************
* INCLUDES								                                       *
*******************************************************************************/


#include "fscfe/sdlgltex.h"

#include "sdlglcfg.h"           /* Read in configuration / rules file   */
#include "code/unittype.h"      /* Data structs to read unit-types      */
#include "code/improve.h"       /* Data structs to read improvements    */
#include "code/techlist.h"      /* Data struct to read in technologies  */

#include "fscfe/fscinit.h"

/*******************************************************************************
* DEFINES      							                                       *
*******************************************************************************/

#define RENDER_STDFLAGS (SDLGLTEX_ICONBORDER | SDLGLTEX_ICONALPHA)

/*******************************************************************************
* DATA       							                                       *
*******************************************************************************/

static const char StarIconName[] = "data/staricon.bmp";
static SDLGLTEX_ICONCREATEINFO StarIcons[] = {

    { FSCINIT_STARICONMAP,   7, {   0,   0, 64, 64 },  7, RENDER_STDFLAGS },
    { FSCINIT_UNIT,          7, {   0,  65, 64, 64 },  7, RENDER_STDFLAGS },
    { FSCINIT_KNOB,          4, {   0, 130, 64, 64 },  4, RENDER_STDFLAGS },
    { FSCINIT_IMPERIUMSIGN,  6, {   0, 195, 64, 64 },  6, RENDER_STDFLAGS },
    { FSCINIT_PLANETS,      18, {   0, 260, 64, 64 },  9, RENDER_STDFLAGS },
    { FSCINIT_CURSOR,        5, {   0, 390, 64, 64 },  5, RENDER_STDFLAGS },
    { FSCINIT_BACKTEXTURE,   6, { 195, 130, 64, 64 },  6, RENDER_STDFLAGS },
    { FSCINIT_SMALLMAPICON, 12, { 195, 130, 32, 32 }, 12, RENDER_STDFLAGS },
    { 0 }                               /* Signs end of array */

};

/* ========================================================================== */
/* ======================== PUBLIC FUNCTIONS	============================= */
/* ========================================================================== */

/*
 * Name:
 *      fscinitLoadIcons
 * Description:
 *      Loads all the icons needed for drawing of the map
 * Input:
 *     None
 */
void fscinitLoadIcons(void)
{

    sdlgltexLoadIcons(StarIconName, 0xFF00FF, &StarIcons[0]);

}

/*
 * Name:
 *      fscinitDeleteIcons
 * Description:
 *      Deletes all the icons needed for drawing of the map
 * Input:
 *     None
 */
void fscinitDeleteIcons(void)
{

     sdlgltexFreeIcons();

}

/*
 * Name:
 *      fscinitReadRules
 * Description:
 *      Reads in the rules for the game
 * Input:
 *     None
 */
void fscinitReadRules(void)
{

    int nextblock;
    
    
    nextblock = sdlglcfgOpenFile("data/fscrules.txt", "@");
    if (! nextblock) {

        return;

    }
    
    while(nextblock) {
    
        if (sdlglcfgIsActualBlockName("UNITS")) {

            nextblock = sdlglcfgReadRecordLines(unittypeGetDataDesc(), 0);

        }
        else if (sdlglcfgIsActualBlockName("UNITS_EFFECT")) {

            nextblock = sdlglcfgReadRecordLines(unittypeGetEffectDataDesc(), 0);

        }
        else if(sdlglcfgIsActualBlockName("IMPROVE")) {

            nextblock = sdlglcfgReadRecordLines(improveGetDataDesc(), 0);

        }
        else if(sdlglcfgIsActualBlockName("IMPROVE_EFFECT")) {

            nextblock = sdlglcfgReadRecordLines(improveGetEffectDataDesc(), 0);

        }
        else if(sdlglcfgIsActualBlockName("TECH")) {

            nextblock = sdlglcfgReadRecordLines(techlistGetDataDesc(), 0);

        }
        else {

            nextblock = sdlglcfgSkipBlock();

        }

    }

    sdlglcfgCloseFile();

    /* ------ Now make some additional initalisation --------- */
    techlistInit();     /* _MUST_ be set for UNITTYPES 'nil' */

}
