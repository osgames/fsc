/* *****************************************************************************
*  LANGUAGE.C                                                                  *
*	    - Functions for usage of laungauge specific strings and messages       *
*                                                                              *
*	FREE SPACE COLONISATION               	                                   *
*       Copyright (C) 2002-2010  Paul Mueller <pmtech@swissonline.ch>          *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*******************************************************************************/

/* *****************************************************************************
* INCLUDES    							                                       *
*******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <memory.h>

#include "sdlglcfg.h"       /* Read in text files               */
#include "fscmap.h"         /* Sector-Name                      */
#include "fsctool.h"        /* Scan fro strings                 */
#include "game.h"           /* Get the date                     */    
#include "msgtostr.h"       /* Generate a string from message   */


#include "language.h"       /* Own header                       */

/* *****************************************************************************
* DEFINES     							                                       *
*******************************************************************************/

#define FILE_TYPE_MENU  0x00
#define FILE_TYPE_LABEL 0x01
#define FILE_TYPE_MSG   0x02

/*******************************************************************************
* TYPEDEFS 								                                       *
*******************************************************************************/

typedef struct {

    char *name;			/* e.g. "ORDER"                     */
    char *first_string;	/* Pointer on first string          */
	int num_string;		/* Number of strings in this list   */

} LANGUAGE_LABEL;


/* *****************************************************************************
* DATA       							                                       *
*******************************************************************************/

/* TODO: Make a list of 'block_names' for different values
   block_name: Name of block, if needed (for menu-strings and labels)
*/
/* TODO: Build indexed table with pointer on first string of any type at
         loading time
*/

static char *pStrings;

static SDLGLCFG_FILE text_files[] = {
  { "menu.txt" },
  /* { "label.txt" }, */
  /* { "message.txt" }, */
  { "" }
};

/*
static LANGUAGE_LABEL LanguageLabels[] = {
  { "" },
  { "ADVISOR" },
  { "STATNAME" },
  { "ORDERS" },
  { "EFFECT" },
  { "RANGE" },
  { "UNITABILITY" },
  { "MONTH" },
  { " " },
  { " " },
  { " " },
  { " " },
  { " " },
  { " " },
  { "" },
};
*/

/* TODO: Handling of 'rulename.txt' which should overwrite the names of
   UNITTYPEs, TECHs and IMPROVEments after the rules are loaded */

static char SectorStr[] = "Sector"; 
static char AdvisorNames[] = 
    "Domestic Advisor\0" 
    "Trade Advisor\0"     
    "Military Advisor\0"    
    "Foreign Advisor\0"   
    "Cultural Advisor\0" 
    "Science Advisor\0" 
    "Galaxy Wonders\0"  
    "Histography\0"      
    "Planet Management\0"
    "Demography\0"      
    "Statistics\0"    
    "\0"
    "\0"; 

static char UnitOrderList[] =
    "Idle\0"
    "Sentry\0"
    "Guard\0"
    "Auto-Attack\0"
    "Auto-Retreat\0"
    "Patrol\0"
    "Border-patrol\0"
    "Goto\0"
    "Explore\0"
    "Build\0"
    "Terraform\0"
    "Colonize\0"
    "Cancel\0"
    "Attack\0"
    "Defend\0"
    "Traderoute\0"
    "\0";

static char LanguageUnitAbilityName[] = 
    "Unknown\0"         
    "Military\0"        
    "Colonize\0"        
    "Transport\0"       
    "Diplomacy\0"       
    "Trade\0"            
    "Scout\0"           
    "Construct\0"       
    "Outpost\0"         
    "Starbase\0"       
"\0";

static char RangeName[]  = "Short\0Middle\0Long\0";
static char MonthStr[]   = "Jan\0Feb\0Mar\0Apr\0May\0Jun\0Jul\0Aug\0Sep\0Oct\0Nov\0Dec";
static char StatNames[] = 
    "Population\0"
    "Military\0"
    "Economy\0"
    "Research\0"
    "Influence\0"
    "Diplomacy\0"
    "Money\0"
    "\0";

static char EffectNames[] = 

    "Food\0"
    "Resources\0"
    "Economy\0"
    "Military\0"
    "Social\0"
    "Stock\0"
    "Research\0"
    "Taxes\0"
    "Luxury\0"
    "-\0"
    "-\0"
    "Morale\0"
    "Approval\0"
    "Net Income\0"
    "Expenses\0"
    "Assimilate\0"
    "-\0"
    "-\0"
    "-\0"
    "-\0"
    "-\0"
    "Weapons\0"
    "Defense\0"
    "Speed\0"
    "Pop. Growth\0"
    "Diplomacy\0"
    "Sensors\0"
    "Espionage\0"
    "Influence\0"
    "Navigation\0"
    "Hitpoints\0"
    "Repair\0"
    "Prestige\0"
    "Starships\0"
    "Soldiering\0"
    "-\0"
    "-\0"
    "Range\0"
    "Warfare\0"
    "Planet Quality\0"
    "-\0"
    "Propaganda\0"
    "Terraform\0"
    "Outpost\0"
    "Trade\0"
    "Population\0"
    "\0\0";


/* One big buffer for all messages */
static char MsgStr[] =
"@0102 $UNIT is stucked on map"
"@0103 Unit $UNIT tries to move out of range."
"@0104 $UNIT has no moves left"
"@0105 Tried to move $UNIT of wrong ownership"
"@0201 $PLANET has built $IMPROVE. "
"@0202 $PLANET has built $UNIT."
"@0203 On planet $PLANET an $IMPROVE outpost-is built."
"@0204 Research for $TECH is completed. We need a new task."
"@0205 On planet $PLANET terraforming is completed."
"@0";

/* *****************************************************************************
* CODE       							                                       *
*******************************************************************************/

/*
 * Name:
 *     languagePopulationValue
 * Description:
 *     Generates a string for a planets population using the given number.
 *     Returns a pointer on a static string holding this string
 * Input:
 *     population: In millions
 *     value *:    Where to return the string
 */
void languagePopulationValue(int population, char *value)
{

    int pop, poppart;
    char poppartstr[10];


    if (population < 1000) {

        sprintf(value, "%d M", population); /* In Millions */

    }
    else {

        pop     = population / 1000;    /* Full billions    */
        poppart = population % 1000;    /* Part billions    */

        sprintf(poppartstr, "%03d", poppart);
        poppartstr[2] = 0;                      /* Maximum two digits */
        sprintf(value, "%d.%s B", pop, poppartstr);

    }

}

/*
 * Name:
 *     languageFindBlock
 * Description:
 *     Find the block with given  
 * Input:
 *     fdesc *: In this file desriptor
 *     sign *:  Pointer on sign to find 
 * Output: 
 *     Block found yes/no 
 */
static int languageFindBlock(SDLGLCFG_FILE *fdesc, char *sign)
{

    int length;
    char *pact, *pend;
    
    
    if (fdesc -> size > 0) {
    
        pact   = fdesc -> buffer;
        pend   = pact + fdesc -> size;
        length = strlen(sign);
        
        while (pact < pend) {
        
            pact = memchr(pact, '@', pend - pact);
            if (pact) {
            
                pact++;
                if (! strncmp(sign, pact, length)) {
                                    
                    pStrings = pact + length + 1;
                    
                    return 1;
                
                }
                
            }
 
        }

    }        
    
    pStrings = "";
    
    return 0;

}

/*
 * Name:
 *     languageGetString
 * Description:
 *     Returns the next string from function 'languageFindBlock' 
 * Input:
 *     None  
 * Output:
 *     Pointer on string. Zero lenght string, if no string available
 */
static char *languageGetString(void)
{

    char *found_str;       
     
            
    if (*pStrings > 0) {
    
        found_str = pStrings;
        pStrings  = strchr(pStrings, 0) + 1;
        
    }
    else {
    
        found_str = "";

    }
    
    return found_str; 

}

/* ========================================================================== */
/* =========================== PUBLIC FUNCTION(S) =========================== */
/* ========================================================================== */

/*
 * Name:
 *     languageInit
 * Description:
 *     Loads all buffers needed for the different types of strings
 *     Main function
 * Input:
 *     None
 */
void languageInit(void)
{

    SDLGLCFG_FILE *fdesc;
    char *pact, *pend;


    /* TODO: Load all language files for the different types of strings */
    fdesc = &text_files[0];

    while (fdesc -> filename[0] > 0) {

        sdlglcfgLoadFile("data", fdesc);
        
        if (fdesc -> size > 0) {
    
            /* Now prepare it: Change Lines to strings */
            pact   = fdesc -> buffer;
            pend   = pact + fdesc -> size;
            
            while (pact < pend) {
        
                pact = memchr(pact, '\n', pend - pact);
                
                if (pact) {
            
                    *pact = 0;
                    pact++;
                                   
                }
                else {

                    break;

                }
                
            }
 
        }
 
        fdesc++;

    }

}

/*
 * Name:
 *     languageExit
 * Description:
 *     Releases all memory buffers loaded by 'languageInit()'
 * Input:
 *     None
 */
void languageExit(void)
{

    SDLGLCFG_FILE *fdesc;


    fdesc = &text_files[0];

    while (fdesc -> filename[0] > 0) {

        sdlglcfgFreeFile(fdesc);
        fdesc++;

    }

}

/*
 * Name:
 *     languageMsgToString
 * Description:
 *     Translates given message into human readable form, using the strings loaded from file
 * Input:
 *     msg *:     Pointer on message to translate
 *     msg_buf *: Where to return the message in human readable form
 *     buf_size:  Size of buffer
 */
char *languageMsgToString(MSG_INFO *msg, char *msg_buf, int buf_size)
{

    SDLGLCFG_FILE *fdesc;
    char raw_str[256];


    fdesc = &text_files[FILE_TYPE_MSG];

    if (fdesc -> size > 0) {

    }

    /* TODO: Call translation function from 'msgtostr' with correct data source */
    if (*msgtostrFindMsgStr(msg -> num, MsgStr, raw_str, 254) > 0) {

        msgtostrTranslate(msg, raw_str, msg_buf, buf_size);
        return msg_buf;

    }


    return "";

}

/*
 * Name:
 *     languageLabel
 * Description:
 *     Return string with 'number' from buffer 'which
 * Input:
 *     which:    Kind of string (source buffer)
 *     label_no: Number of label of given kind
 */
char *languageLabel(char which, int label_no)
{

    char *psrc;


    psrc    = "";
    /*
    SDLGLCFG_FILE *fdesc;


    fdesc = &text_files[FILE_TYPE_LABEL];

    if (fdesc -> size > 0) {

        / * TODO: Return string with 'number' from buffer 'which * /

    }
    */
    if (label_no >= 0) {
    
        switch(which) {

            case LANGUAGE_LABEL_ADVISOR:
                if (label_no > 0) {
                    
                    label_no--;
                    psrc = AdvisorNames;

                }
                break;
                
            case LANGUAGE_LABEL_STATS:
                if (label_no > 0) {
        
                    label_no--;    
                    psrc = StatNames;    

                }
                break;
                
            case LANGUAGE_LABEL_UNITORDER:
                if (label_no < 16) {

                    psrc = UnitOrderList;    

                }
                break;
                
            case LANGUAGE_LABEL_EFFECT:
                if (label_no < 45) {

                    psrc = EffectNames;

                }
                break;
                
            case LANGUAGE_LABEL_RANGE:
                if (label_no < 3) {

                    psrc = RangeName;

                }
                break;
                
            case LANGUAGE_LABEL_ABILITY:
                if (label_no < 10) {

                    psrc = LanguageUnitAbilityName;

                }
                break;
                
            default:
                return "";

        }
        
    }   /*  if (number >= 0) */

    return fsctoolStrFromStrList(psrc, label_no);    

}

/*
 * Name:
 *     languageSectorName
 * Description:
 *     Returns the name of given sector
 * Input:
 *     pos:      Map position to generate the sector name for
 *     buffer *: Where to print to the sector name
 * Output:
 *     Pointer on sector name generated.
 */
char *languageSectorName(int pos, char *buffer)
{

    char name[80];
    char posname[80];


    if (fscmapGetSectorName(pos, name, posname)) {      /* Has a 'real' name */

        sprintf(buffer, "%s-%s (%s)", name, SectorStr, posname);
        /* 'blahblah'-Sector (x-y) */

    }
    else {

        sprintf(buffer, "%s %s", SectorStr, posname); /* "Sector x-y" */

    }

    return buffer;

}

/*
 * Name:
 *     languageSpecialString
 * Description:
 *     Returns some special strings (generated labels) 
 * Input:
 *     which:    Number of label to return  
 *     value:    For string, if needed  
 *     buffer *: Where to return the string asked for
 * Output:
 *     Pointer on string asked for
 */
char *languageSpecialString(char which, int value, char *buffer)
{

    int month, year;

    
    switch(which) {
    
        case LANGUAGE_SPEC_DATE:
            gameGetDate(&year, &month);
            sprintf(buffer, "%s %d", fsctoolStrFromStrList(MonthStr, month), year);
            break;
            
        case LANGUAGE_SPEC_POPULATION:
            languagePopulationValue(value, buffer);
            break;
    
    }
    
    return "";

}

/*
 * Name:
 *     languageFindMenu
 * Description:
 *     Returns 1, if a block with given name is found in menu-block 
 * Input:
 *     sign *:   To find in buffer, start of block
 * Output:
 *     Block found yes / no 
 */
int languageFindMenu(char *sign)
{

    return languageFindBlock(&text_files[0], sign);

}

/*
 * Name:
 *     languageMenuString
 * Description:
 *     Retunrs a pointer on a string from block found in 'languageFindMenu'  
 * Input:
 *     None 
 * Output:
 *     Pointer on string (an empty one, if no more found) 
 */
char *languageMenuString(void)
{

    return languageGetString();

}
