/* *****************************************************************************
*  LANGUAGE.H                                                                  *
*	    - Functions for usage of laungauge specific strings and messages       *
*                                                                              *
*	FREE SPACE COLONISATION               	                                   *
*       Copyright (C) 2002-2010  Paul Mueller <pmtech@swissonline.ch>          *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*******************************************************************************/

#ifndef _FSC_LANGUAGE_H_
#define _FSC_LANGUAGE_H_

/* *****************************************************************************
* INCLUDES    							                                       *
*******************************************************************************/

#include "msg.h"

/*******************************************************************************
* ENUMS                                                                        *
*******************************************************************************/

typedef enum {

    LANGUAGE_LABEL_NONE      = 0,
    LANGUAGE_LABEL_ADVISOR   = 1,
    LANGUAGE_LABEL_STATS     = 2,
    LANGUAGE_LABEL_UNITORDER = 3,
    LANGUAGE_LABEL_EFFECT    = 4,
    LANGUAGE_LABEL_RANGE     = 5,
    LANGUAGE_LABEL_ABILITY   = 6 

} LANGUAGE_LABEL_NAME;

/* *****************************************************************************
* DEFINES      							                                       *
*******************************************************************************/

#define LANGUAGE_SPEC_DATE          0
#define LANGUAGE_SPEC_POPULATION    1

/* *****************************************************************************
* CODE       							                                       *
*******************************************************************************/

void languageInit(void);
void languageExit(void);
char *languageMsgToString(MSG_INFO *msg, char *str_buf, int buf_size);
char *languageLabel(char which, int number);
char *languageSectorName(int pos, char *buffer);
char *languageSpecialString(char which, int value, char *buffer);
int languageFindMenu(char *sign);
char *languageMenuString(void);

#endif  /* _FSC_LANGUAGE_H_ */