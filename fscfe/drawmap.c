/*******************************************************************************
*  MAPDRAW.C                                                                   *
*	    - All map drawing functions                                   	       *
*                                                                              *
*  FREE SPACE COLONISATION                                                     *
*      (c)2002 - 2017 Paul Mueller <muellerp61@bluewin.ch>                     *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*******************************************************************************/

/*******************************************************************************
* INCLUDES								                                       *
*******************************************************************************/

#include "sdlgl.h"


#include "../code/fscmap.h"
#include "../code/fsctool.h"

/*
#include "sdlgltex.h"

#include "fscfont.h"
#include "fscinit.h"

#include "game.h"
#include "star.h"
#include "nation.h"
#include "unittype.h"
*/


#include "drawtool.h"


#include "drawmap.h"

/*******************************************************************************
* DEFINES 							                                           *
*******************************************************************************/

/* ----- Different cursors ------ */
#define CURSOR_FIX       0
#define CURSOR_GREEN     1
#define CURSOR_RED       2
#define CURSOR_BLUE      3
#define CURSOR_OWNERRECT 4

#define DRAWMAP_CURSORTIME   0.25
#define DRAWMAP_ANIM_TIME    0.50
#define DRAWMAP_STARNAMESIZE 15    /* Before it was 10 */

/* === Different definitions for test purposes === */
#define FSC_TEX_STAR   1
#define FSC_TEX_PLANET 2
#define FSC_TEX_UNIT   3
#define FSC_TEX_CURSOR  4

/*******************************************************************************
* TYPEDEFS	      							                                   *
*******************************************************************************/

typedef struct
{
    float drawpos_x,
          drawpos_y;
    float src_pos_x,
          src_pos_y;
    float clock,            /* The clock for the animation          */
          clocktime;        /* Ticks per animation step             */
    float pixeldist_x,      /* Distance in pixels in each direction */
          pixeldist_y;
    int unit_no;            /* This unit is animated                */
    int on;                 /* Animation is active yes/no           */
}
DRAWMAP_ANIM_T;

typedef struct
{
    int on;
    float clock;
}
DRAWMAP_CURSORDATA_T;

/*******************************************************************************
* DATA  							                                           *
*******************************************************************************/

static DRAWMAP_ANIM_T Animation;            /* Data for animation of unit   */
static DRAWMAP_CURSORDATA_T Mapdraw_Cursor; /* Date for drawing of cursor   */

/*******************************************************************************
* CODE								                                           *
*******************************************************************************/

/*
 * Name:
 *     drawmap_minimap_stars
 * Description:
 *     Draws the stars for the minimap
 * Input:
 *     prect *: Rectangle of minimap
 *     pmap_info *: Pointer on info about extent of map
 */
static void drawmap_minimap_stars(SDLGL_RECT_T *prect, FSCMAP_INFO_T *pmap_info)
{
    FSCMAP_TILEINFO_T ti[1000], *pti;
    float half_rectsize;


    half_rectsize = (float)prect->w / (float)pmap_info->map_w / 2.0;

    sdlgl_set_color(SDLGL_COL_WHITE);

    glPushAttrib(GL_POINT_BIT);
    /* Smooth points    */
    glEnable(GL_POINT_SMOOTH);
    glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
    glPointSize(half_rectsize * 2);

    glBegin(GL_POINTS);

        /* Get the info about the stars */
        if(fscmap_get_mapinfo(1, &ti[0], 998, FSCMAP_FINFO_STAR, 0))
        {
            pti = ti;

            while(pti->pos >= 0)
            {
                drawtool_set_colorno(1, pti->owner);
                glVertex2f(pti->draw_x + half_rectsize, pti->draw_y + half_rectsize);

                pti++;
            }
        }

    glEnd();
    glPopAttrib();
}

    /* =============== MAP draw help functions =============== */

/*
 * Name:
 *     drawmap_owner_info
 * Description:
 *     Draws the fog of war and the map grid
 * Input:
 *     prect *:       To draw the map into
 *     visitile_size: Size of visible tile
 */
static void drawmap_owner_info(SDLGL_RECT_T *prect, int visitile_size)
{
    FSCMAP_TILEINFO_T ti[1000], *pti;


    if(fscmap_get_mapinfo(1, &ti[0], 998, FSCMAP_FINFO_OWNER, 0))
    {
        pti = ti;

        while(pti->pos >= 0)
        {
            drawtool_set_colorno(1, pti->owner);
            /* @TODO: Draw transparent rectangle in the owners color */
            /*
            glVertex2f(pti->draw_x + half_rectsize, pti->draw_y + half_rectsize);
            */

            pti++;
        }
    }
}

/*
 * Name:
 *     drawmap_star_background
 * Description:
 *     Draws the fog of war and the map grid
 * Input:
 *     rect *:       To draw background into
 */
static void drawmap_star_background(SDLGL_RECT_T *prect)
{
    SDLGL_RECT_T star_rect;
    int i, r;
    int base_color_no, starcol_no;


    /* Draw always the same */
    fsctool_rand_set_seed(749851);

    star_rect.w = 6;
    star_rect.h = 6;
    base_color_no = drawtool_get_colorno(4, 0);
    starcol_no    = fsctool_rand_no(11);
    starcol_no    = (starcol_no < 5) ? 5 : (starcol_no - 5);

    for(i = 0; i < 94; i++)
    {
        /* @TODO: Use dynamic screen size */
        star_rect.x = fsctool_rand_no(prect->w);
        star_rect.y = fsctool_rand_no(prect->h);
        r = fsctool_rand_no(3);

        drawtool_circle(&star_rect, r, base_color_no + fsctool_rand_no(6), 0x03);
    }
}

/*
 * Name:
 *     drawmap_base
 * Description:
  *     Draws the fog of war and the map grid
 * Input:
 *     prect *:       Rectangle to draw map into
 *     pmapinfo *:    Pointer on viewport extent
 *     visitile_size: Size of visible tile
 *     gridon:        Draw grid, yes no
 */
static void drawmap_base(SDLGL_RECT_T *prect, FSCMAP_INFO_T *pmapinfo, int visitile_size, int gridon)
{
    SDLGL_RECT_T gridrect;
    int sectorborder_x, sectorborder_y;
    int tiles_sector;
    int x, y;


    if(gridon)
    {
        gridrect.y = prect->y;
        gridrect.w = visitile_size;
        gridrect.h = visitile_size;

        for(y = 0; y < pmapinfo->vp_h; y++)
        {
            gridrect.x = prect->x;

            for(x = 0; x < pmapinfo->vp_w; x++)
            {
                /* Draw the map grid rectangle, if asked for */
                drawtool_drawrect_colno(&gridrect, SDLGL_COL_LIGHTGREY, 0);
                gridrect.x += visitile_size;
            }

            gridrect.y += visitile_size;
        }
    }

    /* ----- Now draw possible visible sector borders -------- */
    #if 0
    tiles_sector = pmapinfo->map_w / pmapinfo->sec_w;

    drawtool_set_colorno(3, 1);

    sectorborder_x = tiles_sector - (pmapinfo->vp_x % tiles_sector);
    sectorborder_y = tiles_sector - (pmapinfo->vp_y % tiles_sector);

    x = (sectorborder_x * visitile_size) + prect->x;
    y = (sectorborder_y * visitile_size) + prect->y;

    glBegin(GL_LINES);
        /* --- First the vertical sector border --- */
        glVertex2i(x, prect->y);
        glVertex2i(x, prect->y + (pmapinfo->vp_h * visitile_size));
        /* --- Second the horizontal sector border --- */
        glVertex2i(prect->x, y);
        glVertex2i(prect->x + (pmapinfo->vp_w * visitile_size), y);
    glEnd();
    #endif
    /* Draw debug info, if needed */
    drawmap_owner_info(prect, visitile_size);
}

/*
 * Name:
 *     mapdraw_range_borders
 * Description:
 *     Draws the border of the range for the players ship
 * Input:
 *     maprect *:    Visible rectangle in pixel for map
 *     map *:        Pointer on description of viewport
 *     player_no:    For this nation
 *     range_no:     This range, if > 0, otherwise draw all of them
 *     visitilesize: Size of a visibile tile
 */
static void mapdraw_range_borders(SDLGL_RECT_T *pmaprect,
                                  FSCMAP_INFO_T *pmapinfo,
                                  char player_no,
                                  char range_no,
                                  int visitile_size)
{
    FSCMAP_TILEINFO_T ti[1000], *pti;
    SDLGL_RECT_T drawrect;


    /* ---- Init draw rectangle ------ */
    drawrect.x = pmaprect->x;
    drawrect.y = pmaprect->y;
    drawrect.w = visitile_size;
    drawrect.h = visitile_size;

    /* DRAWTOOL_COLOR_RANGE */
    /* range_color = drawtool_get_colorno(2, range_no); */

    if(fscmap_get_mapinfo(player_no, ti, 998, FSCMAP_FINFO_RANGE, 1))
    {
        pti = ti;

        while(pti)
        {
            /* @FIXME: Draw all ranges, if 'GameInfo.drawranges' is on  */
            drawrect.x = ti->draw_x * visitile_size;
            drawrect.y = ti->draw_y * visitile_size;

            drawtool_drawrect_sides(&drawrect, ti->range_border);

            pti++;
        }
    }
}

#if 0
/*
 * Name:
 *     mapdrawAddUnitToDisplayList
 * Description:
 *     Add the given unit to the display list. It is assumed that the
 *     first given SDLGL_INPUT_T points on an empty field.
 * Input:
 *     did *:      Pointer on actual did, holding position and size of texture
 *     unit_no:    Number of unit to display
 *     unit_type:  Type of unit to draw
 *     unit_owner: Owner of drawn unit
 */
static SDLGL_INPUT_T *mapdrawAddUnitToDisplayList(SDLGL_INPUT_T *did,
                                                int unit_no,
                                                char unit_type,
                                                char unit_owner)
{

    if(unit_no == Animation.unit_no)
    {
        /* If unit is animated, set the position to the animated unit */
        did->rect.x = Animation.drawpos_x;
        did->rect.y = Animation.drawpos_y;
    }

    did->sdlgl_type = SDLGL_TYPE_TEXTURE;
    did->val_min    = FSC_TEX_UNIT;
    did->val_max    = unittype_get(unit_type)->iconno;

    /* Position and size are held in this SDLGL_INPUT_T */
    did[1].rect.x = did->rect.x;  /* Copy size to next SDLGL_INPUT_T */
    did[1].rect.y = did->rect.y;
    did[1].rect.w = did->rect.w;
    did[1].rect.h = did->rect.h;

    did++;

    /* Set the color rectangle */
    drawtoolGetColor(DRAWTOOL_COLOR_RACE, unit_owner, &did->color[0]);

    did->sdlgl_type = SDLGL_TYPE_TEXTURE;
    did->val_min    = FSCINIT_CURSOR;       /* 'tex' Name of texture group */
    did->val_max    = CURSOR_OWNERRECT;     /* 'subtex' Position in texture group */

    /* Copy size to next SDLGL_INPUT_T */
    did[1].rect.x = did->rect.x;
    did[1].rect.y = did->rect.y;
    did[1].rect.w = did->rect.w;
    did[1].rect.h = did->rect.h;

    did++;

    return did;
}
#endif

#if 0
/*
 * Name:
 *     mapdrawAddCursorToDisplayList
 * Description:
 *     Add the cursor if it's visible at all.
 * Input:
 *     did:  Pointer on actual did, holding position and size of textured
 *           tile
 *     moves_left: To help choose the color of the cursor < 0: Don't display cursor
 * Last change:
 *     2009-12-25 / bitnapper
 */
static SDLGL_INPUT_T *mapdrawAddCursorToDisplayList(SDLGL_INPUT_T *did, char moves_left)
{
    if(! Animation.on)
    {
        /* Draw the cursor if no animation is on */
        did->sdlgl_type  = SDLGL_TYPE_TEXTURE;
        did->val_min     = FSC_TEX_CURSOR;        /* Number of iconlist */

        if(moves_left > 0)
        {
            if(Mapdraw_Cursor.on)
            {
                /* Draw only if cursor is on (support of blinking) */
                did->val_max = CURSOR_GREEN;
            }
            else
            {
                /* Cursor is off (blinking) */
                return did;
            }
        }
        else
        {
            did->val_max        = CURSOR_RED;
            Mapdraw_Cursor.on    = 1;
            Mapdraw_Cursor.clock = 0.0;
        }

        did++;

    } /* if(! Animation.on) */

    return did;
}
#endif

        /* ======== Animation help functions ======= */

/*
 * Name:
 *     drawmap_run_animation
 * Description:
 *     Does an update on the animation data.
 * Input:
 *     sec_passed: Seconds passed since last call
 */
static void drawmap_run_animation(float sec_passed)
{
    Animation.clock += sec_passed;     /* The clock for the animation   */

    if(Animation.clock < Animation.clocktime)
    {
        /* Animation running */
        Animation.drawpos_x = Animation.src_pos_x +
                             (Animation.pixeldist_x * Animation.clock / Animation.clocktime);
        Animation.drawpos_y = Animation.src_pos_y +
                             (Animation.pixeldist_y * Animation.clock / Animation.clocktime);
    }
    else
    {
        /* Stop the animation   */
        Animation.on      = 0;
        /* No unit animated     */
        Animation.unit_no = 0;
    }
}

/* ========================================================================== */
/* ======================== PUBLIC FUNCTIONS	============================= */
/* ========================================================================== */

/*
 * Name:
 *     drawmap_minimap
 * Descritption:
 *     Draws the minimap into the given rectangle
 *     The minimaps sectors are assumed to be squares.
 * Input:
 *     rect *:    Pointer on rect to draw the minimap into
 *     player_no: For this player
 */
void drawmap_minimap(SDLGL_RECT_T *prect, char player_no)
{
    FSCMAP_INFO_T map_info;
    int x, y, i;
    float sector_size;
    float tile_size;
    float frect[8];                 /* Four edges of rectangle */


    fscmap_get_info(&map_info);

    /* ------- Draw the background and add a border --------- */
    drawtool_drawrect_colno(prect, SDLGL_COL_BLACK, 1);

    /* ------ Draw the stars --------- */
    drawmap_minimap_stars(prect, &map_info);

    /* Set color for map grid */
    drawtool_set_colorno(3, 0);

    sector_size = (float)prect->w / (float)map_info.sec_w;
    tile_size   = (float)prect->w / (float)map_info.map_w;

    for(y = 0; y < map_info.sec_h; y++)
    {
        frect[1] = (float)prect->y;
        frect[1] += y * sector_size;
        frect[3] = frect[1] + sector_size;
        frect[5] = frect[1] + sector_size;
        frect[7] = frect[1];

        for(x = 0; x < map_info.sec_w; x++)
        {
            frect[0] = (float)prect->x;
            frect[0] += x * sector_size;
            frect[2] = frect[0];
            frect[4] = frect[0] + sector_size;
            frect[6] = frect[0] + sector_size;

            /* Draw a rectangle for each sector */
            glBegin(GL_LINE_LOOP);
                for(i = 0; i < 8; i += 2)
                {
                    glVertex2fv(&frect[i]);
                }
            glEnd();
        }
    }

    /* Draw the frame of the minimap */
    drawtool_drawrect_colno(prect, SDLGL_COL_WHITE, 0);

    /* == Now draw the rectangle of the visible screen (Viewport) == */
    frect[0] = (float)prect->x;
    frect[0] += tile_size * (float)map_info.vp_x;
    frect[1] = (float)prect->y;
    frect[1] += tile_size * (float)map_info.vp_y;
    frect[2] = frect[0];
    frect[3] = frect[1] + (tile_size * (float)map_info.vp_h);
    frect[4] = frect[0] + (tile_size * (float)map_info.vp_w);
    frect[5] = frect[1] + (tile_size * (float)map_info.vp_h);
    frect[6] = frect[0] + (tile_size * (float)map_info.vp_w);
    frect[7] = frect[1];

    sdlgl_set_color(SDLGL_COL_YELLOW);

    glBegin(GL_LINE_LOOP);
        for(i = 0; i < 8; i += 2)
        {
            glVertex2fv(&frect[i]);
        }
    glEnd();
}


/*
 * Name:
 *     drawmap_main
 * Description:
 *     Draws the map into given field.
 * Input:
 *     prect *: Rectangle to draw the map into
 *     grid_on: Draw the map grid yes/no
 * Last change:
 *     2017-07-16 / bitnapper
 */
void drawmap_main(SDLGL_RECT_T *prect, int grid_on)
{
    FSCMAP_INFO_T map_info;
    /*
    FSCMAP_TILEINFO_T tile_info;

    SDLGL_INPUT_T input, input_unit;
    SDLGL_RECT_T rect;
    */
    int visitile_size;
    /*
    SDLGL_LABEL_T labels[80], *plb;
    SDLGL_INPUT_T   mapdisplay[300], *did;
    */

    /* New calculate size of square in pixel from rectangle and tiles visible */
    /* Height should be the same as width (pixel square)                      */
    /* First draw the background                */
    drawmap_star_background(prect);

    fscmap_get_info(&map_info);

    /* plb  = &labels[0]; */

    /* ------- Get the size of a visible tile ---------- */
    visitile_size = prect->w / map_info.vp_w;

    /* Second draw the map grid and fog of war  */
    /* @TODO: Take 'gridon' from  game settings  */
    drawmap_base(prect, &map_info, visitile_size, grid_on);

    mapdraw_range_borders(prect, &map_info, 1, 0, visitile_size);

#if 0
    sdlgl_set_color(SDLGL_COL_WHITE);
    /* fscfont_set_size(DRAWMAP_STARNAMESIZE); */

    input.sdlgl_type = SDLGL_TYPE_TEXTURE;
    input.rect.w = visitile_size;
    input.rect.h = visitile_size;


    for(y = 0; y < vp_winsize[3]; < y++)
    {
        input.rect.x = prect->x + (x * input.rect.w);
        input.rect.y = prect->y + (y * input.rect.h);

        for(x = 0; x < vp_winsize[2]; < x++)
        {
            if(fscmap_get_drawinfo(1, x + vp_winsize[0], y + vp_winsize[1], &tile_info, FSCMAP_INFO_STAR))
            {
                input.val_min    = FSC_TEX_STAR;            /* Number for texture and subtexture            */
                input.val_max    = tile_info.star_type;     /* Minimum an maximum of value for *pdata       */
                /* @TODO: Call function to draw this texture */

                if(tile_info.star_name[0] != 0)
                {
                    /* If we know the stars Name, then set the label and it's color */
                    plb->val_type  = SDLGL_VAL_STRING;
                    plb->pos_x     = (input.rect.x + (visitile_size  / 2));
                    plb->pos_y     = (input.rect.y + visitile_size) + 5;
                    plb->pdata     = tile_info.star_name;
                    plb->color_no  = drawtool_get_colorno(1, tile_info.owner)
                    plb->font_size = DRAWMAP_STARNAMESIZE;
                    plb->align     = 'C';
                    plb++;

                }
            }

            if(fscmap_get_drawinfo(1, x + vp_winsize[0], y + vp_winsize[1], &tile_info, FSCMAP_INFO_UNIT))
            {
                memcpy(&input_unit, &input, sizeof(SDLGL_INPUT_T));

                input.val_min    = FSC_TEX_UNIT;            /* Number for texture and subtexture            */
                input.val_max    = tile_info.unit_type;     /* Minimum an maximum of value for *pdata       */
            }
        }
    }

    /* -- Sign end of array */
    plb->val_type = 0;


    while(ti->pos >= 0)
    {
        /* There is an info about this tile */
        /* TODO: Calc XY-Pos from 'pos' relative to viewport */
        rect.x = maprect->x + (ti->viewx * visitilesize);
        rect.y = maprect->y + (ti->viewy * visitilesize);

        /* Draw unit if there is a unit, but not animated */
        if(ti->unit_stack[0] > 0) {

            did->rect.x = rect.x;
            did->rect.y = rect.y;
            did->rect.w = visitilesize;
            did->rect.h = visitilesize;

            /* It's our unit and we have moves left and it's the active unit */
            if(ti->pos == mng->pos)
            {
                did = mapdrawAddUnitToDisplayList(did, mng->list[mng->cursor], mng->type, player_no);
                did = mapdrawAddCursorToDisplayList(did, mng->moves_left);

                if(! Animation.on)
                {
                    mapdraw_range_borders(maprect, map_info, player_no, mng->range_no, visitilesize);
                }

            }
            else
            {
                /* For alien and other units draw first unit on unit-stack */
                did = mapdrawAddUnitToDisplayList(did, ti->unit_stack[0], ti->unit_type, ti->unit_owner);
            }

        }  /* if(ti->unit_stack[0] > 0)  */

        ti++;   /* Next tile to draw */

    }  /* while(ti->pos >= 0) */

    did->sdlgl_type  = 0;     /* Sign end of list for textures    */

    lb->valtype = 0;          /* Sign end of list for labels      */

    /* ------- Display the icons -------- */
    sdlgltexDisplayIconList(&mapdisplay[0], 0);
    /* ------- Display the labels ------- */
    fscfont_print_labels(0, 0, &labels[0]);
#endif
}

/*
 * Name:
 *     drawmap_animation_on
 * Description:
 *     Returns if animation is on yes/no
 * Input:
 *     None
 * Output:
 *     Animation is on yes/no
 */
int drawmap_animation_on(void)
{
    return Animation.on;
}

#if 0
/*
 * Name:
 *     drawmap_start_animation
 * Description:
 *     Starts the animation for the given unit.
 * Input:
 *     prect *:  Rectangle to draw the unit into
 *     unitino:  Number of unit to animate
 *     src_pos:  Where the unit came from
 *     dest_pos: Dest pos for unit movement
 */
void drawmap_start_animation(SDLGL_RECT_T *prect, int unitno, int src_pos, int destpos)
{
    SDLGLMAP_XY diffxy;
    int visitilesize;
    int viewportw;


    viewportw = fscmap_get_viewportxy(src_pos, destpos, &Animation.src_pos, &diffxy);

    visitilesize = rect->w / viewportw;

    /* ------- Init the basic data -------- */
    Animation.on = 1;            /* Set animation on              */
    Animation.unitno     = unitno;
    Animation.pixeldistx = diffxy.x * visitilesize;
    Animation.pixeldisty = diffxy.y * visitilesize;

    /* ----------- Convert tile position to display position ----------- */
    Animation.src_pos.x = (Animation.src_pos.x * visitilesize) + rect->x;
    Animation.src_pos.y = (Animation.src_pos.y * visitilesize) + rect->y;

    /* The animation data itself */
    Animation.clock      = 0;                  /* The clock for the animation */
    Animation.clocktime  = DRAWMAP_ANIM_TIME;   /* A half second               */

    /* -------- Adjust the cursor values -------------- */
    Mapdraw_Cursor.clock = 0;                   /* No cursor clock             */
}

#endif
/*
 * Name:
 *     drawmap_timer_func
 * Description:
 *     Does all the timed stuff for the drawed part for the map display
 * Input:
 *     tickspassed: Ticks passed since last call
 */
void drawmap_timer_func(float seconds_passed)
{
    if(Animation.on)
    {
        /* No cursor visible, as long as animation is running */
        drawmap_run_animation(seconds_passed);
    }
    else
    {
        /* Check clock for cursor blinking */
        Mapdraw_Cursor.clock += seconds_passed;

        if(Mapdraw_Cursor.clock >= DRAWMAP_CURSORTIME) {

            Mapdraw_Cursor.clock -= DRAWMAP_CURSORTIME;
            Mapdraw_Cursor.on ^= 1;

        }
    }
}

