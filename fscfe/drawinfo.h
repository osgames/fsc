/*******************************************************************************
*  DRAWINFO.H                                                                   *
*      - Functions and data for handling of infobar menu				       *
*                                                                              *
*  FREE SPACE COLONISATION                                                     *
*      (c)2004 Paul Mueller <pmtech@swissonline.ch>                            *
*                                                                              *
*  This program is free software; you can redistribute it and/or modify        *
*  it under the terms of the GNU General Public License as published by        *
*  the Free Software Foundation; either version 2 of the License, or           *
*  (at your option) any later version.                                         *
*                                                                              *
*  This program is distributed in the hope that it will be useful,             *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of              *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
*  GNU Library General Public License for more details.                        *
*                                                                              *
*  You should have received a copy of the GNU General Public License           *
*  along with this program; if not, write to the Free Software                 *
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  *
*******************************************************************************/

#ifndef _FSC_DRAWINFO_H_
#define _FSC_DRAWINFO_H_

/*******************************************************************************
* INCLUDES                              				                       *
*******************************************************************************/

#include "msg.h"

/*******************************************************************************
* DEFINES                                  				                       *
*******************************************************************************/

/* --------- The number of the infobar - fields ----------------------- */
#define DRAWINFO_ORDERLIST    ((char)101)    /* Signs order-list     */
#define DRAWINFO_PLANETSYS    ((char)102)    /* Signs planet-system  */
#define DRAWINFO_CHOOSESTAR   ((char)103)
#define DRAWINFO_CHOOSEPLANET ((char)104)

/*******************************************************************************
* CODE                                  				                       *
*******************************************************************************/

void drawinfo_starsys(int star_no, int planet_no);
/*
void infobarTileInfo(int pos, char nation_no, int chosen_planet_no);
char *infobarUnitOrderText(char order, int target_no, char build_no, char *buffer);
void infobarDebugInfo(char *info);
int  infobarUnitOrderMenu(int x, int y, char *cmdlist, int order_code);  / * Order menu for human player * /
void infobarHandleMsg(int which, MSG_INFO *msg);
void infobarSetPlanetInfo(int planet_no);
void infobarSetPlanetMenu(int x, int y, int planet_no, int unit_no, char leader_no);
void infobarDrawUnitInfo(int x, int y, int unit_no, int draw_rect);
void infobarDrawDebugInfo(void);
void infobarDrawLabels(void);
void infobarDrawGlobalInfo(int x, int y);
void infobarUpdateGlobalInfo(int mappos, char nation_no);
*/

#endif  /* _FSC_DRAWINFO_H_ */
