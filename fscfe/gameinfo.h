/*******************************************************************************
*  GAMEINFO.H                                                                   *
*      - Functions and data for handling of infobar menu				       *
*                                                                              *
*  FREE SPACE COLONISATION                                                     *
*      (c)2004 Paul Mueller <pmtech@swissonline.ch>                            *
*                                                                              *
*  This program is free software; you can redistribute it and/or modify        *
*  it under the terms of the GNU General Public License as published by        *
*  the Free Software Foundation; either version 2 of the License, or           *
*  (at your option) any later version.                                         *
*                                                                              *
*  This program is distributed in the hope that it will be useful,             *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of              *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
*  GNU Library General Public License for more details.                        *
*                                                                              *
*  You should have received a copy of the GNU General Public License           *
*  along with this program; if not, write to the Free Software                 *
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  *
*******************************************************************************/

#ifndef _FSC_GAMEINFO_H_
#define _FSC_GAMEINFO_H_

/*******************************************************************************
* INCLUDES                              				                       *
*******************************************************************************/

/* #include "../code/msg.h" */

#include "sdlgldef.h"   /* -- Definitions of input, which is generated -- */

/*******************************************************************************
* DEFINES                                  				                       *
*******************************************************************************/

/* --------- The number of the infobar - fields ----------------------- */
#define GAMEINFO_ORDERLIST    ((char)101)    /* Signs order-list     */
#define GAMEINFO_PLANETSYS    ((char)102)    /* Signs planet-system  */

/* === Different definitions for test purposes === */
#define FSC_TEX_STAR   1
#define FSC_TEX_PLANET 2
#define FSC_TEX_UNIT   3

/*******************************************************************************
* CODE                                  				                       *
*******************************************************************************/

/* ==== Info about star system */

/* void infobarTileInfo(int pos, char player_no, int chosen_planet_no); */
void gameinfo_add_starsysinfo(char player_no, int planet_no);
/*
void gameinfo_add_starsysinfo(int x, int y, char player_no, int planet_no, int unit_no);
*/
void gameinfo_draw_starsys(int x, int y);
/* void infobarSetPlanetInfo(int planet_no); */
void gameinfo_set_planetinfo(int planet_no);
/* And the code for drawing it... ? */

void gameinfo_unit(int x, int y, int unit_no, char draw_rect);
/* int  infobarUnitOrderMenu(int x, int y, char *cmdlist, int order_code);  */
SDLGL_INPUT_T *gameinfo_create_ordermenu(int bott_x, int bott_y, char *porder_list);

void gameinfo_draw_globallabels(void);
/*
void infobarDebugInfo(char *info);
*/
/* Order menu for human player */
/*
void infobarHandleMsg(int which, MSG_INFO_T *msg);
*/

/* void infobarSetPlanetMenu(int x, int y, int planet_no, int unit_no, char leader_no); */
/*
void infobarDrawDebugInfo(void);
void infobarDrawLabels(void);
void infobarDrawGlobalInfo(int x, int y);
void infobarUpdateGlobalInfo(int mappos, char player_no);
*/

#endif  /* _FSC_GAMEINFO_H_ */
