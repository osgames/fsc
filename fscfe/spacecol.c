/* *****************************************************************************
*  SPACECOL.C                                                                  *
*      - The main initializing functions, where the game starts.               *
*                                                                              *
*  FREE SPACE COLONISATION                                                     *
*      (c)2002 Paul Mueller <pmtech@swissonline.ch>                            *
*                                                                              *
*  This program is free software; you can redistribute it and/or modify        *
*  it under the terms of the GNU General Public License as published by        *
*  the Free Software Foundation; either version 2 of the License, or           *
*  (at your option) any later version.                                         *
*                                                                              *
*  This program is distributed in the hope that it will be useful,             *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of              *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
*  GNU Library General Public License for more details.                        *
*                                                                              *
*  You should have received a copy of the GNU General Public License           *
*  along with this program; if not, write to the Free Software                 *
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  *
*******************************************************************************/


/* *****************************************************************************
* INCLUDES								                                       *
*******************************************************************************/

#include <stdio.h>		/* sprintf()    */
#include <memory.h>     /* memset()     */
#include <string.h>     /* strncmp()    */
/* #include <SDL_main.h> */  /* SDL_main --> Has to be tested with Dev-Cpp */


/* ----- Own headers --- */
#include "fsccmd.h"                 /* All commands available           */
#include "fscshare.h"
#include "fscfe/fscfont.h"          /* Textured font                    */
#include "fscfe/mainscr.h"          /* mainscrStartmenu()               */
#include "fscfe/fscinit.h"
#include "fsctool.h"
#include "gamemenu.h"              
#include "gamefile.h"
#include "language.h"               /* Support of multiple languages    */
#include "msg.h"                    /* Logging  */
#include "sdlglcfg.h"
#include "fscfe/sdlgl.h"            /* OpenGL and SDL-Stuff             */


#include "spacecol.h"           /* The declaration for the key commands     */

/* *****************************************************************************
* DEFINES								                                       *
*******************************************************************************/

#define SPACECOL_GAMENAME "Free Space Colonization"


#define SPACECOL_SAVENAMEBUF  (12 * 32)
#define SPACECOL_VERSIONTEXT  3
#define SPACECOL_MENUMARKER   ((char)100)   /* Where Savegame-Menu starts and ends */

/* *****************************************************************************
* ENUMS                     			                                       *
*******************************************************************************/

typedef enum {

    SCC_NONE             = 0,
    SCC_EDIT             = 1,
    SCC_CHOOSEPARTY      = 2,
    SCC_CHOOSEABILITY    = 3,
    SCC_SETUP_BACK       = 4,
    SCC_SETUP_DONE       = 5,
    SCC_CHOOSE_PARTY     = 6,
    SCC_HABITABLEPLANETS = 7,
    SCC_DIFFICULTYLEVEL  = 8,
    SCC_ALIGNEMENT       = 9,
    SCC_INTELLIGENCE    = 10,
    SCC_GALAXYSIZE      = 11,
    SCC_CHOOSEADVERSARY = 12,
    SCC_ARROWLEFT       = 13,
    SCC_ARROWRIGHT      = 14,

} SPACECOl_CMD;

/* *****************************************************************************
* DEFINES                  			                                           *
*******************************************************************************/

#define SPACECOL_NEWGAME          ((char)1)
#define SPACECOL_LOADGAME         ((char)2)
#define SPACECOL_LOADCURRENTGAME  ((char)3)
#define SPACECOL_SETGAMEOPTIONS   ((char)4)
#define SPACECOL_EXITPROGRAM      ((char)5)
#define SPACECOL_SAVEGAMELIST     ((char)10)

#define STARTSCR_BASE_SIZE        2 /* Start with 2 sectors (instead of 3)   */
#define STARTSCR_BASE_TILESPERSECTOR 12
#define STARTSCR_BASE_HABITABLE  50
#define STARTSCR_BASE_DIFFICULTY 50
#define STARTSCR_BASE_NUMPLAYER   4

/* *****************************************************************************
* FORWARD DECLARATIONS      			                                       *
*******************************************************************************/

static int  spacecolStartScreenHandler(SDLGL_EVENT *event);

/* *****************************************************************************
* DATA									                                       *
*******************************************************************************/

/* ------ Default-Info for the test game -------------- */
static GAME_STARTINFO DefaultGame = {       /* Preset info for test games... */

    /* ------------ Data about the size of the galaxy ----------------- */
    0,              /* Random seed to use for generation of this map    */
                    /* if 0, a random seed is generated as the map is   */
                    /* created                                          */
    /* ------------ Data about the size of the galaxy ----------------- */
    STARTSCR_BASE_SIZE,
    STARTSCR_BASE_SIZE,             /* In sectors                           */
    STARTSCR_BASE_TILESPERSECTOR,   /* For handling of sectors              */
    /* ------------ Data used while for map generation ---------------- */
	STARTSCR_BASE_NUMPLAYER,    /* Number of leaders            		    */
    STARTSCR_BASE_HABITABLE,    /* The chance in percent from 'habitable'   */
    STARTSCR_BASE_DIFFICULTY    /* How difficult it is for the human leader */

};

static GAME_STARTINFO NewGame;

static SDLGL_CONFIG SdlGlConfig = {

    0,
    800, 600,           /* scrwidth, scrheight: Size of screen  */
    32,                 /* colordepth: Colordepth of screen     */
    0,                  /* wireframe                            */
    0,                  /* hidemouse                            */
    SDLGL_SCRMODE_WINDOWED, /* screenmode                       */
    0,                  /* debugmode                            */
    0,                  /* zbuffer                              */
    50,                 /* dblclicktime                         */
    1024, 768           /* displaywidth, displayheight          */

};

static SDLGLCFG_NAMEDVALUE CfgValues[] = {

    { SDLGLCFG_VAL_INT, &SdlGlConfig.scrwidth, "scrwidth" },
    { SDLGLCFG_VAL_INT, &SdlGlConfig.scrheight, "scrheight" },
    { SDLGLCFG_VAL_INT, &SdlGlConfig.colordepth, "colordepth" },
    { SDLGLCFG_VAL_INT, &SdlGlConfig.screenmode, "screenmode" },
    { SDLGLCFG_VAL_INT, &SdlGlConfig.debugmode, "debugmode" },
    { 0 }
    
};

static char SaveGameName[SPACECOL_SAVENAMEBUF];

/* Handling and data for the setup screens */
static SDLGL_FIELD SpaceColMenu[] = {  /* Menu for startscreen */

    /* -------- The menu itself ------------------- */
    { SDLGL_TYPE_MENU, { 440, 200, 256, 16 }, SPACECOL_NEWGAME, 0, "Start a new game" },
    { SDLGL_TYPE_MENU, { 440, 230, 272, 16 }, SPACECOL_LOADGAME, 0, "Load a saved game" },
    { SDLGL_TYPE_MENU, { 440, 260, 336, 16 }, SPACECOL_LOADCURRENTGAME, 0, "Load game in progress" },
    { SDLGL_TYPE_MENU, { 440, 290, 336, 16 }, SPACECOL_SETGAMEOPTIONS, 0, "Set game options" },
    { SDLGL_TYPE_MENU, { 440, 320, 160, 16 }, SPACECOL_EXITPROGRAM, 0, "Exit to OS" },
    { 0 }

};

static SDLGL_CMDKEY SpaceColCmdKey[] = {
    { { SDLK_ESCAPE }, SPACECOL_EXITPROGRAM },
    { { 0 } }
};


static SDLGL_FIELD SaveGameMenu[20 + 2] = {
    /* Default position and size, add-height should be 20 */
    { 0, { 0, 0, 160, 16 }, SPACECOL_LOADGAME },
    { 0 }
};

/* Print titles as labels */
static FSCFONT_LABEL StartScreenLabel[] = {

    {  FSCFONT_VALSTR, 20, 20, SPACECOL_GAMENAME, 0, { 204, 204, 229 }, 26 },
    {  FSCFONT_VALSTR, 20, 650, "Spacecol Caption", 0 , { 0, 0, 0 }, 10 },
    { 0 }

};

/* *****************************************************************************
* CODE									                                       *
*******************************************************************************/

/*
 * Name:
 *     spacecolAddSavedGameMenu
 * Description:
 *     Adds the list of saved games as menu, if any game is available
 * Input:
 *     None
 */
static void spacecolAddSavedGameMenu(void)
{

    char numgame, i;
    SDLGL_FIELD *psavemenu;
    char *psavename;


    SaveGameMenu[0].sdlgl_type = 0;       /* Is an empty menu */

    numgame = (char)gamefileGetNameList(&SaveGameName[0], SPACECOL_SAVENAMEBUF);
    /* TODO: Don't allow more the 20 savegames to be displayed */
    if (numgame > 0) {

        /* Set pointer */
        psavemenu = &SaveGameMenu[0];
        psavename = &SaveGameName[0];

        /* Propagate actual menu size to next one */
        memcpy(&psavemenu[1], psavemenu, sizeof(SDLGL_FIELD));
        psavemenu -> sdlgl_type = SDLGL_TYPE_MENU;
        psavemenu -> code       = 1;                           /* Number of game */
        psavemenu -> pdata      = psavename;
        psavemenu++;

        for (i = 2; i <= numgame; i++) {

            memcpy(psavemenu, &psavemenu[-1], sizeof(SDLGL_FIELD)); /* Previous menu point */
            psavemenu -> code    = i;
            psavemenu -> rect.y  += 16;

            psavename = strchr(psavename, 0) + 1;
            psavemenu++;

        }

        /* TODO: Additional point: "Cancel" as last one */
        psavemenu -> sdlgl_type = 0;      /* Sign end of buffer */
        /* TODO: New screen for savegame-menus */
        sdlglInputAdd(SPACECOL_MENUMARKER, &SaveGameMenu[0], -1, 60);

    }
    else {

        /* TODO: Display message that no savegame is available ? */

    }

}

/*
 * Name:
 *     spacecolDrawStartScreen
 * Description:
 *     Drawing of start screen
 * Input:
 *      fields *:
 *      event *:
 */
static void spacecolDrawStartScreen(SDLGL_FIELD *fields, SDLGL_EVENT *event)
{

    /* Print the menu */
    fscfontMenuStrings(fields, SDLGL_TYPE_MENU, "\xB2\x99\x00", "\xE5\x19\x00");
    fscfontPrintLabels(0, 0, StartScreenLabel);

}

/*
 * Name:
 *     spacecolStartScreenHandler
 * Description:
 *     Handler for the start screen
 * Input:
 *     event *: Pointer on event
 */
static int spacecolStartScreenHandler(SDLGL_EVENT *event)
{

    if (event -> code > 0) {

        switch(event -> code) {

            case SPACECOL_NEWGAME:
                /* Call the screen for game configuration */
                gamemenuNewGame(&NewGame);
                break;

            case SPACECOL_SAVEGAMELIST:
                /* TODO: Display menu with list of saved games, if any available*/
                if (event -> sub_code < 0) {

                    spacecolAddSavedGameMenu();

                }
                else {

                    /* For Test purposes: Load allways current game */
                    DefaultGame.seed = 0;
                    DefaultGame.game_no = event -> sub_code;
                    /* TODO: Call loading of game here... */
                    mainscrStartmenu(GAME_SAVEDGAME, &DefaultGame);

                }
                break;

            case SPACECOL_LOADGAME:
                DefaultGame.seed = 0;
                mainscrStartmenu(GAME_SAVEDGAME, &DefaultGame);
                break;

            case SPACECOL_LOADCURRENTGAME:
                /* For test purposes always generate same game */
                DefaultGame.seed = 314528;
                /* TODO: Call loading of game here... */
                mainscrStartmenu(GAME_CURRENTGAME, &DefaultGame);
                break;

            case SPACECOL_EXITPROGRAM:
                return SDLGL_INPUT_EXIT;

        }

    } /* if (event -> type == SDLGL_INPUT_COMMAND) */

    if (NewGame.game_no == GAMEMENU_DONE_VALUE) {

        NewGame.game_no = GAMEMENU_NONE_VALUE;     /* Reset if return from 'Main-Screen' */
        mainscrStartmenu(GAME_NEWGAME, &NewGame);

    }

    return SDLGL_INPUT_OK;

}

/*
 * Name:
 *     spacecolStart
 * Description:
 *      Sets up the first input screen
 * Input:
 *     None
 */
static void spacecolStart(void)
{

    SDLGL_FIELD *fp;


    StartScreenLabel[1].plabel = gameGetVersion();

    fsctoolSetRandSeed(0);

    /* Get strings from language: 2010-02-18 */
    if (languageFindMenu("MAIN")) {

        fp = &SpaceColMenu[0];

        while(fp -> sdlgl_type > 0) {

            fp -> pdata = languageMenuString();
            fp++;

        }
        
    }

    sdlglInputNew(spacecolDrawStartScreen,
                  spacecolStartScreenHandler,  /* Handler for input     */
                  SpaceColMenu,             /* Menu for start screen */
                  SpaceColCmdKey, 0);

}

/* ========================================================================== */
/* ============================= THE MAIN ROUTINE(S) ======================== */
/* ========================================================================== */

/*
 * Name:
 *     main
 * Description:
 *     Main function
 * Input:
 *     argc:
 *     argv **:
 */
int main(int argc, char **argv)
{

    /* Read configuration from file. 03-19-2006 / PAM */
    sdlglcfgReadSimple("data/fsc.cfg", CfgValues);

    /* Now overwrite the win caption, because in this stage it displays */
    /* the version number and the compile data.				            */
    SdlGlConfig.wincaption = gameGetVersion();

    sdlglInit(&SdlGlConfig);

    glPolygonMode(GL_FRONT, GL_FILL);
    glShadeModel(GL_FLAT);		/* No smoothing	of edges */

    fscinitLoadIcons();         /* Load the star/map icon pictures  */
    fscinitReadRules();         /* Read in the rules from file      */
    languageInit();             /* Load all language based files    */
    fscfontLoad();              /* Load the font textures           */
    msgOpenLog();               /* Open the log file                */
   
    /* Set the start screen */
    spacecolStart();

    /* Now enter the mainloop */
    sdlglExecute();

    /* Do any shutdown stuff. */
    msgCloseLog();
    fscfontRelease();
    languageExit();
    fscinitDeleteIcons();

    sdlglShutdown();
    
    return 0;

}
