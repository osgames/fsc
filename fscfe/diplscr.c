/*******************************************************************************
*  DIPLSCR.C                                                                   *
*	    - Diplomacy screen, for treaties ad such           	                   *
*                                                                              *
*	FREE SPACE COLONISATION               	                                   *
*   Copyright (C) 2002  Paul Mueller <pmtech@swissonline.ch>                   *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*******************************************************************************/

/*******************************************************************************
* INCLUDES     							                                       *
*******************************************************************************/
#include "sdlgl.h"


#include "fscfe/fscfont.h"


#include "fscfe/diplscr.h"

/*******************************************************************************
* DEFINES      							                                       *
*******************************************************************************/



/*******************************************************************************
* DATA       							                                       *
*******************************************************************************/


static SDLGL_FIELD InputFields[] = {    /* Color: 13, 66, 116 */

    /* "c:\fsc\DiplomacySpeakToScreen" */
    /* --------- Background "We have" ---------  */
    /* type, code, subcode, rect[]   */ /* FIXME: Expand to slider boxes */
    { 0, {  35, 21, 218, 545 } },
    { 0, {  35, 0, 0, 0 } },     /* Money        */
    { 0, {  35, 0, 0, 0 } },     /* Treaties: Possible treaties to offer */
    { 0, {  35, 0, 0, 0 } },     /* Star systems (colonies)  */
    { 0, {  35, 0, 0, 0 } },     /* Technology               */
    { 0, {  35, 0, 0, 0 } },     /* Star ships (units)       */
    /* ---- Our treaty offers (Heigth 12 pixels per element, fontsize = 10 ----- */
    { 0, {  32, 605, 218, 96 } },     /* Treaty offer */
    /* Our message */
    { 0, { 317, 570, 388, 142 } },    /* Textsize 12 */
    /* Background "They have"   */
    { 0, { 771, 21, 221, 545 } },
    { 0, { 771, 0, 0, 0 } },     /* Money */
    { 0, { 771, 0, 0, 0 } },     /* Treaties: Possible treaties to offer */
    { 0, { 771, 0, 0, 0 } },     /* Star systems (colonies)  */
    { 0, { 771, 0, 0, 0 } },     /* Technology               */
    { 0, { 771, 0, 0, 0 } },     /* Star ships (units)       */
    /* ---- Their treaty offers ----- */
    { 0, { 771, 605, 218, 96 } },
    /* Their message */
    { 0, { 319, 392, 388, 142  } },  /* Textsize 12 */
    /* ------- Additional info buttons -------- */
    { 0, { 439, 339, 43, 37 } },      /* Fist     */
    { 0, { 488, 339, 43, 37 } },      /* Flag     */
    { 0, { 538, 339, 43, 37 } },      /* Monitor  */
    /* --------- Buttons: -------- */
    { 0, {  96, 566, 94, 25 } },  /* We add/remove from treaty offer   */
    { 0, { 832, 566, 94, 25 } },  /* They have add/remove treaty offer */
    { 0, { 335, 720, 94, 25 } },  /* Send     */
    { 0, { 594, 720, 94, 95 } },  /* Leave    */
    /* { 0, 0, 0, { , , ,  } }, */
    /* { 0 } */

};

static FSCFONT_LABEL DiplScrLabels[] = {

    /* Adversary name Rect: { 371, 12, 282 16 } --> textsize = 10 */
    /* adversary picture: { 296, 45, 430, 280 } */
    /* ---------- Button labels -------- */
    { FSCFONT_VALSTR, 394, 10, "Diplomacy", FSCFONT_TEXT_CENTER, { 85, 123, 142 }, 20 },
    { 0 }

};

/*******************************************************************************
* CODE       							                                       *
*******************************************************************************/

/* ========================================================================== */
/* ======================= PUBLIC FUNCTIONS ================================= */
/* ========================================================================== */

/*
 * Name:
 *     diplscrMain
 * Description:
 *     Opens the diplomatic screen
 * Input:
 *     None
 */
void diplscrMain(void)
{

}
