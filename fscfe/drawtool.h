/*******************************************************************************
*  SDLGDRW.H                                                                   *
*      - Output of text in an OpenGL-Window.                                   *
*                                                                              *
*   FREE SPACE COLONISATION                                                    *
*      Copyright (C) 2002-2017  Paul M�ller <muellerp61@bluewin.ch>            *
*                                                                              *
*  This program is free software; you can redistribute it and/or modify        *
*  it under the terms of the GNU General Public License as published by        *
*  the Free Software Foundation; either version 2 of the License, or           *
*  (at your option) any later version.                                         *
*                                                                              *
*  This program is distributed in the hope that it will be useful,             *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of              *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
*  GNU Library General Public License for more details.                        *
*                                                                              *
*  You should have received a copy of the GNU General Public License           *
*  along with this program; if not, write to the Free Software                 *
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  *
*******************************************************************************/

#ifndef _SDLGDRW_H_
#define _SDLGDRW_H_

/*******************************************************************************
* INCLUDES         							                                   *
*******************************************************************************/

#include "sdlgldef.h"

/******************************************************************************
* DEFINES								      								  *
******************************************************************************/

#define SDLGDRW_FONT6     0     /* Font 6 Points width and 8 Points height */
#define SDLGDRW_FONT8	  1     /* Font 8 Points width and 8 Points height */
#define SDLGDRW_FONTSPACE 2     /* Font 8 Points width and 8 Points height */


/******************************************************************************
* TYPEDEFS    								      *
******************************************************************************/

typedef struct
{
    int font_no;                    /* Basic style font                 */
    int drawfont_no;		        /* Font to use for drawing (if changed) */
    unsigned char text_col[6][6];   /* For the different button colors  */
    unsigned char hp_color[9];      /* Colors for HP-Bar                */
    unsigned char scrollbk[3];		/* Color for scrollbox background   */
    unsigned char bar_color[3];     /* Color for Progress-Bar           */
    unsigned char button_col[10][9];/* For the different button colors  */
}
DRAWTOOL_STYLE_T;

/* ------ Drawing function -------- */
typedef void (* DRAWTOOL_USERFUNC_T)(SDLGL_INPUT_T *pinput, SDLGL_RECT_T *prect, int el_no);

/******************************************************************************
* ROUTINES								      								  *
******************************************************************************/

/* Drawing function */
void drawtool_drawfields(SDLGL_INPUT_T *pinput, DRAWTOOL_USERFUNC_T drawfunc);
void drawtool_labels(SDLGL_LABEL_T *plabel);
void drawtool_labels_pos(int draw_x, int draw_y, int draw_w, int draw_h, SDLGL_LABEL_T *plabel, char draw_bk);
void drawtool_print_string(SDLGL_RECT_T *ppos, char *ptext);
void drawtool_print_stringpos(SDLGL_RECT_T *prect, char *text, int flags);

void sdlglstrStringF(SDLGL_RECT_T *pos, char *ptext, ...);
void sdlglstrStringBackground(SDLGL_RECT_T *ppos, int len, int color);

/* Additional text drawing functions for dialog boxes, using TEXTOUTSTYLE */
void drawtool_text_torect(SDLGL_RECT_T *prect, char *pstring);

/* Additional function for user defined fonts and styles */
int  drawtool_font_set(int fontno);
void drawtool_font_add(unsigned char *pfont, int fontw, int fonth, int fontno);
void drawtool_set_drawstyle(DRAWTOOL_STYLE_T *pstyle);

/* Additional drawing functions */
void drawtool_set_colorno(int pal_no, int color_no);
char drawtool_get_colorno(int pal_no, int color_no);
void drawtool_circle(SDLGL_RECT_T *prect, int radius, char col_no, char filled);
void drawtool_drawrect(SDLGL_RECT_T *prect, unsigned char *pcolor, int filled);
void drawtool_drawrect_colno(SDLGL_RECT_T *prect, int color_no, int filled);
void drawtool_drawrect_sides(SDLGL_RECT_T *prect, int range_no, char side_bits);
/* Something for the show */
void drawtool_generate_starbk(short int width, short int height);
void drawtool_draw_starbk(int num_star);

#endif /* _SDLGDRW_H_ */

