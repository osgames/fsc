/*******************************************************************************
*   GAMEMENU.H                                                                 *
*	    - Screen for setting up of a new game             	                   *
*                                                                              *
*	FREE SPACE COLONISATION               	                                   *
*      Copyright (C) 2002-2017  Paul Müller <muellerp61@bluewin.ch>            *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*******************************************************************************/

#ifndef _FSC_GAMEMENU_H_
#define _FSC_GAMEMENU_H_

/*******************************************************************************
* CODE								                                           *
*******************************************************************************/

/* Get input info and start new game if set so */
void gamemenu_new_game(void);
void gamemenu_choose_game(int load);

#endif /* _FSC_GAMEMENU_H_ */
