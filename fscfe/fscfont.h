/*******************************************************************************
*  FSCFONT.H                                                                   *
*      - Textured fonts for FSC                      					       *
*                                                                              *
*	FREE SPACE COLONISATION               	                                   *
*      (c)2002 - 2017 Paul Mueller <muellerp61@bluewin.ch>                     *
*                                                                              *
*  This program is free software; you can redistribute it and/or modify        *
*  it under the terms of the GNU General Public License as published by        *
*  the Free Software Foundation; either version 2 of the License, or           *
*  (at your option) any later version.                                         *
*                                                                              *
*  This program is distributed in the hope that it will be useful,             *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of              *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
*  GNU Library General Public License for more details.                        *
*                                                                              *
*  You should have received a copy of the GNU General Public License           *
*  along with this program; if not, write to the Free Software                 *
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  *
*******************************************************************************/

#ifndef _FSCFONT_H_
#define _FSCFONT_H_

/*******************************************************************************
* INCLUDES                                                                     *
*******************************************************************************/

#include "sdlgldef.h"

/*******************************************************************************
* CODE                                                                         *
*******************************************************************************/

void fscfont_set_color_no(int colorno);
void fscfont_set_colorv(unsigned char *color);
void fscfont_draw_char(int letter, int x, int y);
int  fscfont_string(SDLGL_RECT_T *ppos, char *szText);
int  fscfont_stringf(SDLGL_RECT_T *ppos, char *text, ...);
void fscfont_load(void);
void fscfont_release(void);
void fscfont_set_font(int fontno);
void fscfont_set_size(int height);
void fscfont_string_size(char *text, SDLGL_RECT_T *rect);
void fscfont_text_to_rect(SDLGL_RECT_T *rect, char *ptext, char flags);
void fscfont_print_labels(int x, int y, SDLGL_LABEL_T *plabels);
/* void fscfontPrintLabelValues(int x, int y, SDLGL_LABEL_T *list); */
void fscfont_print_inputtext(SDLGL_INPUT_T *pfields, char inp_type);

#endif /* _FSCFONT_H_ */
