/*******************************************************************************
*  FSCINIT.H                                                                   *
*	    - Inital functions and data                                  	       *
*                                                                              *
*	FREE SPACE COLONISATION               	                                   *
*   Copyright (C) 2002  Paul Mueller <pmtech@swissonline.ch>                   *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*******************************************************************************/

#ifndef _FSCINIT_H_
#define _FSCINIT_H_

/*******************************************************************************
* DEFINES  								                                       *
*******************************************************************************/

/* ------- The different icons -------- */
#define FSCINIT_STARICONMAP      1
#define FSCINIT_UNIT             2
#define FSCINIT_KNOB             3
#define FSCINIT_IMPERIUMSIGN     4
#define FSCINIT_PLANETS          5
#define FSCINIT_CURSOR           6
#define FSCINIT_BACKTEXTURE      7
#define FSCINIT_SMALLMAPICON     8

/*******************************************************************************
* CODE  								                                       *
*******************************************************************************/

void fscinitLoadIcons(void);
void fscinitDeleteIcons(void);
void fscinitReadRules(void);

#endif  /* _FSCINIT_H_ */
