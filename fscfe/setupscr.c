/*******************************************************************************
*  SETUPSCR.C                                                                  *
*      - Game setup screen: Setup for the game (general)                       *
*                                                                              *
*  FREE SPACE COLONISATION                                                     *
*      (c)2010 Paul Mueller <pmtech@swissonline.ch>                            *
*                                                                              *
*  This program is free software; you can redistribute it and/or modify        *
*  it under the terms of the GNU General Public License as published by        *
*  the Free Software Foundation; either version 2 of the License, or           *
*  (at your option) any later version.                                         *
*                                                                              *
*  This program is distributed in the hope that it will be useful,             *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of              *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
*  GNU Library General Public License for more details.                        *
*                                                                              *
*  You should have received a copy of the GNU General Public License           *
*  along with this program; if not, write to the Free Software                 *
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  *
*******************************************************************************/

/*******************************************************************************
* INCLUDES								                                       *
*******************************************************************************/

#include <memory.h>

#include "game.h"               /* Setup data struct */


#include "setupscr.h"           /* Own header */

/* *********************************************************************
* DEFINES							                                   *
********************************************************************** */

#define SUSCR_BASE_WSIZE      3 /* Start with 3 sectors (instead of 5)       */
#define SUSCR_BASE_HSIZE      3
#define SUSCR_BASE_TILESPERSECTOR 12
#define SUSCR_BASE_HABITABLE  50
#define SUSCR_BASE_DIFFICULTY 50


/* **********************************************************************
* DATA								                                    *
*********************************************************************** */

static GAME_STARTINFO DefaultGame = {

    /* ------------ Data about the size of the galaxy ----------------- */
    0,              /* Random seed to use for generation of map         */
                    /* if 0, a random seed is generated as the map is   */
                    /* created                                          */
    /* ------------ Data about the size of the galaxy ----------------- */
    SUSCR_BASE_WSIZE,
    SUSCR_BASE_HSIZE,            /* In sectors                          */
    SUSCR_BASE_TILESPERSECTOR,   /* For handling of sectors             */
    /* ------------ Data used while for map generation ---------------- */
	4,                       /* Number of leaders / nations   		    */
    SUSCR_BASE_HABITABLE,    /* The chance in percent from 'habitable'   */
    SUSCR_BASE_DIFFICULTY    /* How difficult it is for the human leader */
    
};
static GAME_STARTINFO SetupGame;

/* ****************************************************************************
* CODE  								                                      *
***************************************************************************** */


/* ========================================================================== */
/* ======================== PUBLIC FUNCTIONS	============================= */
/* ========================================================================== */

/*
 * Name:
 *     setupscrGame
 * Description:
 *     Initializes the data and handles the game setup screen and calls the
 *     'Main-Screen' after game setup, if needed.  
 * Input:
 *     None
 */
void setupscrGame(void)
{
    
        memcpy(&SetupGame, &DefaultGame, sizeof(GAME_STARTINFO));
        
        /* FIXME: Create screen and fill it with input fields */

}