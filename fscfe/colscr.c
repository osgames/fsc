/*******************************************************************************
*  COLSCR.C                                                                    *
*	 - Management screen for colony on planet                                  *
*                                                                              *
*  FREE SPACE COLONISATION                                                     *
*      (c)2002 - 2017 Paul Mueller <muellerp61@bluewin.ch>                     *
*                                                                              *
*  This program is free software; you can redistribute it and/or modify        *
*  it under the terms of the GNU General Public License as published by        *
*  the Free Software Foundation; either version 2 of the License, or           *
*  (at your option) any later version.                                         *
*                                                                              *
*  This program is distributed in the hope that it will be useful,             *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of              *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
*  GNU Library General Public License for more details.                        *
*                                                                              *
*  You should have received a copy of the GNU General Public License           *
*  along with this program; if not, write to the Free Software                 *
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  *
*******************************************************************************/

/*******************************************************************************
* INCLUDES								                                       *
*******************************************************************************/

#include <stdio.h>                  /* sprintf()    */
#include <string.h>                 /* strcpy()     */


#include "sdlgl.h"                  /* SDLGL_INPUT_T      */
#include "drawtool.h"
/*
#include "sdlgltex.h"
*/

#include "../code/colony.h"
/* Names for improvements and unittypes + labels */
#include "../code/game2str.h"


/* ---- Own header ---- */
#include "colscr.h"

/*******************************************************************************
* DEFINES  							                                           *
*******************************************************************************/

/* ------ List of buildings actually available (ColScrBuildStrings) ------ */
#define COLSCR_VARSTR_ACTPROJ   0   /* ActProjectStr  */
#define COLSCR_VARSTR_MILPROJ   1   /* MilitaryProjectStr  */
#define COLSCR_VARSTR_SOCPROJ   2   /* SocialProjectStr  */

/* ---------- ColonyInfoValues ---------- */
#define COLINFO_PLANETNAME  3
#define COLINFO_PLANETCLASS 4
#define COLINFO_INHABITANTS 5

/*******************************************************************************
* ENUMS                       		                                           *
*******************************************************************************/

#define SCCMD_NONE          0x00
#define SCCMD_CHOOSEUNIT    0x01
#define SCCMD_CHOOSESOCIAL  0x02
#define SCCMD_CHANGECOLONY  0x03
#define SCCMD_IMPROVEBUILT  0x04
#define SCCMD_CHOOSEBUILD   0x05
#define SCCMD_BUYBUILD      0x06
#define SCCMD_USE_ADJUST    0x07
#define SCCMD_INFOUNIT      0x08
#define SCCMD_INFOIMPROVE   0x09
#define SCCMD_CLOSESCR      0x20

#define COLSCR_MINIMAP    ((char)100)
#define COLSCR_PROJDESC   ((char)101)
#define COLSCR_ARROWLEFT  ((char)102)
#define COLSCR_ARROWRIGHT ((char)103)

/*******************************************************************************
* DATA  							                                           *
*******************************************************************************/

static GAME_PLAYINFO_T *pGamePlayInfo;

/* -- Info about a colony for test purposes */
static COLONY_INFO_T ColInfo =
{
    0,
    1,
    1,
    17, 17, /* Size of colony: Number of workers and idle  */
    8,      /* Maximum workers per yield        */
    { 0 },  /* Workers set per production       */
    { 0 },  /* Value per production             */
    { 0 },  /* 'prod_use' * 'prod_val'          */
    { { 0 }, { 0 } },   /* Projects */
    { 0, 0 },           /* Projects: Turns left */
    { 0 },              /* Improvements available */
    528,                /* Positon on map      */
    "Humans",
    "Earth",
    "M",
    { 22, 18, 15 },     /* planet_yield */
    1465                /* population   */
};

static char ProgressInfo[COLONY_PROJ_MAX][80];

static SDLGL_INPUT_T ColScr_Fields[128] =
{
    /* ----------- Sliderbox for choosing units (new kind) --------- */
    /* #0 */
    { SDLGL_TYPE_LABEL,    {  10, 432, 280, 10 }, 0, 0, NULL, SCCMD_INFOUNIT },
    { SDLGL_TYPE_SLIBOXV,  {  10, 442, 280, 60 }, SCCMD_CHOOSEUNIT, 0, &ColInfo.proj[COLONY_PROJ_MILITARY].which, SDLGL_VAL_CHAR },
    { SDLGL_TYPE_TEXTAREA, {  10, 502, 280, 30 }, 0, 0, &ProgressInfo[COLONY_PROJ_MILITARY] },
    /* --- Rectangle to draw description of actual project info (military/social) ------- */
    /* #3 */
    { COLSCR_PROJDESC,     { 300, 442, 200, 10 } },
    /* ----------- Sliderbox for choosing upgrades (new kind) --------- */
    /* #5 */
    { SDLGL_TYPE_LABEL,    { 510, 432, 280, 10 }, 0, 0, NULL, SCCMD_INFOIMPROVE },
    { SDLGL_TYPE_SLIBOXV,  { 510, 442, 280, 60 }, SCCMD_CHOOSESOCIAL, 0, &ColInfo.proj[COLONY_PROJ_SOCIAL].which, SDLGL_VAL_CHAR },
    { SDLGL_TYPE_TEXTAREA, { 510, 502, 280, 30 }, 0, 0, &ProgressInfo[COLONY_PROJ_SOCIAL] },
    /* ----------- Sliderbox with list of upgrades already built (new kind) --------- */
    /* #8 */
    { SDLGL_TYPE_SLIBOXV,  {  10, 195, 280, 204 }, SCCMD_IMPROVEBUILT, 0, &ColInfo.proj[COLONY_PROJ_SOCIAL].which, SDLGL_VAL_CHAR },
    /* ------------ Switch between planets ----------------------- */
    { COLSCR_ARROWLEFT,    { 327, 606,  22, 18 }, SCCMD_CHANGECOLONY, -1 },
    { COLSCR_ARROWRIGHT,   { 388, 606,  22, 18 }, SCCMD_CHANGECOLONY, +1 },
    { COLSCR_MINIMAP,      { 776, 45, 241, 241 } },
    /* ---------- Yield sliders ----------------- */
    /* FOOD */
    { SDLGL_TYPE_CHOOSEH, { 424, 108,  203, 16 }, SCCMD_USE_ADJUST, GVAL_FOOD, &ColInfo.prod_use[GVAL_FOOD], SDLGL_VAL_CHAR, 0, 8, 0, 8 },
    /* TAXES */
    { SDLGL_TYPE_CHOOSEH, { 424, 185,  203, 16 }, SCCMD_USE_ADJUST, GVAL_TAXES, &ColInfo.prod_use[GVAL_TAXES], SDLGL_VAL_CHAR, 0, 8, 0, 8 },
    /* RESEARCH */
    { SDLGL_TYPE_CHOOSEH, { 424, 214,  203, 16 }, SCCMD_USE_ADJUST, GVAL_RESEARCH, &ColInfo.prod_use[GVAL_RESEARCH], SDLGL_VAL_CHAR, 0, 8, 0, 8 },
    /* LUXURY   */
    { SDLGL_TYPE_CHOOSEH, { 424, 239,  203, 16 }, SCCMD_USE_ADJUST, GVAL_LUXURY, &ColInfo.prod_use[GVAL_LUXURY], SDLGL_VAL_CHAR, 0, 8, 0, 8 },
    /* MILITARY */
    { SDLGL_TYPE_CHOOSEH, { 424, 321,  203, 16 }, SCCMD_USE_ADJUST, GVAL_MILITARY, &ColInfo.prod_use[GVAL_MILITARY], SDLGL_VAL_CHAR, 0, 8, 0, 8 },
    /* SOCIAL   */
    { SDLGL_TYPE_CHOOSEH, { 424, 347,  203, 16 }, SCCMD_USE_ADJUST, GVAL_SOCIAL, &ColInfo.prod_use[GVAL_SOCIAL], SDLGL_VAL_CHAR, 0, 8, 0, 8 },
    /* STOCK    */
    { SDLGL_TYPE_CHOOSEH, { 424, 374,  203, 16 }, SCCMD_USE_ADJUST, GVAL_STOCK, &ColInfo.prod_use[GVAL_STOCK], SDLGL_VAL_CHAR, 0, 8, 0, 8 },
    /* ------------- Buttons ------------- */
    { SDLGL_TYPE_BUTTON,  { 410, 560, 94, 25 }, SCCMD_BUYBUILD },
    { SDLGL_TYPE_BUTTON,  { 250, 560, 94, 25 }, SCCMD_CHOOSEBUILD },
    { SDLGL_TYPE_BUTTON,  { 664, 600, 94, 25 }, SCCMD_CLOSESCR },
    /* --------- Print production points  ----------- */
    { SDLGL_TYPE_POINTBAR, { 424, 44, 240, 16 }, 0, 0, &ColInfo.pop_idle, SDLGL_VAL_CHAR, 0, 17, 0, 17 },
    /*
    { SDLGL_TYPE_VALUE, { 236, 109, 0, 0 }, 0, 0, &ColInfo.prod_use[GVAL_NETINCOME], SDLGL_VAL_INT },
    */
    /* ------ FOOD ----------- */
    { SDLGL_TYPE_VALUE, { 657, 110, 0, 0 }, 0, 0, &ColInfo.prod_total[GVAL_FOOD], SDLGL_VAL_INT },
    /* ------ ECONOMY -------- */
    { SDLGL_TYPE_VALUE, { 657, 187, 0, 0 }, 0, 0, &ColInfo.prod_total[GVAL_TAXES], SDLGL_VAL_INT },
    { SDLGL_TYPE_VALUE, { 657, 216, 0, 0 }, 0, 0, &ColInfo.prod_total[GVAL_RESEARCH], SDLGL_VAL_INT },
    { SDLGL_TYPE_VALUE, { 657, 241, 0, 0 }, 0, 0, &ColInfo.prod_total[GVAL_LUXURY], SDLGL_VAL_INT },
    /* ------ INDUSTRY ------- */
    { SDLGL_TYPE_VALUE, { 657, 323, 0, 0 }, 0, 0, &ColInfo.prod_total[GVAL_MILITARY], SDLGL_VAL_INT },
    { SDLGL_TYPE_VALUE, { 657, 349, 0, 0 }, 0, 0, &ColInfo.prod_total[GVAL_SOCIAL], SDLGL_VAL_INT },
    { SDLGL_TYPE_VALUE, { 657, 376, 0, 0 }, 0, 0, &ColInfo.prod_total[GVAL_STOCK], SDLGL_VAL_INT },
    /* ------ Labels ------- */
    { SDLGL_TYPE_TEXT,  {  10,  21, 120, 14 }, 1, 0, ColInfo.planet_name, SDLGL_VAL_STRING, 0, 19 },
    { SDLGL_TYPE_VALUE, { 408,  21,  96,  0 }, 0, 0, &ColInfo.population, SDLGL_VAL_INT },
    { SDLGL_TYPE_LABEL, {  60,  44,  50, 10 }, 0, 0, ColInfo.planet_sign },
    { SDLGL_TYPE_VALUE, {  60,  90,  50, 10 }, 0, 0, &ColInfo.morale, SDLGL_VAL_CHAR },
    { SDLGL_TYPE_LABEL, {  10,  67,  50, 10 }, 0, 0, ColInfo.nation_name },
    { SDLGL_TYPE_VALUE, {  60, 113,  50, 10 }, 0, 0, &ColInfo.netincome, SDLGL_VAL_INT },
    { SDLGL_TYPE_VALUE, {  60, 136,  50, 10 }, 0, 0, &ColInfo.expenses, SDLGL_VAL_INT },
    { SDLGL_TYPE_VALUE, {  60, 159,  50, 10 }, 0, 0, &ColInfo.prod_total[GVAL_DEFENSE], SDLGL_VAL_INT },
    /* --------- Slider labels --------- */
    { 0 }
};

static SDLGL_LABEL_T ColScr_Labels[] =
{
    { SDLGL_VAL_STRING, 312,  21, "Population:", 8, 'L' },
    { SDLGL_VAL_STRING,  10,  44, "Class:" },
    { SDLGL_VAL_STRING,  10,  90, "Morale:" },
    { SDLGL_VAL_STRING,  10, 113, "Income:" },
    { SDLGL_VAL_STRING,  10, 136, "Expenses:" },
    { SDLGL_VAL_STRING,  10, 159, "Defense:" },
    { SDLGL_VAL_STRING, 312,  44, "Citizens:" },
    { SDLGL_VAL_STRING, 312,  82, "Production Overview" },
    { SDLGL_VAL_STRING, 604,  82, "Yield" },
    { SDLGL_VAL_STRING, 312, 108, "FOOD" },
    { SDLGL_VAL_STRING, 424, 155, "ECONOMY" },
    { SDLGL_VAL_STRING, 312, 185, "Taxes" },
    { SDLGL_VAL_STRING, 312, 214, "Research" },
    { SDLGL_VAL_STRING, 312, 239, "Luxury" },
    { SDLGL_VAL_STRING, 424, 289, "INDUSTRY" },
    { SDLGL_VAL_STRING, 312, 321, "Military" },
    { SDLGL_VAL_STRING, 312, 347, "Social" },
    { SDLGL_VAL_STRING, 312, 374, "Stock" },
    { SDLGL_VAL_STRING,  10, 432, "Military Projects" },
    { SDLGL_VAL_STRING, 300, 432, "Project Decription" },
    { SDLGL_VAL_STRING, 510, 432, "Social Projects" },
    { SDLGL_VAL_STRING, 410 + 47, 560 + 8, "Buy", 0, 0, 'C' },
    { SDLGL_VAL_STRING, 250 + 47, 560 + 8, "Choose", 0, 0, 'C'  },
    { SDLGL_VAL_STRING, 664 + 47, 600 + 8, "Done", 0, 0, 'C'  },
    { 0 }
};

static SDLGL_INPUTKEY_T ColScr_CmdKey[] =
{
    { { SDLK_RETURN }, SCCMD_CLOSESCR },
    { { 0 } }
};

    /* =========== FIXME: Add here the 'COLONY-Details' (subscreen) ======== */

/*******************************************************************************
* CODE								                                           *
*******************************************************************************/

/*
 * Name:
 *     colscr_update_projinfo
 * Description:
 *     Does an update on the strings which display the progress of the actual
 *     chosen projects.
 * Input:
 *     pinfo *: Pointer on info about colony to manage
 */
static void colscr_update_projinfo(COLONY_INFO_T *pinfo)
{
    char build_info[40];
    char *pname;
    int  i;


    for(i = 0; i < COLONY_PROJ_MAX; i++)
    {
        if(i == COLONY_PROJ_MILITARY)
        {
            pname = game2str_label(GAME2STR_LABEL_UNITTYPENAME, pinfo->proj[i].which);
        }
        else if(i == COLONY_PROJ_SOCIAL)
        {
            pname = game2str_label(GAME2STR_LABEL_IMPROVENAME, pinfo->proj[i].which);
        }
        else
        {
            pname = "";
        }

        if(ColInfo.proj_turns_left[i] > 0)
        {
            sprintf(build_info, "Time to build: %d", ColInfo.proj_turns_left[i]);
        }
        else
        {
            sprintf(build_info, "%s", "Construction halted");
        }

        sprintf(ProgressInfo[i], "%s\n%s", pname, build_info);
    }
}

/*
 * Name:
 *     colscr_init_buildinfo
 * Description:
 *     Does an update on the variable strings for given field. For planet
 *     actually managed.
 * Input:
 *     pinfo *:     Pointer on info about colony to manage
 *     pgameinfo *: Pointer on game info
 */
static void colscr_init_buildinfo(COLONY_INFO_T *pinfo, GAME_PLAYINFO_T *pgameinfo)
{
    SDLGL_INPUT_T *pfields;


    pfields = ColScr_Fields;

    while(pfields->sdlgl_type)
    {
        switch(pfields->code)
        {
            case SCCMD_CHOOSEUNIT:
                pfields->val_max = pgameinfo->num_ut_avail;
                pfields->el_visi = 10;
                pfields->el_top  = 0;
                break;

            case SCCMD_CHOOSESOCIAL:
                pfields->val_max = pinfo->num_imp_avail;
                pfields->el_visi = 10;
                pfields->el_top  = 0;
                break;

            case SCCMD_IMPROVEBUILT:
                pfields->val_max = pinfo->num_imp_built;
                pfields->el_visi = 10;
                pfields->el_top  = 0;
                break;
        }

        pfields++;
    }

    /* ---- What is the status of the actual projects ? --- */
    colscr_update_projinfo(pinfo);
}

#if 0
/*
 * Name:
 *     colscr_change_colony
 * Description:
 *     Changes the chosen planet
 * Input:
 *     dir: Direction in list
 * Output:
 *     None
 */
static void colscr_change_colony(int dir)
{
    int colony_no;


    /* fsctoolListMoveCursor */
    if (fsctool_list_movecursor(&pGamePlayInfo->colony_list[0],
                                &pGamePlayInfo->colony_cursor,
                                &colony_no, dir))
    {
        /* Only refresh display info, if colony has changed */
        colony_get_info(colony_no, &ColInfo, 1);

        colscr_init_buildinfo(&ColInfo, pGamePlayInfo);

        /* Create the planetary system for display and add it to display  */
        /* infobarTileInfo(ColInfo.pos, pGamePlayInfo->player_no, ColInfo.planet_no); */
    }
}
#endif

/*
 * Name:
 *     colscr_drawfield_owner
 * Description:
 *     Display function for owner drawing, callback for ''sdlgldraw_drawfields
 * Input:
 *      input_code: Command-Code from Caller
 *      prect *:
 *      el_no:   Draw this element from list, if Slider-Box
 */
static void colscr_drawfield_owner(SDLGL_INPUT_T *pfield, SDLGL_RECT_T *prect, int el_no)
{
    char string[100];


    switch(pfield->code)
    {
        case SCCMD_CHOOSEUNIT:
            /* @TOD0: Draw string with name of Unit-Type -- single element*/
            sprintf(string, "Unit Element %d", el_no);
            break;

        case SCCMD_CHOOSESOCIAL:
            /* @TOD0: Draw name of social improvement to be built */
            sprintf(string, "Social Element %d", el_no);
            break;

        case SCCMD_IMPROVEBUILT:
            /* @TOD0: Draw name of social improvement built */
            sprintf(string, "Built Improve Element %d", el_no);
            break;

        case COLSCR_PROJDESC:
            /* @TOD0: Draw description of chosen project military / improvement */
            break;

        case SCCMD_CHANGECOLONY:
            if(pfield->code < 0)
            {
                /* @TOD0: Draw an arrow */
                drawtool_drawrect_colno(prect, SDLGL_COL_LIGHTGREY, 1);
                /* sdlgldraw_draw_button(prect, "L", 0); */
            }
            break;

        case COLSCR_ARROWRIGHT:
            /* @TOD0: Draw an arrow */
            drawtool_drawrect_colno(prect, SDLGL_COL_LIGHTGREY, 1);
            /* sdlgldraw_draw_button(prect, "R", 0); */
            break;
    }
}

/*
 * Name:
 *     colscr_drawfunc
 * Description:
 *     Display function for the screens set by the "menucmd" functions.
 * Input:
 *      pfields *: Pointer on array of fields to draw
 *      pevent *:  Event holding different info used for drawing
 */
static void colscr_drawfunc(SDLGL_INPUT_T *pfields, SDLGL_EVENT_T *pevent)
{
    /*
    static SDLGL_RECT_T MiniMapRect = { 776, 45, 241, 241 };
    static SDLGL_RECT_T WorkersRect = { 410, 44, 256,  16 };
    */

    /* Simply clear the screen  */
    glClear(GL_COLOR_BUFFER_BIT);

    drawtool_drawfields(pfields, colscr_drawfield_owner);

    /*
    / * -------- Draw fixed things which need no input ------ * /
    infobarDrawGlobalInfo(3,  3);          / * Draw the info about money * /
    mapdrawMiniMap(&MiniMapRect, pGamePlayInfo->nation_no);

    / * Draw the part using the own functions for fields  * /
    if (fields) {

        / * 1) Draw owner drawn fields * /
        colscrOwnerDrawFields(fields);

        / * 2) Draw textures     * /
        sdlgltexDisplayIconList(fields, 0);
    }

    / * -----Print fixed labels (must be adjusted for language support -- * /
    fscfont_print_labels(0, 0, &ColonyScreenLabels[2]);
    / * ----- Variable strings, that change on input -------------------- * /
    fscfont_print_labels(0, 0, ColScrBuildStrings);
    / * -------- Print info from 'colony' data -------------------------- * /
    fscfont_print_labels(0, 0, &ColonyInfoValues[0]);
    / * -------- Info about what's built (actual project) --------------- * /
    fscfontPrintLabelValues(0, 0, InfoBuildLabels);
    */

    /* -- Draw all labels -- */
    drawtool_labels(ColScr_Labels);
}

/*
 * Name:
 *     colscr_yield_adjust
 * Description:
 *     Checks the number of the workers set for yields.
 *     Adjust them according to the type of yield.
 * Input:
 *     yield_no: This yield has changed
 * Last change:
 *     2017-07-30 / <bitnapper>
 */
static void colscr_yield_adjust(int yield_no)
{
    int i, use_diff, total_use;
    char *pprod_use;


    pprod_use = ColInfo.prod_use;

    total_use = 0;

    for(i = 0; i < GVAL_COLSHARE_MAX; i++)
    {
        total_use += pprod_use[i];
    }

    use_diff = ColInfo.pop_points - total_use;

    if(use_diff >= 0)
    {
        /* Push it back into buffer for available workers */
        ColInfo.pop_idle = use_diff;
    }
    else if(use_diff < 0)
    {
        /* The player has 'overshot' with choosing */
        pprod_use[yield_no] += use_diff;
        #if 0
        switch(yield_no)
        {
            case GVAL_FOOD:
                /* Remove them if food */

                break;

            case GVAL_MILITARY:
            case GVAL_SOCIAL:
                /* -- Take first from stock */
                total_use = pprod_use[GVAL_STOCK];
                if(total_use > 0)
                {
                    use_diff += total_use;
                    /* if it's still not enough, then take from other production */
                    shared_no = (yield_no == GVAL_MILITARY) ? GVAL_SOCIAL : GVAL_MILITARY;
                    if(use_diff < 0)
                    {
                        total_use = pprod_use[shared_no];
                    }
                }
                break;
        }
        #endif
    }

    /* Do an update on the values for the use of the yields */
    colony_calc_yielduse(&ColInfo);
}

/*
 * Name:
 *     colscr_process_input
 * Description:
 *     Translates the input for the star system screen
 * Input:
 *     pevent *: Struct holding the input event info
 * Last change:
 *     2017-06-28 / PAM
 */
static int colscr_process_input(SDLGL_EVENT_T *pevent)
{
    if(pevent->code > 0)
    {
        switch(pevent->code)
        {
            case SCCMD_USE_ADJUST:
                colscr_yield_adjust(pevent->sub_code);
                break;

            case SCCMD_CHOOSEUNIT:
                break;

            case SCCMD_CHOOSESOCIAL:
                break;


            case SCCMD_CHOOSEBUILD:
                #if 0
                /* Build actual displayed unit/improvement  */
                if (DisplayProjectType == COLONY_PROJ_MILITARY)
                {
                    buildno = ColInfo.ut_avail[UnitChooseBox.actel];
                    ColScrBuildStrings[COLSCR_VARSTR_MILPROJ].plabel = UnitBuildLabels->plabel;
                }
                else
                {
                    buildno = ColInfo.imp_avail[SocialChooseBox.actel];
                    ColScrBuildStrings[COLSCR_VARSTR_SOCPROJ].plabel = SocialBuildLabels->plabel;
                }

                ColInfo.pcolony->proj[DisplayProjectType].which = buildno;
                /* Recalc turns needed for production of improvement / unit */
                colonyGetMngInfo(0, &ColInfo, COLONY_MNG_VALUES);
                #endif
                break;

            case SCCMD_BUYBUILD:
                /* Buy actual displayed unit/improvement    */
                /* FIXME: Add proper code, checking if it can be buyed and so on */
                /* colscrBuyBuild(pPlanet, buildno, DisplayProjectType); */
                break;

            case SCCMD_INFOUNIT:
                /* @TODO: Switch build display to info about actual built unit (Pictre + Text) */
                break;

            case SCCMD_INFOIMPROVE:
                /* @TODO: Switch build display to info about actual built improvement (Pictre + Text) */
                break;

            case SCCMD_CHANGECOLONY:
                /* Subcode is direction */
                /*
                colony_get_info(-1, &ColInfo, pevent->sub_code);
                */
                 /*
                colscr_change_colony();
                */
                break;

            case SCCMD_CLOSESCR:
                return SDLGL_INPUT_REMOVE;
        }
    }

    return SDLGL_INPUT_OK;
}

/* ========================================================================== */
/* =========================== PUBLIC FUNCTION(S) =========================== */
/* ========================================================================== */

/*
 * Name:
 *     colscr_main
 * Description:
 *     Opens the management screen for the given colony
 * Input:
 *     pgameinfo *: Pointer about general info for player about game
 */
void colscr_main(GAME_PLAYINFO_T *pgameinfo)
{
    /* int colony_no; ==> pinfo->act_colony */


    pGamePlayInfo = pgameinfo;

    /* There's a colony to manage, get the info for management */
    /* colony_get_info(colony_no, &ColInfo); */

    /* pinfo->chosen_planetno = ColInfo.planet_no; */

    colscr_init_buildinfo(&ColInfo, pgameinfo);

    colony_calc_yielduse(&ColInfo);

    sdlgl_input_new(colscr_drawfunc,
                    colscr_process_input,
                    ColScr_Fields,
                    ColScr_CmdKey,
                    128 - 1,
                    0);

    /* @TODO: Create the planetary system for display and add it to display  */
    /* infobarTileInfo(ColInfo.pos, mng->nation_no, mng->chosen_planetno); */
    /* Display the actual chosen build projects */
    /*
    ColScrBuildStrings[1].plabel = unittypeGetName(ColInfo.pcolony->proj[COLONY_PROJ_MILITARY].which);
    ColScrBuildStrings[2].plabel = improveName(ColInfo.pcolony->proj[COLONY_PROJ_SOCIAL].which);
    */
}


