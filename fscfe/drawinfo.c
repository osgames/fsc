/*******************************************************************************
*  DRAWINFO.C                                                                   *
*      - Functions and data for handling of infobar menu				       *
*                                                                              *
*  FREE SPACE COLONISATION                                                     *
*      (c)2002 - 2017 Paul Mueller <muellerp61@bluewin.ch>                     *
*                                                                              *
*  This program is free software; you can redistribute it and/or modify        *
*  it under the terms of the GNU General Public License as published by        *
*  the Free Software Foundation; either version 2 of the License, or           *
*  (at your option) any later version.                                         *
*                                                                              *
*  This program is distributed in the hope that it will be useful,             *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of              *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
*  GNU Library General Public License for more details.                        *
*                                                                              *
*  You should have received a copy of the GNU General Public License           *
*  along with this program; if not, write to the Free Software                 *
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  *
*******************************************************************************/

/*******************************************************************************
* INCLUDES                              				                       *
*******************************************************************************/

#include <memory.h>             /* memcpy(), memset()   */
#include <string.h>             /* strncpy()            */


/*
#include "sdlgl.h"
#include "sdlgltex.h"
#include "fscfont.h"
#include "fscinit.h"
#include "drawtool.h"


#include "fsccmd.h"
#include "fscmap.h"
#include "improve.h"
#include "msg.h"
#include "language.h"
#include "nation.h"
#include "outpost.h"
#include "starinfo.h"
#include "star.h"
#include "unit.h"
#include "unitinfo.h"           / * Holding comands to set           * /
#include "unitrule.h"           /* Get possible actions for unit    * /
*/

#include "drawinfo.h"

/*******************************************************************************
* DEFINES                                  				                       *
*******************************************************************************/

/* ----- Different cursors ------ */
#define DRAWINFO_CURSOR_FIX       0
#define DRAWINFO_CURSOR_GREEN     1
#define DRAWINFO_CURSOR_RED       2
#define DRAWINFO_CURSOR_BLUE      3
#define DRAWINFO_CURSOR_OWNERRECT 4
#define DRAWINFO_DRAW_NOTHING     10000

#define DRAWINFO_MSGLINELEN    119
#define DRAWINFO_DEBUGINFOSIZE 256

/* --------- Position of unit orders menu -------- */

#define DRAWINFO_UNITORDER_W 244
#define DRAWINFO_UNITORDER_H 160

#define DRAWINFO_UNITINFO_Y  344
#define DRAWINFO_UNITINFO_H   80     /* Including command */

#define DRAWINFO_MAX_MSG 100

/*******************************************************************************
* TYPEDEFS                                                                     *
*******************************************************************************/

typedef struct {

    /* ------ Strings always displayed on main screen --------------------- */
    char sectorname[32];    /* Actual chosen sector as text                 */
    char datestr[20];       /* Actual game date as string                   */
    char moneystr[20];      /* Actual amount of money as string             */
    char popvalstr[20];     /* Population as string                         */

}
INPUT_STATE_T;

/*******************************************************************************
* DATA                                    				                       *
*******************************************************************************/

/* === Static data for display of a star system, to be filled, if needed == */
static SDLGL_INPUTMOU_T Starsys_Info[] = 
{
    /* #0: The Star */
    { SDLGL_TYPE_TEXTURE, { 300, 0, 64, 64 0}
};


/*******************************************************************************
* CODE                                    				                       *
*******************************************************************************/



/* ========================================================================== */
/* =========================== PUBLIC FUNCTION(S) =========================== */
/* ========================================================================== */

/*
 * Name:
 *     drawinfo_starsys
 * Description:
 *     Set input code about a tile at given position, if any is visible
 *     for the caller.
 *     - Star
 *     - Planets
 * Input:
 *     star_no:   Set info for this star
 *     planet_no: This planet is active
 */
void drawinfo_starsys(int star_no, int planet_no)
{
}
