/*******************************************************************************
*  AITOOLS.C                                                                   *
*	   - Code for AI-management of diplomacy                                   *
*                                                                              *
*   FREE SPACE COLONISATION			                                           *
*      (c)2002 - 2017 Paul Mueller <muellerp61@bluewin.ch>                     *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*******************************************************************************/

/*******************************************************************************
* INCLUDES                                                                     *
*******************************************************************************/

#include "aitool.h"

/*******************************************************************************
* CODE                                                                         *
*******************************************************************************/

/* ========================================================================== */
/* ============================= PUBLIC PROCEDURE(S) ======================== */
/* ========================================================================== */

/*
 * Name:
 *     aitool_get_effectvalue
 * Description:
 *     Returns the 'value' for given effect(s) baesd on 'pweights'
 * Input:
 *     pge *:      Pointer on effects to calculate
 *     weights *:  Pointer on array with weigths for the different YIELDS
 *     num_effect: Number of effects in 'pge'
 * Output:
 *     Weighted value for given effect(s)
 */
int aitool_get_effectvalue(GAME_EFFECT_T *pge, char *pweights, int num_effect)
{
    int i, value, weight;


    value = 0;

    for(i = 0; i < num_effect; i++)
    {
        weight = (pweights) ? (int)(pweights[pge->type]) : 1;
        value += (int)pge[i].amount * (int)pge[i].range * weight;
    }

    return value;
}

/*
 * Name:
 *     aitool_amortize
 * Description:
 *     Returns the amortization for given 'value'
 * Input:
 *     value: Amortize this value
 *     base:  Amotization-base
 * Output:
 *     Amortized value
 */
int aitool_amortize(int value, int base)
{

    /* FIXME: Add proper code */
    return value * base;

}




