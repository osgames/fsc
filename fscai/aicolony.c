/*******************************************************************************
*  AICOLONY.C                                                                  *
*	   - Code for AI-management of planets                                     *
*                                                                              *
*  FREE SPACE COLONISATION               	                                   *
*      Copyright (C) 2002-2017  Paul Müller <muellerp61@bluewin.ch>            *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*******************************************************************************/

/*******************************************************************************
* INCLUDES                                                                     *
*******************************************************************************/

#include "../code/colony.h"
#include "../code/effect.h"
#include "../code/fsctool.h"
#include "../code/game.h"
#include "../code/improve.h"
#include "../code/unittype.h"


#include "aitool.h"

/* -- Own header -- */
#include "aicolony.h"

/*******************************************************************************
* DATA                                                                         *
*******************************************************************************/

/*******************************************************************************
* CODE                                                                         *
*******************************************************************************/

/*
 * Name:
 *     aicolony_manage_social
 * Description:
 *     Asseses what social improvement to build
 * Input:
 *     pinfo *:    Pointer on management info for given colony
 *     pweights *: Pointer on decision weight of calling leader
 */
static void aicolony_manage_social(COLONY_INFO_T *pinfo, char *pweights)
{
    int chosen;


    /* Assess what's best to build next */

    chosen = pinfo->imp_avail[0];


    /*
        --> First calc need of different effects <--
        need         = 0;
        best_need    = 0;
        best_improve = 0;
        al = mng -> imp_avail;

        while(*al > 0) {

            need += effectCalcWeightedValue(improveGetEffect(*al), pweight);
            al++;

        }
    */
    pinfo->proj[COLONY_PROJ_SOCIAL].which = chosen;
}

/*
 * Name:
 *     aicolony_manage_military
 * Description:
 *     Asseses what military improvement to build.
 * Input:
 *     pinfo *:     Pointer on management info for given colony
 *     put_avail *: Pointer on numbers of available unit types
 *     pweights *:  Pointer on decision wieight of calling leader
 */
static void aicolony_manage_military(COLONY_INFO_T *pinfo, char *put_avail, char *pweight)
{
    int chosen, num_ut_avail;


    num_ut_avail = 0;

    while(*put_avail > 0)
    {
        put_avail++;
        num_ut_avail++;
    }
    /* TODO: Assess what's best to build next */
    /* TODO: Replace by decision of 'advmilitary', corrected by 'pweight' */
    /* Choose either 'UNIT_ABILITY_CONSTRUCT' or 'UNIT_ABILITY_COLONIZE' */
    chosen = fsctool_rand_no(num_ut_avail) - 1;

    pinfo->proj[COLONY_PROJ_SOCIAL].which = chosen;

    /* colony_set_project(pinfo->colony_no, COLONY_PROJ_MILITARY, pginfo->ut_avail[chosen]); */
}

/*
 * Name:
 *     aicolony_manage_nogrow
 * Description:
 *     TODO:
 *          - Build improvement for growth (YIELD_FOOD),
 *          - Build colony ships to 'export' people
 * Input:
 *     mng *:     Pointer on management info for given colony
 *     weights *: Pointer on decision wieight of calling leader
 */
static void aicolony_manage_nogrow(COLONY_INFO_T *pinfo, char *weights)
{

}

/*
 * Name:
 *     aicolony_manage_famine
 * Description:
 *     TODO:
 *          - Rearrange workers
 *          - Build improvement which removes the famine
 * Input:
 *     mng *:     Pointer on management info for given colony
 *     weights *: Pointer on decision wieight of calling leader
 */
static void aicolony_manage_famine(COLONY_INFO_T *pinfo, char *weights)
{
    /* TODO: Arrange workers */
}

/*
 * Name:
 *     aicolony_manage_growth
 * Description:
 *     TODO:
 *          - Rearrange workers based on actual weights
 *          - Examine the sharing of workers
 * Input:
 *     mng *:     Pointer on management info for given colony
 *     weights *: Pointer on decision wieight of calling leader
 */
static void aicolony_manage_growth(COLONY_INFO_T *pinfo, char *weights)
{
    /* TODO: Arrange workers */
}

/*
 * Name:
 *     aicolony_assess_danger
 * Description:
 *
 * Input:
*     mng *: Pointer on management info for given colony
 */
static void aicolony_assess_danger(COLONY_INFO_T *pinfo)
{

    /* TODO: Create proper code, use some kind of 'military' advisor    */
    /* TODO: Use a danger-map, created at start of turn -- same use for military decisions */
    /* TODO: Set GOALs to be taken into account by UNITs and COLONIE's  */

}

/* ========================================================================== */
/* ========================= PUBLIC FUNCTIONS =============================== */
/* ========================================================================== */

/*
 * Name:
 *     aicolonyManage
 * Description:
 *     Manages the colony with given number
 * Input:
 *     colony_no:       Number of 'Colony' to manage
 *     why:             What what has changed in colony
 *     put_avail *:     Pointer on list with numbers with unit-types to choose from
 *     pweight *:       Weight of player for choosing
 */
void aicolony_manage(int colony_no, int why, char *put_avail, char *pweight)
{
    COLONY_INFO_T col_info;


    colony_get_info(colony_no, &col_info, 0);

    /* @TODO: Look for goals we can fullfill in the GOALS list */
    switch(why)
    {
        case GMSG_IMPROVEBUILT:
            aicolony_manage_social(&col_info, pweight);
            break;

        case GMSG_UNITBUILT:
            aicolony_manage_military(&col_info, put_avail, pweight);
            break;

        case GMSG_NOGROW:
            aicolony_manage_nogrow(&col_info, pweight);
            break;

        case GMSG_FAMINE:
            aicolony_manage_famine(&col_info, pweight);
            break;

        case GMSG_POPULATION_GROWN:
            aicolony_manage_growth(&col_info, pweight);
            break;

        default:
            /* Manage completely */
            aicolony_assess_danger(&col_info);
            aicolony_manage_social(&col_info, pweight);
            aicolony_manage_military(&col_info, put_avail, pweight);
    }

    /* @TODO: Write the information back to colony */
}
