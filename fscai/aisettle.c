/*******************************************************************************
*  AISETTLE.C                                                                  *
*	   - Code for AI-management of colony ships                                *
*                                                                              *
*   FREE SPACE COLONISATION             				                     *
*       Copyright (C) 2010  Paul Mueller <pmtech@swissonline.ch>               *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*******************************************************************************/

/*******************************************************************************
* INCLUDES                                                                     *
*******************************************************************************/

#include "../code/fscmap.h"
#include "../code/game.h"
#include "../code/msg.h"
#include "../code/starsys.h"
#include "../code/unitinfo.h"
#include "../code/unitmove.h"


/* -- Own header -- */
#include "aisettle.h"

/*******************************************************************************
* CODE                                                                         *
*******************************************************************************/

/*
 * Name:
 *     aisettle_log
 * Description:
 *     Logs the orders given by the AI to the unit
 * Input:
 *     punit_mng *: This unit is doing something
 *     order:       This order
 *     planet_no:   This is the planet, if a planet
 *     pos:         This is the positon, if a GOTO
 */
static void aisettle_log(UNIT_INFO_T *punit_mng, int order, int planet_no, int pos)
{
    MSG_INFO_T msg;

    msg_prepare(0, punit_mng->pi.owner, MSG_TYPE_LOG, &msg);

    msg.num       = MSG_ORDER_UNIT;
    msg.planet_no = planet_no;
    msg.map_x     = punit_mng->pi.x;
    msg.map_y     = punit_mng->pi.y;
    msg.pos       = -1;
    msg.unit_no   = punit_mng->unit_no;
    msg.args[0]   = order;

    msg_send(&msg, 0);
}

/*
 * Name:
 *     aisettle_colonize
 * Description:
 *     Colonizes best planet in given planet list
 *     If none can be colonized directly, the most valuable is terraformed
 * Input:
 *     punit_mng *: This unit is doing something
 *     pstar_planets *: Pointer on array with numbers of planets
 *     unit_pos:     Position for logging
 * Output:
 *     Order could be set yes/no
 */
static int aisettle_colonize(UNIT_INFO_T *punit_mng, int *pstar_planets, int unit_pos)
{
    PLANET_INFO_T mng;
    int *pplanet_no;
    int act_val, best_val, best_planet;


    /* TODO: Use function 'starBestPlanet()' and a value of 16'000 as usable value */
    pplanet_no = pstar_planets;

    while(*pplanet_no > 0)
    {
        starsys_get_planetinfo(*pplanet_no, &mng);

        if(mng.settle_class == 'M')
        {
            /* TODO: Check the size (minimum 17), take terraforming better planet int account */
            unitmove_set_order(punit_mng->unit_no, ORDER_COLONIZE, *pplanet_no, 0);

            aisettle_log(punit_mng, ORDER_COLONIZE, *pplanet_no, unit_pos);

            return 1;
        }

        pplanet_no++;
    }

    /* ---------- Only planets that must be terraformed --------------- */
    pplanet_no = pstar_planets;

    best_val    = 0;
    best_planet = 0;

    while(*pplanet_no > 0)
    {
        starsys_get_planetinfo(*pplanet_no, &mng);

        /* TODO: Take weights of player info account */
        act_val = mng.yield_size * mng.yield[GVAL_FOOD];

        if(act_val > best_val)
        {
            best_val    = act_val;
            best_planet = *pplanet_no;
        }

        pplanet_no++;
    }

    if(best_planet > 0)
    {
        unitmove_set_order(punit_mng->unit_no, ORDER_TERRAFORM, *pplanet_no, 0);

        aisettle_log(punit_mng->unit_no, ORDER_TERRAFORM, *pplanet_no, unit_pos);

        return 1;
    }

    return 0;
}

/*
 * Name:
 *     aisettle_goto_star
 * Description:
 *     Goes to the first star in range of colonize-unit
 * Input:
 *     pui *: Pointer on management data of actual chosen unit
 * Output:
 *     Found a GOTO-Pos yes/no
 */
static int aisettle_goto_star(UNIT_INFO_T *pui)
{
    int num_ti;
    FSCMAP_TILEINFO_T ti[3];


    /* Get next star to move to */
    num_ti = fscmap_get_rangeinfo(&pui->pi, pui->range_no, ti, 1, FSCMAP_FINFO_STAR);

    /* @TODO: Check for better planetary system / take from leaders GOAL_T / Check danger  */
    if(num_ti > 0)
    {
        unitmove_set_order(pui->unit_no, ORDER_GOTO, ti[0].pos, 0);

        aisettle_log(pui, ORDER_GOTO, 0, ti[0].pos);

        return 1;
    }

    return 0;
}

/* ========================================================================== */
/* ============================= PUBLIC PROCEDURE(S) ======================== */
/* ========================================================================== */

/*
 * Name:
 *     aisettle_manage
 * Description:
 *     Manages unit which info is held in the unit-manage data
 * Input:
 *     punit_mng *: Pointer on management data
 * Output:
 *     Found job, yes/no
 */
void aisettle_manage(UNIT_INFO_T *punit_mng)
{
    FSCMAP_TILEINFO_T ti;
    int has_info;
    int unit_no;


    has_info = fscmap_get_tileinfo(&punit_mng->pi, &ti, FSCMAP_FINFO_STAR);

    if(has_info > 0)
    {
        /* We are in a un-owned planetary system. Check, if we want to settle here */
        if(! aisettle_colonize(punit_mng, ti.star_planets, punit_mng->map_pos))
        {
            /* TODO: Look for a better planetary system to colonize */
            aisettle_goto_star(punit_mng);
        }
    }
    else
    {
        /* TODO: If already colonized by us:
            - Is it worth to colonize a second planet in this planetary system ?
            - Is it worth to immigrate in a colony already exisiting ?
            - Don't immigrate to 'home' system
        */
        aisettle_goto_star(punit_mng);
        /* TODO: If GOTO can't be reached, because we are stuck at range radius,
              call for help (add the GOAL_T 'range' at 'pos')
              Need EFFECT "RANGE" at position of unit
         */
    }
}

