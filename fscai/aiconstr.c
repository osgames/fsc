/*******************************************************************************
*  AICONSTR.C                                                                  *
*	   - Code for AI-management constructors                                   *
*                                                                              *
*   FREE SPACE COLONISATION                                                    *
*       Copyright (C) 2002 - 2017 Paul Mueller <muellerp61@bluewin.ch>         *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*******************************************************************************/

/*******************************************************************************
* INCLUDES                                                                     *
*******************************************************************************/

#include <stdio.h>


#include "../code/fscmap.h"
#include "../code/fsctool.h"
#include "../code/improve.h"        /* Possible outposts                */
#include "../code/starsys.h"
#include "../code/unit.h"
#include "../code/unitinfo.h"
#include "../code/unitmove.h"       /* struct UNIT_INFO_T               */
#include "../code/unitrule.h"       /* Possible 'outposts' on planet    */


#include "aitool.h"


/* -- Own header -- */
#include "aiconstr.h"

/*******************************************************************************
* CODE                                                                         *
*******************************************************************************/

/*
 * Name:
 *     aiconstr_goto_border
 * Description:
 *     Looks for nearest star off this units range and moves toward it.
 * Input:
 *     pui *:   Pointer on info about unit to manage
 *     unit_no: Number of unit for order
 * Output:
 *     Could set order successful
 */
static int aiconstr_goto_border(UNIT_INFO_T *pui)
{
    FSCMAP_TILEINFO_T ti;
    int num_ti;


    /* Get next star to move to */
    num_ti = fscmap_get_rangeinfo(&pui->pi,
                                  pui->range_no,
                                  &ti,
                                  1,
                                  FSCMAP_FINFO_STAR | FSCMAP_FINFO_OFFRANGE);

    /* @TODO: Check for better planetary system / take from leaders GOAL_T / Check danger  */
    if(num_ti > 0)
    {
        unitmove_set_order(pui, ORDER_GOTO, ti.pos, 0);

        return 1;
    }

    return 0;
}

/*
 * Name:
 *     aiconstr_project_value
 * Description:
 *     Calculates the value of an
 *     Checks all given planets for possibility for building an outpost.
 * Input:
 *     pos:       At this position  (for range)
 *     planet_no: Check this planet
 *     project:   This projet
 * Output:
 *     Value of building given project
 */
static int aiconstr_project_value(int pos, int planet_no, char project)
{
    IMPROVE_INFO_T improve_info;
    int num_effect;

    /*
        TODO: Get the following values into account:
        pplanet->growth + pplanet->size ==> Do terraform YIELD_TERRAFORM
        pplanet->yield[YIELD_FOOD],
        pplanet->yield[YIELD_RESOURCES],
        pplanet->yield[YIELD_ECONOMY]);
    */

    num_effect = improve_get_info(project, &improve_info, 0, 0);

    return aitool_get_effectvalue(&improve_info.effect, NULL, num_effect);
}

/*
 * Name:
 *     aiconstr_build_planet
 * Description:
 *     Checks all given planets for possibility for building an outpost.
 * Input:
 *     ti*:          Info about tile to check its planets
 *     punit_info *: Pointer on info about unit to handle
 * Output:
 *     Could set order successful
 */
static int aiconstr_build_planet(FSCMAP_TILEINFO_T *ti, UNIT_INFO_T *punit_info)
{
    int j, *pplanet_no;
    int act_val, best_val, best_planet;
    int numproj, map_pos;
    char best_proj;
    char projlist[22];


    best_proj   = 0;
    best_val    = 0;
    best_planet = 0;


    if(ti->star_planets[0] > 0)
    {
        /* Possible outposts that can be built */
        numproj = improve_get_by_type(projlist, PROJ_OUTPOST);

        pplanet_no = ti->star_planets;

        while(*pplanet_no > 0)
        {
            /* @TODO: Check the planets for desirability of outpost */
            if(unitmove_get_planetactions(punit_info) == ORDER_BUILD)
            {
                for(j = 0; j < numproj; j++)
                {
                    map_pos = fscmap_pos_fromxy(punit_info->pi.x, punit_info->pi.y, 0);
                    act_val = aiconstr_project_value(map_pos, *pplanet_no, projlist[j]);

                    if(act_val > best_val)
                    {
                        best_val    = act_val;
                        best_proj   = projlist[j];
                        best_planet = *pplanet_no ;
                    }
                }
            }

            pplanet_no++;
        }
    }

    return 0;

}

/* ========================================================================== */
/* ============================= PUBLIC PROCEDURE(S) ======================== */
/* ========================================================================== */

/*
 * Name:
 *     aiconstr_manage
 * Description:
 *     Manage a constructor, which has no orders
 * Input:
 *     punit_info *: Info about consructor-unit to manage
 */
int aiconstr_manage(UNIT_INFO_T *punit_info)
{
    FSCMAP_TILEINFO_T ti;
    int unit_no;


    fscmap_get_tileinfo(&punit_info->pi, &ti, FSCMAP_FINFO_STAR);

    if(ti.star_type >= 0)
    {
        if(ti.owner == punit_info->pi.owner)
        {
            /* It's already used by us, choose another one              */
            /* Leave this planetary system and go to expand the range   */
            if(aiconstr_goto_border(punit_info))
            {
                return 1;
            }
        }
        else
        {
            /* Build an outpost in this planetary system, if enough desire  */
            if(aiconstr_build_planet(&ti, punit_info))
            {
                return 1;
            }
        }
    }
    else
    {
        /* TODO: Extend outpost to starbase, if already an outpost at this position */
        /* Build the outpost, if at border */
        if(punit_info->numpos_reachable < 8)
        {
            unitmove_set_order(punit_info, ORDER_BUILD, 0, 0);
            return 1;
            /* TODO: Tell the 'goal()' what happened.   */
            /*        Could a goal be fullfilled ?      */
            /* goalDone(punit->owner, punit->pos, GOAL_EXTEND_RANGE, 0)    */
        }
        else
        {
            /* TODO: Goto towards first star off range */
            if(aiconstr_goto_border(punit_info))
            {
                return;
            }
        }
    }

    /* DEFAULT: Go, explore, until at we are at border of range */
    unitmove_set_order(unit_no, ORDER_EXPLORE, 0, 0);

    return 1;
}




