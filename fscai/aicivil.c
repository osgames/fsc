/* *****************************************************************************
*  AICIVIL.C                                                                   *
*      - AI handling of civil buildings and units: improvements                *
*                                                                              *
*  FREE SPACE COLONISATION                                                     *
*      (c)2002 Paul Mueller <pmtech@swissonline.ch>                            *
*                                                                              *
*  This program is free software; you can redistribute it and/or modify        *
*  it under the terms of the GNU General Public License as published by        *
*  the Free Software Foundation; either version 2 of the License, or           *
*  (at your option) any later version.                                         *
*                                                                              *
*  This program is distributed in the hope that it will be useful,             *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of              *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
*  GNU Library General Public License for more details.                        *
*                                                                              *
*  You should have received a copy of the GNU General Public License           *
*  along with this program; if not, write to the Free Software                 *
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  *
*******************************************************************************/

/* *****************************************************************************
* INCLUDES                                                                     *
*******************************************************************************/

#include "colony.h"


#include "aicivil.h"    /* Own header */

/* ****************************************************************************
* CODE                                                                        *
*******************************************************************************/



/* ========================================================================== */
/* ============================= PUBLIC PROCEDURE(S) ======================== */
/* ========================================================================== */

/*
 * Name:
 *     aicivilChooseBuild
 * Description:
 *     This function should fill the supplied GOAL structure. 
 *     If want is 0, this advisor doesn't want anything.
 * Input:
 *     nation_no:   Calc GOAL for this nation, if no colony is given
 *     colony_no:   Calc need of given GOAL for given colony
 *     goal *:      Pointer on GOAL to work on 
 * Output:
 *     goal -> want: > 0, Want to build something  
 */
int aicivilChooseBuild(char nation_no, int colony_no, GOAL *goal)
{

    goalInit(nation_no, GOAL_TYPE_COLONY, goal);

    if (colony_no > 0) {

        /* TODO: Choose building for this colony */

    }
    else {

        /* TODO: Choose general GOAL for given nation */ 

    }


    return goal -> want;

}