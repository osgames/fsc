/*******************************************************************************
*  AIDIPL.C                                                                    *
*	   - Code for AI-management of diplomacy                                   *
*                                                                              *
*   FREE SPACE COLONISATION             				                       *
*       Copyright (C) 2008  Paul Mueller <pmtech@swissonline.ch>               *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*******************************************************************************/

/*******************************************************************************
* INCLUDES                                                                     *
*******************************************************************************/

#include <stdlib.h> /* abs() */
#include <string.h> /* memset() */



#include "../code/colony.h"
#include "../code/fscdipl.h"
#include "../code/fscmap.h"
#include "../code/fsctool.h"
#include "../code/game.h"
#include "../code/msg.h"
#include "../code/player.h"
#include "../code/starsys.h"
#include "../code/tech.h"
#include "../code/unittype.h"
#include "../code/unit.h"



#include "aimap.h"
#include "aitool.h"


/* -- own header -- */
#include "aidipl.h"

/*******************************************************************************
* ENUMS                                                                        *
*******************************************************************************/

#define AIDIPL_NUM_ASK    8
#define AIDIPL_BIG_NUMBER ((int)100000)
#define TURNS_BEFORE_TARGET 15
#define MAX_AI_LOVE       1000
#define WAR_THRESHOLD     -(MAX_AI_LOVE / 8)

#define AIDIPL_MIN(x, y) (x < y ? x : y)
#define AIDIPL_MAX(x, y) (x > y ? x : y)

/*******************************************************************************
* ENUMS                                                                        *
*******************************************************************************/

enum
{
    WAR_REASON_NONE = 0,
    WAR_REASON_BEHAVIOUR,
    WAR_REASON_EXCUSE,
    WAR_REASON_HATRED,
    WAR_REASON_ALLIANCE
}
E_AIDIPL_WAR_REASON;

/*******************************************************************************
* TYPEDEFS                                                                     *
*******************************************************************************/

typedef struct
{
    /* Info about owner */
    char owner;		        /* Owner of this diplomacy info */
    char other;	            /* Info is about this player	*/
    /* Remember one example of each for text spam purposes.   */
    char is_allied_with_enemy;
    char at_war_with_ally;
    char is_allied_with_ally;

    char spam;       	    /* timer to avoid spamming a player with chat	*/
    int  distance;   	    /* average distance to that player's colonies 	*/
    int  countdown;  	    /* we're on a countdown to war declaration 	*/
    char war_reason; 	    /* why we declare war 				        */
    char ally_patience; 	/* we EXPECT our allies to help us!         */
    char asked_about[AIDIPL_NUM_ASK];    /* don't ask again FSCDIPL_PACT_...  */
    /* Additional info for AI */
    int love;		            /* The love we have for this player		      */
                                /* < 0: We hate this one                      */
    char is_dangerous;          /* This player is dangerous for us (value)    */
    /* the following are for "pacts" */
    char diplstate_type;		/* this player's disposition towards other    */
    char diplstate_max_state;	/* maximum treaty level ever had 	          */
    char first_contact_turn;    /* turn we had first contact with this player */
    char pact_turns_left;		/* until pact (e.g., cease-fire) ends         */
    char has_reason_to_cancel;  /* 0: no, 1: this turn, 2: this or next turn  */
    char contact_turns_left;	/* until contact ends                         */
    int  gold;                  /* Gold we know about                         */
    /* ------- Foreign policy relations --------- */
    short int tribute;          /* Tribute per round if income                */
                                /* < 0: owed > 0: Get from them               */
    short int sumtribute;       /* Sum of all tributes payed and got          */
    short int trade;            /* Trade per round with this partner          */
    short int sumtrade;         /* Total trade with this partner              */
    /* ------- How to feel about encounters --------- */
    char encountertime;         /* Months passed since last dipl encounter    */
    char encounterissue;        /* Last issue talked about                    */
    char hostilities;           /* Attacks risen by this player against us    */
    char share_vision;          /* Shares vison with this player              */

}
AIDIPL_INFO_T;

/*******************************************************************************
* DATA                                                                         *
*******************************************************************************/


static AIDIPL_INFO_T AiDiplInfo[100];
static TECH_INFO_T AiTechMng[2];              /* AI's tech management info me / other */

/*******************************************************************************
* CODE                                                                         *
*******************************************************************************/

/*
 * Name:
 *     aidiplMsgSend
 * Description:
 *     Fills in additional values for sending a message
 * Input:
 *      msg_num: Number of message to send
 *      msg *:   Buffer which holds the message
 */
static void aidiplMsgSend(int msg_num, MSG_INFO_T *msg)
{

    msg -> type = MSG_TYPE_DIPL;
    msg -> num  = msg_num;

}

/*
 * Name:
 *     aidiplCreateRelation
 * Description:
 *     Looks for an empty slot for new diplomacy info. Fills it with usable
 *     values.
  * Input:
 *      me :    This one generates the relations
 *      other:  With this one
 * Output:
 *	    Number of relation info, it could be generated
 */
 static int aidiplCreateRelation (char me, char other)
 {

    AIDIPL_INFO_T *pdipl;
    int i, j;


    for (i = 1; i < 99; i++) {

        if (AiDiplInfo[i].owner == 0) {	    /* We found an empty slot */

            pdipl = &AiDiplInfo[i];

            /* Fill in usable values */
            pdipl -> owner = me;
            pdipl -> other = other;
            pdipl -> spam = (char)(i % 5); /* pseudorandom */
            pdipl -> countdown = -1;
            pdipl -> war_reason = WAR_REASON_NONE;
            pdipl -> distance = 1;
            pdipl -> ally_patience = 0;
            pdipl -> love = 1;
            pdipl -> is_dangerous = 0;

            for (j = 0; j < AIDIPL_NUM_ASK; j++) {

                pdipl -> asked_about[j] = 0;

            }

        }  /* If relation could be generated */

    }

    return 0;       /* No space for relation */

}

/*
 * Name:
 *     aidiplGetInfoList
 * Description:
 *     Fills in given list with numbers of 'AIDIPL_INFO_T' in list for given
 *     player
 * Input:
 *      me :    This one wants to know the relations ...
 *	    list *: To fill with relation info
 * Output:
 *	    Number of relation info
 */
static int aidiplGetInfoList(char me, int *list)
{

    int count;
    int i;


    count = 0;
    (*list) = 0;
    for (i = 1; i < 99; i++) {

        if (AiDiplInfo[i].owner == me) {	/* We found a relation */

            (*list) = i;
            list++;
            count++;

        }

    }

    (*list) = 0;    /* Sign end of list */

    return count;

}

/*
 * Name:
 *     aidiplGetInfo
 * Description:
 *     Returns a pointer on a relation, if available. If not, it's a zeroed out buffer
 * Input:
 *      me : This one wants to know the relations ...
 *      other: To this one
 * Output:
 *	    Pointer on relation info
 */
static AIDIPL_INFO_T *aidiplGetInfo(char me, char other)
{

    int i;


    for (i = 1; i < 99; i++) {

        if (AiDiplInfo[i].owner == me && AiDiplInfo[i].other == other) {

            return &AiDiplInfo[i];

        }

    }

    return &AiDiplInfo[0];     /* Return  invalid */

}

/*
 * Name:
 *     aidiplDistance
 * Description:
 *     Calculates the average distance of both players involved in
 *     meeting, based on the colony-lists given in 'meet_info'
 * Input:
 *      meet_info *: To use for distance calculation
 * Output:
 *      distance
 */
static int aidiplDistance(FSCDIPL_INFO_T *meet_info)
{

    int *plist, *alist;
    int all_dist, one_dist;
    int pos1, pos2;

    /* Calculate from 'me' to 'you' */
    if (meet_info[1].num_planet == 0) {

        return AIDIPL_BIG_NUMBER;   /* We don't know any of his colonies */

    }

    if (meet_info[0].num_planet) {

        /* Catch possible error */
        return meet_info[1].num_planet * 2;

    }

    plist    = meet_info[0].planet_list;
    all_dist = 0;

    while(*plist > 0) {

        one_dist = 0;

        pos1  = starGet(starGetPlanet(*plist) -> orbits_star, 0) -> pos;

        alist = meet_info[1].planet_list;

        while (*alist) {

            pos2 = starGet(starGetPlanet(*alist) -> orbits_star, 0) -> pos;
            one_dist += sdlglmapGetDist(pos1, pos2);

            alist++;

        }

        one_dist /= meet_info[1].num_planet;
        plist++;

        all_dist += one_dist;

    }

    return (all_dist / meet_info[0].num_planet);

}

/*
 * Name:
 *     aidiplHasDiplState
 * Description:
 *     Returns a pointer on a relation, if available. If not, it's a zeroed out buffer
 * Input:
 *      me :        This one wants to know the relations ...
 *      other:      To this one
 *      dipl_state: Return this one
 * Output:
 *     Has this diplstate yes/no
 */
int aidiplHasDiplState(char me, char other, int dipl_state)
{

    AIDIPL_INFO_T *my_info;


    my_info = aidiplGetInfo(me, other);     /* Always returns a valid pointer */
    if (my_info -> diplstate_type == dipl_state) {

        return 1;

    }

    return 0;

}

/*
 * Name:
 *     aidiplGreed
 * Description:
 *     This is your typical human reaction. Convert lack of love into
 *     lust for gold.
 * Input:
 *     missing_love:
 * Output:
 *     greed: How much do my love cost ?
 */
static int aidiplGreed(int missing_love)
{

    if (missing_love > 0) {

        return 0;

    } else {
        /* Don't change the operation order here.
            * We do not want integer overflows */
        return -((missing_love * MAX_AI_LOVE) / 1000) *
                ((missing_love * MAX_AI_LOVE) / 1000) / 10;
    }

}

/*
 * Name:
 *     aidiplGoldequivTech (ai_goldequiv_tech)
 * Description:
 *     How much is a tech worth to player measured in gold
 * Input:
 *     mng *:   Pointer on technology management data
 *     me:      My number
 *     tech_no: This tech
 * Output:
 *     Value in gold
 */
static int aidiplGoldequivTech(TECH_INFO_T *mng, char me, int tech_no)
{

    int tech_want, worth;


    worth = techlistBulbsRequired(mng, tech_no);

    if (worth > 0) {

        /* Not yet known and reachable, it's worth something */
        tech_want = nationValue(me, YIELD_TECHWANT, tech_no);

        /* Division by 'bulbs_per_turn'. The more turns needed, the higher the value */
        worth /= nationValue(me, YIELD_RESEARCH, 0);
        worth *= 3;
        worth += tech_want;

    }

    return worth;

}

/*
 * Name:
 *     aidiplSharedVisionIsSafe
 * Description:
 *     Avoid giving from owner 'my_info' vision to non-allied player through
 *     'other' (shared vision is transitive).
 * Input:
 *     my_info *:  This is my info about the current other player
 */
static int aidiplSharedVisionIsSafe(AIDIPL_INFO_T *my_info)
{

    int info_list[50];  /* FIXME: Replace by PLAYER_MAX */
    int p_idx;


    if (my_info -> diplstate_type == FSCDIPL_PACT_TEAM) {

        /* We have teamed with this one, share vision */
        return 1;

    }

    /*** Look up all players, for which the other player has diplomatic info ***/
    aidiplGetInfoList(my_info -> other, &info_list[0]);
    p_idx  = 0;
    while (info_list[p_idx] > 0) {

        if (AiDiplInfo[info_list[p_idx]].diplstate_type < FSCDIPL_PACT_ALLIANCE) {

            return 0;

        }

        p_idx++;

    }

    return 1;

}

/*
 * Name:
 *     aidiplAiCanAgreeCeasefire
 * Description:
 *     Checks if owner of 'my_info' can agree on ceasefire with 'other'
 *     This function should only be used for ai players
 * Input:
 *     my_info *: Pointer on info to check
 */
static int aidiplAiCanAgreeCeasefire(AIDIPL_INFO_T *my_info)
{

    if (my_info -> love > - (MAX_AI_LOVE * 4 / 10) && my_info -> countdown == -1) {

        return 1;

    }

    return 0;

}

/*
 * Name:
 *     aidiplComputeTechSellPrice
 * Description:
 *     Calculate a price of a tech.
 *     Note that both AI players always evaluate the tech worth symetrically
 *     This eases tech exchange.
 *     'is_dangerous' returns if the giver is afraid of giving that tech
 *     (the taker should evaluate it normally, but giver should never give that)
 * Input:
 *     my_info *: Holder is giver, 'other' is taker
 *     tech_no:
 *     is_dangerous *:
 */
static int aidiplComputeTechSellPrice(AIDIPL_INFO_T *my_info, int tech_no, int* is_dangerous)
{

    NATION *pnation;
    TECH_INFO_T tech_mng;
    int worth;
    AIDIPL_INFO_T *other_info;
    int info_list[50];  /* TODO: Replace by PLAYER_MAX */
    int p_idx;


    pnation = nationGet(my_info -> other);

    techlistGetMngInfo(&tech_mng, pnation -> research.advancebits);

    worth = aidiplGoldequivTech(&tech_mng, my_info -> other, tech_no);

    *is_dangerous = 0;

    if (worth > 0) {

        /* if it has some worth at all for the taker */
        if (my_info -> diplstate_type == FSCDIPL_PACT_TEAM) {

            return 0;       /* Give it for free */

        }

        /* Share and expect being shared brotherly between allies */
        if (my_info -> diplstate_type == FSCDIPL_PACT_ALLIANCE) {

            worth /= 2;

        }

        /* Calculate in tech leak to our opponents, guess 50% chance for 'giver' */
        aidiplGetInfoList(my_info -> other, &info_list[0]);
        p_idx  = 0;
        while (info_list[p_idx] > 0) {

            other_info = &AiDiplInfo[info_list[p_idx]];

            /* Knows already this tech */
            /*
            if (! leaderGetValue(other -> info.other, VALUE_TECH, tech_no)) {


                continue;

            }
            */

            /* Don't risk it falling into enemy hands */
            if (other_info -> diplstate_type == FSCDIPL_PACT_ALLIANCE
                && leaderGetValue(my_info -> owner, VALUE_DANGER, other_info -> other)) {

                *is_dangerous = 1;

            }

            if (my_info -> diplstate_type == FSCDIPL_PACT_ALLIANCE
                && other_info -> diplstate_type < FSCDIPL_PACT_ALLIANCE) {

                /* Taker can enrichen his side with this tech */
                worth += aidiplGoldequivTech(&tech_mng, other_info -> other, tech_no) / 4;

            }

            p_idx++;

        }

    }

    return worth;

}

/*
 * Name:
 *     aidiplGoldequivClause (ai_goldequiv_clause)
 * Description:
 *     Evaluate gold worth of a single clause in a treaty. Note that it
 *     sometimes matter a great deal who is giving what to whom, and
 *     sometimes (such as with treaties) it does not matter at all.
 *     'ds_after' means a pact offered in the same treaty or current diplomatic
 *     state
 * Input:
 *     my_info:   Owner is AI to evaluate wort of a clause 'other' is the other one
 *     pclause *: Clause to evaluate
 *     meet_info *: Info about the meeting for me and the other
 *     ds_after:
 * Output:
 *     Value in gold for 'FSCDIPL_OFFER'
 */
static int aidiplGoldequivClause(AIDIPL_INFO_T *my_info, FSCDIPL_CLAUSE *pclause, FSCDIPL_INFO_T *meet_info, int ds_after)
{

    MSG_INFO_T msg;
    NATION *pnation;
    LEADER *pleader;
    int worth, give;
    int is_dangerous;
    char giver, owner;
    int turns;


    /* Initialize values */
    pnation = nationGet(my_info -> owner);
    pleader = leaderGet(my_info -> owner);

    worth = 0;  /* worth for 'my_info -> owner'     */
    give  = 0;  /* Assume it's offered              */
    if (pclause -> from == my_info -> owner) {

        give = 1;   /* We should give this */

    }

    ds_after = AIDIPL_MAX(ds_after, my_info -> diplstate_type);
    giver = pclause -> from;

    msgPrepare(my_info -> owner, my_info -> other, &msg);

    switch (pclause -> type) {

        case FSCDIPL_CLAUSE_TECH:
            worth = aidiplComputeTechSellPrice(my_info, pclause -> number, &is_dangerous);
            if (give) {

                worth = -worth;
                if (is_dangerous) {

                    return -AIDIPL_BIG_NUMBER;

                }

            }

            /* DIPLO_LOG(LOG_DIPL, pplayer, aplayer, "%s clause worth %d",
              advance_name_by_player(pplayer, pclause->value), worth); */
            break;

        case FSCDIPL_CLAUSE_PACT:
            /* This guy is allied to one of our enemies. Only accept
              * ceasefire. */
            if (my_info -> is_allied_with_enemy
                && pclause -> number != FSCDIPL_PACT_CEASEFIRE) {

                msg.other = my_info -> is_allied_with_enemy;
                aidiplMsgSend(1019, &msg);
                worth = -AIDIPL_BIG_NUMBER;
                break;

            }

            /* Steps of the treaty ladder */
            if (pclause -> number == FSCDIPL_PACT_PEACE) {

                if (my_info -> diplstate_type < FSCDIPL_PACT_CEASEFIRE) {

                    aidiplMsgSend(1020, &msg);
                    worth = -AIDIPL_BIG_NUMBER;

                } else if (my_info -> diplstate_type == FSCDIPL_PACT_CEASEFIRE
                           && my_info -> pact_turns_left > 4) {

                    aidiplMsgSend(1021, &msg);
                    worth = -AIDIPL_BIG_NUMBER;

                } else if (my_info -> countdown >= 0 || my_info -> countdown < -1) {

                    worth = -AIDIPL_BIG_NUMBER; /* but say nothing */

                } else {

                    worth = aidiplGreed(my_info -> love
                                        - pleader -> req_love_for[FSCDIPL_PACT_PEACE]);

                }

            } else if (pclause -> number == FSCDIPL_PACT_ALLIANCE) {

                if (my_info -> diplstate_type < FSCDIPL_PACT_PEACE) {

                    worth = aidiplGreed(my_info -> love
                                        - pleader -> req_love_for[FSCDIPL_PACT_PEACE]);

                }

                if (my_info -> countdown >= 0 || my_info -> countdown < -1) {

                    worth = -AIDIPL_BIG_NUMBER; /* but say nothing */

                } else {

                    worth += aidiplGreed(my_info -> love
                                         - pleader -> req_love_for[FSCDIPL_PACT_ALLIANCE]);

                }

                if (my_info -> love < MAX_AI_LOVE / 10) {

                    aidiplMsgSend(1022, &msg);
                    worth = -AIDIPL_BIG_NUMBER;

                }
                /* DIPLO_LOG(LOG_DIPL, pplayer, aplayer, "ally clause worth %d", worth); */

            } else {

                if (pnation -> ai_control && aidiplAiCanAgreeCeasefire(my_info)) {

                    worth = 0;

                } else {

                    turns = gameGetTurn();
                    turns -= my_info -> first_contact_turn;

                    if (turns < TURNS_BEFORE_TARGET) {

                        worth = 0; /* show some good faith */
                        break;

                    } else {

                    worth = aidiplGreed(my_info -> love);
                    /*
                    DIPLO_LOG(LOG_DIPL, pplayer, aplayer, "ceasefire worth=%d love=%d "
                    "turns=%d", worth, pplayer->ai.love[player_index(aplayer)],
                    turns);
                    */

                    }

                }

            }

            /* Let's all hold hands in one happy family! */
            if (my_info -> is_allied_with_ally) {

                worth /= 2;
                break;

            }

            /* DIPLO_LOG(LOG_DIPL, pplayer, aplayer, "treaty clause worth %d", worth); */
            break;

        case FSCDIPL_CLAUSE_GOLD:
            if (give) {

                worth -= pclause -> number;

            } else {

                worth += pclause -> number;

            }
            break;

        case FSCDIPL_CLAUSE_MAP:
            if (give) {

                /* Reasoning is he has more use of map for settling
                   new areas the more colonies he has already. */
                worth -= meet_info[1].num_colony * 15;
                /* map more worth the more colonies
                   we have, since we expose all of these to the enemy. */
                worth -= meet_info[0].num_colony * 40;
                /* FIXME: Make maps from novice player cheap ==>  worth /= 2; */
                /* Don't like him? Don't give him! */
                worth -= AIDIPL_MIN(-my_info -> love * 7, worth);
                /* Inflate numbers if not peace */
                if (my_info -> diplstate_type < FSCDIPL_PACT_PEACE) {

                    worth *= 2;

                }

            }
            else {

                worth = meet_info[0].num_colony * 15;
                worth += meet_info[1].num_colony * 40;

            }

            /*
            DIPLO_LOG(LOG_DIPL, pplayer, aplayer, "map clause worth %d",
              worth); */
            break;

        case FSCDIPL_CLAUSE_COLONY:

            worth = colonyGoldValue(pclause -> number, &owner, 1);
            /* Above function takes the 'count_capital' into account */
            if (give) {

                /* AI must be crazy to trade away its colonies */
                worth = -worth;
                worth *= 15;

            }
            else {

                /* Let them buy back their own colony cheaper. */
                if (owner == pclause -> to) {

                    worth /= 2;

                }

            }
            break;

        case FSCDIPL_CLAUSE_VISION:
            if (give) {

                if (ds_after == FSCDIPL_PACT_ALLIANCE) {

                    if (! aidiplSharedVisionIsSafe(my_info)) {

                        aidiplMsgSend(1023, &msg);
                        worth = -AIDIPL_BIG_NUMBER;

                    } else {

                        worth = 0;

                    }

                } else {

                    worth -= meet_info[1].num_colony * 30;
                    worth -= meet_info[1].num_unit * 40;

                }

            } else {

                worth = meet_info[0].num_colony * 30;
                /* The more units he has, the more worth */
                worth = meet_info[1].num_unit * 40;

            }
            break;

        case FSCDIPL_CLAUSE_EMBASSY:
            if (give) {

                if (ds_after == FSCDIPL_PACT_ALLIANCE) {
                    worth = 0;
                } else if (ds_after == FSCDIPL_PACT_PEACE) {
                    worth = -5 * gameGetTurn();
                } else {
                    worth = AIDIPL_MIN(-50 * gameGetTurn()
                            + my_info -> love,
                            -5 * gameGetTurn());
                }

            } else {

                worth = 5 * gameGetTurn();

            }
            /* DIPLO_LOG(LOG_DIPL, pplayer, aplayer, "embassy clause worth %d",
              worth); */
            break;

        case FSCDIPL_CLAUSE_UNIT:
            /* FIXME: Insert proper code */
            worth = 0;

    } /* end of switch */

    return worth;

}

/*
 * Name:
 *     aidiplTreatyEvaluate
 * Description:
 *     Owner of 'my_info' is AI-Player, 'other' is the other player involved,

 *     'clause_list' is the list of clauses considered.
 *     It is all a question about money :-)
 * Input:
 *     my_info *:     Info of Ai about other player (dipl-state)
 *     meet_info *:   Info about this meeting
 *     clause_list *: List of clauses to evaluate
 */
void aidiplTreatyEvaluate(AIDIPL_INFO_T *my_info, FSCDIPL_INFO_T *meet_info, FSCDIPL_CLAUSE *clause_list)
{

    MSG_INFO_T msg;
    int total_balance, only_gifts, ds_after, given_colonies;
    FSCDIPL_CLAUSE *pclause;


    /* Initialize values */
    total_balance = 0;
    only_gifts = 1;
    given_colonies = 0;
    ds_after = my_info -> diplstate_type;

    /* Prepare evaluation */
    pclause = clause_list;
    while (pclause -> type > 0) {

        /* As long as we have clauses */
        if (pclause -> type == FSCDIPL_CLAUSE_PACT) {

            ds_after = pclause -> number;

        }
        else if (pclause -> type == FSCDIPL_CLAUSE_COLONY) {

            given_colonies++;

        }

        pclause++;

    }

    /* Evaluate clauses */
    pclause = clause_list;
    while (pclause -> type > 0) {

        total_balance += aidiplGoldequivClause(my_info, pclause, &meet_info[0], ds_after);

        switch(pclause -> type) {

            case FSCDIPL_CLAUSE_PACT:
            case FSCDIPL_CLAUSE_GOLD:
            case FSCDIPL_CLAUSE_MAP:
            case FSCDIPL_CLAUSE_VISION:
            case FSCDIPL_CLAUSE_EMBASSY:
                /* We accept the above list of clauses as gifts, even if we are
                   * at war. We do not accept tech or cities since these can be used
                   * against us, unless we know that we want this tech anyway, or
                   * it doesn't matter due to tech cost style. */
                only_gifts = 1;
                break;
             case FSCDIPL_CLAUSE_TECH:
                if ( /* game.info.tech_cost_style != 0
                    || */
                    pclause -> number == leaderGetValue(my_info -> owner, VALUE_TECHGAOL, 0)
                    || pclause -> number == leaderGetValue(my_info -> owner, VALUE_TECHRESEARCHING, 0)) {

                    only_gifts = 1;

                }
                else {

                    only_gifts = 0;

                }
                break;

             default:
                only_gifts = 0;
                break;
        }

    }

    /* If we are at war, and no peace is offered, then no deal, unless
       * it is just gifts, in which case we gratefully accept. */
    if (ds_after == FSCDIPL_PACT_WAR && ! only_gifts) {
        /* DIPLO_LOG(LOG_DIPL2, pplayer, aplayer, "no peace offered, must refuse"); */
        return;

    }

    if (given_colonies > 0
        && meet_info[0].num_colony - given_colonies <= 2) {
        /* always keep at least two cities */
        /* DIPLO_LOG(LOG_DIPL2, pplayer, aplayer, "cannot give last cities"); */
        return;

    }

    /* Accept if balance is good */
    /* TODO: Accept bad balance, if other player is novice and human... */
    /* Normalize balance to a value from 1..100 for this kind of acceptance */
    if (total_balance >= 0) {

        /* handle_diplomacy_accept_treaty_req(pplayer, player_number(aplayer)); */
        /* DIPLO_LOG(LOG_DIPL2, pplayer, aplayer, "balance was good: %d",
              total_balance); */
        /* FIXME: Send message, that treaty was accepted and add treatys to list
                  of both players */

    } else {

        /* TODO: Send message, how bad the balance is as normalized value from 1..100 */
        /* msg.value1 = ???? */
        msgPrepare(my_info -> owner, my_info -> other, &msg);
        aidiplMsgSend(1024, &msg);
        /*
         DIPLO_LOG(LOG_DIPL2, pplayer, aplayer, "balance was bad: %d",
              total_balance);
        */


    }

}

/*
 * Name:
 *     aidiplTreatyReact (ai_treaty_react)
 * Description:
 *     Comments to player from AI on clauses being agreed on. Does not
 *     alter any state.
 * Input:
 *     my_info *:
 *     pclause *: Pointer on clause to comment
 */
static void aidiplTreatyReact(AIDIPL_INFO_T *my_info, FSCDIPL_CLAUSE *pclause)
{

    MSG_INFO_T msg;


    msgPrepare(my_info -> owner, my_info -> other, &msg);

    if (pclause -> type == FSCDIPL_CLAUSE_PACT) {

        switch(pclause -> number) {

            case FSCDIPL_PACT_ALLIANCE:
                if (my_info -> is_allied_with_ally) {

                    aidiplMsgSend(1025, &msg);

                } else {

                    aidiplMsgSend(1026, &msg);
                }
                /* DIPLO_LOG(LOG_DIPL, pplayer, aplayer, "become allies"); */
                break;

            case FSCDIPL_PACT_PEACE:
                aidiplMsgSend(1027, &msg);
                break;

            case FSCDIPL_PACT_CEASEFIRE:
                aidiplMsgSend(1028, &msg);
                /* DIPLO_LOG(LOG_DIPL, pplayer, aplayer, "sign ceasefire"); */
                break;
            default:
                  break;
        }

    }

}


/*
 * Name:
 *     aidiplAiTreatyAccepted (ai_treaty_accepted)
 * Description:
 *     This function is called when a treaty has been concluded, to deal
 *     with followup issues like comments and relationship (love) changes.
 *
 *     'my_info -> owner' is AI player, 'my_info -> other' is the other player
 *     involved, 'clause_list' is the list of clauses accepted.
 * Input:
 *      my_info *:
 *      meet_info *: Info about our meeting
 *      clause_list *:
 */
void aidiplAiTreatyAccepted(AIDIPL_INFO_T *my_info, FSCDIPL_INFO_T *meet_info, FSCDIPL_CLAUSE *clause_list)
{

    int total_balance, gift, ds_after, balance, i;
    FSCDIPL_CLAUSE *pclause;
    LEADER *pleader;


    /* Initialize the values */
    total_balance = 0;
    gift = 1;
    ds_after = my_info -> diplstate_type;


    pclause = clause_list;
    while (pclause -> type > 0) {

        if (pclause -> type == FSCDIPL_CLAUSE_PACT) {

            ds_after = pclause -> number;

        }

        pclause++;

    }

    pleader = leaderGet(my_info -> owner);

    /* Evaluate clauses */
    pclause = clause_list;
    while (pclause -> type > 0) {

        balance = aidiplGoldequivClause(my_info, pclause, &meet_info[0], ds_after);
        total_balance += balance;
        gift = (gift && (balance >= 0));

        aidiplTreatyReact(my_info, pclause);
        if (pclause -> type == FSCDIPL_CLAUSE_PACT && my_info -> countdown != -1) {

            /* Cancel a countdown towards war if we just agreed to peace... */
            my_info -> countdown = -1;

        }

        pclause++;

    }


    /* Rather arbitrary algorithm to increase our love for a player if
       * he or she offers us gifts. It is only a gift if _all_ the clauses
       * are beneficial to us. */
    if (total_balance > 0 && gift) {

        i = total_balance / ((meet_info[0].num_colony * 10) + 1);

        i = AIDIPL_MIN(i, (pleader -> love_incr * 150) * 10);
        my_info -> love += i;
        /* DIPLO_LOG(LOG_DIPL2, pplayer, aplayer, "gift increased love by %d", i); */

    }

}

/*
 * Name:
 *     aidiplWarDesire (ai_war_desire)
 * Description:
 *     Calculate our desire to go to war against 'my_info -> other'. We want to
 *     attack a player that is easy to beat and will yield a nice profit.
 *
 *     This function is full of hardcoded constants by necessity.  They are
 *     not #defines since they are not used anywhere else.
 * Input:
 *     my_info *:   Info of owner about the 'other' player
 *     meet_info *: Info about the meeting
 *     dipl_info *:
 */
static int aidiplWarDesire(AIDIPL_INFO_T *my_info, FSCDIPL_INFO_T *meet_info, AIDIPL_INFO_T *dipl_info)
{

    static int off_values[8] = { 1, 10, 1, 7, 5, 3 };

    int want, fear, distance, colony_ships, colonies;
    int *plist;
    char owner;
    UNIT *punit;


    /* Initialise the values */
    want = 0;
    fear = 0;
    distance = 0;
    colony_ships = 0;


    plist = &meet_info[1].colony_list[0];
    while (*plist > 0) {

        want += 100; /* base colony want */
        want += colonyGoldValue(*plist, &owner,0);

        /* FIXME: This might be buggy if it ignores unmet UnitClass reqs. */
        fear += colonyBonus(*plist, YIELD_DEFENSE, 50);

        plist++;

    }

    plist = &meet_info[1].unit_list[0];
    while (*plist > 0) {

        /* TODO: Use 'aimap' for this calculation ? */
        punit = unitGet(*plist);
        fear += punit -> attack;

        /* Fear enemy expansionism */
        if (punit -> ability == UNIT_ABILITY_COLONIZE) {

            want += 100;

        }

        plist++;

    }

    plist = &meet_info[0].unit_list[0];
    while (*plist > 0) {

        /* TODO: Use 'aimap' for this calculation */
        fear -= (unitGet(*plist) -> attack / 2);

        /* Our own expansionism reduces our want for war */
        if (punit -> ability == UNIT_ABILITY_COLONIZE) {

            want -= 200;
            colony_ships++;

        }

        plist++;

    }

    colonies = meet_info[0].num_colony;
    plist = &meet_info[0].colony_list[0];
    while (*plist > 0) {

        /* If our colony is producing a colony-ship, count it as such */
        if (colonyProduction(*plist, COLONY_PROJ_MILITARY, UNIT_ABILITY_COLONIZE)) {

            want -= 150;
            colony_ships++;

        }

        plist++;

    }

    /* Modify by colony-ship/colony ratio to prevent early wars when
      * we should be expanding. This will eliminate want if we
      * produce colony-ships in all colonies (ie full expansion). */
    want -= abs(want) / AIDIPL_MAX(colonies - colony_ships, 1);

    /* Calculate average distances to other player's empire. */
    my_info -> distance = aidiplDistance(meet_info);

    /* Worry a bit if the other player has extreme amounts of wealth
      * that can be used in cities to quickly buy an army. */
    fear += (my_info -> gold / 5000) * meet_info[1].num_colony;

    /* Tech lead is worrisome. FIXME: Only consider 'military' techs. */
    fear += AIDIPL_MAX(meet_info[1].num_tech - meet_info[0].num_tech, 0) * 100;

    /* Modify by which treaties we would break to other players, and what
      * excuses we have to do so. FIXME: We only consider immediate
      * allies, but we might trigger a wider chain reaction. */
    while (dipl_info -> owner > 0) {

        if (my_info -> other == dipl_info -> other) {

            continue;

        }

        if (! dipl_info -> has_reason_to_cancel
              && aidiplHasDiplState(my_info -> other, dipl_info -> other, FSCDIPL_PACT_ALLIANCE)) {

            /* N/A, 10%, N/A, 15%, 20%, 33% off */
            if (off_values[dipl_info -> diplstate_type] > 1) {

                want -= abs(want) / off_values[dipl_info -> diplstate_type];

            }

        }

        dipl_info++;

    }

    /* Modify by love. Increase the divisor to make ai go to war earlier */
    want -= AIDIPL_MAX(0, want * my_info -> love / (2 * MAX_AI_LOVE));

    /* FIXME: Make novice AI more peaceful with human players */
    /*
    if (ai_handicap(pplayer, H_DIPLOMACY) && !target->ai.control) {
        want /= 2;
    }
    */

    /* Amortize by distance */
    want = aitoolAmortize(want, distance);

    if (my_info -> diplstate_type >= FSCDIPL_PACT_ALLIANCE) {

        want /= 4;

    }

    /*
    DIPLO_LOG(LOG_DEBUG, pplayer, target, "War want %d, war fear %d",
            want, fear);
    */
    return (want - fear);

}

/*
 * Name:
 *     aidiplAiDiplomacySuggest (ai_diplomacy_suggest)
 * Description:
 *     Suggest a treaty from 'my_info -> owner' to 'my_info -> other'
 * Input:
 *     my_info *: Info of owner about the 'other' player
 */
static void aidiplAiDiplomacySuggest(AIDIPL_INFO_T *my_info)
{
    /*
    if (!could_meet_with_player(pplayer, aplayer)) {
        freelog(LOG_DIPL2, "%s tries to do diplomacy to %s without contact",
            player_name(pplayer),
            player_name(aplayer));
        return;
    }
    */

    /*
     handle_diplomacy_init_meeting_req(pplayer, player_number(aplayer));
     handle_diplomacy_create_clause_req(pplayer, player_number(aplayer),
				     player_number(pplayer), what, value);
    */
    /* FIXME: Add this clause to list */

}

/*
 * Name:
 *     aidiplAIDiplomacyFirstContact (ai_diplomacy_first_contact)
 * Description:
 *     What to do when we first meet. pplayer is the AI player.
 * Input:
 *     my_info *: Info of owner about the 'other' player
 */
void aidiplAIDiplomacyFirstContact(AIDIPL_INFO_T *my_info)
{

    /*
    if (pplayer->ai.control && !ai_handicap(pplayer, H_AWAY)) {
        notify(aplayer, _("*%s (AI)* Greetings %s! May we suggest a ceasefire "
           "while we get to know each other better?"),
           player_name(pplayer),
           player_name(aplayer));
    ai_diplomacy_suggest(pplayer, aplayer, CLAUSE_CEASEFIRE, 0);

    }
    */
    /* FIXME: Replace by proper Code */
    /* FIXME: First create diplomacy info for both players */

}

/*
 * Name:
 *     aidiplAIDiplomacyBeginNewPhase (ai_diplomacy_begin_new_phase)
 * Description:
 *     Calculate our diplomatic predispositions here. Don't do anything.
 *
 *     Only ever called for AI players and never for barbarians.
 *
 *     This is called at the start of a new AI phase.  It's not called when
 *     a game is loaded.  So everything calculated here should be put into
 *     the savegame.
 * Input:
 *     my_info *: List of all players we have diplomatic knowledge about
 */
void aidiplAIDiplomacyBeginNewPhase(AIDIPL_INFO_T *my_info)
{

    FSCDIPL_INFO_T Meet_Info[2];
    int war_desire;
    int best_desire, love_incr;
    char best_target;
    int amount, reduction;
    AIDIPL_INFO_T *dipl_info;
    LEADER *pleader;


    /* Inititialize values */
    pleader = leaderGet(my_info -> owner);

    best_desire = 0;
    best_target = 0;

    /* Calculate our desires, and find desired war target */
    dipl_info = my_info;
    while(dipl_info -> owner > 0) {

        /* Check all our relations */
        /* We don't hate  team members. */
        if (my_info -> diplstate_type == FSCDIPL_PACT_TEAM) {

            continue;

        }

        fscdiplInitMeeting(Meet_Info, my_info -> owner, my_info -> other);

        war_desire = aidiplWarDesire(my_info, Meet_Info, dipl_info);
        if (war_desire > best_desire) {

            best_desire = war_desire;
            best_target = my_info -> other;

        }

        dipl_info++;

    }

    /* Time to make love. If we've been wronged, hold off that love
     * for a while. Also, cool our head each turn with love_coeff. */
    dipl_info = my_info;
    love_incr = pleader -> love_incr;

    while(dipl_info -> owner > 0) {

        amount = 0;

        if ( (/* (pplayers_non_attack(pplayer, aplayer)
             || */ dipl_info -> diplstate_type == FSCDIPL_PACT_ALLIANCE)
             && dipl_info -> has_reason_to_cancel == 0
             && dipl_info -> countdown == -1
             && !dipl_info -> is_allied_with_enemy
             && !dipl_info -> at_war_with_ally
             && dipl_info -> other != best_target
             && dipl_info -> ally_patience >= 0) {

            amount += love_incr / 2;
            if (dipl_info -> diplstate_type == FSCDIPL_PACT_ALLIANCE) {
                amount += love_incr / 3;
            }
            /* Increase love by each enemy he is at war with */
            /* FIXME: Insert correct code
            players_iterate(eplayer) {
                if (WAR(eplayer, aplayer) && WAR(pplayer, eplayer)) {
                    amount += love_incr / 4;
                }

            } players_iterate_end; */
            dipl_info -> love += amount;
            /* DIPLO_LOG(LOG_DEBUG, pplayer, aplayer, "Increased love by %d", amount); */

        } else if (dipl_info -> diplstate_type == FSCDIPL_PACT_WAR) {

            amount -= love_incr / 2;
            dipl_info -> love += amount;
            /* DIPLO_LOG(LOG_DEBUG, pplayer, aplayer, "%d love lost to war", amount); */

        } else if (dipl_info -> has_reason_to_cancel != 0) {
            /* Provoked in time of peace */
            if (dipl_info -> love > 0) {
                amount -= dipl_info -> love / 2;
            }
            amount -= love_incr * 6;
            dipl_info -> love += amount;
            /* DIPLO_LOG(LOG_DEBUG, pplayer, aplayer, "Provoked! %d love lost!",
                amount); */
        }

        if (dipl_info -> love > MAX_AI_LOVE * 8 / 10
            && dipl_info -> diplstate_type < FSCDIPL_PACT_ALLIANCE) {

            amount = love_incr / 3;

            /* Upper levels of AI trust and love is reserved for allies. */
            dipl_info -> love -= amount;
            /* DIPLO_LOG(LOG_DEBUG, pplayer, aplayer, "%d love lost from excess",
                amount); */

        }

        amount = 0;

        /* Reduce love due to units in our territory.
          * AI is so naive, that we have to count it even if players are allied */
        amount -= AIDIPL_MIN(aimapUnitInTerritory(dipl_info -> owner, dipl_info -> other) * (MAX_AI_LOVE / 200),
                      love_incr
                      * ((dipl_info -> is_allied_with_enemy != 0) + 1));
        dipl_info -> love += amount;
        if (amount != 0) {
            /* DIPLO_LOG(LOG_DEBUG, pplayer, aplayer, "%d love lost due to units inside "
                "our borders", amount); */
        }

        /* Increase the love if aplayer has got a building that makes
         * us love him more. Typically it's Eiffel Tower */

        dipl_info -> love += nationGetBonus(dipl_info -> other, YIELD_PRESTIGE) * MAX_AI_LOVE / 1000;

        dipl_info++;

    }

    dipl_info = my_info;
    while(dipl_info -> owner > 0) {

        if (dipl_info -> other == best_target && best_desire > 0) {

            reduction = AIDIPL_MIN(best_desire, MAX_AI_LOVE / 20);

            dipl_info -> love -= reduction;
            /* DIPLO_LOG(LOG_DEBUG, pplayer, aplayer, "Wants war, reducing "
                "love by %d ", reduction); */
        }

        /* Massage our numbers to keep love and its opposite on the ground.
         * Gravitate towards zero. */
        dipl_info -> love -= dipl_info -> love * (pleader -> love_coeff / 100);

        /* ai love should always be in range [-MAX_AI_LOVE..MAX_AI_LOVE] */
        dipl_info -> love = AIDIPL_MAX(-MAX_AI_LOVE, AIDIPL_MIN(MAX_AI_LOVE, dipl_info -> love));

        dipl_info++;

    }

}

/*
 * Name:
 *     aidiplSuggestTechExchange (suggest_tech_exchange)
 * Description:
 *     Find techs that can be exchanged and suggest that
 * Input:
 *     my_info *: My Info about the other
 *     meet_info *: Info about meeting
 */
static void aidiplSuggestTechExchange(AIDIPL_INFO_T *my_info, FSCDIPL_INFO_T *meet_info)
{

    int is_dangerous, i, tech, tech_worth, diff;
    int worth_mine, worth_his, suggest_mine, suggest_his;
    AIDIPL_INFO_T *his_info;


    /* Techs we can offer */
    worth_mine = 0;
    worth_his  = 0;
    suggest_mine = 0;
    suggest_his = 0;

    for (i = 0; i < meet_info -> num_tech; i++) {

        tech = meet_info -> tech_offerlist[i];
        tech_worth = aidiplComputeTechSellPrice(my_info, tech, &is_dangerous);

        if (is_dangerous) {
            /* don't try to exchange */
            tech_worth = 0;

        }

        if (tech_worth > worth_mine) {

            worth_mine = tech_worth;
            suggest_mine = tech;

        }

    }

    /* Techs he can offer */
    meet_info++;
    his_info = aidiplGetInfo(my_info -> other, my_info -> owner);
    for (i = 0; i < meet_info -> num_tech; i++) {

        tech = meet_info -> tech_offerlist[i];
        tech_worth = aidiplComputeTechSellPrice(his_info, tech, &is_dangerous);

        if (is_dangerous) {
            /* don't try to exchange */
            tech_worth = 0;
        }

        if (tech_worth > worth_mine) {

            worth_his   = tech_worth;
            suggest_his = tech;

        }

    }

    if (worth_mine > 0 && worth_his > 0) {

        /* Only exchange techs */ /* FIXME: Offer as gift, if we have no money !? */
        fscdiplAddClause(my_info -> owner, my_info -> other, FSCDIPL_CLAUSE_TECH, suggest_mine, 0);
        fscdiplAddClause(my_info -> other, my_info -> owner, FSCDIPL_CLAUSE_TECH, suggest_his, 0);

        /* And now offer/ask the difference in gold */
        diff = worth_mine - worth_his;
        if (diff > 0) {
            fscdiplAddClause(my_info -> other, my_info -> owner, FSCDIPL_CLAUSE_GOLD, diff, 0);
        }
        else if (diff < 0) {
            fscdiplAddClause(my_info -> owner, my_info -> other, FSCDIPL_CLAUSE_GOLD, -diff, 0);
        }

    }

}

/*
 * Name:
 *     aidiplAiShare
 * Description:
 *     Offer techs and stuff to other player and ask for techs we need.
 * Input:
 *     my_info *:   My Info about the other
 *     meet_info *: Info about meeting
 */
static void aidiplAiShare(AIDIPL_INFO_T *my_info, FSCDIPL_INFO_T *meet_info)
{

    NATION *pnation, *onation;
    int i;
    int *tech_list;


    /* Only share techs with team mates */
    /* FIXME: Set maximum tech to 5 per meeting ?! */
    if (my_info -> diplstate_type == FSCDIPL_PACT_TEAM) {

        tech_list = &meet_info -> tech_offerlist[0];
        for (i = 0; i < meet_info -> num_tech; i++) {

            fscdiplAddClause(my_info -> owner, my_info -> other, FSCDIPL_CLAUSE_TECH, tech_list[i], 0);

        }

        tech_list = &meet_info[1].tech_offerlist[0];
        for (i = 0; i < meet_info[1].num_tech; i++) {

            fscdiplAddClause(my_info -> owner, my_info -> other, FSCDIPL_CLAUSE_TECH, tech_list[i], 0);

        }

    }

    pnation = nationGet(my_info -> owner);
    onation = nationGet(my_info -> other);
    /* Only give shared vision if safe. Only ask for shared vision if fair. */
    if (! my_info -> share_vision && aidiplSharedVisionIsSafe(my_info)) {
        /* Shared vision counts allways on both sides */
        fscdiplAddClause(my_info -> owner, my_info -> other, FSCDIPL_CLAUSE_VISION, 0, 0);

    }

    if (! LEADER_DIPL_HASEMBASSY(pnation -> dipl[my_info -> other])) {
        fscdiplAddClause(my_info -> other, my_info -> owner, FSCDIPL_CLAUSE_EMBASSY, 0, 0);
    }

    if (! LEADER_DIPL_HASEMBASSY(onation -> dipl[my_info -> owner])) {
        fscdiplAddClause(my_info -> owner, my_info -> other, FSCDIPL_CLAUSE_EMBASSY, 0, 0);
    }

    /* FIXME: Add handicap, based on hardness of game
    if (!ai_handicap(my_info -> owner, H_DIPLOMACY) || !aplayer->ai.control) {
        aidiplSuggestTechExchange(my_info -> owner, my_info -> other);
    }
    */

}

/*
 * Name:
 *     aidiplGoToWar (ai_go_to_war)
 * Description:
 *     Go to war.  Explain to target why we did it, and set countdown to
 *     some negative value to make us a bit stubborn to avoid immediate
 *     reversal to ceasefire.
 * Input:
 *     my_info *: My Info about the other
 *     reason:    Why do we go to war ?
 */
static void aidiplGoToWar(AIDIPL_INFO_T *my_info, int reason)
{

    MSG_INFO_T msg;
    int msg_num;


    msg_num = 0;
    msgPrepare(my_info -> owner, my_info -> other , &msg);
    switch (reason) {

        case WAR_REASON_BEHAVIOUR:
            msg_num = 1007;
            my_info -> countdown = -20;
            break;

        case WAR_REASON_NONE:
            msg_num = 1008;
            my_info -> countdown = -10;
            break;

        case WAR_REASON_HATRED:
            msg_num = 1009;
            /*
            notify(target, _(""),
            player_name(pplayer));
            */
            my_info -> countdown = -20;
            break;
        case WAR_REASON_EXCUSE:
            msg_num = 1010;
            my_info -> countdown = -20;
            break;
        case WAR_REASON_ALLIANCE:
            if (my_info -> at_war_with_ally) {
                msg_num = 1011;
                msg.ally = my_info -> at_war_with_ally;
                my_info -> countdown = -3;
            } else {
                /* Ooops! */
                /*
                DIPLO_LOG(LOG_DIPL, pplayer, target, "Wanted to declare war "
                "for his war against an ally, but can no longer find "
                "this ally!  War declaration aborted.");
                */
                my_info -> countdown = -1;
                return;
            }
            break;
    }

    if (msg_num > 0) {

        aidiplMsgSend(msg_num, &msg);

    }

    if (my_info -> share_vision) {
        /* FIXME: Write proper code */
        /*
        remove_shared_vision(my_info -> owner, my_info -> other);
        */
        my_info -> share_vision = 0;

    }

    /* Check for Senate obstruction.  If so, dissolve it. */
    /* FIXME: Replace by proper code
    if (pplayer_can_cancel_treaty(my_info -> owner, my_info -> other) == DIPL_SENATE_BLOCKING) {
         handle_player_change_government(pplayer,
                                        game.info.government_during_revolution_id);

    }
    */

    /* This will take us straight to war. */
    /* Cancel all treaties */
    fscdiplCancelTreaty(my_info -> owner, my_info -> other, 0);
    my_info -> diplstate_type = FSCDIPL_PACT_WAR;

    /* Throw a tantrum */
    if (my_info -> love > 0) {
        my_info -> love = -1;
    }

    my_info -> love -= MAX_AI_LOVE / 8;

    /* DIPLO_LOG(LOG_DIPL, pplayer, target, "war declared"); */
}

/*
 * Name:
 *     aidiplWarCountdown
 * Description:
 *     Do diplomatic actions. Must be called only after calculate function
 *     above has been run for _all_ AI players.
 *
 *     Only ever called for AI players and never for barbarians.
 *
 *     When the 'target -> countdown' variable is set to a positive value, it
 *     counts down to a declaration of war. When it is set to a value
 *     smaller than -1, it is a countup towards a "neutral" stance of -1,
 *     in which time the AI will refuse to make treaties. This is to make
 *     the AI more stubborn.
 * Input:
 *	    target *:    Info about player to going to war at
 *	    countdown:
 *	    war_reason: why ?
 *      info_list *: List of all our diplomatic relations
 * Output:
 *	    None
 */
void aidiplWarCountdown(AIDIPL_INFO_T *target, int countdown, int war_reason,  int *info_list)
{

    MSG_INFO_T msg;
    AIDIPL_INFO_T *ally_info;
    int msg_number;
    int p_idx;


    if (target -> countdown == -1) {

        /* Otherwise we're resetting an existing countdown, which is very bad */
        /* TODO: Write message to log */
        return;

    }

    target -> countdown  = countdown;
    target -> war_reason = (char)war_reason;

    /* ----------------- inform our allies about out action ----------- */
    msgPrepare(target -> owner, target -> other, &msg);
    p_idx = 0;

    if (countdown < 2) {
        countdown++;    /* For message  */
    }

    msg.value1 = countdown;

    while(info_list[p_idx] > 0) {

        ally_info = &AiDiplInfo[info_list[p_idx]];

        if (ally_info -> diplstate_type == FSCDIPL_PACT_ALLIANCE
            && ally_info -> other != target -> other) {

            /* Send info to our allies that we are going to war */
            /* The message depends on 'war reason' 		        */
            switch (war_reason) {

                case WAR_REASON_BEHAVIOUR:
                case WAR_REASON_EXCUSE:
                    msg_number = 1001;
                    break;
                case WAR_REASON_HATRED:
                    msg_number = 1002;
                    break;
                case WAR_REASON_ALLIANCE:
                    if (aidiplHasDiplState(ally_info -> other, target -> other, FSCDIPL_PACT_WAR)) {
                        msg_number = 1003;
                    }
                    else if (target -> at_war_with_ally) {
                        msg.ally = target -> at_war_with_ally;
                        msg_number = 1004;
                    }
                    break;
                case WAR_REASON_NONE:
                default:
                    aidiplMsgSend(1005, &msg);
                    msg_number = 1006;
	             break;

            } /* switch(reason) */

            aidiplMsgSend(msg_number, &msg);

        }

        p_idx++;

    }

}

/* ========================================================================== */
/* ======================== PUBLIC FUNCTION(S) ============================== */
/* ========================================================================== */

/*
 * Name:
 *     aidiplStart
 * Description:
 *     Fill in the struct for 'me' and 'other' in 'FSCDIPL_INFO_T'
 *     with values about what can be negotiated about.
 *     TODO: Move this one into 'dipl.c'
 * Input:
 *     me:        This is always an AI-Player (number of nation)
 *     other:     May be a human player (number of nation)
 *     weights *: Weights of leader for calculation of values
 */
void aidiplStart(char me, char other, char *weights)
{

}


/*
 * Name:
 *     aidiplActions (ai_diplomacy_actions)
 * Description:
 *     Do diplomatic actions. Must be called only after calculate function
 *     above has been run for _all_ AI players.
 *
 *     Only ever called for AI players and never for barbarians.
 * Input:
 *     me: This one is doing diplomacy actions
 */
void aidiplDiplomacyActions(char me)
{

    FSCDIPL_INFO_T Meet_Info[2];
    AIDIPL_INFO_T *my_info, *other_info, *my_info2;
    int info_list[50];  /* FIXME: Replace by PLAYER_MAX */
    int other_info_list[50];
    int p_idx, o_idx, p_idx2;
    NATION *pnation;
    int need_targets;
    char target;
    int most_hatred;
    int turns; /* turns since contact */
    int war_reason, i;
    MSG_INFO_T msg;
    FSCDIPL_CLAUSE Clause;


    pnation = leaderGet(me);
    if (! pnation -> ai_control) {

        /* FIXME: Log this one */
        return;

    }

    /* Initialize values */
    need_targets = 1;
    target = 0;
    most_hatred = MAX_AI_LOVE;

    /* ** Look up all players, about which we have diplomatic info at all ** */
    aidiplGetInfoList(me, &info_list[0]);

    p_idx = 0;
    while(info_list[p_idx] > 0) {

        my_info = &AiDiplInfo[info_list[p_idx]];

        /* ** If we are greviously insulted, go to war immediately. ** */
        if (my_info -> love < 0 && my_info -> has_reason_to_cancel >= 2 && my_info -> countdown == -1) {

            /* map.size ==> 2 --> The bigger the map, the longer the countdown */
            aidiplWarCountdown(my_info, 2, WAR_REASON_BEHAVIOUR, info_list);

        }

        p_idx++;

    }

    /* ** Declare war against somebody if we are out of targets ** */
    /* TODO: Make this depending on 'yield_wight' of actual nations leader */
    p_idx = 0;
    while(info_list[p_idx] > 0) {

        my_info = &AiDiplInfo[info_list[p_idx]];

        turns =  gameGetTurn();
        turns -= my_info -> first_contact_turn;

        if (my_info -> diplstate_type == FSCDIPL_PACT_WAR) {

            need_targets = 0;

        }
        else if (my_info -> love < most_hatred && turns > TURNS_BEFORE_TARGET) {

            most_hatred = my_info -> love;
            target = my_info -> other;

        }

        p_idx++;

    }

    if (need_targets && target && most_hatred < WAR_THRESHOLD
        && my_info -> countdown == -1) {

        if (my_info -> diplstate_type == FSCDIPL_PACT_ALLIANCE) {
            /* DIPLO_LOG(LOG_DEBUG, pplayer, target, "Plans war against an ally!"); */
        }

        if (my_info -> has_reason_to_cancel > 0) {
            /* We have good reason */
            war_reason = WAR_REASON_EXCUSE;
        }

        if (my_info -> love < 0) {
            /* We have a reason of sorts from way back, maybe? */
            war_reason = WAR_REASON_HATRED;
        } else {
            /* We have no legimitate reason... So what? */
            war_reason = WAR_REASON_NONE;
        }
        /* DIPLO_LOG(LOG_DEBUG, pplayer, target, "plans war for spoils"); */
        /* 4 + 1: 4 + map.size */
        aidiplWarCountdown(my_info, 4 + 1, war_reason, info_list);

    }

    /* Get list of contacts of our ally -- for declaration an check */
    aidiplGetInfoList(my_info -> other, &other_info_list[0]);

    /* ** Declare war - against enemies of allies ** */
    p_idx = 0;
    while(info_list[p_idx] > 0) {

        my_info = &AiDiplInfo[info_list[p_idx]];

        if (my_info -> diplstate_type == FSCDIPL_PACT_ALLIANCE) {

            o_idx = 0;
            while(other_info_list[o_idx] > 0) {

                other_info = &AiDiplInfo[info_list[p_idx]];
                if (other_info -> other != me
                    && other_info -> diplstate_type == FSCDIPL_PACT_WAR
                    && other_info -> countdown == -1) {

                    /* FIXME: Create diplomatic info, if needed */
                    /* 2 + 1: 2 + map.size */
                    aidiplWarCountdown(my_info, 2 + 1, WAR_REASON_ALLIANCE, info_list);
                    /* FIXME: We can only send them a message, if we know them (game-masters work) ? */

                }

                o_idx++;

            }

        }

        p_idx++;

    }

    /* ** Actually declare war (when we have moved units into position) ** */
    p_idx = 0;
    while(info_list[p_idx] > 0) {

        my_info = &AiDiplInfo[info_list[p_idx]];

        if (my_info -> countdown > 0) {

            my_info -> countdown--;

        } else if (my_info -> countdown == 0) {

            if (my_info -> diplstate_type != FSCDIPL_PACT_WAR) {

                /* DIPLO_LOG(LOG_DIPL2, pplayer, aplayer, "Declaring war!"); */
                aidiplGoToWar(my_info, my_info -> war_reason);

            }

        } else if (my_info -> countdown < -1) {

            /* negative countdown less than -1 is war stubbornness */
            my_info -> countdown++;

        }

        p_idx++;

    }

    /* ** Try to make peace with everyone we love ** */
    p_idx = 0;
    while(info_list[p_idx] > 0) {

        my_info = &AiDiplInfo[info_list[p_idx]];

        /* Remove shared vision if we are not allied or it is no longer safe. */
        if (my_info -> share_vision) {

            if (my_info -> diplstate_type < FSCDIPL_PACT_ALLIANCE ) {

                /* TODO: Add proper code */
                /* remove_shared_vision(pplayer, aplayer); */

            } else if (! aidiplSharedVisionIsSafe(my_info)) {

                msgPrepare(my_info -> owner, my_info -> other, &msg);
                aidiplMsgSend(1012, &msg);

                /* FIXME: Add proper code */
                /* remove_shared_vision(pplayer, aplayer); */

            }

        }

        /* No peace to enemies of our allies... or pointless peace. */
        if (my_info -> at_war_with_ally
            || my_info -> other ==  target     /* no mercy */
            || my_info -> countdown >= 0) {

                continue;
        }

        /* Spam control */
        for (i = 0; i < FSCDIPL_PACT_MAX; i++) {

            my_info -> asked_about[i] = (char)AIDIPL_MAX(my_info -> asked_about[i] - 1, 0);

        }

        my_info -> spam = (char)AIDIPL_MAX(my_info -> spam - 1, 0);
        if (my_info -> spam > 0) {
            /* Don't spam */
            continue;
        }

        /* Canvass support from existing friends for our war, and try to
         * make friends with enemies. Then we wait some turns until next time
         * we spam them with our gibbering chatter. */
        if (pnation -> ai_control) {
            if (my_info -> diplstate_type >= FSCDIPL_PACT_ALLIANCE) {
                my_info -> spam = (char)(fsctoolRand(4) + 3); /* Bugger allies often. */
            } else {
                my_info -> spam = (char)(fsctoolRand(8) + 6); /* Others are less important. */
            }

        }

        fscdiplInitMeeting(Meet_Info, my_info -> owner, my_info -> other);

        switch (my_info -> diplstate_type) {

            case FSCDIPL_PACT_TEAM:
                aidiplAiShare(my_info, Meet_Info);
                break;

            case FSCDIPL_PACT_ALLIANCE:
                /* See if our allies are diligently declaring war on our enemies... */
                if (my_info -> at_war_with_ally) {
                    break;
                }
                target = 0;

                p_idx2 = 0;
                while(info_list[p_idx2] > 0) {

                     /* Read the countdown check below carefully... Note that we check
                      * our ally's intentions directly here. */
                    my_info2 = &AiDiplInfo[info_list[p_idx]];
                    if (my_info2 -> diplstate_type == FSCDIPL_PACT_WAR
                        && aidiplHasDiplState(my_info2 -> other, my_info -> other, FSCDIPL_PACT_WAR)) {

                        /* FIXME: aidiplAtWar counts also countdown == -1 */
                        target = my_info2 -> other;
                        break;

                    }

                    p_idx2++;

                }

                if ((my_info -> diplstate_type == FSCDIPL_PACT_TEAM /* on same team */
                    || my_info -> love > MAX_AI_LOVE / 2)) {
                    /* Share techs only with team mates and allies we really like. */
                    aidiplAiShare(my_info, Meet_Info);
                }

                if (! target) {
                    my_info -> ally_patience = 0;
                    break; /* No need to nag our ally */
                }

                msg.other = target;
                switch (my_info -> ally_patience--) {
                    case 0:
                        aidiplMsgSend(1013, &msg);
                        break;
                    case -1:
                        aidiplMsgSend(1014, &msg);
                        break;
                    case -2:
                        aidiplMsgSend(1015, &msg);
                        /* DIPLO_LOG(LOG_DIPL2, pplayer, aplayer, "breaking useless alliance"); */
                        /* to peace */
                        my_info -> diplstate_type = FSCDIPL_PACT_PEACE;
                        /* handle_diplomacy_cancel_pact(pplayer, player_number(aplayer), CLAUSE_ALLIANCE); */
                        my_info -> love = AIDIPL_MIN(my_info -> love, 0);
                        if (my_info -> share_vision) {
                            my_info -> share_vision = 0;
                            /* FIXME: Add proper code */
                            /* remove_shared_vision(pplayer, aplayer); */
                        }
                        /* assert(!gives_shared_vision(pplayer, aplayer)); */
                        break;
                }
                break;

            case FSCDIPL_PACT_PEACE:
                Clause.type = FSCDIPL_CLAUSE_PACT;
                Clause.number = FSCDIPL_PACT_ALLIANCE;
                if (my_info -> at_war_with_ally
                   || (my_info -> asked_about[FSCDIPL_PACT_ALLIANCE] > 0)
                   || aidiplGoldequivClause(my_info, &Clause, &Meet_Info[0], FSCDIPL_PACT_ALLIANCE) < 0) {
                    break;
                }
                fscdiplAddClause(my_info -> owner, my_info -> other, FSCDIPL_CLAUSE_PACT, FSCDIPL_PACT_ALLIANCE, 0);
                my_info -> asked_about[FSCDIPL_PACT_ALLIANCE] = 13;
                aidiplMsgSend(1016, &msg);
                break;

            case FSCDIPL_PACT_CEASEFIRE:
                Clause.type = FSCDIPL_CLAUSE_PACT;
                Clause.number = FSCDIPL_PACT_PEACE;
                if (my_info -> at_war_with_ally
                    || (my_info -> asked_about[FSCDIPL_PACT_PEACE] > 0)
                    || aidiplGoldequivClause(my_info, &Clause, &Meet_Info[0], FSCDIPL_PACT_PEACE) < 0) {
                    break;
                }
                fscdiplAddClause(my_info -> owner, my_info -> other, FSCDIPL_CLAUSE_PACT, FSCDIPL_PACT_PEACE, 0);
                my_info -> asked_about[FSCDIPL_PACT_PEACE] = 12;
                aidiplMsgSend(1017, &msg);
                break;
            case FSCDIPL_PACT_WAR:
                Clause.type   = FSCDIPL_CLAUSE_PACT;
                Clause.number = FSCDIPL_PACT_CEASEFIRE;

                if ((my_info -> asked_about[FSCDIPL_PACT_CEASEFIRE] > 0)
                    || aidiplGoldequivClause(my_info, &Clause, &Meet_Info[0], FSCDIPL_PACT_CEASEFIRE) < 0) {
                    break; /* Fight until the end! */
                }
                fscdiplAddClause(my_info -> owner, my_info -> other, FSCDIPL_CLAUSE_PACT, FSCDIPL_PACT_CEASEFIRE, 0);
                my_info -> asked_about[FSCDIPL_PACT_CEASEFIRE] = 9;
                aidiplMsgSend(1018, &msg);
                break;
            case FSCDIPL_PACT_ARMISTICE:
                break;
            case FSCDIPL_PACT_NOCONTACT:
            default:
                /* FIXME: Log unknown pact type */
            break;
        }

        p_idx++;

    }

    /* NEW presentation: Start diplomacy, it there are some clauses to speak about */
    if (fscdiplGetClauses() -> id > 0) {



    }

}

/*
 * Name:
 *     aidiplEndOfTurn
 * Description:
 *     Does all actions needed at end of turn for Ai-Code
 * Input:
 *      None
 */
int aidiplEndOfTurn(void)
{

    return 0;


}




