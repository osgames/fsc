/*******************************************************************************
*  AIPLAYER_T.C                                                                *
*	- General AI-Nation decision, uses data from LEADER for evaluation         *
*									                                           *
*  FREE SPACE COLONISATION                                                     *
*      (c)2002 - 2017 Paul Mueller <muellerp61@bluewin.ch>                     *
*                                                                              *
*   This program is free software; you can redistribute it and/or modify       *
*   it under the terms of the GNU General Public License as published by       *
*   the Free Software Foundation; either version 2 of the License, or          *
*   (at your option) any later version.                                        *
*                                                                              *
*   This program is distributed in the hope that it will be useful,            *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Library General Public License for more details.                       *
*                                                                              *
*   You should have received a copy of the GNU General Public License          *
*   along with this program; if not, write to the Free Software                *
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
*******************************************************************************/

/*******************************************************************************
* INCLUDES     							                                       *
*******************************************************************************/

#include <string.h>     /* memset() */
#include <stdlib.h>     /* abs()    */


#include "../code/fscshare.h"       /* Definitions of GVAL_...      */
#include "../code/fsctool.h"
#include "../code/fscmap.h"         /* Map functions                */
#include "../code/game.h"           /* Management struct of game    */
#include "../code/goal.h"           /* Handling of AI-Goals         */
#include "../code/improve.h"
#include "../code/msg.h"
#include "../code/player.h"
#include "../code/unittype.h"

#if 0
#include "fsctool.h"
#include "colony.h"
#include "fscmap.h"


#include "unitmove.h"       /* Base management of unit      */
#include "unittype.h"


#include "aidipl.h"
#include "aiunit.h"
#endif

#include "aicolony.h"
#include "aitech.h"
#include "aitool.h"

/* -- Own header -- */
#include "aiplayer.h"

/*******************************************************************************
* DEFINES      							                                       *
*******************************************************************************/

#define AIPLAYER_MAX_WANT       1000
#define AIPLAYER_CRUCIAL_WANT    900

/*******************************************************************************
* DATA      							                                       *
*******************************************************************************/

static GAME_PLAYINFO_T AiPlayInfo;

/*******************************************************************************
* CODE      							                                       *
*******************************************************************************/

/*
 * Name:
 *     player_choose_tech
 * Description:
 *     Chooses a technology, based on 'pweights'
 * Input:
 *     player_no:     Number of player
 *     logpweights *: Weights to use for calculation the value
 */
static void player_choose_tech(int player_no, char *pweights)
{
    PLAYER_RESEARCH_T research;
    int best_tech_no;


    /* -- Get the information about the research done -- */
    player_get_researchinfo(player_no, &research);

    best_tech_no = aitech_choose(research.advancebits, pweights);

    player_set_research(player_no, best_tech_no);
}

/*
 * Name:
 *     aiplayer_new_game
 * Description:
 *     The AI does here it's setup choices, depending on:
 *     From game:
 *          - Size in sectors
 *          - Percentage of habitable planets (from seldom to any)
 *          TODO: Add this choices
 *      Does _NOT_ check if given player is human!
 * Input:
 *     player_no: Number of player to make AI-Setup for
 *     log_on:    Write to log yes/no
 */
static void aiplayer_new_game(int player_no, char log_on)
{
    PLAYER_RACE_T race_info;
    IMPROVE_INFO_T improve_info;
    FSCMAP_POSINFO_T pi;
    FSCMAP_TILEINFO_T ti[5], *pti;
    GAME_EFFECT_T ge[6];
    MSG_INFO_T pmsg;
    GOAL_T goal;
    int num_star;
    int share_points, set_val;
    char etype, best_etype;
    int value, my_pos;
    int best_value, num_eff;
    int num_colony, i;


    /* Do the players setup... */
    player_race(player_no, &race_info, 0);

    /* -- Set randon decision weights */
    share_points = 3 * GVAL_MAX;

    for(i = 0; i < GVAL_MAX; i++)
    {
        set_val = fsctool_rand_no(6) -1;

        race_info.base_weight[i] = (char)set_val;
        race_info.act_weight[i]  = (char)set_val;

        share_points -= set_val;

        if(share_points <= 0)
        {
            /* The weights are shared */
            break;
        }
    }

    /* Share 10 points ability bonuses like in game setup */
    share_points = 10;

    for(i = 0; i < 10; i++)
    {
        /* Points to set for special bonus */
        set_val       = fsctool_rand_no(2);
        share_points -= set_val;

        if(share_points < 0)
        {
            set_val      += share_points;
            share_points = 0;
        }

        race_info.fix_bonus[i] = (char)set_val;

        if(set_val <= 0)
        {
            break;
        }
    }

    /* -- And set it -- */
    player_race(player_no, &race_info, 1);

    goal_init(player_no, GOAL_WHO_LEADER, &goal);
    goal.want   = AIPLAYER_MAX_WANT;


    /* Choose two tech goals: One based on unittype, one based on improvement */
    /* First tech goal: based on unit                                         */
    /* TODO: Move this code to 'aitech', translate GOAL_T improvement + unit
             to a GOAL_T technology                                             */
    best_value = 0;
    best_etype = 0;

    for(etype = 0; etype < 10; etype++)
    {
        num_eff = unittype_get_effect(etype, &ge[0]);

        value = aitool_get_effectvalue(ge, race_info.act_weight, num_eff);

        if( value >= best_value)
        {
            /* Take farther goal */
            best_value = value;
            best_etype = etype;
        }
    }

    goal.type   = GOAL_TYPE_UNIT;
    goal.value  = best_etype;
    goal.want   = AIPLAYER_CRUCIAL_WANT - 10;
    goal_set(&goal);

    /* -- Log it, if asked for --- */
    if(log_on)
    {
        msg_prepare(0, player_no, MSG_TYPE_LOG, &pmsg);
        pmsg.num      = MSG_GOAL_UNITTYPE;
        pmsg.args[0]  = goal.value;
        pmsg.args[1]  = goal.want;

        msg_send(&pmsg, 0);
    }

    /* ------- TODO: Translate GOAL_T unit to GOAL_T tech -------- */

    /* --------- Second look for best improvement ------------ */
    best_value = 0;
    best_etype = 0;

    for(etype = 1; etype < 20; etype++)
    {
        improve_get_info(etype, &improve_info, 0, 0);

        value = aitool_get_effectvalue(&improve_info.effect, race_info.act_weight, 1);

        if(value >= best_value)
        {
            /* Take farther goal */
            best_value = value;
            best_etype = etype;
        }
    }

    /* ------- TODO: Translate GOAL_T unit to GOAL_T tech -------- */
    goal.type   = GOAL_TYPE_IMPROVE;
    goal.value  = best_etype;
    goal_set(&goal);

    /* -- Log it --- */
    if(log_on)
    {
        pmsg.num         = MSG_GOAL_IMPROVE;
        pmsg.improve_no  = goal.value;
        pmsg.args[1]     = goal.want;
        msg_send(&pmsg, 0);
    }

    /* ------- Set stars as goals for colonization ----- */
    num_colony = 1 + race_info.act_weight[GVAL_NUMCOLONY] / 3;

    /* ------- Look for stars, that can be settled ---------- */
    my_pos = player_get_homepos(player_no);

    /*
    num_star = fscmap_star_list(player_no, star_list, 58, 0, 0);
    */
    pi.owner = (char)player_no;

    fscmap_xy_frompos(my_pos, &pi);

    num_star = fscmap_get_distinfo(&pi, 3, ti, 3, FSCMAP_FINFO_STAR);

    /* ---- Preset goal values --- */
    goal.type = GOAL_TYPE_COLONY;

    num_colony = (num_star < num_colony) ? num_star : num_colony;

    for(i = 0; i < num_colony; i++)
    {
        goal.pos = ti[i].pos;
        /* Higher want for nearer stars, differ wants, if same distance */
        goal.want = 1000 - ((i + 1) * 48) - (i * 6);

        if(goal.want > 0)
        {
            goal_set(&goal);

            if(log_on)
            {
                /* -- Log it --- */
                pmsg.num     = MSG_GOAL_COLONY;
                pmsg.pos     = goal.pos;
                pmsg.args[1] = goal.want;
                msg_send(&pmsg, 0);
            }
        }

        num_star--;
    }

    /* Set tech to research */
    player_choose_tech(player_no, race_info.act_weight);
}


/*
 * Name:
 *     aiplayer_manage_goals
 * Description:
 *     This player manages its GOAL_T, based on the RACE_T weights.
 *     Simply reduce the want for each GOAL_T at every turn.
 * Input:
 *     player_no: Manage GOALS for this player
 */
static void aiplayer_manage_goals(int player_no)
{
    int goal_list[100];
    int num_goal, i;
    GOAL_T *pgoal;


    num_goal = goal_get_list(player_no, goal_list);

    for(i = 0; i < num_goal; i++)
    {
        /* @TODO: Manage the goals, based on leaders weight */
        pgoal = goal_get(goal_list[i]);

        if( pgoal->want < AIPLAYER_CRUCIAL_WANT)
        {
            /* Want for a GOAL_T is reduced, if not crucial want  */
            pgoal->want -= 30;

            if( pgoal->want <= 0)
            {
                goal_delete(goal_list[i]);
            }
        }
    }
}

/*
 * Name:
 *     aiplayer_handle_diplmsg
 * Description:
 *     Handles messages generated by the 'fsccmd'-Module for diplomacy
 * Input:
 *     pmsg *:     Message to handle
 *     player_no: This player should manage the message
 */
static void aiplayer_handle_diplmsg(MSG_INFO_T *pmsg, char player_no)
{
    /* TODO: Fill in reaction on any possible action and info-messages */
    switch(pmsg->num)
    {
        /* ------- Diplomacy messages --------- */
        case MSG_DIPL_START:
            /* Start of negotiations from/to player */
            /*
            aidiplStart(pmsg->to, pmsg->from, leaderGet(player_no)->yieldweight);
            */
            break;

        case MSG_DIPL_NEGOTIATE:
            /* Do negotiations */
            /*
            aidiplNegotiate(pmsg->to, pmsg->from, leaderGet(player_no)->yieldweight);
            break;
            */

        case MSG_DIPL_DONE:
            /* Negotiations are done */
            /*
            aidiplDone(pmsg->to, pmsg->from);
            */
            break;
    }
}



/*
 * Name:
 *     aiplayer_get_descision
 * Description:
 *     Leader makes a decision between actual and new goal based on
 *     value of given effects
 * Input:
 *     player_no:   Number of player to get decision from
 *     pge *:       Pointer on List of game effects of 'pnew_goal' to calc weight for
 *     pact_goal *: Pointer on actual goal
 *     pnew_goal *: Pointer on new goal offered
 *
 */
static void aiplayer_get_descision(int player_no, GAME_EFFECT_T *pge, GOAL_T *pact_goal, GOAL_T *pnew_goal)
{
    const char *pweights;


    /* Get the weighting for decision */
    pweights = player_get_weights(player_no);

    pnew_goal->value = aitool_get_effectvalue(pge, pweights, 1);

    if( pnew_goal->value > pact_goal->value)
    {
        /* Idea: Choose at random, if value difference is less than 10% */
        memcpy(pact_goal, pnew_goal, sizeof(GOAL_T));
    }
}


/*
 * Name:
 *     aiplayer_handle_result
 * Description:
 *     Handles results generated by the game and other players
 *     - Messages from other players
 *     - Results from a command given
 * Input:
 *     pmsg *:     Message to handle
 *     player_no:  This player should manage the message
 *     pweights *: To get into accout for choosing something
 */
static void aiplayer_handle_result(MSG_INFO_T *pmsg, int player_no, char *pweights)
{
    switch(pmsg->num)
    {
        case GRES_NEW_GAME:
            aiplayer_new_game(player_no, 1);
            break;

        case GRES_BEGIN_TURN:
            game_get_playinfo(&AiPlayInfo);
            break;

        case GRES_END_TURN:
            break;

        case GMSG_RESEARCHDONE:
            player_choose_tech(player_no, pweights);
            break;
    }
}

/*
 * Name:
 *     aiplayer_handle_message
 * Description:
 *     Handles general messages, e.g "Improvement built"
 * Input:
 *     player_no: This player should handle the message
 *     pmsg *:     Message to handle
 */
void aiplayer_handle_message(int player_no, MSG_INFO_T *pmsg)
{
    const char *pweights;


    /* Get the weighting for decision */
    pweights = player_get_weights(player_no);

    switch(pmsg->type)
    {
        /*
        case MSG_TYPE_GAME:
            aiplayer_handle_result(pmsg, player_no, pweights);
            break;
        */

        case MSG_TYPE_DIPL:
            aiplayer_handle_diplmsg(pmsg, player_no);
            break;

        case MSG_TYPE_COLONY:
            aicolony_manage(pmsg->colony_no, pmsg->num, AiPlayInfo.ut_avail, pweights);
            break;

        default:
            /* TODO: Handle all other types of messages */
            break;
    }
}

/* ========================================================================== */
/* ========================= PUBLIC FUNCTIONS =============================== */
/* ========================================================================== */

/*
 * Name:
 *     aiplayer_manage
 * Description:
 *     Manages all
 * Input:
 *     player_no: Manage this player by AI
 * Output:
 *     Done with all management, yes/no
 */
int aiplayer_manage(char player_no)
{
    MSG_INFO_T msg;
    const char *pweights;


    /* Get the weighting for decision */
    pweights = player_get_weights(player_no);


    /* FIRST: Manage results from game */
    if(game_get_result(player_no, &msg))
    {
        aiplayer_handle_result(&msg, player_no, pweights);

        return 0;
    }

    /* SECOND: React on general messages e.g Unit built */
    if(game_get_msg(&msg, 0))
    {
        aiplayer_handle_message(player_no, &msg);

        return 0;
    }


    /* THIRD: Manage all units */
    if(aiunit_manage(player_no))
    {
        return 1;
    }

    /* LAST: Manage GOALS */
    aiplayer_manage_goals(player_no);

    return 1;
}

