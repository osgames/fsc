/*******************************************************************************
*  AIUNIT.C                                                                    *
*      - AI handling of units                                                  *
*                                                                              *
*  FREE SPACE COLONISATION                                                     *
*      (c)2002 - 2017 Paul Mueller <muellerp61@bluewin.ch>                     *
*                                                                              *
*  This program is free software; you can redistribute it and/or modify        *
*  it under the terms of the GNU General Public License as published by        *
*  the Free Software Foundation; either version 2 of the License, or           *
*  (at your option) any later version.                                         *
*                                                                              *
*  This program is distributed in the hope that it will be useful,             *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of              *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
*  GNU Library General Public License for more details.                        *
*                                                                              *
*  You should have received a copy of the GNU General Public License           *
*  along with this program; if not, write to the Free Software                 *
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  *
*******************************************************************************/


/*******************************************************************************
* INCLUDES                                                                     *
*******************************************************************************/

#include <string.h>


#include "../code/fscmap.h"
#include "../code/fsctool.h"
#include "../code/starsys.h"
#include "../code/unit.h"
#include "../code/unitinfo.h"
#include "../code/unitrule.h"
#include "../code/unittype.h"


#include "aiconstr.h"
#include "aisettle.h"


/* -- Own header -- */
#include "aiunit.h"

/*******************************************************************************
* DATA                                                                         *
*******************************************************************************/

/* static SDLGLMAP_VIEWPORT UnitVp; */    /* User for planning of targets */

/*******************************************************************************
* CODE                                                                         *
*******************************************************************************/

/*
 * Name:
 *     aiunitMilitaryManage
 * Description:
 *     Finds a job for a military unit
 * Input:
 *     punit_info *: Management info about unit to manage
 */
static void aiunitMilitaryManage(UNIT_INFO_T *punit_info)
{
    /* TODO: Move this to aimilit.c */
    unitmove_set_order(punit_info, ORDER_EXPLORE, 0, 0);
}

/*
 * Name:
 *     aiunitTransportManage
 * Description:
 *
 * Input:
 *     unit_info *: Management info about unit to manage
 */
static void aiunitTransportManage(UNIT_INFO_T *punit_info)
{
    /* TODO: Move this to aitrans.c */
    unitmove_set_order(punit_info, ORDER_EXPLORE, 0, 0);
}

/*
 * Name:
 *     aiunitScoutManage
 * Description:
 *     Finds a job for a scout.
 *         - Explore new terrain        ==> ORDER_EXPLORE
 *         - Control of players border  ==> ORDER_BORDERPATROL
 * Input:
 *     unit_info *: Management info about unit to manage
 */
static void aiunitScoutManage(UNIT_INFO_T *punit_info)
{
    /* TODO: Change to 'BORDERPATROL', if nothing to explore.
       Better exploration code aiexplor.c */
    unitmove_set_order(punit_info, ORDER_EXPLORE, 0, 0);

}

/*
 * Name:
 *     aiunitDiplomacyManage
 * Description:
 *     Finds a job for a diplomacy unit
 *          -
 * Input:
 *     unit_info *: Info about actual unit to manage
 */
static void aiunitDiplomacyManage(UNIT_INFO_T *punit_info)
{
    /* TODO: Find a planet to influence or a planet to incite */
    unitmove_set_order(punit_info, ORDER_BORDERPATROL, 0, 0);
}

/*
 * Name:
 *     aiunitTradeManage
 * Description:
 *     Finds a job for a trade unit.
 * Input:
 *     unit_info *: Info about actual unit to manage
 */
static void aiunitTradeManage(UNIT_INFO_T *punit_info)
{
    /* TODO:  Find a planet to trade with (install a trade route) */
    unitmove_set_order(punit_info, ORDER_BORDERPATROL, 0, 0);
}

/* ========================================================================== */
/* ============================= THE MAIN ROUTINE(S) ======================== */
/* ========================================================================== */

/*
 * Name:
 *     aiunit_manage
 * Description:
 *     Manages given unit.
 *     Finds a job for the actual unit, if the given unit has no orders.
 *     Does _NOT_ check if the owner of the unit is a human player or not!
 * Input:
 *     punit_info *: Pointer on struct with management info
 * Output:
 *     Still units to manage yes/no
 * Last change:
 *     2017-08-06 <bitnapper>
 */
int aiunit_manage(UNIT_INFO_T *punit_info)
{
    int unit_no;
    UNIT_T *punit;
    int  unit_list[GAME_MAX_LIST + 2];   /* List of units (numbers)          */


    if(punit_info->unit_no > 0)
    {
        if(punit_info->numpos_reachable == 0)
        {
            /* If the unit has no range to move, skip it */
            unitmove_set_order(punit_info, ORDER_SENTRY, 0, 0);
        }

        if(punit->order.what > 0)
        {
            return 1;
        }

        /* --- Manage the unit depending on its abilities --- */
        switch(punit->ability)
        {
            case UNIT_ABILITY_MILITARY:
                aiunitMilitaryManage(punit_info);
                break;

            case UNIT_ABILITY_COLONIZE:     /* Colonize planets --> SETTLE  */
                aisettle_manage(punit_info);
                break;

            case UNIT_ABILITY_TRANSPORT:    /* Troop transport/transport    */
                aiunitTransportManage(punit_info);
                break;

            case UNIT_ABILITY_DIPLOMACY:    /* Diplomacy                    */
                aiunitDiplomacyManage(punit_info);
                break;

            case UNIT_ABILITY_TRADE:
                aiunitTradeManage(punit_info);
                break;

            case UNIT_ABILITY_SCOUT:        /* Explore Superiority          */
                aiunitScoutManage(punit_info);
                break;

            case UNIT_ABILITY_CONSTRUCT:    /* Construction of starbases    */
                aiconstr_manage(punit_info);
                break;

            default:
                unitmove_set_order(punit_info, ORDER_EXPLORE, 0, 0);
                return 1;
        }

        return 1;
    }

    /* All units managed */
    return 0;
}

/*
 * Name: aiuni_manage_all
 * Description:
 *     Manages all units for given AI-Player
 * Input:
 *     player_no: Number of player to manage units for
 * Output:
 *     None
 * Last change:
 *     2017-07-21 bitnapper
 */
void aiuni_manage_all(char player_no)
{
    UNIT_INFO_T unit_info;
    int unit_no;
    int i, j;


    /* If there is a unit to manage at all... */
    /* Try it two times */
    for(j = 0; j < 2; j++)
    {
        /* Try two times for every unit */
        unit_no = 0;

        do
        {
            unit_no = unit_find_next(player_no, unit_no, 1);

            unitmove_fill_manageinfo(player_no, unit_no, &unit_info);

            if(unit_info.numpos_reachable == 0)
            {
                /* If the unit has no range to move, skip it */
                unitmove_set_order(&unit_info, ORDER_SENTRY, 0, 0);
            }
            else
            {
                if(unit_info.order_no == 0)
                {
                    /* --- Manage the unit depending on its abilities --- */
                    switch(unit_info.ability)
                    {
                        case UNIT_ABILITY_MILITARY:
                            aiunitMilitaryManage(&unit_info);
                            break;

                        case UNIT_ABILITY_COLONIZE:
                            /* Colonize planets --> SETTLE  */
                            aisettle_manage(&unit_info);
                            break;

                        case UNIT_ABILITY_TRANSPORT:    /* Troop transport/transport    */
                            aiunitTransportManage(&unit_info);
                            break;

                        case UNIT_ABILITY_DIPLOMACY:    /* Diplomacy                    */
                            aiunitDiplomacyManage(&unit_info);
                            break;

                        case UNIT_ABILITY_TRADE:
                            aiunitTradeManage(&unit_info);
                            break;

                        case UNIT_ABILITY_SCOUT:        /* Explore Superiority          */
                            aiunitScoutManage(&unit_info);
                            break;

                        case UNIT_ABILITY_CONSTRUCT:    /* Construction of starbases    */
                            aiconstr_manage(&unit_info);
                            break;

                        default:
                            unitmove_set_order(&unit_info, ORDER_EXPLORE, 0, 0);
                    }
                }
            }
        }
        while(unit_no > 0);
    }

    /* All units managed */
    return 0;
}
